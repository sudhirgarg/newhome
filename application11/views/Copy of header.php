<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>iApotheca | Seamless Care Pharmacy</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>css/cloud-admin.css">
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>css/themes/default.css"
	id="skin-switcher">
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>css/responsive.css">
<!-- STYLESHEETS -->
<!--[if lt IE 9]><script src="js/flot/excanvas.min.js"></script><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
<!-- ANIMATE -->
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>css/animatecss/animate.min.css" />
<!-- DATE RANGE PICKER -->
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
<!-- DATA TABLES -->
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>js/datatables/media/css/jquery.dataTables.min.css" />
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>js/datatables/media/assets/css/datatables.min.css" />
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>js/datatables/extras/TableTools/media/css/TableTools.min.css" />

<!-- TABLE CLOTH -->
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>js/tablecloth/css/tablecloth.min.css" />
<!-- TODO -->
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>js/jquery-todo/css/styles.css" />
<!-- FULL CALENDAR -->
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>js/fullcalendar/fullcalendar.min.css" />
<!-- GRITTER -->
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>js/gritter/css/jquery.gritter.css" />
<!-- XCHARTS -->
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>js/xcharts/xcharts.min.css" />

<!-- DATE PICKER -->
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>js/datepicker/themes/default.min.css" />
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>js/datepicker/themes/default.date.min.css" />
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>js/datepicker/themes/default.time.min.css" />


<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>css/custom.css" />
	
<!-- FONTS -->
<link
	href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'
	rel='stylesheet' type='text/css'>
<link
	href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css"
	rel="stylesheet">
</head>
<body>
	<!-- HEADER -->
	<header class="navbar clearfix navbar-fixed-top" id="header">
		<div class="container">
			<div class="navbar-brand">
				<!-- COMPANY LOGO -->
				<a href="<?php echo base_url();?>news/myNews"> 
					<img src="<?php echo base_url();?>img/logo/logo.gif"
						alt="Cloud Admin Logo" class="img-responsive" height="30"
						width="200">
				</a>
				<!-- /COMPANY LOGO -->
				<!-- TEAM STATUS FOR MOBILE -->
				<div class="visible-xs">
					<a href="#" class="team-status-toggle switcher btn dropdown-toggle">
						<i class="fa fa-users"></i>
					</a>
				</div>
				<!-- /TEAM STATUS FOR MOBILE -->
				<!-- SIDEBAR COLLAPSE -->
				<div id="sidebar-collapse" class="sidebar-collapse btn">
					<i class="fa fa-bars" data-icon1="fa fa-bars"
						data-icon2="fa fa-bars"></i>
				</div>
				<!-- /SIDEBAR COLLAPSE -->
			</div>
			<!-- NAVBAR LEFT -->
			<ul class="nav navbar-nav pull-left hidden-xs" id="navbar-left">
				<li class="dropdown"><a href="#"
					class="team-status-toggle dropdown-toggle tip-bottom"
					data-toggle="tooltip" title="Toggle Team View"> <i
						class="fa fa-users"></i> <span class="name">Team</span>
						<i class="fa fa-angle-down"></i>
				</a></li>
			</ul>
			<!-- /NAVBAR LEFT -->
			<!-- BEGIN TOP NAVIGATION MENU -->
			<ul class="nav navbar-nav pull-right">
				<!-- BEGIN NOTIFICATION DROPDOWN -->
				<li class="dropdown" id="header-notification"><a href="#"
					class="dropdown-toggle" data-toggle="dropdown"> <i
						class="fa fa-bell"></i> <span class="badge">7</span>
				</a>
					<ul class="dropdown-menu notification">
						<li class="dropdown-title"><span><i class="fa fa-bell"></i> 7
								Notifications</span></li>
						<li><a href="#"> <span class="label label-success"><i
									class="fa fa-user"></i></span> <span class="body"> <span
									class="message">5 users online. </span> <span class="time"> <i
										class="fa fa-clock-o"></i> <span>Just now</span>
								</span>
							</span>
						</a></li>
						<li><a href="#"> <span class="label label-primary"><i
									class="fa fa-comment"></i></span> <span class="body"> <span
									class="message">Martin commented.</span> <span class="time"> <i
										class="fa fa-clock-o"></i> <span>19 mins</span>
								</span>
							</span>
						</a></li>
						<li><a href="#"> <span class="label label-warning"><i
									class="fa fa-lock"></i></span> <span class="body"> <span
									class="message">DW1 server locked.</span> <span class="time"> <i
										class="fa fa-clock-o"></i> <span>32 mins</span>
								</span>
							</span>
						</a></li>
						<li><a href="#"> <span class="label label-info"><i
									class="fa fa-twitter"></i></span> <span class="body"> <span
									class="message">Twitter connected.</span> <span class="time"> <i
										class="fa fa-clock-o"></i> <span>55 mins</span>
								</span>
							</span>
						</a></li>
						<li><a href="#"> <span class="label label-danger"><i
									class="fa fa-heart"></i></span> <span class="body"> <span
									class="message">Jane liked. </span> <span class="time"> <i
										class="fa fa-clock-o"></i> <span>2 hrs</span>
								</span>
							</span>
						</a></li>
						<li><a href="#"> <span class="label label-warning"><i
									class="fa fa-exclamation-triangle"></i></span> <span
								class="body"> <span class="message">Database overload.</span> <span
									class="time"> <i class="fa fa-clock-o"></i> <span>6 hrs</span>
								</span>
							</span>
						</a></li>
						<li class="footer"><a href="#">See all notifications <i
								class="fa fa-arrow-circle-right"></i></a></li>
					</ul></li>
				<!-- END NOTIFICATION DROPDOWN -->
				<!-- BEGIN INBOX DROPDOWN -->
				<li class="dropdown" id="header-message"><a href="#"
					class="dropdown-toggle" data-toggle="dropdown"> <i
						class="fa fa-envelope"></i> <span class="badge">3</span>
				</a>
					<ul class="dropdown-menu inbox">
						<li class="dropdown-title"><span><i class="fa fa-envelope-o"></i>
								3 Messages</span> <span class="compose pull-right tip-right"
							title="Compose message"><i class="fa fa-pencil-square-o"></i></span>
						</li>
						<li><a href="#"> <img
								src="<?php echo base_url();?>img/avatars/avatar2.jpg" alt="" />
								<span class="body"> <span class="from">Jane Doe</span> <span
									class="message"> Duis autem vel eum iriure dolor in hendrerit
										in vulputate velit esse mole ... </span> <span class="time"> <i
										class="fa fa-clock-o"></i> <span>Just Now</span>
								</span>
							</span>

						</a></li>
						<li><a href="#"> <img
								src="<?php echo base_url();?>img/avatars/avatar1.jpg" alt="" />
								<span class="body"> <span class="from">Vince Pelt</span> <span
									class="message"> Duis autem vel eum iriure dolor in hendrerit
										in vulputate velit esse mole ... </span> <span class="time"> <i
										class="fa fa-clock-o"></i> <span>15 min ago</span>
								</span>
							</span>

						</a></li>
						<li><a href="#"> <img
								src="<?php echo base_url();?>img/avatars/avatar8.jpg" alt="" />
								<span class="body"> <span class="from">Debby Doe</span> <span
									class="message"> Duis autem vel eum iriure dolor in hendrerit
										in vulputate velit esse mole ... </span> <span class="time"> <i
										class="fa fa-clock-o"></i> <span>2 hours ago</span>
								</span>
							</span>

						</a></li>
						<li class="footer"><a href="#">See all messages <i
								class="fa fa-arrow-circle-right"></i></a></li>
					</ul></li>
				<!-- END INBOX DROPDOWN -->
				<!-- BEGIN TODO DROPDOWN -->
				<li class="dropdown" id="header-tasks"><a href="#"
					class="dropdown-toggle" data-toggle="dropdown"> <i
						class="fa fa-tasks"></i> <span class="badge">3</span>
				</a>
					<ul class="dropdown-menu tasks">
						<li class="dropdown-title"><span><i class="fa fa-check"></i> 6
								tasks in progress</span></li>
						<li><a href="#"> <span class="header clearfix"> <span
									class="pull-left">Software Update</span> <span
									class="pull-right">60%</span>
							</span>
								<div class="progress">
									<div class="progress-bar progress-bar-success"
										role="progressbar" aria-valuenow="60" aria-valuemin="0"
										aria-valuemax="100" style="width: 60%;">
										<span class="sr-only">60% Complete</span>
									</div>
								</div>
						</a></li>
						<li><a href="#"> <span class="header clearfix"> <span
									class="pull-left">Software Update</span> <span
									class="pull-right">25%</span>
							</span>
								<div class="progress">
									<div class="progress-bar progress-bar-info" role="progressbar"
										aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"
										style="width: 25%;">
										<span class="sr-only">25% Complete</span>
									</div>
								</div>
						</a></li>
						<li><a href="#"> <span class="header clearfix"> <span
									class="pull-left">Software Update</span> <span
									class="pull-right">40%</span>
							</span>
								<div class="progress progress-striped">
									<div class="progress-bar progress-bar-warning"
										role="progressbar" aria-valuenow="40" aria-valuemin="0"
										aria-valuemax="100" style="width: 40%;">
										<span class="sr-only">40% Complete</span>
									</div>
								</div>
						</a></li>
						<li><a href="#"> <span class="header clearfix"> <span
									class="pull-left">Software Update</span> <span
									class="pull-right">70%</span>
							</span>
								<div class="progress progress-striped active">
									<div class="progress-bar progress-bar-danger"
										role="progressbar" aria-valuenow="70" aria-valuemin="0"
										aria-valuemax="100" style="width: 70%;">
										<span class="sr-only">70% Complete</span>
									</div>
								</div>
						</a></li>
						<li><a href="#"> <span class="header clearfix"> <span
									class="pull-left">Software Update</span> <span
									class="pull-right">65%</span>
							</span>
								<div class="progress">
									<div class="progress-bar progress-bar-success"
										style="width: 35%">
										<span class="sr-only">35% Complete (success)</span>
									</div>
									<div class="progress-bar progress-bar-warning"
										style="width: 20%">
										<span class="sr-only">20% Complete (warning)</span>
									</div>
									<div class="progress-bar progress-bar-danger"
										style="width: 10%">
										<span class="sr-only">10% Complete (danger)</span>
									</div>
								</div>
						</a></li>
						<li class="footer"><a href="#">See all tasks <i
								class="fa fa-arrow-circle-right"></i></a></li>
					</ul></li>
				<!-- END TODO DROPDOWN -->
				<!-- BEGIN USER LOGIN DROPDOWN -->
				<li class="dropdown user" id="header-user"><a href="#"
					class="dropdown-toggle" data-toggle="dropdown"> <img alt=""
						src="<?php echo base_url();?>img/avatars/avatar3.jpg" /> <span
						class="username">
						<?php
						
						$user = $this->ion_auth->user ()->row ();
						if (! empty ( $user->first_name )) {
							echo $user->first_name . ' ' . $user->last_name;
						}
						
						?>
						</span> <i class="fa fa-angle-down"></i>
				</a>
					<ul class="dropdown-menu">
						<li><a href="#"><i class="fa fa-user"></i> My Profile</a></li>
						<li><a href="#"><i class="fa fa-cog"></i> Account Settings</a></li>
						<li><a href="#"><i class="fa fa-eye"></i> Privacy Settings</a></li>
						<li><a><?php if ($this->quick->is_internal()){echo '<i class="fa fa-building-o"></i> Internal User'; }else {echo '<i class="fa fa-cloud"></i> External User';} ?></a></li>
						<li><a href="<?php echo base_url();?>auth/logout"><i
								class="fa fa-power-off"></i> Log Out</a></li>
					</ul></li>
				<!-- END USER LOGIN DROPDOWN -->
			</ul>
			<!-- END TOP NAVIGATION MENU -->
		</div>
		<!-- TEAM STATUS -->
		<div class="container team-status" id="team-status">
			<div id="scrollbar">
				<div class="handle"></div>
			</div>
			<div id="teamslider">
				<ul class="team-list">
					<li class="current"><a href="javascript:void(0);"> <span
							class="image"> <img
								src="<?php echo base_url();?>img/avatars/avatar3.jpg" alt="" />
						</span> <span class="title"> KPI 1 </span>
							<div class="progress">
								<div class="progress-bar progress-bar-success"
									style="width: 35%">
									<span class="sr-only">35% Complete (success)</span>
								</div>
								<div class="progress-bar progress-bar-warning"
									style="width: 20%">
									<span class="sr-only">20% Complete (warning)</span>
								</div>
								<div class="progress-bar progress-bar-danger" style="width: 10%">
									<span class="sr-only">10% Complete (danger)</span>
								</div>
							</div> <span class="status">
								<div class="field">
									<span class="badge badge-green">6</span> completed <span
										class="pull-right fa fa-check"></span>
								</div>
								<div class="field">
									<span class="badge badge-orange">3</span> in-progress <span
										class="pull-right fa fa-adjust"></span>
								</div>
								<div class="field">
									<span class="badge badge-red">1</span> pending <span
										class="pull-right fa fa-list-ul"></span>
								</div>
						</span>
					</a></li>
					<li><a href="javascript:void(0);"> <span class="image"> <img
								src="<?php echo base_url();?>img/avatars/avatar1.jpg" alt="" />
						</span> <span class="title"> KPI 2 </span>
							<div class="progress">
								<div class="progress-bar progress-bar-success"
									style="width: 15%">
									<span class="sr-only">35% Complete (success)</span>
								</div>
								<div class="progress-bar progress-bar-warning"
									style="width: 40%">
									<span class="sr-only">20% Complete (warning)</span>
								</div>
								<div class="progress-bar progress-bar-danger" style="width: 20%">
									<span class="sr-only">10% Complete (danger)</span>
								</div>
							</div> <span class="status">
								<div class="field">
									<span class="badge badge-green">2</span> completed <span
										class="pull-right fa fa-check"></span>
								</div>
								<div class="field">
									<span class="badge badge-orange">8</span> in-progress <span
										class="pull-right fa fa-adjust"></span>
								</div>
								<div class="field">
									<span class="badge badge-red">4</span> pending <span
										class="pull-right fa fa-list-ul"></span>
								</div>
						</span>
					</a></li>
					<li><a href="javascript:void(0);"> <span class="image"> <img
								src="<?php echo base_url();?>img/avatars/avatar2.jpg" alt="" />
						</span> <span class="title"> KPI 3 </span>
							<div class="progress">
								<div class="progress-bar progress-bar-success"
									style="width: 65%">
									<span class="sr-only">35% Complete (success)</span>
								</div>
								<div class="progress-bar progress-bar-warning"
									style="width: 10%">
									<span class="sr-only">20% Complete (warning)</span>
								</div>
								<div class="progress-bar progress-bar-danger" style="width: 15%">
									<span class="sr-only">10% Complete (danger)</span>
								</div>
							</div> <span class="status">
								<div class="field">
									<span class="badge badge-green">10</span> completed <span
										class="pull-right fa fa-check"></span>
								</div>
								<div class="field">
									<span class="badge badge-orange">3</span> in-progress <span
										class="pull-right fa fa-adjust"></span>
								</div>
								<div class="field">
									<span class="badge badge-red">4</span> pending <span
										class="pull-right fa fa-list-ul"></span>
								</div>
						</span>
					</a></li>
					<li><a href="javascript:void(0);"> <span class="image"> <img
								src="<?php echo base_url();?>img/avatars/avatar4.jpg" alt="" />
						</span> <span class="title"> Ellie Doe </span>
							<div class="progress">
								<div class="progress-bar progress-bar-success" style="width: 5%">
									<span class="sr-only">35% Complete (success)</span>
								</div>
								<div class="progress-bar progress-bar-warning"
									style="width: 48%">
									<span class="sr-only">20% Complete (warning)</span>
								</div>
								<div class="progress-bar progress-bar-danger" style="width: 27%">
									<span class="sr-only">10% Complete (danger)</span>
								</div>
							</div> <span class="status">
								<div class="field">
									<span class="badge badge-green">1</span> completed <span
										class="pull-right fa fa-check"></span>
								</div>
								<div class="field">
									<span class="badge badge-orange">6</span> in-progress <span
										class="pull-right fa fa-adjust"></span>
								</div>
								<div class="field">
									<span class="badge badge-red">2</span> pending <span
										class="pull-right fa fa-list-ul"></span>
								</div>
						</span>
					</a></li>
					<li><a href="javascript:void(0);"> <span class="image"> <img
								src="<?php echo base_url();?>img/avatars/avatar5.jpg" alt="" />
						</span> <span class="title"> Lisa Doe </span>
							<div class="progress">
								<div class="progress-bar progress-bar-success"
									style="width: 21%">
									<span class="sr-only">35% Complete (success)</span>
								</div>
								<div class="progress-bar progress-bar-warning"
									style="width: 20%">
									<span class="sr-only">20% Complete (warning)</span>
								</div>
								<div class="progress-bar progress-bar-danger" style="width: 40%">
									<span class="sr-only">10% Complete (danger)</span>
								</div>
							</div> <span class="status">
								<div class="field">
									<span class="badge badge-green">4</span> completed <span
										class="pull-right fa fa-check"></span>
								</div>
								<div class="field">
									<span class="badge badge-orange">5</span> in-progress <span
										class="pull-right fa fa-adjust"></span>
								</div>
								<div class="field">
									<span class="badge badge-red">9</span> pending <span
										class="pull-right fa fa-list-ul"></span>
								</div>
						</span>
					</a></li>
					<li><a href="javascript:void(0);"> <span class="image"> <img
								src="<?php echo base_url();?>img/avatars/avatar6.jpg" alt="" />
						</span> <span class="title"> Kelly Doe </span>
							<div class="progress">
								<div class="progress-bar progress-bar-success"
									style="width: 45%">
									<span class="sr-only">35% Complete (success)</span>
								</div>
								<div class="progress-bar progress-bar-warning"
									style="width: 21%">
									<span class="sr-only">20% Complete (warning)</span>
								</div>
								<div class="progress-bar progress-bar-danger" style="width: 10%">
									<span class="sr-only">10% Complete (danger)</span>
								</div>
							</div> <span class="status">
								<div class="field">
									<span class="badge badge-green">6</span> completed <span
										class="pull-right fa fa-check"></span>
								</div>
								<div class="field">
									<span class="badge badge-orange">3</span> in-progress <span
										class="pull-right fa fa-adjust"></span>
								</div>
								<div class="field">
									<span class="badge badge-red">1</span> pending <span
										class="pull-right fa fa-list-ul"></span>
								</div>
						</span>
					</a></li>
					<li><a href="javascript:void(0);"> <span class="image"> <img
								src="<?php echo base_url();?>img/avatars/avatar7.jpg" alt="" />
						</span> <span class="title"> Jessy Doe </span>
							<div class="progress">
								<div class="progress-bar progress-bar-success" style="width: 7%">
									<span class="sr-only">35% Complete (success)</span>
								</div>
								<div class="progress-bar progress-bar-warning"
									style="width: 30%">
									<span class="sr-only">20% Complete (warning)</span>
								</div>
								<div class="progress-bar progress-bar-danger" style="width: 10%">
									<span class="sr-only">10% Complete (danger)</span>
								</div>
							</div> <span class="status">
								<div class="field">
									<span class="badge badge-green">1</span> completed <span
										class="pull-right fa fa-check"></span>
								</div>
								<div class="field">
									<span class="badge badge-orange">6</span> in-progress <span
										class="pull-right fa fa-adjust"></span>
								</div>
								<div class="field">
									<span class="badge badge-red">2</span> pending <span
										class="pull-right fa fa-list-ul"></span>
								</div>
						</span>
					</a></li>
					<li><a href="javascript:void(0);"> <span class="image"> <img
								src="<?php echo base_url();?>img/avatars/avatar8.jpg" alt="" />
						</span> <span class="title"> Debby Doe </span>
							<div class="progress">
								<div class="progress-bar progress-bar-success"
									style="width: 70%">
									<span class="sr-only">35% Complete (success)</span>
								</div>
								<div class="progress-bar progress-bar-warning"
									style="width: 20%">
									<span class="sr-only">20% Complete (warning)</span>
								</div>
								<div class="progress-bar progress-bar-danger" style="width: 5%">
									<span class="sr-only">10% Complete (danger)</span>
								</div>
							</div> <span class="status">
								<div class="field">
									<span class="badge badge-green">13</span> completed <span
										class="pull-right fa fa-check"></span>
								</div>
								<div class="field">
									<span class="badge badge-orange">7</span> in-progress <span
										class="pull-right fa fa-adjust"></span>
								</div>
								<div class="field">
									<span class="badge badge-red">1</span> pending <span
										class="pull-right fa fa-list-ul"></span>
								</div>
						</span>
					</a></li>
				</ul>
			</div>
		</div>
		<!-- /TEAM STATUS -->
	</header>
	<!--/HEADER -->

	<!-- PAGE -->
	<section id="page">
		<!-- SIDEBAR -->
		<div id="sidebar" class="sidebar sidebar-fixed">
			<div class="sidebar-menu nav-collapse">
				<div class="divide-20"></div>
				<!-- SEARCH BAR -->
				<div id="search-bar">
					<input class="search" id="patSearch" type="text" placeholder="Search">
					<i onclick="searchPat()" class="fa fa-search search-icon"></i>
				</div>
				<!-- /SEARCH BAR -->

				<!-- SIDEBAR QUICK-LAUNCH -->
				<!-- <div id="quicklaunch">
						<!-- /SIDEBAR QUICK-LAUNCH -->

				<!-- SIDEBAR MENU -->
				<ul>
					<li
						<?php if ($this->uri->segment(1,0)=='news'){echo 'class="active"';};?>>
						<a href="<?php echo base_url("news/myNews");?>"> <i
							class="fa fa-tachometer fa-fw"></i> <span class="menu-text">Dashboard</span>
							<span class="selected"></span>
					</a>
					</li>
					<li
						class="has-sub <?php if ($this->uri->segment(1,0)=='filingCab'){echo 'active';}?>"><a
						href="javascript:;"> <i class="fa fa-briefcase fa-fw"></i> <span
							class="menu-text">Filing Cabinet</span> <span class="arrow"></span>
					</a>
						<ul class="sub">
							<li
								<?php if ($this->uri->segment(2,0)=='committeeDash'){echo 'class="active"';};?>><a
								href="<?php echo base_url("filingCab/committeeDash/");?>"><span
									class="sub-menu-text">Committees</span></a></li>
							<li
								<?php if ($this->uri->segment(2,0)=='policiesDash'){echo 'class="active"';};?>><a
								href="<?php echo base_url("filingCab/policiesDash/");?>"><span
									class="sub-menu-text">Policies</span></a></li>
							<li
								<?php if ($this->uri->segment(2,0)=='incidentsDash'){echo 'class="active"';};?>><a
								href="<?php echo base_url("filingCab/incidentsDash/");?>"><span
									class="sub-menu-text">Incidents</span></a></li>

							<li
								<?php if ($this->uri->segment(2,0)=='KPIDash'){echo 'class="active"';};?>><a
								href="<?php echo base_url("filingCab/KPIDash/");?>"><span
									class="sub-menu-text">KPI</span></a></li>

							<li
								<?php if ($this->uri->segment(2,0)=='inventoryDash'){echo 'class="active"';};?>><a
								href="<?php echo base_url("filingCab/inventoryDash/");?>"><span
									class="sub-menu-text">Inventory</span></a></li>
						</ul></li>



					<li
						class="has-sub <?php if ($this->uri->segment(1,0)=='compliance'){echo 'active';}?>"><a
						href="javascript:;" class=""> <i class="fa fa-table fa-fw"></i> <span
							class="menu-text">Compliance</span> <span class="arrow"></span>
					</a>
						<ul class="sub">
							<li
								<?php if ($this->uri->segment(2,0)=='homes'){echo 'class="active"';};?>><a
								href="<?php echo base_url("compliance/homes");?>"><span
									class="sub-menu-text">Homes</span></a></li>
							<li
								<?php if ($this->uri->segment(2,0)=='compliancepatients'){echo 'class="active"';};?>><a
								href="<?php echo base_url("compliance/compliancepatients");?>"><span
									class="sub-menu-text">Patients</span></a></li>
							<li
								<?php if ($this->uri->segment(2,0)=='portalUsers'){echo 'class="active"';};?>><a
								href="<?php echo base_url("compliance/portalUsers");?>"><span
									class="sub-menu-text">Home Portal Users</span></a></li>
							<li
								<?php if ($this->uri->segment(2,0)=='ticketsDash'){echo 'class="active"';};?>><a
								href="<?php echo base_url("compliance/ticketsDash");?>"><span
									class="sub-menu-text">Tickets</span></a></li>
							<li
								<?php if ($this->uri->segment(2,0)=='medchanges'){echo 'class="active"';};?>><a
								href="<?php echo base_url("compliance/medchanges");?>"><span
									class="sub-menu-text">Med Changes</span></a></li>
							<li
								<?php if ($this->uri->segment(2,0)=='production'){echo 'class="active"';};?>><a
								href="<?php echo base_url("compliance/production");?>"><span
									class="sub-menu-text">Production</span></a></li>

							<li
								<?php if ($this->uri->segment(2,0)=='delivery'){echo 'class="active"';};?>><a
								href="<?php echo base_url("compliance/delivery");?>"><span
									class="sub-menu-text">Delivery</span></a></li>
						</ul></li>
					<li
						class="has-sub <?php if ($this->uri->segment(1,0)=='setting'){echo 'active';}?>"><a
						href="javascript:;" class=""> <i class="fa fa-gears fa-fw"></i> <span
							class="menu-text">Settings</span> <span class="arrow"></span>
					</a>
						<ul class="sub">
							<li
								<?php if ($this->uri->segment(1,0)=='auth'){echo 'class="active"';};?>><a
								href="<?php echo base_url();?>auth"><span class="sub-menu-text">Users</span></a></li>
							<li
								<?php if ($this->uri->segment(2,0)=='settings'){echo 'class="active"';};?>><a
								href="<?php echo base_url();?>setting/settings"><span
									class="sub-menu-text">Settings</span></a></li>
						</ul></li>
				</ul>
				<!-- /SIDEBAR MENU -->
			</div>
		</div>
		<!-- /SIDEBAR -->
		<div id="main-content">
			<div class="container">
				<div class="row">
					<div id="content" class="col-lg-12">
						<!-- PAGE HEADER-->
						<div class="row">
							<div class="col-sm-12">
								<div class="page-header">