<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left">Production Order Patient</h3>
</div>
<div class="description">

	<a class="btn btn-default btn-lg" type="button"
		href="<?php echo base_url("compliance/archiveproductionrun/");?>">Add
		Error</a> <a class="btn btn-default btn-lg" type="button"
		href="<?php echo base_url("compliance/printorders/");?>"> Package </a>

	<a class="btn btn-default btn-lg" type="button"
		href="<?php echo base_url("compliance/printorders/");?>"> Pharmacist
		Check </a>

</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!--  MAIN PAGE HERE -->


<!--  Detail fields -->
<div class="row">
	<div class="col-lg-12">
		<div class="box border blue">
			<div class="box-title">
				<h4>
					<i class="fa fa-archive"></i>Patient Order
				</h4>
			</div>
			<div class="box-body">
				<table class="table table-hover">
					<thead>
					</thead>
					<tbody>
						<tr>
							<td>
								<p>
									<span class="label label-danger arrow-in"> New <i
										class="fa fa-bookmark"></i></span>

								</p>
								<p>
									<i class="fa fa-truck fa-3x"></i>
								</p>
								<p>2342</p>
							</td>
							<td>
								<h3>Production Description: PRN - A. LIDDIARD - ATIVAN SL 1MG
									Batch Num: 66310</h3>
								<p>Applewood, Account 30277 Start: 4/7/2014 End: 4/14/2014</p>
								<p>
								
								<div class="progress">
									<div class="progress-bar progress-bar-danger"
										role="progressbar" aria-valuenow="25" aria-valuemin="0"
										aria-valuemax="100" style="width: 25%"></div>
								</div>
								</p>
							</td>

						</tr>

					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<!--  /detail fields -->

<!--  ORDERS -->
<div class="row">
	<div class="col-lg-12">
		<div class="box border blue">
			<div class="box-title">
				<h4>
					<i class="fa fa-truck"></i>Errors
				</h4>
			</div>
			<div class="box-body">


				<table class="table table-hover">
					<thead>
						<tr>
							<th>Date</th>
							<th>Drug</th>
							<th>Canister</th>
							<th>Error Type</th>
							<th>Comment</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>

						<tr>
							<td>
								<p>
									<span class="label label-danger arrow-in"> Error Not Fixed <i
										class="fa fa-bookmark"></i></span>
								</p> Mar 13/ 14
							</td>
							<td>Metformin 500mg</td>
							<td>35</td>
							<td>Double Drop</td>
							<td>Found in 5 pouches</td>
							<td><a class="btn btn-default btn-lg" type="button"
								href="<?php echo base_url("compliance/productionorderpatient/");?>">Fixed
							</a></td>
							<td><a class="btn btn-default btn-lg" type="button"
								href="<?php echo base_url("compliance/productionorderpatient/");?>"><i
									class="fa fa-archive"></i> </a></td>
						</tr>

						<tr>
							<td>
								<p>
									<span class="label label-success arrow-in"> Error Fixed <i
										class="fa fa-bookmark"></i></span>
								</p> Mar 13/ 14
							</td>
							<td>Metformin 500mg</td>
							<td>35</td>
							<td>Double Drop</td>
							<td>Found in 5 pouches</td>
							<td><a class="btn btn-default btn-lg" type="button"
								href="<?php echo base_url("compliance/productionorderpatient/");?>">Fixed
							</a></td>
							<td><a class="btn btn-default btn-lg" type="button"
								href="<?php echo base_url("compliance/productionorderpatient/");?>"><i
									class="fa fa-archive"></i> </a></td>
						</tr>
						<tr>
							<td>
								<p>
									<span class="label label-success arrow-in"> Error Fixed <i
										class="fa fa-bookmark"></i></span>
								</p> Mar 13/ 14
							</td>
							<td>Metformin 500mg</td>
							<td>35</td>
							<td>Double Drop</td>
							<td>Found in 5 pouches</td>
							<td>Fixed By: Jane Mar 14/ 14</td>
							<td><a class="btn btn-default btn-lg" type="button"
								href="<?php echo base_url("compliance/productionorderpatient/");?>"><i
									class="fa fa-archive"></i> </a></td>
						</tr>

					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<!--  /ORDER -->


<button class="btn btn-primary" data-toggle="modal"
	data-target="#modalAddError">Large modal</button>

<div id="modalAddError" class="modal fade bs-example-modal-lg"
	tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
	aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Add Error For Order: Mark
					Joe</h4>
			</div>
			<div class="modal-body">
				<p>Date: Mar 30/ 2014</p>
				<p>User: Kelly</p>
				<p>Drug: Metformin 500mg</p>
				<p>Canister</p>
				<p>Error Type</p>
				<p>Comment</p>
				<p>Fixed?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-primary">Add</button>
			</div>

		</div>
	</div>
</div>

<!-- /CALENDAR & CHAT -->
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>
