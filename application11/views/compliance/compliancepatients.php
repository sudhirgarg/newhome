
<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left">Compliance Patients</h3>
</div>
<div class="description">
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!--  MAIN PAGE HERE -->

<div class="row">
	<div class="col-lg-12">
		<div class="box border blue">
			<div class="box-title">
				<h4>
					<i class="fa fa-users"></i>Patients
				</h4>
			</div>
			<div class="box-body">
				<table id="datatable1" cellpadding="0" cellspacing="0" border="0"
					class="datatable table table-striped table-bordered table-hover table-responsive">
					<thead>
						<tr>
							<th>Client</th>
							<th>Home</th>
							<th>Tickets</th>
							<th>Med Changes</th>
							<th>Billing</th>
						</tr>
					</thead>
					<tbody>
					<?php
					$patients = $this->comp->getCompliancePatients ();
					if ($patients != false) { // Often times I'll return false in a model if no results are found
						foreach ( $patients->result () as $patient ) {
$patHomeID = $this->comp->getPatHomes ( $patient->patID, '0' );
							?>
						<tr
							onclick="window.location='<?php echo base_url("compliance/viewPat/". $patient->patID . "/profile");?>'">
							<td>
								<h4><?php echo $patient->patInitials . $patient->patID ;?></h4>
							</td>
							<td><?php if ($patHomeID>0){$patHome = $this->comp->getHome($patHomeID); echo $patHome->homeName;};?></td>
							<td><a class="btn btn-lg btn-primary"
								href="<?php echo base_url("compliance/viewPat/". $patient->patID . "/tickets");?>"><?php echo $this->comp->getTicketCount($patient->patID);?>
									Tickets</a></td>
							<td><a class="btn btn-lg btn-warning"
								href="<?php echo base_url("compliance/viewPat/". $patient->patID . "/medchanges");?>"><?php echo $this->comp->getMedChangeCount($patient->patID);?>
									Med Changes</a></td>
							<td><a class="btn btn-lg btn-danger"
								href="<?php echo base_url("compliance/viewPat/". $patient->patID . "/billing");?>">
									Billing</a></td>
						</tr>
<?php }} else { ?><tr>
							<td>No Records</td>
						</tr><?php  }?>
					</tbody>
				</table>

			</div>
			
			<div><h1>.</h1></div>
		</div>
	</div>
</div>

<!-- / MAIN PAGE HERE -->
</div>
<!-- /CALENDAR & CHAT -->
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>

