<?php
$homeID = $this->uri->segment ( 3 );
?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"
		aria-hidden="true">&times;</button>
	<h4 class="modal-title">Edit Client</h4>
</div>
<div class="modal-body">
	<div class="form-horizontal" role="form">
		<input id="homeID" type="hidden" value="<?php echo $homeID;?>">
		<div class="form-group">
			<label class="col-sm-3 control-label">User Email Address</label>
			<div class="col-sm-9">

				<select class="form-control" id="email">
					<?php
					$portalUsers = $this->ion_auth->users ( '18' );
					
					if($portalUsers != false) { 
						foreach ( $portalUsers->result () as $portalUser ) 
						{ 								
						?>
                        <option
						value="<?php echo $portalUser->email;?>"> 
						<?php echo $portalUser->first_name . ' ' . $portalUser->last_name; ?> </option>   
                        <?php
					}}else
					{
						echo '<option value="null">No users available</option>';
					}
					?>
				</select> 								
			
			</div>
		</div>

	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal"
			id="closeButtonModal">Close</button>
		<button type="button" class="btn btn-primary" id="addUserButton"
			onclick="addUserToHome()" data-loading-text="Saving...">Save changes</button>
	</div>
</div>