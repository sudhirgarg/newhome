

<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<!--<div class="clearfix">
	<h3 class="content-title pull-left">Error summary </h3>
</div>

<div class="description">
	<a href="<?php echo base_url("compliance/errorReport/") ;?>">
		<button class="btn btn-primary btn-lg" type="button">
			<i class="fa fa-pencil"></i> Add Error Details
	    </button>
   </a>
	
		
</div>
</div>
</div>
</div>


<div id="main1" class="row" ng-app="AngularChartExample" >

	<div class="col-lg-10">
		<div class="box border blue">
			<div class="box-title">
				<h4>
					<i class="fa fa-exclamation-triangle"></i>Error report chart 
				</h4>
			</div>
			<div class="box-body">
				
				<div ng-view></div>
				
			</div>
		</div>
		<br/><br/>
	</div>
	
	<div class="col-lg-10">
		<div class="box border blue">
			<div class="box-title">
				<h4>
					<i class="fa fa-bullhorn" aria-hidden="true"></i> Details of Error 
				</h4>
			</div>
			<div class="box-body">
				
				  <table class="table table-hover">
					  <thead>
					      <tr>
					        <th>#</th>
					        <th>Department    </th>
					        <th>Type of error </th>
					        <th>Description   </th>
					        <th>Resolution    </th>
					        <th>Source of error</th>
					      </tr>
					  </thead>					    
					  <tbody>
					    <?php 
					        $i = 1;
					        foreach($info AS $record){
					        	echo '
									 <tr>
								         <td>'.$i++.'</td>
								         <td>'.$record->depName.'</td>
								         <td>'.$record->name.'</td>
			                             <td>'.$record->description.'</td>
				                         <td>'.$record->resolution.'</td>
					                     <td>'.$record->sourceOferror.'</td>
								     </tr>
									';
					        }					    
					    ?>     
					    
					  </tbody>
				  </table>				
				
			</div>
		</div>
		<br/><br/>
	</div>
	
</div>

<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>-->













<?php $ci = &get_instance();
$index='index.php/';	
?>

<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN BREADCRUMBS -->
        <div class="breadcrumbs">
            <h1>Error summary</h1>
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?><?php $index; ?>compliance/ticketsdash">Home</a></li>
                <li class="active">Error summary</li>
            </ol>
        </div>
        <!-- END BREADCRUMBS -->
        <!-- BEGIN SIDEBAR CONTENT LAYOUT -->
        <div class="page-content-container">
            <div class="page-content-row">
                <div class="page-content-col">
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="fa fa-exclamation-triangle font-dark"></i>
                                        <span class="caption-subject bold uppercase">Error report chart</span>
					<span class="add_error_detail"><a href="<?php echo base_url('index.php/compliance/errorReport/') ;?>">Add Error Details</a></span>
                                    </div>
                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body" id="main1" ng-app="AngularChartExample">
                                    <div ng-view></div>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="fa fa-paperclip font-dark"></i>
                                <span class="caption-subject bold uppercase">Details of Error</span>
                            </div>
                            <div class="tools"> </div>
                        </div>
                        <div class="portlet-body">
                            <table class="table table-striped table-bordered table-hover" id="sample_2">
                                <thead>
                                    <tr>
                                        <th> Department </th>
                                        <th> Type of Error </th>
                                        <th> Description </th>
                                        <th> Resolution </th>
                                        <th> Source of Error </th>	
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($info as $record){ ?>
                                    <tr>
                                        <td><?php echo ucfirst($record->depName); ?></td>
                                        <td><?php echo $record->name; ?></td>
                                        <td><?php echo $record->description; ?></td>
                                        <td><?php echo $record->resolution; ?></td>
                                        <td><?php echo $record->sourceOferror; ?></td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
        </div>
        <!-- END SIDEBAR CONTENT LAYOUT -->
    </div>   
