<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left">Dashboard</h3>
</div>
<div class="description">

</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!--  MAIN PAGE HERE -->

<div class="row">
	<div class="col-lg-12">
		<div class="box border blue">
			<div class="box-title"> 
				<h4>
					<i class="fa fa-truck"></i> Notifications 
				</h4>
			</div>
			<div class="box-body">
			<ul>
				<?php

				$notifications = $this->comp->getUserNotifications ();
			
						if ($notifications) {
							foreach ( $notifications->result () as $notification ) {
								if(isset($notification->typeID))
								if ($notification->typeID == 0) { // message
									?>
						<li ><a href="#"> <span class="label label-primary"><i
									class="fa fa-comment"></i></span> <span class="body"> <span
									class="message"><?php echo $notification->description; ?></span> <span
									class="time"> <i class="fa fa-clock-o"></i> <span><?php 
									$time_ago =strtotime($notification->timeStamp);
									echo $this->quick->timeAgo($time_ago);?></span>
								</span>
							</span>
						</a></li>
						<?php
								} elseif ($notification->typeID == 1) { // general
									?>
						<li><a href="#"> <span class="label label-info"><i
									class="fa fa-ticket"></i></span> <span class="body"> <span
									class="message"><?php echo $notification->description; ?></span> <span class="time">
										<i class="fa fa-clock-o"></i> <span><?php echo $notification->timeStamp;?></span>
								</span>
							</span>
						</a></li> 
						<?php
								} elseif ($notification->typeID == 2) { // Reorder
									?>
						<li><a href="#"> <span class="label label-warning "><i
									class="fa fa-ticket"></i></span> <span class="body"> <span
									class="message"><?php echo $notification->description; ?></span> <span class="time">
										<i class="fa fa-clock-o"></i> <span><?php echo $notification->timeStamp;?></span>
								</span>
							</span>
						</a></li>
						<?php
								} elseif ($notification->typeID == 3) { // Pharmacist
									?>
						<li><a href="#"> <span class="label label-primary "><i
									class="fa fa-ticket"></i></span> <span class="body"> <span
									class="message"><?php echo $notification->description; ?></span> <span class="time">
										<i class="fa fa-clock-o"></i> <span><?php echo $notification->timeStamp;?></span>
								</span>
							</span>
						</a></li>
						
						<?php
								}}}
						else{ ?>
						 <li class="footer"><a href="#">No new notifications</a></li>
						<?php }
						?>

					</ul>
			</div>
		</div>
	</div>
</div>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>