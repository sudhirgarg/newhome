<?php $ci = &get_instance();
$index='index.php/';	
?>
<!-- STYLER -->


<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<!--<div class="clearfix">
	<h3 class="content-title pull-left"> Survey </h3>
</div>
<div class="description">

	
</div>
</div>
</div>
</div>


<div class="row" >
	<div class="col-lg-6">
		<div class="box border blue">
			<div class="box-title">
				<h4>
					<i class="fa fa-home"></i>Survey 
				</h4>
			</div>
			<div class="box-body">
			
			     <form action="<?php echo base_url();?><?php echo $index; ?>compliance/surveyQuestion" method="POST">
					
					  <div class="form-group">
					    <label for="homeName">Select home</label>					    
					     <select data-placeholder="Choose home..." name="nhId" id="nhId" class="chosen-select form-control" style="width:280px;" tabindex="2">
							<option value=""></option>
							<?php 
							   	    
								foreach($homeList AS $comboRecord){
									echo '<option value="'.$comboRecord->ID.'">'.$comboRecord->LastName.' '.$comboRecord->FirstName.' </option>';
								}
								
								
							?>										
						</select>
					  </div>
					
					  <fieldset class="form-group">
					    <legend> Which of following best describe your role? </legend>
					    
					    <div class="form-check">
					       <label class="form-check-label">
					        <input type="radio" class="form-check-input" name="roleName" id="roleName" value="Administrator"> Administrator
					       </label>
					    </div>		    
					   
					    <div class="form-check">
					       <label class="form-check-label">
					        <input type="radio" class="form-check-input" name="roleName" id="roleName" value="Caregiver" checked > Caregiver
					       </label>
					    </div>
					    
					    <div class="form-check disabled">
					        <label class="form-check-label">
					        <input type="radio" class="form-check-input" name="roleName" id="roleName" value="Case Manager Nurse" > Case Manager Nurse
					        </label>
					    </div>
					    
					    <div class="form-check">
					        <label class="form-check-label">
						        <input type="radio" class="form-check-input" name="roleName" id="roleName" value="Patient" > Patient
					        </label>
					    </div>
					    
					     <div class="form-check">
					        <label class="form-check-label">
						        <input type="radio" class="form-check-input" name="roleName" id="roleName" value="Physician Executive" > Physician Executive
					        </label>
					    </div>
					    
					    
					    <div class="form-check disabled">
					        <label class="form-check-label">
					        <input type="radio" class="form-check-input" name="roleName" id="roleName" value="Other" > Other :
					        <input type="text" name="otherRole" >
					        </label>
					    </div>
					    
					  </fieldset>
					  
					  					 
					  <button type="submit" class="btn btn-primary">Continue </button>
					  
					</form>
				   
			</div>
		</div>
	</div>	
	
	<div class="col-lg-6">
		<div class="box border blue">
			<div class="box-title">
				<h4>
					<i class="fa fa-home"></i>Survey List
				</h4>
			</div>
			<div class="box-body">
	              
	                <table class="table table-hover">
					    <thead>
					      <tr>
					        <th>#</th>
					        <th>Home name</th>
					        <th>Roles</th>
					        <th>Satisfaction</th>					        
					        <th>view</th>
					      </tr>
					    </thead>
					    <tbody>
					     <?php 
					     $i = 1;
					         foreach($survey AS $record){
					         	 echo '<tr>
							        <td>'.$i++.'</td>
							        <td>'.$record->LastName.' '.$record->FirstName.'</td>
							        <td>'.$record->roles.'</td>
		                            <td>';
					         	      $ci -> getSurveyInPersentage($record -> id);
		                          echo'% </td><td>		                               
										<a href="'.base_url().''.$index.'compliance/viewSurvey/'.$record->id.'">
							                <button type="button" class="btn btn-default btn-sm">
											    <span class="glyphicon glyphicon-eye-open"></span>
							                </button>
							            </a>				           
							
				                     </td>
							      </tr>';
					         }
					         
					     ?>
					      
					      					      
					    </tbody>
				  </table>
	        </div>
		</div>
	</div>	
	
</div>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>






-->









<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN BREADCRUMBS -->
        <div class="breadcrumbs">
            <h1>Survey</h1>
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?><?php echo $index; ?>compliance/ticketsdash">Home</a></li>
                <li class="active">Survey</li>
            </ol>
            <!-- Sidebar Toggle Button -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".page-sidebar">
                <span class="sr-only">Toggle navigation</span>
                <span class="toggle-icon">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </span>
            </button>
            <!-- Sidebar Toggle Button -->
        </div>
        <!-- END BREADCRUMBS -->
        <!-- BEGIN SIDEBAR CONTENT LAYOUT -->
        <div class="page-content-container">
            <div class="page-content-row">
                <div class="page-content-col">
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_0">
                                        <div class="portlet box blue-hoki">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-home"></i>Survey
                                                </div>
                                            </div>
                                            <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                                <form action="<?php echo base_url();?><?php echo $index; ?>compliance/surveyQuestion" method="POST" class="form-horizontal">
                                                    <div class="form-body">
                                                        <div class="form-group">
                                                            <label class="col-md-4 control-label">Select Home</label>
                                                            <div class="col-md-3">
                                                                <select name="nhId" id="nhId" class="form-control select2" style="width:280px;" tabindex="2">
							            <option value="">Choose Home...</option>
                                                                    <?php 
                                                                    foreach($homeList AS $comboRecord){
                                                                        echo '<option value="'.$comboRecord->ID.'">'.$comboRecord->LastName.' '.$comboRecord->FirstName.' </option>';
                                                                    } ?>	
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-4 control-label">Which of following best describe your role?</label>
                                                            <div class="col-md-6">
                                                                <div class="radio-list">
                                                                    <label>
                                                                        <input type="radio" name="roleName" id="roleName" value="Administrator"> Administrator </label>
                                                                    <label><input type="radio" name="roleName" id="roleName" value="Caregiver" checked > Caregiver </label>
                                                                    <label><input type="radio" name="roleName" id="roleName" value="Case Manager Nurse" > Case Manager Nurse </label>
                                                                    <label><input type="radio" class="form-check-input" name="roleName" id="roleName" value="Patient" > Patient </label>
                                                                    <label><input type="radio" class="form-check-input" name="roleName" id="roleName" value="Physician Executive" > Physician Executive </label>
                                                                    <label><input type="radio" class="form-check-input" name="roleName" id="roleName" value="Other" > Other :
                                                                    <input type="text" placeholder="Other..." name="otherRole" class="form-control">
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-offset-4 col-md-9">
                                                                <button type="submit" class="btn green">Continue</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                                <!-- END FORM-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="fa fa-info-circle font-dark"></i>
                                        <span class="caption-subject bold uppercase">Information</span>
                                    </div>
                                    <div class="tools"> </div>
                            </div>
                            <div class="portlet-body">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                            </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                </div>
                <!-- END PAGE BASE CONTENT -->
            </div>
        </div>
        <!-- Survey Listing Div -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="fa fa-paperclip font-dark"></i>
                            <span class="caption-subject bold uppercase">Survey List</span>
                        </div>
                        <div class="tools"> </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                            <thead>
                                <tr>
                                    <th> Home Name </th>
                                    <th> Roles </th>
                                    <th> Satisfaction </th>
                                    <th> View </th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $i = 1;
                            foreach($survey AS $record){ ?>
                                <tr>
                                    <td> <?php echo ucfirst($record->LastName).' '.ucfirst($record->FirstName) ?> </td>
                                    <td> <?php echo $record->roles ?> </td>
                                    <td> <?php echo $ci -> getSurveyInPersentage($record -> id).'%'; ?> </td>
                                    <td> <a href="<?php echo base_url() ?><?php echo $index; ?>compliance/viewSurvey/<?php echo $record->id; ?>">
                                        <button type="button" class="btn btn-default btn-sm">
                                            <span class="glyphicon glyphicon-eye-open"></span>
                                        </button>
                                        </a>
                                    </td>
                                </tr>
                            <?php $i++; } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END SIDEBAR CONTENT LAYOUT -->
</div>       
