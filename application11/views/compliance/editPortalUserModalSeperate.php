<?php
$isNew = true;
$userID = $this->uri->segment ( 4 );
$groups = $this->uri->segment ( 3 );
$user;
if ($userID > 0) {
	$isNew = false;
	$user = $this->ion_auth->user ($userID)->row ();
}
$index='index.php/';
?>
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN BREADCRUMBS -->
        <div class="breadcrumbs">
            <h1><?php if($isNew==false){
		if($groups == 18 ) { echo 'Edit Home User Detail ';}
		if($groups == 17 ) { echo 'Edit Pharmacy User Detail ';}	
	}else{ echo 'Create New Home Portal User'; }?></h1><a href="<?php echo base_url() ?><?php echo $index; ?>compliance/portalUsers"><button class="btn btn-square btn-sm green todo-bold">Home Portal Users</button></a>
            <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li class="active">User Information</li>
            </ol>
            <!-- Sidebar Toggle Button -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".page-sidebar">
                <span class="sr-only">Toggle navigation</span>
                <span class="toggle-icon">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </span>
            </button>
            <!-- Sidebar Toggle Button -->
        </div>
        <!-- END BREADCRUMBS -->
        <!-- BEGIN SIDEBAR CONTENT LAYOUT -->
        <div class="page-content-container">
            <div class="page-content-row">
                <div class="page-content-col">
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption"><i class="fa fa-info"></i>User Information</div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"> </a>
                                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="tabbable-custom ">
                                        <ul class="nav nav-tabs ">
                                            <li class="active"><a href="#tab_5_1" data-toggle="tab"> Edit Home Portal User </a></li>
                                            <li><a href="#tab_5_2" data-toggle="tab"> Reset Password </a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <!-- START UPDATE HOME PORTAL USER DETAIL DIV -->
                                            <div class="tab-pane active" id="tab_5_1">
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form action="<?php echo base_url() ?>index.php/compliance/editPortalUserModalBySeperatePageRun" class="form-horizontal form-row-seperated">
                                                        <input id="id" name='id' type="hidden" value="<?php echo $userID; ?>">
		                                        <input id="groups" name='groups' type="hidden" value="<?php echo $groups; ?>">
                                                        <div class="form-body">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">First Name</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" id="first_name" name='first_name' placeholder="First Name" value="<?php if($isNew==false){ echo $user->first_name; }else{ echo ''; } ?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Last Name</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" id="last_name" name='last_name' placeholder="Last Name" value="<?php if($isNew==false){ echo $user->last_name; }else{ echo ''; } ?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Organisation</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" id="company" name='company' placeholder="Organisation" value="<?php if($isNew==false){ echo $user->company; }else{ echo ''; } ?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Log In Email</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" id="email" name='email' placeholder="Log In Email" value="<?php if($isNew==false){ echo $user->email; }else{ echo ''; } ?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Username</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" id="username" name='username' placeholder="Username" value="<?php if($isNew==false){ echo $user->username; }else{ echo ''; } ?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Password</label>
                                                                <div class="col-md-9">
                                                                    <input type="password" class="form-control" id="password" name='password' placeholder="Text input" value="<?php if($isNew==false){ echo $user->password; }else{ echo ''; } ?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Nursing Homes ( Agency )</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control" id="NHID" name='NHID'>
                                                                        <option value="0">Null</option>
                                                                        <?php foreach($homeList AS $record){ ?>
                                                                        <option value="<?php echo $record->ID; ?>" <?php if($user->NHID == $record->ID ) { echo 'selected'; } ?>><?php echo $record->Name; ?></option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Phone</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" id="phone" name='phone' placeholder="Text input" value="<?php if($isNew==false){ echo $user->phone; }else{ echo ''; } ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-offset-3 col-md-9">
                                                                    <button type="submit" class="btn green" value="Save"><i class="fa fa-pencil"></i> Update</button>
                                                                    <a href="<?php echo base_url().'compliance/portalUsers/' ?>"><button type="button" class="btn default">Cancel</button></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>
                                            <!-- END UPDATE HOME PORTAL USER DETAIL DIV -->
                                            <!-- START REEST PASSWORD DIV -->
                                            <div class="tab-pane" id="tab_5_2">
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form action="#" class="form-horizontal form-row-seperated">
                                                        <div class="form-body">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">New Password</label>
                                                                <div class="col-md-9">
                                                                    <input type="password" class="form-control" id="reSetPassword" placeholder="Text input" value="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Confirm Password</label>
                                                                <div class="col-md-9">
                                                                    <input type="password" class="form-control" id="reSetPasswordCon" placeholder="Text input" value="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-offset-3 col-md-9">
                                                                    <button type="button" class="btn btn-success" onclick="reSetPAW()" >RESET NEW PASSWORD</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>
                                            <!-- END REEST PASSWORD DIV -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
            </div>
        </div>
        <!-- END SIDEBAR CONTENT LAYOUT -->
    </div>
