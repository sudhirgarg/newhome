<?php
		$production = $this -> pharm -> getProduction();
?>


<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left">Production</h3>
</div>
<div class="description">
	
	<a class="btn btn-lg btn-warning" href="<?php echo base_url() . 'compliance/archivedTickets'; ?>">
		Cancelled Orders
	</a>
	
	<a class="btn btn-lg btn-success" href="<?php echo base_url() . 'compliance/archivedTickets'; ?>">
		Complete Orders
	</a>
	
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!--  MAIN PAGE HERE -->
<div class="row">
	<div class="col-lg-12">
		<div class="box border blue">
			<div class="box-title">
				<h4><i class="fa fa-truck"></i>Production </h4>
			</div>
			<div class="box-body">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>Order Name</th>
							<th>Order Info</th>				
							<th>Dates</th>
							<th>Status</th>
							<th>Trouble</th>
							<th>Urgent</th>
							<th>View</th>
						</tr>
					</thead>
					<tbody>
						<?php
							if ($production) {
								foreach ( $production->result () as $productionOrder ) {
								?>
								<tr>
									
											
									<td>										
										<?php 
										 	if ( $productionOrder -> OrderType != ''){
												echo $productionOrder -> OrderType . '<br>' . $productionOrder -> OrderName; 
											}  
											else
												{
													echo '<h4><span class="label label-warning">New</span></h4>';
												}
										?>
									</td> 
									
									
									<td>
										<p>
											Batch <?php echo $productionOrder -> BatchNum; 
											?>
											| <i class="fa fa-truck fa-2x"></i> <?php if ($productionOrder -> ScheduledDeliveryDate != '') {echo $productionOrder -> ScheduledDeliveryDate;}
											 else{
											 	echo ' not scheduled';} ?>
										</p>
										<p>
										<div class="progress progress-striped active">
											<div class="progress-bar progress-bar-success"
											role="progressbar" aria-valuenow="100" aria-valuemin="0"
											aria-valuemax="0" style="width: 0%"></div>
										</div>
										</p>
									</td>
							
									
									<td>  
										<p><?php echo '<h3><span class="label label-info">' .substr($productionOrder -> StartDate, 0,10).'</span>' .  ' to ' . '<span class="label label-info">' . substr($productionOrder->EndDate, 0,10) . '</span></h3>'; ?></p>
										
									</td>
									
									<td>
										<?php 
											if ($productionOrder->ProductionStartedBy !=''){
											 	echo $this->ion_auth->user($productionOrder -> ProductionStartedBy)-row()->first_name;
											 }	
											echo 'Created: ' . substr($productionOrder ->DateCreated, 0 ,19) . '<br>';			
											if ($productionOrder -> ProductionStartDate != ''){
												echo $this->ion_auth->user($productionOrder -> ProductionStartedBy)-row()->first_name . 'Started:' . $productionOrder -> ProductionStartDate;
											}
											else {
												echo 'Started: NA';
											}
									 	 ?>   
								 
									 </td>
									 <td></td>
									 <td></td>
									 <!--
									 <td>
									 	<a class="btn btn-lg btn-warning" type="button"
										href="<?php echo base_url("compliance/productiondetail/"); ?>"> 
											<i class="fa fa-exclamation-triangle"></i>											
										</a>											
									 </td>
								
									<td><a class="btn btn-lg btn-danger" type="button"
									href="<?php echo base_url("compliance/productiondetail/"); ?>"> <i
									class="fa fa-exclamation"></i></a></td>
									 -->
									 <td>
									 	<a class="btn btn-default btn-lg" type="button"
										href="<?php echo base_url("compliance/productiondetail") . '/' . $productionOrder -> BatchNum; ?>"> <i
										class="fa fa-eye"></i></a>
									 </td>
								
								</tr>
								
						<?php }}else{ ?> <tr>
							<td>No data</td>
						</tr> <?php } ?>
						
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog"
aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
				aria-hidden="true">
					&times;
				</button>
				<h4 class="modal-title" id="myModalLabel">Create New Production</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" role="form">
					<div class="form-group">
						<label class="col-sm-3 control-label">Batch Number</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" placeholder="Text input">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Production Type</label>
						<div class="col-sm-9">
							<select class="form-control">
								<option>Operations</option>
								<option>Med Change</option>
								<option>Reorder</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Production Name</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" placeholder="Text input">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label">Delivery Date</label>
						<div class="col-sm-9">
							<input type="text" name="regular"
							class="form-control datepicker-fullscreen">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Week</label>
						<div class="col-sm-9">
							<select class="form-control">
								<option>1</option>
								<option>2</option>
								<option>3</option>
								<option>4</option>
								<option>5</option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label"></label>
						<div class="col-sm-9">
							<a class="btn btn-primary btn-block">Save</a>
						</div>
					</div>

				</form>
			</div>

		</div>
	</div>
</div>

<!-- /CALENDAR & CHAT -->
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top </span>
</div>

