<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left">Production Order Detail</h3>
</div>
<div class="description">
	<a class="btn btn-default btn-lg" type="button"
		href="<?php echo base_url("compliance/archiveproductionrun/");?>">
		Archive </a> <a class="btn btn-default btn-lg" type="button"
		href="<?php echo base_url("compliance/printorders/");?>"> Print
		Packing Sheet </a>
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!--  MAIN PAGE HERE -->


<!--  Detail fields -->
<div class="row">
	<div class="col-lg-12">
		<div class="box border blue">
			<div class="box-title">
				<h4>
					<i class="fa fa-archive"></i>Order Detail
				</h4>
			</div>
			<div class="box-body">
				<table class="table table-hover">
					<thead>
					</thead>
					<tbody>
						<tr>
							<td>
								<p>
									<span class="label label-danger arrow-in"> New <i
										class="fa fa-bookmark"></i></span>

								</p>
								<p>
									<i class="fa fa-truck fa-3x"></i>
								</p>
								<p>2342</p>
							</td>
							<td>
								<h3>Production Description: PRN - A. LIDDIARD - ATIVAN SL 1MG
									Batch Num: 66310</h3>
								<p>Applewood, Account 30277 Start: 4/7/2014 End: 4/14/2014</p>
								<p>
								
								<div class="progress">
									<div class="progress-bar progress-bar-danger"
										role="progressbar" aria-valuenow="25" aria-valuemin="0"
										aria-valuemax="100" style="width: 25%"></div>
								</div>
								</p>
							</td>

						</tr>

					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<!--  /detail fields -->

<!--  ORDERS -->
<div class="row">
	<div class="col-lg-12">
		<div class="box border blue">
			<div class="box-title">
				<h4>
					<i class="fa fa-truck"></i>Patients
				</h4>
			</div>
			<div class="box-body">


				<table class="table table-hover">
					<thead>
						<tr>
							<th>Patient</th>
							<th>Progress</th>
							<th>Dates</th>
							<th>Alerts</th>
							<th>Work Flow</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>

						<tr>
							<td><p>
									<span class="label label-success arrow-in"> Awaiting Packaging
										<i class="fa fa-bookmark"></i>
									</span>
								</p>
								<p>John Doe</p></td>

							<td>
								<p>Applewood, Account 30277</p>
								<p>
								
								<div class="progress progress-striped active">
									<div class="progress-bar progress-bar-success"
										role="progressbar" aria-valuenow="100" aria-valuemin="0"
										aria-valuemax="50" style="width: 50%"></div>
								</div>
								</p>
							</td>

							<td>Start: 4/7/2014 End: 4/14/2014</td>
							<td><div>
									<i class="fa fa-exclamation-triangle fa-2x"></i> 3 errors
								</div>
								<div>
									<i class="fa fa-exclamation-circle fa-2x"></i> 1 Med Change
								</div></td>
							<td><p>Produced: Sarah Mar 14th</p>
								<p>Packaged: Kelly Mar 14th</p>
								<p>Pharmacist Check: Kim Mar 14th</p></td>
							<td><a class="btn btn-default btn-lg" type="button"
								href="<?php echo base_url("compliance/productionorderpatient/");?>">
									<i class="fa fa-eye"></i>
							</a></td>


						</tr>

						<tr>
							<td><p>
									<span class="label label-success arrow-in"> Awaiting Packaging
										<i class="fa fa-bookmark"></i>
									</span>
								</p>
								<p>John Doe</p></td>

							<td>
								<p>Applewood, Account 30277</p>
								<p>
								
								<div class="progress progress-striped active">
									<div class="progress-bar progress-bar-success"
										role="progressbar" aria-valuenow="100" aria-valuemin="0"
										aria-valuemax="50" style="width: 50%"></div>
								</div>
								</p>
							</td>

							<td>Start: 4/7/2014 End: 4/14/2014</td>
							<td><div>
									<i class="fa fa-exclamation-triangle fa-2x"></i> 3 errors
								</div>
								<div>
									<i class="fa fa-exclamation-circle fa-2x"></i> 1 Med Change
								</div></td>
							<td><p>Produced: Sarah Mar 14th</p>
								<p>Packaged: Kelly Mar 14th</p>
								<p>Pharmacist Check: Kim Mar 14th</p></td>
							<td><a class="btn btn-default btn-lg" type="button"
								href="<?php echo base_url("compliance/productionorderpatient/");?>">
									<i class="fa fa-eye"></i>
							</a></td>


						</tr>
						<tr>
							<td><p>
									<span class="label label-success arrow-in"> Awaiting Packaging
										<i class="fa fa-bookmark"></i>
									</span>
								</p>
								<p>John Doe</p></td>

							<td>
								<p>Applewood, Account 30277</p>
								<p>
								
								<div class="progress progress-striped active">
									<div class="progress-bar progress-bar-success"
										role="progressbar" aria-valuenow="100" aria-valuemin="0"
										aria-valuemax="50" style="width: 50%"></div>
								</div>
								</p>
							</td>

							<td>Start: 4/7/2014 End: 4/14/2014</td>
							<td><div>
									<i class="fa fa-exclamation-triangle fa-2x"></i> 3 errors
								</div>
								<div>
									<i class="fa fa-exclamation-circle fa-2x"></i> 1 Med Change
								</div></td>
							<td><p>Produced: Sarah Mar 14th</p>
								<p>Packaged: Kelly Mar 14th</p>
								<p>Pharmacist Check: Kim Mar 14th</p></td>
							<td><a class="btn btn-default btn-lg" type="button"
								href="<?php echo base_url("compliance/productionorderpatient/");?>">
									<i class="fa fa-eye"></i>
							</a></td>


						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<!--  /ORDER -->

<!-- /CALENDAR & CHAT -->
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>
