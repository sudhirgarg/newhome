<?php
$DIN = $this->uri->segment ( 3 );
?>

<style>
.modal-dialog {
	width: 60%;
}
</style>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"
		aria-hidden="true">&times;</button>
	<h4 class="modal-title">Medication Coverage Info retrieved from healthinfo.moh.gov.on.ca</h4>	
</div>
<div class="modal-body">

<div>
		<object type="text/html" data="https://www.healthinfo.moh.gov.on.ca/formulary/" 
		 width="100%" height="600px" >
   		</object>
	</div>	

<!-- 
	<div style="display: none;">
		<object type="text/html" data="https://www.healthinfo.moh.gov.on.ca/formulary/index.jsp" 
		 width="100%" height="600px" >
   		</object>
	</div>	

	<div>
		<object type="text/html" data="https://www.healthinfo.moh.gov.on.ca/formulary/SearchServlet?searchType=singleQuery&phrase=exact&keywords=<?php echo $DIN;?>" 
		 width="100%" height="600px" >
   		</object>
	</div>	
 -->
	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal"
			id="closeButtonModal">Close</button>
	</div>
</div>

