<?php
$invoiceNum = $this->uri->segment ( 3 );
?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"
		aria-hidden="true">&times;</button>
	<h4 class="modal-title">Invoice: <?php echo $invoiceNum; ?></h4>
</div>
<div class="modal-body">
	<table cellpadding="0" cellspacing="0" border="0"
		class="table table-bordered table-hover">
		<thead>
			<tr>
				<th>Date</th>
				<th>Rx Num</th>
				<th>Comment</th>
				<th>Amount</th>
			</tr>
		</thead>
		<tbody>
												<?php
												$invoiceDetail = $this->pharm->getPatARInvoiceDetails ( $invoiceNum );
												//$invoiceDetail =FALSE;
												if ($invoiceDetail != FALSE) {
													foreach ( $invoiceDetail->result () as $invoiceItem ) {
														?> 
														<tr>
				<td><?php echo $invoiceItem->Date;?></td>
				<td><?php echo $invoiceItem->RxNum;?></td>
				<td><?php echo $invoiceItem->Comment;?></td>
				<td><?php echo $invoiceItem->Amount;?></td>
			</tr>
												<?php
													}
												}else {
echo '<tr><td colspan="4"><div class="center">FEATURE COMING SOON</div></td></tr>';}
												?>
												</tbody>
	</table>
	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal"
			id="closeButtonModal">Close</button>
	</div>
</div>