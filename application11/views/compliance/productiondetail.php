<?php
$batchNum = $this -> uri -> segment(3);
$production = $this -> pharm -> getProductionDetail($batchNum);
$progress = 0;
?>

<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left">Production Run Detail</h3>
</div>
<div class="description">

</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->

<script>
	$(".datepicker-fullscreen").pickadate();
	$(".timepicker-fullscreen").pickatime(); 
</script>

<div class="row">
	<div class="col-lg-12">
		<div class="box border blue">
			<div class="box-title">
				<h4><i class="fa fa-truck"></i>Status </h4>
			</div>
			<div class="box-body">

				<table class="table table-hover">
					<thead>
						<tr>
							<th>Started</th>
							<th>Packaged</th>
							<th>Tech Check</th>
							<th>Pharmacist Check</th>
						</tr>
						<tr>
							<td>
							<?php
								if($production->ProductionStartDate=="")
								{
									
									?>
									<button class="btn btn-default btn-block" type="button">Start</button> <?php 
								}else{
									echo $production->ProductionStartedBy . " Started: " . $production->ProductionStartDate;
									$progress = 25;
								} ?> </a></td>

							<td>
							<?php
								if($production->PackagedDate=="")
								{
									
									?>
									<button class="btn btn-default btn-block" type="button">Package</button> <?php 
								}else{
									echo $production->PackagedBy . " Started: " . $production->PackagedDate;
									$progress = 50;
								} ?> </a></td>
								
								
								<td>
							<?php
								if($production->TechDate=="")
								{
									
									?>
									<button class="btn btn-default btn-block" type="button">Pharmacist</button> <?php 
								}else{
									echo $production->PharmacistBy . " Started: " . $production->PharmacistDate;
									$progress = 75;
								} ?> </a></td>
								
								
															<td>
							<?php
								if($production->PharmacistDate=="")
								{
									
									?>
									<button class="btn btn-default btn-block" type="button">Pharmacist</button> <?php 
								}else{
									echo $production->PharmacistBy . " Started: " . $production->PharmacistDate;
									$progress = 100;
								} ?> </a></td>


						</tr>
						<tr>

							<td colspan="5">
							<div class="progress progress-striped active">
								<div class="progress-bar progress-bar-success"
								role="progressbar" aria-valuenow="100" aria-valuemin="0"
								aria-valuemax="<?php echo $progress; ?>" style="width: <?php echo $progress; ?>%"></div>
							</div></td>
						</tr>
					</thead>
					<tbody>
						<tr></tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<!--  Detail fields -->
<div class="row">
	<div class="col-lg-12">
		<div class="box border blue">
			<div class="box-title">
				<h4><i class="fa fa-archive"></i> Order Detail </h4>
			</div>
			<div class="box-body">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>Cancel</th>
							<th>Order Info</th>
							<th>Order Name</th>
							<th>Delivery Info</th>
							<th>Delivery or Pickup</th>
							<th>Rush Order</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><a class="btn btn-default btn-lg" type="button"
							href="<?php echo base_url("compliance/productiondetail/"); ?>"> <i
							class="fa fa-ban"></i></a></td>

							<td>
							<p>
								Batch 1654 | Katie Gibney
								<br>
								Strip Dates: June 1, 2015  - June 7, 2015
							</p></td>

							<td>
							<select class="form-control">
								<option ="selected">Operations</option>
								<option>Med Change</option>
								<option>Reorder</option>
							</select>
							<input type="text" name="" class="form-control">
							</td>

							<td>
							<p>
								<input type="text" class="form-control datepicker-fullscreen"
								id="eventDate"
								value="">

								<input type="text" id="eventTime" name="regular"
								class="form-control timepicker-fullscreen"
								value="">
							</p></td>

							<td>
							<br>
							<div class="make-switch switch-large"
							data-on="danger"
							data-off="success"
							data-on-label="<i class='fa fa-truck'></i>"
							data-off-label="<i class='fa fa-hand-o-down'></i>">
								<input type="checkbox">
							</div></td>

							<td>
							<br>
							<div class="make-switch switch-large"
							data-on="danger" data-off="success">
								<input type="checkbox">
							</div></td>



						</tr>
						<tr></tr>
						<tr>
							<td><a class="btn btn-default btn-block" type="button"
							href="<?php echo base_url("compliance/printorders/"); ?>"> Print Cover Sheet & Packmed Reports </a></td>
							<td><a class="btn btn-default btn-block" type="button"
							href="<?php echo base_url("compliance/printorders/"); ?>"> Print Cover Sheet</a></td>
							<td><a class="btn btn-default btn-block" type="button"
							href="<?php echo base_url("compliance/printorders/"); ?>"> Print Pacmed Reports</a></td>
							<td colspan="4"><a class="btn btn-danger btn-block" type="button"
							href="<?php echo base_url("compliance/printorders/"); ?>"> Changes Made, Press to Save</a></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<!--  /detail fields -->

<!--  ORDERS -->
<div class="row">
	<div class="col-lg-12">
		<div class="box border blue">
			<div class="box-title">
				<h4><i class="fa fa-truck"></i>Patients </h4>
			</div>
			<div class="box-body">

				<table class="table table-hover">
					<thead>
						<tr>
							<th>Patient</th>
							<th>Trouble</th>
							<th>Package</th>
							<th>Pharmacist Check</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$patients = $this -> pharm -> getProductionPatients($batchNum);
						if ($patients != FALSE) {
						foreach ( $patients->result () as $patient ) {
						?>
						<tr>
							<td>
							<p>
								<?php echo $patient -> LastName . ', ' . $patient -> FirstName . '<br>' . $patient -> ID; ?>
							</p></td>
							<td><!--
							<div>
							<table class="table table-hover">
							<thead>
							<td><a class="btn btn-default btn-lg" type="button"
							href="<?php echo base_url("compliance/productionorderdetail/"); ?>"><i class="fa fa-exclamation-triangle"></i> Flag for trouble</a></td>
							</thead>
							<tbody>
							<tr>
							<td>Med Change</td>
							<td>
							<button class="btn btn-warning btn-block">
							<i class="fa fa-exclamation-triangle"></i> Fix - Reported by Kim
							</button></td>
							</tr>
							<tr>
							<td>Need Refills</td>
							<td>
							<button disabled class="btn btn-success btn-block">
							<i class="fa fa-check"></i> Fixed by Kate
							</button></td>
							</tr>
							</tbody>
							</table>
							</div> --></td>
							<td><a class="btn btn-default btn-lg" type="button"
							href="<?php echo base_url("compliance/productionorderdetail/"); ?>"> <i class="fa fa-archive"></i> </a></td>

							<td><a class="btn btn-default btn-lg" type="button"
							href="<?php echo base_url("compliance/productionorderdetail/"); ?>"> <i class="fa fa-edit"></i> </a></td>

						</tr>
						<?php }} ?>

					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<!--  /ORDER -->

<!-- /CALENDAR & CHAT -->
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top </span>
</div>
