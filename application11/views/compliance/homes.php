<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left"> Homes</h3>
</div>
<div class="description">

	<!--button class="btn btn-primary btn-lg" type="button"
		onclick="loadModal('<?php echo base_url("compliance/editHomeModal/"). '/-1';?>')"> <i
		class="fa fa-plus"></i> New
	</button--> Client Groups
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!--  MAIN PAGE HERE -->

<div class="row" ng-app="myApp" ng-controller="DoubleController" >
	<div class="col-lg-6">
		<div class="box border blue">
			<div class="box-title">
				<h4>
					<i class="fa fa-home"></i>Homes Name
				</h4>
			</div>
			<div class="box-body">
				<table id="datatable1" cellpadding="0" cellspacing="0" border="0"
					class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th width="60px">#</th>
							<th width="75px">View</th>
							<th>Name</th>
							
						</tr>
					</thead>
					<tbody>
						
					   <?php
					    $i = 1;					
					    if ($homeList) { foreach ( $homeList as $group ) { ?>
							<tr>
								<td><?php echo $i++;              ?></td>
								<td ><button ng-click="getHomeID(<?php echo $group->ID;?>)" class="btn btn-defult"><i class="fa fa-eye"> </i> </button></td>
								<td><?php echo $group->Name;  ?></td>
							</tr>
						<?php }} else {?> 
							<tr>
								<td colspan="3">No data</td>
							</tr> 
						<?php } ?>	
											
					</tbody>
				</table>
			</div>
		</div>
	</div>
	
	<div class="col-lg-6" >
		<div class="box border blue">
			<div class="box-title">
				<h4>
					<i class="fa fa-home" ng-hide="homeIcon"></i><span class="label label-success" ng-hide="homeIcon" >{{ homeName }}</span> <i class="fa fa-angle-double-right" ng-hide="arrow"></i> <i class="fa fa-user"></i>Clients Name 
				</h4>
			</div>
			<div class="box-body">
				<table id="" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th width="60px">#</th>
							<th width="75px">PatID</th>
							<th> <input ng-model="searchPat" class="form-control" placeholder="Search Patient Name"  width="100px" /></th>							
						</tr>
					</thead>
					<tbody>					
						<tr  ng-repeat="patientInfo in patientInfos | filter:searchPat">
							<td>{{ $index + 1 }} </td>
							<td><a ng-href="<?php echo base_url();?>compliance/viewPat/{{ patientInfo.ID }}/profile"> {{ patientInfo.ID }} </a></td>
							<td>{{ patientInfo.LastName }} {{ patientInfo.FirstName }}</td>
						</tr>						
					</tbody>
				</table>
			</div>
		</div>
	</div>
	
</div>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>