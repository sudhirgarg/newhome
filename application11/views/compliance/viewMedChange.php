<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left">Med Change 234 | Patrick, Hoe</h3>
</div>
<div class="description">
	<a class="btn btn-default btn-lg" type="button"
		href="<?php echo base_url();?>compliance/medchanges"> <i class="fa fa-refresh"></i>Med Changes</span>
	</a> <a class="btn btn-default btn-lg" type="button"
		href="<?php echo base_url();?>"> <i class="fa fa-plus"></i>Archive</span>
	</a>
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!--  MAIN PAGE HERE -->

<div class="row">
	<div class="col-lg-12">
		<div class="box border blue">
			<div class="box-title">
				<h4>
					<i class="fa fa-truck"></i>Med Change
				</h4>

			</div>
			<div class="box-body">

				<form class="form-horizontal" role="form">

					<div class="form-group">
						<label class="col-sm-3 control-label">Med Change ID</label>
						<div class="col-sm-9">
							<input type="text" name="regular"
								class="form-control datepicker-fullscreen">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label">Patient</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" placeholder="Text input">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label">Date Created</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" placeholder="Text input">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label">Change</label>
						<div class="col-sm-9">
							<textarea class="form-control" rows="3"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label">Action Taken</label>
						<div class="col-sm-9">
							<textarea class="form-control" rows="3"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label"></label>
						<div class="col-sm-9">
							<a class="btn btn-default btn-lg">Save</a>
						</div>
					</div>

				</form>
			</div>
		</div>
	</div>
</div>

<!-- /CALENDAR & CHAT -->
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>

