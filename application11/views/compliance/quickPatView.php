<?php
												
	$homeInfo = "";
	$patId = "";
	$initial = "";
	$nhWardName ="";
	$patAddress = "";
	$home = "";
	$NHWardId ="";
        if(!empty($patInfo)){
		foreach($patInfo AS $record){
		    $home = $record->Name;
			$homeInfo = $record->Name .'<br/>'.$record->homeAdd1.', '.$record->homeCity.'<br/>'.$record->homePro.', '.$record->homePostal.'<br/>'.$record->HomePhone;
			$patId = $record->ID; 
			$initial = $record->LastName.' '.$record->FirstName;
		    $nhWardName = $record->wardName;
			$patAddress = $record->LastName.' '.$record->FirstName.'<br/>'.$record->Address1.', '.$record->Address2.'<br/>'.$record->PatCity.', '.$record->PatPro.'<br/>'.$record->PatPostal;
			$NHWardId = $record->NHWardID;
		
		}
        }
	
	 $ci =& get_instance();
	 $index='index.php/';

?>




<!--
<div class="clearfix">
	<h3 class="content-title pull-left">Client Profile</h3>
</div>
<div class="description">

	<a class="btn btn-lg btn-default" href="<?php echo base_url().'compliance/viewPat/'.$patId.'/profile'?>" > Full Profile </a>

	<button class="btn btn-lg btn-warning"
		onclick="loadModal('<?php echo base_url() . 'compliance/editMedChangeModal/' . $patId; ?>/-1')">
		<i class="fa fa-plus"></i> New Med Change
	</button>

	<button class="btn btn-lg btn-info"
		onclick="loadModal('<?php echo base_url() . 'compliance/editTicketModal/' . $patId; ?>/-1')">
		<i class="fa fa-plus"></i> New Ticket
	</button>
	
	
	<input id="patID" value="<?php echo $patId; ?>" type="hidden">
	
</div>
</div>
</div>
</div>


<div class="row">
	<div class="col-lg-12">

		
		<div class="row">
			<div class="col-md-12">
				
				<div class="box border">
					<div class="box-title">
						<h4>
							<i class="fa fa-user"></i><span class="hidden-inline-mobile"><?php  echo '<span class="label label-primary">' . $initial.'   -  '.$patId. '</span>'; ?></span>
						</h4>
					</div>
					<div class="box-body">
						<div class="tabbable header-tabs user-profile">
							<ul class="nav nav-tabs">
							    								
								
								<li <?php if ($this -> uri -> segment(4) == "medchanges") {echo 'class="active"'; } ?>>
								    <a href="#medchanges" data-toggle="tab"><i class="fa fa-edit"></i>
									<span class="hidden-inline-mobile"> Med Changes</span></a>
								</li>
								
								<li <?php if ($this -> uri -> segment(4) == "tickets") {echo 'class="active"'; }  	?>>
								     <a href="#tickets" data-toggle="tab"><i class="fa fa-edit"></i> 
								     <span class="hidden-inline-mobile"> Tickets</span></a>
							   </li>

                                <li <?php if ($this -> uri -> segment(4) == "reorders") {echo 'class="active"'; } ?>>
                                       <a href="#reorders" data-toggle="tab"><i class="fa fa-bar-chart-o"></i>
                                        <span class="hidden-inline-mobile"> ReOrders</span></a>
                                </li>

                               
							</ul>
							<div class="tab-content">
								


                                <!-- REORDERS -->
<!-- custom                                <div  id="reorders"  class="tab-pane fade <?php if ($this -> uri -> segment(4) == "reorders") {echo 'in active'; } ?>" >
                                	
                                
                                	
								
                                 <table id="" cellpadding="0" cellspacing="0" border="0"  class="table table-striped table-bordered table-hover datatable table-responsive"> 
                                           
                                        <thead >
												<tr>												
													<th>Medication</th>
													<th>Doctor</th>
													<th>Directions</th>
													<th>Comment</th>
													<th>Select</th>
												</tr>
											</thead>
											<tbody>
											<?php
											
												$checkBoxCount = 1;
												
												if(!empty($rxPatZero)){
													
													foreach ($rxPatZero as $drg) {
														//if($drg->NHBatchFill == 0){
															echo '<tr>';
															echo '<td>' . $drg -> BrandName . ' ' . $drg -> Strength . '<br>RX-' . $drg -> RxNum . '</td>';
															echo '<td>' . $drg -> doc.'</td>';
															echo '<td>' . $drg -> SIG . '</td>';
															echo '<td><input type="text" name="reOrderComment" class="form-control reOrderCommentClass" id="comment'.$checkBoxCount.'" ></td>';
														    echo '<td id="reOrderList" ><input type="checkbox" name="reOrderCheckBox" class="form-control reOrderCheckBoxClass" value="'. $drg -> RxNum .'" id="checkBox' . $checkBoxCount++. '"></td>';
															echo '</tr>';
														//}
														
													} 																																			
												}
																				
											?>
										</tbody>   
										                                  
                                    </table>							
                                    
                                    <input type="hidden" id="patID" value="<?php echo $patId; ?>"> 
                                    <input type="hidden" id="reOrderBox">
                                    <input type="hidden" id="reOrderRx">
                                    <button class="btn btn-block btn-success"  onclick="reOrderPRNsNew()"> <h3>Create Order</h3> </button>
                                    
                                </div>
                                <!-- /REORDERS -->



								<!-- medchanges -->
<!-- custom							<div class="tab-pane fade <?php if ($this -> uri -> segment(4) == "medchanges") { echo 'in active'; } ?>" id="medchanges">

									<table cellpadding="0" cellspacing="0" border="0" id="medchanges1" class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th>MedChangeID -</th>
												<th>Created</th>
												<th>Tech</th>
												<th>Pharmacist</th>
												<th>Starts/Ends</th>
												<th>Change</th>
												<th>Action Completed</th>
											</tr>
										</thead>
										<tbody>
										<?php
											  $medChange = $this->comp->getMedChange ( '-1', $patId );
										  	  if ($medChange != FALSE) {
											  	  foreach ( $medChange->result () as $medChangeItem ) {
										?>
											<tr
												onclick="loadModal('<?php echo base_url('compliance/editMedChangeModal') . '/' . $patId . '/' . $medChangeItem -> medChangeID; ?>')">
												<td><?php echo $medChangeItem -> medChangeID; ?></td>
												<td><?php echo $medChangeItem -> dateOfChange; ?></td>
												<td><?php $tech = $this -> ion_auth -> user($medChangeItem -> technicianUserID) -> row();
													echo $tech -> first_name;
												?></td>
												<td><?php
												if ($medChangeItem -> pharmacistUserID > 0) {$pharm = $this -> ion_auth -> user($medChangeItem -> pharmacistUserID) -> row();
													echo $pharm -> first_name;
												}
												?></td>


												<td><?php echo $medChangeItem -> startDate . ' to ' . $medChangeItem -> endDate; ?></td>
												<td><?php echo $medChangeItem -> changeText; ?></td>
												<td><?php
												
												if ($medChangeItem->actionComplete > 0) {
													?>
													<span class="badge badge-green"><i class="fa fa-check"></i></span>
												<?php
													;
												}
												?></td>
											</tr>
											<?php
											}
											} else {
											echo '<tr><td colspan="7"><div class="center">No MED CHANGES</div></td></tr>';
											}
										?>
										</tbody>
									</table>
								</div>
								<!-- /medchanges -->
								
								
								

								<!-- tickets -->
<!-- custom								<div class="tab-pane fade <?php if ($this -> uri -> segment(4) == "tickets") { echo 'in active'; } ?>" id="tickets">
									
									<table id="dtTickets" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th>Created Date</th>
												<th>Complete Date</th>
												<th>Created By</th>
												<th>Ticket Type</th>
												<th>Ranks</th>
												<th>Description</th>
												<th>Complete</th>
											</tr>
										</thead>
										<tbody>
												<?php
												$generalTicket = $this->comp->getTickets ( '-1', '1', $patId );
												if ($generalTicket != FALSE) {
													foreach ( $generalTicket->result () as $generalTicketItem ) {
														?>
				                                <tr onclick="loadModal('<?php echo base_url('compliance/editTicketModal/') . '/' . $generalTicketItem -> patID . '/' . $generalTicketItem -> ticketID; ?>')">
													<td><?php echo $generalTicketItem -> dateCreated; ?></td>
													<td><?php echo $generalTicketItem -> dateComplete; ?></td>
													<td><?php $ticketCreator = $this -> ion_auth -> user($generalTicketItem -> createdBy) -> row();
														echo $ticketCreator -> first_name;
													?></td>
													<td><?php $refType = $this -> comp -> getTicketTypeReference($generalTicketItem -> ticketTypeReferenceID);
														echo $refType;
													?></td>
													<td><?php if(empty($generalTicketItem -> rank)) { echo "0.0"; } else { echo $generalTicketItem -> rank; }   ?> </td>
													<td><?php echo $generalTicketItem -> Description; ?></td>
													<td><?php
															
															if ($generalTicketItem->dateComplete != '') {
																?>
														<span class="badge badge-green"><i class="fa fa-check"></i></span>
													<?php
																;
															}
														?></td>
											</tr>
							<?php  }}else{echo '<tr><td colspan="6"><div class="center">NO TICKETS</div></td></tr>';} ?>
											</tbody>
									</table>
								</div>
								<!-- /tickets -->

								<!-- mars -->
<!-- custom								<div
									class="tab-pane fade <?php
									if ($this -> uri -> segment(3) == "mars") {echo 'in active';
									}
								?>"
									id="mars">

									<table id="dtmars" cellpadding="0" cellspacing="0" border="0"
										class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th></th>
												<th>Medication</th>
												<th>Scheduled Date/Time</th>
												<th>Qty</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td></td>
												<td>Metformin 500mg Take 1 tab daily Dr. Joe Doe</td>
												<td>23/02/2014 8:00pm</td>
												<td>2 tablets</td>
											</tr>

										</tbody>
									</table>
								</div>
								<!-- /mars -->


                                <!-- Delivery -->
                                
                             
                               
                                <!-- /Delivery -->



								<!-- billing -->
<!-- custom								<div
									class="tab-pane fade <?php
									if ($this -> uri -> segment(4) == "billing") {echo 'in active';
									}
								?>"
									id="billing">
									<h1>We are currently updating the AR Module to include new features. Please check again soon.</h1>
											<?php
											// 											$patAR = $this->pharm->getPatAR ( $patient->patID );

											//
 											?>
											<h2><?php
											// 											if ($patAR != FALSE) {
											// 												echo 'Account #: ' . $patAR->AccountNum . ' Last Statement: ' . $patAR->LastStatementDate;
											// 											}
											?></h2>

											<?php
											// 											$progressBarValue;
											// 											if ($patAR->CreditLimit > 0) {
											// 												$progressBarValue = (($patAR->balance * 100) / $patAR->CreditLimit);
											// 											} else {
											// 												$progressBarValue = 100;
											// 											}
											//
 											?>
										<!-- 	<h4 class="pull-right">Credit Limit: <?php //echo $patAR->CreditLimit; ?></h4>
<!-- 									<div class="progress progress-striped active"> -->
<!-- 										<div class="progress-bar" role="progressbar" -->
<!-- 													aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" -->
												 <!-- 	style="width: <?php //echo $progressBarValue;?>%">  -->
										 <!-- 	<span class="sr-only"><?php //echo 'Balance $ ' . $patAR->balance;?></span>
<!-- 										</div> -->
<!-- 									</div> -->
								<!-- 	<h4 class="pull-right">Balance <?php //echo $patAR->balance;?></h4>


<!-- 									<table id="dtbilling" cellpadding="0" cellspacing="0" -->
<!-- 										border="0" class="table table-bordered table-hover"> -->
<!-- 										<thead> -->
<!-- 											<tr> -->
<!-- 												<th>Invoice #</th> -->
<!-- 												<th>Invoice Date</th> -->
<!-- 												<th>Status</th> -->
<!-- 												<th>SubTotal</th> -->
<!-- 												<th>Tax</th> -->
<!-- 												<th>Paid</th> -->
<!-- 											</tr> -->
<!-- 										</thead> -->
<!-- 										<tbody> -->
												<?php
												// 												$patARInvoices = $this->pharm->getPatARInvoices ( $patient->patID );
												// 												if ($patARInvoices != FALSE) {
												// 													foreach ( $patARInvoices->result () as $invoice ) {
														?> 
<!-- 														<tr -->
									<!-- 			onclick="loadModal('<?php //echo base_url() . 'compliance/viewInvoiceModal/' . $invoice->InvoiceNum;?>')"
												class="<?php //switch ($invoice->Status){ case 1: echo "danger";break; case 0: echo "warning";break; case 3: echo "success";break; }?>">
												<td><?php //echo $invoice->InvoiceNum;?></td>
												<td><?php //echo $invoice->InvoiceDate;?></td>
												<td><?php //switch($invoice->Status){ case 1: echo "Posted";break; case 0: echo "Open";break; case 3: echo "Closed";break; }?></td>
												<td><?php //echo $invoice->SubTotal;?></td>
												<td><?php //echo $invoice->tax;?></td>
												<td><?php // echo $invoice->Paid;?></td>
<!-- 											</tr> -->
												<?php
												// 													}
												// 												} else {
												// 													echo '<tr><td colspan="6"><div class="center">No Data</div></td></tr>';
												// 												}
												//
 												?>
													

<!-- 												</tbody> -->
<!-- 									</table> -->
<!-- custom								</div>
								

								


							</div>
						</div>
						
					</div>
					
					
				</div>

				
				
				

			</div>

			
			<div class="footer-tools">
				<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
				</span>
			</div>

-->





















<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN BREADCRUMBS -->
        <div class="breadcrumbs">
            <h1>Client Profile</h1>
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?><?php echo $index; ?>compliance/medchanges">Home</a></li>
                <li class="active">Client Profile</li>
            </ol>
        </div>
        <!-- END BREADCRUMBS -->
        <div class="portlet-body util-btn-margin-bottom-5 button_div">
            <div class="btn-group btn-group-solid custom-button">
                <p>
                    <a href="<?php echo base_url() ?><?php echo $index; ?>compliance/viewPat/<?php echo $patId; ?>/profile"><button type="button" class="btn red"><i class="fa fa-user"></i> Full Profile</button></a>
                    <button type="button" class="btn purple" onclick="loadModal('<?php echo base_url() ?><?php echo $index; ?>compliance/editMedChangeModal/<?php echo $patId; ?>/-1')"><i class="fa fa-plus"></i> New Med Change</button>
                    <button type="button" class="btn blue" onclick="loadModal('<?php echo base_url() ?><?php echo $index; ?>compliance/editTicketModal/<?php echo $patId; ?>/-1')"><i class="fa fa-plus"></i> New Ticket</button>
                </p>   
            </div>
        </div>
        <!-- BEGIN SIDEBAR CONTENT LAYOUT -->
        <div class="page-content-container users_list">
            <div class="page-content-row">
                <div class="page-content-col">
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box yellow">
                                <div class="portlet-title">
                                    <div class="caption"><i class="fa fa-user"></i><?php  echo (!empty($initial)) ? $initial : ''.' - '.(!empty($patId)) ? $patId : ''; ?> </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"> </a>
                                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="tabbable-custom ">
                                        <ul class="nav nav-tabs ">
                                            <li <?php if ($this -> uri -> segment(4) == "reorders") { ?>class="active"<?php } ?>>
                                                <a href="#tab_5_1" data-toggle="tab"> Reorders </a>
                                            </li>
                                            <li <?php if ($this -> uri -> segment(4) == "tickets") { ?>class="active"<?php } ?>>
                                                <a href="#tab_5_2" data-toggle="tab"> Tickets </a>
                                            </li>
                                            <li <?php if ($this -> uri -> segment(4) == "medchanges") { ?>class="active"<?php } ?>>
                                                <a href="#tab_5_3" data-toggle="tab"> Med Changes </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane <?php if ($this -> uri -> segment(4) == 'reorders') { ?>active<?php } ?>" id="tab_5_1">
                                                <!-- BEGIN REORDER TABLE PORTLET-->
                                                <div class="portlet box">
                                                    <input type="hidden" id="patID" value="<?php echo $patId; ?>"> 
                                                    <input type="hidden" id="reOrderBox">
                                                    <input type="hidden" id="reOrderRx">
                                                    <button class="btn btn-block btn-success"  onclick="reOrderPRNsNew()"> <h3>Create Order</h3> </button>
                                                    <div class="portlet-body">
                                                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                                                            <thead>
                                                                <tr>
                                                                    <th> Medication </th>
                                                                    <th> Doctor </th>
                                                                    <th> Directions </th>
                                                                    <th> Comment </th>
                                                                    <th> Select </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                if(!empty($rxPatZero)){
                                                                $checkBoxCount = 1;
                                                                foreach ($rxPatZero as $drg) {
                                                                ?> 
                                                                <tr>
                                                                    <td> <?php echo $drg -> BrandName . ' ' . $drg -> Strength . ' #RX-' . $drg -> RxNum ?> </td>
                                                                    <td> <?php echo $drg -> doc; ?> </td>
                                                                    <td> <?php echo $drg -> SIG; ?> </td>
                                                                    <td> <input type="text" name="reOrderComment" class="form-control reOrderCommentClass" id="comment<?php echo $checkBoxCount; ?>" > </td>
                                                                    <td> <input type="checkbox" name="reOrderCheckBox" class="form-control reOrderCheckBoxClass" value="<?php echo $drg -> RxNum; ?>" id="checkBox<?php echo $checkBoxCount; ?>"> </td> 
                                                                </tr>
                                                                <?php $checkBoxCount++; }} ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <!-- END REORDER TABLE PORTLET-->
                                            </div>
                                            <div class="tab-pane <?php if ($this -> uri -> segment(4) == 'tickets') { ?>active<?php } ?>>" id="tab_5_2">
                                                <!-- BEGIN TICKETS TABLE PORTLET-->
                                                <div class="portlet box">
                                                    <div class="portlet-body">
                                                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                                                            <thead>
                                                                <tr>
                                                                    <th> Created Date </th>
                                                                    <th> Complete Date </th>
                                                                    <th> Created By </th>
                                                                    <th> Ticket Type </th>
                                                                    <th> Ranks </th>
                                                                    <th> Description </th>
                                                                    <th> Complete </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                $generalTicket = $this->comp->getTickets ( '-1', '1', $patId );
                                                                if ($generalTicket != FALSE) {
                                                                foreach ( $generalTicket->result () as $generalTicketItem ) {
                                                                ?>
                                                                <tr onclick="loadModal('<?php echo base_url($index.'compliance/editTicketModal/') . '/' . $generalTicketItem -> patID . '/' . $generalTicketItem -> ticketID; ?>')">
                                                                    <td> <?php echo $generalTicketItem -> dateCreated; ?> </td>
                                                                    <td> <?php echo $generalTicketItem -> dateComplete; ?> </td>
                                                                    <td>
                                                                        <?php $ticketCreator = $this -> ion_auth -> user($generalTicketItem -> createdBy) -> row();
                                                                        echo $ticketCreator -> first_name;?>
                                                                    </td>
                                                                    <td>
                                                                        <?php $refType = $this -> comp -> getTicketTypeReference($generalTicketItem -> ticketTypeReferenceID);
                                                                        echo $refType; ?>
                                                                    </td>
                                                                    <td> <?php if(empty($generalTicketItem -> rank)) { echo "0.0"; } else { echo $generalTicketItem -> rank; }   ?> </td>
                                                                    <td> <?php echo $generalTicketItem -> Description; ?> </td>
                                                                    <td> <?php if ($generalTicketItem->dateComplete != '') { ?><i class="fa fa-check-square-o" aria-hidden="true"></i><?php } ?></td>
                                                                </tr>
                                                                <?php }} ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <!-- END TICKETS TABLE PORTLET-->
                                            </div>
                                            <div class="tab-pane <?php if ($this -> uri -> segment(4) == 'medchanges') { ?>active<?php } ?>" id="tab_5_3">
                                                <!-- BEGIN MEDCHANGES TABLE PORTLET-->
                                                <div class="portlet box">
                                                    <div class="portlet-body">
                                                        <table class="table table-striped table-bordered table-hover" id="sample_3">
                                                            <thead>
                                                                <tr>
                                                                    <th> MedChangeID </th>
                                                                    <th> Created </th>
                                                                    <th> Tech </th>
                                                                    <th> Pharmacist </th>
                                                                    <th> Starts/Ends </th>
                                                                    <th> Change </th>
                                                                    <th> Action Completed </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                $medChange = $this->comp->getMedChange ( '-1', $patId );
                                                                if ($medChange != FALSE) {
                                                                foreach ( $medChange->result () as $medChangeItem ) {
                                                                ?>
                                                                <tr onclick="loadModal('<?php echo base_url($index.'compliance/editMedChangeModal') . '/' . $patId . '/' . $medChangeItem -> medChangeID; ?>')">
                                                                    <td> <?php echo $medChangeItem -> medChangeID; ?> </td>
                                                                    <td> <?php echo $medChangeItem -> dateOfChange; ?> </td>
                                                                    <td>
                                                                        <?php $tech = $this -> ion_auth -> user($medChangeItem -> technicianUserID) -> row();
                                                                        echo $tech -> first_name; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php
                                                                        if ($medChangeItem -> pharmacistUserID > 0) {
                                                                            $pharm = $this -> ion_auth -> user($medChangeItem -> pharmacistUserID) -> row();
                                                                            echo $pharm -> first_name;
                                                                        }
                                                                        ?>
                                                                    </td>
                                                                    <td> <?php echo $medChangeItem -> startDate . ' to ' . $medChangeItem -> endDate; ?> </td>
                                                                    <td> <?php echo $medChangeItem -> changeText; ?> </td>
                                                                    <td> <?php if ($medChangeItem->actionComplete > 0) { ?><i class="fa fa-check-square-o" aria-hidden="true"></i><?php } ?> </td> 
                                                                </tr>
                                                                <?php }} ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <!-- END MEDCHANGES TABLE PORTLET-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
            </div>
        </div>
        <!-- END SIDEBAR CONTENT LAYOUT -->
    </div>
