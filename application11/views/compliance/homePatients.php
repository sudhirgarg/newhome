<!-- STYLER -->
<?php $group = $this->comp->getHome($this->uri->segment(3));?>
<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left"><?php echo $group->homeName; ?></h3>
</div>
<div class="description">
	<button class="btn btn-primary btn-lg" type="button"
		onclick="loadModal('<?php echo base_url("compliance/editHomeModal") . "/" . $group->homeID ;?>')">
		<i class="fa fa-pencil"></i> Edit Home
	</button>

	<button class="btn btn-primary btn-lg" type="button"
		onclick="loadModal('<?php echo base_url("compliance/addUserToHomeModal") . "/" . $group->homeID ;?>')">
		<i class="fa fa-pencil"></i> Assign Home Portal User
	</button>
	
	<a class="btn btn-info btn-lg" type="button"
		href="<?php echo base_url("compliance/printHomeInfo") . "/" . $group->homeID ;?>">
		<i class="fa fa-pencil"></i> Print Home Info
	</a>
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!--  MAIN PAGE HERE -->

<div class="row">
	<div class="col-lg-12">
		<div class="box border blue">
			<div class="box-title">
				<h4>
					<i class="fa fa-users"></i>Clients
				</h4>
			</div>
			<div class="box-body">
				<table id="datatable1" cellpadding="0" cellspacing="0" border="0"
					class="datatable table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>Client</th>
							<th>Home</th>
							<th>Tickets</th>
							<th>Med Changes</th>
							<th>Billing</th>
						</tr>
					</thead>
					<tbody>
					<?php
					$patients = $this->comp->getPatHomes ( '', $group->homeID );
					if ($patients != false) { // Often times I'll return false in a model if no results are found
						foreach ( $patients->result () as $patient ) {
							$patHomeID = $this->comp->getPatHomes ( $patient->patID, '0' );
							$patHome = $this->comp->getHome ( $patHomeID );
							?>
						<tr
							onclick="window.location='<?php echo base_url("compliance/viewPat/". $patient->patID . "/profile");?>'">
							<td>
								<h4><?php echo $patient->patInitials . $patient->patID ;?></h4>
							</td>
							<td><?php echo $patHome->homeName;?></td>
							<td><a class="btn btn-lg btn-primary"
								href="<?php echo base_url("compliance/viewPat/". $patient->patID . "/tickets");?>"><?php echo $this->comp->getTicketCount($patient->patID);?>
									Tickets</a></td>
							<td><a class="btn btn-lg btn-warning"
								href="<?php echo base_url("compliance/viewPat/". $patient->patID . "/medchanges");?>"><?php echo $this->comp->getMedChangeCount($patient->patID);?>
									Med Changes</a></td>
							<td><a class="btn btn-lg btn-danger"
								href="<?php echo base_url("compliance/viewPat/". $patient->patID . "/billing");?>">
									Billing</a></td>
						</tr>
<?php }} else { ?><tr>
							<td>No Records</td>
						</tr><?php  }?>
					</tbody>
				</table>
			</div>
		</div>
	</div>


	<div class="col-lg-12">
		<div class="box border blue">
			<div class="box-title">
				<h4>
					<i class="fa fa-users"></i>Home Portal Users
				</h4>
			</div>
			<div class="box-body">
				<table id="datatable1" cellpadding="0" cellspacing="0" border="0"
					class="datatable table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>User Name</th>
							<th>company</th>
							<th>Email</th>
							<th>phone</th>
							<th>Delete</th>
						</tr>
					</thead>
					<tbody>
					<?php
					$userIDs = $this->comp->getUserHomes ( '0', $group->homeID );
					if ($userIDs != false) { // Often times I'll return false in a model if no results are found
						foreach ( $userIDs->result () as $myuser ) {
							?>
						<tr>
							<td>
								<h4><?php echo $myuser->first_name . ' ' . $myuser->last_name ;?></h4>
							</td>
							<td><?php echo $myuser->company;?></td>
							<td><?php echo $myuser->email;?></td>
							<td><?php echo $myuser->phone;?></td>
							<td><button class="btn btn-lg btn-default"
									onclick="removeAccess(<?php echo $group->homeID . ', ' . $myuser->id;?>)">Remove Access</button></td>
						</tr>
<?php }} else { ?><tr>
							<td>No Records</td>
						</tr><?php  }?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

</div>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>