<?php
     $ci =& get_instance();
     $index='index.php/';	
?>

<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN SIDEBAR CONTENT LAYOUT -->
        <div class="page-content-container">
            <div class="page-content-row">
                <div class="page-content-col">
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <!--<div class="todo-main-header">
                        <h3>Tickets</h3>
			<a href="<?php echo base_url(). 'compliance/archivedTickets';?>"><button class="btn btn-square btn-sm green todo-bold">Archived</button></a><a class="btn btn-sm btn-info" href="<?php echo base_url(). 'compliance/ticketsDashRefresh';?>" style="margin-left: 9px;padding: 4px 10px 4px 10px;"><i class="fa fa-refresh"></i></a>
                        <ul class="todo-breadcrumb">
                            <li><a href="javascrip:;">Home</a></li>
                            <li><a class="todo-active" href="javascrip:;">Tickets</a></li>
                        </ul>
                    </div>-->
                    <!-- BEGIN BREADCRUMBS -->
                    <div class="breadcrumbs">
                        <h1>Tickets</h1>
                        <a href="<?php echo base_url(). 'compliance/archivedTickets';?>"><button class="btn btn-square btn-sm green todo-bold">Archived</button></a>
                        <a class="btn btn-sm btn-info" href="<?php echo base_url(). 'compliance/ticketsDashRefresh';?>" style="margin-left: 9px;padding: 4px 10px 4px 10px;"><i class="fa fa-refresh"></i></a>
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url(); ?><?php echo $index; ?>compliance/ticketsdash">Home</a></li>
                            <li class="active">Tickets</li>
                        </ol>
                        <!-- Sidebar Toggle Button -->
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".page-sidebar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="toggle-icon">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </span>
                        </button>
                        <!-- Sidebar Toggle Button -->
                    </div>
                    <?php 
                    $monthRank = "";
                    $weekRank = "";
                    $dayRank = "";
                    foreach ( $totalTicketRankMonth as $totalTicketRankList ) {						
                        if(!empty($totalTicketRankList->monthRank)) { 
                            $monthRank =  $totalTicketRankList->monthRank / $totalTicketRankList->idTickets;
                        } 
                    }					
                    foreach ( $totalTicketRankWeek as $totalTicketRankList ) {
                        if(!empty($totalTicketRankList->weekRank)) {
                            $weekRank = $totalTicketRankList->weekRank / $totalTicketRankList->idTickets;
                        } 
                    }					
                    foreach ( $totalTicketRankDay as $totalTicketRankList ) {
                        if(!empty($totalTicketRankList->dayRank)) {
                            $dayRank =  $totalTicketRankList->dayRank / $totalTicketRankList->idTickets;
                        }
                    }
                    ?>
                    <div class="rating_top">
                        <div class="score-common score">
                            <h4>Today's Score:</h4>
                            <p class="ratebox raterater-wrapper" data-id="2" data-rating="<?php echo $dayRank; ?>"></p>
                        </div>
                        <div class="score-common week">
                            <h4>This Week:</h4>
                            <p class="ratebox" data-id="2" data-rating="<?php echo $weekRank; ?>"></p>
                        </div>
                        <div class="score-common month">
                            <h4>This Month:</h4>
                            <p class="ratebox" data-id="2" data-rating="<?php echo $monthRank; ?>"></p>
                        </div>
                    </div>
                    <div class="todo-container">
                        <div class="row">
                            <div class="col-md-6">
                                <ul class="todo-projects-container">
                                    <li class="todo-padding-b-0">
                                        <div class="todo-head">
                                            <h3><i class=" fa fa-ticket font-green"></i>General Ticket</h3>
                                            <!--<p>4 Waiting Attention</p>-->
                                        </div>
                                    </li>
                                    <?php
                                    foreach($ticketInfo AS $record){
                                        if($record->ticketTypeReferenceID == 1){
                                    ?>
                                            <li class="todo-projects-item">
                                                <h3><a href="#" onclick="loadModal('<?php echo base_url() ?>index.php/compliance/editTicketModal/<?php echo $record->patID; ?>/<?php echo $record->ticketID; ?>')"><?php echo ucfirst($record->LastName).' '.ucfirst($record->FirstName) ?></a><a class="todo-add-button" href="#" onclick="loadModal('<?php echo base_url() ?>index.php/compliance/deleteTicket/<?php echo $record->patID; ?>/<?php echo $record->ticketID; ?>')">X</a><?php echo $ci->getNewTicket($record->ticketID); ?></h3>
						<p><?php echo ucfirst($record->first_name); ?></p>	
                                                <p><?php echo $record->Description; ?></p>
                                                <div class="todo-project-item-foot">
                                                    <!--<p class="todo-red todo-inline">17 Tasks Remaining</p>-->
                                                    <p class="todo-inline todo-float-r"><?php echo $record->datecreated; ?></p>
                                                </div>
                                            </li>
                                            <div class="todo-projects-divider"></div>
                                    <?php }} ?>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <ul class="todo-projects-container">
                                    <li class="todo-padding-b-0">
                                        <div class="todo-head">
                                            <h3><i class=" fa fa-ticket font-green"></i>Re Order</h3>
                                            <!--<p>4 Waiting Attention</p>-->
                                        </div>
                                    </li>
                                    <?php
                                    foreach($ticketInfo as $record){
                                        if($record->ticketTypeReferenceID == 2){
                                    ?>
                                            <li class="todo-projects-item">
                                                <h3><a href="#" onclick="loadModal('<?php echo base_url() ?>index.php/compliance/editTicketModal/<?php echo $record->patID; ?>/<?php echo $record->ticketID; ?>')"><?php echo ucfirst($record->LastName).' '.ucfirst($record->FirstName) ?></a><a class="todo-add-button" href="#" onclick="loadModal('<?php echo base_url() ?>index.php/compliance/deleteTicket/<?php echo $record->patID; ?>/<?php echo $record->ticketID; ?>')">X</a></h3>
                                                <p><?php echo $record->Description; ?></p>
						<?php if(!empty($record->rxnumList)){
                                                    $rxInfo = $this->rest->get('production/exApi/rxInfo/'.$record->rxnumList);
                                                    if(!empty($rxInfo)){		
                                                        foreach($rxInfo as $record){
                                                ?>		
						            <p>Rx# <?php echo $record->RxNum.' : '.$record -> BrandName.' ('.$record -> Strength.')'; ?></p>
                                                <?php }} ?>	
                                                <?php } ?>
                                                <div class="todo-project-item-foot">
                                                    <!--<p class="todo-red todo-inline">17 Tasks Remaining</p>-->
                                                    <p class="todo-inline todo-float-r"><?php echo $record->datecreated; ?></p>
                                                </div>
                                            </li>
                                            <div class="todo-projects-divider"></div>
                                    <?php }} ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE BASE CONTENT -->
            </div>
        </div>
    </div>
    <!-- END SIDEBAR CONTENT LAYOUT -->  
