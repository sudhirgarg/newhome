<?php

	header("Cache-Control: no-cache, must-revalidate");
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	
	$isNew = true;	
	
	
	if ($this -> uri -> segment(4) > 0) {
		$isNew = false;
		$ticketID = $this -> uri -> segment(4);		
		
	} else {
		$ticketID = 0;		
	}
	
	
	
?>
<div class="modal-header">

	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

	<h4 class="modal-title">
		<?php
			
			
			if ($isNew == false) {			
				echo 'Ticket Detail';	
			} else {
				echo 'New Ticket';
			}		
			
			echo '<a href="' . base_url("compliance/quickPatView") . '/' . $patient ->ID . '/reorders' . '"><span class="label label-primary">' . $patient -> LastName .' '.$patient->FirstName. '-' . $patient ->ID . '</span></a>';
			
		?> 
	</h4>
</div>
<div class="modal-body">
	<div class="form-horizontal" role="form">
		<input id="ticketID" type="hidden" value="<?php if ($isNew == false) {echo $Ticket -> ticketID; } else { echo $ticketID; } ?>">
		<input id="patID"    type="hidden" value="<?php if ($isNew == false) {echo $Ticket -> patID;    } else { echo $patient ->ID;    } ?>">
		<input id="active"   type="hidden" value="<?php if ($isNew == false) {echo $Ticket -> active;   } else { echo '1';       } ?>">
	
	   <?php if($isNew==FALSE) { ?>  
		
	    <div class="form-group">
			<label class="col-sm-3 control-label">Date Recorded</label>
			<div class="col-sm-9">
				<input type="text" id="dateCreated" class="form-control" disabled value="<?php echo $Ticket -> dateCreated; ?>">
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-3 control-label">Date Complete</label>
			<div class="col-sm-9">
				<input type="text" id="dateComplete" class="form-control" disabled value="<?php echo $Ticket -> dateComplete; ?>">
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-3 control-label">Created By</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" id="createdByName" placeholder="Text input" disabled value="<?php $tech = $this -> ion_auth -> user($Ticket -> createdBy) -> row(); echo $tech -> first_name;?>">
				<input type="hidden" class="form-control" id="createdBy" placeholder="Text input" value="<?php echo $tech -> id; ?>">
			</div>
		</div>
		
        <input type="hidden" id="ticketModalURI" value="<?php echo uri_string(); ?>" />
        
		<div class="form-group">
			<label class="col-sm-3 control-label">Ticket Type</label>
			<div class="col-sm-9">
				<select class="form-control" id="ticketTypeReferenceID">
					<?php				
				
						
						foreach ( $ticketTypeReference->result () as $ticketTypeReferenceItem ) {
							
							echo '<option value="'.$ticketTypeReferenceItem -> ticketTypeReferenceID.'"';
						
							if ($ticketTypeReferenceItem -> ticketTypeReferenceID == $Ticket -> ticketTypeReferenceID) { echo ' selected="selected"'; }
				            echo '>'.$ticketTypeReferenceItem -> ticketTypeReferenceType.'</option>';   
	                
						}
					?>
				</select>
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-3 control-label">  </label>
			<div class="col-sm-9">
				<input type="hidden" id="rxnumList" class="form-control" value="<?php echo $Ticket -> rxnumList; ?>">
			</div>
		</div>
		
		
	    
	    <div class="form-group">
			<label class="col-sm-3 control-label">Date of delivery</label>
			<div class="col-sm-3">
			    <input type="hidden" id="getDeliveryDate" value="<?php if(!empty($Ticket -> dateOfDelivery)){ echo date('Y-m-d',strtotime($Ticket->dateOfDelivery));} else { echo ''; }   ?>">
				<input type="date" class="form-control" id="deliveryDate" placeholder="Text input" value="<?php if(!empty($Ticket -> dateOfDelivery)){ echo date('Y-m-d',strtotime($Ticket->dateOfDelivery));} else { echo date('Y-m-d'); }   ?>">
			
			</div>
				
			<div class="col-sm-3">
			    <div class="radio"> 
			        <label> Send to Eboard : </label>  
				    <label><input type="radio" name="batchNum" value="1" <?php if($Ticket->batchNum == 1){ echo ' checked '; } ?> > No </label> 
				    <label><input type="radio" name="batchNum" value="0" <?php if($Ticket->batchNum == 0){ echo ' checked '; } ?> > Yes </label>
				 </div>
			</div>
			
		</div>
		
	<?php } else { ?>
		
		<input id="createdBy" type="hidden" value="<?php echo $user -> id; ?>"> 
		<input id="dateComplete" type="hidden" value="">
		
		<div class="form-group">
			<label class="col-sm-3 control-label">Date Created</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" id="dateCreated" placeholder="Text input" disabled value="<?php echo date('Y-m-d H:i:s'); ?>">
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label">Created By</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" id="createdByName" placeholder="Text input" disabled value="<?php  echo $user -> first_name; ?>">
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label">Ticket Type</label>
			<div class="col-sm-9">
				<select class="form-control" id="ticketTypeReferenceID">
				<?php	
						
						foreach ( $ticketTypeReference->result () as $ticketTypeReferenceItem ) {							
							echo '<option value="'.$ticketTypeReferenceItem -> ticketTypeReferenceID.'">'.$ticketTypeReferenceItem -> ticketTypeReferenceType.'</option>'; 
	                
						}
				?>
					
				</select>
			</div>
		</div>
		
		
		
	<?php } ?>	
	

		<div class="form-group">
			<label class="col-sm-3 control-label">Description</label>
			<div class="col-sm-9">
				<textarea class="form-control" rows="4" id="Description"> <?php if ($isNew == false) { echo $Ticket -> Description; } ?> </textarea>
			</div>
		</div>
		
		<?php if($isNew == false) {  ?>

		<!-- CHAT -->
		<div class="row">
			<div class="col-md-12">
				<!-- BOX -->
				<div class="box border green chat-window">
					<div class="box-title">
						<h4> <i class="fa fa-comments"></i>Chat Window </h4>
					</div>
					<div class="box-body big">
						<div class="scroller" data-height="450px" data-always-visible="1" data-rail-visible="1">
							<div class="chat-form">
								<div class="input-group">
									<input type="text" class="form-control" id="message"> 
									<span class="input-group-btn">
										<button class="btn btn-primary" type="button" onclick="addComment()" id="addCommentButton" data-loading-text="Send">
											<i class="fa fa-check"></i>
										</button>
									</span>
								</div>
							</div>
							
							<div class="divide-20"></div>
							
							<ul class="media-list chat-list">
								<?php
									
									  if ($comments != FALSE) {
										  foreach ( $comments->result() as $comment ) {
											 if ($comment->userID == $user->id) {								
								
													echo'<li class="media">
														<div class="media-body chat-pop mod">
				
															<h4 class="media-heading pull-right">
																You 
																<span class="pull-left"> 
																	<abbr class="timeago" title="'.$comment -> messageDate.'">'.$comment -> messageDate.'</abbr>
																	<i class="fa fa-clock-o"></i>
																</span>
															</h4>
					                                        <br/>
															<h4>'.$comment -> message.'</h4>
														</div>
												    </li>';
													    
											  } else {
									
													echo '<li class="media">
															<div class="media-body chat-pop">
																<h4 class="media-heading">';																	
						
																  $messenger = $this -> ion_auth -> user($comment -> userID) -> row();
																
																	echo '<span class="label label-info">' .$messenger -> first_name. ' <span class="fa fa-magic"></span></span>
																	<span class="pull-right"><abbr class="timeago" title="'.$comment -> messageDate.'">'.$comment -> messageDate.'</abbr>
																		<i class="fa fa-clock-o"></i></span>
																</h4>
																<h4>'.$comment -> message.'</h4>
															</div>
													    </li>';
												 
											} 
										}
                                     } 
                                ?>
                                
							 </ul>
						</div>

					</div>
				</div>
				<!-- /BOX -->
			</div>
		</div>
		<!-- /CHAT -->

		<?php } ?>
		
	</div>
	<div class="modal-footer">

		<?php
		
			if ($isNew == false) {
				if ($Ticket->active == 1) {
					
					echo '<button type="button" class="btn btn-warning pull-left" onclick="closeTicket()" id="closeTicketButton" data-loading-text="Completing Ticket...">Close Ticket</button>';
				
				} else {
				// show no buttons
				}
			}
		
		?>
		
		<button type="button" class="btn btn-default" data-dismiss="modal" id="closeTicketButtonModal">Close</button>
		<button type="button" class="btn btn-primary" id="saveTicketButton" onclick="saveTicket()" data-loading-text="Saving...">Save</button>
		
		
		  		
		  		<?php  
		  		
		  		if(!empty($Ticket)){
		  		     if($Ticket -> active == 0){
		  		         if(!empty($Ticket->deliveryDateChangeAfterClose)){
		  		            $changeArray = explode(";",$Ticket->deliveryDateChangeAfterClose); 
		  		        
		  		         	 echo '<div class="alert alert-warning">
		  							<strong>Delivery date('.$changeArray[0].') changed after close ticket!</strong> ';
		  		         	         $users = $this -> ion_auth -> user($changeArray[1]) -> row();
			  		        
			  		               echo " By ".$users->last_name.' '.$users->first_name;
			  		         echo '</div>';
		  		         }
		  		         
		  		         
		  		         
		  		         	echo '<div class="alert alert-info">';
					        if($Ticket->closedBy > 0){
		  						echo '<strong>This ticket closed on ('.$Ticket->closedDate.') </strong> ';	
		  		         		$users = $this -> ion_auth -> user($Ticket->closedBy) -> row();		  		         	 
		  		         		echo " By ".$users->last_name.' '.$users->first_name;
		  		         	}else{
		  		         		echo "Old tickets";
		  		         	}
		  		         	echo '</div>';		  		        
		  		         
		  		     }
		  		}
		  		
		  		?>
		  		
		
	</div>
    
    
    <script>
		$("#message").keypress(function(e) {
			if (e.which == 13) {
				addComment();
			}
		});

		$(function() {
			$("#message").focus();
		});

    </script>