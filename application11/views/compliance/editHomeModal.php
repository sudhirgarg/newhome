<?php
$isNew = true;
if ($this->uri->segment ( 3 ) > 0) {
	$isNew = false;
	$home = $this->comp->getHome ( $this->uri->segment ( 3 ) );
}
?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"
		aria-hidden="true">&times;</button>
	<h4 class="modal-title"><?php if($isNew==false){echo 'Group Detail';}else{echo 'New Group';}?></h4>
</div>
<div class="modal-body">
	<div class="form-horizontal" role="form">
		<input id="homeID" type="hidden"
			value="<?php if($isNew==false){echo $home->homeID;}else{ echo '-1';}?>">
		<div class="form-group">
			<label class="col-sm-3 control-label">Home Name</label>
			<div class="col-sm-9">
				<input type="text" id="homeName" class="form-control"
					value="<?php if($isNew==false){echo $home->homeName; }else {echo '';}?>">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label">Address</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" id="homeAddress"
					placeholder="Text input"
					value="<?php if($isNew==false){ echo $home->homeAddress;}?>"
					>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label">Contact</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" id="contact"
					placeholder="Text input"
					value="<?php if($isNew==false){ echo $home->contact;}?>">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label">Phone</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" id="phone"
					placeholder="Text input"
					value="<?php if($isNew==false){ echo $home->phone;}?>">
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal"
			id="closeButtonModal">Close</button>
		<button type="button" class="btn btn-primary" id="saveButton"
			onclick="saveHome()" data-loading-text="Saving...">Save changes</button>
	</div>