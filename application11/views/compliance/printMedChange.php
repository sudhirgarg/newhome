<?php

    $isNew = true;
	if ($this->uri->segment ( 4 ) > 0) {
		$isNew = false;		
	}

?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Med change report | Seamless Care Pharmacy</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/cloud-admin.css">
	</head>
	
    <body onload="window.print()">
	
	<section id="page">
	
		<div class="center">
			<img src="<?php echo base_url();?>img/logo/logo.png" alt="Seamless Care Pharmacy" height="50px" width="300px">
				<br><br><br><br>
		</div>

		<div class="form-horizontal" role="form">

			<div class="box border purple center">
			
				<div class="box-title">
					<h4>
					    <?php if($isNew==false){echo 'Med Change Detail';}else{echo 'New Med Change' . 'ChangeID:'.$medChangeID . 'pat:'.$patID;}?> 
						<?php  echo '<span class="label label-primary">'.$patient -> LastName .' '.$patient->FirstName. '-' . $patient ->ID.'</span>' ;?>
				    </h4>
					<div class="tools hidden-xs">
						<button href="#box-config" onClick="window.print()" data-toggle="modal" class="config"> <i class="fa fa-print"> </i> </button>
					</div>
				</div>
				
				<div class="box-body big">
				
					<div class="row">
						<div class="col-xs-2">
							<div placeholder="">Pharmacist</div>
						</div>
						<div class="col-xs-3">
							<?php $pharm = $this->ion_auth->user($medChange->pharmacistUserID)->row(); echo $pharm->first_name;?>
						</div>
						<div class="col-xs-2">
							<div placeholder="">Technician</div>
						</div>
						<div class="col-xs-3">
							<?php $tech = $this->ion_auth->user($medChange->technicianUserID)->row(); echo $tech->first_name;?>
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-2">
							<div placeholder="">Date Recorded</div>
						</div>
						<div class="col-xs-3">
							<?php echo $medChange->dateOfChange; ?>
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-2">
							<div placeholder="">Start Date</div>
						</div>
						<div class="col-xs-3">
							<?php if($isNew==false){ echo $medChange->startDate;}?>
						</div>
						<div class="col-xs-2">
							<div placeholder="">End Date</div>
						</div>
						<div class="col-xs-3">
							<?php if($isNew==false){ echo $medChange->endDate;}?>
						</div>
					</div>
					
				</div>
			</div>
			

			<div class="box border purple center">
				<div class="box-title"> <h4>Change</h4> </div>
				<div class="box-body big">
					<div class="col-lg-8">
						<div><?php if($isNew==false){ echo $medChange->changeText;}?></div>						
					</div>
				</div>
			</div>

			<div class="box border purple center">
				<div class="box-title">
					<h4>Action</h4>
				</div>
				<div class="box-body big">
					<div class="row">
						<div class="col-lg-8">
							<div><?php if($isNew==false){ echo $medChange->actionText;}?></div>
							<p></p>
						</div>
					</div>
				</div>
			</div>
			
			<div class="box border purple center">
				<div class="box-title"> <h4> Details </h4> </div>
				<div class="box-body big">
					<div class="col-lg-8">
						<div>
							<?php
								echo ($medChange->spares == 1)? " Spares : YES " : " Spares : NO " ;
								echo "<br/>";
								echo ($medChange->mar == 1)? " Mar : YES " : " Mar : NO " ;
								echo "<br/>";
								echo ($medChange->numOfStripts > 0)? " Num of stripts :  $medChange->numOfStripts " : " Num of stripts : 0 " ;
								echo "<br/>";
								
							
							?>
						</div>						
					</div>
				</div>
			</div>
	
	</section>
	<!--/PAGE -->
	


</body>
</html>
