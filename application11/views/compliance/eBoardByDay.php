<?php
     $ci =& get_instance();
?>


<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->


<div class="clearfix">
	<h3 class="content-title pull-left"> E - Board </h3>
</div>
<div class="description">
	
	<table>
		<tr>
			<!--td>
				<button class="btn btn-lg btn-danger"
					onclick="loadModal('<?php echo base_url() . 'compliance/editTicketModal/'; ?>')">
					<i class="fa fa-plus"></i> New Internal Ticket
				</button>
			</td-->
						
		</tr>
	</table>
	
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<div class="row" ng-app="myApp" ng-controller="DoubleController">

<div class="row">
	 <div class="col-lg-12" >
		<div class="panel panel-danger">
		    <div class="panel-heading">
		    	<a href="<?php echo base_url();?>compliance/eBoard"> <button class="btn btn-success"> <i class="fa fa-angle-double-left"> </i>  <i class="fa fa-tachometer" style="font-size:24px;color:red"></i> <b>Dashboard</b>  </button></a>
		    	<button class="btn btn-info" ng-click="goToWeekDay('MON')";> Monday </button>
		    	<button class="btn btn-info" ng-click="goToWeekDay('TUE')";> Tuesday </button>
		    	<button class="btn btn-info" ng-click="goToWeekDay('WED')";> Wednesday </button>
		    	<button class="btn btn-info" ng-click="goToWeekDay('THU')";> Thursday </button>
		    	<button class="btn btn-info" ng-click="goToWeekDay('FRI')";> Friday </button>
		    	<button class="btn btn-info" ng-click="goToWeekDay('SAT')";> Saturday </button>
		    	<button class="btn btn-info" ng-click="goToWeekDay('SUN')";> Sunday </button>
		    </div>
	    </div>
	 </div>   
</div>

<div class="row">
	 <div class="col-lg-6">
		<div class="panel panel-info">
		    <div class="panel-heading"> Up coming Med Changes for TODAY   </div>
		    
		    <div class="panel-body">
		          <ul class="list-group">
		          <?php 
		          		foreach($upComingMedChange AS $record){
		          			echo '<li class="list-group-item list-group-item-warning"> 
									<i class="fa fa-home" aria-hidden="true"></i>
									'; $ci->getHomeInfo($record->NHWardID);  echo '  => <i class="fa fa-user" aria-hidden="true"></i> '.$record->LastName.' '.$record->FirstName.' '.$record-> endDate.'</li>';
		          			
		          		}
		          ?>
		          </ul>
		    </div>
		    
	    </div>
	 </div>  
	
	 
	  <div class="col-lg-6">
		<div class="panel panel-info">
		    <div class="panel-heading"> Up coming Tickets for TODAY   </div>
		    <div class="panel-body">
		          <ul class="list-group">
		          <?php 
		          		foreach($currentTicket AS $rec){
		          			echo '<li class="list-group-item list-group-item-warning">
									<i class="fa fa-home" aria-hidden="true"></i>
									'; $ci->getHomeInfo($rec->NHWardID);  echo '  => <i class="fa fa-user" aria-hidden="true"></i> '.$rec->LastName.' '.$rec->FirstName.' '.date('Y-m-d',strtotime($rec-> dateOfDelivery)).'</li>';		          			
		          		}
		          ?>
		          </ul>
		    </div>
	    </div>
	 </div>   
</div>

<div class="row" >
    
    <!-- div class="col-lg-6">
		<div class="panel panel-danger">
		    <div class="panel-heading">Mini Board <span class="badge" ng-if="totalError">{{totalError - 1 }}</span> </div>
		    <div class="panel-body">
		    	 		    	
		    	 <ul class="list-group" ng-show="homePatient">
				   
					    <li class="list-group-item list-group-item-success" ng-repeat-start="getErrorDetailsById in getErrorDetailsByIds"  ng-if = "getErrorDetailsById.Name"> <span class="badge" ng-if="totalError > 2">{{ $index + 1 }}</span>  Name :  {{ getErrorDetailsById.Name }} </li>
					    <li class="list-group-item list-group-item-info" ng-if = "getErrorDetailsById.dateOfDelivery" > Delivery Date      : {{ getErrorDetailsById.dateOfDelivery }}          </li>
					    <li class="list-group-item list-group-item-info" ng-if = "getErrorDetailsById.actionText" > Action Text      : {{ getErrorDetailsById.actionText }}          </li>
					    <li class="list-group-item list-group-item-info" ng-if = "getErrorDetailsById.changeText" > Change Text      : {{ getErrorDetailsById.changeText }}          </li>
					    <li class="list-group-item list-group-item-info" ng-if = "getErrorDetailsById.spares"     > Spares           : Yes              </li>
					    <li class="list-group-item list-group-item-info" ng-if = "getErrorDetailsById.mar"        > Mar              : Yes              </li>
					    <li class="list-group-item list-group-item-info" ng-if = "getErrorDetailsById.extraLabels"> Extra Labels     : Yes              </li>
					    <li class="list-group-item list-group-item-info" ng-if = "getErrorDetailsById.numOfStripts"> Number of Strips : {{ getErrorDetailsById.numOfStripts }}        </li>
					    <li class="list-group-item list-group-item-info" ng-if = "getErrorDetailsById.Description"> Description      : {{ getErrorDetailsById.Description }}         </li>
					    <li class="list-group-item list-group-item-warning" ng-if = "getErrorDetailsById.id" > <button class="btn btn-success" ng-click="complteAllEboard(getErrorDetailsById.id)";> Complete <span class="badge" ng-if="totalError > 2">{{ $index + 1 }}</span> </button> </li>
					    <li class="list-group-item list-group-item-warning" ng-if = "totalError > 2 && getErrorDetailsById.errorIdList" > <button class="btn btn-warning" ng-click="complteAllEboard(getErrorDetailsById.errorIdList)";> Complete All  </button> </li>
					    <li class="list-group-item list-group-item-defult" ng-repeat-end>  </li>
					    
				  </ul>
				  
				  <ul class="list-group" ng-show="individualPat">
				   
					    <li class="list-group-item list-group-item-success" ng-repeat-start="getErrorDetailsByIdEboardId in getErrorDetailsByIdEboardIds"  ng-if = "getErrorDetailsByIdEboardId.Name"> <span class="badge" ng-if="totalErrorEboard > 2">{{ $index + 1 }}</span>  Name :  {{ getErrorDetailsByIdEboardId.Name }} </li>
					    <li class="list-group-item list-group-item-info" ng-if = "getErrorDetailsByIdEboardId.dateOfDelivery" > Delivery Date      : {{ getErrorDetailsByIdEboardId.dateOfDelivery }}          </li>
					    <li class="list-group-item list-group-item-info" ng-if = "getErrorDetailsByIdEboardId.actionText" > Action Text      : {{ getErrorDetailsByIdEboardId.actionText }}          </li>
					    <li class="list-group-item list-group-item-info" ng-if = "getErrorDetailsByIdEboardId.changeText" > Change Text      : {{ getErrorDetailsByIdEboardId.changeText }}          </li>
					    <li class="list-group-item list-group-item-info" ng-if = "getErrorDetailsByIdEboardId.spares"     > Spares           : Yes              </li>
					    <li class="list-group-item list-group-item-info" ng-if = "getErrorDetailsByIdEboardId.mar"        > Mar              : Yes              </li>
					    <li class="list-group-item list-group-item-info" ng-if = "getErrorDetailsByIdEboardId.extraLabels"> Extra Labels     : Yes              </li>
					    <li class="list-group-item list-group-item-info" ng-if = "getErrorDetailsByIdEboardId.numOfStripts"> Number of Strips : {{ getErrorDetailsByIdEboardId.numOfStripts }}        </li>
					    <li class="list-group-item list-group-item-info" ng-if = "getErrorDetailsByIdEboardId.Description"> Description      : {{ getErrorDetailsByIdEboardId.Description }}         </li>
					    <li class="list-group-item list-group-item-warning" ng-if = "getErrorDetailsByIdEboardId.id" > <button class="btn btn-success" ng-click="complteAllEboard(getErrorDetailsByIdEboardId.id)";> Complete <span class="badge" ng-if="totalErrorEboard > 2">{{ $index + 1 }}</span> </button> </li>
					    <li class="list-group-item list-group-item-warning" ng-if = "totalErrorEboard > 2 && getErrorDetailsByIdEboardId.errorIdList" > <button class="btn btn-warning" ng-click="complteAllEboard(getErrorDetailsByIdEboardId.errorIdList)";> Complete All  </button> </li>
					    <li class="list-group-item list-group-item-defult" ng-repeat-end>  </li>
				    
				  </ul>
		    	 
		    	<br/><br/>
		    			    	 
		    	
		    	
		    </div>
		    <div class="panel-footer"> </div>
	 	</div>
	</div-->
	
	<div class="col-lg-6">
		<div class="panel panel-primary">
		    <div class="panel-heading"> <?php echo $day; ?> </div>
		    <div class="panel-body">
		    	
		    	<ul class="list-group">	    	
		    	<?php
		    	      $i = 0;
					  $day = $day;
					  $type = 1;
		    	      foreach($EboardHome AS $record){
		    	      	
		    	      	    $deliveryDate = date("l", strtotime($record->dateOfDelivery));	
						  	
							if($day == $deliveryDate){
								$ci->getHomeInfoForEboard($record->NHID, $record->NHWardID, $record->dateOfDelivery,$type);	
							}			  
		    	      }		    	
		    	?>		    	
		    	</ul>
		    	
		   </div>
		    <div class="panel-footer"> 
		    	
		    	<ul class="list-group">	    	
		    	<?php
		    	      $i = 0;
					  $day = $day;
					  $type = 2;
		    	      foreach($medChange AS $mRecord){
		    	      	
		    	      	    $deliveryDate = date("l", strtotime($mRecord->dateOfDelivery));	
						  	
							if($day == $deliveryDate){
								//$ci->getHomeInfoForEboard($mRecord->NHID, $mRecord->NHWardID, $mRecord->dateOfDelivery,$type);	
							}			  
		    	      }		    	
		    	?>		    	
		    	</ul>
		    		
		    </div>
	 	</div>
	</div>
	
	
	
		
</div>


</div>
<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>
