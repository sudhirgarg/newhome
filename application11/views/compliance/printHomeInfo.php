<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>iApotheca | Seamless Care Pharmacy</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>css/cloud-admin.css">
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>css/themes/default.css"
	id="skin-switcher">
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>css/responsive.css">
<!-- STYLESHEETS -->
<!--[if lt IE 9]><script src="js/flot/excanvas.min.js"></script><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
<!-- ANIMATE -->
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>css/animatecss/animate.min.css" />
<!-- DATE RANGE PICKER -->
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
<!-- DATA TABLES -->
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>js/datatables/media/css/jquery.dataTables.min.css" />
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>js/datatables/media/assets/css/datatables.min.css" />
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>js/datatables/extras/TableTools/media/css/TableTools.min.css" />

<!-- TABLE CLOTH -->
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>js/tablecloth/css/tablecloth.min.css" />
<!-- TODO -->
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>js/jquery-todo/css/styles.css" />
<!-- FULL CALENDAR -->
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>js/fullcalendar/fullcalendar.min.css" />
<!-- GRITTER -->
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>js/gritter/css/jquery.gritter.css" />
<!-- XCHARTS -->
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>js/xcharts/xcharts.min.css" />

<!-- DATE PICKER -->
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>js/datepicker/themes/default.min.css" />
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>js/datepicker/themes/default.date.min.css" />
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>js/datepicker/themes/default.time.min.css" />


<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>css/custom.css" />

<!-- FONTS -->
<link
	href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'
	rel='stylesheet' type='text/css'>
<link
	href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css"
	rel="stylesheet">
</head>
<body>
	<!-- HEADER -->

	<!-- PAGE -->
	<section id="page">
		
	
<?php $group = $this->comp->getHome($this->uri->segment(3));?>

<div class="center">
			<img src="<?php echo base_url();?>img/seamlogo.gif"
				alt="Seamless Care Pharmacy" height="50px" width="300px"> <br> <br>
		</div>

		<div class="form-horizontal" role="form"></div>
		<h1><?php echo $group->homeName;?></h1>
		<div class="box border purple center">
			<table id="datatable1" cellpadding="0" cellspacing="0" border="0"
				class="datatable table table-striped table-bordered table-hover">
				<thead>
					<tr>
						<th><h2 class="center">Client Code</h2></th>
						<th><h2 class="center">Client Name</h2></th>
					</tr>
				</thead>
				<tbody>
					<?php
					$patients = $this->comp->getPatHomes ( '', $group->homeID );
					if ($patients != false) { // Often times I'll return false in a model if no results are found
						foreach ( $patients->result () as $patient ) {
							$patHomeID = $this->comp->getPatHomes ( $patient->patID, '0' );
							$patHome = $this->comp->getHome ( $patHomeID );
							?>
						<tr
						onclick="window.location='<?php echo base_url("compliance/viewPat/". $patient->patID . "/profile");?>'">
						<td>
							<h4><?php echo $patient->patInitials . $patient->patID ;?></h4>
						</td>
						<td col-span="3"></td>
					</tr>
<?php }} else { ?><tr>
						<td>No Records</td>
					</tr><?php  }?>
					</tbody>
			</table>
		</div>

		<div class="box border purple center">End</div>

	</section>
	<!--/PAGE -->
	<!-- JAVASCRIPTS -->
	<!-- Placed at the end of the document so the pages load faster -->
	<!-- JQUERY -->
	<script src="<?php echo base_url();?>js/jquery/jquery-2.0.3.min.js"></script>
	<!-- JQUERY UI-->
	<script
		src="<?php echo base_url();?>js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
	<!-- BOOTSTRAP -->
	<script src="<?php echo base_url();?>js/bootstrap.min.js"></script>

	<!-- Data Tables-->
	<script
		src="<?php echo base_url();?>js/datatables/media/js/jquery.dataTables.min.js"></script>


	<!-- FLOT CHARTS -->
	<script src=<?php echo base_url("js/flot/jquery.flot.min.js");?>></script>
	<script src=<?php echo base_url("js/flot/jquery.flot.time.min.js");?>></script>
	<script
		src=<?php echo base_url("js/flot/jquery.flot.selection.min.js");?>></script>
	<script src=<?php echo base_url("js/flot/jquery.flot.resize.min.js");?>></script>
	<script src=<?php echo base_url("js/flot/jquery.flot.pie.min.js");?>></script>
	<script src=<?php echo base_url("js/flot/jquery.flot.stack.min.js");?>></script>
	<script
		src=<?php echo base_url("js/flot/jquery.flot.crosshair.min.js");?>></script>

	<!--  FOR STACKED AREA CHARTS -->
	<script type="text/javascript"
		src=<?php echo base_url("/js/jquery-1.8.3.min.js");?>></script>
	<script type="text/javascript"
		src=<?php echo base_url("/js/flot/jquery.flot.js");?>></script>
	<script type="text/javascript"
		src=<?php echo base_url("/js/flot/jquery.flot.axislabels.js");?>></script>
	<script type="text/javascript"
		src=<?php echo base_url("/js/flot/jquery.flot.stack.js");?>></script>
	<script type="text/javascript"
		src=<?php echo base_url("/js/flot/jshashtable-2.1.js");?>></script>
	<script type="text/javascript"
		src=<?php echo base_url("/js/flot/jquery.numberformatter-1.2.3.min.js");?>></script>
	<script type="text/javascript"
		src=<?php echo base_url("js/flot/jquery.flot.time.js");?>></script>
	<!--   -------------------- -->

	<!-- DATE RANGE PICKER -->
	<script
		src="<?php echo base_url();?>js/bootstrap-daterangepicker/moment.min.js"></script>
	<script
		src="<?php echo base_url();?>js/bootstrap-daterangepicker/daterangepicker.min.js"></script>
	<!-- DATE PICKER -->
	<script type="text/javascript"
		src="<?php echo base_url()?>js/datepicker/picker.js"></script>
	<script type="text/javascript"
		src="<?php echo base_url()?>js/datepicker/picker.date.js"></script>
	<script type="text/javascript"
		src="<?php echo base_url()?>js/datepicker/picker.time.js"></script>
	<!-- FULL CALENDAR -->
	<script type="text/javascript"
		src="<?php echo base_url()?>js/fullcalendar/fullcalendar.min.js"></script>
	<!-- SLIMSCROLL -->
	<script type="text/javascript"
		src="<?php echo base_url();?>js/jQuery-slimScroll-1.3.0/jquery.slimscroll.min.js"></script>
	<script type="text/javascript"
		src="<?php echo base_url();?>js/jQuery-slimScroll-1.3.0/slimScrollHorizontal.min.js"></script>
	<!-- COOKIE -->
	<script type="text/javascript"
		src="<?php echo base_url();?>js/jQuery-Cookie/jquery.cookie.min.js"></script>
	<!-- CUSTOM SCRIPT -->
	<script src="<?php echo base_url();?>js/script.js"></script>
	<script>
		jQuery(document).ready(function() {		
			window.print();
		});
	</script>
<?php $this->load->view('extras'); ?>
<script>

		function searchPat()
		{
			var patID = $("#patSearch").val();
			window.location.href = "<?php echo base_url();?>compliance/viewPat/" + patID + "/profile";
		}
		
		function globalSuccess(successMessage)
		{
			$("#successMessageContent").html(successMessage);
			$("#successMessage").fadeIn().delay(3500).fadeOut();
		}

		function globalError(errorMessage)
		{
			$("#errorMessageContent").html(errorMessage);
			$("#errorMessage").fadeIn().delay(3500).fadeOut();
		}

		function loadModal(ajaxURL)
		{
			$.get(ajaxURL, function(data) {
				$("#myModalContent").html(data);
				$('#myModal').modal();
			});
		}

		function read(data)
		{
			var args = data.split('|');
			if (args[0] == 0)
			{
				globalError(args[1]);
			} else {
				globalSuccess(args[1]);
			}
		}
	</script>
	<!-- /JAVASCRIPTS -->
<?php $this->load->view('js/footer');?>

</body>
</html>
