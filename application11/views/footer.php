<!-- BEGIN FOOTER -->
                <p class="copyright"> 2017 &copy; Seamless Care
                    <!--<a target="_blank" href="http://keenthemes.com">Keenthemes</a> &nbsp;|&nbsp;
                    <a href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" title="Purchase Metronic just for 27$ and get lifetime updates for free" target="_blank">Purchase Metronic!</a>-->
                </p>
                <a href="#index" class="go2top">
                    <i class="icon-arrow-up"></i>
                </a>
                <!-- END FOOTER -->
            </div>
</div>
<!--/PAGE -->
<!-- JAVASCRIPTS -->
<!-- Placed at the end of the document so the pages load faster -->
<!-- JQUERY -->

<script src="<?php echo base_url(); ?>js/jquery/jquery-2.0.3.min.js"></script>

<script src="<?php echo base_url(); ?>js/script.js"></script>



<!-- New Design -->

<!-- BEGIN CORE PLUGINS -->
     
        <script src="<?php echo base_url(); ?>js_new/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/js.cookie.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?php echo base_url(); ?>js_new/moment.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/morris/morris.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/morris/raphael-min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/counterup/jquery.counterup.min.js" type="text/javascript"></script>
        
        <script src="<?php echo base_url(); ?>js_new/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/horizontal-timeline/horizontal-timeline.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/flot/jquery.flot.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/jquery.sparkline.min.js" type="text/javascript"></script>
	
        <script src="<?php echo base_url(); ?>css_new/select2/js/select2.full.min.js" type="text/javascript"></script>
              


        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?php echo base_url(); ?>js_new/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <!--<script src="<?php echo base_url(); ?>js_new/dashboard.min.js" type="text/javascript"></script>-->
	<script src="<?php echo base_url(); ?>js_new/bootstrap-table/bootstrap-table.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>js_new/table-datatables-responsive.min.js" type="text/javascript"></script>
	
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="<?php echo base_url(); ?>js_new/layout.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/quick-nav.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS --> 
	<script src="<?php echo base_url(); ?>js/raterater.jquery.js"></script> 


<!-- New Design End -->
	<script>
		/* Here we initialize raterater on our rating boxes
	 */
	$(function() {
	    $( '.ratebox' ).raterater( { 
	        submitFunction: 'rateAlert', 
	        allowChange: false,
	        starWidth: 20,
	        spaceWidth: 3,
	        numStars: 5
	    } );
	    
	   
		
		});
	</script>
	
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular.js"></script>
	<script src="<?php echo base_url(); ?>js/angularHomeportal.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/dirPagination.js"></script>
     	<script src="<?php echo base_url(); ?>js_new/todo.min.js" type="text/javascript"></script>	
	<script src="<?php echo base_url(); ?>js_new/components-select2.min.js" type="text/javascript"></script>
	<!-- DATE PICKER -->
	<script type="text/javascript" src="<?php echo base_url()?>js/datepicker/picker.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>js/datepicker/picker.date.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>js/datepicker/picker.time.js"></script>
	
	<!-- DATE TIME PICKER -->
	<script type="text/javascript" src="<?php echo base_url()?>js/datepicker/bootstrap-datetimepicker.min.js"></script>
	<script src="<?php echo base_url(); ?>js/datatables/media/js/jquery.dataTables.min.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jQuery-slimScroll-1.3.0/jquery.slimscroll.min.js"></script>
    <script src="<?php echo base_url(); ?>js/jquery/jquery.sparkline.min.js"></script>
    
		<script type="text/javascript" src="<?php echo base_url(); ?>js/chosen.jquery.js"></script>
        
	
	
 	
<script>
	jQuery(document).ready(function() {
		
		$('[data-toggle="tooltip"]').tooltip();   
		
		$('#sample_1').dataTable();
		$('#sample_2').dataTable();

		$(".timepicker-fullscreen").pickatime({
			interval: 60
		});

		$(".datepicker-fullscreen").pickadate();

		$('.sparklines').sparkline('html', {
			enableTagOptions : true,
			"barWidth" : 15,
			"height" : 50
		});
		
		$('.inlinebar').sparkline('html', {
			type : 'bar',
			barColor : 'red'
		});
		
		App.setPage("widgets_box");
		//Set current page
		//App.init();
		//Initialise plugins and elements
		
				
		//$('#datatable2').dataTable(); 			
		//$('#medchanges1').dataTable();
		//$('#dtTickets').dataTable();
		
		 $('tbody').on("click", "button[name=edit]", function (){			
            $('#myModal').modal('toggle');
        });

		 
		 $(".surveyQuestionAnswer").submit(function(e){

			 var formData  = $(this).serialize();
			 var buttonCon = $(this).find('button[type=submit]'); 
			 buttonCon.html('Progressing....');

			 $.ajax({
				 
                   url : "<?php echo site_url('compliance/addSurveyQuestionAnswer'); ?>",
                   type : "POST",
                   data : formData
                   
			 }).done(function(data){	
				 			 
				   setTimeout(function() { location.reload(); }, 500);				
			 });
			  	 
             e.preventDefault();
              
	     });
	     

		 $(".deliveryDate").submit(function(e){
             
             var form_data = $(this).serialize();
             var button_content = $(this).find('button[type=submit]');
             var buttonVal      = button_content.val();
             button_content.html('<i class="fa fa-spinner" aria-hidden="true"></i>'); 
                       
           

             $.ajax({ 
					
                 url: "<?php echo site_url('compliance/updateDeliveryDate');?>",
                 type: "POST",	                   
                 data: form_data
                     
             }).done(function(data){
                 	
            	 read(data);			       
			     setTimeout(function() { location.reload(); }, 1000);	                
              	                   
             });
             
             e.preventDefault();
             
         });    
		
	});
	
	var config = {
	    	
	         '.chosen-select'           : {},
	}
	    
	for (var selector in config) {
	    	
	         jQuery(selector).chosen(config[selector]);
	      
	}
	

    function reSetPAW(){
    	$.post('<?php echo base_url().'compliance/reSetPawTech';?>', {
    		 id : $('#id').val(),
    		 reSetPassword : $('#reSetPassword').val(),
    		 reSetPasswordCon : $('#reSetPasswordCon').val()
    	},function(data){
    		alert(data);
    		$('#reSetPassword').val('');
    		$('#reSetPasswordCon').val('');
    	});
    	    	
    }

	function searchPat(){
		var patID = $("#patSearch").val();
		window.location.href = "<?php echo base_url(); ?>compliance / viewPat / " + patID + " / profile";
	}

	function globalSuccess(successMessage){
		$("#successMessageContent").html(successMessage);
		$("#successMessage").fadeIn().delay(3500).fadeOut();
	}

	function globalError(errorMessage){
		$("#errorMessageContent").html(errorMessage);
		$("#errorMessage").fadeIn().delay(3500).fadeOut();
	}

	function loadModal(ajaxURL){
		$.get(ajaxURL, function(data) {
			$("#myModalContent").html(data);
			$('#myModal').modal();
		});
	}

	function GoCloud(){
		 var patId = $("#patId").val();
		 if(patId > 0){
			 window.location.href = "<?php echo base_url(); ?>compliance/quickPatView/" + patId + "/reorders";
		 }else{
             alert('Choose a patient');
		 }
		 
	}
	

	function GoLocal(){
		 var patId = $("#patId").val();
		 if(patId > 0){
			 window.location.href = "<?php echo base_url(); ?>compliance/localPatProfile/" + patId;
		 }else{
			 alert('Choose a patient');
		 }
	}
	

	function read(data){
		var args = data.split('|');
		if (args[0] == 0){
			globalError(args[1]);
		} else {
			globalSuccess(args[1]);
		}
	}
	
	

	function showResult(data){
		if (data.length > 3){
			var full = data.split(",");
			var firstName = full[1];
			var lastName = full[0];
	
			$("#patientResults").text("Search Results:");
			$.post('<?php echo site_url('compliance/getPatientResults'); ?>',{
				fullName: $("#patFuzySearch").val()
			},function(data, status){
				$("#patientResults").html(data);
			});
		}
	}
	
	function getPatFromApi(){
		
		 $.post('<?php echo site_url('compliance/getPatDetailsBySearch'); ?>',{
		 	  info : $("#patFuzySearch").val()
		 },function(data, status){
		 	 $("#patientResults").html(data);
		 	  
		 });
		
	}
	
	
		
		function reOrderPRNsNew(){
			updateRx();
			updateTextArea();				      		
	      	   if(confirm("Please confirm to reorder the following prescription numbers: " + $("#reOrderBox").val())) {
			    $.post('<?php echo site_url('compliance/createReOrder');?>', {
		        	patID: $("#patID").val(),
		      		rxs: $("#reOrderBox").val(),
		      		rxnumList : $("#reOrderRx").val()
			      		
			    }, function (data) {
			        read(data);			       
			        setTimeout(function() { location.reload(); }, 1000);
			    });
			    	
				}else{
					//do nothing
				}	
		}

		 
		
		function updateTextArea() {         
		    var allVals = [];
		    $('#reOrderList :checked').each(function() {
		      allVals.push("| Rx# " + $(this).val() + " comment: " + $(this).closest('tr').find('.reOrderCommentClass').val() );		     
		      
		    });
		    $('#reOrderBox').val(allVals);		    		   
		}
		
		function updateRx(){
			var favorite = [];
			$.each($("input[name='reOrderCheckBox']:checked"), function(){            
                favorite.push($(this).val());
            });
		    $('#reOrderRx').val(favorite.join(","));
		    
		 }
		 

		

		function updateEboardId(){
			var allEboardId = [];
			$.each($("input[name='edoardId']:checked"), function(){            
				allEboardId.push($(this).val());
            });
		    $('#eboardIdList').val(allEboardId.join(","));
		    
		 }

		function updateEboard(){            			
			updateEboardId();
			
               $("#updateEboard").html("Loading...");
		
			    $.post('<?php echo site_url('compliance/updateCheckItems');?>', {
		        	eBoardId: $("#eboardIdList").val(),
		        	eboardAllIdList : $("#eboardAllIdList").val()
			      		
			    }, function (data) {				   
			         read(data);			       
			         setTimeout(function() { location.reload(); }, 1000);
			    });
			    			 
		}

		function printSurvey(){
			var info = $("#roles").val();

			var myURL = "<?php echo base_url(); ?>compliance/printSurvey/" + info;          
            window.open(myURL, "", "width=950, height=1000"); 
		}

		function closeSurvey(){
			$("#closeS").html("Loading....");
			
     		$.post('<?php echo site_url('compliance/closeSurvey');?>', {
	        	
	        	roleId : $("#roles").val()
		      		
		    }, function (data) {				   
		         read(data);	
		         $("#closeS").html("Close survey");		       
		         
		    });
		    
    	}
    	
    	

		function completeEboard(){            			
			updateEboardId();
			
			   $("#comEboard").html("Loading...");
		
			    $.post('<?php echo site_url('compliance/completeEboardTwo');?>', {
		        	eBoardId: $("#eboardIdList").val(),
		        	eboardAllIdList : $("#eboardAllIdList").val()
			      		
			    }, function (data) {				   
			         read(data);			       
			         setTimeout(function() { location.reload(); }, 1000);
			    });
			    			 
		}

		function delEboardIdList(){
			var allDelEboardId = [];
			$.each($("input[name='delEdoardId']:checked"), function(){            
				allDelEboardId.push($(this).val());
            });
		    $('#delEboardIdList').val(allDelEboardId.join(", "));
		}

		
		function deleteEboard(){
			delEboardIdList();
			var d = $("#delEboardIdList").val();
            if(d){ 
            	 if(confirm("Are you sure?")){

            		 $("#delEboard").html("Del...");
            			
            		 $.post('<?php echo site_url('compliance/deleteEboard');?>', {
                		      		        	
      		        	eboardDelIdList : d
      			      		
      			    }, function (data) {				   
      			         read(data);			       
      			         setTimeout(function() { location.reload(); }, 1000);
      			    });
            		 
            	 }else{
            		 alert('OUT');
            	 }                
            }else{
            	alert('Select atleast one ITEM');
            }

		}

		function printDeliverySheet(){         	
			
			$.post('<?php echo base_url();?>compliance/printDeliverySheetWithIternary',{
				patId : $("#patId").val()
			},function(data){
				if(data > 0){
					var myURL = "<?php echo base_url(); ?>compliance/printOrderSheet/" + data;          
	                window.open(myURL, "", "width=950, height=1000");  
				}else{					
					read(data);
				}
			});           
    	}

    	
		
		function saveEmailCredential(){
			var med;
			var ticketToken;
			var commentToken;
			
			if($("#medChange").prop('checked') == true)  { med          = $("#medChange").val(); }
			if($("#ticket").prop('checked')    == true)  { ticketToken  = $("#ticket").val();    }
			if($("#comment").prop('checked')   == true)  { commentToken = $("#comment").val();   }
			
					
	        $.post('<?php echo site_url("homePortalEdu/setEmailCredential") ?>',{
	            userId     : $("#userIDEmail").val(),
	            medChange  : med,
	            ticket     : ticketToken,
	            comment    : commentToken           
	            
	        },function(data){
	        	              
	            $("#credentialAlert").text(data);  
	            setTimeout('autoRefresh1()', 1000);               
	            
	        });
                
	    }
	
	
		function autoRefresh1(){
	         window.location.reload();
	    }

		function goBack() {  window.history.back();  }  

		function getDayToPrint(){
			var info = $("#dayPrint").val();
           
            var myURL = "<?php echo base_url(); ?>setting/getDeliveryTimeAndHomeOrPatInfoBaseOnDay/" + info;          
            window.open(myURL, "", "width=950, height=1000");  
		}

</script>

	<?php 
		 
		 $page = $this->uri->segment ( 2 );
		 $page1 = $this->uri->segment ( 1 );
		
		if(($page =='ticketsDashRefresh') && $page1=='compliance' ){		
			
				echo "<script>
					function autoRefresh1(){
					   window.location.reload();
					}
					setInterval('autoRefresh1()', 9000);				
				</script>";	 
			 
		}	
			 
		
		if(($page =='viewpat')      && $page1=='compliance' ) {	$this->load->view('js/viewPatient'); 	}	
		if(($page =='quickPatView') && $page1=='compliance' ) {	$this->load->view('js/viewPatient'); 	}
		
		if($page1 == ""){ $this->load->view('js/medchanges'); }
		
		
		
		$this -> load -> view('extras');
		
		$this -> load -> view('js/footer');
		
		
		if($page =='Error') { 
			$ci = & get_instance();
	 ?> 
   
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.3/angular.js"></script>
	<script src="<?php echo base_url(); ?>js/highcharts.js"></script>
    <script src="<?php echo base_url(); ?>js/chart.js"></script>    
    
    <script>

	    function MainCtrl($scope, $http){
			
			var data = {"xData": ["Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],"yData":[
			      			<?php   $ci->getErrorInfoForChart(); ?>
						]
					}
			
			$scope.lineChartYData=data.yData
			$scope.lineChartXData=data.xData
		};
		
		angular.module('AngularChartExample',['AngularChart'], function( $routeProvider, $locationProvider ){
			$routeProvider.when('/',{
				template: '<chart title="Error" xData="lineChartXData" yData="lineChartYData" xName="Month" yName="Amount" subtitle="Monthly report"></chart>',
				controller: MainCtrl
			});	
		});
		
       
	    
  </script>
  
      
    <?php  } // error page end ?>

<script>
jQuery("#nhId").select2();
jQuery("#patId").select2();
</script>
</body>
</html>
