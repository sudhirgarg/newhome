

<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left">Quizzes</h3>
</div>
<div class="description">
	
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DELIVERY RUNS CONTENT -->
<!-- DATA TABLES -->

<!--***************************************** PAGE BODY STARTS ***************************************************************-->

<div class="row">
	
	<div class="col-md-6">
		<div class="panel panel-default">
  		<div class="panel-body">
  	
		<form action="<?php echo base_url()?>homePortalEdu/addQuizzesGeneralInfo" class="register" method="POST">
            <h1>Create Quizzes</h1>
			<fieldset class="row1">
                <legend>Information</legend>
                <?php if(validation_errors()) { ?>
			        		
		        	 <div class="alert alert-warning" role="alert" id="wrapData" >
	          			 <a href="#" class="close" data-dismiss="alert">&times;</a>		  	
					  	 <div id="formChange"><?php echo validation_errors(); ?></div>
					 </div>
				 
				<?php }else	{
							
							if (!empty($s)){
							echo '
							 <div class="alert alert-warning" role="alert" id="wrapData" >
	              				 <a href="#" class="close" data-dismiss="alert">&times;</a>		  	
						  		 <div id="formChange">'.$s.'</div>
							 </div>
							';
							}
						}
				 ?>  
				<p>
					<label>Quiz Title</label> 
                    <input  type="text" name="quizeTitle"  class="form-control" placeholder="Enter Quiz title" id="quizeTitle" required="required"/>  				
                </p>
                <p>					
					<div class="form-group">
				    	 <label for="exampleInputPassword1">Quizzes Topics</label>
				    	 <div id="topicsComboEbook"> </div>	
				    </div>
                </p>
                <p>
					<label>Time</label> 
                    <input  type="text" name="quizeTime"  class="form-control" placeholder="Enter Quiz Time" id="quizeTime" required="required"/>  				
                </p>
                <p>
					<label>General Informations</label>                     
                    <?php  echo $this->ckeditor->editor('quizeGeneralInformation',@$default_value);?> 				
                </p>
                    <input class="submit" type="submit" value="Continue &raquo;" /> 
                             
				<div class="clear"></div>
            </fieldset>
            
            </form> 
        
        
	   </div>
	   </div>	
	</div>	
			
	<div class="col-md-6">
		     	
	 	<div class="panel panel-info">
	    <div class="panel-heading">
	        <h3 class="panel-title">Uncompleted Quiz Titiles</h3>
	    </div>
	    <div class="panel-body">
	    	
	    	<div class="bs-example">
			    <div class="list-group">
			    <?php 
			    	
			    	foreach ($generalUncompletedInfoList AS $info) {
			        	echo '<a href="'.base_url().'homePortalEdu/listOfQuizzesBaseOnUncompletedTopicTech/'.$info->topicsId.'" class="list-group-item ">
			           		 <i class="fa fa-book"></i> '.ucfirst($info->topicsName).' <span class="badge badge-info">'.$info->totailQuzzes.'</span>
			      		 </a>';			        
			        
					} 
					
				?>
			    </div>
			</div>	
		</div>
		</div>	
	 </div>
	 
	 <div class="col-md-6">
		     	
	 	<div class="panel panel-success">
	    <div class="panel-heading">
	        <h3 class="panel-title">Completed Quiz Titiles</h3>
	    </div>
	    <div class="panel-body">
	    	
	    	<div class="bs-example">
			    <div class="list-group">
			    <?php 
			    	
			    	foreach ($generalCompletedInfoList AS $info) {
			        	echo '<a href="'.base_url().'homePortalEdu/listOfQuizzesBaseOnCompletedTopicTech/'.$info->topicsId.'" class="list-group-item ">
			           		 <i class="fa fa-book"></i> '.ucfirst($info->topicsName).' <span class="badge badge-info">'.$info->totailQuzzes.'</span>
			      		 </a>';			        
			        
					} 
					
				?>
			    </div>
			</div>	
		</div>
		</div>	
	 </div>
	 
	 <div class="col-md-6">
		     	
	 	<div class="panel panel-warning">
	    <div class="panel-heading">
	        <h3 class="panel-title">Freezed Quiz Titiles</h3>
	    </div>
	    <div class="panel-body">
	    	
	    	<div class="bs-example">
			    <div class="list-group">
			    <?php 
			    	
			    	foreach ($generalFreezedCompletedInfoList AS $info) {
			        	echo '<a href="'.base_url().'homePortalEdu/#/'.$info->topicsId.'" class="list-group-item ">
			           		 <i class="fa fa-book"></i> '.ucfirst($info->topicsName).' <span class="badge badge-info">'.$info->totailQuzzes.'</span>
			      		 </a>';			        
			        
					} 
					
				?>
			    </div>
			</div>	
		</div>
		</div>	
	 </div>
	
</div>


<!--***************************************** PAGE BODY ENDS ***************************************************************-->


<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>