<?php	

	 $topicID = $this->uri->segment ( 3 );
		
?>
	
<div class="modal-header">

</div>

<div class="modal-body">

	<div id="topicsFormChangeInUpdate"></div>
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

	
		<h4 class="modal-title">Update Topic  </h4>
		
		<?php foreach ($topic as $raw ){ ?>
			 
			 <input id="tId" type="hidden" value="<?php echo $raw->id ?>">
			 <input id="topTitle" class="form-control" type="text" value="<?php echo htmlspecialchars($raw->topicsName) ?>">
			  
			 
		<?php }  ?>
	
		
</div>

<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal" id="closeButtonModal">Close</button>
		
	 <button type="button" class="btn btn-warning" id="updateButton" onclick="updateTutorialTopics()"  data-loading-text="updating..."><i class="fa fa-square-o"></i> Update Topic</button>
	
</div>
