<?php $ci = &get_instance(); ?>

<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left">Tutorials</h3>
</div>
<div class="description">
	
	<?php echo '<button  class="btn btn-lg btn-default" onclick="goBack();" ><img src="'.base_url().'img/Arrow-Back-icon.png"  width="24px"  >Go Back</button>';	 ?>
	
		
	
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DELIVERY RUNS CONTENT -->
<!-- DATA TABLES -->

<!--***************************************** PAGE BODY STARTS ***************************************************************-->

	
	<div class="row">
		<div class="col-md-10">
		     <div class="col-md-3">
		          <div class="panel panel-info">
					   <div class="panel-heading"> Lesson's Details </div>
					   
                       <div class="panel-body">
                            <div class="list-group">
								  							
		                          <?php 
		                          //print_r($byId);
		                               foreach($byId AS $record){
		                               	   //echo '<a href="'.base_url().'homePortalEdu/viewTutorial/'.$courseId.'/'.$record->id.'" class="loadNextVideo list-group-item">'.$record->name.'</a>';		                               	   
		                               	   echo '<p class="loadNextVideo list-group-item" data-watch-video="'.$record -> ebookandvideoId.'" data-topic-id="'.$courseId.'" data-info-id="'.$record -> id.'" data-video-id="'.$record->details.'">'.$record->name.'</p>';
		                               }		                          
		                          ?>
		                          <a href="<?php echo base_url().'homePortalEdu/viewTutorial/'.$courseId.'/q';?>" class="list-group-item">Quiz</a>
		                          
		                     </div>
                       </div>
				  </div>
		     </div>
		     
		     <div class="col-md-7">
		        <div class="panel panel-info">
			        <div class="panel-heading" id="lessonTitle"> 
			            <?php 
			            
		                     if(!empty($intro)) { 
		                     	  echo $intro -> name; 
		                     	  echo '<input type="hidden" id="introVideo" value="'.$intro -> details.'"/>'; 
		                     	  echo '<input type="hidden" id="introVideoId" value="'.$intro -> id.'"/>';
		                     	  echo '<input type="hidden" id="topic" value="'.$courseId.'"/>';
		                     	  echo '<input type="hidden" id="watchedId" value="'.$intro -> ebookandvideoId.'"/>';
		                     }else{
		                     	  echo "Details"; 
		                     	  echo '<input type="hidden" id="introVideo" value=""/>';
		                     } 
			             ?> 
			         </div>
	                <div class="panel-body">  
	                      
	                     <?php $url = $this -> uri -> segment(4); if($url != "q") { ?>       
		                     <div class="embed-responsive embed-responsive-16by9">
		                          <div id="video-placeholder"></div>
		                          <p><span id="current-time">0:00</span> / <span id="duration">0:00</span></p>
		                          <p><span id="alertInfo"> </span></p>	                          
		                     </div>
	                     <?php }else {     
                              if($quiz){
                               echo '<div class="panel panel-info">
									      <div class="panel-body">
			                                    <h2>'.$quiz ->gQname.'</h2>			
			                                    <p> Please complete lessons before beginning quiz. 
		                                          Please complete the quiz in a timely manner within the deadline that was instructed.  
		                                          If you are unsure of a question, you may go back and review the lessons. 
		                                          In order to schedule for the practical and receive certification, you must complete the quiz. 
		                                          Once you complete the quiz please contact the pharmacy to schedule your practical and certification. 
		                                          Once complete, you will see your score, you will not be able to see the answers. 
		                                          You will have a chance review the answers to the quiz with the pharmacist during the practical and certification. Good Luck! </p>';
                                                 $ci -> getStartQuizButton($courseId,$quiz -> id);                                                 
			                              echo'</div>
									  </div>';
                              }			                              
                          } 
                          ?>	                     
	                     
	                </div>
	            </div>
			 </div>
			 
		</div>	
	</div>



<!--***************************************** PAGE BODY ENDS ***************************************************************-->


<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>