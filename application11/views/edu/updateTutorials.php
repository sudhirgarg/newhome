<?php
	$isNew = true;
	$userID = $this->ion_auth->user()->row()->id;
	$user;
	if ($userID > 0) {
		$isNew = false;
		$user = $this->ion_auth->user ($userID)->row ();
	}
?>

<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left">Tutorials</h3>
</div>
<div class="description">
	
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DELIVERY RUNS CONTENT -->
<!-- DATA TABLES -->

<!--***************************************** PAGE BODY STARTS ***************************************************************-->

<div class="row" style ="min-height: 550px;" >
	<div class="col-md-12">

		<div class="well well-sm">
			<h3><?php echo $this->lang->line('HELLO');?> <?php echo $this->ion_auth->user()->row()->first_name;?> </h3>
		</div>
	
		<ul id="myTab" class="nav nav-tabs">
		   <li class="active"> <a href="#home" data-toggle="tab"> Upload Ebooks / Video </a> </li>
		   <li > <a href="#imageGallery" data-toggle="tab"> Image Gallery </a> </li>
		   <li><a href="<?php echo base_url().'homePortalEdu/uploadImage';  ?>" data-toggle="">Upload Images</a></li>		   
		</ul>
		
		
		<div id="myTabContent" class="tab-content" >
			<!-- ebooks start -->
		   <div class="tab-pane fade in active" id="home" >
		  
		   	<div class="col-md-6">
		   		
		   		<div class="bs-example">
			    <div class="panel panel-success">
			   		<div class="panel-heading">
			            <h3 class="panel-title">Add Book</h3>
			        </div>
			        <div class="panel-body ebookBody">
			        	<?php if(validation_errors()) { ?>
			        		
			        	 <div class="alert alert-warning" role="alert" id="wrapData" >
	              			 <a href="#" class="close" data-dismiss="alert">&times;</a>		  	
						  	 <div id="formChange"><?php echo validation_errors(); ?></div>
						 </div>
						 
						<?php }else	{
							$a = $this->uri->segment('2');
							if ($a == "saveEbookTutorial"){
								
							echo '
							 <div class="alert alert-warning" role="alert" id="wrapData" >
	              				 <a href="#" class="close" data-dismiss="alert">&times;</a>		  	
						  		 <div id="formChange">'.$s.'</div>
							 </div>
							';
							}
						}						
						
						 ?>  
							
			        	<?php echo form_open_multipart('homePortalEdu/saveEbookTutorial') ?>
			        		
						  <div class="form-group">
						     <label for="ebookTitle">Title</label>
						     <input type="text" class="form-control" id="ebookTitle" name="ebookTitle" placeholder="Title">
						  </div>
						  <div class="form-group">
						     <label for="exampleInputPassword1">Course</label>
						     <div id="topicsComboEbook"> </div>	
						  </div>
						  <div class="form-group">
						     <label for="exampleInputPassword1">Type</label>
						     <input type="radio" name="documentType" value="1"> E book
						     <input type="radio" name="documentType" value="2"> Video
						  </div>
						  <!--div class="form-group">
						     <label for="exampleInputFile">File input</label>
						     <input type="file" id="userfile" name="userfile">
						   
						  </div-->						  
						  <div class="form-group">
						  	<?php  echo $this->ckeditor->editor('quizeGeneralInfo',@$default_value);?> 
						
						  </div>
						  
						  <input type="submit" class="btn btn-primary" id="saveButton"  data-loading-text="Saving..." value="Save">
						  
						</form>	    
						    	
			        </div> 
			    </div>
				</div>	
						
					   		 	     
			</div>  
						    
		     <!--div class="col-md-4">
		     	
		     	<div class="bs-example">
			    <div class="panel panel-success">
			   		<div class="panel-heading">
			            <h3 class="panel-title">Add Topics</h3>
			        </div>
			        <div class="panel-body ebookBody">
			        	 <div>
	              			 <a href="#" class="close" data-dismiss="alert">&times;</a>		  	
						  	 <div id="topicsFormChange"></div>
						 </div>
			        	
						  <div class="form-group">
						     <label for="tutorialTitle">Topics</label>
						     <input type="text" class="form-control" id="tutorialTitle" placeholder="Topics">
						  </div>
						 		  
						  <button type="submit" class="btn btn-primary" id="saveButtonTopics" onclick="saveTutorialTopics()" data-loading-text="Saving...">Submit</button>
						  <a href="<?php echo base_url();?>homePortalEdu/viewAllTopic"<button type="submit" class="btn btn-info" ><i class="fa fa-desktop"></i> View All</button> </a>
								        	
			        </div> 
			    </div>
				</div>	
				
		     </div-->
		     
		     <div class="col-md-6">
		     	
		     	<div class="bs-example">
			    <div class="panel panel-success">
			   		<div class="panel-heading">
			            <h3 class="panel-title">Course List <i class="fa fa-angle-double-right"></i>  </h3>
			        </div>
			        <div class="panel-body ebookBody">
			        	
			        	<div class="bs-example">				
						     <hr>				   
							    <ol>
							    	<span id="listOfBook">   </span>
							    </ol>
							  <hr>
						</div>		        			        	
			        </div> 
			    </div>
				</div>	
				
		     </div>
		     
		      <!--div class="col-md-6">
		     	
		     	<div class="bs-example">
			    <div class="panel panel-success">
			   		<div class="panel-heading">
			            <h3 class="panel-title">List Of Videos <i class="fa fa-angle-double-right"></i> <a href=""  class="label label-warning"> View All </a> </h3>
			        </div>
			        <div class="panel-body ebookBody">
			        	<div class="bs-example">				
						     <hr>				   
							    <ol>
							        <span id="listOfvideo">   </span>
							    </ol>
							  <hr>
						</div>	
								        			        	
			        </div> 
			    </div>
				</div>	
				
		     </div-->
		     
		   </div>
		   <!-- ebooks end -->
		   
		   
		   
		   <!-- Videos start -->
		   <div class="tab-pane fade" id="imageGallery">
		   <div class="tab-pane fade in active" id="home" >
		  
		   	<div class="col-md-6">
		   		
		   		<div class="bs-example">
			    <div class="panel panel-success">
			   		<div class="panel-heading">
			            <h3 class="panel-title">Image Gallery </h3>
			        </div>
			        <div class="panel-body ebookBody">
			        	<div>	              			   	
						  	 <div id="formChangeInVideoPanel"></div>
						 </div>
									        	
						 
						  <?php
						   
						   if(!empty($imageFile)) {						  
							 	$i = 0;								
								for($i = 0; $i < count($imageFile); $i++ ){																									
									echo '<img class="photo" src="'.base_url().'img/edu/'.$imageFile[$i].'" width="210px" >';									
								}							
						   }						   			 
						 
						 ?>
						        	
			        </div> 
			    </div>
				</div>	
						
					   		 	     
			</div>  
						    
		     <!--div class="col-md-4">
		     	
		     	<div class="bs-example">
			    <div class="panel panel-success">
			   		<div class="panel-heading">
			            <h3 class="panel-title">Add Topics</h3>
			        </div>
			        <div class="panel-body ebookBody">
			        	
			        	 <div>
	              			 <a href="#" class="close" data-dismiss="alert">&times;</a>		  	
						  	 <div id="topicsFormChangeInVideoPanel"></div>
						 </div>
			        
						  <div class="form-group">
						     <label for="exampleInputEmail1">Topics</label>
						     <input type="text" class="form-control" id="topicVideo" placeholder="Topics">
						  </div>
						 		  
						  <button type="submit" class="btn btn-primary" id="saveButton" onclick="saveTutorialTopicsInVideoPanel()" data-loading-text="Saving...">Submit</button>
						  <a href="<?php echo base_url();?>homePortalEdu/viewAllTopic"<button type="submit" class="btn btn-info" ><i class="fa fa-desktop"></i> View All</button> </a>
						        	
			        </div> 
			    </div>
				</div>	
				
		     </div-->
		     
		     <div class="col-md-6">
		     	
		     	<div class="bs-example">
			    <div class="panel panel-success">
			   		<div class="panel-heading">
			            <h3 class="panel-title">Course List <i class="fa fa-angle-double-right"></i>  </h3>
			        </div>
			        <div class="panel-body ebookBody">
			        	
			        	<div class="bs-example">				
						     <hr>				   
							    <ol>
							    	<span id="listOfBook1">   </span>
							    </ol>
							  <hr>
						</div>		        			        	
			        </div> 
			    </div>
				</div>	
				
		     </div>
		     
		      <!--div class="col-md-6">
		     	
		     	<div class="bs-example">
			    <div class="panel panel-success">
			   		<div class="panel-heading">
			            <h3 class="panel-title">List Of Videos <i class="fa fa-angle-double-right"></i> <a href=""  class="label label-warning"> View All </a> </h3>
			        </div>
			        <div class="panel-body ebookBody">
			        	<div class="bs-example">				
						     <hr>				   
							    <ol>
							        <span id="listOfvideo1">   </span>
							    </ol>
							  <hr>
						</div>	
								        			        	
			        </div> 
			    </div>
				</div>	
				
		     </div-->
		     
		   </div>
		    <!-- Videos end -->
		   
		 
		</div>
		
		
	</div>
</div>











<!--***************************************** PAGE BODY ENDS ***************************************************************-->









<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>