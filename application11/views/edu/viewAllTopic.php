

<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left">Topics List</h3>
</div>
<div class="description">
	
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DELIVERY RUNS CONTENT -->
<!-- DATA TABLES -->

<!--***************************************** PAGE BODY STARTS ***************************************************************-->

<div class="row">
	
	
	
	<div class="col-md-8">
		
	<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Topics</h3>
    </div>
    <div class="panel-body">
    	
		<table id="topicTable" cellpadding="0" cellspacing="0" border="0" class="datatable table table-striped table-bordered table-hover">
		<thead class="active">
			<tr>
				<th align="center" >#</th>
				<th>Name</th>
				<th>Edit/Delete</th>			
			</tr>			
		</thead>
			<?php
				$i = 1;
				foreach ($topics as $row)	{
				   
					echo '<tr><td align="center">'.$i++.'</td>
					<td>'.ucfirst($row->topicsName).'</td>
					<td> 
					<button type="button" class="label label-warning" onclick="loadModal(\''.base_url().'homePortalEdu/editTopic/'.$row->id.'\')" >									
					<i class="fa fa-square-o"></i> Edit 
					</button>
					
					<button type="button" class="label label-danger" onclick="loadModal(\''.base_url().'homePortalEdu/deleteTopic/'.$row->id.'\')" >									
					<i class="fa fa-trash-o"></i> Delete 
					</button>
					
					</td></tr>';	
				}				 	 	     
				
			?>
		<tfoot>
			<tr>
				<th>#</th>
				<th>Name</th>
				<th>Edit/Delete</th>
			</tr>
		</tfoot>
		
		</table>
		
		</div>
	</div>
	
	</div>
	

</div>


<!--***************************************** PAGE BODY ENDS ***************************************************************-->


<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>