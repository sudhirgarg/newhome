<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left">Delete Question and Answer</h3>
</div>
<div class="description">
	
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DELIVERY RUNS CONTENT -->
<!-- DATA TABLES -->

<!--***************************************** PAGE BODY STARTS ***************************************************************-->

<div class="row">
	<div class="col-md-6">
		<div class="panel panel-info">
	   
	    <div class="panel-body"> 	
	    
			
			<div class="modal-body">
			
				<div id="topicsFormChangeInUpdate"></div>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			
				
					<h4 class="modal-title">  </h4>
					
					<?php
		    		$i = 1;
		    		foreach($quiz AS $raw){
		    			
		    			$raw->question;
						$answers = explode(",", $raw->quizzesAnswer);
						$answers = array_filter($answers);
						shuffle($answers);						
						$correctAnswers = explode(",", $raw->correctAnswer);
						$countAnswer = count($answers);
						$countCorrectAnswers = count($correctAnswers);						
						
						$A1 = "";
					
						if($raw->answerType == 3) {
							$inputType = 'checkbox';	         										
							
						}else{
							$inputType = 'radio';
							for($c= 0; $c < $countCorrectAnswers; $c++){
	    			             $A1 = $correctAnswers[$c];			
	    		         	}
						}
						
						$a = 0;
						$c = 0;
						
		    								
		    		echo '<ul class="list-unstyled">
						<li>
						  '.$i++.') '.$raw->question.'
						</li>';
						for($a=0; $a < $countAnswer; $a++){
							if($raw->answerType == 3){
								
								 echo '<li><input type="'.$inputType.'" name="answer[]" id="answer[]"';
								  if(in_array($answers[$a],$correctAnswers)) { echo ' checked="checked"'; } 
								 echo 'value="'.$answers[$a].'">'.$answers[$a].'</li>';	
								
							}else{
								 echo '<li><input type="'.$inputType.'" name="answer'.$i.'" id="answer[]"';								   								  
									  if($answers[$a] == $A1) { echo ' checked="checked"'; }  	
								 echo ' value="'.$answers[$a].'">'.$answers[$a].'</li>';	
							}			
						}					
					echo'<li>
						 <span class="label label-info">Hint :</span>  '.$raw->quizzesHint.'
						</li>';
								
		    		echo '</ul>';	
					
					echo'<input type="hidden" id="qId" value="'.$raw->id.'" >';			
		    		
				}		    	
		    	
		    	?>	
				
					
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal" onclick="goBack()" id="closeButtonModal">Close</button>
					
				 <button type="button" class="btn btn-danger" id="updateButton" onclick="deleteQuestion()"  data-loading-text="updating..."><i class="fa fa-square-o"></i>Delete </button>
				
			</div>

		
	
		</div>
		</div>
	</div>
</div>



<!--***************************************** PAGE BODY ENDS ***************************************************************-->


<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>