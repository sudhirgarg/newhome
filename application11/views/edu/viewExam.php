
<!DOCTYPE html>
<html lang="en">
<head>
	
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title> <?php if($this->uri->segment(2) == "NULL") { echo "Home"; } else { echo $this->uri->segment(2); } ?> </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/cloud-admin.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/themes/default.css" id="skin-switcher">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">		

   <link rel="stylesheet" href = "http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">	
   <style>
       .border-bottom {
            border-bottom: 1px solid black;
        }
        
         body {
        width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
        background-color: #FAFAFA;
        font: 12pt "Tahoma";
	    }
	    * {
	        box-sizing: border-box;
	        -moz-box-sizing: border-box;
	    }
	    .page {
	        width: 210mm;
	        min-height: 297mm;
	        margin: 10mm auto;
	        border-radius: 5px;
	        background: white;
	        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
	    }
	    .subpage {
	        padding: 1cm;
	        height: 257mm;
	    }
	    
	    @page {
	        size: A4;
	        margin: 0;
	    }
	    @media print {
	        html, body {
	            width: 210mm;
	            height: 297mm;        
	        }
	        .page {
	            margin: 0;
	            border: initial;
	            border-radius: initial;
	            width: initial;
	            min-height: initial;
	            box-shadow: initial;
	            background: initial;
	            page-break-after: always;
	        }
	    }
   </style>
</head>
<body>
  

<!--***************************************** PAGE BODY STARTS ***************************************************************-->
<div class="book">




<div class="page">
   <div class="subpage">
      <div class="row">
      <div class="col-md-6">
      
       <div class="panel panel-info">
		      <div class="panel-body">
		          <?php
		                echo 'Subject   : '.$info -> gQname.'<br/>';
		                echo 'Fullname  : '.$info -> first_name.' '.$info -> last_name.'<br/>';
		                echo 'Attempted : '.$info -> attempt.'<br/>';
		                echo 'Score     : '.round($info -> score).'<br/>';
		          ?>
		       </div>
	   </div>
		  
       <div class="panel panel-info">
	     <div class="panel-body">
           <?php
		    	 
		    		$i = 1;
		    		foreach($quiz AS $raw){
		    			
		    			$raw->question;
						$answers = explode(";", $raw->quizzesAnswer);
						$answers = array_filter($answers);
						shuffle($answers);						
						$candidateAnswers = explode(";", $raw->answer);
						$countAnswer = count($answers);
						$countCandidateAnswers = count($candidateAnswers);						
						
						$A1 = "";
					
						if($raw->answerType == 3) {
							$inputType = 'checkbox';	         										
							
						}else{
							$inputType = 'radio';
							for($c= 0; $c < $countCandidateAnswers; $c++){
	    			             $A1 = $candidateAnswers[$c];			
	    		         	}
						}
						
						$a = 0;
						$c = 0;
						
		    								
		    		echo '<ul class="list-unstyled">
						<li>
						  '.$i++.') '.$raw->question.'<br/>   		
						</li>';
						for($a=0; $a < $countAnswer; $a++){
							if($raw->answerType == 3){
								
								 echo '<li><input type="'.$inputType.'" name="answer[]" id="answer[]"';
								  if(in_array($answers[$a],$candidateAnswers)) { echo ' checked="checked"'; } 
								 echo 'value="'.$answers[$a].'">'.$answers[$a].'</li>';	
								
							}else{
								 echo '<li><input type="'.$inputType.'" name="answer'.$i.'" id="answer[]"';								   								  
									  if($answers[$a] == $A1) { echo ' checked="checked"'; }  	
								 echo ' value="'.$answers[$a].'">'.$answers[$a].'</li>';	
							}			
						}					
					echo'<li>
	        					<div class="alert alert-success">
								  <strong>Right Answer:</strong> '.$raw->correctAnswer.'
								</div>
	        		  </li>';
								
		    		echo '</ul>';	
		    		
				}		    	
				
		    	
		   ?>
		   </div>
    </div>	
    </div>

        
		</div>
    </div>
</div>





<!--***************************************** PAGE BODY ENDS ***************************************************************-->



</body>
</html>