<?php	

	 $topicID = $this->uri->segment ( 3 );
		
?>
	
<div class="modal-header">

</div>

<div class="modal-body">

	<div id="topicsFormChangeInDelete"></div>
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

	
		<h4 class="modal-title">Are you sure (You will lose your data, once you confirm!) ?  </h4>
		
		<?php if ($topicID > 0 ){ ?>
			 
			 <input id="id" type="hidden" value="<?php echo $topicID; ?>">
			 
		<?php } else {
			 echo "No Data";
		}
		
		 ?>
	
		
</div>

<div class="modal-footer">
	 <button type="button" class="btn btn-default" data-dismiss="modal" id="closeButtonModal">Close</button>		
	 <button type="button" class="btn btn-danger" id="deleteButton" onclick="deleteLessonConfirm()"  data-loading-text="deleting..."><i class="fa fa-trash-o"></i> Delete Lesson</button>
	
</div>
