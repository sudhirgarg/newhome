<?php
	
	$id ="";
	$gQname ="";
	$topicsId ="";
	$details ="";
    $timeDutation = "";
	  
	 foreach ($byId as $qData) {
	     $id =$qData->id;
		 $gQname =$qData->gQname;
		 $topicsId =$qData->topicsId;
		 $details =$qData->details;
	     $timeDutation =$qData->timeDutation;
	 }

?>

<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left">Quiz</h3>
</div>
<div class="description">
	
	<?php foreach ($byId as $value) {				 
			echo '<a  class="btn btn-lg btn-default"  href="'.base_url().'homePortalEdu/listOfQuizzesBaseOnCompletedTopicTech/'.$value->topicsId.'"><img src="'.base_url().'img/Arrow-Back-icon.png"  width="24px" >Go Back</a>';	
		}
	 ?>
	
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DELIVERY RUNS CONTENT -->
<!-- DATA TABLES -->

<!--***************************************** PAGE BODY STARTS ***************************************************************-->

<div class="row">
	<div class="col-md-6">
		<div class="panel panel-info">
  		<div class="panel-body">
  	
		<form action="<?php echo base_url()?>homePortalEdu/addQuizzesGeneralInfo" class="register" method="POST">
            <h1>Update General Quiz Infomation</h1>
			<fieldset class="row1">
                <legend>Information</legend>
                <?php if(validation_errors()) { ?>
			        		
		        	 <div class="alert alert-warning" role="alert" id="wrapData" >
	          			 <a href="#" class="close" data-dismiss="alert">&times;</a>		  	
					  	 <div id="formChange"><?php echo validation_errors(); ?></div>
					 </div>
				 
				<?php }else	{
							
							if (!empty($s)){
							echo '
							 <div class="alert alert-warning" role="alert" id="wrapData" >
	              				 <a href="#" class="close" data-dismiss="alert">&times;</a>		  	
						  		 <div id="formChange">'.$s.'</div>
							 </div>
							';
							}
						}
				 ?>  
				<p>
					<label>Quiz Title</label> 
                    <input  type="text" name="quizeTitle"  class="form-control" placeholder="Enter Quize title" id="quizeTitle" required="required" value="<?php echo $gQname; ?>"/>  				
                </p>
                <p>					
					<div class="form-group">
				    	 <label for="exampleInputPassword1">Quizzes Topics</label>
				    	 <?php
						     	echo '<select class="form-control" id="topicsId" name="topicsId">
						     		<option value="" > </option>
								 ';
								
								foreach ($allTopics as $row)	{
								    echo '<option value="'.$row->id.'"';
									if($row->id == $topicsId) { echo "selected"; }
									echo '>'.ucfirst($row->topicsName).'</option>';
								}				 	 	     
								echo '</select>';					     
					     ?>
				    	 <!--div id="topicsComboEbook"> </div-->	
				    </div>
                </p>
                <p>
					<label>Time</label> 
                    <input  type="text" name="quizeTime"  class="form-control" placeholder="Enter Quize Time" id="quizeTime" required="required" value="<?php echo $timeDutation; ?>"/>  				
                </p>
                <p>
					<label>General Informations</label>                     
                    <?php  echo $this->ckeditor->editor('quizeGeneralInformation',@$details);?> 				
                </p>
                	<input  type="hidden" name="id"  value="<?php echo $id; ?>"/>  	
                    <input class="submit" type="submit" value="Continue &raquo;" /> 
                             
				<div class="clear"></div>
            </fieldset>
            
            </form> 
        
        
	   </div>
	   </div>	
	</div>
</div>



<!--***************************************** PAGE BODY ENDS ***************************************************************-->


<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>