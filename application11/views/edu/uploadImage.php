

<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left">Upload Images</h3>
</div>
<div class="description">
	<a href="<?php echo base_url();?>homePortalEdu/updateTutorials#imageGallery"  class="btn btn-lg btn-default" >Back To Gallery </a>
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DELIVERY RUNS CONTENT -->
<!-- DATA TABLES -->

<!--***************************************** PAGE BODY STARTS ***************************************************************-->

<div class="row">
	<div class="col-md-6">
	<div class="panel panel-success">
		  
    <div class="panel-heading">
        <h3 class="panel-title"> Image Upload Panel</h3>
    </div>
    <div class="panel-body">
    	
    	
		<?php if(isset($success)) {
						 
			 echo'<div class="alert alert-success fade in">
		        <a href="#" class="close" data-dismiss="alert">&times;</a>
		        <strong>Success!</strong>'.$success.'
		    </div>';
		}?>

			<?php echo form_open_multipart('homePortalEdu/doMultiImageupload');?>
			
			<input type="file" multiple name="userfile[]" size="20" />
			<br /><br />
			
			
			<input type="submit" value="upload" />
		    <!--a href="<?php echo base_url();?>homePortalEdu/updateTutorials#imageGallery"  class="label label-info" >Back To Gallery </a-->
		</form>
		
	</div>
	</div>
	
	</div>
	
</div>


<!--***************************************** PAGE BODY ENDS ***************************************************************-->


<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>