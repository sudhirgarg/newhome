<?php

	$sec1 = $this->uri->segment(1);
	$sec2 = $this->uri->segment(2);

	if(($sec1 == "homePortalEdu") && ($sec2 == "tutorials"))                                             {  $this->load->view('edu/jsFunction/jsTutorials');   } 
	if(($sec1 == "homePortalEdu") && ($sec2 == "quizes"))                                                {  $this->load->view('edu/jsFunction/jsQuizes');      }
	if(($sec1 == "homePortalEdu") && ($sec2 == "quizesStart"))                                           {  $this->load->view('edu/jsFunction/jsQuizesStart'); }
	if(($sec1 == "homePortalEdu") && ($sec2 == "quizzesLoadOneByOne"))                                   {  $this->load->view('edu/jsFunction/jsQuizesStart'); }
	if(($sec1 == "homePortalEdu") && ($sec2 == "updateTutorials"))                                       {  $this->load->view('edu/jsFunction/jsTutorials');   } 
	if(($sec1 == "homePortalEdu") && ($sec2 == "addSubject"))                                            {  $this->load->view('edu/jsFunction/jsTutorials');   }
	if(($sec1 == "homePortalEdu") && ($sec2 == "saveEbookTutorial"))                                     {  $this->load->view('edu/jsFunction/jsTutorials');   }
	if(($sec1 == "homePortalEdu") && ($sec2 == "updateQuizzes"))                                         {  $this->load->view('edu/jsFunction/jsQuizes');      }
	if(($sec1 == "homePortalEdu") && ($sec2 == "addQuizzesGeneralInfo"))                                 {  $this->load->view('edu/jsFunction/jsQuizes');      }
	if(($sec1 == "homePortalEdu") && ($sec2 == "addQuestion"))                                           {  $this->load->view('edu/jsFunction/jsQuizes');      }
    if(($sec1 == "homePortalEdu") && ($sec2 == "editQuiz"))                                              {  $this->load->view('edu/jsFunction/jsQuizes');      }
	if(($sec1 == "homePortalEdu") && ($sec2 == "viewAllTopic"))                                          {  $this->load->view('edu/jsFunction/jsTutorials');   }
	if(($sec1 == "homePortalEdu") && ($sec2 == "listOfinfoBaseOnTopicTech"))                             {  $this->load->view('edu/jsFunction/jsTutorials');   }
    if(($sec1 == "homePortalEdu") && ($sec2 == "viewQuizTech"))                                          {  $this->load->view('edu/jsFunction/jsQuizes');      } 
	if(($sec1 == "homePortalEdu") && ($sec2 == "editQuizIndividually"))                                  {  $this->load->view('edu/jsFunction/jsQuizes');      }
	if(($sec1 == "homePortalEdu") && ($sec2 == "listOfQuizzesBaseOnUncompletedTopicTech"))               {  $this->load->view('edu/jsFunction/jsQuizes');      }
	if(($sec1 == "homePortalEdu") && ($sec2 == "listOfQuizzesBaseOnCompletedTopicTech"))                 {  $this->load->view('edu/jsFunction/jsQuizes');      }

	if((($sec1 == "homePortalEdu") && ($sec2 == "getResult")) || (($sec1 == "homePortalEdu") && ($sec2 == "quizzesLoadOneByOne"))){
		  echo '<script>
				window.location.hash="no-back-button";
				window.location.hash="Again-No-back-button";
				window.onhashchange=function(){window.location.hash="no-back-button";}
				</script>';	  
	}
	
?>


<script>
	jQuery(document).ready(function() {
		App.setPage("widgets_box");
		//Set current page
		App.init();
		//Initialise plugins and elements	
				
		
	});
	
	function saveHomeUser() {
        $("#saveButton").button('loading');
        $.post(
        	'<?php echo site_url('homePortalEdu/editPortalUserModalBySeperatePageRun');?>',  {
        		
	             id: $("#userID").val(),	
	             username: $("#username").val(), 
	             password: $("#password").val(),  
	             email: $("#email").val(),  
	             groups: $("#groups").val(),
	             company: $("#company").val(),
	             phone: $("#phone").val(),
	             first_name: $("#first_name").val(),
	             last_name: $("#last_name").val()
             }, 
	        function (data, status) {  
	          	
	            var pTags = $( "#formChange" ); 	
	         	
	         	if ( pTags.parent().is( "p" ) ) {	         		
				    $("#formChange").html(data);				    
				 } else {				   
	         		$("#formChange").html(data);	
	         		$("#formChange").wrap('<p class="alert alert-warning" role="alert" id="wrapData"> </p>');			
				}	         	           
	        	 $("#saveButton").button('reset');      
	        
	        });
    }
    
    function saveNewUserPassword() {
        $("#saveButtonPassword").button('loading');
        $.post(
        	'<?php echo site_url('homePortalEdu/changePortalUserPassword');?>',  {
        		
	             id: $("#userID").val(),	
	             currentPassword: $("#currentPassword").val(), 
	             password: $("#password").val(),  
	             passconf: $("#passconf").val()
             }, 
	        function (data, status) { 
	        	var pTags = $( "#passwordChange" ); 	
	         	
	         	if ( pTags.parent().is( "p" ) ) {
	         		
				    $("#passwordChange").html(data);
				    
				 } else {				   
	         		$("#passwordChange").html(data);	
	         		$("#passwordChange").wrap('<p class="alert alert-warning" role="alert" id="wrapData"> </p>');			
				}	         	           
	        	 $("#saveButtonPassword").button('reset');
	        });
    }
    
    function loadModal(ajaxURL){
    	
		$.get(ajaxURL, function(data) {
			$("#myModalContent").html(data);
			$('#myModal').modal();
		});
	}
	
	

</script>


 <?php $this -> load -> view('extras'); ?>