

<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left"> Certificate </h3>
</div>
<div class="description">
	
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DELIVERY RUNS CONTENT -->
<!-- DATA TABLES -->

<!--***************************************** PAGE BODY STARTS ***************************************************************-->

<div class="row" ng-app="viewResult" ng-controller="viewResultController">

	<div class="col-md-3" >
		
		<div class="panel panel-info">
	    <div class="panel-heading">
	        <h3 class="panel-title"> Candidate list </h3>	        
	    </div>
	    <div class="panel-body">
	    		    	
	    	<ul class="list-group">	    	    
		    	  <?php  
		    	   $i = 1;
		    	    foreach($scoreDetails AS $record) {
		    	    	 echo '<li class="list-group-item" ng-click="getUserId('.$record -> userName.')" ng-init="userName = \''.$record -> first_name.' '.$record -> last_name.'\'">';
				         echo $i++.')<a href=""> '.$record -> first_name.' '.$record -> last_name.'</a><br/>';
				         echo '<input type="hidden" ng-model="userName"  />';
				         echo '</li>';
				         
				    } 
				  ?>	
				  			
			</ul>
	       
		</div>
		</div>
		

	</div>
	
	<div class="col-md-6" ng-show="scoreHistory">
		
		 <div class="panel panel-info">
	 		 <div class="panel-heading">
	   			 <h3 class="panel-title">{{candidateName}}'s Activities </h3>
	   			    
	  		 </div>
	  		 
	  		 <div class="panel-body">
	  		 	 <table class="table table-hover">
	  		 	      <thead>
					      <tr>
						        <th>#</th>
						        <th>Title</th>
						        <th>Score%</th>
						        <th>Attempt</th>
						        <th> <button class="btn btn-info" > <i class="fa fa-print" aria-hidden="true"></i>         </button>   </th>
						        <th> <button class="btn btn-info" > <i class="fa fa-folder-open-o" aria-hidden="true"></i> </button>   </th>
						        <th> <button class="btn btn-info" > <i class="fa fa-certificate" aria-hidden="true"></i>   </button>   </th>
					      </tr>
					  </thead>
					  <tbody>
						  <tr ng-repeat="getScore in getScores" >	
						  					   
							    <td>{{ $index + 1 }}</td> 
							    <td>{{ getScore.Title }}</td>
							    <td>{{ getScore.score | number:0}}</td>
							    <td>{{ getScore.attempt }}</td>
							    <td> <button class="btn btn-success" ng-click="printExamReport(getScore.id)">    <i class="fa fa-print" aria-hidden="true"></i>         </button> </td>
							    <td> <button class="btn btn-success" ng-click="viewExamReport(getScore.id)">     <i class="fa fa-folder-open-o" aria-hidden="true"></i> </button> </td>
							    <td> <button class="btn btn-success" ng-click="completeExamReport(getScore.id)"> <i class="fa fa-certificate" aria-hidden="true"></i>   </button> </td>							    
						    						      
						  </tr>
					  </tbody>	  
				 </table>
	 		 </div> 		  
   		</div>      
   				
	</div>
	
	
</div>


<!--***************************************** PAGE BODY ENDS ***************************************************************-->


<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>
