<!DOCTYPE html>
<html lang="en">
	
<head>
	
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<title> <?php if($this->uri->segment(2) == "NULL") { echo ""; } else { echo $this->uri->segment(2); } ?> </title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
	<meta name="description" content="">
	<meta name="author" content="">
	
	<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/cloud-admin.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/themes/default.css" id="skin-switcher">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/eduProtalCustom.css" id="skin-switcher">
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">-->		
	
	<!-- DATA TABLES -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/datatables/media/css/jquery.dataTables.min.css" />




	<!-- New Design Css -->


	<!-- BEGIN LAYOUT FIRST STYLES -->
        <link href="//fonts.googleapis.com/css?family=Oswald:400,300,700" rel="stylesheet" type="text/css" />
        <!-- END LAYOUT FIRST STYLES -->
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>css_new/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>css_new/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>css_new/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>css_new/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?php echo base_url(); ?>css_new/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>css_new/morris/morris.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>css_new/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>css_new/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>css_new/bootstrap-table/bootstrap-table.min.css" rel="stylesheet" type="text/css" />

	<link href="<?php echo base_url(); ?>css_new/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>css_new/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?php echo base_url(); ?>css_new/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo base_url(); ?>css_new/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?php echo base_url(); ?>css_new/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>css_new/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->

        <!-- New Design Css End -->	
	
</head>



<!-- New HTML Start -->



<body class="page-header-fixed page-sidebar-closed-hide-logo">
        <!-- BEGIN CONTAINER -->
        <div class="wrapper">
            <!-- BEGIN HEADER -->
            <header class="page-header">
                <nav class="navbar mega-menu" role="navigation">
                    <div class="container-fluid">
                        <div class="clearfix navbar-fixed-top">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="toggle-icon">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </span>
                            </button>
                            <!-- End Toggle Button -->
                            <!-- BEGIN LOGO -->
                            <a id="index" class="page-logo" href="<?php echo base_url("index.php/homeportal/"); ?>">
                                <img src="<?php echo base_url(); ?>img/logo/logo.gif" alt="Logo"> </a>
                            <!-- END LOGO -->
                            <!-- BEGIN SEARCH -->
                            <!--<form class="search" action="extra_search.html" method="GET">
                                <input type="name" class="form-control" name="query" placeholder="Search...">
                                <a href="javascript:;" class="btn submit md-skip">
                                    <i class="fa fa-search"></i>
                                </a>
                            </form>-->
                            <!-- END SEARCH -->
                            <!-- BEGIN TOPBAR ACTIONS -->
                            <div class="topbar-actions">
				<!-- BEGIN GROUP INFORMATION -->

				<?php 				
						
				   $page = "";
				   $page1 = "";
				   if($this->ion_auth->in_group ( 'pharmacyuser' ))   { $page = ""; $page1 ="tutorials"; }
				   if($this->ion_auth->in_group ( 'homeportaluser' )) { $page = "homeportal"; $page1 ="tutorials"; }		
				   
				
			?>


                                <div class="btn-group-red btn-group">
                                    <button type="button" class="btn btn-sm md-skip dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                        <i class="fa fa-users"></i>
                                    </button>
                                    <ul class="dropdown-menu-v2" role="menu">
                                        <li>
                                            <a href="<?php echo base_url().$page; ?>">Home Portal</a>
                                        </li>
                                        <li class="active">
                                            <a href="<?php echo base_url() . 'index.php/homePortalEdu/'.$page1;?>">Education Portal</a>
                                        </li>
                                        
                                    </ul>
                                </div>
                                <!-- END GROUP INFORMATION -->
                                <!-- BEGIN GROUP NOTIFICATION -->
                                <div class="btn-group-notification btn-group" id="header_notification_bar">
                                    <button type="button" class="btn btn-sm md-skip dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                        <i class="icon-bell"></i>
                                    </button>
                                    <ul class="dropdown-menu-v2">
                                        <li class="external">
                                            <h3>
                                                <span class="bold">0</span> notifications</h3>
                                            <a href="#">view all</a>
                                        </li>
                                        <li>
                                            <ul class="dropdown-menu-list scroller" style="height: 250px; padding: 0;" data-handle-color="#637283">
                                                <li>
                                                    <a href="javascript:;">
                                                        <span class="details">
                                                            <span class="label label-sm label-icon label-success md-skip">
                                                                <i class="fa fa-plus"></i>
                                                            </span> No Notifications. </span>
                                                        <span class="time">just now</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                <!-- END GROUP NOTIFICATION -->
				<!-- BEGIN GROUP NOTIFICATION -->
                                <div class="btn-group-notification btn-group" id="header_notification_bar">
                                    <button type="button" class="btn btn-sm md-skip dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                        <i class="fa fa-envelope"></i>
                                    </button>
                                    <ul class="dropdown-menu-v2">
                                        <li class="external">
                                            <h3>
                                                <span class="bold"></span> Messages</h3>
                                            <a href="#">view all</a>
                                        </li>
                                        <li>
                                            <ul class="dropdown-menu-list scroller" style="height: 250px; padding: 0;" data-handle-color="#637283">
						<?php 
							$homeComments = $this->comp->getNewHomeComments();
							if ($homeComments) {
								foreach ( $homeComments->result () as $homeComment ) {
									$time_ago =strtotime($homeComment->messageDate);																											
							?>
                                                <li>
                                                    <a href="#" onclick="loadModal('<?php echo base_url('index.php/compliance/editTicketModal/') . '/' . 0 . '/' . $homeComment->ticketID ;?>')">
                                                        <span class="details">
                                                            <span class="label label-sm label-icon label-success md-skip">
                                                                <i class="fa fa-plus"></i>
								
                                                            </span>
								<span class="bold"><?php echo $homeComment->first_name . ' ' . $homeComment->last_name ; ?>:</span><br/>
								 <?php echo $homeComment->message; ?> </span><br/>
                                                        <span class="time"><?php echo $this->quick->timeAgo($time_ago); ?></span>
                                                    </a>
                                                </li>
						<?php }} ?>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                <!-- END GROUP NOTIFICATION -->
                                <!-- BEGIN GROUP INFORMATION -->
                                <div class="btn-group-red btn-group">
                                    <button type="button" class="btn btn-sm md-skip dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                        <i class="fa fa-font"></i>
                                    </button>
                                    <ul class="dropdown-menu-v2" role="menu">
                                        <li <?php if($this -> session -> userdata['user']['language']=='english'){ ?>class="active"<?php } ?>>
                                            <a href="<?php echo base_url(); ?>homeportal/english">English</a>
                                        </li>
                                        <li <?php if($this -> session -> userdata['user']['language']=='chinese'){ ?>class="active"<?php } ?>>
                                            <a href="<?php echo base_url(); ?>homeportal/chinese">Traditional Chinese</a>
                                        </li>
					<li <?php if($this -> session -> userdata['user']['language']=='simplifiedchinese'){ ?>class="active"<?php } ?>>
					    <a href="<?php echo base_url(); ?>homeportal/simplifiedchinese">Simplified Chinese</a>
					</li>
					<li <?php if($this -> session -> userdata['user']['language']=='korean'){ ?>class="active"<?php } ?>>
					    <a href="<?php echo base_url(); ?>homeportal/korean">Korean</a>
					</li>
                                    </ul>
                                </div>
                                <!-- END GROUP INFORMATION -->
                                <!-- BEGIN USER PROFILE -->
                                <div class="btn-group-img btn-group">
                                    <button type="button" class="btn btn-sm md-skip dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                        <span><?php
							$user = $this -> ion_auth -> user() -> row();
							if (!empty($user -> first_name)) { echo $user -> first_name . ' ' . $user -> last_name; }
						?></span>
                                        <!--<img src="../assets/layouts/layout5/img/avatar1.jpg" alt="">--> </button>
                                    <ul class="dropdown-menu-v2" role="menu">
                                        <li>
                                            <a href="<?php echo base_url("index.php/homePortalEdu"); ?>">
                                                <i class="icon-user"></i> <?php echo $this -> lang -> line('MY_PROFILE'); ?>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="http://www.seamlesscare.ca/terms-of-use/">
                                                <i class="fa fa-cog"></i> <?php echo $this -> lang -> line('TERMS_OF_USE'); ?> </a>
                                        </li>
                                        <li>
                                            <a href="http://www.seamlesscare.ca/privacy-policy/">
                                                <i class="fa fa-eye"></i> <?php echo $this -> lang -> line('PRIVACY_SETTINGS'); ?>
                                            </a>
                                        </li>
                                        <li class="divider"> </li>
                                        <li>
                                            <a href="<?php echo base_url(); ?>index.php/auth/logout">
                                                <i class="icon-key"></i> <?php echo $this -> lang -> line('LOG_OUT'); ?> </a>
                                        </li>
                                    </ul>
                                </div>
                                <!-- END USER PROFILE -->
                                <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                                <!--<button type="button" class="quick-sidebar-toggler md-skip" data-toggle="collapse">
                                    <span class="sr-only">Toggle Quick Sidebar</span>
                                    <i class="icon-logout"></i>
                                </button>-->
                                <!-- END QUICK SIDEBAR TOGGLER -->
                            </div>
                            <!-- END TOPBAR ACTIONS -->
                        </div>


			<?php 				
						
				   $page = "";
				   $page1 = "";
				   if($this->ion_auth->in_group ( 'pharmacyuser' ))   { $page = ""; $page1 ="tutorials"; }
				   if($this->ion_auth->in_group ( 'homeportaluser' )) { $page = "homeportal"; $page1 ="tutorials"; }		
				   
				
			?>


			<!-- BEGIN HEADER MENU -->
                        <div class="nav-collapse collapse navbar-collapse navbar-responsive-collapse">
                            <ul class="nav navbar-nav">
                                <li class="dropdown dropdown-fw dropdown-fw-disabled  active open selected">
                                    <a href="javascript:;" class="text-uppercase">
                                        <i class="icon-home"></i> Dashboard </a>
                                    <ul class="dropdown-menu dropdown-menu-fw">
                                        <?php   if($this->ion_auth->in_group ( 'homeportaluser' )) {   ?>

					<?php  } if($this->ion_auth->in_group ( 'pharmacyuser' )) {   ?>

					<li <?php if ($this -> uri -> segment(2, 0) == 'tutorials' && $this -> uri -> segment(2, 0) != '0') { echo 'class="active"'; }?> >
						<a href="<?php echo base_url("index.php/homePortalEdu/tutorials"); ?>"> 
							<i class="fa fa-book fa-fw"></i>Tutorials
						</a>
					</li>

					<li <?php if ($this -> uri -> segment(2, 0) == 'updateQuizzes' && $this -> uri -> segment(2, 0) != '0') { echo 'class="active"'; }?> >
						<a href="<?php echo base_url("index.php/homePortalEdu/updateQuizzes"); ?>"> 
							<i class="fa fa-ticket fa-fw"></i>Quizzes
						</a>
					</li>
					
					<li <?php if ($this -> uri -> segment(2, 0) == 'certificate' && $this -> uri -> segment(2, 0) != '0') { echo 'class="active"'; }?> >
						<a href="<?php echo base_url("index.php/homePortalEdu/certificate"); ?>"> 
							<i class="fa fa-certificate"></i>Certificate
						</a>
					</li>
					
											
					
					<?php } ?>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <!-- END HEADER MENU -->
			</div>
                    <!--/container-->
                </nav>
            </header>



<!-- New Html End -->




<!--



<body>
	
	<header class="navbar clearfix navbar-fixed-top" id="header">
		<div class="container">
			<div class="navbar-brand">
				
				<a href="<?php echo base_url("index.php/homeportal/"); ?>"> 
					<img src="<?php echo base_url(); ?>img/logo/logo.gif" alt="Cloud Admin Logo" class="img-responsive" height="30" width="120">
				</a>
								
				
				
				<div class="visible-xs">
					<a href="#" class="team-status-toggle switcher btn dropdown-toggle"> <i class="fa fa-users"></i></a>
				</div>
				
				
				<div id="sidebar-collapse" class="sidebar-collapse btn">
					<i class="fa fa-bars" data-icon1="fa fa-bars" data-icon2="fa fa-bars"></i>
				</div>	
				
							
			</div>			 
		 	<?php 				
						
				   $page = "";
				   $page1 = "";
				   if($this->ion_auth->in_group ( 'pharmacyuser' ))   { $page = ""; $page1 ="tutorials"; }
				   if($this->ion_auth->in_group ( 'homeportaluser' )) { $page = "homeportal"; $page1 ="tutorials"; }		
				   
				
			?>	
				 
			<ul class="nav navbar-nav pull-left hidden-xs" id="navbar-left">
				<li class="dropdown">
					<a href="<?php echo base_url().$page; ?>" class="dropdown-toggle tip-bottom"  title="Home Portal"> 
						<i class="fa fa-users"></i> <span class="name">Home Portal</span> <i class="fa fa-angle-down"></i>
				</a>
				</li>
			</ul>
			
			<ul class="nav navbar-nav pull-left hidden-xs" id="navbar-left">
				<li class="dropdown">
					<a href="<?php echo base_url() . 'index.php/homePortalEdu/'.$page1;?>" class="dropdown-toggle tip-bottom" data-toggle="tooltip" title="Education Portal"> 
						<i class="fa fa-users"></i> <span class="name">Education Portal</span> <i class="fa fa-angle-down"></i>
				</a>
				</li>
			</ul>			
			
							
	
			 
			
			<ul class="nav navbar-nav pull-right">
				
				<li class="dropdown" id="header-notification"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-bell"></i></a>
					<ul class="dropdown-menu notification">
						<li class="dropdown-title"><span><i class="fa fa-bell"></i> 0 Notifications</span> </li>
						<li>
							<a href="#">
								<span class="label label-success"> <i class="fa fa-user"></i></span>
								<span class="body"> <span class="message">No notifications </span> 
									<span class="time"> <i class="fa fa-clock-o"></i> <span>Just now</span> </span>
								</span>
							</a>
						</li>
						<li class="footer"><a href="#">See all notifications <i class="fa fa-arrow-circle-right"></i></a></li>
					</ul>
				</li>
				
				
				<li class="dropdown" id="header-message">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-envelope"></i> </a>
					<ul class="dropdown-menu inbox">
						<li class="dropdown-title">
							<span><i class="fa fa-envelope-o"></i>Messages</span> 
							<span class="compose pull-right tip-right" title="Compose message"><i class="fa fa-pencil-square-o"></i></span>
						</li>
						<?php 
							$homeComments = $this->comp->getNewHomeComments();
							if ($homeComments) {
								foreach ( $homeComments->result () as $homeComment ) {
									$time_ago =strtotime($homeComment->messageDate);																											
							?>						
						<li  onclick="loadModal('<?php echo base_url('index.php/compliance/editTicketModal/') . '/' . 0 . '/' . $homeComment->ticketID ;?>')">
							<a> 
								<span class="body"> 
									<span class="from"><?php echo $homeComment->first_name . ' ' . $homeComment->last_name ; ?> </span> 
									<span class="message"> <?php echo $homeComment->message; ?> </span> 
									<span class="time"> <i class="fa fa-clock-o"></i> <span><?php echo $this->quick->timeAgo($time_ago); ?></span></span>
								</span>
							</a>
						</li>
						<?php }}?>					
						
						<li class="footer"><a href="#">See all messages <i class="fa fa-arrow-circle-right"></i></a></li>
					</ul>
				</li>
				
				
				<li class="dropdown" id="header-notification">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-font"></i> </a>
					<ul class="dropdown-menu notification">
						<li class="dropdown-title"><span><i class="fa fa-font"></i> Selected: <?php echo $this -> session -> userdata['user']['language']; ?></span></li>
						<li><a href="<?php echo base_url(); ?>homeportal/english"> <span class="body"> <span class="message">English</span></span></a></li>
						<li><a href="<?php echo base_url(); ?>homeportal/chinese"> <span class="body"> <span class="message">Traditional Chinese</span></span></a></li>
						<li><a href="<?php echo base_url(); ?>homeportal/simplifiedchinese"> <span class="body"> <span class="message">Simplified Chinese</span></span></a></li>
						<li><a href="<?php echo base_url(); ?>homeportal/korean"> <span class="body"> <span class="message">Korean</span></span></a></li>						
					</ul>
				</li>
				
				
				<li class="dropdown user" id="header-user">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="username">
						<?php
							$user = $this -> ion_auth -> user() -> row();
							if (!empty($user -> first_name)) { echo $user -> first_name . ' ' . $user -> last_name; }
						?>
						</span> <i class="fa fa-angle-down"></i>
					</a>
					
					<ul class="dropdown-menu">

						<li><a href="<?php echo base_url("index.php/homePortalEdu"); ?>"><i class="fa fa-user"></i> <?php echo $this -> lang -> line('MY_PROFILE'); ?></a></li>
						<li><a href="http://www.seamlesscare.ca/terms-of-use/"
							target="_blank"><i class="fa fa-cog"></i> <?php echo $this -> lang -> line('TERMS_OF_USE'); ?></a></li>
						<li><a href="http://www.seamlesscare.ca/privacy-policy/"
							target="_blank"><i class="fa fa-eye"></i> <?php echo $this -> lang -> line('PRIVACY_SETTINGS'); ?></a></li>	
						<li><a href="<?php echo base_url(); ?>index.php/auth/logout"><i class="fa fa-power-off"></i> <?php echo $this -> lang -> line('LOG_OUT'); ?></a></li>
					</ul>
				</li>
				
			</ul>
			
		</div>
		
		
		<div class="container team-status" id="team-status">
			<div id="scrollbar"> <div class="handle"></div></div>
			<div id="teamslider">
				<h1>
					Phone (905) 432 9900 <a href="http://seamlesscare.ca/inquiry.html"> Click to send us feedback</a>
				</h1>
			</div>
		</div>
		
	</header>
	
	<section id="page">
		
		<div id="sidebar" class="sidebar sidebar-fixed">
			<div class="sidebar-menu nav-collapse">
				<div class="divide-20"></div>
				
				<ul>

					
					
					<?php   if($this->ion_auth->in_group ( 'homeportaluser' )) {   ?>
						
					
					
					
					<?php  } if($this->ion_auth->in_group ( 'pharmacyuser' )) {   ?>
						
					
					
					<li <?php if ($this -> uri -> segment(2, 0) == 'tutorials' && $this -> uri -> segment(2, 0) != '0') { echo 'class="active"'; }?> >
						<a href="<?php echo base_url("index.php/homePortalEdu/tutorials"); ?>"> 
							<i class="fa fa-book fa-fw"></i> <span class="menu-text">Tutorials</span>
						</a>
					</li>
					
					<li <?php if ($this -> uri -> segment(2, 0) == 'updateQuizzes' && $this -> uri -> segment(2, 0) != '0') { echo 'class="active"'; }?> >
						<a href="<?php echo base_url("index.php/homePortalEdu/updateQuizzes"); ?>"> 
							<i class="fa fa-ticket fa-fw"></i> <span class="menu-text">Quizzes</span>
						</a>
					</li>
					
					<li <?php if ($this -> uri -> segment(2, 0) == 'certificate' && $this -> uri -> segment(2, 0) != '0') { echo 'class="active"'; }?> >
						<a href="<?php echo base_url("index.php/homePortalEdu/certificate"); ?>"> 
							<i class="fa fa-certificate"></i> <span class="menu-text">Certificate</span>
						</a>
					</li>
					
											
					
					<?php } ?>
					
								
				</ul>
				
			</div>
		</div>
		
		<div id="main-content">
			<div class="container">
				<div class="row">
					<div id="content" class="col-lg-12">
						<!-- PAGE HEADER-->
						<!--<div class="row">
							<div class="col-sm-12">
								<div class="page-header">-->
