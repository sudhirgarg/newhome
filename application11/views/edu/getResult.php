 <?php
    	$id = "";
   		foreach ($info as $raw) {
   			$id = $raw->topicsId;
			//echo "Name   :". $raw->gQname ."<br/>"; 
			//echo "Details: ". $raw->details ."<br/>";
			//echo "Time Duration : ". $raw->timeDutation;  
			//echo "EDIT";
		}   
		
		$courseName = "";
		foreach($topicsInfo AS $tInfo){
			$courseName = $tInfo->topicsName;
		} 	   
   ?> 

<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left">Result</h3>
</div>
<div class="description">
	<?php 				 
			echo '<a  class="btn btn-lg btn-default"  href="'.base_url().'homePortalEdu/tutorials/"><img src="'.base_url().'img/Arrow-Back-icon.png"  width="24px" >Go Back</a>';	
		
	 ?>
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DELIVERY RUNS CONTENT -->
<!-- DATA TABLES -->

<!--***************************************** PAGE BODY STARTS ***************************************************************-->

<div class="row">
	<div class="col-md-6">
		<div class="panel panel-info">			
	    <div class="panel-heading">
	        <h3 class="panel-title"><?php echo ucfirst($courseName); ?></h3>
	    </div>
	    
	     <div class="panel-body">	   	   
		    <div class="panel panel-info my">
			  <div class="panel-body">
			       <div class="bs-example">
		    			Your Score: <?php echo round($score);?> %
						<div class="progress progress-striped active">
							 <div class="progress-bar" style="width: <?php echo round($score);?>%;"> <span class="sr-only"><?php echo round($score);?>% Complete</span></div>
						</div>
					</div>
			  </div>
			</div>
       
	    	 <div class="panel panel-info">
			  <div class="panel-body">
			  
			  
		    	<?php
		    	
		    	if (round($score) >= 70) {		    		
		    		echo '<div class="alert alert-success">
							  <strong>Congratulations!</strong> You have passed. Please contact the pharmacy to schedule the practical and certification.
							</div>';
		    	}else{
		    	
		    	    echo '<div class="alert alert-warning">
							  <strong>You have not passed!</strong> Please review the modules and attempt the test again.
							</div>';
		    	}
		    	
		    	
		    	 /*
		    		$i = 1;
		    		foreach($quiz AS $raw){
		    			
		    			$raw->question;
						$answers = explode(",", $raw->quizzesAnswer);
						$answers = array_filter($answers);
						shuffle($answers);						
						$candidateAnswers = explode(",", $raw->answer);
						$countAnswer = count($answers);
						$countCandidateAnswers = count($candidateAnswers);						
						
						$A1 = "";
					
						if($raw->answerType == 3) {
							$inputType = 'checkbox';	         										
							
						}else{
							$inputType = 'radio';
							for($c= 0; $c < $countCandidateAnswers; $c++){
	    			             $A1 = $candidateAnswers[$c];			
	    		         	}
						}
						
						$a = 0;
						$c = 0;
						
		    								
		    		echo '<ul class="list-unstyled">
						<li>
						  '.$i++.') '.$raw->question.'
						</li>';
						for($a=0; $a < $countAnswer; $a++){
							if($raw->answerType == 3){
								
								 echo '<li><input type="'.$inputType.'" name="answer[]" id="answer[]"';
								  if(in_array($answers[$a],$candidateAnswers)) { echo ' checked="checked"'; } 
								 echo 'value="'.$answers[$a].'">'.$answers[$a].'</li>';	
								
							}else{
								 echo '<li><input type="'.$inputType.'" name="answer'.$i.'" id="answer[]"';								   								  
									  if($answers[$a] == $A1) { echo ' checked="checked"'; }  	
								 echo ' value="'.$answers[$a].'">'.$answers[$a].'</li>';	
							}			
						}					
					echo'<li> Right Answer: '.$raw->correctAnswer.'</li>';
								
		    		echo '</ul><br/>';	
		    		
				}		    	
				 */
		    	
		    	?>		    		
		    											    	
	    	</div>
			</div>		
		</div>
		</div>
	</div>
</div>



<!--***************************************** PAGE BODY ENDS ***************************************************************-->


<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>