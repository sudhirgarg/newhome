	 <?php
		$isNew = false;
		if ($this->uri->segment ( 3 ) == '0') {
			$isNew = true;
		}
		
		if (! $isNew) {
			$policy = $this->policy->getPolicies ( '0', $this->uri->segment ( 3 ) );
		}
		?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"
		aria-hidden="true">&times;</button>
	<h4 class="modal-title">New Policy</h4>
</div>
<div class="modal-body">
	<div class="form-horizontal" role="form">

		<input id="policyID" value="<?php if (! $isNew) {echo $policy->policyID;}else{echo '0';}?>"
			type="hidden">
		<div class="form-group">
			<label class="col-sm-3 control-label">Policy Title</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" placeholder="Text input"
					id="policyTitle" value="<?php if (! $isNew) {echo $policy->policyTitle;}?>">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label">Policy Area</label>
			<div class="col-sm-9">
				<select class="form-control" id="policyAreaID">
							<?php
							$policyAreas = $this->policy->getPolicyArea ();
							foreach ( $policyAreas->result () as $policyArea ) {
								?>
								?><option value="<?php echo $policyArea->policyAreaID; ?>"
						<?php if ((! $isNew) && $policy->policyAreaID == $policyArea->policyAreaID) { echo ' selected="selected"';} ?>><?php echo $policyArea->policyAreaName;?></option><?php }?>
							</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label">Created By</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" placeholder=""
					id="approvedBy" disabled
					value="<?php if (! $isNew) {$user= $this->ion_auth->user($policy->createdBy)->row(); echo $user->first_name . ' ' . $user->last_name ;}?>">
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label">Approved On</label>
			<div class="col-sm-9">
				<input type="text" name="regular" id="approvedOn"
					class="form-control" disabled
					value="<?php if (! $isNew) {echo $policy->dateCreated;}?>">
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label">Revision Date</label>
			<div class="col-sm-9">
				<input type="text" name="regular" id="revisionDate"
					class="form-control datepicker-fullscreen"
					value="<?php if (! $isNew) {echo $policy->dateRevised;}?>">
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label">Purpose</label>
			<div class="col-sm-9">
				<textarea class="form-control" rows="5" id="purpose"><?php if (! $isNew) {echo $policy->purpose;}?></textarea>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label">Policy</label>
			<div class="col-sm-9">
				<textarea class="form-control" rows="5" id="policy"><?php if (! $isNew) {echo $policy->policy;}?></textarea>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label">Procedures</label>
			<div class="col-sm-9">
				<textarea class="form-control" rows="25" id="procedure"><?php if (! $isNew) {echo $policy->procedures;}?></textarea>
			</div>
		</div>

	</div>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal"
		id="closeButtonModal">Close</button>
	<button type="button" class="btn btn-primary" id="modifyPolicyButton"
		onclick="modifyPolicy()">Save changes</button>
</div>