
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"
		aria-hidden="true">&times;</button>
	<h4 class="modal-title">New Policy Area</h4>
</div>
<div class="modal-body">
	<div class="form-horizontal">
		<div class="form-group">
			<label class="col-sm-3 control-label">Policy Area</label>
			<div class="col-sm-9">
				<input type="text" name="regular" class="form-control" placeholder="Name of Policy Area" id="policyAreaName">
			</div>
		</div>
	</div>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal" id="closeModalButton">Close</button>
	<button type="button" class="btn btn-primary" onclick="newPolicyArea()" id="savePolicyAreaButton" data-loading-text="Saving...">Save changes</button>
</div> 
