<script>

	function deleteKPI()
	{
		$.post('<?php echo site_url('filingCab/deleteKPI');?>',
				{
					KPIID: <?php echo $this->uri->segment('3', '-1');?>,
					active: '0'
				}, function(data) {
					read(data);
					window.location.replace('<?php echo base_url().'filingCab/KPIDash';?>');
				});
	}


	function newKPIPoint()
	{
		loadModal('<?php echo site_url('filingCab/addNewKPIPoint');?>');
	}

	function saveDataPoint()
	{
		$.post('<?php echo site_url('filingCab/updateKPIPoint'); ?>',
				{
					KPIID: <?php echo $this->uri->segment('3', '-1');?>,
					KPIDetailID: $("#KPIDetailID").val(),
					pointValue: $("#pointValue").val()
				}, function(data) {
					read(data);
					$("#closeModalButton").click();
					var args = data.split('|');
			        if (args[0] == 1)
			        {
			            setTimeout(function() {
			                location.reload();
			                }, 1000);
			                
			        }
				});
	}

	function deleteDataPoint($KPIPoint)
	{
		$.post('<?php echo site_url('filingCab/deleteKPIPoint'); ?>',
				{
					KPIDetailID: $KPIPoint,
					active: '0'
				}, function(data) {
					read(data);
					var args = data.split('|');
			        if (args[0] == 1)
			        {
			            setTimeout(function() {
			                location.reload();
			                }, 1000);
			                
			        }
				});
	}
	
	function saveKPI()
    {
        $("#saveKPIButton").button('loading');
        $.post('<?php echo site_url('filingCab/updateKPI');?>', 
                {KPIID:<?php echo $this->uri->segment('3', '-1');?>, 
            	indicatorName: $("#indicatorName").val(),  
            	indicatorDescription: $("#indicatorDescription").val(), 
            	minValue: $("#minValue").val(), 
            	maxValue: $("#maxValue").val(), 
            	targetValue: $("#targetValue").val()}, 
        function (data) {
            		read(data);
            $("#closeButtonModal").click();
            var args = data.split('|');
            if (args[0] == 1)
            {
                setTimeout(function() {
                    location.reload();
                    }, 1000);
                    
            }
        });
    }
</script>



<script type="text/javascript">
		jQuery(document).ready(function() {	
			$(".datepicker-fullscreen").pickadate();
<?php
$KPIID = $this->uri->segment ( 3 );
$KPI = $this->kpi->getKPI ( $KPIID );
$KPIPoints = $this->kpi->getKPIPoints ( $KPIID );
if ($KPIPoints) {
	?>
	var data = [ {
		label: "Value",
		data: [ 
		<?php
	$KPIPointCount = 1;
	foreach ( $KPIPoints->result () as $KPIPoint ) {
		if ($KPIPointCount != 1) {
			echo ',';
		}
		echo '[' . $KPIPointCount . ',' . $KPIPoint->pointValue . ']';
		$KPIPointCount ++;
	}
	?>],
		color: "green"
	},{ label: "Target", data: [ [1, <?php echo $KPI->targetValue;?>], [2, <?php echo $KPI->targetValue;?>], [3, <?php echo $KPI->targetValue;?>], [4, <?php echo $KPI->targetValue;?>], [5, <?php echo $KPI->targetValue;?>], [6,<?php echo $KPI->targetValue;?>], [7,<?php echo $KPI->targetValue;?>], [8, <?php echo $KPI->targetValue;?>], [9, <?php echo $KPI->targetValue;?>], [10, <?php echo $KPI->targetValue;?>], [11,<?php echo $KPI->targetValue;?>], [12,<?php echo $KPI->targetValue;?>] ],
		 color: "red" }];
	$.plot("#mychart", data);
	<?php
}
?>

			Charts.initCharts();		
		});
	</script>