<script>

function saveHome()
{
    $("#saveButton").button('loading');
    $.post('<?php echo site_url('compliance/editHome');?>',
             {
        homeID: $("#homeID").val(),
        homeName: $("#homeName").val(),
        homeAddress: $("#homeAddress").val(),
        contact: $("#contact").val(),
        phone: $("#phone").val()    
        }, 
    function (data) {
        $("#closeButtonModal").click();
        read(data);
        
        var args = data.split('|');
        if (args[0] == 1)
        {
            setTimeout(function() {
                location.reload();
                }, 1000);
                
        }
    });
}

</script>


