
<script>

function saveMedChange()
{
	 $("#saveButton").button('loading');
	    $.post('<?php echo site_url('compliance/saveMedChange');?>',
	             {
	        medChangeID: $("#medChangeID").val(),
	        patID: $("#patID").val(),
	        dateOfChange: $("#dateOfChange").val(),
	        changeText: $("#changeText").val(),
	        actionText: $("#actionText").val(),
	        technicianUserID: $("#technicianUserID").val(),
	        pharmacistUserID: $("#pharmacistUserID").val(),
	        active: $("#active").val(),
	        startDate: $("#startDate").val(),
	        endDate: $("#endDate").val(),
	        actionComplete: $("#actionComplete").val(),
	        spares : $('input[name=spares]:checked').val(),
	        marval : $('input[name=marval]:checked').val(),
	        eLabels : $('input[name=eLabels]:checked').val(),
	        numOfStrips: $("#numOfStrips").val(),
	        rdd: $("#rdd").val(),
	        batchNum : $('input[name=batchNum]:checked').val()
	        }, 
	    function (data) {
	    	//alert(data);
	        $("#closeButtonModal").click();
	        read(data);
	        var args = data.split('|');
	        if (args[0] == 1)
	        {
	            setTimeout(function() {
	                location.reload();
	                }, 1000);
	                
	        }
	    });	
}


function deleteMedChange()
{
	
	 $("#deleteButton").button('loading');
	 
	 if (window.confirm("Are you sure?")) { 
	 	
	    $.post('<?php echo site_url('compliance/saveMedChange');?>',
	             {
	        medChangeID: $("#medChangeID").val(),
	        patID: $("#patID").val(),
	        dateOfChange: $("#dateOfChange").val(),
	        changeText: $("#changeText").val(),
	        actionText: $("#actionText").val(),
	        technicianUserID: $("#technicianUserID").val(),
	        pharmacistUserID:  $("#pharmacistUserID").val(),
	        active: 0,
	        startDate: $("#startDate").val(),
	        endDate: $("#endDate").val(),
	        actionComplete: $("#actionComplete").val()
	        }, 
	    function (data) {
	        $("#closeButtonModal").click();
	        read(data);
	        var args = data.split('|');
	        if (args[0] == 1)
	        {
	            setTimeout(function() {
	                location.reload();
	                }, 1000);
	                
	        }
	    });	
	 }else{
	 	
	 	 setTimeout(function() {
            location.reload();
         }, 1000);	 	
	 }
}

function pharmacistCheckButtonModal()
{

	
	 $("#pharmacistCheckButton").button('loading');
	    $.post('<?php echo site_url('compliance/pharmacistCheckMedChange');?>',
	             {
            
	        medChangeID: $("#medChangeID").val(),
	        patID: $("#patID").val(),
	        dateOfChange: $("#dateOfChange").val(),
	        changeText: $("#changeText").val(),
	        actionText: $("#actionText").val(),
	        technicianUserID: $("#technicianUserID").val(),
	        active: $("#active").val(),
	        startDate: $("#startDate").val(),
	        endDate: $("#endDate").val(),
	        actionComplete: $("#actionComplete").val()
	        }, 
	    function (data) {
	        $("#closeButtonModal").click();
	        read(data);
	        var args = data.split('|');
	        if (args[0] == 1)
	        {
	            setTimeout(function() {
	                location.reload();
	                }, 1000);
	                
	        }
	    });	
}


	function actionCompleteButtonModal(){
		 $("#actionCompleteButton").button('loading');
		    $.post('<?php echo site_url('compliance/actionCompleteMedChange');?>', {
		    	
		        medChangeID: $("#medChangeID").val(),
		        patID: $("#patID").val(),
		        dateOfChange: $("#dateOfChange").val(),
		        changeText: $("#changeText").val(),
		        actionText: $("#actionText").val(),
		        technicianUserID: $("#technicianUserID").val(),
		        pharmacistUserID: $("#pharmacistUserID").val(),
		        active: $("#active").val(),
		        startDate: $("#startDate").val(),
		        endDate: $("#endDate").val(),
		        actionComplete: 1,
		        spares : $('input[name=spares]:checked').val(),
		        marval : $('input[name=marval]:checked').val(),
		        eLabels : $('input[name=eLabels]:checked').val(),
		        numOfStrips: $("#numOfStrips").val(),
		        rdd: $("#rdd").val(),
		        batchNum : $('input[name=batchNum]:checked').val()
		    },function (data) {
		        $("#closeButtonModal").click();
		        read(data);
		        var args = data.split('|');
		        if (args[0] == 1)
		        {
		            setTimeout(function() {
		                location.reload();
		                }, 1000);
		                
		        }
		    });	
	}
	
	
	function actionCompleted(){
		$("#actionComplete").button('loading');
		
		$.post('<?php echo base_url();?>compliance/actionComleted',{
			
			    medChangeID: $("#medChangeID").val(),
		        patID: $("#patID").val(),
		        dateOfChange: $("#dateOfChange").val(),
		        changeText: $("#changeText").val(),
		        actionText: $("#actionText").val(),
		        technicianUserID: $("#technicianUserID").val(),
		        pharmacistUserID: $("#pharmacistUserID").val(),
		        active: $("#active").val(),
		        startDate: $("#startDate").val(),
		        endDate: $("#endDate").val(),
		        actionComplete: 1,
		        spares : $('input[name=spares]:checked').val(),
		        marval : $('input[name=marval]:checked').val(),
		        eLabels : $('input[name=eLabels]:checked').val(),
		        numOfStrips: $("#numOfStrips").val(),
		        rdd: $("#rdd").val(),
		        batchNum : $('input[name=batchNum]:checked').val()
			
		},function(data){
			   $("#closeButtonModal").click();
			   
			   
		        read(data);
		        var args = data.split('|');
		        if (args[0] == 1){
		            setTimeout(function() {
		                location.reload();
		            }, 1000);			                
		        }
		});		
	}

    </script>