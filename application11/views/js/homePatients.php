<script>

function saveHome()
{
    $("#saveButton").button('loading');
    $.post('<?php echo site_url('compliance/editHome');?>',
             {
        homeID: $("#homeID").val(),
        homeName: $("#homeName").val(),
        homeAddress: $("#homeAddress").val(),
        contact: $("#contact").val(),
        phone: $("#phone").val()    
        }, 
    function (data) {
        $("#closeButtonModal").click();
        read(data);
        var args = data.split('|');
        if (args[0] == 1)
        {
            setTimeout(function() {
                location.reload();
                }, 1000);
                
        }
    });
}


function addUserToHome()
{
    $("#addUserButton").button('loading');
    $.post('<?php echo site_url('compliance/addUserToHome');?>',
             {
        homeID: $("#homeID").val(),
        email: $("#email").val()
        }, 
    function (data) {
        $("#closeAddUserButtonModal").click();
        read(data);
        var args = data.split('|');
        if (args[0] == 1)
        {
            setTimeout(function() {
                location.reload();
                }, 1000);
                
        }
    });
}

function removeAccess($groupID, $userID)
{
    $.post('<?php echo site_url('compliance/removeGroupUserAccess');?>',
             {
        homeID: $groupID,
        userID: $userID
        }, 
    function (data) {
        $("#closeAddUserButtonModal").click();
        read(data);
        var args = data.split('|');
        if (args[0] == 1)
        {
            setTimeout(function() {
                location.reload();
                }, 1000);               
        }
    });
}

</script>


