<script>
    function newPolicyArea()
    {
        $("#savePolicyAreaButton").button('loading');
        $.post('<?php echo site_url('filingCab/newPolicyArea');?>',
                 {
            policyAreaName: $("#policyAreaName").val()
            }, 
        function (data) {
            $("#closeButtonModal").click();
            read(data);
            var args = data.split('|');
            if (args[0] == 1)
            {
                setTimeout(function() {
                    location.reload();
                    }, 1000);
                    
            }
        });
    }

    function newPolicy()
    {
		$("#savePolicyButton").button('loading');
		$.post('<?php echo site_url('filingCab/newPolicy');?>',
                {
			policyTitle: $("#policyTitle").val(),
			policyAreaID: $("#policyAreaID").val(),
			approvedBy: $("#approvedBy").val(),
			approvedOn: $("#apporvedOn").val(),
			revisionDate: $("#revisionDate").val(),
			purpose: $("#purpose").val(),
			policy: $("#policy").val(),
			procedure: $("#procedure").val()			
           }, 
       function (data) {
           $("#closeButtonModal").click();
           read(data);
           var args = data.split('|');
           if (args[0] == 1)
           {
               setTimeout(function() {
                   location.reload();
                   }, 1000);
                   
           }
       });

    }

    function modifyPolicyArea(policyAreaID)
    {
    	$("#savePolicyAreaButton").button('loading');
		$.post('<?php echo site_url('filingCab/editPolicyArea');?>',
                {
			policyAreaID: policyAreaID,
			policyAreaName: $("#policyAreaName").val()		
           }, 
       function (data) {
           $("#closeButtonModal").click();
           read(data);
           var args = data.split('|');
           if (args[0] == 1)
           {
               setTimeout(function() {
                   location.reload();
                   }, 1000);
                   
           }
       });
    }

    function modifyPolicy()
    {
    	$("#modifyPolicyButton").button('loading');
		$.post('<?php echo site_url('filingCab/editPolicy');?>',
                {
            policyID: $('#policyID').val(),
			policyTitle: $("#policyTitle").val(),
			policyAreaID: $("#policyAreaID").val(),
			approvedBy: $("#approvedBy").val(),
			approvedOn: $("#apporvedOn").val(),
			revisionDate: $("#revisionDate").val(),
			purpose: $("#purpose").val(),
			policy: $("#policy").val(),
			procedure: $("#procedure").val(),
			active: 1		
           }, 
       function (data) {
           $("#closeButtonModal").click();
           read(data);
           var args = data.split('|');
           if (args[0] == 1)
           {
               setTimeout(function() {
                   location.reload();
                   }, 1000);
                   
           }
       });
    }
    
    </script>