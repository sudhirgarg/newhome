<script>
    function saveCommittee()
    {
        $("#saveCommitteeButton").button('loading');
        $.post('<?php echo site_url('filingCab/updateCommittee');?>', {committeeID: $("#committeeID").val(), committeeName: $("#committeeName").val(),  committeeDescription: $("#committeeDescription").val(), active: '1'}, 
        function (data) {
            $("#closeButtonModal").click();
            read(data);
            var args = data.split('|');
            if (args[0] == 1)
            {
                setTimeout(function() {
                    location.reload();
                    }, 1000);
                    
            }
        });
    }
    
    function archive(committeeID)
    {
        $("#saveCommitteeButton").button('loading');
        $.post('<?php echo site_url('filingCab/updateCommittee');?>', {
            committeeID: committeeID, 
            committeeName: '',  
            committeeDescription: '', 
            active: '0'}, 
        function (data) {
            $("#closeButtonModal").click();
            read(data);           
            var args = data.split('|');
            if (args[0] == 1)
            {
                setTimeout(function() {
                    location.reload();
                    }, 1000);
                    
            }
        });
    }
    
    function newMeeting()
    {
        loadModal('<?php echo site_url('filingCab/editMinutesModal/-1');?>');
    }
    
    function saveMeeting()
    {
        $("#saveMinutesButton").button('loading');
        $.post('<?php echo site_url('filingCab/updateMinutes');  ?>', 
        {   committeeMinutesID:     $("#committeeMinutesID").val(), 
            dateCreated:            $("#meetingDate").val(), 
            title:                  $("#minutesTitle").val(), 
            committeeID:            $("#committeeID").val(), 
            present:                $("#present").val(),
            discussion:             $("#meetingDiscussion").val(),
            active: '1'
        }, 
        function(data) {
            read(data);
            location.reload();
        });
    }
</script>  