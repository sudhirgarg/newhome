<style>
    .modal-dialog {
        width:60%;
    }

</style>

<script>


function saveTicket()
{
	 $("#saveTicketButton").button('loading');
	    $.post('<?php echo site_url('homeport/saveTicket');?>',
	             {
	        ticketID: $("#ticketID").val(),
	        createdBy: $("#createdBy").val(),
	        dateCreated: $("#dateCreated").val(),
	        dateComplete: $("#dateComplete").val(),
	        patID: $("#patID").val(),
	        ticketTypeReferenceID: $("#ticketTypeReferenceID").val(),
	        Description: $("#Description").val(),
	        active: $("#active").val()
	        }, 
	    function (data) {
	        $("#closeTicketButtonModal").click();
	        read(data);
	        var args = data.split('|');
	        if (args[0] == 1)
	        {
	            setTimeout(function() {
	                location.reload();
	                }, 1000);
	                
	        }
	    });	
}

function addComment()
{
	 $("#addCommentButton").button('loading');
	    $.post('<?php echo site_url('homeportal/addComment');?>',
	             {
	        ticketID: $("#ticketID").val(),
	        message: $("#message").val()
	        }, 
	    function (data) {
	        var uriString = $("#ticketModalURI").val();
            $.get('<?php echo base_url();?>'+uriString, function(data) {
				$("#myModalContent").html(data);
                });
            read(data);
	    });	
}


function closeTicket()
{

	if(confirm("Are you sure you want to close this ticket?")) {
	$("#closeTicketButton").button('loading');
    $.post('<?php echo site_url('homeportal/saveTicket');?>',
             {
        ticketID: $("#ticketID").val(),
        createdBy: $("#createdBy").val(),
        dateCreated: $("#dateCreated").val(),
        dateComplete: $("#dateComplete").val(),
        patID: $("#patID").val(),
        ticketTypeReferenceID: $("#ticketTypeReferenceID").val(),
        Description: $("#Description").val(),
        active: 0
        }, 
    function (data) {
        $("#closeTicketButtonModal").click();
        read(data);
        var args = data.split('|');
        if (args[0] == 1)
        {
            setTimeout(function() {
                location.reload();
                }, 1000);
                
        }
    });	
	}
}

	 function deleteTicketButton()
	    {
	        $("#deleteButton").button('loading');
	        $.post('<?php echo site_url('homeportal/deleteTicketConfirm');?>', 
	                {
	            	id: $("#ticketID").val()
	                }, 
	        function (data) {
           
                setTimeout(function() {
                    location.reload();
                    }, 1000);
	           
	            
	        });
	    }


</script>