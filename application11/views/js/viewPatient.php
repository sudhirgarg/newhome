<style>
    .modal-dialog {
        width:60%;
    }

</style>


<script>


function saveTicket()
{
	 $("#saveTicketButton").button('loading');
	    $.post('<?php echo site_url('homeportal/saveTicket');?>',
	             {
	        ticketID: $("#ticketID").val(),
	        createdBy: $("#createdBy").val(),
	        dateCreated: $("#dateCreated").val(),
	        dateComplete: $("#dateComplete").val(),
	        patID: $("#patID").val(),
	        ticketTypeReferenceID: $("#ticketTypeReferenceID").val(),
	        Description: $("#Description").val(),
	        active: $("#active").val()
	        }, 
	    function (data) {
	        $("#closeTicketButtonModal").click();
	        read(data);
	        var args = data.split('|');
	        if (args[0] == 1)
	        {
	            setTimeout(function() {
	                location.reload();
	                }, 1000);
	                
	        } 
	    });	
}


function addComment()
{
	 $("#addCommentButton").button('loading');
	    $.post('<?php echo site_url('homeportal/addComment');?>',
	             {
	        ticketID: $("#ticketID").val(),
	        message: $("#message").val()
	        }, 
	    function (data) {
	        var uriString = $("#ticketModalURI").val();
            $.get('<?php echo base_url();?>'+uriString, function(data) {
				$("#myModalContent").html(data);
                });
            read(data);
	    });	
}




function closeTicket()
{
	if(confirm("Are you sure you want to close this ticket?")) {
	$("#closeTicketButton").button('loading');
    $.post('<?php echo site_url('homeportal/saveTicket');?>',
             {
        ticketID: $("#ticketID").val(),
        createdBy: $("#createdBy").val(),
        dateCreated: $("#dateCreated").val(),
        dateComplete: $("#dateComplete").val(),
        patID: $("#patID").val(),
        ticketTypeReferenceID: $("#ticketTypeReferenceID").val(),
        Description: $("#Description").val(),
        active: 0
        }, 
    function (data) {
        $("#closeTicketButtonModal").click();
        read(data);
        var args = data.split('|');
        if (args[0] == 1)
        {
            setTimeout(function() {
                location.reload();
                }, 1000);
                
        }
    });	
	}
}



function reOrderPRNs()
{
	updateTextArea();
		if(confirm("Please confirm to reorder the following prescription numbers: " + $("#reOrderBox").val())) {
	    $.post('<?php echo site_url('homeportal/createReOrder');?>',
	             {
	        	patID: $("#patID").val(),
	      		rxs: $("#reOrderBox").val()
	        }, 
	    function (data) {
	        read(data);
	        alert(data);
	        //setTimeout(function() { location.reload(); }, 1000);
	    });
	    	
		}else{
			//do nothing
		}
}

function updateTextArea() {         
    var allVals = [];
    $('#reOrderList :checked').each(function() {
      allVals.push("| Rx# " + $(this).val() + " comment: " + $(this).closest('tr').find('.reOrderCommentClass').val() );
      
    });
    $('#reOrderBox').val(allVals)
 }

</script>






