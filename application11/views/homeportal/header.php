
<!DOCTYPE html>
<html lang="en">
<head>
	
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title> <?php if($this->uri->segment(2) == "NULL") { echo "Home"; } else { echo $this->uri->segment(2); } ?> </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/cloud-admin.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/themes/default.css" id="skin-switcher">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">	    

     <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    
    <link href="<?php echo base_url(); ?>css/raterater.css" rel="stylesheet"/>
    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/datepicker/themes/default.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/datepicker/themes/default.date.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/datepicker/themes/default.time.min.css" />

	<!-- DATE TIME PICKER -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/bootstrap-datetimepicker.min.css" />
	<!-- script for calender -->
	<script src="<?php echo base_url(); ?>css/fullcalender/jquery.min.js"></script>
	<!-- script for calender end-->
	

    
    <style>
		/* Override star colors */
		.raterater-bg-layer {
		    color: rgba( 0, 0, 0, 0.25 );
		}
		.raterater-hover-layer {
		    color: rgba( 255, 255, 0, 0.75 );
		}
		.raterater-hover-layer.rated {
		    color: rgba( 255, 255, 0, 1 );
		}
		.raterater-rating-layer {
		    color: rgba( 255, 155, 0, 0.75 );
		}
		.raterater-outline-layer {
		    color: rgba( 0, 0, 0, 0.25 );
		}
		
		.ratebox { width : 10px; }
		
		.modal-content{
			
			width:850px;
		}
		</style>
</head>
<body>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-96469535-1', 'auto');
  ga('send', 'pageview');

</script>


	<!-- HEADER -->
	<header class="navbar clearfix navbar-fixed-top" id="header">
		<div class="container">
			<div class="navbar-brand">
				<!-- COMPANY LOGO -->
				<a href="<?php echo base_url("homeportal/"); ?>"> <img
					src="<?php echo base_url(); ?>img/logo/logo.gif"
					alt="Cloud Admin Logo" class="img-responsive" height="30"
					width="120">
				</a>
				<!-- /COMPANY LOGO -->
				<!-- TEAM STATUS FOR MOBILE -->
				<div class="visible-xs">
					<a href="#" class="team-status-toggle switcher btn dropdown-toggle">
						<i class="fa fa-users"></i>
					</a>
				</div>
				<!-- /TEAM STATUS FOR MOBILE -->

				<!-- SIDEBAR COLLAPSE -->
				<div id="sidebar-collapse" class="sidebar-collapse btn">
					<i class="fa fa-bars" data-icon1="fa fa-bars"
						data-icon2="fa fa-bars"></i>
				</div>
				<!-- /SIDEBAR COLLAPSE -->
			</div>			
			
			 
			
							 
			<ul class="nav navbar-nav pull-left hidden-xs" id="navbar-left">
				<li class="dropdown">
					<a href="<?php echo base_url().'homeportal'; ?>"" class="dropdown-toggle tip-bottom"  title="Home Portal"> 
						<i class="fa fa-users"></i> <span class="name">Home Portal</span> <i class="fa fa-angle-down"></i>
				</a>
				</li>
			</ul>
			
			<ul class="nav navbar-nav pull-left hidden-xs" id="navbar-left">
				<li class="dropdown">
					<a href="<?php echo base_url() . 'homePortalEdu/tutorials'?>" class="dropdown-toggle tip-bottom" data-toggle="tooltip" title="Education Portal"> 
						<i class="fa fa-users"></i> <span class="name">Education Portal</span> <i class="fa fa-angle-down"></i>
				</a>
				</li>
			</ul>		
						
			
			 
			<!-- BEGIN TOP NAVIGATION MENU -->
			<ul class="nav navbar-nav pull-right">
				<!-- BEGIN NOTIFICATION DROPDOWN -->
				<li class="dropdown" id="header-notification">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						 <i class="fa fa-bell"></i>
					</a>
					<ul class="dropdown-menu notification">
						<li class="dropdown-title"><span><i class="fa fa-bell"></i> 
								0 Notifications</span>
						</li>
						<li>
							<a href="#">
								<span class="label label-success">
									<i class="fa fa-user"></i>
								</span>
								<span class="body"> <span class="message">No notifications </span> 
									<span class="time"> <i class="fa fa-clock-o"></i> <span>Just now</span> </span>
								</span>
							</a>
						</li>

						<li class="footer">
							<a href="#">See all notifications <i class="fa fa-arrow-circle-right"></i></a>
						</li>
					</ul>
				</li>
				
				<!-- END NOTIFICATION DROPDOWN -->

				<!-- BEGIN INBOX DROPDOWN -->
				<li class="dropdown" id="header-message"><a href="#"
					class="dropdown-toggle" data-toggle="dropdown"> <i
						class="fa fa-envelope"></i>
				</a>
					<ul class="dropdown-menu inbox">
						<li class="dropdown-title"><span><i class="fa fa-envelope-o"></i>
								0 Messages</span> <span class="compose pull-right tip-right"
							title="Compose message"><i class="fa fa-pencil-square-o"></i></span>
						</li>
						<li><a href="#"> <img
								src="<?php echo base_url(); ?>img/avatars/avatar2.jpg" alt="" />
								<span class="body"> <span class="from">System</span> <span
									class="message"> No New Messages </span> <span class="time"> <i
										class="fa fa-clock-o"></i> <span>Just Now</span>
								</span>
							</span>

						</a></li>

						<li class="footer"><a href="#">See all messages <i
								class="fa fa-arrow-circle-right"></i></a></li>
					</ul></li>
				<!-- END INBOX DROPDOWN -->
				<!-- BEGIN LANG DROPDOWN -->
				<li class="dropdown" id="header-notification"><a href="#"
					class="dropdown-toggle" data-toggle="dropdown"> <i
						class="fa fa-font"></i>
				</a>
					<ul class="dropdown-menu notification">
						<li class="dropdown-title"><span><i class="fa fa-font"></i> Selected: <?php echo $this -> session -> userdata['user']['language']; ?></span></li>
						<li><a href="<?php echo base_url(); ?>homeportal/english"> <span class="body"> <span class="message">English</span>
							</span>
						</a></li>
						<li><a href="<?php echo base_url(); ?>homeportal/chinese"> <span class="body"> <span class="message">Traditional Chinese</span>
							</span>
						</a></li>
						<li><a href="<?php echo base_url(); ?>homeportal/simplifiedchinese"> <span class="body"> <span class="message">Simplified Chinese</span>
							</span>
							<li><a href="<?php echo base_url(); ?>homeportal/korean"> <span class="body"> <span class="message">Korean</span>
							</span>
						</a></li>
						</a></li>
					</ul></li>
				<!-- END LANG DROPDOWN -->
				<!-- BEGIN USER LOGIN DROPDOWN -->
				<li class="dropdown user" id="header-user"><a href="#"
					class="dropdown-toggle" data-toggle="dropdown"> <span
						class="username">
						<?php

						$user = $this -> ion_auth -> user() -> row();
						if (!empty($user -> first_name)) {
							echo $user -> first_name . ' ' . $user -> last_name;
						}
						?>
						</span> <i class="fa fa-angle-down"></i>
				</a>
					<ul class="dropdown-menu">

						<li><a href="<?php echo base_url("homePortal/profile"); ?>"><i class="fa fa-user"></i> <?php echo $this -> lang -> line('MY_PROFILE'); ?></a></li>
						<li><a href="http://www.seamlesscare.ca/terms-of-use/"
							target="_blank"><i class="fa fa-cog"></i> <?php echo $this -> lang -> line('TERMS_OF_USE'); ?></a></li>
						<li><a href="http://www.seamlesscare.ca/privacy-policy/"
							target="_blank"><i class="fa fa-eye"></i> <?php echo $this -> lang -> line('PRIVACY_SETTINGS'); ?></a></li>	
						<li><a href="<?php echo base_url(); ?>auth/logout"><i
								class="fa fa-power-off"></i> <?php echo $this -> lang -> line('LOG_OUT'); ?></a></li>
					</ul></li>
				<!-- END USER LOGIN DROPDOWN -->
			</ul>
			<!-- END TOP NAVIGATION MENU -->
		</div>
		<!-- TEAM STATUS -->
		<div class="container team-status" id="team-status">
			<div id="scrollbar">
				<div class="handle"></div>
			</div>
			<div id="teamslider">
				<h1>
					Phone (905) 432 9900 <a href="http://seamlesscare.ca/inquiry.html">
						Click to send us feedback</a>
				</h1>
			</div>
		</div>
		<!-- /TEAM STATUS -->
	</header>
	<!--/HEADER -->

	<!-- PAGE -->
	<section id="page">
		<!-- SIDEBAR -->
		<div id="sidebar" class="sidebar sidebar-fixed">
			<div class="sidebar-menu nav-collapse">
				<div class="divide-20"></div>
				<!-- SEARCH BAR -->
				<div id="search-bar">
					<input class="search" id="patSearch" type="text"
						placeholder="Search"> <i onclick="searchPat()"
						class="fa fa-search search-icon"></i>
				</div>
				<!-- /SEARCH BAR -->

				<!-- SIDEBAR QUICK-LAUNCH -->
				<!-- <div id="quicklaunch">
						<!-- /SIDEBAR QUICK-LAUNCH -->

				<!-- SIDEBAR MENU -->
				<ul>

					<!--li
						<?php
						if ($this -> uri -> segment(2, 0) == '0') {echo 'class="active"';
						};
					?>>
						<a href="<?php echo base_url("homeportal/"); ?>"> <i
							class="fa fa-home fa-fw"></i> <span class="menu-text"><?php echo $this -> lang -> line('MENU_HOMES'); ?></span>
					</a>
					</li-->

					<li
						<?php
						if ($this -> uri -> segment(2, 0) == 'patients' && $this -> uri -> segment(2, 0) != '0') {echo 'class="active"';
						};
					?>>
						<a href="<?php echo base_url("homeportal"); ?>"> <i
							class="fa fa-users fa-fw"></i> <span class="menu-text"><?php echo $this -> lang -> line('MENU_CLIENTS'); ?></span>
					</a>
					</li>

					<li
						<?php
						if ($this -> uri -> segment(2, 0) == 'tickets' && $this -> uri -> segment(2, 0) != '0') {echo 'class="active"';
						};
					?>>
						<a href="<?php echo base_url("homeportal/tickets"); ?>"> <i
							class="fa fa-ticket fa-fw"></i> <span class="menu-text"><?php echo $this -> lang -> line('MENU_TICKETS'); ?></span>
					</a>
					</li>

					<li
						<?php
						if ($this -> uri -> segment(2, 0) == 'userEvents' && $this -> uri -> segment(2, 0) != '0') {echo 'class="active"';
						};
					?>>
						<a href="<?php echo base_url("homeportal/userEvents"); ?>"> <i
							class="fa fa-bar-chart-o fa-fw"></i> <span class="menu-text"><?php echo $this -> lang -> line('MENU_EVENTS'); ?></span>
					</a>
					</li>
					
					<?php  
						$NHID = $this->ion_auth->user()->row()->NHID;
						if($NHID > 0) {	 ?>
					
					<li
						<?php
						if ($this -> uri -> segment(2, 0) == 'ListOfPatForAgents' && $this -> uri -> segment(2, 0) != '0') {echo 'class="active"';
						};
					?>>
						<a href="<?php echo base_url("homeportal/ListOfPatForAgents/"); ?>"> <i
							class="fa fa-users fa-fw"></i> <span class="menu-text">User Setting</span>
					</a>
					</li>					
					
					<?php }  ?>
					
					
					<li
						<?php
						if ($this -> uri -> segment(2, 0) == 'schedules' && $this -> uri -> segment(2, 0) != '0') {echo 'class="active"';
						};
					?>>
						<a  href="<?php echo base_url("homeportal/schedules"); ?>"> <i
							class="fa fa-calendar fa-fw"></i> <span class="menu-text">Calendar <span class="pull-right"><span class="label label-warning">Coming Soon</span></span></span>
					</a>
					</li>
					
					

					<!-- 
					<li
						<?php //if ($this->uri->segment(2,0)=='userEvents' && $this->uri->segment(2,0)!='0'){echo 'class="active"';};?>>
						<a href="<?php //echo base_url("homeportal/userEvents");?>"> <i
							class="fa fa-calendar fa-fw"></i> <span class="menu-text">Appointments</span>
					</a>
					</li>
					 -->
					<li><a
						onclick="loadModal('<?php echo base_url() . 'homeportal/DrugInfoModal/'?>')">
							<i class="fa fa-medkit"></i> <span class="menu-text"><?php echo $this -> lang -> line('MENU_COVERAGE_INFO'); ?></span>
					</a></li>
					<li>
						<a
						onclick="loadModal('<?php echo base_url() . 'homeportal/pharmacyInfo/'?>')">
							<i class="fa fa-info-circle"></i> <span class="menu-text"><?php echo $this -> lang -> line('MENU_PHARMACY_INFO'); ?></span>
					</a></li>
					<!-- <li><a
						onclick="loadModal('<?php echo base_url() . 'homeportal/feedback/'?>')">
							<i class="fa fa-info-circle"></i> <span class="menu-text">Feedback</span>
					</a></li> -->
				</ul>
				<!-- /SIDEBAR MENU -->
			</div>
		</div>
		<!-- /SIDEBAR -->
		<div id="main-content">
			<div class="container">
				<div class="row">
					<div id="content" class="col-lg-12">
						<!-- PAGE HEADER-->
						<div class="row">
							<div class="col-sm-12">
								<div class="page-header">
