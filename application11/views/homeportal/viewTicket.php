<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left">Tickets</h3>
</div>
<div class="description">
	<a class="btn btn-default btn-lg"
		href="<?php echo base_url();?>#"> <i
		class="fa fa-archive"></i> Close Ticket </a> <h2>
				<i class="fa fa-circle text-green"></i> <span
					class="label label-info">Pharmacist Question </span> - <span
					class="label label-info">Ticket # 1231 </span> - <span
					class="label label-info">March 24, 2014</span> <br> <br>

			</h2>
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DELIVERY RUNS CONTENT -->
<!-- DATA TABLES -->
<div class="row">
	<div class="col-md-12">

		<!-- CHAT -->
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<!-- BOX -->
								<div class="box border green chat-window">
									<div class="box-title">
										<h4><i class="fa fa-comments"></i>Chat Window</h4>
										<div class="tools">
											<a href="#box-config" data-toggle="modal" class="config">
												<i class="fa fa-cog"></i>
											</a>
											<a href="javascript:;" class="reload">
												<i class="fa fa-refresh"></i>
											</a>
											<a href="javascript:;" class="collapse">
												<i class="fa fa-chevron-up"></i>
											</a>
											<a href="javascript:;" class="remove">
												<i class="fa fa-times"></i>
											</a>
										</div>
									</div>
									<div class="box-body big">
										<div class="scroller" data-height="450px" data-always-visible="1" data-rail-visible="1">
											<ul class="media-list chat-list">
												
												<li class="media">
												  <a class="pull-right" href="#">
													<img class="media-object"  alt="Generic placeholder image" src="<?php echo base_url()?>img/chat/headshot2.jpg">
												  </a>
												  <div class="pull-right media-body chat-pop mod">
													<h4 class="media-heading">You <span class="pull-left"><abbr class="timeago" title="Oct 11, 2013" >Oct 11, 2013</abbr> <i class="fa fa-clock-o"></i></span></h4></h4>
													Hey can you please order lorazapam 1mg for this client! Thanks!
												  </div>
												</li>
												<li class="media">
												  <a class="pull-left" href="#">
													<img class="media-object"  alt="Generic placeholder image" src="<?php echo base_url()?>img/chat/headshot4.jpg">
												  </a>
												  <div class="media-body chat-pop">
													<h4 class="media-heading">Jess Doe <span class="pull-right"><i class="fa fa-clock-o"></i> <abbr class="timeago" title="Oct 12, 2013" >Oct 12, 2013</abbr> </span></h4></h4>
													<p>Ok done!</p>
												  </div>
												</li>
												<li class="media">
												  <a class="pull-right" href="#">
													<img class="media-object"  alt="Generic placeholder image" src="<?php echo base_url()?>img/chat/headshot2.jpg">
												  </a>
												  <div class="pull-right media-body chat-pop mod">
													<h4 class="media-heading">You <span class="pull-left"><abbr class="timeago" title="Oct 12, 2013" >Oct 12, 2013</abbr> <i class="fa fa-clock-o"></i></span></h4></h4>
													Thanks!!
												  </div>
												</li>
											</ul>
										</div>
										<div class="divide-20"></div>
										<div class="chat-form">
											<div class="input-group"> 
												<input type="text" class="form-control"> 
												<span class="input-group-btn"> <button class="btn btn-primary" type="button"><i class="fa fa-check"></i></button> </span> 
											</div>
										</div>
									</div>
								</div>
								<!-- /BOX -->
							</div>
						</div>
						<!-- /CHAT -->
	


	</div>
</div>
<!-- /PAGE -->


<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>
</div>
<!-- /CONTENT-->
</div>
</div>
</div>
</section>
<!--/PAGE -->
<!-- JAVASCRIPTS -->
<!-- Placed at the end of the document so the pages load faster -->
<!-- JQUERY -->
<script src="<?php echo base_url("js/jquery/jquery-2.0.3.min.js");?>"></script>
<!-- JQUERY UI-->
<script
	src=<?php echo base_url("js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js");?>></script>
<!-- BOOTSTRAP -->
<script
	src=<?php echo base_url("js/bootstrap-dist/js/bootstrap.min.js");?>></script>


<!-- DATE RANGE PICKER -->
<script
	src=<?php echo base_url("js/bootstrap-daterangepicker/moment.min.js");?>></script>

<script
	src=<?php echo base_url("js/bootstrap-daterangepicker/daterangepicker.min.js");?>></script>
<!-- SLIMSCROLL -->
<script type="text/javascript"
	src=<?php echo base_url("js/jQuery-slimScroll-1.3.0/jquery.slimscroll.min.js");?>></script>
<!-- SLIMSCROLL -->
<script type="text/javascript"
	src=<?php echo base_url("js/jQuery-slimScroll-1.3.0/jquery.slimscroll.min.js");?>></script>
<script type="text/javascript"
	src="<?php echo base_url();?>js/jQuery-slimScroll-1.3.0/slimScrollHorizontal.min.js"></script>


<!-- BLOCK UI -->
<script type="text/javascript"
	src=<?php echo base_url("js/jQuery-BlockUI/jquery.blockUI.min.js");?>></script>
<!-- SPARKLINES -->
<script type="text/javascript"
	src=<?php echo base_url("js/sparklines/jquery.sparkline.min.js");?>></script>
<!-- EASY PIE CHART -->
<script
	src=<?php echo base_url("js/jquery-easing/jquery.easing.min.js");?>></script>
<script type="text/javascript"
	src=<?php echo base_url("js/easypiechart/jquery.easypiechart.min.js");?>></script>
<!-- FLOT CHARTS -->
<script src=<?php echo base_url("js/flot/jquery.flot.min.js");?>></script>
<script src=<?php echo base_url("js/flot/jquery.flot.time.min.js");?>></script>
<script
	src=<?php echo base_url("js/flot/jquery.flot.selection.min.js");?>></script>
<script src=<?php echo base_url("js/flot/jquery.flot.resize.min.js");?>></script>
<script src=<?php echo base_url("js/flot/jquery.flot.pie.min.js");?>></script>
<script src=<?php echo base_url("js/flot/jquery.flot.stack.min.js");?>></script>
<script
	src=<?php echo base_url("js/flot/jquery.flot.crosshair.min.js");?>></script>
<!-- TODO -->
<script type="text/javascript"
	src=<?php echo base_url("js/jquery-todo/js/paddystodolist.js");?>></script>
<!-- TIMEAGO -->
<script type="text/javascript"
	src=<?php echo base_url("js/timeago/jquery.timeago.min.js");?>></script>
<!-- FULL CALENDAR -->
<script type="text/javascript"
	src=<?php echo base_url("js/fullcalendar/fullcalendar.min.js");?>></script>
<!-- COOKIE -->
<script type="text/javascript"
	src=<?php echo base_url("js/jQuery-Cookie/jquery.cookie.min.js");?>></script>
<!-- GRITTER -->
<script type="text/javascript"
	src=<?php echo base_url("js/gritter/js/jquery.gritter.min.js");?>></script>
<!-- CUSTOM SCRIPT -->
<script src=<?php echo base_url("js/script.js");?>></script>
<script>
		jQuery(document).ready(function() {	
			App.setPage("viewTicket");  //Set current page
			App.init(); //Initialise plugins and elements
		});
	</script>
<!-- /JAVASCRIPTS -->

