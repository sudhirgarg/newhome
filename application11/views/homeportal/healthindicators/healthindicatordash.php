<?php
$patID = $this->uri->segment ( 3 );
$patient = $this->comp->getCompliancePatient ( $patID );
?>

<div class="clearfix">
	<h3 class="content-title pull-left"><?php echo $patient -> patInitials . $patID; ?>
	Client Health Indicators</h3>
</div>
<div class="description">
	<a class="btn btn-default btn-lg"
	href="<?php echo base_url() . "homeportal/viewpat/" . $patID . "/profile"; ?>"> <i class="fa fa-chevron-left"></i> Back to <?php echo $patient -> patInitials; ?>
	Profile </a>
	| Discovering trends, increasing awareness
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DASHBOARD CONTENT -->

<!--  ACTIVITY -->
<div class="row">
	<div class="col-lg-12">
		
		<!-- CLOZARIL chart -->
			<div class="row">
				<div class="col-lg-10">
					<!-- BOX SOLID-->
					<div class="box border">
						<div class="box-title">
							<h4><i class="fa fa-bars"></i>Clozaril </h4>
						</div>
						<div class="box-body">
							<div id="mychart" style="height: 300px; width: 600px;"></div>
						</div>
					</div>
					<!-- /BOX SOLID -->
				</div>

				<div class="col-lg-2">
					<!-- BOX SOLID-->
					<div class="box border">
						<div class="box-title">
							<h4><i class="fa fa-bars"></i>Clozaril </h4>
						</div>
						<div class="box-body">
							<?php
$lights = $this->pharm->getANCValue ( $patient->patID );
switch ($lights) {
case ($lights >= 2) :
?>
							<img src="<?php echo base_url(); ?>img/greenlight.jpg"
							alt="" height="300px" /><?php

;
break;
case ($lights >= 1.5 && $lights < 2) :
?>
							<img src="<?php echo base_url(); ?>img/yellowlight.jpg"
							alt="" /><?php

;
break;
case ($lights < 1.5) :
?>
							<img src="<?php echo base_url(); ?>img/redlight.jpg"
							alt="" /><?php

;
break;
default :
?>
							<img src="<?php echo base_url(); ?>img/redlight.jpg"
							alt="" /><?php

;
break;
}
							?>
						</div>
					</div>
					<!-- /BOX SOLID -->
				</div>
			</div>
			<!-- /charts -->
		</div>

<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top </span>
</div>

