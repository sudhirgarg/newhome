

<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left">Questions and Answers</h3>
</div>
<div class="description">
	<?php 				 
			echo '<button onclick="goBack();" class="btn btn-lg btn-default"  href=""><img src="'.base_url().'img/Arrow-Back-icon.png"  width="24px" >Go Back</button>';			
	 ?>
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DELIVERY RUNS CONTENT -->
<!-- DATA TABLES -->

<!--***************************************** PAGE BODY STARTS ***************************************************************-->

<div class="row" ng-app="viewResult" ng-controller="viewResultController">
	<div class="col-md-6">
		<div class="panel panel-info">			
	    <div class="panel-heading">
	        <h3 class="panel-title"><?php echo $user -> first_name.' '.$user -> last_name; ?></h3>
	    </div>
	    
	        <div class="panel-body">	   	   
		    <div class="panel panel-info my">
			  <div class="panel-body">
			  
			      
			       
			       <table class="table table-hover">
	  		 	      <thead>
					      <tr>
						        <th>#</th>
						        <th>Title</th>
						        <th>Score%</th>
						        <th>Attempt</th>
						        <!-- th> <button class="btn btn-info" > <i class="fa fa-print" aria-hidden="true"></i> </button> </th>
						        <th> <button class="btn btn-info" > <i class="fa fa-folder-open-o" aria-hidden="true"></i> </button> </th>
						        <th> <button class="btn btn-info" > <i class="fa fa-certificate" aria-hidden="true"></i> </button> </th -->
					      </tr>
					  </thead>
					  <tbody>
						  
						     <?php 
						        $i = 1;
						        foreach($examInfo AS $record){ 
								    echo '<tr><td>'.$i++.'</td> 
								    <td>'.$record -> gQname.'</td>
								    <td>'.round($record -> score).'</td>
								    <td>'.$record -> attempt.'</td>
								    <!--td> <button class="btn btn-success" ng-click="printExamReport('.$record -> id.')">    <i class="fa fa-print" aria-hidden="true"></i>         </button> </td>
								    <td> <button class="btn btn-success" ng-click="viewExamReport('.$record -> id.')">     <i class="fa fa-folder-open-o" aria-hidden="true"></i> </button> </td>
								    <td> <button class="btn btn-success" ng-click="completeExamReport('.$record -> id.')"> <i class="fa fa-certificate" aria-hidden="true"></i>   </button> </td--></tr>';							    
							    } 
						      ?> 
						  
					  </tbody>	  
				 </table>
			       
			  </div>
			</div>       
	    		
		</div>
		</div>
	</div>
</div>



<!--***************************************** PAGE BODY ENDS ***************************************************************-->


<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>