</div>
</div>
</div>
</div>
</section>
<!--/PAGE -->
<!-- JAVASCRIPTS -->
<!-- Placed at the end of the document so the pages load faster -->
<!-- JQUERY -->
	<!--- script for calender --->
	<script src="<?php echo base_url(); ?>css/fullcalender/moment.min.js"></script>
	<script src="<?php echo base_url(); ?>css/fullcalender/fullcalendar.min.js"></script>
	<!--- script for calender end--->
	<script src="<?php echo base_url(); ?>js/jquery/jquery-2.0.3.min.js"></script>
	<script src="<?php echo base_url(); ?>js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
	<script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/bootstrap-switch/bootstrap-switch.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jQuery-slimScroll-1.3.0/slimScrollHorizontal.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jQuery-Cookie/jquery.cookie.min.js"></script>
	<script src="<?php echo base_url(); ?>js/script.js"></script>	
	<script src="<?php echo base_url(); ?>js/raterater.jquery.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular.min.js"></script>
	<script src="<?php echo base_url(); ?>js/angularHomeportal.js"></script>
	<script src="<?php echo base_url().'js/eduAngular.js'?>" ></script>
	
	<script type="text/javascript" src="<?php echo base_url(); ?>js/dirPagination.js"></script>
	<!-- DATE PICKER -->
	<script type="text/javascript" src="<?php echo base_url()?>js/datepicker/picker.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>js/datepicker/picker.date.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>js/datepicker/picker.time.js"></script>
	
	<!-- DATE TIME PICKER -->
	<script type="text/javascript" src="<?php echo base_url()?>js/datepicker/bootstrap-datetimepicker.min.js"></script>
	<script src="<?php echo base_url(); ?>js/datatables/media/js/jquery.dataTables.min.js"></script>

	
	<script>
		jQuery(document).ready(function() {	
			
			App.setPage("widgets_box");
			//Set current page
			App.init();
			//Initialise plugins and elements			
			
		});
		
		$(".timepicker-fullscreen").pickatime({
			interval: 60
		});

		$(".datepicker-fullscreen").pickadate();
		
		function loadModal(ajaxURL){
			$.get(ajaxURL, function(data) {
				$("#myModalContent").html(data);
				$('#myModal').modal();
			});
		}
		
		function globalSuccess(successMessage){
			$("#successMessageContent").html(successMessage);
			$("#successMessage").fadeIn().delay(3500).fadeOut();
		}
	
		function globalError(errorMessage){
			$("#errorMessageContent").html(errorMessage);
			$("#errorMessage").fadeIn().delay(3500).fadeOut();
		}
			
	
		function read(data){
			var args = data.split('|');
			if (args[0] == 0){
				globalError(args[1]);
			} else {
				globalSuccess(args[1]);
				setTimeout('autoRefresh1()', 500);
			}
		}


		$(".viewDisposal").click(function (e){
			 var disId = $(this).closest('tr').find('.disposalId').val();
			 var myURL = "<?php echo base_url(); ?>homeportal/viewDisposal/" + disId;          
	           window.open(myURL, "", "width=950, height=1000");
			
			 
		});  

		$(".printDisposal").click(function (e){
			 var disId = $(this).closest('tr').find('.disposalId').val();
			 var myURL = "<?php echo base_url(); ?>homeportal/printDisposal/" + disId;          
	           window.open(myURL, "", "width=950, height=1000");
		}); 
				

		/*
		$.post('<?php echo site_url('homeportal/getPatPRN'); ?>',{
			patID: $("#patID").val()
			
		},function(data, status){
			$("#dtorders").html(data);
			$("#patPRNLoading").hide();
		});
	    */
	     
				
		$(".agencyResetCGpwd").submit(function (e){
			
			var inputInfo = $(this).serialize();
			var buttonContant = $(this).find(':submit');			
			buttonContant.html('Please wait...');
			
			$.ajax({
				
				url : "<?php echo base_url()?>homeportal/agencyResetCGpwd",
				type : "POST",
				data : inputInfo
				
			}).done(function(data){
				
				$("#alertCGpedRest").html(data);
				
			});
			
			e.preventDefault();
		});	
		
		function agencyResetCGpwdOnClick(){
			$.post('<?php echo site_url('homeportal/agencyResetCGpwd'); ?>',{
				userId :$('#userId').val(),
				newPassword : $('#passwordSet').val(),
				newPasswordAgain : $('#passwordConfirm').val()
				
			}).done(function(data){
				alert(data);
				setTimeout('autoRefresh1()', 500);
			});
			
		}	
	
		
		/* This is out callback function for when a rating is submitted
		 */
		function rateAlert(id, rating) {
		   // alert( 'homeportal/saveRanksToTicket/'+id+'/'+rating);
		   $.post('<?php echo site_url('homeportal/saveRanksToTicket/'); ?>',
			{
				id: id,
				rant: rating
			});	
			 
		}
		
		/* Here we initialize raterater on our rating boxes
		 */
		$(function() {
		    $( '.ratebox' ).raterater( { 
		        submitFunction: 'rateAlert', 
		        allowChange: true,
		        starWidth: 20,
		        spaceWidth: 3,
		        numStars: 5
		    } );
		});
		
		function addHomePortalUser() {
	       	       	        
	         $.post('<?php echo site_url('homeportal/addCG'); ?>',{
					username: $("#username").val(), 
	            	password: $("#password").val(),  
	            	email: $("#email").val(),
	            	company: $("#company").val(),
	            	phone: $("#phone").val(),
	            	first_name: $("#first_name").val(),
	            	last_name: $("#last_name").val()
				
			}).done(function(data){
				alert(data);
				if(data == "Successfully Created"){ setTimeout('autoRefresh1()', 500); }
				
			});
	    }
    
		function autoRefresh1(){
             window.location.reload();
        }
        
	
	function saveTicket(){
		
		 $("#saveTicketButton").button('loading');
		    $.post('<?php echo site_url('homeportal/saveTicket');?>',{
		    	
		        ticketID: $("#ticketID").val(),
		        createdBy: $("#createdBy").val(),
		        dateCreated: $("#dateCreated").val(),
		        dateComplete: $("#dateComplete").val(),
		        patID: $("#patID").val(),
		        ticketTypeReferenceID: $("#ticketTypeReferenceID").val(),
		        Description: $("#Description").val(),
		        active: $("#active").val()
		},function (data) {
		        $("#closeTicketButtonModal").click();
		        read(data);		       	        	
		        setTimeout(function() { location.reload(); }, 1000);          
		 });	
	}


	function addComment(){
		
		 $("#addCommentButton").button('loading');
		    $.post('<?php echo site_url('homeportal/addComment');?>',{
		    	
		        ticketID: $("#ticketID").val(),
		        message: $("#message").val()
		        
		    },function (data) {
		        var uriString = $("#ticketModalURI").val();
	            $.get('<?php echo base_url();?>'+uriString, function(data) {
					$("#myModalContent").html(data);
	                });
	            read(data);
		  });	
	}




	function closeTicket(){
		
		if(confirm("Are you sure you want to close this ticket?")) {
			$("#closeTicketButton").button('loading');
		    $.post('<?php echo site_url('homeportal/saveTicket');?>',{
		    	
		        ticketID: $("#ticketID").val(),
		        createdBy: $("#createdBy").val(),
		        dateCreated: $("#dateCreated").val(),
		        dateComplete: $("#dateComplete").val(),
		        patID: $("#patID").val(),
		        ticketTypeReferenceID: $("#ticketTypeReferenceID").val(),
		        Description: $("#Description").val(),
		        active: 0
		    },function (data) {
		        $("#closeTicketButtonModal").click();
		        read(data);
		        setTimeout(function() { location.reload(); }, 1000);
		    });	
		}
	}

    function createDisposalSheet(){

    	var data;
        updateSelectedInfo();

        var printBy = $('#printBy').val();

        if(printBy){

            var info = $('#info').val();

            if(info){
               
                $.post('<?php echo site_url('homeportal/createDisposal');?>',{
                     printBy : printBy,
                     info    : info,
                     patId   : $('#patId').val()
                },function(data){
                      read(data);
                });
                
            }else{
            	data = '0|Select med!';
                read(data);
            }

        }else{
              data = '0|Enter your name!';
              read(data);  
              $('#printBy').focus();
        }          

    }

    function updateSelectedInfo(){
        var allInfo = [];
        $('#dispoalCheckBox :checked').each(function(){
               allInfo.push($(this).val() + '^' + $(this).closest('tr').find('.disposalNumPills').val() + '^' + $(this).closest('tr').find('.disposalComment').val() );              
        });
        $('#info').val(allInfo.join(';'));
        
    }

	function reOrderPRNs(){
		updateRx();
		updateTextArea();
		if(confirm("Please confirm to reorder the following prescription numbers: " + $("#reOrderBox").val())) {
	    $.post('<?php echo site_url('homeportal/createReOrder');?>',
	             {
	        	patID: $("#patID").val(),
	      		rxs: $("#reOrderBox").val(),
	      		rxnumList : $("#reOrderRx").val()
	        }, 
	    function (data) {
	        read(data);
	        setTimeout(function() { location.reload(); }, 1000);
	    });
	    	
		}else{
			//do nothing
		}
	}

	function updateTextArea() {         
	    var allVals = [];
	    $('#reOrderList :checked').each(function() {
	      allVals.push("| Rx# " + $(this).val() + " comment: " + $(this).closest('tr').find('.reOrderCommentClass').val() );
	      
	    });
	    $('#reOrderBox').val(allVals)
	}

	function updateRx(){
		var favorite = [];
		$.each($("input[name='reOrderCheckBox']:checked"), function(){            
            favorite.push($(this).val());
        });
	    $('#reOrderRx').val(favorite.join(","));
	    
	 }
	
	function getPatIdForAssignCG(){
		
		var favorite = [];
        $.each($("input[name='patId']:checked"), function(){            
            favorite.push($(this).val());
        });
        var listPatId = favorite.join(",");
        $('#patList').val(listPatId); 
		
	}
	
	
	function assignPatToCG(){
		getPatIdForAssignCG();
		
		if($('#patList').val() == '') {                
              
                $("#aMsg").text("Please Select Patient");  
               
               
        }else{ 
            $.post('<?php echo site_url("homeportal/AssignPatForCG") ?>',{
                id : $("#patList").val(),
                CG : $("#CG").val()
            },function(data){
            	                
                $("#aMsg").text(data);  
                setTimeout('autoRefresh1()', 1000);               
                
            }); 
        }
	}
	
	
	function saveEmailCredential(){
		var med;
		var ticketToken;
		var commentToken;
		
		if($("#medChange").prop('checked') == true)  { med          = $("#medChange").val(); }
		if($("#ticket").prop('checked')    == true)  { ticketToken  = $("#ticket").val();    }
		if($("#comment").prop('checked')   == true)  { commentToken = $("#comment").val();   }
		
				
        $.post('<?php echo site_url("homePortalEdu/setEmailCredential") ?>',{
            userId     : $("#userIDEmail").val(),
            medChange  : med,
            ticket     : ticketToken,
            comment    : commentToken           
            
        },function(data){
        	              
            $("#credentialAlert").text(data);  
            setTimeout('autoRefresh1()', 1000);               
            
        });
            
    }


	function autoRefresh1(){
         window.location.reload();
    }
	    
	function goBack() {
	    window.history.back();
	}    

</script>


	<?php $this -> load -> view('extras'); ?>
	<?php $this -> load -> view('js/footer'); ?>
	<?php 		 
		 $page = $this->uri->segment ( 2 );
		 $page1 = $this->uri->segment ( 1 );
		
		 if(($page =='viewpat') && $page1=='homeportal' ){		
			
			$this->load->view('js/viewPatient'); 	 
		}	
	 ?>
	

</body>
</html>
