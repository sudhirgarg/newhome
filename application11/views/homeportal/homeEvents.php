<?php
$patID = $this->uri->segment ( 3 );
$patient = $this->comp->getCompliancePatient ( $patID );
$user = $this->ion_auth->user ()->row ();
?>

<!-- STYLER -->

<style>
.modal-dialog {
	width: 60%;
}
</style>
<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left"><?php echo $patient->patInitials . $patID;?> Client Events</h3>

</div>
<div class="description">
<a class="btn btn-default btn-lg"
		href="<?php echo base_url() ."homeportal/viewPat/" . $patID . "/profile"; ?>">
		<i class="fa fa-chevron-left"></i> Back to <?php echo $patient->patInitials;?> Profile
	</a>
	<button class="btn btn-primary btn-lg"
		onclick="loadModal('<?php echo base_url() ."homeportal/eventModal/" . $patID; ?>')">
		<i class="fa fa-plus"></i> Event
	</button>
	| Discovering trends, increasing awareness
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DASHBOARD CONTENT -->



<!--  ACTIVITY -->
<div class="row">
	<div class="col-lg-12">
		<div class="box border blue">
			<div class="box-title">
				<h4>
					<i class="fa fa-truck"></i>Events for year <?php echo date("Y"); ?>
				</h4>
				<div class="tools">
					<a href="#box-config" data-toggle="modal" class="config"> <i
						class="fa fa-cog"></i>
					</a> <a href="javascript:;" class="reload"> <i
						class="fa fa-refresh"></i>
					</a> <a href="javascript:;" class="collapse"> <i
						class="fa fa-chevron-up"></i>
					</a> <a href="javascript:;" class="remove"> <i class="fa fa-times"></i>
					</a>
				</div>
			</div>
			<div class="box-body">
				<div class = "row"> &nbsp;&nbsp;
				<button style="background-color: #A8BC7B;" class="btn btn-lg">Bowel Movement</button>
				<button style="background-color: #70AFC4;" class="btn btn-lg">Sleep Event</button>
				<button style="background-color: #5487B0;" class="btn btn-lg">PRN Administration</button>
				<button style="background-color: #D9534F;" class="btn btn-lg">ER Visit</button>
				<button style="background-color: #FCD76A;" class="btn btn-lg">Seizure</button>
				
				</div>
				<canvas id="myChart" style="height: 200px;"></canvas>
			</div>
		</div>
	</div>
	<div class="col-lg-12">
		<!-- BOX -->
		<div class="box border blue">
			<div class="box-title">
				<h4>
					<i class="fa fa-reorder"></i> <span class="hidden-inline-mobile">
						Activity</span>
				</h4>
			</div>
			<div class="box-body">
				<div data-always-visible="1" data-rail-visible="1">
					<?php
					$patEvents = $this->portal->getPatEvents ( $patID );
					if ($patEvents != false) {
						foreach ( $patEvents->result () as $event ) {
							?>
					<div class="feed-activity clearfix">
						<div>
						<?php
							
switch ($event->eventTypeID) {
								
								case 1 : 
									?><i class="pull-left roundicon fa fa-male btn btn-success"></i> Bowel Movement: 									
																 
																<?php
									break;
								case 2 :
									?>
									<i class="pull-left roundicon fa fa-moon-o btn btn-info"></i> Sleep Event: 
																
																<?php
									break;
								case 3 :
									?>
																	<i
								class="pull-left roundicon fa fa-edit btn btn-primary"></i> PRN Administration: 
																<?php
									break;
								case 4 :
									?>
									<i
								class="pull-left roundicon fa fa-ambulance btn btn-danger"></i> Emgergency Room Visit: 
															 
																<?php
									break;
								case 5 :
									?>
																<i
								class="pull-left roundicon fa fa-flash btn btn-yellow"></i>  Seizure: 
																<?php
									break;
								default :
									?>
																								<i
								class="pull-left roundicon fa fa-edit btn btn-primary"></i> 
																								<?php
							}
							?>
							<?php echo $event->desc;?> <br>
						</div>
						<div class="time">
							<i class="fa fa-clock-o"></i> <?php echo timespan(strtotime($event->eventDate), time()) . ' ago';?>
						</div>
						
					</div>
					
					<?php }}else
					{echo 'no events recorded';}?>

				</div>

			</div>
		</div>
	</div>
</div>
<!-- /BOX -->


<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>
