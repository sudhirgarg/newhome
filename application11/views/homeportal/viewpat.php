<?php
$this -> load -> library('ion_auth');
$this -> load -> helper('url');
$patient = $this -> pharm -> getCompliancePatientWithCheck($this -> uri -> segment(3));
?>

<script>
	$(".timepicker-fullscreen").pickatime();
	$(".datepicker-fullscreen").pickadate({
		showOtherMonths : true,
		autoSize : true,
		appendText : '<span class="help-block">(yyyy-mm-dd)</span>',
		dateFormat : "yy-mm-dd"
	}); 
</script>

<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left">Client Profile</h3>
</div>
<div class="description">
	<a class="btn btn-lg btn-default"
		href="<?php echo base_url()?>compliance/compliancepatients">Clients</a>

	<button class="btn btn-lg btn-warning"
		onclick="loadModal('<?php echo base_url() . 'compliance/editMedChangeModal/' . $patient -> patID; ?>/-1')">
		<i class="fa fa-plus"></i> New Med Change
	</button>

	<button class="btn btn-lg btn-info"
		onclick="loadModal('<?php echo base_url() . 'compliance/editTicketModal/' . $patient -> patID; ?>/-1')">
		<i class="fa fa-plus"></i> New Ticket
	</button>
	
	
	<input id="patID" value="<?php echo $patient -> patID; ?>" type="hidden">
	
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!--  MAIN PAGE HERE -->

<div class="row">
	<div class="col-lg-12">

		<!-- USER PROFILE -->
		<div class="row">
			<div class="col-md-12">
				<!-- BOX -->
				<div class="box border">
					<div class="box-title">
						<h4>
							<i class="fa fa-user"></i><span class="hidden-inline-mobile"><?php  echo '<span class="label label-primary">' . $patient -> patInitials . '-' . $patient -> patID . '</span>'; ?></span>
						</h4>
					</div>
					<div class="box-body">
						<div class="tabbable header-tabs user-profile">
							<ul class="nav nav-tabs">
								<li
									<?php
									if ($this -> uri -> segment(4) == "billing") {echo 'class="active"';
									}
								?>><a
									href="#billing" data-toggle="tab"><i class="fa fa-edit"></i> <span
										class="hidden-inline-mobile"> Billing</span></a></li>
								<li
									<?php
									if ($this -> uri -> segment(4) == "medchanges") {echo 'class="active"';
									}
								?>><a
									href="#medchanges" data-toggle="tab"><i class="fa fa-edit"></i>
										<span class="hidden-inline-mobile"> Med Changes</span></a></li>
								<li
									<?php
									if ($this -> uri -> segment(4) == "tickets") {echo 'class="active"';
									}
								?>><a
									href="#tickets" data-toggle="tab"><i class="fa fa-edit"></i> <span
										class="hidden-inline-mobile"> Tickets</span></a></li>

                                <li
                                    <?php
									if ($this -> uri -> segment(4) == "reorders") {echo 'class="active"';
									}
								?>><a
                                        href="#reorders" data-toggle="tab"><i class="fa fa-bar-chart-o"></i>
                                        <span class="hidden-inline-mobile"> ReOrders</span></a></li>

                                <li
									<?php
									if ($this -> uri -> segment(4) == "profile" || $this -> uri -> segment(3) == "") {echo 'class="active"';
									}
								?>><a
									href="#pro_overview" data-toggle="tab"><i
										class="fa fa-dot-circle-o"></i> <span
										class="hidden-inline-mobile">Profile</span></a></li>
							</ul>
							<div class="tab-content">
								<!-- OVERVIEW -->
								<div
									class="tab-pane fade <?php
									if ($this -> uri -> segment(4) == "profile") {echo 'in active';
									}
								?>"
									id="pro_overview">
									<div class="row">
										<!-- PROFILE PIC -->
										<div class="col-md-3">
											<div class="list-group">
												<li class="list-group-item zero-padding">
													<!--  <img alt=""
													class="img-responsive"
													src="<?php //  echo base_url();?>img/profile/avatar.jpg">-->
												</li>
												<div class="list-group-item profile-details">
													<h2><?php  echo '<span class="label label-primary">' . $patient -> patInitials . '-' . $patient -> patID . '</span>'; ?></h2>
													<p>
														<i class="fa fa-circle text-green"></i> Active
													</p>
												</div>
												
												<div class="list-group-item profile-details">
													<h2>ALLERGIES</h2>
													<div id="alergiesTable">
														<div id="patAlergyLoading"> <center>Loading<br>
															<img src="<?php echo base_url(); ?>img/Preloader2.gif"
															alt="" class="img-responsive" height="100"
															width="100"></center></div>
																							</div>
												</div>
												<div class="list-group-item profile-details">
														<h2>CONDITIONS</h2>											
													<div id="conditionsTable">
														<div id="patConditionsLoading"> <center>Loading<br>
															<img src="<?php echo base_url(); ?>img/Preloader2.gif"
															alt="" class="img-responsive" height="100"
															width="100"></center></div>
																							</div>

												</div>
												<div class="list-group-item profile-details">
													<h2>Note</h2>
													<p><?php echo $patient -> note; ?></p>
												</div>
												<div class="list-group-item profile-details">
													<h2>Home</h2>
												<?php
												$patientHomes = $this->comp->getPatHomes ( $patient->patID, '0' );
												if ($patientHomes == FALSE) {
													?> <button class="btn" type="button"
														onclick="loadModal('<?php echo base_url("compliance/assignPatToHomeModal") . '/' . $patient -> patID . '/0'; ?>')">Add
														To Home</button><?php
														} else {
														$home = $this->comp->getHome ( $patientHomes );

														if ($home != FALSE) {
														?>	
													<button class="btn pull-right" type="button"
														onclick="loadModal('<?php echo base_url("compliance/assignPatToHomeModal") . '/' . $patient -> patID . '/' . $home -> homeID; ?>')">Modify</button>

													<h3>
														<i class="fa fa-home"></i> <?php echo $home -> homeName; ?>													</h3>
													<p><?php echo $home -> homeAddress; ?>	</p>
													<p>
														<i class="fa fa-phone"></i><?php echo $home -> phone; ?>	
													</p>
													<?php
													} else {
														?>
														<button class="btn pull-right" type="button"
														onclick="loadModal('<?php echo base_url("compliance/assignPatToHomeModal") . '/' . $patient -> patID . '/0'; ?>')">Modify</button>
														
														<?php
														}
														}
												?>
												</div>
												<div class="list-group-item profile-details">
													<h2>Delivery</h2>
													<h3>
														<?php
														if ($patient -> deliver == TRUE) { echo '<i class="fa fa-truck"></i> Deliver';
														} else { '<i class="fa fa-office"></i> Pick up';
														}
 ?>
													</h3>

													<p><?php echo 'Deliverying Instructions: ' . $patient -> deliveryInstructions; ?></p>
													<p><?php
													if ($patient -> deliveryTimeID > 0) {
														$deliveryTimeRef = $this -> comp -> getProductionTimeReference($patient -> deliveryTimeID);
														echo $deliveryTimeRef -> productionDay . $deliveryTimeRef -> productionTime;
													} else {
														echo 'date and time not set';
													}
													?></p>
												</div>


											</div>
										</div>
										<!-- /PROFILE PIC -->
										<!-- PROFILE DETAILS -->
										<div class="col-md-9">
											<!-- ROW 1 -->
											<div class="row">
												<div class="col-md-12">
													<div class="box border blue">
														<div class="box-title">
															<h4>
																<i class="fa fa-columns"></i> <span
																	class="hidden-inline-mobile">Compliance Package</span>
															</h4>
														</div>
														
														
														<div class="box-body">
															<div id="patCompPackLoading"> <center>Loading<br>
																<img
					src="<?php echo base_url(); ?>img/Preloader2.gif"
					alt="" class="img-responsive" height="100"
					width="100"></center></div>
					
															<table id="dtmeds" cellpadding="0" cellspacing="0"
																border="0"
																class="table table-striped table-bordered table-hover">
																
															</table>														
															
														</div>
													</div>
												</div>
											</div>
											<!-- /ROW 1 -->


										</div>
										<!-- /PROFILE DETAILS -->
									</div>
								</div>
								<!-- /OVERVIEW -->


                                <!-- REORDERS -->
                                <div
                                    class="tab-pane fade <?php
									if ($this -> uri -> segment(4) == "reorders") {echo 'in active';
									}
								?>"
                                    id="reorders">
					<div id="patPRNLoading"> <center>Loading<br>
																<img
					src="<?php echo base_url(); ?>img/Preloader2.gif"
					alt="" class="img-responsive" height="100"
					width="100"></center></div>
					
                                    <table id="dtorders" cellpadding="0" cellspacing="0" border="0"
                                           class="table table-hover">                                       
                                    </table>
                                    
                                    <input type="hidden" id="patID"
                                           value="<?php echo $patient -> patID; ?>"> <input type="hidden"
                                                                                         id="reOrderBox">
                                    <button class="btn btn-block btn-success"
                                            onclick="reOrderPRNs()">
                                        <h3>Create Order</h3>
                                    </button>
                                </div>
                                <!-- /REORDERS -->



								<!-- medchanges -->
								<div
									class="tab-pane fade <?php
									if ($this -> uri -> segment(4) == "medchanges") {echo 'in active';
									}
								?>"
									id="medchanges">

									<table cellpadding="0" cellspacing="0" border="0"
										class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th>MedChangeID -</th>
												<th>Created</th>
												<th>Tech</th>
												<th>Pharmacist</th>
												<th>Starts/Ends</th>
												<th>Change</th>
												<th>Action Completed</th>
											</tr>
										</thead>
										<tbody>
										<?php
										$medChange = $this->comp->getMedChange ( '-1', $patient->patID );
										if ($medChange != FALSE) {
											foreach ( $medChange->result () as $medChangeItem ) {
												?>
											<tr
												onclick="loadModal('<?php echo base_url('compliance/editMedChangeModal') . '/' . $patient -> patID . '/' . $medChangeItem -> medChangeID; ?>')">
												<td><?php echo $medChangeItem -> medChangeID; ?></td>
												<td><?php echo $medChangeItem -> dateOfChange; ?></td>
												<td><?php $tech = $this -> ion_auth -> user($medChangeItem -> technicianUserID) -> row();
													echo $tech -> first_name;
												?></td>
												<td><?php
												if ($medChangeItem -> pharmacistUserID > 0) {$pharm = $this -> ion_auth -> user($medChangeItem -> pharmacistUserID) -> row();
													echo $pharm -> first_name;
												}
												?></td>


												<td><?php echo $medChangeItem -> startDate . ' to ' . $medChangeItem -> endDate; ?></td>
												<td><?php echo $medChangeItem -> changeText; ?></td>
												<td><?php
												
												if ($medChangeItem->actionComplete > 0) {
													?>
													<span class="badge badge-green"><i class="fa fa-check"></i></span>
												<?php
													;
												}
												?></td>
											</tr>
											<?php
											}
											} else {
											echo '<tr><td colspan="7"><div class="center">No MED CHANGES</div></td></tr>';
											}
										?>
										</tbody>
									</table>
								</div>
								<!-- /medchanges -->

								<!-- tickets -->
								<div
									class="tab-pane fade <?php
									if ($this -> uri -> segment(4) == "tickets") {echo 'in active';
									}
								?>"
									id="tickets">
									<table id="dtTickets" cellpadding="0" cellspacing="0"
										border="0"
										class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th>Created Date</th>
												<th>Complete Date</th>
												<th>Created By</th>
												<th>Ticket Type</th>
												<th>Description</th>
												<th>Complete</th>
											</tr>
										</thead>
										<tbody>
												<?php
												$generalTicket = $this->comp->getTickets ( '-1', '1', $patient->patID );
												if ($generalTicket != FALSE) {
													foreach ( $generalTicket->result () as $generalTicketItem ) {
														?>
				<tr
												onclick="loadModal('<?php echo base_url('compliance/editTicketModal/') . '/' . $generalTicketItem -> patID . '/' . $generalTicketItem -> ticketID; ?>')">
												<td><?php echo $generalTicketItem -> dateCreated; ?></td>
												<td><?php echo $generalTicketItem -> dateComplete; ?></td>
												<td><?php $ticketCreator = $this -> ion_auth -> user($generalTicketItem -> createdBy) -> row();
													echo $ticketCreator -> first_name;
												?></td>
												<td><?php $refType = $this -> comp -> getTicketTypeReference($generalTicketItem -> ticketTypeReferenceID);
													echo $refType;
												?></td>
												<td><?php echo $generalTicketItem -> Description; ?></td>
												<td><?php
														
														if ($generalTicketItem->dateComplete != '') {
															?>
													<span class="badge badge-green"><i class="fa fa-check"></i></span>
												<?php
															;
														}
														?></td>
											</tr>
							<?php  }}else{echo '<tr><td colspan="6"><div class="center">NO TICKETS</div></td></tr>';} ?>
											</tbody>
									</table>
								</div>
								<!-- /tickets -->

								<!-- mars -->
								<div
									class="tab-pane fade <?php
									if ($this -> uri -> segment(3) == "mars") {echo 'in active';
									}
								?>"
									id="mars">

									<table id="dtmars" cellpadding="0" cellspacing="0" border="0"
										class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th></th>
												<th>Medication</th>
												<th>Scheduled Date/Time</th>
												<th>Qty</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td></td>
												<td>Metformin 500mg Take 1 tab daily Dr. Joe Doe</td>
												<td>23/02/2014 8:00pm</td>
												<td>2 tablets</td>
											</tr>

										</tbody>
									</table>
								</div>
								<!-- /mars -->

								<!-- billing -->
								<div
									class="tab-pane fade <?php
									if ($this -> uri -> segment(4) == "billing") {echo 'in active';
									}
								?>"
									id="billing">
									<h1>We are currently updating the AR Module to include new features. Please check again soon.</h1>
											<?php
											// 											$patAR = $this->pharm->getPatAR ( $patient->patID );

											//
 											?>
											<h2><?php
											// 											if ($patAR != FALSE) {
											// 												echo 'Account #: ' . $patAR->AccountNum . ' Last Statement: ' . $patAR->LastStatementDate;
											// 											}
											?></h2>

											<?php
											// 											$progressBarValue;
											// 											if ($patAR->CreditLimit > 0) {
											// 												$progressBarValue = (($patAR->balance * 100) / $patAR->CreditLimit);
											// 											} else {
											// 												$progressBarValue = 100;
											// 											}
											//
 											?>
										<!-- 	<h4 class="pull-right">Credit Limit: <?php //echo $patAR->CreditLimit; ?></h4>
<!-- 									<div class="progress progress-striped active"> -->
<!-- 										<div class="progress-bar" role="progressbar" -->
<!-- 													aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" -->
												 <!-- 	style="width: <?php //echo $progressBarValue;?>%">  -->
										 <!-- 	<span class="sr-only"><?php //echo 'Balance $ ' . $patAR->balance;?></span>
<!-- 										</div> -->
<!-- 									</div> -->
								<!-- 	<h4 class="pull-right">Balance <?php //echo $patAR->balance;?></h4>


<!-- 									<table id="dtbilling" cellpadding="0" cellspacing="0" -->
<!-- 										border="0" class="table table-bordered table-hover"> -->
<!-- 										<thead> -->
<!-- 											<tr> -->
<!-- 												<th>Invoice #</th> -->
<!-- 												<th>Invoice Date</th> -->
<!-- 												<th>Status</th> -->
<!-- 												<th>SubTotal</th> -->
<!-- 												<th>Tax</th> -->
<!-- 												<th>Paid</th> -->
<!-- 											</tr> -->
<!-- 										</thead> -->
<!-- 										<tbody> -->
												<?php
												// 												$patARInvoices = $this->pharm->getPatARInvoices ( $patient->patID );
												// 												if ($patARInvoices != FALSE) {
												// 													foreach ( $patARInvoices->result () as $invoice ) {
														?> 
<!-- 														<tr -->
									<!-- 			onclick="loadModal('<?php //echo base_url() . 'compliance/viewInvoiceModal/' . $invoice->InvoiceNum;?>')"
												class="<?php //switch ($invoice->Status){ case 1: echo "danger";break; case 0: echo "warning";break; case 3: echo "success";break; }?>">
												<td><?php //echo $invoice->InvoiceNum;?></td>
												<td><?php //echo $invoice->InvoiceDate;?></td>
												<td><?php //switch($invoice->Status){ case 1: echo "Posted";break; case 0: echo "Open";break; case 3: echo "Closed";break; }?></td>
												<td><?php //echo $invoice->SubTotal;?></td>
												<td><?php //echo $invoice->tax;?></td>
												<td><?php // echo $invoice->Paid;?></td>
<!-- 											</tr> -->
												<?php
												// 													}
												// 												} else {
												// 													echo '<tr><td colspan="6"><div class="center">No Data</div></td></tr>';
												// 												}
												//
 												?>
													

<!-- 												</tbody> -->
<!-- 									</table> -->
								</div>
								<!-- /billing -->

								


							</div>
						</div>
						<!-- /USER PROFILE -->
					</div>
				</div>

				<!-- / MAIN PAGE HERE -->
			</div>
			<div class="footer-tools">
				<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
				</span>
			</div>