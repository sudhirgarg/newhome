
<?php
$this->load->library ( 'ion_auth' );
$this->load->helper ( 'url' );

?>

<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left"><?php echo lang('index_heading');?></h3>
</div>
<div class="description">

	<a class="btn btn-default btn-lg"
		href="<?php echo base_url();?>auth/create_user"> <i class="fa fa-plus">
			Create User</i>
	</a> <a class="btn btn-default btn-lg"
		href="<?php echo base_url();?>auth/create_group"> <i
		class="fa fa-plus"> Create Group</i>
	</a>
	
<?php echo lang('index_subheading');?>
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DELIVERY RUNS CONTENT -->
<!-- DATA TABLES -->
<div class="row">


	<div class="col-md-12">
		<div class="box border primary">
			<div class="box-title">
				<h4>
					<i class="fa fa-bars"></i>Users and Permissions
				</h4>
			</div>
			<div class="box-body big">

						<table id="datatable1" cellpadding="0" cellspacing="0" border="0"
					class="datatable table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>First Name</th>
							<th>Last</th>
							<th>Emai</th>
							<th>Groups</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
					
	<?php 
	$users = $this->ion_auth->users();
	
	foreach ($users as $user):?>
		<tr>
							<td><?php echo $user->first_name;?></td>
							<td><?php echo $user->last_name;?></td>
							<td><?php echo $user->email;?></td>
							<td>
				<?php foreach ($user->groups as $group):?>
					<?php echo anchor("auth/edit_group/".$group->id, $group->name) ;?><br />
                <?php endforeach?>
			</td>
							<td><?php echo ($user->active) ? anchor("auth/deactivate/".$user->id, lang('index_active_link')) : anchor("auth/activate/". $user->id, lang('index_inactive_link'));?></td>
							<td><?php echo anchor("auth/edit_user/".$user->id, 'Edit') ;?></td>
						</tr>
	<?php endforeach;?>
</tbody>
				</table>

			</div>
		</div>

	</div>
</div>
<!-- /CONTENT -->



<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>
</div>
<!-- /CONTENT-->
</div>
</div>
</div>
</section>
<!--/PAGE -->
<!-- JAVASCRIPTS -->
<!-- Placed at the end of the document so the pages load faster -->
<!-- JQUERY -->
<script src="<?php echo base_url()?>js/jquery/jquery-2.0.3.min.js"></script>
<!-- JQUERY UI-->
<script
	src="<?php echo base_url()?>js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
<!-- BOOTSTRAP -->
<script
	src="<?php echo base_url()?>js/bootstrap-dist/js/bootstrap.min.js"></script>


<!-- DATA TABLES -->
<script type="text/javascript"
	src="<?php echo base_url();?>js/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript"
	src="<?php echo base_url();?>js/datatables/media/assets/js/datatables.min.js"></script>
<script type="text/javascript"
	src="<?php echo base_url();?>js/datatables/extras/TableTools/media/js/TableTools.min.js"></script>
<script type="text/javascript"
	src="<?php echo base_url();?>js/datatables/extras/TableTools/media/js/ZeroClipboard.min.js"></script>



<!-- DATE RANGE PICKER -->
<script
	src="<?php echo base_url()?>js/bootstrap-daterangepicker/moment.min.js"></script>
<script
	src="<?php echo base_url()?>js/bootstrap-daterangepicker/daterangepicker.min.js"></script>
<!-- SLIMSCROLL -->
<script type="text/javascript"
	src="<?php echo base_url()?>js/jQuery-slimScroll-1.3.0/jquery.slimscroll.min.js"></script>
<script type="text/javascript"
	src="<?php echo base_url()?>js/jQuery-slimScroll-1.3.0/slimScrollHorizontal.min.js"></script>
<!-- BLOCK UI -->
<script type="text/javascript"
	src="<?php echo base_url()?>js/jQuery-BlockUI/jquery.blockUI.min.js"></script>
<!-- TODO -->
<script type="text/javascript"
	src="<?php echo base_url()?>js/jquery-todo/js/paddystodolist.js"></script>
<!-- TIMEAGO -->
<script type="text/javascript"
	src="<?php echo base_url()?>js/timeago/jquery.timeago.min.js"></script>
<!-- FULL CALENDAR -->
<script type="text/javascript"
	src="<?php echo base_url()?>js/fullcalendar/fullcalendar.min.js"></script>
<!-- COOKIE -->
<script type="text/javascript"
	src="<?php echo base_url()?>js/jQuery-Cookie/jquery.cookie.min.js"></script>
<!-- GRITTER -->
<script type="text/javascript"
	src="<?php echo base_url()?>js/gritter/js/jquery.gritter.min.js"></script>
<!-- CUSTOM SCRIPT -->
<script src="<?php echo base_url()?>js/script.js"></script>
<script src="<?php echo base_url()?>js/googlemaps.js"></script>
<script>
		jQuery(document).ready(function() {		
			$('#datatable1').dataTable();
			App.setPage("auth");  //Set current page
			App.init(); //Initialise plugins and elements
		});
	</script>
<!-- /JAVASCRIPTS -->



