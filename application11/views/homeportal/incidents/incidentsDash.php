<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left">Incidents</h3>
</div>
<div class="description">
	<button class="btn btn-primary btn-lg"
		onclick="loadModal('<?php echo base_url("filingCab/editIncidentModal/");?>')">
		<i class="fa fa-plus"></i> Incident
	</button>
	| Discovering trends, increasing awareness
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DASHBOARD CONTENT -->



<!--  ACTIVITY -->
<div class="row">
	<div class="col-lg-12">
		<div class="box border blue">
			<div class="box-title">
				<h4>
					<i class="fa fa-truck"></i>Incidents vs Months
				</h4>
				<div class="tools">
					<a href="#box-config" data-toggle="modal" class="config"> <i
						class="fa fa-cog"></i>
					</a> <a href="javascript:;" class="reload"> <i
						class="fa fa-refresh"></i>
					</a> <a href="javascript:;" class="collapse"> <i
						class="fa fa-chevron-up"></i>
					</a> <a href="javascript:;" class="remove"> <i class="fa fa-times"></i>
					</a>
				</div>
			</div>
			<div class="box-body">
				<div id="mychart" style="height: 200px;"></div>
			</div>
		</div>
	</div>
	<div class="col-lg-12">
		<!-- BOX -->
		<div class="box border blue">
			<div class="box-title">
				<h4>
					<i class="fa fa-reorder"></i> <span class="hidden-inline-mobile">
						Activity</span>
				</h4>
			</div>
			<div class="box-body">
				<table id="datatable1" cellpadding="0" cellspacing="0" border="0"
					class="datatable table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>Date</th>
							<th>Incident Type</th>
							<th>Description</th>
						</tr>
					</thead>
					<tbody>
					<?php
					$incidents = $this->incidents->getIncidents ();
					if ($incidents != false) {
						foreach ( $incidents->result () as $incident ) {
							?>
						<tr
							onclick="loadModal('<?php echo base_url('filingCab/editIncidentModal').'/'.$incident->incidentID;?>')">
							<td><i class="fa fa-clock-o"></i> <?php echo $incident->incidentDatetime;?></td>
							<td><?php echo $this->incidents->getIncidentTypes($incident->incidentType);?></td>
							<td><?php echo $incident->description;?></td>
						</tr>
						<?php }}else{?>  
						<tr>
							<td>No Records Found</td>
						</tr> 
						<?php }?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<!-- /BOX -->


<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>
