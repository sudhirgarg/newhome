<style>
.modal-dialog {
	width: 70%;
}
</style>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"
		aria-hidden="true">&times;</button>
	<h1 class="modal-title">
		<i class="fa fa-info-circle"></i> Seamless Care Pharmacy Information
	</h1>
</div>
<div class="modal-body">


	<div class="row">
		<div class="col-lg-6">
			<center>
				<img src="<?php echo base_url();?>img/teamphoto.jpg"
					alt="Team Photo" class="img-responsive">
			</center>
		</div>
		<div class="col-lg-6">
				<?php $timestamp = time();?>
			<h1>
				<i class="fa fa-clock-o"></i> Hours of Operations
			</h1>
			<h4>After Hours on call support is available 24/7</h4>
			<h4
				<?php if(date('D', $timestamp) == 'Mon') echo 'style="font-weight: bold;"';?>>Mon:
				8:30 am to 7 pm. Pharmacist available till 10 pm</h4>
			<h4
				<?php if(date('D', $timestamp) == 'Tue') echo 'style="font-weight: bold;"';?>>Tue:
				8:30 am to 7 pm. Pharmacist available till 10 pm</h4>
			<h4
				<?php if(date('D', $timestamp) == 'Wed') echo 'style="font-weight: bold;"';?>>Wed:
				8:30 am to 7 pm. Pharmacist available till 10 pm</h4>
			<h4
				<?php if(date('D', $timestamp) == 'Thu') echo 'style="font-weight: bold;"';?>>Thu:
				8:30 am to 7 pm. Pharmacist available till 10 pm</h4>
			<h4
				<?php if(date('D', $timestamp) == 'Fri') echo 'style="font-weight: bold;"';?>>Fri:
				8:30 am to 7 pm. Pharmacist available till 10 pm</h4>
			<h4
				<?php if(date('D', $timestamp) == 'Sat') echo 'style="font-weight: bold;"';?>>Sat:
				9 am to 5 pm. Pharmacist available till 10 pm</h4>
			<h4
				<?php if(date('D', $timestamp) == 'Sun') echo 'style="font-weight: bold;"';?>>Sun:
				after hours services available. Pharmacist available till 10 pm</h4>
		</div>
	</div>


	<div class="row">
		<div class="col-lg-6">
			<h1>
				<i class="fa fa-location-arrow"></i> Address
			</h1>
			
			<h3>15 Grand Marshall Drive,</h3>
			<h3> Scarborough, ON. M1B 5N6</h3>
			<h3></h3>
		</div>
		<div class="col-lg-6">
			<h1>
				<i class="fa fa-phone"></i> Call us
			</h1>
			<h3>(416) 281 9900</h3>
			<h3>(877) 666 9502</h3>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-6">
			<h1>
				<i class="fa fa-envelope"></i> Email us
			</h1>
			<h3>info@seamlesscare.ca</h3>
		</div>
		<div class="col-lg-6">
			<h1>
				<i class="fa fa-print"> Fax</i>
			</h1>
			<h3>(416) 281 9905</h3>
			<h3>Toll Free fax 1-877-666-0902</h3>
		</div>
	</div>


	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal"
			id="closeButtonModal">Close</button>
	</div>
</div>

