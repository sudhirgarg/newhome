<?php
$this -> load -> library('ion_auth');
$this -> load -> helper('url');

$patient = $this -> portal -> getCompliancePatient($this -> uri -> segment(3));
?>

<script>
	$(".datepicker-fullscreen").pickadate({
		showOtherMonths : true,
		autoSize : true,
		appendText : '<span class="help-block">(yyyy-mm-dd)</span>',
		dateFormat : "yy-mm-dd"
	});
</script>

<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left"><?php echo $this -> lang -> line('CLIENT_PROFILE'); ?></h3>
</div>
<div class="description">
	<a class="btn btn-lg btn-default"
		href="<?php echo base_url()?>homeportal/patients"><i
		class="fa fa-chevron-left"></i> <?php echo $this -> lang -> line('CLIENTS'); ?></a>

	<button class="btn btn-lg btn-primary"
		onclick="loadModal('<?php echo base_url() . 'homeportal/editTicketModal/' . $patient -> patID; ?>/-1')">
		<i class="fa fa-plus"></i> <?php echo $this -> lang -> line('NEW_TICKETS'); ?>
	</button>

	<a class="btn btn-lg btn-primary"
		href="<?php echo base_url() . 'homeportal/clientEvents/' . $patient -> patID; ?>/-1">
		<i class="fa fa-eye"></i> <?php echo $this -> lang -> line('HEALTH_PROFILE'); ?>
	</a> 
	
		
	<a class="btn btn-lg btn-primary"
		href="<?php echo base_url() . 'homeportal/healthindicatorsdash/' . $patient -> patID; ?>/-1">
		<i class="fa fa-bar-chart-o fa-fw"></i> Health Indicators
	</a> 

	
	<a class="btn btn-lg btn-warning pull-right"
		href="<?php echo base_url() . 'homeportal/printPatProfile/' . $patient -> patID; ?>/-1">
		<i class="fa fa-print"></i> <?php echo $this -> lang -> line('PRINT_PATIENT_PROFILE'); ?>
	</a>
	
	<a class="btn btn-lg btn-success" href=""> <i class="fa fa-table"></i> eMARS is Comming soon </a>
	<!--?php echo base_url() . 'homeportal/emars/' . $patient -> patID; ?-->
	<input id="patID" value="<?php echo $patient -> patID; ?>" type="hidden">

</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!--  MAIN PAGE HERE -->

<div class="row">
	<div class="col-lg-12">

		<!-- USER PROFILE -->
		<div class="row">
			<div class="col-md-12">
				<!-- BOX -->
				<div class="box border">
					<div class="box-title">
						<h4>
							<i class="fa fa-user"></i><span class="hidden-inline-mobile"></span>
						</h4>
					</div>
					<div class="box-body">
						<div class="tabbable header-tabs user-profile">
							<ul class="nav nav-tabs">
								
										
								<li
									<?php
									if ($this -> uri -> segment(4) == "medchanges") {echo 'class="active"';
									}
								?>><a
									href="#medchanges" data-toggle="tab"><i class="fa fa-user-md"></i>
										<span class="hidden-inline-mobile"> <?php echo $this -> lang -> line('MED_CHANGES'); ?></span></a></li>
								<li
									<?php
									if ($this -> uri -> segment(4) == "tickets") {echo 'class="active"';
									}
								?>><a
									href="#tickets" data-toggle="tab"><i class="fa fa-ticket"></i>
										<span class="hidden-inline-mobile"> <?php echo $this -> lang -> line('SERVICE_TICKETS'); ?></span></a></li>

								<li
									<?php
									if ($this -> uri -> segment(4) == "reorders") {echo 'class="active"';
									}
								?>><a
									href="#reorders" data-toggle="tab"><i class="fa fa-bar-chart-o"></i>
										<span class="hidden-inline-mobile"> <?php echo $this -> lang -> line('RE_ORDERS'); ?></span></a></li>

								<li
									<?php
									if ($this -> uri -> segment(4) == "profile" || $this -> uri -> segment(3) == "") {echo 'class="active"';
									}
								?>><a
									href="#pro_overview" data-toggle="tab"><i
										class="fa fa-dot-circle-o"></i> <span
										class="hidden-inline-mobile"><?php echo $this -> lang -> line('PROFILE'); ?></span></a></li>
							</ul>
							<div class="tab-content">
								<!-- OVERVIEW -->
								<div
									class="tab-pane fade <?php
									if ($this -> uri -> segment(4) == "profile") {echo 'in active';
									}
								?>"
									id="pro_overview">
									<div class="row">
										<!-- PROFILE PIC -->
										<div class="col-md-3">
											<div class="list-group">
												<li class="list-group-item zero-padding">
													<!--  <img alt=""
													class="img-responsive"
													src="<?php //  echo base_url();?>img/profile/avatar.jpg">-->
												</li>
												<div class="list-group-item profile-details">
													<h2><?php  echo $patient -> patInitials . '   -  ' . $patient -> patID; ?> </h2>

												</div>
												<div class="list-group-item profile-details">
													<h2><i class="fa fa-home"></i> <?php echo $this -> lang -> line('HOME'); ?></h2>
												<?php
												$patientHomes = $this->comp->getPatHomes ( $patient->patID, '0' );
												if ($patientHomes == FALSE) {
													?> <?php
													} else {
													$home = $this->comp->getHome ( $patientHomes );
													if ($home != FALSE) {
														?>	
													<h4>
														<?php echo $home -> homeName; ?></h4>
													<p><?php //echo $home->homeAddress; ?>	</p>
													<p>
													<?php //echo $home->phone; ?>	
													</p>
													<?php
													} else {
														?>
														<button class="btn pull-right" type="button"
														onclick="loadModal('<?php echo base_url("compliance/assignPatToHomeModal") . '/' . $patient -> patID . '/0'; ?>')">Modify</button>
														<?php
														}
														}
												?>
												</div>

												<div class="list-group-item profile-details">
													<h2><?php echo $this -> lang -> line('ALLERGIES'); ?></h2>
													
													<div id="alergiesTable">
														<div id="patAlergyLoading"> <center>Loading<br>
															<img src="<?php echo base_url(); ?>img/Preloader2.gif"
															alt="" class="img-responsive" height="50"
															width="50"></center></div>
																							</div>

												</div>
												
												<div class="list-group-item profile-details">
													<h2><?php echo $this -> lang -> line('CONDITIONS'); ?></h2>
													
													
													<div id="conditionsTable">
														<div id="patConditionsLoading"> <center>Loading<br>
															<img src="<?php echo base_url(); ?>img/Preloader2.gif"
															alt="" class="img-responsive" height="50"
															width="50"></center></div>
																							</div>
													
												</div>
												<div class="list-group-item profile-details">
													<h2><?php echo $this -> lang -> line('NOTE'); ?></h2>
													<p><?php echo $patient -> note; ?></p>
												</div>
												

												<div class="list-group-item profile-details">
													<h2><?php

													echo $this -> lang -> line('DrugPlan');
													$patDrugCard = $this -> pharm -> getPatDrugPlan($patient -> patID);
													?></h2>
													<?php if ($patDrugCard!= false){?>
													<h3>
														<?php echo $patDrugCard -> SubPlanCode; ?>
													</h3>
													<p><?php

													$patplaninfo = str_replace('fs20', '', strstr($patDrugCard -> Comment, 'fs20'));
													$patplaninfo = str_replace('\par', '', $patplaninfo);
													$patplaninfo = str_replace('}', '', $patplaninfo);
													echo $patplaninfo;
														?></p>	
													<?php } ?>											
												</div>

												<div class="list-group-item profile-details">
													<h2><?php echo $this -> lang -> line('DELIVERY'); ?></h2>
													<h3>
														<?php
														if ($patient -> deliver == TRUE) { echo '<i class="fa fa-truck"></i> Deliver';
														} else { '<i class="fa fa-office"></i> Pick up';
														}
 ?>
													</h3>

													<p><?php echo 'Deliverying Instructions: ' . $patient -> deliveryInstructions; ?></p>
													<p><?php
													if ($patient -> deliveryTimeID > 0) {
														$deliveryTimeRef = $this -> comp -> getProductionTimeReference($patient -> deliveryTimeID);
														echo $deliveryTimeRef -> productionDay . $deliveryTimeRef -> productionTime;
													} else {
														echo 'date and time not set';
													}
													?></p>
												</div>


											</div>
										</div>
										<!-- /PROFILE PIC -->
										<!-- PROFILE DETAILS -->
										<div class="col-md-9">
											<!-- ROW 1 -->
											<div class="row">
												<div class="col-md-12">
													<div class="box border blue">
														<div class="box-title">
															<h4>
																<i class="fa fa-columns"></i> <span
																	class="hidden-inline-mobile">Compliance Package</span>
															</h4>
														</div>
														<div class="box-body">
															<div id="patCompPackLoading"> 
																<center>Loading<br>
																	<img src="<?php echo base_url(); ?>img/Preloader2.gif" alt="" class="img-responsive" height="100"width="100">
																</center>
															</div>
															<table id="dtmeds" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover datatable table-responsive">
																
																 <!--thead >
																		<tr>												
																			<th>Medication</th>
																			<th>Doctor</th>
																			<th>Directions</th>
																		</tr>
																	</thead>
																	<tbody>
																	<?php
																	
																	$compPack = $this -> pharm -> getPatCompliancePackage($patient -> patID);
																		if ($compPack != FALSE) {
																			foreach ($compPack->result () as $drg) {
																				echo '<tr>';
																				echo '<td>' . $drg -> BrandName . ' ' . $drg -> Strength . '<br>RX-' . $drg -> RxNum . '</td>';
																				echo '<td>' . $drg -> doc . '</td>';
																				echo '<td>' . $drg -> SIG . '</td>';
																				echo '</tr>';
																			}
																		} else {
																			echo '<tr><td colspan="3">No Medication Recorded</td></tr>';
																		}
																										
																	?>
																	</tbody-->
															</table>
														</div>
													</div>
												</div>
											</div>
											<!-- /ROW 1 -->
										</div>

										<!-- /PROFILE DETAILS -->
									</div>
								</div>
								<!-- /OVERVIEW -->


								 <!-- REORDERS -->
                                <div id="reorders" class="tab-pane fade <?php if ($this -> uri -> segment(4) == "reorders") {echo 'in active'; } ?>" >
                                	
								<div id="patPRNLoading"> 
									<center>Loading<br>
										<img src="<?php echo base_url(); ?>img/Preloader2.gif" alt="" class="img-responsive" height="100" width="100">
									</center>
								</div>
					
                                 <table id="dtorders" cellpadding="0" cellspacing="0" border="0"  class="table table-striped table-bordered table-hover datatable table-responsive"> 
                                           
                                            <!--thead >
													<tr>												
														<th>Medication</th>
														<th>Doctor</th>
														<th>Directions</th>
														<th>Comment</th>
														<th>Select</th>
													</tr>
												</thead>
												<tbody>
												<?php
												
												$patPRNs = $this -> pharm -> getPatPRNs($patient -> patID);
												if ($patPRNs != FALSE) {
													$checkBoxCount = 1;
													foreach ($patPRNs->result () as $patPRN) {
														$mix='';
														if($patPRN->MixID>0){$mix='class="warning"';}
														echo '<tr id="reOrderList" ' . $mix .'>';
														echo '<td>' . $patPRN -> BrandName . ' ' . $patPRN -> Strength . '<br>RX-' . $patPRN -> RxNum . '</td>';
														echo '<td>' . $patPRN -> doc . '</td>';
														echo '<td>' . $patPRN -> SIG . '</td>';
														echo '<td><input type="text" name="reOrderComment" class="form-control reOrderCommentClass" id="comment' . $checkBoxCount . '" ></td>';
														echo '<td><input type="checkbox" name="reOrderCheckBox" class="form-control reOrderCheckBoxClass" value="' . $patPRN -> RxNum . '" id="checkBox' . $checkBoxCount++ . '"></td>';
														echo '</tr>';
													}
												} else {
													echo '<tr><td>No Medication Recorded..</td></tr>';
												}
																					
												?>
											</tbody-->                                     
                                    </table>
                                    
                                    
                                    
                                    <input type="hidden" id="patID"
                                           value="<?php echo $patient -> patID; ?>"> <input type="hidden"
                                                                                         id="reOrderBox">
                                    <button class="btn btn-block btn-success"
                                            onclick="reOrderPRNs()">
                                        <h3>Create Order</h3>
                                    </button>
                                </div>
                                <!-- /REORDERS -->


								<!-- medchanges -->
								<div
									class="tab-pane fade <?php
									if ($this -> uri -> segment(4) == "medchanges") {echo 'in active';
									}
								?>"
									id="medchanges">

									<table cellpadding="0" cellspacing="0" border="0" id="medChanges"
										class="table table-striped table-bordered table-hover datatable table-responsive">
										<thead >
											<tr>
												<th>ID</th>
												<th><?php echo $this -> lang -> line('CREATED'); ?></th>
												<th><?php echo $this -> lang -> line('TECHNICIAN'); ?></th>
												<th><?php echo $this -> lang -> line('PHARMACIST'); ?></th>
												<th><?php echo $this -> lang -> line('STARTS_ENDS'); ?></th>
												<th><?php echo $this -> lang -> line('CHANGE'); ?></th>
												<th><?php echo $this -> lang -> line('COMPLETED'); ?></th>
											</tr>
										</thead>
										<tbody>
										<?php
										$medChange = $this->comp->getMedChange ( '-1', $patient->patID );
										if ($medChange != FALSE) {
											foreach ( $medChange->result () as $medChangeItem ) {
												?>
											<tr
												onclick="loadModal('<?php echo base_url('homeportal/editMedChangeModal') . '/' . $patient -> patID . '/' . $medChangeItem -> medChangeID; ?>')">
												<td><?php echo $medChangeItem -> medChangeID; ?></td>
												<td><?php echo $medChangeItem -> dateOfChange; ?></td>
												<td><?php $tech = $this -> ion_auth -> user($medChangeItem -> technicianUserID) -> row();
													echo $tech -> first_name;
												?></td>
												<td><?php
												if ($medChangeItem -> pharmacistUserID > 0) {$pharm = $this -> ion_auth -> user($medChangeItem -> pharmacistUserID) -> row();
													echo $pharm -> first_name;
												}
												?></td>


												<td><?php echo $medChangeItem -> startDate . ' to ' . $medChangeItem -> endDate; ?></td>
												<td><?php echo $medChangeItem -> changeText; ?></td>
												<td><?php
												
												if ($medChangeItem->actionComplete > 0) {
													?>
													<span class="badge badge-green"><i class="fa fa-check"></i></span>
												<?php
													;
												}
												?></td>
											</tr>
											<?php
											}
											} else {
											echo '<tr><td colspan="7"><div class="center">No MED CHANGES</div></td></tr>';
											}
										?>
										</tbody>
									</table>
								</div>
								<!-- /medchanges -->

								<!-- tickets -->
								<div
									class="tab-pane fade <?php
									if ($this -> uri -> segment(4) == "tickets") {echo 'in active';
									}
								?>"
									id="tickets">
									<table id="dtTickets" cellpadding="0" cellspacing="0"
										border="0"
										class="table table-striped table-bordered table-hover datatable table-responsive">
										<thead>
											<tr>
												<th><?php echo $this -> lang -> line('CREATED'); ?></th>
												<th><?php echo $this -> lang -> line('COMPLETED_DATE'); ?></th>
												<th><?php echo $this -> lang -> line('CREATED_BY'); ?></th>
												<th><?php echo $this -> lang -> line('TICKET_TYPE'); ?></th>
												<th>Ranks</th>
												<th><?php echo $this -> lang -> line('DESCRIPTION'); ?></th>
												<th><?php echo $this -> lang -> line('COMPLETED'); ?></th>
											</tr>
										</thead>
										<tbody>
												<?php
												$generalTicket = $this->comp->getTickets ( '-1', '1', $patient->patID );
												if ($generalTicket != FALSE) {
													foreach ( $generalTicket->result () as $generalTicketItem ) {
														?>
				                         <tr	onclick="loadModal('<?php echo base_url('homeportal/editTicketModal/') . '/' . $generalTicketItem -> patID . '/' . $generalTicketItem -> ticketID; ?>')">
												<td><?php echo $generalTicketItem -> dateCreated; ?></td>
												<td><?php echo $generalTicketItem -> dateComplete; ?></td>
												<td><?php $ticketCreator = $this -> ion_auth -> user($generalTicketItem -> createdBy) -> row();
													echo $ticketCreator -> first_name;
												?></td>
												<td><?php $refType = $this -> comp -> getTicketTypeReference($generalTicketItem -> ticketTypeReferenceID);
													echo $refType;
												?></td>
												<td><?php if(empty($generalTicketItem -> rank)) { echo "0.0"; } else { echo $generalTicketItem -> rank; }   ?> </td>
												<td><?php echo $generalTicketItem -> Description; ?></td>
												<td><?php
														
												if ($generalTicketItem->dateComplete != '') { ?>
													<span class="badge badge-green"><i class="fa fa-check"></i></span>
												 <?php	} ?></td>
											</tr>
							<?php  }}else{echo '<tr><td colspan="6"><div class="center">NO TICKETS</div></td></tr>';} ?>
											</tbody>
									</table>
								</div>
								<!-- /tickets -->

								</div>

							</div>
						</div>
						<!-- /USER PROFILE -->
					</div>
				</div>

				<!-- / MAIN PAGE HERE -->
			</div>
			<div class="footer-tools">
				<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
				</span>
			</div>