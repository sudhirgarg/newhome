<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left">eMARS</h3>
</div>
<div class="description">

</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!--  MAIN PAGE HERE -->

<div class="row">
	<div class="col-lg-12">

		<!-- BOX -->
		<div class="box border">
			<div class="box-title">
				<h4><i class="fa fa-columns"></i><span class="hidden-inline-mobile">Administer Medication</span></h4>
			</div>
			<div class="box-body">
				<div class="tabbable header-tabs">
					<ul class="nav nav-tabs">
						<li>
							<a href="#box_tab3" data-toggle="tab"><i class="fa fa-circle-o"></i> <span class="hidden-inline-mobile">View & Print MARs</span></a>
						</li>
						<li>
							<a href="#box_tab2" data-toggle="tab"><i class="fa fa-laptop"></i> <span class="hidden-inline-mobile">PRNs</span></a>
						</li>
						<li class="active">
							<a href="#box_tab1" data-toggle="tab"><i class="fa fa-calendar-o"></i> <span class="hidden-inline-mobile">Scheduled Medication</span></a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane fade in active" id="box_tab1">
							<input disabled type="text" class="form-control"
							value="<?php
							$date = date_create(date("Y/m/d"));
							$time_in_12_hour_format = date("g:i a", strtotime(date("h:i:sa")));
							echo "Administration Date: " . date_format($date, "d F Y") . " " . $time_in_12_hour_format;
							?>">
							<br>

							<table id="dtmeds" cellpadding="0" cellspacing="0"
							border="0"
							class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th> Dose Type </th>
										<th> Dose Date </th>
										<th> Medication </th>
										<th> Instructions </th>
										<th> Administer </th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><span class="label label-success">Compliance</span></td>

										<td>2015 Aug 21 <h4 class="strong">10 AM</h4></td>
										<td>
										<p>
											1231
										</p>
										<p>
											2 Tabs - Apo-Metformin
										</p></td>

										<td>Take one tab daily</td>
										<td>
										<input type="checkbox" class="form-control" value="" >
										</td>
									</tr>
									<tr>
										<td><span class="label label-danger">PRN</span></td>
										<td></td>
										<td>
										<p>
											1231
										</p>
										<p>
											2 Tabs - Apo-Metformin
										</p></td>

										<td>Take one tab daily</td>
										<td>
										<input type="checkbox" class="form-control" value="" >
										</td>
									</tr>
								</tbody>
							</table>
							<br>
							<textarea class="form-control" rows="3" id="changeText" placeholder="Administration Notes/Comments"></textarea>
							<br>	
							<div class="form-group">
								<div class="col-sm-2">
								  <input type="text" class="form-control" id="check1" placeholder="check 1">
								</div>
								<div class="col-sm-2">
								  <input type="text" class="form-control" id="check2" placeholder="check 2">
								</div>
								<button class="btn btn-lg btn-success col-sm-8">
								Administer Selected
								</button>
							  </div>
							  <br>
							  <br>
						</div>
						<div class="tab-pane fade" id="box_tab2">

						</div>
						<div class="tab-pane fade" id="box_tab3">
							<h2>Print MARS</h2>
							<h4>Year
							<select class="form-control" id="ticketTypeReferenceID">
								<?php
								for ($year = date("Y"); $year >= 2015; $year = $year - 1) {
									printf('<option value="%u">%u</option>', $year, $year);
								}
								?>
							</select></h4>
							<h4>Month
							<select class="form-control" id="ticketTypeReferenceID">
								<?php
								$months = array(1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec');
								foreach ($months as $num => $name) {
									printf('<option value="%u">%s</option>', $num, $name);
								}
								?>
							</select></h4>
							<br>
							<button class="btn btn-block btn-warning">
								View & Print
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /BOX -->

	</div>
</div>

<!-- / MAIN PAGE HERE -->
</div>
<!-- /CALENDAR & CHAT -->
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top </span>
</div>

