<?php
$patID = $this->uri->segment ( 3 );
$eventID = $this->uri->segment ( 4 );
$event;
if ($this->uri->segment ( 4 ) > 0) {
	$event = $this->portal->getPatEvents ( $patID, $eventID );
}
?>

<script>
$(".datepicker-fullscreen").pickadate();
$(".timepicker-fullscreen").pickatime();
</script>


<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"
		aria-hidden="true">&times;</button>
	<h4 class="modal-title">Psychotropic PRN Effectiveness Tracking Sheet - <?php echo $patID; ?></h4>
</div>
<div class="modal-body">
	<div class="form-horizontal" role="form">

		<!-- eventID -->
		<input id="eventID" type="hidden" value="<?php  echo $event->id;?>">

		<!-- patID -->
		<input id="patID" type="hidden" value="<?php  echo $event->patID;?>">

		<!-- EventDateTime -->
		<div class="form-group">
			<label class="col-sm-3 control-label">Date & Time PRN was given: </label>
			<div class="col-sm-9">
				<input type="text" id="eventDate"
					class="form-control datepicker-fullscreen"
					value="<?php echo $event->eventDate;?>" disabled>
			</div>
		</div>

		<!-- Event Reported By -->
		<div class="form-group">
			<label class="col-sm-3 control-label">Reported By</label>
			<div class="col-sm-9">

				<input type="text" class="form-control" id="reportedByUserName"
					placeholder="Text input"
					value="<?php
					$user = $this->ion_auth->user ( $event->reportedByUser )->row ();
					echo $user->first_name . ' ' . $user->last_name;
					?>"
					disabled>
			</div>
		</div>

		<!-- Reason PRN given -->
		<div class="form-group">
			<label class="col-sm-3 control-label">Reason PRN given:</label>
			<div class="col-sm-9">
				<textarea class="form-control" id="desc" rows="4" disabled><?php echo $event->desc ;?></textarea>
			</div>
		</div>

		<!-- 
		<div class="form-group">
			<label class="col-sm-3 control-label"> In each time interval, comment
				on the progress of PRN effectiveness (Required)</label>
			<div class="col-sm-9">
				<label class="col-sm-9 control-label" style="text-align: left;"> 1)
					Note any changes in frequency, intensity or duration of behaviour </label>

				<label class="col-sm-9 control-label" style="text-align: left;"> 2)
					Note any medication side effects. </label> <label
					class="col-sm-9 control-label" style="text-align: left;"> 3) Note
					observable behaviour(s) that indicate that PRN was effective </label>
			</div>
		</div>
 -->
		<!-- PRN Effectiveness monitoring -->
		<div class="form-group">
			<label class="col-sm-3 control-label">PRN Effectiveness monitoring. In each time interval comment on:
			<br> 1) Note frequency, intensity, duration of behaviour 
			<br> 2) Note any medication side effects
			<br> 3) Note observable behaviour(s) that indicate that PRN was effective 
			</label>
			<div class="col-sm-9">
				<textarea class="form-control" id="prn1desc" rows="6"><?php
				if (strcmp($event->prn1desc, "")!=0) {
					echo $event->prn1desc;
				} else {
					echo '0-15:&#13;&#10;15-30:&#13;&#10;30-60:&#13;&#10;60-90:&#13;&#10;90-120:&#13;&#10';
				}
				?></textarea>
			</div>
		</div>

		<!-- Second PRN Effectiveness monitoring -->
		<div class="form-group">
			<label class="col-sm-2 control-label">Was a subsequent PRN required
				for continuation of behaviour</label>
			<div class="col-sm-1">
				<input type="checkbox" name="SubsequentGiven" class="form-control"
					id="prn2given" value="<?php echo $event->prn2given;?>" <?php if($event->prn2given==TRUE){echo 'checked';}?> >
			</div>
			<div class="col-sm-9" id="subprn" style="display:<?php if($event->prn2given==TRUE){echo 'block';} else { echo 'none';}?>;">
				<textarea class="form-control" id="prn2desc" rows="6"><?php
				if (strcmp($event->prn2desc, "")!=0) {
					echo $event->prn2desc;
				} else {
					echo '0-15:&#13;&#10;15-30:&#13;&#10;30-60:&#13;&#10;60-90:&#13;&#10;90-120:&#13;&#10';
				}
				?></textarea>
			</div>
		</div>


		<!-- Lower row -->
		<div class="form-group subprnCheckBox">
			<label class="col-sm-2 control-label">PRN Protocol Followed</label>
			<div class="col-sm-1">
				<input type="checkbox" name="protocol" class="form-control" id="protocol"
					value="<?php echo $event->protocol; ?>" <?php if($event->protocol==1){echo 'checked';}?>>
			</div>
			
			<label class="col-sm-2 control-label">Time PRN Effective</label>
			<div class="col-sm-3">							
					<input type="text" class="form-control datepicker-fullscreen"
					id="dateEffective"
					value="<?php echo substr($event->timeEffective,0,10); ?>">
					
					<input type="text" id="timeEffective" name="regular"
					class="form-control timepicker-fullscreen"
					value="<?php if ($event->timeEffective !=""){$time_in_12_hour_format  = date("g:i a", strtotime(substr($event->timeEffective,10,6))); 
					echo $time_in_12_hour_format;} ?>">
			</div>

			<label class="col-sm-2 control-label">Staff Initial</label>
			<div class="col-sm-2">
				<input type="text" class="form-control" id="staffInitial"
					placeholder="Text input" value="<?php echo $event->staffInitial; ?>" >
			</div>
		</div>

	</div>
	
	<!-- FOOTER -->
	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal"
			id="closeButtonModal">Close</button>
		<button type="button" class="btn btn-primary"
			id="savePRNButton" onclick="savePRNMonitor()"
			data-loading-text="Saving...">Save changes</button>
	</div>

<script>
	$(document).ready(function(){
	    $("#prn2given").click(function(){
	    	if($(this).is(":checked")) {
	    	    // checkbox is checked -> do something
	       	 $("#subprn").show();
	       	 $("#prn2given").val("1");
	       	 
	       } else {
	           // checkbox is not checked -> do something different
	       	 $("#subprn").hide();
	       	 $("#prn2given").val("0");
	       }
	    });

	    $("#protocol").click(function(){
	    	if($(this).is(":checked")) {
	    	    // checkbox is checked -> do something
	       	 $("#protocol").val("1");
	       	 
	       } else {
	           // checkbox is not checked -> do something different
	       	 $("#protocol").val("0");
	       }
	    });
	    
	});
	</script>