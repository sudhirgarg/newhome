<?php
$patID = $this->uri->segment ( 3 );
$patient = $this->comp->getCompliancePatient ( $patID );
$user = $this->ion_auth->user ()->row ();
?>

	<?php
	$PRNStats = $this->portal->getThisMonthPRNStats ( $patID );
	foreach ( $PRNStats->result () as $PRNRow ) {
		if ($PRNRow->total == 'total') {
			$PRNtotal = $PRNRow->PR;
		}
		if ($PRNRow->total == 'protocol') {
			$prnProtocol = $PRNRow->PR;
		}
		if ($PRNRow->total == 'prn2given') {
			$PRN2given = $PRNRow->PR;
		}
		if ($PRNRow->total == 'effective') {
			$PRNEffective = $PRNRow->PR;
		}
	}
	?>

<!-- STYLER -->

<style>
.modal-dialog {
	width: 60%;
}
</style>
<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left"><?php echo $patient->patInitials . $patID;?> Client Events</h3>

</div>
<div class="description">
	<a class="btn btn-default btn-lg"
		href="<?php echo base_url() ."homeportal/viewPat/" . $patID . "/profile"; ?>">
		<i class="fa fa-chevron-left"></i> Back to <?php echo $patient->patInitials;?> Profile
	</a>
	<button class="btn btn-primary btn-lg"
		onclick="loadModal('<?php echo base_url() ."homeportal/eventModal/" . $patID; ?>')">
		<i class="fa fa-plus"></i> Event
	</button>
	| Discovering trends, increasing awareness
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DASHBOARD CONTENT -->

<!--  ACTIVITY -->
<div class="row">
	<div class="col-lg-12">
		<div class="box border">

			<div class="box-body">
				<div class="row">
					<div class="col-lg-12">
						<h1><?php echo $patient->patInitials;?>'s Events Dashboard</h1>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-1"></div>
					<div class="col-lg-4 center">
						<hr>
						<h1>Activity this month</h1>
						<br>
						
						<?php
						$Last7Data = $this->portal->get7DaysEventType ( $patID );
						$BM7 = "";
						$UR7 = "";
						$SE7 = "";
						$PR7 = "";
						$ER7 = "";
						$SZ7 = "";
						foreach ( $Last7Data->result () as $row ) {
							$BM7 = $BM7 . $row->BM . ",";
							$UR7 = $UR7 . $row->UR . ",";
							$SE7 = $SE7 . $row->SE . ",";
							$PR7 = $PR7 . $row->PR . ",";
							$ER7 = $ER7 . $row->ER . ",";
							$SZ7 = $SZ7 . $row->SZ . ",";
						}
						$BM7 = rtrim ( $BM7, "," );
						$UR7 = rtrim ( $UR7, "," );
						$SE7 = rtrim ( $SE7, "," );
						$PR7 = rtrim ( $PR7, "," );
						$ER7 = rtrim ( $ER7, "," );
						$SZ7 = rtrim ( $SZ7, "," );
						
						$pieData = $this->portal->getThisMonthEventTypes ( $patID );
						
						?>
						<div>
							<span class="pull-left">
								<?php
								echo date ( 'l j S', strtotime ( '-7 days', strtotime ( date ( "Y-m-d" ) ) ) );
								?>
								</span> <span class="pull-right">Today <?php echo date("j S");?></span>
						</div>
						<hr>
						<div class="sparkline-row">
							<span class="title" style="color: #009900;"><i
								class="pull-left roundicon fa fa-male fa-3x white"></i>
								<h2><?php echo $pieData->BM;?> Bowel Movements</h2> </span> <span
								class="sparklines big" data-fill-color="#ffada4"><?php echo $BM7;?></span>
						</div>
						<hr>
						<div class="sparkline-row">
							<span class="title" style="color: #CFCF00;"><i
								class="pull-left roundicon fa fa-3x fa-tint"></i>
								<h2><?php echo $pieData->UR;?> Urine Discharges</h2> </span> <span
								class="sparklines big" data-color="green"><?php echo $UR7;?></span>
						</div>
						<hr>
						<div class="sparkline-row">
							<span class="title" style="color: #66CCFF;"> <i
								class="pull-left roundicon fa fa-moon-o fa-3x"></i>
								<h2><?php echo $pieData->SE;?> Sleep Events</h2>
							</span> <span class="sparklines big" data-color="red"><?php echo $SE7;?></span>
						</div>
						<hr>
						<div class="sparkline-row">
							<span class="title" style="color: #0000CC;"><i
								class="pull-left roundicon fa fa-edit fa-3x"></i>
								<h2><?php echo $pieData->PR;?> PRN Administrations</h2></span><span
								class="sparklines big" data-color="green"><?php echo $PR7;?></span>
						</div>
						<hr>
						<div class="sparkline-row">
							<span class="title" style="color: #FF0000;"> <i
								class="pull-left roundicon fa fa-ambulance  fa-3x "></i>
								<h2><?php echo $pieData->ER;?> Emergency Room Visits</h2>
							</span> <span class="sparklines big" data-color="green"><?php echo $ER7;?></span>
						</div>
						<hr>
						<div class="sparkline-row">
							<span class="title" style="color: #FF9900;"><i
								class="pull-left roundicon fa  fa-3x fa-flash "></i>
								<h2><?php echo $pieData->SZ;?> Seizures</h2></span><span
								class="sparklines big" data-color="green"><?php echo $SZ7;?></span>
						</div>

					</div>
					<div class="col-lg-1"></div>
					<div class="col-lg-6">
						<div class="row">
							<hr>
							<h2 class="center">Types of events in <?php echo date("F"); ?></h2>


							<div id="canvas-holder" class="center">
								<canvas id="chart-area" width="200" height="200" />
							</div>

							<hr>


							<h2 class="center"><?php echo $PRNtotal?> PRNs in <?php echo date("F"); ?></h2>

							<div class="center" id="g1"></div>

							<div class="center" id="g2"></div>

							<div class="center" id="g3"></div>

						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-1"></div>
					<div class="col-lg-10 center">
						<hr>
						<h2>Events for this year: <?php echo date("Y"); ?></h2>

						<canvas id="myChart" style="height: 400px;"></canvas>
					</div>
					<div class="col-lg-1"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-12">
		<div class="col-lg-12">
			<!-- BOX -->
			<div class="box border">
				<div class="box-title">
					<h4>
						<i class="fa fa-reorder"></i> <span class="hidden-inline-mobile">
							Activity</span>
					</h4>
				</div>
				<div class="box-body">

					<div data-always-visible="1" data-rail-visible="1">
					<?php
					$patEvents = $this->portal->getPatEvents ( $patID );
					if ($patEvents != false) {
						foreach ( $patEvents->result () as $event ) {
							?>
					<div class="feed-activity clearfix">
							<div>
						<?php
							
							switch ($event->eventTypeID) {
								
								case 1 :
									?><i class="pull-left roundicon fa fa-male btn btn-success"></i> Bowel Movement: 									
																 
																<?php
									break;
								case 2 :
									?>
									<i class="pull-left roundicon fa fa-moon-o btn btn-info"></i> Sleep Event: 
																
																<?php
									break;
								case 3 :
									?>
																	<i
									class="pull-left roundicon fa fa-edit btn btn-primary"></i>
								<button class="btn btn-primary btn-sm"
									onclick="loadModal('<?php echo base_url() ."homeportal/prnAdminModal/" . $patID . '/' . $event->id ; ?>')">
									<i class="fa fa-eye"></i> PRN Administration - Click to monitor
									PRN Effectiveness
								</button>
																<?php
									break;
								case 4 :
									?>
									<i class="pull-left roundicon fa fa-ambulance btn btn-danger"></i> Emgergency Room Visit: 
															 
																<?php
									break;
								case 5 :
									?>
																<i
									class="pull-left roundicon fa fa-flash btn btn-yellow"></i>  Seizure: 
																<?php
									break;
								case 6 :
									?>
																<i class="pull-left roundicon fa fa-tint btn btn-danger"></i>  Diabetes- Blood glucose monitoring: 
																									<?php
									break;
								default :
									?>
																								<i
									class="pull-left roundicon fa fa-edit btn btn-primary"></i> 
																								<?php
							}
							?>
							<?php echo '<strong>' . $event->desc . '</strong>';?> <br>
							</div>
							<div class="time">
								<i class="fa fa-clock-o"></i> <?php echo $event->eventDate;?>														
							<button class="btn btn-default btn-lg"
									onclick="loadModal('<?php echo base_url() ."homeportal/eventModal/" . $patID . '/' . $event->id ; ?>')">
									<i class="fa fa-edit"></i> Edit
								</button>
							</div>
						</div>
					
					<?php
						}
					} else {
						echo 'no events recorded';
					}
					?>

				</div>

				</div>
			</div>
		</div>
	</div>
	<!-- /BOX -->


	<div class="footer-tools">
		<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
		</span>
	</div>


	<script
		src="<?php echo base_url();?>js/justgage/js/raphael.2.1.0.min.js"></script>
	<script
		src="<?php echo base_url();?>js/justgage/js/justgage.1.0.1.min.js"></script>
	<!-- EASY PIE CHART -->
	<script
		src="<?php echo base_url();?>js/jquery-easing/jquery.easing.min.js"></script>
	<script type="text/javascript"
		src="<?php echo base_url();?>js/easypiechart/jquery.easypiechart.min.js"></script>
	<!-- SPARKLINES -->
	<script type="text/javascript"
		src="<?php echo base_url();?>js/sparklines/jquery.sparkline.min.js"></script>


	<script>


	
      var g1, g2, g3;
      
      window.onload = function(){
      var g1 = new JustGage({
        id: "g1", 
        value: <?php echo round($prnProtocol / $PRNtotal*100); ?>, 
        min: 0,
        max: 100,
        title: "<?php echo $prnProtocol;?> followed Protocol, <?php echo $PRNtotal - $prnProtocol;?> didn't.",
        label: "%",    
        shadowOpacity: 1,
        shadowSize: 0,
        shadowVerticalOffset: 5,  
        levelColors: [
                      "#FF3333",
                      "#FFFF00",
                      "#47D147"
                    ]           
      });
      
      var g2 = new JustGage({
        id: "g2", 
        value: <?php echo round($PRN2given / $PRNtotal*100); ?>, 
        min: 0,
        max: 100,
        title: "<?php echo $PRN2given;?> PRNs given to continue same behaviour.",
        label: "%",    
        shadowOpacity: 1,
        shadowSize: 0,
        shadowVerticalOffset: 5,  
        levelColors: [
                      "#FF3333",
                      "#FFFF00",
                      "#47D147"
                    ]           
      });
      
      var g3 = new JustGage({
        id: "g3", 
        value: <?php echo round($PRNEffective / $PRNtotal*100); ?>, 
        min: 0,
        max: 100,
        title: "<?php echo $PRNEffective;?> of PRNs were effective",
        label: "%",    
        shadowOpacity: 1,
        shadowSize: 0,
        shadowVerticalOffset: 5,  
        levelColors: [
                      "#FF3333",
                      "#FFFF00",
                      "#47D147"
                    ]           
      });
 
      };
    </script>

