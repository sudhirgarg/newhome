<?php
$user = $this->ion_auth->user ()->row ();
?>


<!-- STYLER -->

<style>
.modal-dialog {
	width: 60%;
}
</style>
<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left">All Events</h3>

</div>
<div class="description">
	Discovering trends, increasing awareness
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DASHBOARD CONTENT -->

<!--  ACTIVITY -->
<div class="row">

	<div class="col-lg-12">
		<div class="box border">

			<div class="box-body">
				<div class="row">
					<div class="col-lg-12">
						<h1>Events Dashboard</h1>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-1"></div>
					<div class="col-lg-10 center">
						<hr>
						<h2>Events for this year: <?php echo date("Y"); ?></h2>

						<canvas id="myChart" style="height: 600px;"></canvas>
					</div>
					<div class="col-lg-1"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-12">
		<div class="col-lg-12">
			<!-- BOX -->
			<div class="box border">
				<div class="box-title">
					<h4>
						<i class="fa fa-reorder"></i> <span class="hidden-inline-mobile">
							Activity</span>
					</h4>
				</div>
				<div class="box-body">

					<div data-always-visible="1" data-rail-visible="1">
					<?php
					$patEvents = $this->portal->getPatEvents ( 0 );
					if ($patEvents != false) {
						foreach ( $patEvents->result () as $event ) {
							?>
					<div class="feed-activity clearfix">
							<div>
						<?php
							
							switch ($event->eventTypeID) {
								
								case 1 :
									?><i class="pull-left roundicon fa fa-male btn btn-success"></i> <a class="btn btn-lg btn-default" href="<?php echo base_url("homeportal/viewpatInNewMethod/" . $event->patID . "/profile");?>">Patient <?php echo $event->patID; ?> </a>  <strong>Bowel Movement | </strong>
																 
																<?php
									break;
								case 2 :
									?>
									<i class="pull-left roundicon fa fa-moon-o btn btn-info"></i> <a class="btn btn-lg btn-default" href="<?php echo base_url("homeportal/viewpatInNewMethod/" . $event->patID . "/profile");?>">Patient <?php echo $event->patID; ?> </a> <strong>Sleep Event  | </strong>
																
																<?php
									break;
								case 3 :
									?>
																	<i
									class="pull-left roundicon fa fa-edit btn btn-primary"></i> <a class="btn btn-lg btn-default" href="<?php echo base_url("homeportal/viewpatInNewMethod/" . $event->patID . "/profile");?>">Patient <?php echo $event->patID; ?> </a> <strong>PRN Administration |  </strong>
								<button class="btn btn-primary btn-sm"
									onclick="loadModal('<?php echo base_url() ."homeportal/prnAdminModal/" . $event->patID . '/' . $event->id ; ?>')">
									<i class="fa fa-eye"></i> Monitor
								</button>
																<?php
									break;
								case 4 :
									?>
									<i class="pull-left roundicon fa fa-ambulance btn btn-danger"></i> <a class="btn btn-lg btn-default" href="<?php echo base_url("homeportal/viewpatInNewMethod/" . $event->patID . "/profile");?>">Patient <?php echo $event->patID; ?> </a>  <strong>Emergency Room Visit  | </strong>
															 
																<?php
									break;
								case 5 :
									?>
																<i
									class="pull-left roundicon fa fa-flash btn btn-yellow"></i> <a class="btn btn-lg btn-default" href="<?php echo base_url("homeportal/viewpatInNewMethod/" . $event->patID . "/profile");?>">Patient <?php echo $event->patID; ?> </a> <strong>Seizure |  </strong>
																<?php
									break;
								case 6 :
									?>
																<i class="pull-left roundicon fa fa-tint btn btn-danger"></i>  <a class="btn btn-lg btn-default" href="<?php echo base_url("homeportal/viewpatInNewMethod/" . $event->patID . "/profile");?>">Patient <?php echo $event->patID; ?> </a> <strong>Diabetes- Blood glucose monitoring |  </strong>
																									<?php
									break;
                                case 7 :
                                    ?>

                                    <i class="pull-left roundicon fa fa-edit btn btn-warning"></i>  <a class="btn btn-lg btn-default" href="<?php echo base_url("homeportal/viewpatInNewMethod/" . $event->patID . "/profile");?>">Patient <?php echo $event->patID; ?> </a> <strong>Urine Discharge | </strong>
                                    <?php
                                    break;
                                case 8 :
                                    ?>

                                    <i class="pull-left roundicon fa fa-edit btn btn-warning"></i>  <a class="btn btn-lg btn-default" href="<?php echo base_url("homeportal/viewpatInNewMethod/" . $event->patID . "/profile");?>">Patient <?php echo $event->patID; ?> </a> <strong>Menstrual Cycle  | </strong>
                                    <?php
                                    break;
                                case 9 :
                                    ?>

                                    <i class="pull-left roundicon fa fa-edit btn btn-warning"></i>  <a class="btn btn-lg btn-default" href="<?php echo base_url("homeportal/viewpatInNewMethod/" . $event->patID . "/profile");?>">Patient <?php echo $event->patID; ?> </a> <strong>Behavior |  </strong>
                                    <?php
                                    break;
								default :
									?>
																								<i
									class="pull-left roundicon fa fa-edit btn btn-primary"></i> 
																								<?php
							}
							?>
							<?php echo $event->desc ;?> <br>
							</div>
							<div class="time">
								<i class="fa fa-clock-o"></i> <?php echo $event->eventDate;?>														
							<button class="btn btn-default btn-lg"
									onclick="loadModal('<?php echo base_url() ."homeportal/eventModal/" . $event->patID . '/' . $event->id ; ?>')">
									<i class="fa fa-edit"></i> Edit
								</button>
							</div>
						</div>
					
					<?php
						}
					} else {
						echo 'no events recorded';
					}
					?>

				</div>

				</div>
			</div>
		</div>
	</div>
	<!-- /BOX -->


	<div class="footer-tools">
		<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
		</span>
	</div>

