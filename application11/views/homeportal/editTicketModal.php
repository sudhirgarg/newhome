<?php
	
	header("Cache-Control: no-cache, must-revalidate");
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	

   $isNew = true;	
	
	
	if ($this -> uri -> segment(4) > 0) {
		$isNew = false;
		$ticketID = $this -> uri -> segment(4);		
		
	} else {
		$ticketID = 0;		
	}
	
	
?>


    
<div class="modal-header">

	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

	<h4 class="modal-title">
		<?php 
			if($isNew==false){
				echo 'Ticket Detail';
			} else {
				echo 'New Ticket'; 
			}
			
			echo '<a href="' . base_url("homeportal/viewpatInNewMethod") . '/' . $patient ->ID . '/reorders' . '"><span class="label label-primary">' . $patient -> LastName .' '.$patient->FirstName. '-' . $patient ->ID . '</span></a>';
				
		?> 
		
	
		
	</h4>
	
	<!-- Ranking  
	 <hr>
    <label>Font Awesome Stars</label>
    <input id="input-2c" class="rating" min="0" max="5" step="0.5" data-size="sm"
           data-symbol="&#xf005;" data-glyphicon="false" data-rating-class="rating-fa"></hr>
     -->      
    
</div>
<div class="modal-body">
	<div class="form-horizontal" role="form">
		
		<input id="ticketID" type="hidden" value="<?php if($isNew == false){ echo $Ticket->ticketID; } else { echo $ticketID; } ?>">
		<input id="patID"    type="hidden" value="<?php if($isNew==false)  { echo $Ticket->patID;    } else { echo $patient ->ID;    } ?>">
		<input id="active"   type="hidden" value="<?php if($isNew==false)  { echo $Ticket->active;   } else { echo '1';       } ?>">
	
	<?php if($isNew==FALSE) { ?>  
		
	   <div class="form-group">
			<label class="col-sm-3 control-label">Date Recorded</label>
			<div class="col-sm-9">
				<input type="text" id="dateCreated" class="form-control" disabled value="<?php echo $Ticket->dateCreated; ?>">
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-3 control-label">Date Complete</label>
			<div class="col-sm-9">
				<input type="text" id="dateComplete" class="form-control" disabled value="<?php echo $Ticket->dateComplete; ?>">
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-3 control-label">Created By</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" id="createdByName" placeholder="Text input" disabled value="<?php $tech = $this->ion_auth->user($Ticket->createdBy)->row(); echo $tech->first_name;?>">
				<input type="hidden" class="form-control" id="createdBy" placeholder="Text input" value="<?php echo $tech->id;?>">
			</div>
		</div>
		
        <input type="hidden" id="ticketModalURI" value="<?php echo uri_string(); ?>" />
        
		<div class="form-group">
			<label class="col-sm-3 control-label">Ticket Type </label>
			<div class="col-sm-9">
				<select class="form-control" id="ticketTypeReferenceID">
					<?php
							$ticketTypeReference = $this->comp->getTicketTypeReference ();
													
							
							foreach ( $ticketTypeReference->result () as $ticketTypeReferenceItem ) {
					
		                        echo '<option value="'.$ticketTypeReferenceItem->ticketTypeReferenceID.'"';						
					
								if ($ticketTypeReferenceItem->ticketTypeReferenceID == $Ticket->ticketTypeReferenceID) {
									echo ' selected="selected"';
								}
								
					            echo '>'.$ticketTypeReferenceItem->ticketTypeReferenceType.'</option>';   
	                       
							}
					?>
				 </select>
				 
				
				 
				 
			</div>
		</div>
		
	
	<?php } else { ?>
		
		<input id="createdBy" type="hidden" value="<?php echo $user->id; ?>"> 
		<input id="dateComplete" type="hidden" value="">
		
		<div class="form-group">
			<label class="col-sm-3 control-label">Date Created</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" id="dateCreated" placeholder="Text input" disabled value="<?php echo date ( 'Y-m-d H:i:s' );?>" >
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label">Created By</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" id="createdByName" placeholder="Text input" disabled value="<?php  echo $user->first_name;?>">
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label">Ticket Type</label>
			<div class="col-sm-9">
				<select class="form-control" id="ticketTypeReferenceID">
					<?php
						$ticketTypeReference = $this->comp->getTicketTypeReference ();
						foreach ( $ticketTypeReference->result () as $ticketTypeReferenceItem ) {
					
                       	 echo '<option value="'.$ticketTypeReferenceItem->ticketTypeReferenceID.'">'.$ticketTypeReferenceItem->ticketTypeReferenceType.'</option>';   
                       
						}
					?>
				</select>
			</div>
		</div>
		
	<?php 	} ?>	
	

		<div class="form-group">
			<label class="col-sm-3 control-label" id="desclabel">Description</label>
			<div class="col-sm-9">
				<textarea class="form-control" rows="4" id="Description"><?php if($isNew==false){ echo $Ticket->Description;}?></textarea>
			</div>
		</div>
		
		<?php if($isNew==false){  ?>

		<!-- CHAT -->
		<div class="row">
			<div class="col-md-12">
				<!-- BOX -->
				<div class="box border green chat-window">
					<div class="box-title">
						<h4>
							<i class="fa fa-comments"></i>Chat Window
						</h4>
					</div>
					<div class="box-body big">
						<div>
							<div class="chat-form">
								<div class="input-group">
									<input type="text" class="form-control" id="message" value="">
									<span class="input-group-btn">
										<button class="btn btn-primary" type="button" onclick="addComment()" id="addCommentButton" data-loading-text="Send">
											<i class="fa fa-check"></i>
										</button>
									</span>
								</div>
							</div>
							
							<div class="divide-20"></div>
							
							<ul class="media-list chat-list">
								
							<?php
							
							   
								
								$comments = $this->comp->getDiscussion ($ticketID);
															
								
								if ($comments != FALSE) {
									foreach ( $comments->result () as $comment ) {
										if ($comment->userID == $user->id) {
							
											echo'<li class="media">
													<div class="media-body chat-pop mod">
														<h4 class="media-heading pull-right"> You 
															<span class="pull-left">
																<abbr class="timeago" title="'.$comment->messageDate.'>">'.$comment->messageDate.'</abbr>
																<i class="fa fa-clock-o"></i>
															</span>
														</h4>
			                                             <br/>
														<h4>'.$comment->message.'</h4>
													</div>
												</li>';
								
										}else {
											echo'<li class="media">
													<div class="media-body chat-pop">
														<h4 class="media-heading">';															
																			
														$messenger = $this->ion_auth->user ( $comment->userID )->row ();
														echo '<span class="label label-info">'.$messenger->first_name.'</span>
														       <span class="pull-right"><abbr class="timeago"
																title="'.$comment->messageDate.'">'.$comment->messageDate.'</abbr>
																<i class="fa fa-clock-o"></i></span>
														</h4>
														<h4>'.$comment->message.'</h4>
													</div>
												</li>';
									   } 
								   }
							  }
									
						   ?>
												
						  </ul>
						</div>
						
					</div>
				</div>
				<!-- /BOX -->
			</div>
		</div>
		<!-- /CHAT -->

    <?php 	
    
		}

     ?>

	</div>
	
	<div class="modal-footer">		
		<button type="button" class="btn btn-default" data-dismiss="modal" id="closeTicketButtonModal">Close</button>
		<?php 
			if($isNew != false){  
		      echo '<button type="button" class="btn btn-primary" id="saveTicketButton" onclick="saveTicket()"  data-loading-text="Saving...">Save</button>';
			} 
		?>
	</div>
    
    
    <!--script>
    
	    $("#ticketTypeReferenceID").change(function(){
	      	if($(this).val()=="5") {
	    	    // checkbox is checked -> do something
	       	 $("#desclabel").text("Days supply requsted:");	       	 
	       }else{
	    	   $("#desclabel").text("Description:");	    
	       }
	    });
    
        $("#message").keypress(function(e) {
            if(e.which == 13) {
                addComment();
            }
        });
        
        $(function() {
            $("#message").focus();    
        });        
    
    </script-->
    
    
    
   