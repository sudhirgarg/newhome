<?php
												
	$homeInfo = "";
	$patId = "";
	$initial = "";
	$nhWardName ="";
	$patAddress = "";
	$home = "";
	$NHWardId ="";
	
	foreach($patInfo AS $record){
	    $home = $record->Name;
		$homeInfo = $record->Name .'<br/>'.$record->homeAdd1.', '.$record->homeCity.'<br/>'.$record->homePro.', '.$record->homePostal;
		$patId = $record->ID; 
		$initial = $record->LastName.' '.$record->FirstName;
		$nhWardName = $record->wardName;
		$patAddress = $record->LastName.' '.$record->FirstName.'<br/>'.$record->Address1.', '.$record->Address2.'<br/>'.$record->PatCity.', '.$record->PatPro.'<br/>'.$record->PatPostal;
		$NHWardId = $record->NHWardID;
	
		
	}
	
	 $ci =& get_instance();

?>

<div class="clearfix">
	<h3 class="content-title pull-left"><?php echo $this -> lang -> line('CLIENT_PROFILE'); ?></h3>
</div>

<div class="description">
	<a class="btn btn-lg btn-default"
		href="<?php echo base_url()?>homeportal"><i
		class="fa fa-chevron-left"></i> <?php echo $this -> lang -> line('CLIENTS'); ?></a>

	<button class="btn btn-lg btn-primary"
		onclick="loadModal('<?php echo base_url() . 'homeportal/editTicketModal/' . $patId; ?>/-1')">
		<i class="fa fa-plus"></i> <?php echo $this -> lang -> line('NEW_TICKETS'); ?>
	</button>

	<a class="btn btn-lg btn-primary"
		href="<?php echo base_url() . 'homeportal/clientEvents/' . $patId; ?>/-1">
		<i class="fa fa-eye"></i> <?php echo $this -> lang -> line('HEALTH_PROFILE'); ?>
	</a> 
	
		
	<a class="btn btn-lg btn-primary"
		<!--href="<?php echo base_url() . 'homeportal/healthindicatorsdash/' . $patId; ?>/-1" --> 
		<i class="fa fa-bar-chart-o fa-fw"></i> Health Indicators
	</a> 

	<a class="btn btn-lg btn-primary"
		href="<?php echo base_url() . 'homeportal/disposal/' . $patId; ?>/">
		<i class="fa fa-refresh fa-spin fa-fw"></i> Disposal 
	</a>
	
	<a class="btn btn-lg btn-warning pull-right"
		href="<?php echo base_url() . 'homeportal/printPatProfile/' . $patId; ?>/-1">
		<i class="fa fa-print"></i> <?php echo $this -> lang -> line('PRINT_PATIENT_PROFILE'); ?>
	</a>
	
	<a class="btn btn-lg btn-success" href=""> <i class="fa fa-table"></i> eMARS is Comming soon </a>
	<!--?php echo base_url() . 'homeportal/emars/' . patID; ?-->
	<input id="patID" value="<?php echo $patId; ?>" type="hidden">

</div>

</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!--  MAIN PAGE HERE -->

<div class="row">
	<div class="col-lg-12">

		<!-- USER PROFILE -->
		<div class="row">
			<div class="col-md-12">
				<!-- BOX -->
				<div class="box border">
					<div class="box-title">
						<h4>
							<i class="fa fa-user"></i><span class="hidden-inline-mobile"></span>
						</h4>
					</div>
					
					<div class="box-body">
						<div class="tabbable header-tabs user-profile">
							<ul class="nav nav-tabs">
								
										
								<li <?php if ($this -> uri -> segment(4) == "medchanges") {echo 'class="active"'; } ?>>
									<a href="#medchanges" data-toggle="tab"><i class="fa fa-user-md"></i> <span class="hidden-inline-mobile"> <?php echo $this -> lang -> line('MED_CHANGES'); ?></span></a>
								</li>
								
								<li <?php if ($this -> uri -> segment(4) == "tickets") {echo 'class="active"'; } ?>>
									<a href="#tickets" data-toggle="tab"><i class="fa fa-ticket"></i> <span class="hidden-inline-mobile"> <?php echo $this -> lang -> line('SERVICE_TICKETS'); ?></span></a>
								</li>

								<li <?php if ($this -> uri -> segment(4) == "reorders") {echo 'class="active"'; } ?>>
									<a href="#reorders" data-toggle="tab"><i class="fa fa-bar-chart-o"></i> <span class="hidden-inline-mobile"> <?php echo $this -> lang -> line('RE_ORDERS'); ?></span></a>
								</li>

								<li <?php if ($this -> uri -> segment(4) == "profile" || $this -> uri -> segment(3) == "") {echo 'class="active"'; } ?>>
									<a href="#pro_overview" data-toggle="tab"><i class="fa fa-dot-circle-o"></i> <span class="hidden-inline-mobile"><?php echo $this -> lang -> line('PROFILE'); ?></span></a>
								</li>
								
							</ul>
							
							<div class="tab-content">
								<!-- OVERVIEW -->
								<div class="tab-pane fade <?php if ($this -> uri -> segment(4) == "profile") {echo 'in active'; } ?>" id="pro_overview">
									<div class="row">
										<!-- PROFILE PIC -->
										<div class="col-md-3">
											<div class="list-group">
												<li class="list-group-item zero-padding">
													<!--  <img alt=""
													class="img-responsive"
													src="<?php //  echo base_url();?>img/profile/avatar.jpg">-->
												</li>
												
												
												
												
												<div class="list-group-item profile-details">
													<h2><?php  echo $initial; ?> </h2>

												</div>
												
												
												
												<div class="list-group-item profile-details">
													<h2><i class="fa fa-home"></i> <?php echo $this -> lang -> line('HOME'); ?></h2>
													<?php  echo $homeInfo; ?> 
												
												</div>
												

												<div class="list-group-item profile-details">
													<h2><?php echo $this -> lang -> line('ALLERGIES'); ?></h2>
													
													
													<div id="alergiesTable">
														 <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover datatable table-responsive">
														 <thead >
																<tr>												
																	<th>Allergy</th>
																	<th>Comment</th>
																	
																</tr>
															</thead>
															<tbody>
														<?php 
															foreach($allergy AS $aRecord){
																echo '<tr>
																	<td>'; $ci->getAllergyInfo($aRecord->Code);  echo'</td>
																	<td>'.$aRecord->Comment.'</td>																	
																</tr>';
																
															}
														
														?>
															</tbody>
														</table>
													</div>

												</div>
												
												<div class="list-group-item profile-details">
													<h2><?php echo $this -> lang -> line('CONDITIONS'); ?></h2>
													
													
													<div id="conditionsTable">
														<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover datatable table-responsive">
														 <thead >
																<tr>												
																	<th>Conditions</th>
																	
																	
																</tr>
															</thead>
															<tbody>
														<?php 
															foreach($condition AS $aRecord){
																echo '<tr>
																	<td>'; $ci->getConditionsInfo($aRecord->Code);  echo'</td>
																																	
																</tr>';
																
															}
														
														?>
															</tbody>
														</table>
														
													</div>
													
												</div>
												<div class="list-group-item profile-details">
													<h2><?php echo $this -> lang -> line('NOTE'); ?></h2>
													<p>  </p>
												</div>
												

												<div class="list-group-item profile-details">
													<h2>
														
																								
												</div>

												<div class="list-group-item profile-details">
													<h2><?php echo $this -> lang -> line('DELIVERY'); ?> TO</h2>
													<?php
													      if($home === "Local Individuals"){
													      	  echo $patAddress;
													      }else{
													      	  //echo $nhWardName; echo $NHWardId;
													      	  if($NHWardId > 0){
													      	  	 $ci->getNHWardInfo($NHWardId);
													      	  }													  
													      	  
													      }
													      
													   ?>
													
												</div>


											</div>
										</div>
										<!-- /PROFILE PIC -->
										<!-- PROFILE DETAILS -->
										<div class="col-md-9">
											<!-- ROW 1 -->
											<div class="row">
												<div class="col-md-12">
													<div class="box border blue">
														<div class="box-title">
															<h4>
																<i class="fa fa-columns"></i> <span
																	class="hidden-inline-mobile">Compliance Package</span>
															</h4>
														</div>
														<div class="box-body">
															
															<table id="" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover datatable table-responsive">
																
																 <thead >
																		<tr>												
																			<th>Medication</th>
																			<th>Doctor</th>
																			<th>Directions</th>
																		</tr>
																	</thead>
																	<tbody>
																		<?php																	
																	
																			foreach ($patRx as $drg) {
																				if($drg->NHBatchFill == 1){
																					echo '<tr>';
																					echo '<td>' . $drg -> BrandName . ' ' . $drg -> Strength . '<br>RX-' . $drg -> RxNum . '</td>';
																					echo '<td>' . $drg -> doc.'</td>';
																					echo '<td>' . $drg -> SIG . '</td>';
																					echo '</tr>';
																				}
																				
																			} 
																										
																		?>
																	</tbody>
															</table>
														</div>
													</div>
												</div>
											</div>
											<!-- /ROW 1 -->
										</div>

										<!-- /PROFILE DETAILS -->
									</div>
								</div>
								<!-- /OVERVIEW -->


								 <!-- REORDERS -->
                                <div id="reorders" class="tab-pane fade <?php if ($this -> uri -> segment(4) == "reorders") {echo 'in active'; } ?>" >
                                	
									 <table id="" cellpadding="0" cellspacing="0" border="0"  class="table table-striped table-bordered table-hover datatable table-responsive"> 
                                           
                                        <thead >
												<tr>												
													<th>Medication</th>
													<th>Doctor</th>
													<th>Directions</th>
													<th>Comment</th>
													<th>Select</th>
												</tr>
											</thead>
											<tbody>
											<?php
											
												$checkBoxCount = 1;
												foreach ($patRx as $drg) {
													if($drg->NHBatchFill == 0){
														echo '<tr>';
														echo '<td>' . $drg -> BrandName . ' ' . $drg -> Strength . '<br>RX-' . $drg -> RxNum .'</td>';
														echo '<td>' . $drg -> doc.'</td>';
														echo '<td>' . $drg -> SIG.'</td>';
														echo '<td><input type="text" name="reOrderComment" class="form-control reOrderCommentClass" id="comment'.$checkBoxCount.'" ></td>';
													    echo '<td id="reOrderList" ><input type="checkbox" name="reOrderCheckBox" class="form-control reOrderCheckBoxClass" value="'. $drg -> RxNum .'" id="checkBox' . $checkBoxCount++. '"></td>';
														echo '</tr>';
													}
													
												} 													
											
																				
											?>
											  <tr>											     
											      <td colspan="3"> </td>
											      <td colspan="2"> 
											      		<input type="hidden" id="patID" value="<?php echo $patId; ?>"> 
					                                    <input type="hidden" id="reOrderBox">
					                                    <input type="hidden" id="reOrderRx">
					                                    <button class="btn btn-block btn-success" onclick="reOrderPRNs()">  <h3>Create Order</h3> </button>
											      </td>
											  </tr>
											  
										</tbody>   
										                                  
                                    </table>
					                                    
                                   
                                    
                                </div>
                                <!-- /REORDERS -->


								<!-- medchanges -->
								<div
									class="tab-pane fade <?php
									if ($this -> uri -> segment(4) == "medchanges") {echo 'in active';
									}
								?>"
									id="medchanges">

									<table cellpadding="0" cellspacing="0" border="0" id="medChanges"
										class="table table-striped table-bordered table-hover datatable table-responsive">
										<thead >
											<tr>
												<th>ID</th>
												<th><?php echo $this -> lang -> line('CREATED'); ?></th>
												<th><?php echo $this -> lang -> line('TECHNICIAN'); ?></th>
												<th><?php echo $this -> lang -> line('PHARMACIST'); ?></th>
												<th><?php echo $this -> lang -> line('STARTS_ENDS'); ?></th>
												<th><?php echo $this -> lang -> line('CHANGE'); ?></th>
												<th><?php echo $this -> lang -> line('COMPLETED'); ?></th>
											</tr>
										</thead>
										<tbody>
										<?php
										$medChange = $this->comp->getMedChange ( '-1', $patId );
										if ($medChange != FALSE) {
											foreach ( $medChange->result () as $medChangeItem ) {
												?>
											<tr
												onclick="loadModal('<?php echo base_url('homeportal/editMedChangeModal') . '/' . $patId . '/' . $medChangeItem -> medChangeID; ?>')">
												<td><?php echo $medChangeItem -> medChangeID; ?></td>
												<td><?php echo $medChangeItem -> dateOfChange; ?></td>
												<td><?php $tech = $this -> ion_auth -> user($medChangeItem -> technicianUserID) -> row();
													echo $tech -> first_name;
												?></td>
												<td><?php
												if ($medChangeItem -> pharmacistUserID > 0) {$pharm = $this -> ion_auth -> user($medChangeItem -> pharmacistUserID) -> row();
													echo $pharm -> first_name;
												}
												?></td>


												<td><?php echo $medChangeItem -> startDate . ' to ' . $medChangeItem -> endDate; ?></td>
												<td><?php echo $medChangeItem -> changeText; ?></td>
												<td><?php
												
												if ($medChangeItem->actionComplete > 0) {
													?>
													<span class="badge badge-green"><i class="fa fa-check"></i></span>
												<?php
													;
												}
												?></td>
											</tr>
											<?php
											}
											} else {
											echo '<tr><td colspan="7"><div class="center">No MED CHANGES</div></td></tr>';
											}
										?>
										</tbody>
									</table>
								</div>
								<!-- /medchanges -->

								<!-- tickets -->
								<div
									class="tab-pane fade <?php
									if ($this -> uri -> segment(4) == "tickets") {echo 'in active';
									}
								?>"
									id="tickets">
									<table id="dtTickets" cellpadding="0" cellspacing="0"
										border="0"
										class="table table-striped table-bordered table-hover datatable table-responsive">
										<thead>
											<tr>
												<th><?php echo $this -> lang -> line('CREATED'); ?></th>
												<th><?php echo $this -> lang -> line('COMPLETED_DATE'); ?></th>
												<th><?php echo $this -> lang -> line('CREATED_BY'); ?></th>
												<th><?php echo $this -> lang -> line('TICKET_TYPE'); ?></th>
												<th>Ranks</th>
												<th><?php echo $this -> lang -> line('DESCRIPTION'); ?></th>
												<th><?php echo $this -> lang -> line('COMPLETED'); ?></th>
											</tr>
										</thead>
										<tbody>
												<?php
												$generalTicket = $this->comp->getTickets ( '-1', '1', $patId );
												if ($generalTicket != FALSE) {
													foreach ( $generalTicket->result () as $generalTicketItem ) {
														?>
				                         <tr	onclick="loadModal('<?php echo base_url('homeportal/editTicketModal/') . '/' . $generalTicketItem -> patID . '/' . $generalTicketItem -> ticketID; ?>')">
												<td><?php echo $generalTicketItem -> dateCreated; ?></td>
												<td><?php echo $generalTicketItem -> dateComplete; ?></td>
												<td><?php $ticketCreator = $this -> ion_auth -> user($generalTicketItem -> createdBy) -> row();
													echo $ticketCreator -> first_name;
												?></td>
												<td><?php $refType = $this -> comp -> getTicketTypeReference($generalTicketItem -> ticketTypeReferenceID);
													echo $refType;
												?></td>
												<td><?php if(empty($generalTicketItem -> rank)) { echo "0.0"; } else { echo $generalTicketItem -> rank; }   ?> </td>
												<td><?php echo $generalTicketItem -> Description; ?></td>
												<td><?php
														
												if ($generalTicketItem->dateComplete != '') { ?>
													<span class="badge badge-green"><i class="fa fa-check"></i></span>
												 <?php	} ?></td>
											</tr>
							<?php  }}else{echo '<tr><td colspan="6"><div class="center">NO TICKETS</div></td></tr>';} ?>
											</tbody>
									</table>
								</div>
								<!-- /tickets -->

								</div>

							</div>
						</div>
						<!-- /USER PROFILE -->
					</div>
				</div>

				<!-- / MAIN PAGE HERE -->
			</div>
			<div class="footer-tools">
				<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
				</span>
			</div>