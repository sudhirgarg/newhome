<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left"> Disposal </h3>
</div>
<div class="description">

</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DELIVERY RUNS CONTENT -->
<!-- DATA TABLES -->
<div class="row">
	<div class="col-md-12">

		<div class="box border">
			<div class="box-title">
				<h4>
					<i class="fa fa-ticket"></i> <span class="hidden-inline-mobile">Disposaled List - <?php echo $patInfo->LastName.' '.$patInfo->FirstName; ?></span>
					
				</h4>
			</div>
			<div class="box-body">

				<div class="row">

					<div class="col-md-4 col-xl-4">
					   
						
                         
						<div class="table-responsive">
						<table id="1" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover">
							<thead>
								<tr >
									<th colspan="4">
									    <h3 class="center">
											<span class="label label-info">List</span>
										</h3>
									</th>
								</tr>
								<tr>
								    <th width="50">#</th>
							        <th>Date</th>
							        <th width="50">View</th>
							        <th width="50">Print</th>
							      </tr>
							      
							</thead>
							<tbody>
							    <?php 
							          $i = 1;
								      foreach ($info AS $record ){
									       echo '<tr  >
					                           <td>'.$i++.'</td>							        
										        <td>'.$record -> createdDate.' <input type="hidden" class="disposalId" value="'.$record -> id.'" /> </td>
		
										        <td class="viewDisposal" > <button class="btn btn-default" ><i class="fa fa-eye" aria-hidden="true"></i></button>   </td>
										        <td class="printDisposal" > <button class="btn btn-default" ><i class="fa fa-print" aria-hidden="true"></i></button> </td>
					                            
					                        </tr>';
								      }
			                      ?>
							     
																
							</tbody>
						</table>
						</div>
						<?php  echo $link; ?>
					</div>
					<div class="col-md-8 col-xl-8">
					   					   
						<table id="1" cellpadding="0" cellspacing="0" border="0"
							class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th colspan="5"><h3 class="center">
											<span class="label label-info">Create new sheet </span>
										
										</h3></th>
								</tr>
								
								<tr>
								    <th align="center">#</th>
							        <th>Medication</th>
							        <th width="90px"># of Pills</th>
							        <th>Reason</th>
							        <th width="50">Select</th>
							        							        
							    </tr>
							      
							</thead>
							
							
							<tbody>
							
							   <?php
							       											
									$checkBoxCount = 1;
									foreach ($medInfo as $drg) {
										
											echo '<tr>';
											echo '<td align="center">' .$checkBoxCount.'</td>';
											echo '<td>'.$drg -> BrandName.' '.$drg -> Strength.'</td>';											
											echo '<td> <input type="number" class="form-control disposalNumPills" name="numberOfPills" id="numberOfPills" min="1" value="1" /> </td>';
											echo '<td><input type="text" name="disposalComment" class="form-control disposalComment" id="disposalComment" ></td>';
										    echo '<td id="dispoalCheckBox" ><input type="checkbox" name="dispoalCheckBox" class="form-control reOrderCheckBoxClass" value="'. $drg -> RxNum .'" id="checkBox' . $checkBoxCount++. '"></td>';
											echo '</tr>';										
										
									} 													
								
																	
								?>
								  <tr>
							        <td colspan="3" align="right">Print your name: </td>
							        <td>
                                          <input type="text" id="printBy" class="form-control"  />
                                          <input type="hidden" id="info" />
                                          <input type="hidden" id="patId" value="<?php echo $patInfo->ID; ?>" />
                                          
                                    </td>
                                    
                                    <td></td>
							      </tr>
							   
							      <tr>
							        <td colspan="3"></td>
							        <td colspan="2">
                                            <button class="btn btn-block btn-success" onclick="createDisposalSheet()" >Create disposal sheet</button>
                                    </td>
							      </tr>
							</tbody>
						</table>
					</div>
					
					
				</div>
			</div>
		</div>


	</div>
</div>
<!-- /PAGE -->


<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>

