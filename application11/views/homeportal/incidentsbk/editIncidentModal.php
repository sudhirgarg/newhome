<?php
$isNew = true;
if ($this->uri->segment ( 3 ) > 0) {
	$isNew = false;
	$incident = $this->incidents->getIncidents ( $this->uri->segment ( 3 ) );
}
?>
<script>
			$(".datepicker-fullscreen").pickadate({
			    showOtherMonths: true,
			    autoSize: true,
			    appendText: '<span class="help-block">(yyyy-mm-dd)</span>',
			    dateFormat: "yy-mm-dd"
			});
			</script>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"
		aria-hidden="true">&times;</button>
	<h4 class="modal-title">FEATURE COMING SOON</h4>
</div>
<div class="modal-body">
	<div class="form-horizontal" role="form">
		
		
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal"
			id="closeButtonModal" disabled>Close</button>
		<button type="button" class="btn btn-primary" id="saveIncidentButton"
			onclick="saveIncident()" disabled data-loading-text="Saving...">Save changes</button>
	</div></div>