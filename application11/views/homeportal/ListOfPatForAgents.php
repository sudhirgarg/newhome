<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left"> Manage Your Care Givers</h3>
</div>
<div class="description">

</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DELIVERY RUNS CONTENT -->
<!-- DATA TABLES -->
<div class="row" ng-app="myApp" ng-controller="DoubleController">
	<div class="col-md-12">

		<div class="box border">
			<div class="box-title">
				<h4>
					<i class="fa fa-ticket"></i> <span class="hidden-inline-mobile">List Of Care Givers</span>
				</h4>
			</div>
			<div class="box-body">

				<div class="row">

					<div class="col-lg-6">
						 <div class="panel panel-info">
						  <div class="panel-heading"> <h4> Care Givers Information  <button class="btn btn-success" ng-click="addNewCG();"> <i class="fa fa-user"></i> ADD Care Givers </button> </h4> </div>
						  <div class="panel-body">
						 <table class="table table-hover">
						    <thead>
						        <tr>
							        <th>#</th>						       
							        <th>Lastname</th>
							        <th>Firstname</th>
							        <th>Reset PWD</th>
							        <th>Assign Client</th>
							        <th>Edu History</th>
							        <th>Active</th>	   						        
						        </tr>
						    </thead>
						    <tbody>
							<?php
							
						 		$i = 1;
						 		foreach($CG AS $record){
						 			echo '<tr>
								        <td>'.$i++.'</td>								        
										<td>'.$record->last_name.'</td>
								        <td>'.$record->first_name.'</td>
								        <!--td><span data-toggle="collapse" data-target="#demo'.$i.'"> <button class="btn btn-warning" > <i class="fa fa-unlock-alt"></i></button > </span> </td-->';
								        if($record->active == 1) {
										
									        echo '<td> <button class="btn btn-warning" ng-click="reSetPwd('.$record->id.')" > <i class="fa fa-unlock-alt"></i></button > </td>
									        <td ><a href="'.base_url().'homeportal/assignClient/'.$record->id.'"><button class="btn btn-info" > <i class="fa fa-link"></i></button ></a> </td>
						 		            <td ><a href="'.base_url().'homeportal/getUserExamResult/'.$record->id.'"><button class="btn btn-info" > <i class="fa fa-eye"></i></button ></a> </td>';
										}else{
											echo '<td colspan="3"> Inactive User </td>';
										}
																			        
								        echo'<td> <button 
								        		'; if($record->active == 1) { echo 'class="btn btn-success"'; }else { echo 'class="btn btn-danger"'; }   echo '
								        		 ng-click="activeUser('.$record->id.')" > <input type="checkbox"'; if($record->active == 1) { echo " checked"; }   echo '>  </td>
						        		
									        
								      </tr>
								      <!--form class="agencyResetCGpwd">
									   <tr id="demo'.$i.'" class="collapse" style="background-color:#bbff99;" >
                                            <td colspan="5">
                                            	<input type="hidden" value="'.$record->id.'" name="userId" id="userId"> 
                                            	<input type="password" placeholder="Enter new passwrd" name="newPassword" id="newPassword" required >
                                            	<input type="password" placeholder="Enter new passwrd again" name="newPasswordAgain" id="newPasswordAgain" required >
                                            	<button type="submit" class="btn">Submit</button>
                                            	<span class="label label-danger" id="alertCGpedRest"> </span>
											</td>                                            
								       </tr>
								       </form -->
								      ';
						 		}	
						 		
								
													 		
						 	?>
						 	</tbody>
  						</table>
  						</div>
						</div>
						
					</div>
					
					<div class="col-lg-6">
						
						<div class="panel panel-info"  ng-hide="addNew">							
						  <div class="panel-heading"><h4></h4>Add Care Givers</h4></div>
						  		<div class="panel-body">
						  			
						 				<div class="modal-body">

											<div class="form-horizontal" role="form">
												
												
												<input id="groups" type="hidden" value="18">
										
										
												<div class="form-group">
													<label class="col-sm-3 control-label">First Name</label>
													<div class="col-sm-9">
														<input type="text" class="form-control" id="first_name" placeholder="Text input" value="">
													</div>
												</div>
										
												<div class="form-group">
													<label class="col-sm-3 control-label">Last Name</label>
													<div class="col-sm-9">
														<input type="text" class="form-control" id="last_name" placeholder="Text input" value="">
													</div>
												</div>
										
												<div class="form-group">
													<label class="col-sm-3 control-label">Organization</label>
													<div class="col-sm-9">
														<input type="text" class="form-control" id="company" placeholder="Text input" value="">
													</div>
												</div>
										
												<div class="form-group">
													<label class="col-sm-3 control-label">Log In Email</label>
													<div class="col-sm-9">
														<input type="text" class="form-control" ng-model="getEmail" id="email" placeholder="Text input" value="">
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-sm-3 control-label">username(Enter Email Again)</label>
													<div class="col-sm-9">
														<input type="text" class="form-control" id="username" placeholder="Text input" value="" ng-value="getEmail" >
														
													</div>
												</div>
										
												<div class="form-group">
													<label class="col-sm-3 control-label">Password</label>
													<div class="col-sm-9">
														<input type="password" class="form-control" id="password" placeholder="Text input" value="">
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-sm-3 control-label">Phone</label>
													<div class="col-sm-9">
														<input type="text" class="form-control" id="phone" placeholder="Text input" value="">
													</div>
												</div>
											</div>
											
										</div>
										
										<div class="modal-footer">
																																	
											<button type="button" class="btn btn-primary" id="saveButton" onclick="addHomePortalUser()" data-loading-text="Saving...">Save</button>
											
										</div>
						 
  								</div>
						  </div>
						
						<div class="panel panel-info"  ng-hide="changePwd">
						  	<div class="panel-heading"><h4> Change Passward </h4></div>
							  	<div class="panel-body">
							 
							 		<div class="panel-body">
						  			
						 				<div class="modal-body">

											<div class="form-horizontal" role="form">
												
												
												<input id="groups" type="hidden" value="18">
										
										
												<div class="form-group bg-danger">
													 User Name : {{ LastName }}  {{ FirstName }} 
												</div>
																						
										
										
												<div class="form-group">
													<label class="col-sm-3 control-label">Password</label>
													<div class="col-sm-9">
														<input type="password" class="form-control" id="passwordSet" placeholder="Text input" value="" required>
														<input type="hidden" value="{{id}}" id="userId" >
													</div>
												</div>
										
												<div class="form-group">
													<label class="col-sm-3 control-label">Confirm Password</label>
													<div class="col-sm-9">
														<input type="password" class="form-control" id="passwordConfirm" placeholder="Text input" value="" required>
													</div>
												</div>
												
												
											</div>
											
										</div>
										
										<div class="modal-footer">
																																	
											<button type="button" class="btn btn-primary" id="saveButton" onclick="agencyResetCGpwdOnClick()" data-loading-text="Saving...">Save</button>
											
										</div>
						 
  									</div>
							 
	  							</div>
						</div>
						
						<div class="panel panel-info"  ng-hide="assignClient">
							<div class="panel-heading"><h4> Assign Client Information </h4> </div>
							<div class="panel-body">
						
  							</div>
						</div>
						
					</div>
					
				</div>	
				
				
				
			</div>
		</div>


	</div>
</div>
<!-- /PAGE -->


<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>

