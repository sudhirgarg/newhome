<?php
$isNew = true;
$patID = $this->uri->segment ( 3 );
$patient = $this->comp->getCompliancePatient($patID);
$medChangeID = $this->uri->segment ( 4 );
$medChange;
if ($this->uri->segment ( 4 ) > 0) {
	$isNew = false;
	$medChange = $this->comp->getMedChange ( $this->uri->segment ( 4 ) );
}
?>
<div class="modal-header">

	<button type="button" class="close" data-dismiss="modal"
		aria-hidden="true">&times;</button>

	<h4 class="modal-title"><?php if($isNew==false){echo 'Med Change Detail';}else{echo 'New Med Change' . 'ChangeID:'.$medChangeID . 'pat:'.$patID;}?> <?php  echo '<a href="' . base_url("compliance/viewpat") . '/' . $patID . '/profile' . '"><span class="label label-primary">'.$patient->patInitials . '-' . $patient->patID .'</span></a>' ;?></h4>

	<button id="printbutton" class="btn btn-info" onclick="location.href='<?php echo base_url() . "homeportal/printMedChange/" . $patID . "/" .$medChangeID;?>'"><i class="fa fa-print"></i> Print</button>
</div>
<div class="modal-body">
	<div class="form-horizontal" role="form">
		<input id="medChangeID" type="hidden"
			value="<?php if($isNew==false){echo $medChange->medChangeID;}else{ echo $medChangeID;}?>">
		<input id="patID" type="hidden"
			value="<?php if($isNew==false){echo $medChange->patID;}else{ echo $patID;}?>">
		<input id="active" type="hidden"
			value="<?php if($isNew==false){echo $medChange->active;}else{ echo '1';}?>">
			<?php if($isNew==false){ ?>
		
		
		<?php if($isNew==FALSE){ ?>
		<div class="form-group">
			<label class="col-sm-3 control-label">Date Recorded</label>
			<div class="col-sm-9">
				<input type="text" id="dateOfChange" class="form-control" disabled
					value="<?php echo $medChange->dateOfChange; }?>">
			</div>
		</div>


		<div class="form-group">
			<label class="col-sm-3 control-label">Created By</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" id="technicianUserName"
					placeholder="Text input" disabled
					value="<?php $tech = $this->ion_auth->user($medChange->technicianUserID)->row(); echo $tech->first_name;?>">
				<input type="hidden" class="form-control" id="technicianUserID"
					placeholder="Text input" value="<?php echo $tech->id;?>">
			</div>
		</div>
		
		<?php if($medChange->pharmacistUserID > 0){?>
		<div class="form-group">
			<label class="col-sm-3 control-label">Pharmacist Checked By</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" id="pharmacistUserName"
					placeholder="Text input" disabled
					value="<?php $pharm = $this->ion_auth->user($medChange->pharmacistUserID)->row(); echo $pharm->first_name;?>">
				<input type="hidden" class="form-control" id="pharmacistUserID"
					placeholder="Text input" value="<?php echo $pharm->id ; ?>">
			</div>
		</div>
		<?php }else{?>
			<div class="form-group">
			<label class="col-sm-3 control-label">Pharmacist Checked By</label>
			<div class="col-sm-9">
				<span class="label label-danger">Not Verified</span> <input
					type="hidden" class="form-control" id="pharmacistUserID"
					placeholder="Text input" value="0">
			</div>
		</div><?php
				}
			} else {
				?>
			<input id="technicianUserID" type="hidden"
			value="<?php
				$user = $this->ion_auth->user ()->row ();
				echo $user->id;
				?>
			"> <input id="pharmacistUserID" type="hidden" value="0"> <input
			id="dateOfChange" type="hidden" value="<?php echo date('Y-m-d');?>"> 
			<?php
			}
			?>
		
		<div class="form-group">
			<label class="col-sm-3 control-label">Change</label>
			<div class="col-sm-9">
				<textarea class="form-control" rows="3" id="changeText"><?php if($isNew==false){ echo $medChange->changeText;}?></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label">Action</label>
			<div class="col-sm-9">
				<textarea class="form-control" id="actionText" rows="3"><?php if($isNew==false){ echo $medChange->actionText;}?></textarea>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label">Starts on</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" id="startDate"
					placeholder="Text input"
					value="<?php if($isNew==false){ echo $medChange->startDate;}?>">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label">Ends on</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" id="endDate"
					placeholder="Text input"
					value="<?php if($isNew==false){ echo $medChange->endDate;}?>">
			</div>
		</div>

	</div>
	<div class="modal-footer">


		
		<button type="button" class="btn btn-default" data-dismiss="modal"
			id="closeButtonModal">Close</button>
	</div>