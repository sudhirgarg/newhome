			<script>

			$(".datepicker-fullscreen").pickadate();
			</script>
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Add New Point</h4>
			</div>
			<div class="modal-body">
				<div class="form-horizontal">
					<div class="form-group">
						<input type="hidden" id="KPIDetailID" value="-1">
					
						<label class="col-sm-3 control-label">Date of Data point</label>
						<div class="col-sm-9">
							<input type="text" class="form-control datepicker-fullscreen" id="recordedDate">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Point Value</label>
						<div class="col-sm-9">
							<input type="text" name="regular" class="form-control" id="pointValue">
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" id="closeModalButton" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="saveDataPointButton" onclick="saveDataPoint()">Save</button>
			</div>
		