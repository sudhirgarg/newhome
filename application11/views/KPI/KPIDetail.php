<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left">Report Detail</h3>
</div>
<div class="description">

	<a class="btn btn-default btn-lg "
		href="<?php echo base_url();?>filingCab/KPIDash">Back</a> <a
		class="btn btn-info btn-lg" data-toggle="modal"
		data-target=".bs-example-modal-lg" data-toggle="modal"
		data-target="#myModal"> <i class="fa fa-pencil"></i> Indicator Details
	</a> <a class="btn btn-primary btn-lg" onclick="newKPIPoint()"> <i
		class="fa fa-plus"></i> Add Data
	</a> 
	<button class="btn btn-lg btn-warning pull-right" onclick="deleteKPI()"><i class="fa fa-close"></i>Delete Indicator</button>
	| Discovering trends, increasing awareness
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->

<?php
$KPIID = $this->uri->segment ( 3 );
if ($KPIID > 0) {
	$KPI = $this->kpi->getKPI ( $KPIID );
	$KPIPoints = $this->kpi->getKPIPoints ( $KPIID );
}
?>

<!--  ACTIVITY -->
<div class="row">
	<div class="col-lg-12">
		<div class="box border blue">
			<div class="box-title">
				<h4>
					<i class="fa fa-bar-chart-o"></i><?php echo $KPI->indicatorName;?>
				</h4>
			</div>
			<div class="box-body">
			<?php
			if ($KPIPoints) {
				?>
<div id="mychart" style="height: 200px;"></div>
<?php
			} else {
				?>
<h1>No Data</h1>
<?php
			}
			?>
			</div>
		</div>
	</div>
	<div class="col-lg-12">
		<!-- BOX -->
		<div class="box border blue">
			<div class="box-title">
				<h4>
					<i class="fa fa-reorder"></i> <span class="hidden-inline-mobile">
						Plot Detail</span>
				</h4>
			</div>
			<div class="box-body">
				<table id="datatable1" cellpadding="0" cellspacing="0" border="0"
					class="datatable table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>Date</th>
							<th>Recorded By</th>
							<th>Value</th>
							<th>Delete</th>
						</tr>
					</thead>
					<tbody>
					<?php
					if ($KPIPoints) {
						foreach ( $KPIPoints->result () as $Point ) {
							?>
							<tr>
							<td><i class="fa fa-clock-o"></i> <?php echo $Point->recordedDate; ?></td>
							<td><?php echo $this->ion_auth->user($Point->userID)->row()->first_name;?></td>
							<td><?php echo $Point->pointValue;?></td>
							<td><button class="btn btn-default btn-lg"
									onclick="deleteDataPoint(<?php echo $Point->KPIDetailID; ?>)">
									<i class="fa fa-archive fa-1x"></i> Delete 
								</button></td>
						</tr>
							<?php
						}
					}
					?>
						
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<!-- /BOX -->


<!--  KPI INFO MODAL -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog"
	aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">

		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Performance Indicator
					Information</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" role="form">
					<div class="form-group">
						<label class="col-sm-3 control-label">Indicator Name</label>
						<div class="col-sm-9">
							<input type="text" name="regular" class="form-control"
								id="indicatorName" value="<?php echo $KPI->indicatorName; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Indicator Description</label>
						<div class="col-sm-9">
							<input type="text" name="regular" class="form-control"
								id="indicatorDescription"
								value="<?php echo $KPI->indicatorDescription; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Min Value</label>
						<div class="col-sm-9">
							<input type="text" name="regular" class="form-control"
								id="minValue" value="<?php echo $KPI->minValue; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Max Value</label>
						<div class="col-sm-9">
							<input type="text" name="regular" class="form-control"
								id="maxValue" value="<?php echo $KPI->maxValue; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Target Value</label>
						<div class="col-sm-9">
							<input type="text" name="regular" class="form-control"
								id="targetValue" value="<?php echo $KPI->targetValue; ?>">
						</div>
					</div>

				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" onclick="saveKPI()"
					id="saveKPIButton" data-loading-text="Saving...">Save</button>
			</div>
		</div>
	</div>
</div>


<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>
