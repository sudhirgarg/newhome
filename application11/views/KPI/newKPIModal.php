<?php 
$kpi = $this->kpi->getKPI($this->uri->segment('3', '-1'));

?>

<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"
				aria-hidden="true">&times;</button>
			<h4 class="modal-title" id="myModalLabel">Add a New Indicator</h4>
		</div>
		<div class="modal-body">
			<div class="form-horizontal">
				<input type="hidden" id="KPIID" value="<?php echo $kpi->KPIID;?>">
				<div class="form-group">
					<label class="col-sm-3 control-label">Indicator Name</label>
					<div class="col-sm-9">
						<input type="text" name="regular" class="form-control" id="indicatorName" value="<?php echo $kpi->indicatorName;?>">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Indicator Description</label>
					<div class="col-sm-9">
						<input type="text" name="regular" class="form-control" id="indicatorDescription" value="<?php echo $kpi->indicatorDescription;?>">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Min Value</label>
					<div class="col-sm-9">
						<input type="text" name="regular" class="form-control" id="minValue" value="<?php echo $kpi->minValue;?>">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Max Value</label>
					<div class="col-sm-9">
						<input type="text" name="regular" class="form-control" id="maxValue" value="<?php echo $kpi->maxValue;?>">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Target Value</label>
					<div class="col-sm-9">
						<input type="text" name="regular" class="form-control" id="targetValue" value="<?php echo $kpi->targetValue;?>">
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal" id="closeModalButton">Close</button>
			<button type="button" class="btn btn-primary" onclick="saveKPI()" id="saveKPIButton" data-loading-text="Saving...">Save</button>

	</div>
