<div class="clearfix">
	<h3 class="content-title pull-left">Reports</h3>
</div>
<div class="description">
	<button class="pull-right btn btn-primary"
		onclick="loadModal('<?php echo site_url('filingCab/newKPIModal');?>')">
		<i class="fa fa-plus"></i> New Indicator</i>
	</button> Overview, Statistics and more
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!--  MAIN PAGE HERE -->

<div class="row">
<?php
$KPIs = $this->kpi->getKPIs ();
$count = 1;
foreach ( $KPIs->result () as $KPI ) {
	?>
	<div class="col-lg-6">
		<div class="box border blue">
			<div class="box-title">
				<h4><?php echo $KPI->indicatorName; ?>
				</h4>
			</div>
			<div class="box-body">
				<a
					href="<?php echo site_url("filingCab/KPIDetail/" . $KPI->KPIID);?>">
					<div id="mychart<?php echo $count; $count++;?>"
						style="height: 200px;"></div>
				</a>
			</div>
		</div>
	</div>
	<?php
}
?>

</div>

