<?php $ci = get_instance(); ?>
<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left"> Batch Details
	<br/>
	<button class="btn btn-default" onclick="goBack()"> <img src="<?php echo base_url();?>img/go-back-icon.png" width="22px;" > Go Back</button>
	 </h3>
</div>
<div class="description">
	
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DELIVERY RUNS CONTENT -->
<!-- DATA TABLES -->

<!--***************************************** PAGE BODY STARTS ***************************************************************-->

<div class="row">
    <div class="col-sm-6 col-md-8">
    <div class="panel panel-info">
        <div class="panel-heading">

         <?php  include_once('subMenu.php'); ?>
         
         		
        </div>
        
        <div class="panel-body">
        	
        	<div class="table-responsive">
				  <table class="table table-striped">
				  	     <caption ><span class="label label-pill label-danger"> Batch # : <?php echo $batchNum; ?></span></caption>
				  		<thead>
						    <tr>
						    	  
							      <th>#</th>
							      <th>Pat #</th>
							      <th>Name</th>
							      <th>Home</th>
						    </tr>
						  </thead>
						  
						  <tbody>
						  	<?php 
						  
						  	 		$i =1;						  	 											
									
								    foreach ($batches AS $record) {
								    	echo "<tr>
								    		<td>".$i++."</td>
								    		<td>".$record->ID."</td>
								    		<td>".$record->LastName.' '.$record->FirstName."</td>
								    		<td>"; $ci->getPatWard($record->ID);  echo"</td>
								    	</tr>";
										
								    }
						  	 
						  	 ?>
						    
						    
						  </tbody>
										   
				  </table>
			</div>			

        </div>
        
        <div class="panel-body">
        	
        	   <?php 
        	   
        	       $n = 1;
        	       $i = 0;
        	       
        	       $numOfREcord =  COUNT($WardInfomation);
        	       
        	       if($numOfREcord > 1){
        	       		
        	       	echo '<div class="col-md-8">
			                    <div class="container">
					
			                        <div class="panel panel-warning">
			                            <div class="panel-heading">
			                                    <input type="hidden" name="batchNumOne" id="batchNumOne" value="'.$this->uri->segment(3).'">					                           
			                                	<mark> Note : If you like to have one delivery sheet for this batch, click here.	</mark>
		                                        <br/>';        	       	                            
        	       	
							        	       	echo'<select data-placeholder="Choose Patient..." id="patId" name="patId" ng-model="patId" class="chosen-select" style="width:250px;" tabindex="2">
										                        <option value=""></option>';
							        	       	foreach($patInfo AS $comboRecord){
							        	       		echo '<option value="'.$comboRecord->ID.'">'.$comboRecord->LastName.' '.$comboRecord->FirstName.'</option>';
							        	       	}
							        	       	echo'</select>';        	      
		                                        
		                                     echo'<button class="btn btn-danger" onclick="generateOneSheet()"> Print One Sheet</button>
			                            </div>
			                        </div>
			                    </div>
			                  </div>';
        	       }
        	       
        	       foreach($WardInfomation AS $record){
        	       	    $num = $n++;
        	       	    $name =  $record->LastName.' '.$record->FirstName;
					    $address1 = $record->Address1.', '.$record->City.', '.$record->Prov.', '.$record->Postal;
					    $address2 = $record->Address2;
					      
					    $deliveryType = ($record->DeliveryRouteType == 1) ? "N" : "Y";
					    
					    $route = $record->DeliveryRoute;
					    $day = "";
					    $dayTime ="";
					    
					    if(!empty($route)){
					    	$daySub = strtoupper(substr($record->DeliveryRoute,0,3));					    	
					    	if($daySub == 'MON')    { $day = 'Monday'; }
					    	elseif($daySub == 'TUE'){ $day = 'Tuesday'; }
					    	elseif($daySub == 'WED'){ $day = 'Wednesday'; }
					    	elseif($daySub == 'THU'){ $day = 'Thursday'; }
					    	elseif($daySub == 'FRI'){ $day = 'Friday'; }
					    	elseif($daySub == 'SAT'){ $day = 'Saturday'; }
					    	elseif($daySub == 'SUN'){ $day = 'Sunday'; }
					    	else { $day = date('l'); } 					    	
					    	
					    }else{
					    	$day = date('l');
					    }
					    
					    
					    if(!empty($route)){
					    	$dayTimeSub = strtoupper(substr($record->DeliveryRoute,3,5));
					    	if($dayTimeSub == 1000 )     { $dayTime = '10 AM';  }
					    	elseif($dayTimeSub == 1300 ) { $dayTime = '01 PM';  }
					    	elseif($dayTimeSub == 1500 ) { $dayTime = '03 PM';  }
					    	else                          { $dayTime = '11 AM';  }
					    	
					    }else{
					    	$dayTime = "10 AM";
					    }
					    
						
					    $date = date('Y-m-d',strtotime('next '.$day));
											    
					   					   
					    echo '<form class="MultiOrder">';
                       					   
					    echo '<div class="col-md-8">
			                    <div class="container">
			                     
			                        <div class="panel panel-default">
			                            <div class="panel-heading"><b>Order Sheet # '.$num.'</b> <br/>Print By: </div>
			                            <div class="panel-body">
					    					 <div class="input-group">
			                                    <input type="hidden" value="'; $ci->getPatInfoBaseOnWardIdAndBatch($record->ID, $listPatId); echo '" id="listOfPatId" name="listOfPatId">
												<input type="hidden" value="'.$batchNum.'" id="batchNum" name="batchNum">
					    			            <input type="hidden" value="'.$name.'" id="home" name="home">
												<input type="hidden" value="'.$batchNum.'-'.$i++.'" id="orderNum" name="orderNum">
					    			            <input type="hidden" value="'.$record->NHID.'" id="NHID" name="NHID">
												<input type="hidden" value="'.$record->NHWardID.'" id="NHWardID" name="NHWardID">
			                                </div>
			                                
			                                <div class="input-group">
			                                    <span class="input-group-addon" id="basic-addon1" style="width:100px"> D.Day </span>
			                                  
			                                    <input type="text" name="deliverDay" id="deliverDay" class="form-control" aria-describedby="basic-addon1" readonly="readonly" value="'.$day.' / '.$name.'">   
			                                    <input type="text" name="DTime" id="DTime" class="form-control" aria-describedby="basic-addon1"  value="'.$dayTime.'">
			                                </div>
			                                
			                                <div class="input-group">
			                                    <span class="input-group-addon" id="basic-addon1" style="width:100px"> D.Date </span>
			                                    <input type="date" name="deliverDate" id="deliverDate" class="form-control" aria-describedby="basic-addon1" value="'.$date.'"  >                                    
			                                </div>
			                                
			                                <div class="input-group">
			                                    <span class="input-group-addon" id="basic-addon1" style="width:100px"> Address:  </span>
			                                    <input type="text" id="address1" name="address1" class="form-control" placeholder="" aria-describedby="basic-addon1"  value="'.$address1.'"> 
			                                    <input type="text" id="address2" name="address2" class="form-control" placeholder="" aria-describedby="basic-addon1" value="'.$address2.'">
			                                </div>
			                                
			                                <div class="input-group">
			                                    <span class="input-group-addon" id="basic-addon1" style="width:100px">Instruction  </span>
			                                    <input type="text" id="DeliveryInstructions" name="DeliveryInstructions" class="form-control" placeholder="NULL" aria-describedby="basic-addon1" value="">
			                                    <input type="text" class="form-control" placeholder="NULL" aria-describedby="basic-addon1" value="" readonly="readonly">
			                                </div>
			                                
			                                <div class="input-group">
			                                    <span class="input-group-addon" id="basic-addon1" style="width:100px">D Type  </span>
			                                    <input type="radio" id="DeliveryType'.$num.'" name="DeliveryType" value="y"'; if($deliveryType == 'Y')  { echo ' checked '; } echo '>Yes
			                                    <input type="radio" id="DeliveryType'.$num.'" name="DeliveryType" value="n"'; if($deliveryType == 'N')  { echo ' checked '; } echo '>No
			                                </div>
			                                    		
			                                
			                                <div class="input-group">
			                                    <span class="input-group-addon" id="basic-addon1" style="width:100px"> <img src="'.base_url().'img/sg.png" width="22">  </span>
			                                    <select class="form-control" id="event" name="event">
			                                        <option value="OPERATION">  OPERATION  </option>
			                                        <option value="PRN">        PRN        </option>
			                                        <option value="MED CHANGE"> MED CHANGE </option>
			                                        <option value="Re-ORDER">   Re-ORDER   </option>
			                                    	<option value="Spares">     Spares     </option>	
			                                        <option value="ANTIBIOTIC"> ANTIBIOTIC </option>
			                                    </select>
			                                </div>                                
			                                
			                                <div class="input-group">
			                                    <span class="input-group-addon" id="basic-addon1" style="width:100px"> <img src="'.base_url().'img/sg.png" width="22">  </span>
			                                    <input type="text" id="comment" name="comment" class="form-control" placeholder="Comment" aria-describedby="basic-addon1" value="">
			                                </div>
			                                
			                                
			                            </div> 
			                            <div class="panel-footer clearfix">
			                                <div class="pull-right">';
					    
					                            if($numOfREcord == 1){
					                            	
					                            	echo '<input type="hidden" name="GaP" id="GaP" value="1">
					                            	      <button type="submit" class="btn btn-info" > GENERATE </button>';
					                            }else{
					                            	echo '<button type="submit" class="btn btn-info" > Save  # '.$num.' </button>';	
					                            }		                                		
			                                    
			                                echo '</div>
			                                        		
			                            </div> 
			                                        		
			                        </div>
			                   
			                    </div>
			                </div>
			            </form>';
        	       }
        	      
        	       if($numOfREcord > 1){
        	       	   
        	       	     echo '<div class="col-md-8">
			                    <div class="container">
        	       	     
			                        <div class="panel panel-default">
			                            <div class="panel-heading">
			                                   <input type="hidden" name="batchNum" id="batchNum" value="'.$this->uri->segment(3).'">
					                           <button class="btn btn-info" onclick="generateAllSheet()"> GENERATE ALL </button>
			                                	<mark> Note : Save all the orders individually before click "GENERATE ALL" , unless you will miss Delivery sheets.	</mark>			                                		
			                            </div>
			                        </div>
			                    </div>
			                  </div>';                  		
        	       }else{
        	       	  
        	          	echo '<div class="col-md-8">
			                    <div class="container">        	       	
			                        <div class="panel panel-warning">
			                            <div class="panel-heading">
			                                   <input type="hidden" name="batchNumMul" id="batchNumMul" value="'.$this->uri->segment(3).'">
			                                   <mark> Note : If you like to have multiple delivery sheet for this batch, click here.	</mark>
        	       				               <br/>
        	       				               <button class="btn btn-danger" onclick="generateMultipleSheet()"> Print multiple Sheet</button>
			                            </div>
			                        </div>
			                    </div>
			                  </div>';
        	       }
        	   
        	   ?>
         	
        </div>
    </div>
    </div>
</div>



<!--***************************************** PAGE BODY ENDS ***************************************************************-->


<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>

