<?php
  
    $ci =& get_instance();
?>


<!DOCTYPE html>
<html lang="en">
<head>
	
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title> <?php if($this->uri->segment(2) == "NULL") { echo "Home"; } else { echo $this->uri->segment(2); } ?> </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/cloud-admin.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/themes/default.css" id="skin-switcher">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">		

   <link rel="stylesheet" href = "http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">	
   <style>
       .border-bottom {
            border-bottom: 1px solid black;
        }
        
         body {
        width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
        background-color: #FAFAFA;
        font: 12pt "Tahoma";
	    }
	    * {
	        box-sizing: border-box;
	        -moz-box-sizing: border-box;
	    }
	    .page {
	        width: 210mm;
	        min-height: 297mm;
	        margin: 10mm auto;
	        border-radius: 5px;
	        background: white;
	        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
	    }
	    .subpage {
	        padding: 1cm;
	        height: 257mm;
	    }
	    
	    @page {
	        size: A4;
	        margin: 0;
	    }
	    @media print {
	        html, body {
	            width: 210mm;
	            height: 297mm;        
	        }
	        .page {
	            margin: 0;
	            border: initial;
	            border-radius: initial;
	            width: initial;
	            min-height: initial;
	            box-shadow: initial;
	            background: initial;
	            page-break-after: always;
	        }
	    }
   </style>
</head>
<body onLoad="myPrint();">
  

<!--***************************************** PAGE BODY STARTS ***************************************************************-->
<div class="book">


<?php foreach ($batchInfo AS $info){ ?>

<div class="page">
<div class="subpage">
<div class="row">
    <div class="col-md-8 pull-center" style="padding-left:50px;padding-right:50px;">
    
        <img src="<?php echo base_url();?>img/logo/logo.png" width="250px">
    <table width="100%" border="1">
        <tr>
            <td>
                <div class="panel panel-default">
                <div class="panel-body">
                    <table width="100%">
                        <tr>
                            <td>
                             <img class="pull-right" src="<?php echo base_url().'production/setBarcode/'.$info->orderNum; ?>">
                             
                                <h3><?php echo ($info->deliveryType == "y") ? "DELIVER ORDER" : "PICK UP ORDER"; ?></h3>
                                
                            </td> 
                        </tr>    
                        <tr>
                            <td>    
                                DELIVER ON: 
                                   <?php echo $info->deliveryDate ."</br>".$info->DTime .".</br> Order # : ".$info->orderNum; ?>
                                <br/>

                            </td> 
                        </tr>
                        <tr>
                            <td align="center">
                                <h1> <?php echo date('l',strtotime($info->deliveryDate)); ?> </h1>
                            </td> 
                        </tr>
                    </table>
                </div>
                </div>
            </td>              
        </tr>
        
        <tr>
            <td>
                <div class="panel panel-default">
                <div class="panel-body">
                    
                    <h1> <small> SHIP TO: </small> <?php echo $info->home; ?>  </h1> 
                    <h2><?php echo $info->address1.'<br/>'.$info->address2; ?> </h2>
                    </br>
                         Phone # <?php $patId = explode(',',$info->listOfPatId); $ci->getWardPhoneNumPat($patId[0]); ?> 
                    </br>
                   
                    <?php echo $info->instruction; ?>
                    
                </div>
                </div>
            </td>
        </tr>
        
        <tr height="200px" valign="top">
            <td>
                <div class="panel panel-default" >
                <div class="panel-body">
                    Details of patient's </br>
                    # OF Patient :
                    <?php 
                                              
                        $patID = explode(',',$info->listOfPatId);
                                                
                        echo COUNT($patID); 
                        echo "</br>";
                        
                        $i = 0;
                        
                        for($i; $i < COUNT($patID); $i++){
                            $id = $patID[$i]; 
                            $ci->getPatNameBaseOnId($id);
                            
                        }
                    
                    ?>
                </div>
                </div>     
            </td>
        </tr>       
                
        <tr>
            <td>
                </br>
                Print Name : .....................................             <spam class="pull-right"> Sign :..................................... </spam>
               
            </td>
        </tr>
        
       
    </table>
    
    
    
	    </div>
		</div>
    </div>
</div>

<?php } ?>



<!--***************************************** PAGE BODY ENDS ***************************************************************-->

<script>
    function myPrint() {
        window.print();
    }
        
</script>

</body>
</html>