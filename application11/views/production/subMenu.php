<?php  
     $d = $this->uri->segment(2);
     $active = ""; 
     $ci = & get_instance();
?>

<nav class="navbar navbar-default">
	
   <div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
		      </button>
		     
	    </div>
	
	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	      	   		
	
		        <li class="<?php if($d == "getBatch") { echo "active"; } ?>">
		        	  <a href="<?php echo base_url(); ?>production/getBatch">
		        		  Order <span class="sr-only">(current)</span>
		        		  <span class="badge badge-info" style="background-color:blue;"><?php $ci->getNumOFOrders(); ?></span>	
		        		  
		        	  </a>
		        </li>
		        
		        <li class="<?php if($d == "pacMed") { echo "active"; } ?>">
		        	<a href="<?php echo base_url(); ?>production/pacMed">
		        		Pharmacist Check
		        	    <span class="badge badge-info" style="background-color:blue;"><?php $ci->getNumOfPacmed(); ?></span>	
		        	</a>
		        </li>
		        
		        <li class="<?php if($d == "pacVision") { echo "active"; } ?>">
		        	<a href="<?php echo base_url(); ?>production/pacVision">
		        		Pac Med
		        		<span class="badge badge-info" style="background-color:blue;"><?php $ci->getNumOfPacVision(); ?></span>
		        	</a>
		       </li>
		        
		        <li class="<?php if($d == "pharmacistCheck") { echo "active"; } ?>">
		        	<a href="<?php echo base_url(); ?>production/pharmacistCheck">
		        		Pac Vision 
		        		<span class="badge badge-info" style="background-color:blue;"><?php $ci->getNumOfPharmacist(); ?></span>	
		            </a>
		        </li>
		        
		         <li class="<?php if($d == "finalCheck") { echo "active"; } ?>">
		        			        
			         <a href="<?php echo base_url(); ?>production/finalCheck">
		        		Final Check
		        		<span class="badge badge-info" style="background-color:blue;"><?php $ci->getNumOfFinalCheck(); ?></span>	
		            </a>
		        </li>
		        
		        
		        <li class="<?php if($d == "delivery") { echo "active"; } ?>">
		        			        
			         <a href="<?php echo base_url(); ?>production/delivery">
		        		Delivery
		        		<span class="badge badge-info" style="background-color:blue;"><?php $ci->getNumOfDelivery(); ?></span>	
		            </a>
		        </li>
		        
		        <!-- li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Setting <span class="caret"></span></a>
			          <ul class="dropdown-menu">
				            <li class="<?php if($d == "deliveryShedule") { echo "active"; } ?>"><a href="<?php echo base_url();?>production/deliveryShedule">Delivery Schedule</a></li>
				            <li><a href="#">Another action</a></li>
				            <li><a href="#">Something else here</a></li>
			          </ul>
		        </li-->		        
		        
		        
	      </ul>	
	       <form class="navbar-form navbar-right" role="search">
		        <div class="form-group">
		          <input type="text" class="form-control" placeholder="Search">
		        </div>
		   </form>			     
	     
	    </div><!-- /.navbar-collapse -->
    
   </div><!-- /.container-fluid -->
  
</nav>  