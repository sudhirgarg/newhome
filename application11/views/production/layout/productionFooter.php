<?php $infoUri = $this->uri->segment(2); ?>

</div>


 <!--[if !IE]> -->
        
   <script src="<?php echo base_url(); ?>js/jquery/jquery-2.0.3.min.js"></script>

<!-- <![endif]-->

<script	type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jQuery-slimScroll-1.3.0/slimScrollHorizontal.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jQuery-Cookie/jquery.cookie.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/script.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/dirPagination.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/angularProductionHome.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/anysearch.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>js/chosen.jquery.js"></script> 


<script type="text/javascript">
    var config = {
        '.chosen-select'           : {},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
        '.chosen-select-width'     : {width:"95%"}
    }
    
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
    
</script>



<script>

		function loadModal(ajaxURL) {
            $.get(ajaxURL, function(data) {
                $("#myModalContent").html(data);
                $('#myModal').modal();
            });	
		}
	
		$(document).ready(function() {
		        App.setPage("widgets_box");
	            App.init(); 
	              
	                
	            <?php if($infoUri == 'pacMed') { ?>
	            
		            $(document).anysearch({
	        	        searchFunc: function(search) {
	        	           // alert('Search: ' + search);
	        	            
	        	            $.post("<?php echo base_url(); ?>production/complatePACmedByOrderId",{
	                            orderId : search
	                        },function(data){	                          
	                            $("#returnInfo").html(data); 	                            
	                            if(data == "Update Successfully!!!"){
	                                setTimeout(autoRefresh1(), 1000); 
	                            }
	                        });
	        	        },
	        	        isBarcode: function(barcode) {
	        	            alert('Barcode: ' + barcode);
	        	        }	        
	        	    });     
	        	           

	          <?php  } ?>

	          <?php if($infoUri == 'pacVision') { ?>	        
	              
	        	    $(document).anysearch({
	        	        searchFunc: function(search) {
	        	           // alert('Search: ' + search);
	        	            $.post("<?php echo base_url(); ?>production/complatePACvisionByOrderId",{
	                            orderId : search
	                        },function(data){	                          
	                            $("#returnInfo").html(data); 	                            
	                            if(data == "Update Successfully!!!"){
	                                setTimeout(autoRefresh1(), 1000); 
	                            }
	                        });
	        	        },
	        	        isBarcode: function(barcode) {
	        	            alert('Barcode: ' + barcode);
	        	        }
	        	    });

	          <?php  } ?>

	          <?php if($infoUri == 'pharmacistCheck') {	        
              
		      	  echo '$(document).anysearch({
		      	        searchFunc: function(search) {
		      	          
		      	            $.post("'.base_url().'production/complatePharmacistByOrderId",{
		                          orderId : search
		                      },function(data){	                          
		                    	  $("#returnInfo").html(data); 	
		      	            		
		                            if(data == "Update Successfully!!!"){
		      	            		   
		                                setTimeout(autoRefresh1(), 1000); 
		                            }
		                      });
		      	        },
		      	        isBarcode: function(barcode) {
		      	            alert("Barcode: " + barcode);
		      	        }
		      	    });';
	
	         } ?>

	         <?php if($infoUri == 'finalCheck') {	        
	              
		      	  echo '$(document).anysearch({
		      	        searchFunc: function(search) {
		      	          
		      	            $.post("'.base_url().'production/complateFinalCheckOrderId",{
		                          orderId : search
		                      },function(data){	                          
		                    	  $("#returnInfo").html(data); 	
		      	            		
		                            if(data == "Update Successfully!!!"){
		      	            		   
		                                setTimeout(autoRefresh1(), 1000); 
		                            }
		                      });
		      	        },
		      	        isBarcode: function(barcode) {
		      	            alert("Barcode: " + barcode);
		      	        }
		      	    });';
	
	         } ?>
	        
			    
	            $(".MultiOrder").submit(function(e){
	               
	                var form_data = $(this).serialize();
	                var button_content = $(this).find('button[type=submit]');
	                var buttonVal      = button_content.val();
	                button_content.html('Generating...'); 

	               
	                $.ajax({ 
					
	                    url: "<?php echo site_url('Production/saveOrderSheet');?>",
	                    type: "POST",	                   
	                    data: form_data
	                        
	                }).done(function(data){   
	                	
		                if(data > 0){
			                alert("Something went wrong, Try again!");		                	
			                
		                }else if(data == 0){
		                	button_content.html('Saved');             
		                }else{
		                	button_content.html('Saved');
		                	var myURL = "<?php echo base_url(); ?>Production/printOrderSheet/" + data;          
			                window.open(myURL, "", "width=950, height=1000");
   		                }
   		                
	                 	                   
	                });
	               
	                e.preventDefault();
	                
	            });
	           
	            $(".assignDeliveryID").submit(function(e){
	                
	                var form_data = $(this).serialize();
	                var button_content = $(this).find('button[type=submit]');
	                button_content.html('Inserting...'); 
	                
	                $.ajax({ 
					
	                    url: "<?php echo site_url('Production/assignDeliveryHomeID');?>",
	                    type: "POST",
	                    //dataType:"json", //expect json value from server
	                    data: form_data
	                        
	                }).done(function(data){                  
	                    button_content.html('Inserted');
	                    alert(data);
	                    
	                });
	               
	                e.preventDefault();
	            });	         
	        
		});   	   
            
        function goBack() {  window.history.back();  }  
        
        
        function autoRefresh1(){
            window.location.reload();
        }

        function generateAllSheet(){
        	var r = confirm("Did you save all the orders?");
            if (r == true) {
                var data = $('#batchNum').val();
            	var myURL = "<?php echo base_url(); ?>Production/printAllOrderSheet/" + data;          
                window.open(myURL, "", "width=950, height=1000");
            } else {
            	 alert('out');
            }           
        }

        function getPatFromApi(){
    		
	   		 $.post('<?php echo site_url('compliance/getPatDetailsBySearch'); ?>',{
	   		 	  info : $("#patFuzySearch").val()
	   		 },function(data, status){
	   		 	 $("#patientResults").html(data);
	   		 	  
	   		 });   		
     	}

     	function generateOneSheet(){
         	
         	$.post('<?php echo base_url();?>production/generateOneSheet',{
             	  batchNum : $("#batchNumOne").val(),
             	  patId    : $("#patId").val()
            }, function(data, status){            	
	            	if(data == 'Choose the home'){ 
	            		alert(data);  		
	            	}else{
	            		var myURL = "<?php echo base_url(); ?>Production/printAllOrderSheet/" + data;          
	               		window.open(myURL, "", "width=950, height=1000");
	            	}	
         	});
         	
     	}

     	function generateMultipleSheet(){
         	
     		 $.post('<?php echo base_url();?>production/generateMultipleSheet',{
	           	  batchNum : $("#batchNumMul").val()
		      }, function(data, status){   
			               	
	            	if(data == 'Choose the home'){ 
	            		alert(data);  		
	            	}else{	            		 
	            		var myURL = "<?php echo base_url(); ?>Production/printAllOrderSheet/" + data;          
	               		window.open(myURL, "", "width=950, height=1000");
	            	}	
	       	});
     	}
    				
     	
    		
</script>




</body>
</html>