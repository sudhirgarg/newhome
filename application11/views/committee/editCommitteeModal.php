<?php 
    $committee = $this->committee->getCommittee($this->uri->segment('3'));
?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"
		aria-hidden="true">&times;</button>
	<h4 class="modal-title">Edit Committee Details</h4>
</div>
<div class="modal-body">
	<div class="form-horizontal">
        <input type="hidden" id="committeeID" value="<?php echo $committee->committeeID;?>"/>

		<div class="form-group">
			<label class="col-sm-3 control-label">Committee Name</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" placeholder="Ex: Policy Review Board" id="committeeName" value="<?php echo $committee->committeeName;?>">
			</div>
		</div>

        
		<div class="form-group">
			<label class="col-sm-3 control-label">Mandate</label>
			<div class="col-sm-9">
				<textarea class="form-control" rows="3" id="committeeDescription"><?php echo $committee->committeeDescription;?></textarea>
			</div>
		</div>

	</div>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal" id="closeModalButton">Close</button>
    <button type="button" class="btn btn-default" onclick="saveCommittee()" id="saveCommitteeButton" data-loading-text="Loading...">Save</button>
</div>

