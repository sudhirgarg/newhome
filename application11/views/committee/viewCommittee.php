<?php
$committeeID = $this->uri->segment ( '3', '-1' );
$committee = $this->committee->getCommittee ( $committeeID );
?>

<div class="clearfix">
	<h3 class="content-title pull-left">
		Committee:
		<?php echo $committee->committeeName; ?>
	</h3>
</div>
<div class="description">
	<a class="btn btn-default btn-lg "
		href="<?php echo base_url();?>filingCab/committeeDash">Back</a>
	<button class="btn btn-default btn-lg "
		onclick="loadModal('<?php echo site_url('filingCab/editCommitteeModal/' . $this->uri->segment('3', '-1'));?>')">
		<i class="fa fa-pencil"></i> Edit
	</button>
	<button class="btn btn-warning pull-right btn-lg"
		onclick="archive(<?php echo $committeeID;  ?>)">
		<i class="fa fa-archive"></i> Archive
	</button>
	<button class="btn btn-primary btn-lg" onclick="newMeeting()">
		<i class="fa fa-plus"></i> New Meeting
	</button>
	| Learning and growing
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!--  MAIN PAGE HERE -->

<div class="well">
	<p><?php echo $committee->committeeDescription; ?></p>
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="box border blue">
			<div class="box-title">
				<h4>
					<i class="fa fa-bars"></i>Meeting Minutes
				</h4>
			</div>
			<div class="box-body">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>Title</th>
							<th>Meeting Date</th>
							<th>Members Present</th>
							<th>Discussion</th>
						</tr>
					</thead>
					<tbody>
                <?php
																$minutes = $this->committee->getCommitteeMeetings ( $committeeID );
																if ($minutes->num_rows () > 0) {
																	foreach ( $minutes->result () as $min ) {
																		?>
                            <tr style="cursor: pointer;"
							onclick="loadModal('<?php echo site_url('filingCab/editMinutesModal/' . $this->uri->segment('3') . '/'. $min->committeeMinutesID);?>')">
							<td><?php echo $min->title; ?></td>
							<td><?php echo date('Y-m-d', strtotime($min->dateCreated)); ?></td>
							<td><?php echo $min->present; ?></td>
							<td><?php echo $min->discussion; ?></td>
						</tr>
                        <?php
																	}
																} else {
																	?>
                        <tr>
							<td colspan="4">No Meetings Yet!</td>
						</tr>
                    <?php
																}
																?>
            </tbody>
				</table>
			</div>
		</div>
	</div>
</div>

