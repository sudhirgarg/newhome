<div class="clearfix">
	<h3 class="content-title pull-left">Users</h3>
</div>
<div class="description">

	<a class="btn btn-default btn-lg"
		href="<?php echo base_url();?>setting/newuser"> <i class="fa fa-plus">
			Create User</i>
	</a> <a class="btn btn-default btn-lg"
		href="<?php echo base_url();?>auth/create_group"> <i
		class="fa fa-plus"> Create Group</i>
	</a>
	
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DELIVERY RUNS CONTENT -->
<!-- DATA TABLES -->
<div class="row">


	<div class="col-md-12">
		<div class="box border primary">
			<div class="box-title">
				<h4>
					<i class="fa fa-bars"></i>Users and Permissions
				</h4>
			</div>
			<div class="box-body big">

				<div class="alert alert-block alert-info fade in">
					<a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times; </a>
					<p>
					<h4>
						<i class="fa fa-check-square-o"></i>
						<div id="infoMessage"><?php echo $message;?></div>
					</h4>
					</p>
				</div>


				<table id="datatable1" cellpadding="0" cellspacing="0" border="0"
					class="datatable table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>First Name</th>
							<th>Last</th>
							<th>Emai</th>
							<th>Groups</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
					
	<?php foreach ($users as $user):?>
		<tr>
							<td><?php echo $user->first_name;?></td>
							<td><?php echo $user->last_name;?></td>
							<td><?php echo $user->email;?></td>
							<td>
				<?php foreach ($user->groups as $group):?>
					<?php echo anchor("auth/edit_group/".$group->id, $group->name) ;?><br />
                <?php endforeach?>
			</td>
							<td><?php echo ($user->active) ? anchor("auth/deactivate/".$user->id, lang('index_active_link')) : anchor("auth/activate/". $user->id, lang('index_inactive_link'));?></td>
							<td><?php echo anchor("auth/edit_user/".$user->id, 'Edit') ;?></td>
						</tr>
	<?php endforeach;?>
</tbody>
				</table>

			</div>
		</div>

	</div>
</div>
<!-- /CONTENT -->



<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>


