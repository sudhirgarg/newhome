<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left">Delivery Run</h3>
</div>
<div class="description">
	<button class="btn btn-default btn-lg">
		<i class="fa fa-save"></i> Save Run
	</button>
	<button class="btn btn-default btn-lg">
		<i class="fa fa-truck"></i> DELIVER
	</button>
	<button class="btn btn-default btn-lg pull-right">
		<i class="fa fa-print"></i>
	</button>
	| Scan Derlivery Orders in this run
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DELIVERY RUNS CONTENT -->
<!-- DATA TABLES -->
<div class="row">
	<div class="col-md-12">

		<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['form1'];
if (!theForm) {
    theForm = document.form1;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>

		<div>
			<img alt="Pharmacy Name" class="logo"
				src="<?php echo base_url();?>img/logo/logo.png" width="40px"
				height="20px" />
			<table>
				<tr>
					<td colspan="2">
						<h1>
							<span id="Repeater1_lbl_Title_0">Delivery Order</span>
						</h1>
						<p>DELIVER ON:
						
						<div class="day">
							<span id="Repeater1_lbl_DeliveryDate_0">Thursday</span>
						</div>
						</p>
						<div class="date">Thursday, March 27, 2014 10:00 AM</div>
					</td>
					<td class="barcode">
						<p>
							<img id="Repeater1_img_BarCode_0"
								src="//25.151.196.69/myhcintranet/UI/deliveries/temp29719.bmp"
								alt="No Data" style="height: 50px; width: 200px;" />
						</p>
						<p>
							ID:<span id="Repeater1_lbl_DeliveryOrderID_0">29719</span>
						</p>
					</td>
				</tr>
				<tr>
					<td style="padding-left: 20px" colspan="3">
						<p class="name">
							SHIP TO: <span id="Repeater1_txt_Name_0" class="name">Doe, John </span>
						</p>
						<div class="address">
							<p>
								<span id="Repeater1_txt_Address1_0">123 Crystal Dr</span>
							</p>
							<p>
								<span id="Repeater1_txt_Address2_0">DEL THURS 10AM</span>
							</p>
							<p>
								<span id="Repeater1_txt_City_0">Peterborough</span> <span
									id="Repeater1_txt_Prov_0"
									style="display: inline-block; width: 30px;">ON</span> <span
									id="Repeater1_txt_Postal_0"
									style="display: inline-block; width: 60px;"></span>
							</p>
							<p class="phone">
								<span id="Repeater1_txt_Phone_0">(705) 749-1133</span>
							</p>
						</div>
						<p>
							<span
								style="font-weight: bold; padding-left: 20px; padding-right: 20px;"><input
								id="Repeater1_chk_Controlled_0" type="checkbox"
								name="Repeater1$ctl00$chk_Controlled" /><label
								for="Repeater1_chk_Controlled_0">Controlled</label></span>
							Patient Count:<span id="Repeater1_lbl_PatientCount_0"
								style="display: inline-block; border-style: Solid;">1</span>
						</p>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<p style="margin-bottom: 1px">Notes / Special Delivery
							Instructions:</p> <textarea name="Repeater1$ctl00$TextBox1"
							rows="2" cols="20" id="Repeater1_TextBox1_0"
							style="border-style: Dashed; height: 100px; width: 700px;">
</textarea>
					</td>
				</tr>
				<tr class="sigbox">
					<td class="sig">_______________________________</td>
					<td class="sig">_______________________________</td>
					<td class="sig"></td>
				</tr>
				<tr>
					<td class="sig">Print Name</td>
					<td class="sig">Signature</td>
					<td class="sig"></td>
				</tr>
				<tr>
					<td colspan="2" class="amountdue">Amount Due: $</td>
					<td><input name="Repeater1$ctl00$txt_AmountDue" type="text"
						value="0.00"
						onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;Repeater1$ctl00$txt_AmountDue\&#39;,\&#39;\&#39;)&#39;, 0)"
						onkeypress="if (WebForm_TextBoxKeyHandler(event) == false) return false;"
						id="Repeater1_txt_AmountDue_0"
						style="border-style: Double; font-size: X-Large; font-weight: bold;" /></td>
				</tr>
			</table>
			<p class="break"></p>


		</div>
		</form>
	</div>
</div>
<!-- /CONTENT -->


<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>

