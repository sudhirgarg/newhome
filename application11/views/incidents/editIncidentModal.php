<?php
$isNew = true;
if ($this->uri->segment ( 3 ) > 0) {
	$isNew = false;
	$incident = $this->incidents->getIncidents ( $this->uri->segment ( 3 ) );
}
?>
<script>
			$(".datepicker-fullscreen").pickadate({
			    showOtherMonths: true,
			    autoSize: true,
			    appendText: '<span class="help-block">(yyyy-mm-dd)</span>',
			    dateFormat: "yy-mm-dd"
			});
			</script>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"
		aria-hidden="true">&times;</button>
	<h4 class="modal-title">Incident Detail</h4>
</div>
<div class="modal-body">
	<div class="form-horizontal" role="form">
		<input id="incidentID" type="hidden"
			value="<?php if($isNew==false){echo $incident->incidentID;}?>">
		<div class="form-group">
			<label class="col-sm-3 control-label">Incident Date</label>
			<div class="col-sm-9">
				<input type="text" id="incidentDatetime"
					class="form-control datepicker-fullscreen"
					value="<?php if($isNew==false){echo $incident->incidentDatetime; }else {echo date('Y-m-d H:t:s');}?>">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label">Reported By</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" id="reportedBy"
					placeholder="Text input"
					value="<?php if($isNew==false){ $user=$this->ion_auth->user($incident->reportedBy)->row() ; echo $user->first_name . ' '. $user->last_name;}?>"
					disabled>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label">Reported To</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" id="reportedTo"
					placeholder="Text input"
					value="<?php if($isNew==false){ echo $incident->reportedTo;}?>">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label">Description</label>
			<div class="col-sm-9">
				<textarea class="form-control" id="description" rows="3"> <?php if($isNew==false){ echo $incident->description ;}?></textarea>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label">Action Taken</label>
			<div class="col-sm-9">
				<textarea class="form-control" id="actionTaken" rows="3"><?php if($isNew==false){ echo $incident->actionTaken;}?></textarea>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label">Incident Type</label>
			<div class="col-sm-9">
				<select class="form-control" id="incidentType">
				<?php
				$incidentTypes = $this->incidents->getIncidentTypes ();
				foreach ( $incidentTypes->result () as $incidentType ) {
					?>
					<option value="<?php echo $incidentType->incidentTypeID;?>"
						<?php if($isNew==false && $incidentType->incidentTypeID==$incident->incidentType){ echo 'selected="selected"';}?>><?php echo $incidentType->incidentType;?></option>
				<?php }?>
				</select>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal"
			id="closeButtonModal">Close</button>
		<button type="button" class="btn btn-primary" id="saveIncidentButton"
			onclick="saveIncident()" data-loading-text="Saving...">Save changes</button>
	</div>