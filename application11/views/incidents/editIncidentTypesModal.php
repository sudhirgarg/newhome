<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"
		aria-hidden="true">&times;</button>
	<h4 class="modal-title">Incident Types</h4>
</div>
<div class="modal-body">
	<div class="form-horizontal" role="form">
		<input id="incidentID" type="hidden"
			value="<?php if($isNew==false){echo $incident->incidentID;}?>">
		<div class="form-group">
			<label class="col-sm-3 control-label">Incident Date</label>
			<div class="col-sm-9">
				<input type="text" id="incidentDatetime"
					class="form-control datepicker-fullscreen"
					value="<?php if($isNew==false){echo $incident->incidentDatetime; }else {echo date('Y-m-d H:t:s');}?>">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label">Incident Type</label>
			<div class="col-sm-9">
				<select class="form-control" id="incidentType">
				<?php
				$incidentTypes = $this->incidents->getIncidentTypes ();
				foreach ( $incidentTypes->result () as $incidentType ) {
					?>
					<option value="<?php echo $incidentType->incidentTypeID;?>"
						<?php if($isNew==false && $incidentType->incidentTypeID==$incident->incidentType){ echo 'selected="selected"';}?>><?php echo $incidentType->incidentType;?></option>
				<?php }?>
				</select>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal"
			id="closeButtonModal">Close</button>
		<button type="button" class="btn btn-primary" id="saveIncidentButton"
			onclick="saveIncident()" data-loading-text="Saving...">Save changes</button>
	</div>