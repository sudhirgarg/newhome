<html>
   <head>
      <meta http-equiv="content-type" content="text/html; charset=UTF-8">
   </head>
   <body>
      <table class="table table-hover">
         <thead>
            <tr>
               <th colspan="4">
                  <input type="checkbox" id="selectall" onclick="toggle_all()">
                     <a class="btn btn-light-grey" href="javascript:loadMailbox('<?php echo $mbox; ?>');"><i class="fa fa-refresh fa-lg"></i></a>
					 <div class="btn-group">
						 <button class="btn btn-light-grey dropdown-toggle" data-toggle="dropdown"> More
							<i class="fa fa-caret-down"></i>
						 </button>
						 <ul class="dropdown-menu context" role="menu">
							<li><a href="javascript:mark_read();"><i class="fa fa-pencil"></i> Mark as Read</a></li>
							<li><a href="javascript:mark_unread();"><i class="fa fa-pencil"></i> Mark as Unread</a></li>
							<li><a href="#"><i class="fa fa-ban"></i> Report Spam</a></li>
							<li><a href="javascript:del_mail();"><i class="fa fa-trash-o"></i> Delete</a></li>
						 </ul>
					 </div>
					 <div class="btn-group">
						 <button class="btn btn-light-grey dropdown-toggle" data-toggle="dropdown"> Move To
							<i class="fa fa-caret-down"></i>
						 </button>
						 <ul class="dropdown-menu context" role="menu">
<?php

$first = true;
for($i=0;$i<sizeof($mboxes);$i++) {
    $m = $mboxes[$i];
    $mailbox = $m;
    $parts = explode('}', $m);
    $name = $parts[1];
    if($mbox == $name) {
        echo "<li><a href=\"#\"><i class=\"fa fa-folder-open\"></i> ".$name."</a></li>\n";
    } else {
        echo "<li><a href=\"javascript:move_to('".$name."');\"><i class=\"fa fa-folder-o\"></i> ".$name."</a></li>\n";
    }
}

?>                                        
						 </ul>
					 </div>
               </th>
               <th colspan="3">
					<div class="btn-group pull-right">
						 <button class="btn btn-light-grey dropdown-toggle" data-toggle="dropdown">
							<i class="fa fa-cog fa-lg"></i> <i class="fa fa-caret-down"></i>
						 </button>
						 <ul class="dropdown-menu context" role="menu">
							<li><a href="#"><i class="fa fa-cogs"></i> Settings</a></li>
							<li><a href="#"><i class="fa fa-desktop"></i> Configure Inbox</a></li>
							<li><a href="#"><i class="fa fa-exclamation"></i> Help</a></li>
						 </ul>
					 </div>
			   </th>
            </tr>
         </thead>
         <tbody>
         <input type="hidden" name="mailbox" id="mailbox" value="<?php echo $mailbox; ?>" />
         
<?php

if(sizeof($msgs) > 0) {
    if(sizeof($msgs) > 1) {
        $msgs = array_reverse($msgs);
    }
    foreach($msgs as $msg) {
?>

            <tr class="new">
               <td class="width-10">
                  <input type="checkbox" class="selectedId" id="selectedId_<?php echo $msg->get_msgno(); ?>" name="selectedId[]" value="<?php echo $msg->get_msgno(); ?>">
               </td>
               <td class="width-10"><i class="fa fa-star"></i></td>
			   <td class="viewEmail width-10"></td>
               <td class="viewEmail hidden-xs" onclick="showMessage(<?php echo $msg->get_msgno(); ?>, '<?php echo $mbox; ?>')"><?php echo htmlspecialchars($msg->get_from()); ?></td>
               <td class="viewEmail" onclick="showMessage(<?php echo $msg->get_msgno(); ?>, '<?php echo $mbox; ?>')"><?php if($msg->Unseen == 'U'){ ?><span class="label label-success">New</span> <?php } ?><?php echo htmlspecialchars($msg->get_subject()); ?></td>
               <td class="viewEmail text-right"><?php echo $this->mailbox_model->utcToDynDate($msg->get_timestamp()); ?></td>
            </tr>

<?php /*
<tr class="clickableRow<?php echo ($msg->get_unseen() == 'U' ? ' unread-message' : ''); ?>">
    <td class="checkbox-col">
        <input type="checkbox" class="selectedId" id="selectedId_<?php echo $msg->get_msgno(); ?>" name="selectedId[]" value="<?php echo $msg->get_msgno(); ?>">
    </td>
    <td class="from-col" onclick="window.location='<?php echo base_url();?>bms/viewMessage/<?php echo $mbox."/".$msg->get_msgno(); ?>/'"><?php echo htmlspecialchars($msg->get_from()); ?></td>
    <td onclick="window.location='<?php echo base_url();?>bms/viewMessage/<?php echo $mbox."/".$msg->get_msgno(); ?>/'"><?php echo htmlspecialchars($msg->get_subject()); ?></td>
    <td onclick="window.location='<?php echo base_url();?>bms/viewMessage/<?php echo $mbox."/".$msg->get_msgno(); ?>/'"><?php echo byte_format($msg->get_size()); ?></td>
    <td class="date-col" onclick="window.location='<?php echo base_url();?>bms/viewMessage/<?php echo $mbox."/".$msg->get_msgno(); ?>/'">
        <?php if($msg->has_attachment()){ ?><i class="fa fa-paperclip"></i><?php } ?>
        <?php echo $this->mailbox_model->utcToDynDate($msg->get_timestamp()); ?>
    </td>
</tr>
*/ ?>
        
<?php
    }
} else {
    $ctr = 0;
}


?>




         </tbody>
		 <thead>
            <tr>
               <th colspan="4">
               </th>
               <th class="emailPager" colspan="3">
                  <span class="emailPagerCount">1-30 of 1343</span>
                  <a class="btn btn-sm btn-light-grey"><i class="fa fa-angle-left"></i></a>
                  <a class="btn btn-sm btn-light-grey"><i class="fa fa-angle-right"></i></a>
               </th>
            </tr>
         </thead>
      </table>
   </body>
</html>