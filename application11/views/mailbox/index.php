<!-- STYLER -->
									
									<!-- /STYLER -->
									<!-- BREADCRUMBS -->
									<ul class="breadcrumb">
										<li>
											<i class="fa fa-home"></i>
											<a href="index.html">Home</a>
										</li>
										<li>
											<a href="#">More Pages</a>
										</li>
										<li>Inbox</li>
									</ul>
									<!-- /BREADCRUMBS -->
									<div class="clearfix">
										<h3 class="content-title pull-left">Inbox</h3>
									</div>
									<div class="description">Email Inbox</div>
								</div>
							</div>
						</div>
						<!-- /PAGE HEADER -->
						<!-- INBOX -->
						<div class="row">
							<div class="col-md-12">
								<!-- BOX -->
								<div class="box border">
									<div class="box-title">
										<h4><i class="fa fa-envelope"></i>Email</h4>
										<div class="tools">
											<a href="#box-config" data-toggle="modal" class="config">
												<i class="fa fa-cog"></i>
											</a>
											<a href="javascript:;" class="reload">
												<i class="fa fa-refresh"></i>
											</a>
											<a href="javascript:;" class="collapse">
												<i class="fa fa-chevron-up"></i>
											</a>
											<a href="javascript:;" class="remove">
												<i class="fa fa-times"></i>
											</a>
										</div>
									</div>
									<div class="box-body">
										<!-- TOP ROW -->
										<div class="emailHeader row">
											<div class="emailTitle">
											  <div class="col-md-2">
												<img class="img-responsive pull-left" alt="Cloud Admin Logo" src="<?php echo base_url();?>img/logo/logo-alt.png">
											  </div>
											  <div class="col-md-10">
												  <form class="form-inline hidden-xs" action="index.html">
													 <div class="input-group input-medium">
														<input type="text" class="form-control" placeholder="Search Inbox">
														<span class="input-group-btn">                   
														<button type="submit" class="btn btn-success"><i class="fa fa-search"></i></button>
														</span>
													 </div>
												  </form>
											  </div>
										   </div>
										</div>
										<!-- /TOP ROW -->
										<hr>
										<!-- INBOX -->
										<div class="row email">
											<div id="list-toggle" class="col-md-2">
											<ul class="list-unstyled">
												<li class="composeBtn">
													 <a href="javascript:compose();" data-title="Compose" class="btn btn-danger"> 
														Compose
													 </a>
												 </li>
											</ul>
											   <ul class="emailNav nav nav-pills nav-stacked margin-bottom-10">	
											   
<?php

$first = true;
for($i=0;$i<sizeof($mboxes);$i++) {
    $m = $mboxes[$i];
    $mailbox = $m;
    $parts = explode('}', $m);
    $name = $parts[1];
    if($mbox == $name) {
        $actv = ' active';
    } else {
        $actv = '';
    }
    echo "<li class='inbox".$actv."' id=\"mailbox_".$name."\">";
    echo "    <a href=\"javascript:loadMailbox('".$name."');\" data-title=\"".$name."\">";
    echo "        <i class='fa fa-folder-o fa-fw'></i> ".$name;
    echo "    </a>";
    echo "</li>";
}

?> 											   


<?php /*											   								
												  <li class="inbox active" >
													 <a href="javascript:;" data-title="Inbox">
														<i class="fa fa-inbox fa-fw"></i> Inbox (2)
													 </a>
												  </li>
												  <li class="starred">
													<a href="javascript:;" data-title="Starred">
														<i class="fa fa-star fa-fw"></i> Starred
													</a>
												  </li>
												  <li class="sent">
													  <a href="javascript:;"  data-title="Sent">
														<i class="fa fa-mail-forward fa-fw"></i> Sent Items
													  </a>
												  </li>
												  <li class="draft">
													<a href="javascript:;" data-title="Draft">
														<i class="fa fa-files-o fa-fw"></i> Drafts
													</a>
												  </li>
												  <li class="spam">
													<a href="javascript:;" data-title="Spam">
														<i class="fa fa-ban fa-fw"></i> Spam
													</a>
												  </li>
												  <li class="trash">
													<a href="javascript:;" data-title="Trash">
														<i class="fa fa-trash-o fa-fw"></i> Trash
													</a>
												  </li>
*/ ?>
												  
											   </ul>
											</div>
											<div class="col-md-10">
											   <div class="emailContent"></div>
											</div>
										</div>
										<!-- /INBOX -->
									</div>
								</div>
								<!-- /BOX -->
							</div>
						</div>
						<!-- /INBOX -->
						<div class="footer-tools">
							<span class="go-top">
								<i class="fa fa-chevron-up"></i> Top
							</span>
						</div>
					</div><!-- /CONTENT-->
				</div>
			</div>
		</div>
	</section>
	<!--/PAGE -->
	<!-- JAVASCRIPTS -->
	<!-- Placed at the end of the document so the pages load faster -->
	<!-- JQUERY -->
	<script src="<?php echo base_url();?>js/jquery/jquery-2.0.3.min.js"></script>
	<!-- JQUERY UI-->
	<script src="<?php echo base_url();?>js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
	<!-- BOOTSTRAP -->
	<script src="<?php echo base_url();?>js/bootstrap-dist/js/bootstrap.min.js"></script>
	<!-- LESS CSS -->
	<script src="<?php echo base_url();?>js/lesscss/less-1.4.1.min.js" type="text/javascript"></script>	
	<!-- DATE RANGE PICKER -->
	<script src="<?php echo base_url();?>js/bootstrap-daterangepicker/moment.min.js"></script>
	<script src="<?php echo base_url();?>js/bootstrap-daterangepicker/daterangepicker.min.js"></script>
	<!-- SLIMSCROLL -->
	<script type="text/javascript" src="<?php echo base_url();?>js/jQuery-slimScroll-1.3.0/jquery.slimscroll.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>js/jQuery-slimScroll-1.3.0/slimScrollHorizontal.min.js"></script>
	<!-- BLOCK UI -->
	<script type="text/javascript" src="<?php echo base_url();?>js/jQuery-BlockUI/jquery.blockUI.min.js"></script>
	<!-- UNIFORM -->
	<script type="text/javascript" src="<?php echo base_url();?>js/uniform/jquery.uniform.min.js"></script>
	<!-- BOOTSTRAP WYSIWYG -->
	<script type="text/javascript" src="<?php echo base_url();?>js/bootstrap-wysiwyg/jquery.hotkeys.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>js/bootstrap-wysiwyg/bootstrap-wysiwyg.min.js"></script>
	<!-- COOKIE -->
	<script type="text/javascript" src="<?php echo base_url();?>js/jQuery-Cookie/jquery.cookie.min.js"></script>
	<!-- CUSTOM SCRIPT -->
	<script src="<?php echo base_url();?>js/script.js"></script>
	<script>
		$(document).ready(function(){
    		$.post('/mailbox/showInbox',
    		    {
        		    mbox: 'INBOX'
    		    }, function(data){
        		    $('.emailContent').html(data);
    		    }
    		);
		});
	</script>

<script type="text/javascript">

function compose() {
    var mbox = $('#mailbox').val();
	$.post('/mailbox/showCompose',
	    {
		    mbox: mbox
	    }, function(data){
		    $('.emailContent').html(data);
		    initToolbarBootstrapBindings();
		    $('#editor').wysiwyg({ fileUploadError: showErrorAlert} );
	    }
	);
}

function toggle_all() {
    if($('#selectall').is(':checked')) {
        $('.selectedId').prop('checked','checked');    
    } else {
        $('.selectedId').prop('checked','');    
    }
}

function loadMailbox(mbox) {
    $('.inbox').removeClass('active');
    $('#mailbox_'+mbox).addClass('active');
	$.post('/mailbox/showInbox',
	    {
		    mbox: mbox
	    }, function(data){
		    $('.emailContent').html(data);
	    }
	);
}

function showMessage(msgNo, mbox) {
    $.post('/mailbox/showSingleEmail/',
        {
            msgNo: msgNo,
            mbox: mbox
        }, function(data){
		    $('.emailContent').html(data);
        }
    );
}

function del_mail() {
    var chkArray = [];
    $(".selectedId:checked").each(function() {
        chkArray.push($(this).val());
    });
     
    var selected;
    selected = chkArray.join(',');
    if(selected.length > 0) {
        if(confirm("Are you sure?")) {
            $.post('/mailbox/deleteMail/',
                {
                    selected: selected,
                    mbox: '<?php echo $mbox; ?>'
                }, function(data) {
                loadMailbox('<?php echo $mbox; ?>');
                }
            );
        }
    }
}

function mark_read() {
    var chkArray = [];
    $(".selectedId:checked").each(function() {
        chkArray.push($(this).val());
    });
     
    var selected;
    selected = chkArray.join(',');
    if(selected.length > 0) {
        $.post('/mailbox/markSeen/',
            {
                selected: selected,
                mbox: '<?php echo $mbox; ?>'
            }, function(data) {
                loadMailbox('<?php echo $mbox; ?>');
            }
        );
    }
}

function mark_unread() {
    var chkArray = [];
    $(".selectedId:checked").each(function() {
        chkArray.push($(this).val());
    });
     
    var selected;
    selected = chkArray.join(',');
    if(selected.length > 0) {
        $.post('/mailbox/markUnseen/',
            {
                selected: selected,
                mbox: '<?php echo $mbox; ?>'
            }, function(data) {
                loadMailbox('<?php echo $mbox; ?>');
            }
        );
    }
}

function move_to(mbox) {
    var chkArray = [];
    $(".selectedId:checked").each(function() {
        chkArray.push($(this).val());
    });
     
    var selected;
    selected = chkArray.join(',');
    if(selected.length > 0) {
        $.post('/mailbox/moveMail/',
            {
                selected: selected,
                mbox: '<?php echo $mbox; ?>',
                moveTo: mbox
            }, function(data) {
                loadMailbox('<?php echo $mbox; ?>');
            }
        );
    }
}

function new_folder() {
    $('#new_folder_link').hide();
    $('#new_folder_box').show();
}

function create_mailbox() {
    var newMailbox = $('#newMailbox').val();
    if(newMailbox.length > 1) {
        $.post('/bms/createMailbox/',
            {
                newMailbox: newMailbox,
                mbox: '<?php echo $mbox; ?>'
            }, function(data) {
                window.location="<?php echo base_url(); ?>bms/mailbox/<?php echo $mbox; ?>/";
            }
        );
    }
}

</script>
	<!-- /JAVASCRIPTS -->