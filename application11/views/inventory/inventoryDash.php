<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left">Inventory</h3>
</div>
<div class="description">
	<a class="btn btn-default btn-lg"
		href="<?php echo base_url("filingCab/inventoryDetail");?>"> <i
		class="fa fa-pencil"></i> Inventory Audit
	</a> | Control the most important aspect the business.
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DELIVERY RUNS CONTENT -->
<!-- DATA TABLES -->
<div class="row">
	<div class="col-md-12">


		<!-- INVENTORY AUDITS -->
		<div class="row">

			<div class="col-lg-12">
				<div class="box border blue">
					<div class="box-title">
						<h4>
							<i class="fa fa-truck"></i>Inventory Audits % Error over Time
						</h4>
						<div class="tools">
							<a href="#box-config" data-toggle="modal" class="config"> <i
								class="fa fa-cog"></i>
							</a> <a href="javascript:;" class="reload"> <i
								class="fa fa-refresh"></i>
							</a> <a href="javascript:;" class="collapse"> <i
								class="fa fa-chevron-up"></i>
							</a> <a href="javascript:;" class="remove"> <i
								class="fa fa-times"></i>
							</a>
						</div>
					</div>
					<div class="box-body">

						<div id="mychart" style="height: 200px;"></div>

					</div>
				</div>
			</div>

			<div class="col-lg-12">
				<div class="box border blue">
					<div class="box-title">
						<h4>
							<i class="fa fa-truck"></i>Inventory Audits
						</h4>
					</div>
					<div class="box-body">
						<table id="datatable1" cellpadding="0" cellspacing="0" border="0"
							class="datatable table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th>Audit #</th>
									<th>Created By</th>
									<th>Created On</th>
									<th>Sample Size</th>
									<th>Error Rate</th>
									<th>View</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><span class="label label-warning arrow-in">In Progress</span><br>123</td>
									<td>John Doe</td>
									<td>Mar 14, 2013</td>
									<td>23</td>
									<td>Error: +3 % <br> SD +14 %
									</td>
									<td><a class="btn btn-default btn-lg"
										href="<?php echo base_url("delivery/rundetail/");?>"> <i
											class="fa fa-eye"></i> View
									</a></td>
								</tr>
								<tr>
									<td><span class="label label-success arrow-in">complete</span><br>123</td>
									<td>John Doe</td>
									<td>Mar 14, 2013</td>
									<td>23</td>
									<td>Error: +3 % <br> SD +14 %
									</td>
									<td><a class="btn btn-default btn-lg"
										href="<?php echo base_url("delivery/rundetail/");?>"> <i
											class="fa fa-eye"></i> View
									</a></td>
								</tr>
								<tr>
									<td><span class="label label-success arrow-in">complete</span><br>123
									</td>
									<td>John Doe</td>
									<td>Mar 14, 2013</td>
									<td>23</td>
									<td>Error: +3 % <br> SD +14 %
									</td>
									<td><a class="btn btn-default btn-lg"
										href="<?php echo base_url("delivery/rundetail/");?>"> <i
											class="fa fa-eye"></i> View
									</a></td>
								</tr>
								<tr>
									<td><span class="label label-success arrow-in">complete</span><br>123</td>
									<td>John Doe</td>
									<td>Mar 14, 2013</td>
									<td>23</td>
									<td>Error: +3 % <br> SD +14 %
									</td>
									<td><a class="btn btn-default btn-lg"
										href="<?php echo base_url("delivery/rundetail/");?>"> <i
											class="fa fa-eye"></i> View
									</a></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- /AUDITS -->


	</div>
</div>
<!-- /PAGE -->

<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>

