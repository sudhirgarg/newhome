<?php
/* 
------------------
Language: Simplified Chinese 
------------------
*/

// Menu

$lang['MENU_HOMES'] = '护理组';
$lang['MENU_CLIENTS'] = '客户资料';
$lang['MENU_TICKETS'] = '服务问询';
$lang['MENU_EVENTS'] = '健康监测';
$lang['MENU_COVERAGE_INFO'] = '药品保险信息';
$lang['MENU_SITE_MAP'] = '网站地图';
$lang['MENU_PHARMACY_INFO'] = '药房信息';


//TOP MENU
$lang['MY_PROFILE'] = '我的账户';
$lang['LANGUAGE'] = '语言';
$lang['TERMS_OF_USE'] = '服务条款';
$lang['PRIVACY_SETTINGS'] = '隐私条例';
$lang['LOG_OUT'] = '退出系统';


// Homes Page

// homes table
$lang['HELLO']= '您好';
$lang['HOMES'] = '护理组';
$lang['ADDRESS'] = '地址';
$lang['CONTACT'] = '联络人';
$lang['PHONE'] = '联络电话';


// CLIENTS

//table
$lang['TITLE'] = '客户资料';
$lang['BACK_TO_HOMES'] = '退到首页';
$lang['CLIENT'] = '客户';
$lang['HOME'] = '护理组';
$lang['TICKETS'] = '服务问询';
$lang['MED CHANGES'] = '药物变更';
$lang['BILLING'] = '健康监测';

// Tickets

$lang['GENERAL'] = '一般问询';
$lang['RE_ORDER'] = '再次订购';
$lang['PHARMACIST'] = '询问药剂师';
$lang['INTERNAL'] = '内部通讯';
$lang['COMPLETE'] = '完成服务';

// Events

$lang['TITLE'] = '健康监测';
$lang['EVENTS_DASHBOARD'] = '健康信息监测表';
$lang['RECENT_ACTIVITY'] = '最近健康事件记录';
$lang['EVENT_TYPES'] = '本月监测的健康事件种类';
$lang['EVENTS_FOR_THIS_YEAR'] = '本年健康事件监测表';
$lang['ACTIVITY'] = '健康事件记录';

// Profile

$lang['CLIENT_PROFILE'] = ' 个体客户详细信息';
$lang['CLIENTS'] = '所有客户';
$lang['NEW_TICKETS'] = '新的服务问询';
$lang['HEALTH_PROFILE'] = '客户健康监测信息';
$lang['PRINT_PATIENT_PROFILE'] = '打印客户的详细用药信息';
$lang['PROFILE'] = '客户信息';
$lang['RE_ORDERS'] = '再次订购';
$lang['SERVICE_TICKETS'] = '服务问询';
$lang['MED_CHANGES'] = '用药变更';
$lang['ALLERGIES'] = '过敏史';
$lang['CONDITIONS'] = '健康状况';
$lang['NOTE'] = '备注';
$lang['HOME'] = '护理组';
$lang['DELIVERY'] = '配送';
$lang['COMPLIANCE_PACKAGE'] = '目前用药';
$lang['MEDICATION'] = '药品名称';
$lang['DOCTOR'] = '医生';
$lang['DIRECTIONS'] = '用药指南';
$lang['COMMENT'] = '留言备注';
$lang['SELECT'] = '选择';
$lang['CREATE_ORDER'] = '下订单';
$lang['CREATED'] = '下单时间';
$lang['COMPLETED_DATE'] = '完成时间';
$lang['CREATED_BY'] = '问询人';
$lang['TICKET_TYPE'] = '问询类别';
$lang['DESCRIPTION'] = '详细描述';
$lang['COMPLETE'] = '完成';
$lang['TECHNICIAN'] = '药房技师';
$lang['PHARMACIST'] = '药剂师';
$lang['STARTS_ENDS'] = '开始日期';
$lang['CHANGE'] = '用药变更描述';
$lang['DrugPlan'] = '受保计划';

