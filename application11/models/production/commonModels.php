
<?php

	class CommonModels extends CI_Model {
		
		
		
		public function __construct() {
		
			parent::__construct ();
		
		}

		// Standed functions Start
		
		public function addDataToDeliveryNotes($data){
			
			$d = $topicData = $this->db->insert('deliverynotes', $data); 
			if($d == TRUE)	{
				return $d;
			}
			
		}
		
		public function updateDataToDeliveryNotes($id,$data){
			
			$this->db->where('patId', $id);
			$d = $this->db->update('deliverynotes', $data); 
			if($d == TRUE)	{
				return $d;
			}
		}
		
				
		public function getAllDataFromDeliveryNotes(){
			$str ="SELECT deliverynotes.id, deliverynotes.patId, deliverynotes.noteForInternal, deliverynotes.deliveryNote, pat.LastName, pat.FirstName FROM deliverynotes
				LEFT JOIN pat 
				ON deliverynotes.patId = pat.ID";			
			return $query = $this->db->query($str)->result();	 
						
		}
		
		public function getAllInfoBaseOnId($id){
			$str ="SELECT deliverynotes.id, deliverynotes.patId, deliverynotes.noteForInternal, deliverynotes.deliveryNote, pat.LastName, pat.FirstName FROM deliverynotes
				LEFT JOIN pat 
				ON deliverynotes.patId = pat.ID WHERE deliverynotes.id=$id";
			return $query = $this->db->query($str)->row();
		}		
		
		public function findDuplicate($data){
			// $data has to be an array
			$this->db->where($data);
			return $query = $this->db->get('deliverynotes')->num_rows();
				
		}
		
		// Standed functions Start End
		
		
		public function getNonPacMedTicketInfoBaseOnPatId($patId){
			$str ="SELECT eboard.id, eboard.dateOfDelivery, eboard.userID, tickets.rxnumList, tickets.Description FROM eboard LEFT JOIN tickets ON eboard.ticketID = tickets.ticketID WHERE eboard.type = 1 AND tickets.patID =$patId ORDER BY dateOfDelivery DESC";
			return $query = $this->db->query($str)->result();
			
		}
		
		public function getNonPacMedMedChangeInfoBaseOnPatId($patId){
			$str ="SELECT eboard.id, eboard.dateOfDelivery, eboard.userID, medchanges.changeText FROM eboard LEFT JOIN medchanges ON eboard.ticketID = medchanges.medChangeID WHERE eboard.type = 2 AND medchanges.patID =$patId ORDER BY dateOfDelivery DESC";
			return $query = $this->db->query($str)->result();
		}
		
		/***************************************************************************************************/
		/***************************************************************************************************/
		/*************************************** Pack MED **************************************************/
		/***************************************************************************************************/
		/***************************************************************************************************/
		
		public function addPackMed($data){
			$d = $this -> db -> insert('pacmed',$data);
			if($d == true){ return $d; }
		}
		
		public function updatePackMed($id,$data){
			$this -> db -> where('id',$id);
			$d = $this -> db -> update('pacmed',$data);
			if($d == true){ return $d; }
		}
		
		public function getRecordBaseOnOrderId($oId){			
			$str = "SELECT * FROM pacmed WHERE orderNum = '".$oId."'";
			return $query = $this->db->query($str)->row();
		}
		
		public function patInfoBaseOnPatId($id) {
			$str = "SELECT * FROM pat WHERE ID =".$id;
			return $query = $this->db->query($str)->row();
		}
		
		public function getAllInfoBaseOnBatchNum($batchNum){
			$str ="SELECT * FROM pacmed WHERE batchNum =".$batchNum;
			return $query = $this->db->query($str)->result();
		}
		
		public function getQueueOfPacMed(){
			$str ="SELECT COUNT(*) num FROM pacmed WHERE orderNum NOT IN (SELECT orderId FROM pacvision)";
			return $query = $this->db->query($str)->row();
			
		}
		
		public function getAllInfoFromPacMed(){
			$str = "SELECT * FROM pacmed WHERE orderNum NOT IN (SELECT orderId FROM pacvision)";
			return $query = $this->db->query($str)->result();
		}
		
		public function getInfoToEboard(){
			$str = "SELECT * FROM pacmed WHERE orderNum NOT IN (SELECT ticketID FROM eboard WHERE type = 3)";
			return $query = $this->db->query($str)->result();
		}
		
		public function getBatchNumberBaseOnPatId($patId){
			$str = "SELECT * FROM pacmed WHERE listOfPatId LIKE '%$patId%' ORDER BY deliveryDate DESC";
			return $query = $this->db->query($str)->result();
			
		}
		
		public function getAllBacklog(){
			$str ="SELECT * FROM pacmed WHERE deliveryDate < '2016-05-13' AND orderNum NOT IN (SELECT orderId FROM pacvision)";
			return $query = $this->db->query($str)->result();
		}
		/*************************************** Pack MED END ***********************************************/
		
		
		
		/***************************************************************************************************/
		/***************************************************************************************************/
		/*************************************** Pac Vision ************************************************/
		/***************************************************************************************************/
		/***************************************************************************************************/
		
		public function addPackVision($data){
			$d = $this -> db -> insert('pacvision',$data);
			if($d == true){ return $d; }
			
		}
		
		public function getRecordBaseOnOrderIdInPacVision($oId){
			$str = "SELECT * FROM pacvision WHERE orderId = '".$oId."'";
			return $query = $this->db->query($str)->row();
		}
       
		public function getQueueOfPacVision(){
			$str ="SELECT COUNT(*) num FROM pacvision WHERE orderId NOT IN (SELECT orderId FROM pharmacistcheck)";
			return $query = $this->db->query($str)->row();
				
		}
		
		public function getAllInfoFromPacVision(){
			$str ="SELECT pv.id, pv.orderId AS orderNum, pm.batchNum, pm.home, pm.deliveryDate, pm.DTime FROM pacvision AS pv
				   LEFT JOIN pacmed AS pm ON pm.orderNum = pv.orderId WHERE pv.orderId NOT IN (SELECT orderId FROM pharmacistcheck)";
			return $query = $this -> db -> query ($str)->result();
		}
		/*************************************** Pac Vision END *********************************************/
		
		
		
		/***************************************************************************************************/
		/***************************************************************************************************/
		/*************************************** Pharmacist ************************************************/
		/***************************************************************************************************/
		/***************************************************************************************************/
		
		public function addPharmacistCheck($data){
			$d = $this -> db -> insert('pharmacistcheck',$data);
			if($d == true){ return $d; }
		}
		
		public function getRecordBaseOnOrderIdInPharmacistcheck($oId){
			$str = "SELECT * FROM pharmacistcheck WHERE orderId = '".$oId."'";
			return $query = $this->db->query($str)->row();
		}
		
		public function getQueueOfPharmacistCheck(){
			$str ="SELECT COUNT(*) num FROM pharmacistcheck WHERE orderId NOT IN (SELECT orderId FROM productiondelivery)";
			return $query = $this->db->query($str)->row();
		
		}
		
				
		public function getAllInfoFrompharmacistcheck(){
			$str ="SELECT pc.id, pc.orderId AS orderNum, pm.batchNum, pm.home, pm.deliveryDate, pm.DTime, pm.event FROM pharmacistcheck AS pc
				   LEFT JOIN pacmed AS pm ON pm.orderNum = pc.orderId WHERE pc.orderId NOT IN (SELECT orderId FROM productiondelivery)";
			return $query = $this -> db -> query ($str)->result();
		}
		
		/*************************************** Parmacist END *********************************************/
		
		
		
		/***************************************************************************************************/
		/***************************************************************************************************/
		/*************************************** Delivery **************************************************/
		/***************************************************************************************************/
		/***************************************************************************************************/
		
		public function addDelivery($data){
			$d = $this -> db -> insert('productiondelivery',$data);
			if($d == true){ return $d; }
		}
		
		public function updateDelivey($id,$data){
			$this -> db -> where('id',$id);
			$d = $this -> db -> update('productiondelivery',$data);
			if($d == true){ return $d; }
		}
		
		public function getRecordBaseOnOrderIdInDelivery($oId){
			$str = "SELECT * FROM productiondelivery WHERE orderId = '".$oId."'";
			return $query = $this->db->query($str)->row();
		}
		
		public function getQueueOfDelivery(){
			$str ="SELECT COUNT(*) num FROM productiondelivery WHERE finalCheck = 0 AND orderId IN (SELECT ticketID FROM eboard WHERE type = 3 AND userID IS  NULL)";
			//$str ="SELECT COUNT(*) num FROM productiondelivery WHERE finalCheck = 0";
			return $query = $this->db->query($str)->row();		
		}
		
		public function getNumOfFinalCheck(){
			$str ="SELECT COUNT(*) num FROM productiondelivery WHERE finalCheck = 1 AND orderId IN (SELECT ticketID FROM eboard WHERE type = 3 AND userID IS  NULL)";
			return $query = $this->db->query($str)->row();
		
		}
		
		public function getAllInfoFromDelivery(){
			$str ="SELECT pd.id, pd.orderId AS orderNum, pd.finalCheck, pm.batchNum, pm.home, pm.deliveryDate, pm.DTime, pm.event FROM productiondelivery AS pd
				   LEFT JOIN pacmed AS pm ON pm.orderNum = pd.orderId
				   WHERE finalCheck = 0 AND pd.orderId IN (SELECT ticketID FROM eboard WHERE type = 3 AND userID IS  NULL)
					";
			return $query = $this -> db -> query ($str)->result();
		}
		
		public function getAllInfoFinalCheck(){
			$str ="SELECT pd.id, pd.orderId AS orderNum, pd.finalCheck, pm.batchNum, pm.home, pm.deliveryDate, pm.DTime, pm.event FROM productiondelivery AS pd
				   LEFT JOIN pacmed AS pm ON pm.orderNum = pd.orderId
				   WHERE finalCheck = 1 AND pd.orderId IN (SELECT ticketID FROM eboard WHERE type = 3 AND userID IS  NULL)
					";
			return $query = $this -> db -> query ($str)->result();
		}
		
		
		/*************************************** Delivery END *********************************************/
		
		
	}


?>