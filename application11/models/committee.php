<?php
class committee extends CI_Model {
	function __construct() {
		// Call the Model constructor
		parent::__construct ();
	}
	function updateCommmittee($committeeID = '-1', $committeeName = '', $committeeDescription = '', $active = '1') {
		if ($committeeID != '-1') { // Update Existing
			if ($active == 0) { // Delete
				$this->db->query ( "Delete from committees where committeeID = ?", array (
						$committeeID 
				) );
				return '1|Deleted committee: ' . $committeeID;
			} else { // Update
				$data = array (
						'committeeName' => $committeeName,
						'committeeDescription' => $committeeDescription 
				);
				$this->db->where ( 'committeeID', $committeeID );
				if ($this->db->update ( 'committees', $data )) {
					return '1|Successfully Updated Committee';
				} else {
					return '0|Unknown Error Occurred';
				}
			}
		} else { // Insert New Committee
			$data = array (
					'committeeName' => $committeeName,
					'committeeDescription' => $committeeDescription 
			);
			if ($this->db->insert ( 'committees', $data )) {
				return '1|Successfully Created ' . $committeeName;
			} else {
				return '0|Unknown Error Occurred';
			}
		}
	}
	function getCommittees() {
		$v = $this->db->query ( "Select * from committees" );
		if ($v->num_rows () > 0) {
			return $v;
		} else {
			return false;
		}
	}
	function getCommittee($committeeID) {
		$v = $this->db->query ( "Select * from committees where committeeID = ?", array (
				$committeeID 
		) );
		if ($v->num_rows () > 0) {
			return $v->row ();
		} else {
			$d = new stdClass ();
			$d->committeeID = '-1';
			$d->committeeName = '';
			$d->committeeDescription = '';
			return $d;
		}
	}
	function getCommitteeMeetings($committeeID) {
		return $this->db->query ( "select * from committeeMinutes where committeeID = ?", array (
				$committeeID 
		) );
	}
	function getMeetings() {
		$v = $this->db->query ( "Select * from committeeMinutes order by dateCreated DESC" );
		if ($v->num_rows () > 0) {
			return $v;
		} else {
			return false;
		}
	}
	function getMeeting($meetingID = -1) {
		$v = $this->db->query ( "Select * from committeeMinutes where committeeMinutesID = ?", array (
				$meetingID 
		) );
		
		if ($v->num_rows () > 0) {
			return $v->row ();
		} else {
			$d = new stdClass ();
			$d->committeeMinutesID = '-1';
			$d->dateCreated = date ( 'Y-m-d' );
			$d->title = '';
			$d->present = '';
			$d->discussion = '';
			
			return $d;
		}
	}
	function getLastMeetingTimeString($committeeID) {
		$LastMins = $this->db->query ( "Select * from committeeMinutes where committeeID = ? order by dateCreated DESC LIMIT 0, 1", array (
				$committeeID 
		) );
		if ($LastMins->num_rows () > 0) {
			return date ( 'Y-m-d', strtotime ( $LastMins->row ()->dateCreated ) );
		} else {
			return "Never";
		}
	}
	function updateMeetingMinutes($committeeMinutesID = '-1', $dateCreated = '', $title = '', $committeeID = '-1', $present = '', $discussion = '', $active = '1') {
		if ($committeeMinutesID > 0) { // Update Minutes
			if ($active == 0) { // Delete Minutes
				$this->db->query ( "Delete from committeeMinutes where committeeMinutesID = ?", array (
						$committeeMinutesID 
				) );
				return '1|Successfully Deleted Minutes';
			} else { // Update Minutes
				$data = array (
						'dateCreated' => $dateCreated,
						'title' => $title,
						'present' => $present,
						'discussion' => $discussion,
						'committeeID' => $committeeID 
				);
				$this->db->where ( 'committeeMinutesID', $committeeMinutesID );
				if ($this->db->update ( 'committeeMinutes', $data )) {
					return '1|Successfully Updated Minutes';
				} else {
					return '0|Unknown Error Occurred';
				}
			}
		} else {
			// Create New Minutes
			$data = array (
					'dateCreated' => $dateCreated,
					'title' => $title,
					'present' => $present,
					'discussion' => $discussion,
					'committeeID' => $committeeID 
			);
			if ($this->db->insert ( 'committeeMinutes', $data )) {
				return '1|Saved Minutes';
			} else {
				return '0|Unknown Error Occurred';
			}
		}
	}
}
