<?php

	class eBookAndVideo extends CI_Model {
		
		/*
		 * Table Properties 
		   CREATE TABLE ebookandvideo (
				id INT NOT NULL AUTO_INCREMENT,
				name VARCHAR(100) NOT NULL,
		 		topicsId INT,
		  		images VARCHAR(100),
				details LONGTEXT,
		        typeInfo INT,              // 1 => eBook, 2 => video
				addBy VARCHAR(100),
				addDate DATETIME,
				remark VARCHAR(100),
				PRIMARY KEY (id)
			);		
		 * 
		 */

		protected  $tableName ='ebookandvideo';
		protected  $where ="id";
		protected  $update ="";
		protected  $orderBy ="gQname";
		
		public function __construct() {
		
			parent::__construct ();
		
		}

		// Standed functions Start
		
		public function addData($data){
			
			$d = $topicData = $this->db->insert($this->tableName, $data); 
			if($d == TRUE)	{
				return $d;
			}
			
		}
		
		public function updateData($id,$data){
			
			$this->db->where($this->where, $id);
			$d = $this->db->update($this->tableName, $data); 
			if($d == TRUE)	{
				return $d;
			}
		}
		
		public function deleteData($id){
			
			$this->db->where($this->where, $id);
			$this->db->delete($this->tableName); 
			
		}
		
		public function getAllData(){
			$this->db->order_by($this->orderBy, "ASC"); 			
			return $query = $this->db->get($this->tableName)->result();	 
						
		}
		
		public function getAllDataBaseOnId($data){
									
			$this->db->where($this->where, $data);			
			return $query = $this->db->get($this->tableName)->result();	 
						
		}
		
		public function findDuplicate($data){
			// $data has to be an array
			$this->db->where($data);
			return $query = $this->db->get($this->tableName)->num_rows();
				
		}
		
		public function fetchDataForPagination($limit, $start) {
	        $this->db->limit($limit, $start);
	        $query = $this->db->get($this->tableName);
	
	        if ($query->num_rows() > 0) {
	            foreach ($query->result() as $row) {
	                $data[] = $row;
	            }
	            return $data;
	        }
	        return false;
   		}
		
		
		// Standed functions Start End
		
		
		public function getAllDataBaseOnTypeInFo($data){
									
			$this->db->where('typeInfo', $data);
			$this->db->order_by("name", "ASC"); 			
			return $query = $this->db->get($this->tableName)->result();	 
						
		}
		
		public function getAllDataBaseOnTopicsId($data,$userId){
									
			//$this->db->where('topicsId', $data);	
			//$this->db->order_by("name", "ASC"); 		
			//return $query = $this->db->get($this->tableName)->result();	 
			
			$str = "SELECT * FROM ebookandvideo AS ev
				LEFT JOIN
				(
				   SELECT  distinct ebookandvideoId FROM eduusertrack WHERE userId = $userId
				)  AS track ON track.ebookandvideoId = ev.id
				WHERE ev.topicsId =$data";
			return $this -> db -> query($str) -> result();
						
		}
		
		public function getIntroVideo($data,$userId){
			/*
			$this -> db -> where('topicsId',$data);
			$this -> db -> where('typeInfo',2);
			return $query = $this->db->get($this->tableName)->row();
			*/
			
			$str = "SELECT * FROM ebookandvideo AS ev
			LEFT JOIN
			(
			SELECT  distinct ebookandvideoId FROM eduusertrack WHERE userId = $userId
			)  AS track ON track.ebookandvideoId = ev.id
			WHERE ev.typeInfo =2 AND ev.topicsId =$data";
			return $this -> db -> query($str) -> row();
		}
		
	    public function getTopicsGroupInList(){
	    	
			$this->db->select('ebookandvideo.topicsId, count(ebookandvideo.id) AS numberOftopic,  topics.topicsName');
			$this->db->from('ebookandvideo');
			$this->db->join('topics', 'ebookandvideo.topicsId = topics.id', 'left');
			$this->db->group_by("ebookandvideo.topicsId"); 
			$this->db->order_by("topics.topicsName", "ASC"); 
			
			return $query = $this->db->get()->result();		
	    	
			
	    } 
		
		public function getEbookDataAndTopicBaseOnEbookId($data){
									
			$this->db->where($this->where, $data);			
			return $query = $this->db->get($this->tableName)->result();	 
						
		}
		
		
		public function getLession($tId,$lId,$userId){
			$str = "SELECT * FROM ebookandvideo AS ev
				LEFT JOIN
				(
				SELECT  distinct ebookandvideoId FROM eduusertrack WHERE userId = $userId
				)  AS track ON track.ebookandvideoId = ev.id
				WHERE ev.topicsId =$tId AND ev.id=$lId";
			
			return $this -> db -> query($str) -> row();
		}
		
		public function getUnWatchedVideo($userId,$topicId){
			$str = "SELECT COUNT(*)  AS num FROM ebookandvideo WHERE id NOT IN (SELECT ebookandvideoId FROM eduusertrack WHERE userId = $userId ) AND topicsId =$topicId";
			return $this -> db -> query($str) -> row();
		}
		
		
		
		/****************************************************************************/
		/****************************************************************************/
		/************************  eduUserTrack   ***********************************/
		/****************************************************************************/
		/****************************************************************************/
		
		public function addEduUserTrack($data){
			 $d = $this -> db -> insert('eduusertrack',$data);
			 if($d) { return $d; }
			 
		}
		
		
		public function findUserStartCourse($id,$userId){
			$str = "SELECT * FROM eduusertrack WHERE topicsId IN ($id) AND userId = $userId";
			return $query = $this -> db -> query($str) -> result();
		}
		
		public function getResultAndStatus($id,$userId){
			$str = "SELECT * FROM candidate WHERE generalQuizInfoId = (SELECT id FROM generalquizzesinfo WHERE topicsId = $id LIMIT 1) AND userName = $userId AND score >= 0";
			return $this -> db -> query($str) -> row();					
		}
		
		


	}


?>