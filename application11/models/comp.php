<?php
class comp extends CI_Model {
	function __construct() {
		// Call the Model constructor
		parent::__construct ();
	}
	public function getCompliancePatient($patID = '-1') {
		$patientRows = $this->db->query ( 
		"SELECT c.patID, c.patInitials, homes.homeName  
			FROM compliancepatient AS c 
			LEFT JOIN pat_home
			ON pat_home.patID = c.patID
			LEFT JOIN homes
			ON homes.homeID = pat_home.homeID 
			where c.patID = ?", array (
				$patID 
		) );
		if ($patientRows->num_rows () > 0) {
			return $patientRows->row ();
		} else {
			return false;
		}
	}
	public function getCompliancePatients() {
		$patientRows = $this->db->query ( "Select * from compliancePatient" );
		if ($patientRows->num_rows () > 0) {
			return $patientRows;
		} else {
			return false;
		}
	}
	public function addNewCompliancePatient($patID, $patInitial) {
		// insert a new record in compliance patient
		$patCheck = TRUE;
		$v = $this->db->query ( "Select * from compliancepatient where patID = ?", array (
				$patID 
		) );
		if ($v->num_rows () > 0) {
			$patCheck = TRUE;
		} else {
			$patCheck = FALSE;
		}
		
		// check to see if patient exists
		if ($patCheck == FALSE) {
			$data = array (
					'patID' => $patID,
					'patInitials' => $patInitial,
					'deliver' => 0,
					'controlled' => 0,
					'deliveryTimeID' => 0,
					'deliveryHomeID' => 0 
			);
			
			if ($this->db->insert ( 'compliancepatient', $data )) {
				echo 'successfully added new patient ';
			} else {
				echo 'Failed to add new patient ';
			}
		} else {
			echo 'patID already exists';
		}
	}
	public function updateCompliancePatient($patID, $note = '', $deliveryInstructions = '', $deliver = '0', $controlled = '0', $deliveryTimeID = '0', $deliveryHomeID = '0') {
		$data = array (
				'note' => $note,
				'deliveryInstructions' => $deliveryInstructions,
				'deliver' => $deliver,
				'controlled' => $controlled,
				'deliveryTimeID' => $deliveryTimeID,
				'deliveryHomeID' => $deliveryHomeID 
		);
		$this->db->where ( 'patID', $patID );
		if ($this->db->update ( 'compliancePatient', $data )) {
			return '1|Successfully Updated Compliance Patient';
		} else {
			return '0|Database Error Occurred';
		}
	}

	
	public function updateRankTicket($id, $data) {
		
		$this->db->where ( 'ticketId', $id );
		$this->db->update ('tickets', $data );
	}
	
	public function deleteTicket($id) {
		
		$this->db->where ( 'ticketId', $id );
		$this->db->delete ('tickets' );
	}
	
	public function getMedChangesAwaitingPharmacist() {
		// returns medchanges with active=1, pharmacistid is empty
		$v = $this->db->query ( "SELECT mc.medChangeID, mc.patID, mc.dateOfChange, mc.changeText, mc.actionText, mc.technicianUserID, mc.pharmacistUserID, mc.active, mc.startDate, mc.endDate, mc.actionComplete, pat.LastName , pat.FirstName, pat.NHWardID, pat.NHID, pat.DeliveryRoute FROM medChanges AS mc
				LEFT JOIN pat
				ON pat.ID = mc.patID
				where mc.active = 1 and mc.pharmacistUserID = 0 ORDER BY mc.medChangeID DESC" );
		if ($v->num_rows () > 0) {
			return $v;
		} else {
			return false;
		}
	}
	public function getMedChangesAwaitingAction() {
		// returns medchanges with active=1, and where pharmacistid is NOT empty ie its been checked by a pharmacist, and actionComplete= FALSE (you need a new collumn for actionComplete
		$v = $this->db->query ( "SELECT mc.medChangeID, mc.patID, mc.dateOfChange, mc.changeText, mc.actionText, mc.technicianUserID, mc.pharmacistUserID, mc.active, mc.startDate, mc.endDate, mc.actionComplete, pat.LastName , pat.FirstName, pat.NHWardID, pat.NHID, pat.DeliveryRoute FROM medChanges AS mc
				LEFT JOIN pat
				ON pat.ID = mc.patID
				where mc.active = 1 and mc.pharmacistUserID > 0 and mc.actionComplete = 0 ORDER BY mc.medChangeID DESC" );
				
		//$v = $this->db->query ( "Select * from medChanges where active = 1 and pharmacistUserID > 0  and actionComplete = 0" );
		
		if ($v->num_rows () > 0) {
			return $v;
		} else {
			return false;
		}
	}
	public function getMedChangesInProgress() {
		//$v = $this->db->query ( "Select * from medChanges where active = 1 and pharmacistUserID > 0 and actionComplete = 2" );
		$v = $this->db->query ( "SELECT mc.medChangeID, mc.patID, mc.dateOfChange, mc.changeText, mc.actionText, mc.technicianUserID, mc.pharmacistUserID, mc.active, mc.startDate, mc.endDate, mc.actionComplete, pat.LastName , pat.FirstName, pat.NHWardID, pat.NHID, pat.DeliveryRoute FROM medChanges AS mc
				LEFT JOIN pat
				ON pat.ID = mc.patID
				where mc.active = 1 and mc.pharmacistUserID > 0 and mc.actionComplete = 2 ORDER BY mc.medChangeID DESC" );
		if ($v->num_rows () > 0) {
			return $v;
		} else {
			return false;
		}
	}
	
	// Dave- can you also include a patID with this function that if passed will return the medchanges for that patid that are active=1
	public function getMedChange($medChangeID = '-1', $patID = '-1') {
		// returns all med changes (ordered by dateOfChange) if null passed in
		if ($medChangeID > 0) {
			$v = $this->db->query ( "Select * from medChanges where active = 1 and medChangeID = ? ", array (
					$medChangeID 
			) );
			if ($v->num_rows () > 0) {
				return $v->row ();
			} else {
				$d = new stdClass ();
				$d->medChangeID = '-1';
				$d->patID = '-1';
				$d->dateOfChange = date ( 'Y-m-d' );
				$d->changeText = '';
				$d->actionText = '';
				$d->technicianUserID = '';
				$d->pharmacistUserID = '';
				$d->active = '1';
				$d->startDate = date ( 'Y-m-d' );
				$d->endDate = '';
				$d->actionComplete = 0;
				
				return $d;
			}
		} else {
			if ($patID > 0) {
				return $this->db->query ( "Select * from medChanges where active = 1 and patID = ? order by dateOfChange DESC", array (
						$patID 
				) );
			} else {
				return $this->db->query ( "Select * from medChanges where active = 1 order by dateOfChange DESC" );
			}
		}
	}

	public function updateMedchange(
			$medChangeID = '-1', 
			$patID, 
			$dateOfChange, 
			$changeText = '', 
			$actionText = '', 
			$technicianUserID = '-1', 
			$pharmacistUserID = '-1', 
			$active = '1', 
			$startDate = '-1', 
			$endDate = '-1', 
			$actionComplete = '0',
			$ad =''
									) {
		if ($medChangeID > 0) { // Update Existing
			if ($active > 0) { // Update Existing
				$data = array (
						'dateOfChange' => $dateOfChange,
						'changeText' => $changeText,
						'actionText' => $actionText,
						'technicianUserID' => $technicianUserID,
						'pharmacistUserID' => $pharmacistUserID,
						'active' => '1',
						'startDate' => $startDate,
						'endDate' => $endDate,
						'actionComplete' => $actionComplete
				);				
				
				$this->db->where ( 'patID', $patID );
				$this->db->where ( 'medChangeID', $medChangeID );
				
				if ($this->db->update ( 'medChanges', $data)) {
					
					if(!empty($ad)){
						$this->db->where ( 'patID', $patID );
						$this->db->where ( 'medChangeID', $medChangeID );
						$this->db->update ( 'medChanges', $ad);
					}
					
					return '1|Successfully Updated Med Change';
					
				} else {
					return '0|Database Error';
				}
			} else { // Delete Med Change
				$data = array (
						'active' => '0' 
				);
				$this->db->where ( 'medChangeID', $medChangeID );
				$this->db->where ( 'patID', $patID );
				if ($this->db->update ( 'medChanges', $data )) {
					return '1|Successfully Deleted Med Change';
				} else {
					return '0|Database Error';
				}
			}
		} else { // add new med change
			if ($technicianUserID == '-1') {
				$technicianUserID = $this->ion_auth->user ()->row ()->id;
			}
			
			$data = array (
					'patID' => $patID,
					'dateOfChange' => $dateOfChange,
					'changeText' => $changeText,
					'actionText' => $actionText,
					'technicianUserID' => $technicianUserID,
					'pharmacistUserID' => $pharmacistUserID,
					'active' => '1',
					'startDate' => $startDate,
					'endDate' => $endDate,
					'actionComplete' => '0' 
			);
			if ($this->db->insert ( 'medChanges', $data )) {
				return '1|Successfully Created Med Change';
			} else {
				return '0|Database Error, Please Try Again';
			}
		}
	}

	public function getMedChangeCount($patID = '-1') { // REturns the number of active medChanges
		if ($patID > 0) {
			return $this->db->query ( "Select COUNT(*) as CNT from medChanges where active = 1 and patID= ?", array (
					$patID 
			) )->row ()->CNT;
		} else {
			return $this->db->query ( "Select COUNT(*) as CNT from medChanges where active = 1" )->row ()->CNT;
		}
	}
	
	public function getProductionTimeReference($timeRefID = '0') {
		if ($timeRefID == 0) {
			// Return all production time references
			$v = $this->db->query ( "select * from productionTimeReference" );
			return $v;
		} else {
			if ($timeRefID == '-1') {
				$d = new stdClass ();
				
				$d->productionTimeID = '-1';
				$d->productionDay = '1';
				$d->productionTime = '1000';
				return $d;
			} else {
				$v = $this->db->query ( "Select * from productionTimeReference where productionTimeID = ?", array (
						$timeRefID 
				) );
				if ($v->num_rows () > 0) {
					return $v->row ();
				} else {
					return $this->comp->getProductionTimeReference ( '-1' );
				}
			}
		}
	}
	public function updateProductionTimeReference($productionTimeID = '-1', $productionDay = '1', $productionTime = '1000', $active = '1') {
		if ($productionTimeID > 0) { // Update existing
			if ($active == '0') { // Delete production time reference
				$this->db->query ( "Delete from productionTimeReference where productionTimeID = ?", array (
						$productionTimeID 
				) );
				return '1|Successfully deleted production order';
			} else { // Update existing time reference
				$data = array (
						'productionDay' => $productionDay,
						'productionTime' => $productionTime 
				);
				$this->db->where ( 'productionTimeID', $productionTimeID );
				if ($this->db->update ( 'productionTimeReference', $data )) {
					return '1|Successfully Updated Production Time Reference';
				} else {
					return '0|Error Updating Production Reference';
				}
			}
		} else {
			// Create New Production Time
			$data = array (
					'productionDay' => $productionDay,
					'productionTime' => $productionTime 
			);
			if ($this->db->insert ( 'productionTimeReference', $data )) {
				return '1|Successfully Created Production Time';
			} else {
				return '0|Error Creating Production Time';
			}
		}
	}
	public function getProductionOrderErrorType($productionOrderErrorTypeID = '0') {
		if ($productionOrderErrorTypeID == 0) {
			// Return all production time references
			$v = $this->db->query ( "select * from productionOrderErrorTypeReference" );
			return $v;
		} else {
			if ($productionOrderErrorTypeID == '-1') {
				$d = new stdClass ();
				
				$d->productionOrderErrorTypeID = '-1';
				$d->productionOrderErrorTypeComment = '';
				
				return $d;
			} else {
				$v = $this->db->query ( "Select * from productionOrderErrorTypeReference where productionOrderErrorTypeID = ?", array (
						$productionOrderErrorTypeID 
				) );
				if ($v->num_rows () > 0) {
					return $v->row ();
				} else {
					return $this->comp->getProductionTimeReference ( '-1' );
				}
			}
		}
	}
	public function updateProductionOrderErrorTypeReference($productionOrderErrorTypeID = '-1', $productionOrderErrorTypeComment = '', $active = '1') {
		if ($productionOrderErrorTypeID > 0) { // Update existing
			if ($active == '0') { // Delete production time reference
				$this->db->query ( "Delete from productionOrderErrorTypeReference where productionOrderErrorTypeID = ?", array (
						$productionOrderErrorTypeID 
				) );
				return '1|Successfully deleted Production Order Error';
			} else { // Update existing time reference
				$data = array (
						'productionOrderErrorTypeComment',
						$productionOrderErrorTypeComment 
				);
				$this->db->where ( 'productionOrderErrorTypeID', $productionOrderErrorTypeID );
				if ($this->db->update ( 'productionOrderErrorTypeReference', $data )) {
					return '1|Successfully Updated Production Error Type Reference';
				} else {
					return '0|Error Updating Production Error Type Reference';
				}
			}
		} else {
			// Create New Production Order Error Type Reference
			$data = array (
					'productionOrderErrorTypeComment',
					$productionOrderErrorTypeComment 
			);
			if ($this->db->insert ( 'productionOrderErrorTypeReference', $data )) {
				return '1|Successfully Created Production Error Type';
			} else {
				return '0|Error Creating Production Order Error Type';
			}
		}
	}
	public function getProductionOrderError($productionOrderErrorID = '0') {
		if ($productionOrderErrorID == 0) {
			// Return all production time references
			$v = $this->db->query ( "select * from productionOrderError" );
			return $v;
		} else {
			if ($productionOrderErrorID == '-1') {
				$d = new stdClass ();
				$d->productionOrderErrorID = '-1';
				$d->productionOrderID = '';
				$d->productionOrderErrorType = '';
				$d->comment = '';
				$d->reportedBy = $this->ion_auth->user ()->row ()->id;
				$d->status = '0';
				$d->reportedDate = date ( 'Y-m-d' );
				$d->din = '';
				
				return $d;
			} else {
				$v = $this->db->query ( "Select * from productionOrderError where productionOrderErrorID = ?", array (
						$productionOrderErrorID 
				) );
				if ($v->num_rows () > 0) {
					return $v->row ();
				} else {
					return $this->comp->getProductionOrderError ( '-1' );
				}
			}
		}
	}
	public function updateProductionOrderError($productionOrderErrorID = '-1', $productionOrderID = '', $productionOrderErrorType = '', $comment = '', $reportedBy = '-1', $status = '0', $reportedDate = '-1', $din = '', $active = '1') {
		if ($productionOrderErrorID > 0) { // Update existing
			if ($active == '0') { // Delete production time reference
				$this->db->query ( "Delete from productionOrderError where productionOrderError = ?", array (
						$productionOrderErrorID 
				) );
				return '1|Successfully Deleted Production Order Error';
			} else { // Update existing time reference
				$data = array (
						'productionOrderID',
						$productionOrderID,
						'productionOrderErrorType' => $productionOrderErrorType,
						'comment' => $comment,
						'reportedBy' => $reportedBy,
						'status' => $status,
						'reportedDate' => $reportedDate,
						'din' => $din 
				);
				$this->db->where ( 'productionOrderErrorID', $productionOrderErrorID );
				if ($this->db->update ( 'productionOrderError', $data )) {
					return '1|Successfully Updated Production Error';
				} else {
					return '0|Error Updating Production Error';
				}
			}
		} else {
			// Create New Production Order Error Type Reference
			$data = array (
					'productionOrderID',
					$productionOrderID,
					'productionOrderErrorType' => $productionOrderErrorType,
					'comment' => $comment,
					'reportedBy' => $reportedBy,
					'status' => $status,
					'reportedDate' => $reportedDate,
					'din' => $din 
			);
			if ($this->db->insert ( 'productionOrderError', $data )) {
				return '1|Successfully Created Production Error';
			} else {
				return '0|Error Creating Production Order Error';
			}
		}
	}
	public function getHome($homeID = '0') {
		if ($homeID != 0) {
			$check = $this->db->query ( "Select * from homes where homeID = ?", array (
					$homeID 
			) );
			if ($check->num_rows () > 0) {
				return $check->row ();
			} else {
				$d = new stdClass ();
				$d->homeID = '-1';
				$d->homeName = '';
				$d->homeAddress = '';
				$d->contact = '';
				$d->phone = '';
				
				return $d;
			}
		} else {
			$check = $this->db->query ( "Select * from homes" );
			if ($check->num_rows () > 0) {
				return $check;
			} else {
				return false;
			}
		}
	}
	public function updateHome($homeID = '-1', $homeName = '', $homeAddress = '', $contact = '', $phone = '', $active = '1') {
		if ($homeID > 0) { // Update Existing Home
			if ($active == 0) {
				// Delete home
				$this->db->query ( 'Delete from homes where homeID = ?', array (
						$homeID 
				) );
				return '1|Successfully Deleted Home';
			} else {
				// Update Fields
				$data = array (
						'homeName' => $homeName,
						'homeAddress' => $homeAddress,
						'contact' => $contact,
						'phone' => $phone 
				);
				
				if (strlen ( $homeName ) > 0) {
					$this->db->where ( 'homeID', $homeID );
					if ($this->db->update ( 'homes', $data )) {
						return '1|Successfully Saved Home';
					} else {
						return '0|Error Saving Home, Please try again';
					}
				} else {
					return '0|Home Name must have a value';
				}
			}
		} else {
			// Create new home
			$data = array (
					'homeName' => $homeName,
					'homeAddress' => $homeAddress,
					'contact' => $contact,
					'phone' => $phone 
			);
			if (strlen ( $homeName ) > 0) {
				if ($this->db->insert ( 'homes', $data )) {
					return '1|Successfully Created Home';
				} else {
					return '0|Unknown Error Occurred, Please try again';
				}
			} else {
				return '0|Home Name must have a value';
			}
		}
	}
	public function getPatHomes($patID = '0', $homeID = '0') {
		// Pass in home, to get all pats in home,
		// pass in pat to get homeID/false of pat has home,
		// pass in pat and home to get true/false of pat is in home
		if (($patID == 0) && ($homeID > 0)) {
			$c = $this->db->query ( "Select * from compliancePatient where patID IN (Select patID from pat_home where homeID = ?)", array (
					$homeID 
			) );
			if ($c->num_rows () > 0) {
				return $c;
			} else {
				return false;
			}
		}
		
		if (($patID > 0) && ($homeID == '0')) {
			$c = $this->db->query ( "Select homeID from homes where homeID IN (SELECT homeID from pat_home where patID = ?)", array (
					$patID 
			) );
			if ($c->num_rows () > 0) {
				return $c->row ()->homeID;
			} else {
				return false;
			}
		}
		
		if (($patID > 0) && ($homeID > 0)) {
			$c = $this->db->query ( "Select * from pat_home where patID = ? and homeID = ?", array (
					$patID,
					$homeID 
			) );
			if ($c->num_rows () > 0) {
				return true;
			} else {
				return false;
			}
		}
	}
	public function assignPatToHome($patID, $homeID) {
		$check = $this->comp->getPatHomes ( $patID, $homeID );
		if (! $check) {
			$data = array (
					'patID' => $patID,
					'homeID' => $homeID 
			);
			if ($this->db->insert ( 'pat_home', $data )) {
				return '1|Successfully Assigned Client to Home';
			} else {
				return '0|Error Assigning Client to Home';
			}
		} else {
			return '0|Patient is already member of that home';
		}
	}
	public function removePatFromHome($patID, $homeID = '0') { // pass in homeID to remove only from certain home
		if ($homeID > 0) { // specific group
			$this->db->query ( 'Delete from pat_home where patID = ? and homeID = ?', array (
					$patID,
					$homeID 
			) );
			return '1|Removed Client from Home';
		} else {
			// all groups
			$this->db->query ( 'Delete from pat_home where patID = ?', array (
					$patID 
			) );
			return '1|Removed Client from Home(s)';
		}
	}
	public function getTicketTypeReference($ticketTypeReferenceID = '-1') { // pass variable in if you want a specific string, otherwise will display all
		if ($ticketTypeReferenceID > 0) {
			$v = $this->db->query ( "Select * from ticketTypeReference where ticketTypeReferenceID = ?", array (
					$ticketTypeReferenceID 
			) );
			if ($v->num_rows () > 0) {
				return $v->row ()->ticketTypeReferenceType;
			} else {
				return 'Error';
			}
		} else {
			$v = $this->db->query ( "Select * from ticketTypeReference WHERE ticketTypeReferenceID != 4" );
			return $v;
		}
	}
	public function getTicket($ticketID) {
		$v = $this->db->query ( "Select * from tickets where ticketID = ? order by dateCreated DESC", array (
				$ticketID 
		) );
		if ($v->num_rows () > 0) {
			return $v->row ();
		} else {
			return false;
		}
	}
	public function getTodayCompleteTickets() {
		$v = $this->db->query ( "select * from tickets WHERE dateComplete > DATE_ADD(CURDATE(), INTERVAL -15 DAY)  order by dateComplete DESC" );
		if ($v->num_rows > 0) {
			return $v;
		} else {
			return false;
		}
	}
	public function getTickets($ticketTypeReferenceID = '-1', $active = '1', $pat = '-1') {
		if ($ticketTypeReferenceID > 0) {
			$v = $this->db->query ( "Select * from tickets where ticketTypeReferenceID = ? and active = ?  order by dateCreated DESC", array (
					$ticketTypeReferenceID,
					$active 
			) );
			if ($v->num_rows > 0) {
				return $v;
			} else {
				return false;
			}
		} elseif ($pat > 0) {
			$v = $this->db->query ("Select * from tickets where patID =$pat ORDER BY dateCreated DESC");
			if ($v->num_rows () > 0) {
				return $v;
			} else {
				return false;
			}
		} else {
			$v = $this->db->query ( "Select * from tickets where active = ? ORDER BY dateCreated DESC", array (
					$active 
			) );
			if ($v->num_rows () > 0) {
				return $v;
			} else {
				return false;
			}
		}
	}
	
	public function getTotalTicketsRankForMonth() {
				
			$v = $this->db->query ("
				SELECT SUM(rank) AS monthRank, COUNT(*) AS idTickets FROM tickets  
				WHERE  DATE_FORMAT(rankAddDate, '%m') = DATE_FORMAT(NOW(), '%m')
				AND rank != 'null'
			")->result() ;			
		
			
			return $v;
			
				 
			
	}
	
	public function getTotalTicketsRankForWeeks() {
				
			$v = $this->db->query ("
				SELECT SUM(rank) AS weekRank, COUNT(*) AS idTickets FROM tickets  
				WHERE  DATE_FORMAT(rankAddDate, '%w') = DATE_FORMAT(NOW(), '%w')
				AND  DATE_FORMAT(rankAddDate, '%d') = DATE_FORMAT(NOW(), '%d')
				AND rank != 'null'
			")->result();			
		
			
			return $v;
			 
			
	}
	
	public function getTotalTicketsRankForDay() {
				
			$v = $this->db->query ("
				SELECT SUM(rank) AS dayRank, COUNT(*) AS idTickets FROM tickets  
				WHERE  DATE_FORMAT(rankAddDate, '%m') = DATE_FORMAT(NOW(), '%m')
				AND  DATE_FORMAT(rankAddDate, '%w') = DATE_FORMAT(NOW(), '%w')
				AND  DATE_FORMAT(rankAddDate, '%d') = DATE_FORMAT(NOW(), '%d')
				AND rank != 'null'
			")->result();			
		
			
		   return $v;
		 
			
	}
	
	public function updateTicket($ticketID = '-1', $patID, $ticketTypeReferenceID, $description, $active,$rx,$dd,$batchNum) {
		if ($ticketID > 0) { // Existing Ticket
			if ($active > 0) { // Just updating ticket
				$data = array (
						'ticketTypeReferenceID' => $ticketTypeReferenceID,
						'Description' => $description,
						'rxnumList'    => $rx,
						'dateOfDelivery' => $dd,
						'batchNum'       => $batchNum
				);
				$this->db->where ( 'ticketID', $ticketID );
				if ($this->db->update ( 'tickets', $data )) {
					return '1|Successfully Updated Ticket';
				} else {
					return '0|Unknown Error';
				}
			} else { // Completing/closing ticket
				$data = array (
						'active' => '0',
						'dateComplete' => date ( 'Y-m-d H:i:s' ),
						'rxnumList'    => $rx,
						'dateOfDelivery' => $dd,
						'batchNum'       => $batchNum
				);
				$this->db->where ( 'ticketID', $ticketID );
				if ($this->db->update ( 'tickets', $data )) {
					return '1|Successfully Closed Ticket';
				} else {
					return '0|An unknown Error Occurred';
				}
			}
		} else { // New Ticket
			$data = array (
					'createdBy' => $this->ion_auth->user ()->row ()->id,
					'dateCreated' => date ( 'Y-m-d H:i:s' ),
					'patID' => $patID,
					'ticketTypeReferenceID' => $ticketTypeReferenceID,
					'Description' => $description,
					'active' => '1',
					'rxnumList' => $rx 
			);
			if ($this->db->insert ( 'tickets', $data )) {
				return '1|Successfully Created Ticket';
			} else {
				return '0|An unknown error occurred... Please try again';
			}
		}
	}
	public function getTicketCount($patID = '-1') { // REturns the number of active medChanges
		if ($patID > 0) {
			return $this->db->query ( "Select COUNT(*) as CNT from tickets where active = 1 and patID= ?", array (
					$patID 
			) )->row ()->CNT;
		} else {
			return $this->db->query ( "Select COUNT(*) as CNT from tickets where active = 1" )->row ()->CNT;
		}
	}
	public function getDiscussion($ticketID) {
		$v = $this->db->query ( "Select * from ticketDiscussion where ticketID = ? order by messageDate DESC", array (
				$ticketID 
		) );
		if ($v->num_rows () > 0) {
			return $v;
		} else {
			return false;
		}
	}
	public function updateDiscussion($ticketID, $comment) {
		$data = array (
				'ticketID' => $ticketID,
				'message' => $comment,
				'userID' => $this->ion_auth->user ()->row ()->id,
				'messageDate' => date ( 'Y-m-d H:i:s' ) 
		);
		if ($this->db->insert ( 'ticketDiscussion', $data )) {
			// $description = $this->ion_auth->user ()->row ()->first_name . ': ' . $comment;
			// $this->notifyTechs($description,0,$ticketID);
			return '1|Message Sent. Email Sent';
		} else {
			return '0|Error Sending Message, Please Try again';
		}
	}
	
	// user home
	public function getUserHomes($userID = '0', $homeID = '0') {
		// Pass in home, to get all pats in home,
		// pass in pat to get homeID/false of pat has home,
		// pass in pat and home to get true/false of pat is in home
		if (($userID == 0) && ($homeID > 0)) {
			$c = $this->db->query ( "Select * from users where id IN (Select userID from user_home where homeID = ?)", array (
					$homeID 
			) );
			if ($c->num_rows () > 0) {
				return $c;
			} else {
				return false;
			}
		}
		
		if (($userID > 0) && ($homeID == '0')) {
			$c = $this->db->query ( "Select homeID from homes where homeID IN (SELECT homeID from user_home where userID = ?)", array (
					$userID 
			) );
			if ($c->num_rows () > 0) {
				return $c->row ()->homeID;
			} else {
				return false;
			}
		}
		
		if (($userID > 0) && ($homeID > 0)) {
			$c = $this->db->query ( "Select * from user_home where userID = ? and homeID = ?", array (
					$userID,
					$homeID 
			) );
			if ($c->num_rows () > 0) {
				return true;
			} else {
				return false;
			}
		}
	}

	public function getEmailAddressFromPatId($patId){
		$str = "SELECT pat_home.homeId, homes.homeName, user_home.userID, users.email
				FROM pat_home 
				LEFT JOIN homes
				ON homes.homeID = pat_home.homeId
				LEFT JOIN user_home
				ON homes.homeID = user_home.homeID
				LEFT JOIN users
				ON users.id = user_home.userID
				WHERE patId = $patId
				";
		return $query=$this->db->query($str)->result();		
		
	}

	public function assignUserToHome($userID, $homeID) {
		// $check = $this->comp->getUserHomes ( $userID, $homeID );
		$check = false;
		if (! $check) {
			$data = array (
					'userID' => $userID,
					'homeID' => $homeID 
			);
			if ($this->db->insert ( 'user_home', $data )) {
				return '1|Successfully Assigned user to Home';
			} else {
				return '0|Error Assigning user to Home';
			}
		} else {
			return '0|user is already member of that home';
		}
	}
	public function removeUserFromHome($userID, $homeID = '0') { // pass in homeID to remove only from certain home
		if ($homeID > 0) { // specific group
			$this->db->query ( 'Delete from user_home where userID = ? and homeID = ?', array (
					$userID,
					$homeID 
			) );
			return '1|removed user Client from Home';
		} else {
			// all groups
			$this->db->query ( 'Delete from user_home where userID = ?', array (
					$userID 
			) );
			return '1|Removed user from all Home(s)';
		}
	}
	public function getHomePortalUser($email = NULL) {
		if (isSet ( $email )) {
			$v = $this->db->query ( "Select id from users where email = ?", array (
					$email 
			) );
			if ($v->num_rows () > 0) {
				return $v->row ()->id;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	public function getUsersEmailsByTicketID($ticketID) {
		$v = $this->db->query ( "
				select distinct a.email from users a 	
left join user_home b on a.id = b.userID
LEFT JOIN pat_home c on c.homeID = b.homeID
LEFT JOIN tickets d on d.patID = c.patID
where d.ticketID = ?", array (
				$ticketID 
		) );
		if ($v->num_rows > 0) {
			return $v;
		} else {
			return false;
		}
	}
	
	public function notifyTechs($description, $typeID, $ticketID) {
		$c = $this->db->query ( "INSERT INTO notifications (userID, description, typeID, readIt, ticketID, timeStamp)
		VALUES(0, ?, ?, 0, ?, ?);
	", array (
				$description,
				$typeID,
				$ticketID,
				date ( 'Y-m-d H:i:s' ) 
		) );
		return true;
	}
	public function getNewComments() {
		$v = $this->db->query ( "
				select distinct a.ticketID, c.first_name, c.last_name, a.message, a.messageDate from ticketdiscussion a
				LEFT JOIN users c on a.userID=c.id
				Order by a.messageDate
				DESC
				LIMIT 40 
				", array (
		))->result();
		
		return $v;
		
	}
	
	public function getNewHomeComments() {
		$v = $this->db->query ( "
					select distinct a.ticketID, c.first_name, c.last_name, a.message, a.messageDate 
					from ticketdiscussion a
					LEFT JOIN users c on a.userID=c.id
					LEFT JOIN users_groups d on c.id = d.user_id
					RIGHT JOIN tickets on tickets.ticketID = a.ticketID
					WHERE
					d.group_id = 18
					Order by a.messageDate
					DESC
					LIMIT 10
				", array (
		) );
		if ($v->num_rows > 0) {
			return $v;
		} else {
			return false;
		}
	}

	
	

	
	
}