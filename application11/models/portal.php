<?php
class portal extends CI_Model {
	function __construct() {
		// Call the Model constructor
		parent::__construct();
	}

	public function getUserPatients($homeID = '0') {
		if ($homeID == '0') {
			$c = $this -> db -> query("
			Select * from compliancePatient a
left join pat_home b on a.PatID = b.patID
left join user_home c on b.homeID = c.homeID
WHERE c.userID = ?;
				", array($this -> ion_auth -> user() -> row() -> id));
			if ($c -> num_rows() > 0) {
				return $c;
			} else {
				return false;
			}
		} else {
			$c = $this -> db -> query("
			Select * from compliancePatient a
left join pat_home b on a.PatID = b.patID
left join user_home c on b.homeID = c.homeID
WHERE c.userID = ? and c.homeID = ?;
				", array($this -> ion_auth -> user() -> row() -> id, $homeID));
			if ($c -> num_rows() > 0) {
				return $c;
			} else {
				return false;
			}
		}
	}

	// view patient
	public function getCompliancePatient($patID = '-1') {
		//if ($this -> pharm -> userPatientAccess($patID)) {
			$patientRows = $this -> db -> query("Select * from compliancePatient where patID = ?", array($patID));
			if ($patientRows -> num_rows() > 0) {
				return $patientRows -> row();
			} else {
				return FALSE;
			}
		//} else {
			//return FALSE;
		//}
	}

	// Homes
	public function getHome($homeID = '0') {
		if ($homeID != 0) {// return a specific home
			$check = $this -> db -> query("Select * from homes where homeID = ?", array($homeID));
			if ($check -> num_rows() > 0) {
				return $check -> row();
			} else {
				$d = new stdClass();
				$d -> homeID = '-1';
				$d -> homeName = '';
				$d -> homeAddress = '';
				$d -> contact = '';
				$d -> phone = '';

				return $d;
			}
		} else {// return all users homes
			$check = $this -> db -> query("
					Select * from homes a
					LEFT JOIN user_home b on a.homeID=b.homeID WHERE b.userID=?", array($this -> ion_auth -> user() -> row() -> id));
			if ($check -> num_rows() > 0) {
				return $check;
			} else {
				return false;
			}
		}
	}

	public function getUserTickets($ticketTypeReferenceID = '-1') {
		if ($ticketTypeReferenceID > 0) {

			$last7Days = date('Y-m-d', strtotime('-7 days'));

			$c = $this -> db -> query("
			select * from tickets a 
left join pat_home b on a.patID = b.patID
left join user_home c on b.homeID = c.homeID
WHERE c.userID= ? AND ticketTypeReferenceID = ? AND ((a.active = 1) OR (a.dateComplete > ?));
				", array($this -> ion_auth -> user() -> row() -> id, $ticketTypeReferenceID, $last7Days));
			if ($c -> num_rows() > 0) {
				return $c;
			} else {
				return false;
			}
		} else {
		}
	}

	public function getUserTicketsCount() {
		return false;
	}

	// events
	public function getPatEvents($patID = 0, $eventID = 0) {
		
		$patId = "";
		
		$NHID = $this->ion_auth->user()->row()->NHID;
		$userId = $this->ion_auth->user()->row()->id;
		
	
		
		if ($NHID > 0) {
			
			$c = $this->rest->get('production/exApi/patInfoBaseOnNHID/'.$NHID);	
						
			if(!empty($c)){
				
				$List = array();
				foreach($c AS $recordCG){
					$List[] = $recordCG->ID;							
				}							
				
				$patId = implode(",",$List);	
								
			}
						
		}else{
			
			$this->load->model('assignPatToCG');
			
			$CGinfo = $this->assignPatToCG->getInfoBaseOnCGID($userId);
		
			if(!empty($CGinfo)){
				
				foreach($CGinfo AS $recordCG){
					$patId = $recordCG->patID;
				}				
						
			}
		}	
		
				
		if ($eventID == 0) {
			if ($patID > 0) {
				$c = $this -> db -> query("
				Select * from events where patID = ? order by eventDate DESC
				", array($patID));
				if ($c -> num_rows() > 0) {
					return $c;
				} else {
					return false;
				}
			} else {
				//$userID = $this -> ion_auth -> user() -> row() -> id;
				$c = $this -> db -> query("
				Select * from events a
					where a.patID IN ($patId) order by a.eventDate DESC
				");
				if ($c -> num_rows() > 0) {
					return $c;
				} else {
					return false;
				}
			}
		} else {
			$c = $this -> db -> query("
				Select * from events where id = ?
				", array($eventID));
			if ($c -> num_rows() > 0) {
				return $c -> row();
			} else {
				return false;
			}
		}
	}

	public function getPatEventsByType($patID = 0, $eventType) {
		if ($patID > 0) {
			$c = $this -> db -> query("
				select  
  sum(case when month = 1 then 1 else 0 end) as jan,
  sum(case when month = 2 then 1 else 0 end) as feb,
  sum(case when month = 3 then 1 else 0 end) as mar,
  sum(case when month = 4 then 1 else 0 end) as apr,
  sum(case when month = 5 then 1 else 0 end) as may,
  sum(case when month = 6 then 1 else 0 end) as jun,
  sum(case when month = 7 then 1 else 0 end) as jul,
  sum(case when month = 8 then 1 else 0 end) as aug,
  sum(case when month = 9 then 1 else 0 end) as sep,
  sum(case when month = 10 then 1 else 0 end) as oct,
  sum(case when month = 11 then 1 else 0 end) as nov,
  sum(case when month = 12 then 1 else 0 end) as dece
FROM ( SELECT extract(month from a.eventDate) as month FROM events a where a.patID =? AND  a.eventTypeID=? AND YEAR(a.eventDate) = YEAR(CURDATE())) patEvents											
				", array($patID, $eventType));
			if ($c -> num_rows() > 0) {
				return $c -> row();
			} else {
				return false;
			}
		} else {
			$userID = $this -> ion_auth -> user() -> row() -> id;
			$c = $this -> db -> query("
				select  
  sum(case when month = 1 then 1 else 0 end) as jan,
  sum(case when month = 2 then 1 else 0 end) as feb,
  sum(case when month = 3 then 1 else 0 end) as mar,
  sum(case when month = 4 then 1 else 0 end) as apr,
  sum(case when month = 5 then 1 else 0 end) as may,
  sum(case when month = 6 then 1 else 0 end) as jun,
  sum(case when month = 7 then 1 else 0 end) as jul,
  sum(case when month = 8 then 1 else 0 end) as aug,
  sum(case when month = 9 then 1 else 0 end) as sep,
  sum(case when month = 10 then 1 else 0 end) as oct,
  sum(case when month = 11 then 1 else 0 end) as nov,
  sum(case when month = 12 then 1 else 0 end) as dece
FROM ( SELECT extract(month from a.eventDate) as month FROM events a 
				LEFT JOIN pat_home b on a.patID=b.patID
					LEFT JOIN user_home c on b.homeID = c.homeID
				where c.userID =? AND  a.eventTypeID=? AND YEAR(a.eventDate) = YEAR(CURDATE())) patEvents											
				", array($userID, $eventType));
			if ($c -> num_rows() > 0) {
				return $c -> row();
			} else {
				return false;
			}
		}
	}

	public function getPatEventsThisMonth($patID = 0, $eventType) {
		if ($patID > 0) {
			$c = $this -> db -> query("
					
					
					", array($patID, $eventType));
			if ($c -> num_rows() > 0) {
				return $c -> row();
			} else {
				return false;
			}
		} else {
			$userID = $this -> ion_auth -> user() -> row() -> id;
			$c = $this -> db -> query("
					
					", array($userID, $eventType));
			if ($c -> num_rows() > 0) {
				return $c -> row();
			} else {
				return false;
			}
		}
	}

	public function getPatMonthEvent($patID, $eventType) {
		if ($patID > 0) {
			$c = $this -> db -> query("
				select  
  sum(case when week = 1 then 1 else 0 end) as week1,
  sum(case when week = 2 then 1 else 0 end) as week2,
  sum(case when week = 3 then 1 else 0 end) as week3,
  sum(case when week = 4 then 1 else 0 end) as week4,
  sum(case when week = 5 then 1 else 0 end) as week5	
FROM ( SELECT WEEK(a.eventDate, 5) -
        WEEK(DATE_SUB(a.eventDate, INTERVAL DAYOFMONTH(a.eventDate) - 1 DAY), 5) + 1 as week FROM events a where a.patID =? AND  a.eventTypeID= ? AND MONTH(a.eventDate) = MONTH(CURDATE()) 											
) patEvents											
				", array($patID, $eventType));
			if ($c -> num_rows() > 0) {
				return $c -> row();
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function getPatEventTypes() {
		$c = $this -> db -> query("
				Select * from eventtypes;
				");
		if ($c -> num_rows() > 0) {
			return $c;
		} else {
			return false;
		}
	}

	public function updateEvent($eventID, $eventTypeID, $eventDate, $reportedByUser, $desc, $patID) {
		if ($eventID > 0) {
			// update old event
			$data = array('eventTypeID' => $eventTypeID, 'eventDate' => $eventDate, 'reportedByUser' => $reportedByUser, 'desc' => $desc, 'patID' => $patID);
			$this -> db -> where('id', $eventID);
			if ($this -> db -> update('events', $data)) {
				return '1|Successfully Updated Compliance Patient';
			} else {
				return '0|Database Error Occurred';
			}
		} else {
			// new event insert record
			$data = array('id' => $eventID, 'eventTypeID' => $eventTypeID, 'eventDate' => $eventDate, 'reportedByUser' => $reportedByUser, 'desc' => $desc, 'patID' => $patID);
			if ($this -> db -> insert('events', $data)) {
				return '1|Successfully Created Event';
			} else {
				return '0|Database Error, Please Try Again';
			}
		}
	}

	public function updatePRNMonitor($eventID, $prn1desc, $prn2given, $prn2desc, $protocol, $timeEffective, $staffInitial) {
		if ($eventID > 0) {
			// update old event

			if ($timeEffective == "") {
				$timeEffective = NULL;
			}

			if ($prn2given == 0) {
				$prn2given = FALSE;
			} else {
				$prn2given = TRUE;
			}

			if ($protocol == 0) {
				$protocol = FALSE;
			} else {
				$protocol = TRUE;
			}

			$data = array('prn1desc' => $prn1desc, 'prn2given' => $prn2given, 'prn2desc' => $prn2desc, 'protocol' => $protocol, 'timeEffective' => $timeEffective, 'staffInitial' => $staffInitial);
			$this -> db -> where('id', $eventID);
			if ($this -> db -> update('events', $data)) {
				return '1|Successfully Updated Compliance Patient';
			} else {
				return '0|Database Error Occurred';
			}
		}
	}

	// NEW GRAPHS
	public function get7DaysEventType($patID = 0) {
		if ($patID > 0) {
			$c = $this -> db -> query("
				SELECT 
'7',
SUM(case when eventTypeID = 1 then 1 else 0 end) as 'BM', 
SUM(case when eventTypeID = 7 then 1 else 0 end) as 'UR',
SUM(case when eventTypeID = 2 then 1 else 0 end) as 'SE',
SUM(case when eventTypeID = 3 then 1 else 0 end) as 'PR',
SUM(case when eventTypeID = 4 then 1 else 0 end) as 'ER',
SUM(case when eventTypeID = 5 then 1 else 0 end) as 'SZ'
FROM events a Where eventTypeID AND DATE(a.eventDate) = DATE_SUB(CURDATE(), INTERVAL 7 DAY) AND a.patID=?
UNION

SELECT 
'6',
SUM(case when eventTypeID = 1 then 1 else 0 end) as 'BM', 
SUM(case when eventTypeID = 7 then 1 else 0 end) as 'UR',
SUM(case when eventTypeID = 2 then 1 else 0 end) as 'SE',
SUM(case when eventTypeID = 3 then 1 else 0 end) as 'PR',
SUM(case when eventTypeID = 4 then 1 else 0 end) as 'ER',
SUM(case when eventTypeID = 5 then 1 else 0 end) as 'SZ'
FROM events a Where eventTypeID AND DATE(a.eventDate) = DATE_SUB(CURDATE(), INTERVAL 6 DAY) AND a.patID=?

UNION

SELECT 
'5',
SUM(case when eventTypeID = 1 then 1 else 0 end) as 'BM', 
SUM(case when eventTypeID = 7 then 1 else 0 end) as 'UR',
SUM(case when eventTypeID = 2 then 1 else 0 end) as 'SE',
SUM(case when eventTypeID = 3 then 1 else 0 end) as 'PR',
SUM(case when eventTypeID = 4 then 1 else 0 end) as 'ER',
SUM(case when eventTypeID = 5 then 1 else 0 end) as 'SZ'
FROM events a Where eventTypeID AND DATE(a.eventDate) = DATE_SUB(CURDATE(), INTERVAL 5 DAY)   AND a.patID=?
UNION
SELECT 
'4',
SUM(case when eventTypeID = 1 then 1 else 0 end) as 'BM', 
SUM(case when eventTypeID = 7 then 1 else 0 end) as 'UR',
SUM(case when eventTypeID = 2 then 1 else 0 end) as 'SE',
SUM(case when eventTypeID = 3 then 1 else 0 end) as 'PR',
SUM(case when eventTypeID = 4 then 1 else 0 end) as 'ER',
SUM(case when eventTypeID = 5 then 1 else 0 end) as 'SZ'
FROM events a Where eventTypeID AND DATE(a.eventDate) = DATE_SUB(CURDATE(), INTERVAL 4 DAY)   AND a.patID=?
UNION
SELECT 
'3',
SUM(case when eventTypeID = 1 then 1 else 0 end) as 'BM', 
SUM(case when eventTypeID = 7 then 1 else 0 end) as 'UR',
SUM(case when eventTypeID = 2 then 1 else 0 end) as 'SE',
SUM(case when eventTypeID = 3 then 1 else 0 end) as 'PR',
SUM(case when eventTypeID = 4 then 1 else 0 end) as 'ER',
SUM(case when eventTypeID = 5 then 1 else 0 end) as 'SZ'
FROM events a Where eventTypeID AND DATE(a.eventDate) = DATE_SUB(CURDATE(), INTERVAL 3 DAY)   AND a.patID=?
UNION
SELECT 
'2',
SUM(case when eventTypeID = 1 then 1 else 0 end) as 'BM', 
SUM(case when eventTypeID = 7 then 1 else 0 end) as 'UR',
SUM(case when eventTypeID = 2 then 1 else 0 end) as 'SE',
SUM(case when eventTypeID = 3 then 1 else 0 end) as 'PR',
SUM(case when eventTypeID = 4 then 1 else 0 end) as 'ER',
SUM(case when eventTypeID = 5 then 1 else 0 end) as 'SZ'
FROM events a Where eventTypeID AND DATE(a.eventDate) = DATE_SUB(CURDATE(), INTERVAL 2 DAY)   AND a.patID=?
UNION
SELECT 
'1',
SUM(case when eventTypeID = 1 then 1 else 0 end) as 'BM', 
SUM(case when eventTypeID = 7 then 1 else 0 end) as 'UR',
SUM(case when eventTypeID = 2 then 1 else 0 end) as 'SE',
SUM(case when eventTypeID = 3 then 1 else 0 end) as 'PR',
SUM(case when eventTypeID = 4 then 1 else 0 end) as 'ER',
SUM(case when eventTypeID = 5 then 1 else 0 end) as 'SZ'
FROM events a Where eventTypeID AND DATE(a.eventDate) = DATE_SUB(CURDATE(), INTERVAL 1 DAY)   AND a.patID=?
UNION
SELECT 
'0',
SUM(case when eventTypeID = 1 then 1 else 0 end) as 'BM', 
SUM(case when eventTypeID = 7 then 1 else 0 end) as 'UR',
SUM(case when eventTypeID = 2 then 1 else 0 end) as 'SE',
SUM(case when eventTypeID = 3 then 1 else 0 end) as 'PR',
SUM(case when eventTypeID = 4 then 1 else 0 end) as 'ER',
SUM(case when eventTypeID = 5 then 1 else 0 end) as 'SZ'
FROM events a Where eventTypeID AND DATE(a.eventDate) =  CURDATE()  AND a.patID=?
					", array($patID, $patID, $patID, $patID, $patID, $patID, $patID, $patID));
			if ($c -> num_rows() > 0) {
				return $c;
			} else {
				return false;
			}
		} else {
			$userID = $this -> ion_auth -> user() -> row() -> id;
			$c = $this -> db -> query("
				SELECT 
'7',
SUM(case when eventTypeID = 1 then 1 else 0 end) as 'BM', 
SUM(case when eventTypeID = 7 then 1 else 0 end) as 'UR',
SUM(case when eventTypeID = 2 then 1 else 0 end) as 'SE',
SUM(case when eventTypeID = 3 then 1 else 0 end) as 'PR',
SUM(case when eventTypeID = 4 then 1 else 0 end) as 'ER',
SUM(case when eventTypeID = 5 then 1 else 0 end) as 'SZ'
FROM events a 
LEFT JOIN pat_home b on a.patID=b.patID
	LEFT JOIN user_home c on b.homeID = c.homeID
				Where eventTypeID AND DATE(a.eventDate) = DATE_SUB(CURDATE(), INTERVAL 7 DAY) AND c.userID=?
UNION

SELECT 
'6',
SUM(case when eventTypeID = 1 then 1 else 0 end) as 'BM', 
SUM(case when eventTypeID = 7 then 1 else 0 end) as 'UR',
SUM(case when eventTypeID = 2 then 1 else 0 end) as 'SE',
SUM(case when eventTypeID = 3 then 1 else 0 end) as 'PR',
SUM(case when eventTypeID = 4 then 1 else 0 end) as 'ER',
SUM(case when eventTypeID = 5 then 1 else 0 end) as 'SZ'
FROM events a 
LEFT JOIN pat_home b on a.patID=b.patID
	LEFT JOIN user_home c on b.homeID = c.homeID
				Where eventTypeID AND DATE(a.eventDate) = DATE_SUB(CURDATE(), INTERVAL 6 DAY) AND c.userID=?

UNION

SELECT 
'5',
SUM(case when eventTypeID = 1 then 1 else 0 end) as 'BM', 
SUM(case when eventTypeID = 7 then 1 else 0 end) as 'UR',
SUM(case when eventTypeID = 2 then 1 else 0 end) as 'SE',
SUM(case when eventTypeID = 3 then 1 else 0 end) as 'PR',
SUM(case when eventTypeID = 4 then 1 else 0 end) as 'ER',
SUM(case when eventTypeID = 5 then 1 else 0 end) as 'SZ'
FROM events a 
LEFT JOIN pat_home b on a.patID=b.patID
	LEFT JOIN user_home c on b.homeID = c.homeID
				Where eventTypeID AND DATE(a.eventDate) = DATE_SUB(CURDATE(), INTERVAL 5 DAY)   AND c.userID=?
UNION
SELECT 
'4',
SUM(case when eventTypeID = 1 then 1 else 0 end) as 'BM', 
SUM(case when eventTypeID = 7 then 1 else 0 end) as 'UR',
SUM(case when eventTypeID = 2 then 1 else 0 end) as 'SE',
SUM(case when eventTypeID = 3 then 1 else 0 end) as 'PR',
SUM(case when eventTypeID = 4 then 1 else 0 end) as 'ER',
SUM(case when eventTypeID = 5 then 1 else 0 end) as 'SZ'
FROM events a 
LEFT JOIN pat_home b on a.patID=b.patID
	LEFT JOIN user_home c on b.homeID = c.homeID
				Where eventTypeID AND DATE(a.eventDate) = DATE_SUB(CURDATE(), INTERVAL 4 DAY)   AND c.userID=?
UNION
SELECT 
'3',
SUM(case when eventTypeID = 1 then 1 else 0 end) as 'BM', 
SUM(case when eventTypeID = 7 then 1 else 0 end) as 'UR',
SUM(case when eventTypeID = 2 then 1 else 0 end) as 'SE',
SUM(case when eventTypeID = 3 then 1 else 0 end) as 'PR',
SUM(case when eventTypeID = 4 then 1 else 0 end) as 'ER',
SUM(case when eventTypeID = 5 then 1 else 0 end) as 'SZ'
FROM events a 
LEFT JOIN pat_home b on a.patID=b.patID
	LEFT JOIN user_home c on b.homeID = c.homeID
				Where eventTypeID AND DATE(a.eventDate) = DATE_SUB(CURDATE(), INTERVAL 3 DAY)   AND c.userID=?
UNION
SELECT 
'2',
SUM(case when eventTypeID = 1 then 1 else 0 end) as 'BM', 
SUM(case when eventTypeID = 7 then 1 else 0 end) as 'UR',
SUM(case when eventTypeID = 2 then 1 else 0 end) as 'SE',
SUM(case when eventTypeID = 3 then 1 else 0 end) as 'PR',
SUM(case when eventTypeID = 4 then 1 else 0 end) as 'ER',
SUM(case when eventTypeID = 5 then 1 else 0 end) as 'SZ'
FROM events a 
LEFT JOIN pat_home b on a.patID=b.patID
	LEFT JOIN user_home c on b.homeID = c.homeID
				Where eventTypeID AND DATE(a.eventDate) = DATE_SUB(CURDATE(), INTERVAL 2 DAY)   AND c.userID=?
UNION
SELECT 
'1',
SUM(case when eventTypeID = 1 then 1 else 0 end) as 'BM', 
SUM(case when eventTypeID = 7 then 1 else 0 end) as 'UR',
SUM(case when eventTypeID = 2 then 1 else 0 end) as 'SE',
SUM(case when eventTypeID = 3 then 1 else 0 end) as 'PR',
SUM(case when eventTypeID = 4 then 1 else 0 end) as 'ER',
SUM(case when eventTypeID = 5 then 1 else 0 end) as 'SZ'
FROM events a 
LEFT JOIN pat_home b on a.patID=b.patID
	LEFT JOIN user_home c on b.homeID = c.homeID
				Where eventTypeID AND DATE(a.eventDate) = DATE_SUB(CURDATE(), INTERVAL 1 DAY)   AND c.userID=?
UNION
SELECT 
'0',
SUM(case when eventTypeID = 1 then 1 else 0 end) as 'BM', 
SUM(case when eventTypeID = 7 then 1 else 0 end) as 'UR',
SUM(case when eventTypeID = 2 then 1 else 0 end) as 'SE',
SUM(case when eventTypeID = 3 then 1 else 0 end) as 'PR',
SUM(case when eventTypeID = 4 then 1 else 0 end) as 'ER',
SUM(case when eventTypeID = 5 then 1 else 0 end) as 'SZ'
FROM events a
LEFT JOIN pat_home b on a.patID=b.patID
	LEFT JOIN user_home c on b.homeID = c.homeID
				Where eventTypeID AND DATE(a.eventDate) =  CURDATE()  AND c.userID=?
					", array($userID, $userID, $userID, $userID, $userID, $userID, $userID, $userID));
			if ($c -> num_rows() > 0) {
				return $c;
			} else {
				return false;
			}
		}
	}

	public function getThisMonthEventTypes($patID = 0) {
		if ($patID > 0) {
			$c = $this -> db -> query("
				SELECT
	SUM(case when eventTypeID = 1 then 1 else 0 end) as 'BM',
	SUM(case when eventTypeID = 7 then 1 else 0 end) as 'UR',
	SUM(case when eventTypeID = 2 then 1 else 0 end) as 'SE',
	SUM(case when eventTypeID = 3 then 1 else 0 end) as 'PR',
	SUM(case when eventTypeID = 4 then 1 else 0 end) as 'ER',
	SUM(case when eventTypeID = 5 then 1 else 0 end) as 'SZ',
	SUM(case when eventTypeID = 8 then 1 else 0 end) as 'MN'
					FROM events a
	Where eventTypeID AND (YEAR(a.eventDate) = YEAR(CURDATE()) AND MONTH(a.eventDate) = MONTH(CURDATE())) AND a.patID=?
					
					", array($patID));
			if ($c -> num_rows() > 0) {
				return $c -> row();
			} else {
				return false;
			}
		} else {
			$userID = $this -> ion_auth -> user() -> row() -> id;
			$c = $this -> db -> query("
				SELECT
	SUM(case when eventTypeID = 1 then 1 else 0 end) as 'BM',
	SUM(case when eventTypeID = 7 then 1 else 0 end) as 'UR',
	SUM(case when eventTypeID = 2 then 1 else 0 end) as 'SE',
	SUM(case when eventTypeID = 3 then 1 else 0 end) as 'PR',
	SUM(case when eventTypeID = 4 then 1 else 0 end) as 'ER',
	SUM(case when eventTypeID = 5 then 1 else 0 end) as 'SZ',
	SUM(case when eventTypeID = 5 then 1 else 0 end) as 'MN'				
					
	FROM events a
	LEFT JOIN pat_home b on a.patID=b.patID
	LEFT JOIN user_home c on b.homeID = c.homeID
	Where eventTypeID AND (YEAR(a.eventDate) = YEAR(CURDATE()) AND MONTH(a.eventDate) = MONTH(CURDATE())) AND c.userID=?
					
					", array($userID));
			if ($c -> num_rows() > 0) {
				return $c -> row();
			} else {
				return false;
			}
		}
	}

	public function getThisMonthPRNStats($patID = 0) {
		if ($patID > 0) {
			$c = $this -> db -> query("
	SELECT 'total', SUM(case when eventTypeID = 3 then 1 else 0 end) as 'PR'
	FROM events a
	WHERE (YEAR(a.eventDate) = YEAR(CURDATE()) AND MONTH(a.eventDate) = MONTH(CURDATE())) AND a.patID=?
	
	union
	
	SELECT 'protocol', SUM(case when eventTypeID = 3 then 1 else 0 end) as 'PR'
	FROM events a
	WHERE (YEAR(a.eventDate) = YEAR(CURDATE()) AND MONTH(a.eventDate) = MONTH(CURDATE())) AND  a.protocol=1 and a.prn2given=1  AND a.patID=?
	
	union
	
	SELECT 'prn2given', SUM(case when eventTypeID = 3 then 1 else 0 end) as 'PR'
	FROM events a
	WHERE (YEAR(a.eventDate) = YEAR(CURDATE()) AND MONTH(a.eventDate) = MONTH(CURDATE())) AND a.prn2given=1  AND a.patID=?
	
	union
	
	SELECT 'effective', SUM(case when eventTypeID = 3 then 1 else 0 end) as 'PR'
	FROM events a
	WHERE (YEAR(a.eventDate) = YEAR(CURDATE()) AND MONTH(a.eventDate) = MONTH(CURDATE())) AND a.timeEffective is not null AND a.patID=?	
					", array($patID, $patID, $patID, $patID));
			if ($c -> num_rows() > 0) {
				return $c;
			} else {
				return false;
			}
		} else {
			// GET USERs EVENTS
			$userID = $this -> ion_auth -> user() -> row() -> id;
			$c = $this -> db -> query("
		SELECT 'total', SUM(case when eventTypeID = 3 then 1 else 0 end) as 'PR'
	FROM events a
	LEFT JOIN pat_home b on a.patID=b.patID
	LEFT JOIN user_home c on b.homeID = c.homeID
	WHERE (YEAR(a.eventDate) = YEAR(CURDATE()) AND MONTH(a.eventDate) = MONTH(CURDATE())) AND c.userID = ?
	
	union	
	SELECT 'protocol', SUM(case when eventTypeID = 3 then 1 else 0 end) as 'PR'
	FROM events a
LEFT JOIN pat_home b on a.patID=b.patID
	LEFT JOIN user_home c on b.homeID = c.homeID
	WHERE (YEAR(a.eventDate) = YEAR(CURDATE()) AND MONTH(a.eventDate) = MONTH(CURDATE())) AND  a.protocol=1 and a.prn2given=1  AND  c.userID = ?
	
	union	
	SELECT 'prn2given', SUM(case when eventTypeID = 3 then 1 else 0 end) as 'PR'
	FROM events a
LEFT JOIN pat_home b on a.patID=b.patID
	LEFT JOIN user_home c on b.homeID = c.homeID
	WHERE (YEAR(a.eventDate) = YEAR(CURDATE()) AND MONTH(a.eventDate) = MONTH(CURDATE())) AND a.prn2given=1  AND  c.userID = ?
	
	union
	SELECT 'effective', SUM(case when a.eventTypeID = 3 then 1 else 0 end) as 'PR'
	FROM events a
LEFT JOIN pat_home b on a.patID=b.patID
	LEFT JOIN user_home c on b.homeID = c.homeID
	WHERE (YEAR(a.eventDate) = YEAR(CURDATE()) AND MONTH(a.eventDate) = MONTH(CURDATE())) AND a.timeEffective is not null AND  c.userID = ?
					", array($userID, $userID, $userID, $userID));
			if ($c -> num_rows() > 0) {
				return $c;
			} else {
				return false;
			}
		}
	}

	//gets a list of all the homes and patients a user has access to
	public function getUserHomesAndPatients() {
		$c = $this -> db -> query("
select a.patInitials,a.patID,d.homeName, d.homeID from compliancepatient a
left join pat_home b on a.patID=b.patID
left join user_home c on b.homeID=c.homeID
left join homes d on c.homeID=d.homeID
where c.userID = ?;
				", array($this -> ion_auth -> user() -> row() -> id));
		if ($c -> num_rows() > 0) {
			return $c;
		} else {
			return false;
		}
	}

	//gets a list of all the calendar events this user has access to between the startdate and enddate
	/*PREVIOUS SQL
	 * 
	 * select z.id, z.title, z.dateTimeStamp,z.desc, z.patID,z.homeID, z.userID from calendar z 
left join
(select d.homeID from compliancepatient a
left join pat_home b on a.patID=b.patID
left join user_home c on b.homeID=c.homeID
left join homes d on c.homeID=d.homeID
where c.userID = ?) x on z.homeID=x.homeID
WHERE z.dateTimeStamp BETWEEN ? AND ?
UNION
select z.id, z.title, z.dateTimeStamp,z.desc, z.patID,z.homeID, z.userID from calendar z 
left join
(select b.patID from compliancepatient a
left join pat_home b on a.patID=b.patID
left join user_home c on b.homeID=c.homeID
left join homes d on c.homeID=d.homeID
where c.userID = ?) x on z.patID=x.patID
WHERE z.dateTimeStamp BETWEEN ? AND ?;
	 * 
	 * 
	 * 
	 */
	 
		// -----------------------------------------------START   HERE 
	public function getUserCalendarEvents($startDate, $EndDate) {
		$userID = $this -> ion_auth -> user() -> row() -> id;

		$c = $this -> db -> query("
select z.id, z.title, z.dateTimeStamp, z.desc, z.patID,z.homeID, z.userID from calendar z 
left join
(select d.homeID from compliancepatient a
left join pat_home b on a.patID=b.patID
left join user_home c on b.homeID=c.homeID
left join homes d on c.homeID=d.homeID
where c.userID = ?) x on z.homeID=x.homeID
UNION
select z.id, z.title, z.dateTimeStamp,z.desc, z.patID,z.homeID, z.userID from calendar z 
left join
(select b.patID from compliancepatient a
left join pat_home b on a.patID=b.patID
left join user_home c on b.homeID=c.homeID
left join homes d on c.homeID=d.homeID
where c.userID = ?) x on z.patID=x.patID;
				", array($userID, $startDate, $startDate,$userID , $startDate, $EndDate));
		if ($c -> num_rows() > 0) {
			return $c;
		} else {
			return false;
		}
	}

	//retrieves a Calendar event detail
	public function GetCalendarEvent($CalendarEventID) {
			$c = $this -> db -> query("
SELECT * FROM calendar a where a.id=?;
				", array($CalendarEventID));
		if ($c -> num_rows() > 0) {
			return $c -> row();
		} else {
			return false;
		}
	}

	//updates a calendar event
	public function UpdateCalendarEvent($calendarID,$dateTimeStamp, $desc, $patID,$appointmentType,$homeID, $userID, $title) {
			
			if ($calendarID > 0) {
			// update old event
			$data = array('dateTimeStamp' => $dateTimeStamp, 'desc' => $desc, 'patID' => $patID, 'homeID' => $homeID, 'userID' => $userID, 'title' => $title, 'appointmentType' => $appointmentType);
			$this -> db -> where('id', $calendarID);
			if ($this -> db -> update('calendar', $data)) {
				return '1|Successfully Updated Compliance Patient';
			} else {
				return '0|Database Error Occurred';
			}
			
		} else {
			// new event insert record
			$data = array('dateTimeStamp' => $dateTimeStamp, 'desc' => $desc, 'patID' => $patID, 'homeID' => $homeID, 'userID' => $userID, 'title' => $title, 'appointmentType' => $appointmentType);
			if ($this -> db -> insert('calendar', $data)) {
				return '1|Successfully Created Calendar Event';
			} else {
				return '0|Database Error, Please Try Again';
			}
		}
	}

//adds a new calendar event
	public function AddCalendarEvent($dateTimeStamp, $desc, $patID, $appointmentType, $homeID, $userID, $title) {
		$data = array('dateTimeStamp' => $dateTimeStamp, 'desc' => $desc, 'patID' => $patID, 'homeID' => $homeID, 'userID' => $userID, 'title' => $title, 'appointmentType' => $appointmentType );
		$this->db->insert('calendar', $data);
		$calendarId = $this->db->insert_id();
		if($calendarId){
			return $calendarId;
		}else{
			return 0;
		}
	}
	
		// -----------------------------------------------END  HERE 

}
