<?php
class incidents extends CI_Model {
	function __construct() {
		// Call the Model constructor
		parent::__construct ();
	}
	function getIncidents($incidentID = '0', $incidentTypeID = '0') {
		if ($incidentID == '0') { // Return all incidents
			if ($incidentTypeID == '0') {
				return $this->db->query ( "Select * from incidentReport" );
			} else {
				return $this->db->query ( "Select * from incidentReport where incidentType= ?", array (
						$incidentTypeID 
				) );
			}
		} else if ($incidentID == '-1') { // Return Empty Incident
			$i = new stdClass ();
			$i->incidentID = '-1';
			$i->incidentDatetime = date ( 'Y-m-d' );
			$i->description = '';
			$i->actionTaken = '';
			$i->reportedBy = $this->ion_auth->user ()->row ()->id;
			$i->reportedTo = '-1';
			$i->patientID = '-1';
			$i->incidentType = '-1';
			$i->severity = '-1';
			return $i;
		} else {
			// REturn Specific Incident
			$v = $this->db->query ( "Select * from incidentReport where incidentID = ?", array (
					$incidentID 
			) );
			if ($v->num_rows () > 0) {
				return $v->row ();
			} else {
				return $this->incidents->getIncidents ( '-1' );
			}
		}
	}
	function modifyIncident($incidentID = '-1', $incidentDatetime = '-1', $description = '', $actionTaken = '', $reportedBy = '-1', $reportedTo = '-1', $patientID = '-1', $incidentType = '-1', $severity = '', $active = '1') {
		if ($reportedBy == '-1') {
			$reportedBy = $this->ion_auth->user ()->row ()->id;
		}
		
		if ($incidentDatetime == '-1') {
			$incidentDatetime = date ( 'Y-m-d' );
		}
		
		if ($incidentID > 0) { // update Incident
			if ($active == 0) { // Delete Incident
				$this->db->query ( "Delete from incidentReport where incidentID = ?", array (
						$incidentID 
				) );
				return '1|Successfully Deleted Incident';
			} else {
				// Update Incident
				$data = array (
						'incidentDatetime' => $incidentDatetime,
						'description' => $description,
						'actionTaken' => $actionTaken,
						'reportedBy' => $reportedBy,
						'reportedTo' => $reportedTo,
						'patientID' => $patientID,
						'incidentType' => $incidentType,
						'severity' => $severity 
				);
				$this->db->where ( 'incidentID', $incidentID );
				if ($this->db->update ( 'incidentReport', $data )) {
					return '1|Successfully Updated Incident Report';
				} else {
					return '0|Error Updating Incident Report, Try again shortly';
				}
			}
		} else { // Create New Incident
			$data = array (
					'incidentDatetime' => $incidentDatetime,
					'description' => $description,
					'actionTaken' => $actionTaken,
					'reportedBy' => $reportedBy,
					'reportedTo' => $reportedTo,
					'patientID' => $patientID,
					'incidentType' => $incidentType,
					'severity' => $severity 
			);
			if ($this->db->insert ( 'incidentReport', $data )) {
				return '1|Successfully Created Incident Report' . $incidentDatetime;
			} else {
				return '0|Error Creating Incident Report, Please Try again later';
			}
		}
	}
	function getIncidentTypes($incidentTypeID = '-1') {
		if ($incidentTypeID != '-1') {
			$v = $this->db->query ( "Select * from incidentTypeReference where incidentTypeID = ?", array (
					$incidentTypeID 
			) );
			if ($v->num_rows () > 0) {
				return $v->row ()->incidentType;
			} else {
				return 'Error';
			}
		} else {
			return $this->db->query ( "select * from incidentTypeReference" );
		}
	}
    
    function getIncidentsByTypeVsMonth($year, $incidentType='0')
    {
        if ($incidentType == '0')
        {   //retreive all incidents regardless of type
            $q = $this->db->query("SELECT MONTH( incidentDatetime ) AS Mo, COUNT( * ) AS CNT FROM incidentReport GROUP BY MONTH ( incidentDatetime ) where YEAR(incidentDatetime) = ?", array($year));
            if ($q->num_rows() > 0) {
                return $q;
            } else {
                return false;
            }
                        
            /*
            COMMENT THE ABOVE CODE and uncomment this code IF YOU WANT TO GET month by month
            $v = new stdClass();
            $v->jan = $this->db->query("select COUNT(*) as CNT from incidentReport where YEAR(incidentDatetime) = ? and MONTH(incidentDatetime) = 1", array($year))->row()->CNT;
            $v->feb = $this->db->query("select COUNT(*) as CNT from incidentReport where YEAR(incidentDatetime) = ? and MONTH(incidentDatetime) = 2", array($year))->row()->CNT;
            $v->mar = $this->db->query("select COUNT(*) as CNT from incidentReport where YEAR(incidentDatetime) = ? and MONTH(incidentDatetime) = 3", array($year))->row()->CNT;
            $v->apr = $this->db->query("select COUNT(*) as CNT from incidentReport where YEAR(incidentDatetime) = ? and MONTH(incidentDatetime) = 4", array($year))->row()->CNT;
            $v->may = $this->db->query("select COUNT(*) as CNT from incidentReport where YEAR(incidentDatetime) = ? and MONTH(incidentDatetime) = 5", array($year))->row()->CNT;
            $v->jun = $this->db->query("select COUNT(*) as CNT from incidentReport where YEAR(incidentDatetime) = ? and MONTH(incidentDatetime) = 6", array($year))->row()->CNT;
            $v->jul = $this->db->query("select COUNT(*) as CNT from incidentReport where YEAR(incidentDatetime) = ? and MONTH(incidentDatetime) = 7", array($year))->row()->CNT;
            $v->aug = $this->db->query("select COUNT(*) as CNT from incidentReport where YEAR(incidentDatetime) = ? and MONTH(incidentDatetime) = 8", array($year))->row()->CNT;
            $v->sep = $this->db->query("select COUNT(*) as CNT from incidentReport where YEAR(incidentDatetime) = ? and MONTH(incidentDatetime) = 9", array($year))->row()->CNT;
            $v->oct = $this->db->query("select COUNT(*) as CNT from incidentReport where YEAR(incidentDatetime) = ? and MONTH(incidentDatetime) = 10", array($year))->row()->CNT;
            $v->nov = $this->db->query("select COUNT(*) as CNT from incidentReport where YEAR(incidentDatetime) = ? and MONTH(incidentDatetime) = 11", array($year))->row()->CNT;
            $v->dec = $this->db->query("select COUNT(*) as CNT from incidentReport where YEAR(incidentDatetime) = ? and MONTH(incidentDatetime) = 12", array($year))->row()->CNT; 
            return $v;
            */
            
        } else 
        {
            //retreive incidents of a specific type
             //$q = $this->db->query("SELECT MONTH( incidentDatetime ) AS Mo, COUNT( * ) AS CNT FROM incidentReport where YEAR(incidentDatetime) = ? and incidentType = ?", array($year, $incidentType));
             //return $q;  
            
                        
//             // COMMENT THE ABOVE CODE and UNCOMMENT THE BELOW CODE IF YOU WANT TO GET MONTH BY MONTH
            $v = new stdClass();
            $v->jan = $this->db->query("select COUNT(*) as CNT from incidentReport where YEAR(incidentDatetime) = ? and MONTH(incidentDatetime) = 1 and incidentType = ?", array($year, $incidentType))->row()->CNT;
            $v->feb = $this->db->query("select COUNT(*) as CNT from incidentReport where YEAR(incidentDatetime) = ? and MONTH(incidentDatetime) = 2 and incidentType = ?", array($year, $incidentType))->row()->CNT;
            $v->mar = $this->db->query("select COUNT(*) as CNT from incidentReport where YEAR(incidentDatetime) = ? and MONTH(incidentDatetime) = 3 and incidentType = ?", array($year, $incidentType))->row()->CNT;
            $v->apr = $this->db->query("select COUNT(*) as CNT from incidentReport where YEAR(incidentDatetime) = ? and MONTH(incidentDatetime) = 4 and incidentType = ?", array($year, $incidentType))->row()->CNT;
            $v->may = $this->db->query("select COUNT(*) as CNT from incidentReport where YEAR(incidentDatetime) = ? and MONTH(incidentDatetime) = 5 and incidentType = ?", array($year, $incidentType))->row()->CNT;
            $v->jun = $this->db->query("select COUNT(*) as CNT from incidentReport where YEAR(incidentDatetime) = ? and MONTH(incidentDatetime) = 6 and incidentType = ?", array($year, $incidentType))->row()->CNT;
            $v->jul = $this->db->query("select COUNT(*) as CNT from incidentReport where YEAR(incidentDatetime) = ? and MONTH(incidentDatetime) = 7 and incidentType = ?", array($year, $incidentType))->row()->CNT;
            $v->aug = $this->db->query("select COUNT(*) as CNT from incidentReport where YEAR(incidentDatetime) = ? and MONTH(incidentDatetime) = 8 and incidentType = ?", array($year, $incidentType))->row()->CNT;
            $v->sep = $this->db->query("select COUNT(*) as CNT from incidentReport where YEAR(incidentDatetime) = ? and MONTH(incidentDatetime) = 9 and incidentType = ?", array($year, $incidentType))->row()->CNT;
            $v->oct = $this->db->query("select COUNT(*) as CNT from incidentReport where YEAR(incidentDatetime) = ? and MONTH(incidentDatetime) = 10 and incidentType = ?", array($year, $incidentType))->row()->CNT;
            $v->nov = $this->db->query("select COUNT(*) as CNT from incidentReport where YEAR(incidentDatetime) = ? and MONTH(incidentDatetime) = 11 and incidentType = ?", array($year, $incidentType))->row()->CNT;
            $v->dec = $this->db->query("select COUNT(*) as CNT from incidentReport where YEAR(incidentDatetime) = ? and MONTH(incidentDatetime) = 12 and incidentType = ?", array($year, $incidentType))->row()->CNT; 
            return $v;          
        }
        
    }
}