<?php
class quick extends CI_Model {
	function __construct() {
		// Call the Model constructor
		parent::__construct ();
		$this->load->library ( 'encrypt' );
	}
	function name_shorten($firstName, $lastName) {
		return $firstName . substr ( $lastName, 1 );
	}
	
	// Checks to see if user's ipaddress is = to the pharmacies ip
	function is_internal() {
		$userIP = $this->input->ip_address ();
		$pharmacyIP = '99.253.74.200';
		if ($userIP == $pharmacyIP) {
			return true;
		} else {
			return false;
		}
	}
	// tested and works! uses encryption key in config file
	function ia_encrypt($msg) {
		return $this->encrypt->encode ( $msg );
	}
	// tested and workss
	function ia_decrypt($msg) {
		return $this->encrypt->decode ( $msg );
	}
	public function getStoreSettings() {
		return $this->db->query ( "select * from pharmacySettings" )->row ();
	}
	public function updateStoreSettings($storename = '', $phone = '', $address = '', $city = '', $prov = '') {
		$data = array (
				'storename' => $storename,
				'phone' => $phone,
				'address' => $address,
				'city' => $city,
				'province' => $prov 
		);
		$this->db->update ( 'pharmacySettings', $data );
		return '1|Successfully Updated Pharmacy Settings';
	}
	public function accessTo($page) {
		if ($this->ion_auth->in_group ( 'admin' )) {
			return true;
		} else {
			// whitelist pages like this
			if ($this->ion_auth->in_group ( 'pharmacyuser' )) {
				$pharmUserAccess = array (
						'news',
						'auth',
						'compliance',
						'filingCab',
						'secure_controller',
						'setting',
						'production',
						'homePortalEdu',
						'localconnection'
				);
				return (in_array ( $page, $pharmUserAccess ));
			}
			
			if ($this->ion_auth->in_group ( 'homeportaluser' )) {
				$pharmUserAccess = array (
						'homeportal',
						'secure_controller',
						'production',
						'homePortalEdu' 
				);
				return (in_array ( $page, $pharmUserAccess ));
			}
			
			if ($this->ion_auth->in_group ( 'guestuser' )) {
				$pharmUserAccess = array (
						'secure_controller',
						'homePortalEdu' 
				);
				return (in_array ( $page, $pharmUserAccess ));
			}
		}
	}
	
	function timeAgo($time_ago) {
		$cur_time = time ();
		$time_elapsed = $cur_time - $time_ago;
		$seconds = $time_elapsed;
		$minutes = round ( $time_elapsed / 60 );
		$hours = round ( $time_elapsed / 3600 );
		$days = round ( $time_elapsed / 86400 );
		$weeks = round ( $time_elapsed / 604800 );
		$months = round ( $time_elapsed / 2600640 );
		$years = round ( $time_elapsed / 31207680 );
		// Seconds
		if ($seconds <= 60) {
			echo "$seconds seconds ago";
		} 		// Minutes
		else if ($minutes <= 60) {
			if ($minutes == 1) {
				echo "one minute ago";
			} else {
				echo "$minutes minutes ago";
			}
		} 		// Hours
		else if ($hours <= 24) {
			if ($hours == 1) {
				echo "an hour ago";
			} else {
				echo "$hours hours ago";
			}
		} 		// Days
		else if ($days <= 7) {
			if ($days == 1) {
				echo "yesterday";
			} else {
				echo "$days days ago";
			}
		} 		// Weeks
		else if ($weeks <= 4.3) {
			if ($weeks == 1) {
				echo "a week ago";
			} else {
				echo "$weeks weeks ago";
			}
		} 		// Months
		else if ($months <= 12) {
			if ($months == 1) {
				echo "a month ago";
			} else {
				echo "$months months ago";
			}
		} 		// Years
		else {
			if ($years == 1) {
				echo "one year ago";
			} else {
				echo "$years years ago";
			}
		}
	}
	public function statusReport() {
		$this->db_sync = $this->load->database ( 'sync', TRUE );
		$v = $this->db_sync->query ( "
				SELECT  (
        SELECT COUNT(*)
        FROM   RX
        ) AS rxCount,
        (
        SELECT COUNT(*)
        FROM   doc
        ) AS docCount,
		(
        SELECT COUNT(*)
        FROM   Drg
        ) AS drgCount,
		
		(
        SELECT COUNT(*)
        FROM   AllergyDrugIngredients
        ) AS AllergyDrugIngredients,
		(
        SELECT COUNT(*)
        FROM   AllergyPickList
        ) AS AllergyPickList,
		(
        SELECT COUNT(*)
        FROM   PatChart
        ) AS PatChart,
		(
        SELECT COUNT(*)
        FROM   PatCnd
        ) AS PatCnd
				" );
		
		// SELECT COUNT (*) as 'rxCount', COUNT(*) as 'doc' FROM RX" );
		if ($v->num_rows () > 0) {
			return $v->row ();
		} else {
			return false;
		}
	}
	public function userStats() {
		$v = $this->db->query ( "
			
SELECT  (
        SELECT COUNT(*)
        FROM   users WHERE DATEDIFF(FROM_UNIXTIME(last_login), CURDATE()) = -14
        ) AS '14',
       (
        SELECT COUNT(*)
        FROM   users WHERE DATEDIFF(FROM_UNIXTIME(last_login), CURDATE()) = -13
        ) AS '13',
       (
        SELECT COUNT(*)
        FROM   users WHERE  DATEDIFF(FROM_UNIXTIME(last_login), CURDATE()) = -12
        ) AS '12',
       (
        SELECT COUNT(*)
        FROM   users WHERE  DATEDIFF(FROM_UNIXTIME(last_login), CURDATE()) = -11
        ) AS '11',
       (
        SELECT COUNT(*)
        FROM   users WHERE  DATEDIFF(FROM_UNIXTIME(last_login), CURDATE()) = -10
        ) AS '10',
       (
        SELECT COUNT(*)
        FROM   users WHERE  DATEDIFF(FROM_UNIXTIME(last_login), CURDATE()) = -9
        ) AS '9',
       (
        SELECT COUNT(*)
        FROM   users WHERE  DATEDIFF(FROM_UNIXTIME(last_login), CURDATE()) = -8
        ) AS '8',
       (
        SELECT COUNT(*)
        FROM   users WHERE  DATEDIFF(FROM_UNIXTIME(last_login), CURDATE()) = -7
        ) AS '7',
       (
        SELECT COUNT(*)
        FROM   users WHERE  DATEDIFF(FROM_UNIXTIME(last_login), CURDATE()) = -6
        ) AS '6',
       (
        SELECT COUNT(*)
        FROM   users WHERE  DATEDIFF(FROM_UNIXTIME(last_login), CURDATE()) = -5
        ) AS '5',
       (
        SELECT COUNT(*)
        FROM   users WHERE  DATEDIFF(FROM_UNIXTIME(last_login), CURDATE()) = -4
        ) AS '4',
       (
        SELECT COUNT(*)
        FROM   users WHERE  DATEDIFF(FROM_UNIXTIME(last_login), CURDATE()) = -3
        ) AS '3',
       (
        SELECT COUNT(*)
        FROM   users WHERE  DATEDIFF(FROM_UNIXTIME(last_login), CURDATE()) = -2
        ) AS '2',
       (
        SELECT COUNT(*)
        FROM   users WHERE  DATEDIFF(FROM_UNIXTIME(last_login), CURDATE()) = -1
        ) AS '1',
       (
        SELECT COUNT(*)
        FROM   users WHERE  DATEDIFF(FROM_UNIXTIME(last_login), CURDATE()) = 0
        ) AS '0',
       (
        SELECT COUNT(*)
        FROM   users
        ) AS 'totalUsers'
				" );
		
		// SELECT COUNT (*) as 'rxCount', COUNT(*) as 'doc' FROM RX" );
		if ($v->num_rows () > 0) {
			return $v;
		} else {
			return false;
		}
	}
	public function userTypes() {
		$v = $this->db->query ( "
		
SELECT  (
        SELECT COUNT(*)
        FROM   users a LEFT JOIN users_groups b on a.id= b.user_id WHERE b.group_id=18) as 'PortalUsers',
       (
       SELECT COUNT(*)
        FROM   users a LEFT JOIN users_groups b on a.id= b.user_id WHERE b.group_id=17) as 'PharmacyUsers'		
				" );
		
		// SELECT COUNT (*) as 'rxCount', COUNT(*) as 'doc' FROM RX" );
		if ($v->num_rows () > 0) {
			return $v->row ();
		} else {
			return false;
		}
	}
	public function patientStats() {
		$v = $this->db->query ( "
	
SELECT  (
              SELECT COUNT(*)
        FROM compliancepatient) as 'compliancepatients'
				" );
		
		// SELECT COUNT (*) as 'rxCount', COUNT(*) as 'doc' FROM RX" );
		if ($v->num_rows () > 0) {
			return $v->row ();
		} else {
			return false;
		}
	}
}
?>