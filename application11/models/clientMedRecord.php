
<?php

	class clientMedRecord extends CI_Model {
		
		/*
		 * Table Properties 
		   CREATE TABLE clientmedrecord (
				id INT NOT NULL AUTO_INCREMENT,
		 		pacMedID INT ,
		        weekOfMed INT,
		        reasonForAdvance VARCHAR(250),
		        deliveryAddress VARCHAR(250),
		        deliveryConfirm INT,                  // 0 not yet received; 1 received
		        deliveryConfirmBy VARCHAR(250),			    			
		 		userName VARCHAR(250), 
		        addDate DATETIME,
				remark VARCHAR(250),
				PRIMARY KEY (id)
			);		
		 * 
		 */

		protected  $tableName ='clientmedrecord';
		protected  $where ="id";
		protected  $update ="";
		protected  $orderBy ="";
		
		public function __construct() {
		
			parent::__construct ();
		
		}

		// Standed functions Start
		
		public function addData($data){
			
			$d = $topicData = $this->db->insert($this->tableName, $data); 
			if($d == TRUE)	{
				return $d;
			}
			
		}
		
		public function updateData($id,$data){
			
			$this->db->where($this->where, $id);
			$d = $this->db->update($this->tableName, $data); 
			if($d == TRUE)	{
				return $d;
			}
		}
		
		public function deleteData($id){
			
			$this->db->where($this->where, $id);
			$this->db->delete($this->tableName); 
			
		}
		
		public function getAllData(){
			$this->db->order_by($this->orderBy, "ASC"); 			
			return $query = $this->db->get($this->tableName)->result();	 
						
		}
		
		public function getAllDataBaseOnId($data){
									
			$this->db->where($this->where, $data);			
			return $query = $this->db->get($this->tableName)->result();	 
						
		}
		
		public function findDuplicate($data){
			// $data has to be an array
			$this->db->where($data);
			return $query = $this->db->get($this->tableName)->num_rows();
				
		}
		
		// Standed functions  End
				
		
		

	}


?>