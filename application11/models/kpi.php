<?php
class kpi extends CI_Model {
	function __construct() {
		// Call the Model constructor
		parent::__construct ();
	}
	
	public function getKPIs()
	{//returns false if no KPIs, will return array of rows if KPIs exist
		//  KPIID|indicatorName|indicatorDescription|minValue|maxValue|targetValue
		$v = $this->db->query("Select * from KPI where active = 1");
		if ($v->num_rows() > 0)
		{
			return $v;
		} else {
			return false;
		}
	}
	
	public function getKPI($KPIID)
	{
		$v = $this->db->query("Select * from KPI where active = 1 and KPIID = ?", array($KPIID));
		if ($v->num_rows() > 0)
		{
			return $v->row();
		} else 
		{
			$d = new stdClass();
			$d->KPIID = '-1';
			$d->indicatorName= '';
			$d->indicatorDescription= '';
			$d->minValue= '0';
			$d->maxValue= '100';
			$d->targetValue= '80';
			return $d;
		}
	}
	
	public function getKPIPoints($KPIID)
	{
		$v = $this->db->query("Select * from KPIDetail where KPIID = ? and active = 1", array($KPIID));
		if ($v->num_rows() > 0)
		{
			return $v;
		} else 
		{
			return false;
		}
	}
	
	public function updateKPI($KPIID='-1', $indicatorName='', $indicatorDescription='', $minValue='0', $maxValue='0', $targetValue='0', $active='1')
	{ // TO BE USED WITH JQUERY POST CALLS ONLY
		if ($KPIID > 0)
		{
			//Update KPIID
			if (strcmp($active , '1'))
			{//update KPI
				$data = array('indicatorName'=>$indicatorName, 'indicatorDescription'=>$indicatorDescription, 'minValue'=>$minValue, 'maxValue'=>$maxValue, 'targetValue'=>$targetValue);
				$this->db->where('KPIID', $KPIID);
				if ($this->db->update('KPI', $data))
				{
					return '1|Successfully Updated KPI';
				} else 
				{
					return '0|Unknown Error Occurred';
				}
			} else 
			{//delete KPI
				$data = array('active'=>'0');
				$this->db->where('KPIID', $KPIID);
				if ($this->db->update('KPI', $data))
				{
					return '1|Successfully Deleted KPI';
				} else {
					return '0|Unknown Error Occurred';
				}
			}
		} else
		{	//Insert into KPIID
			$data = array('active'=>'1', 'indicatorName'=>$indicatorName, 'indicatorDescription'=>$indicatorDescription, 'minValue'=>$minValue, 'maxValue'=>$maxValue, 'targetValue'=>$targetValue);
			if ($this->db->insert('KPI', $data))
			{
				return '1|Successfully Created KPI';
			} else 
			{
				return '0|Unknown Error Occurred';
			}
		}	 
	}
	
	public function updateKPIDetail($KPIDetailID='-1',$KPIID, $pointValue='0', $active='1', $recordedDate='-1')
	{ // TO BE USED WITH JQUERY POST CALLS ONLY
		if (strcmp($recordedDate , '-1'))
		{
			$recordedDate = date('Y-m-d H:i:s');
		}
		if ($KPIDetailID > 0)
		{
			//Update KPIIDDetail
			if (strcmp($active , '1'))
			{//update KPIDetail
				$data = array('pointValue'=>$pointValue, 'recordedDate'=>$recordedDate);
				$this->db->where('KPIID', $KPIID);
				$this->db->where('KPIDetailID', $KPIDetailID);
				if ($this->db->update('KPIDetail', $data))
				{
					return '1|Successfully Updated KPI Point';
				} else
				{
					return '0|Unknown Error Occurred';
				}
			} else
			{//delete KPI
				$data = array('active'=>'0');
				$this->db->where('KPIDetailID', $KPIDetailID);
				if ($this->db->update('KPIDetail', $data))
				{
					return '1|Successfully Deleted KPI Point';
				} else {
					return '0|Unknown Error Occurred';
				}
			}
		} else
		{	//Insert into KPIID
			$data = array('active'=>'1', 'KPIID'=>$KPIID, 'pointValue'=>$pointValue, 'recordedDate'=>$recordedDate, 'userID'=>$this->ion_auth->user()->row()->id);
			if ($this->db->insert('KPIDetail', $data))
			{
				return '1|Successfully Created KPI Point';
			} else
			{
				return '0|Unknown Error Occurred';
			}
		}
	}
	
	
}