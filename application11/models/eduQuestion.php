<?php

	class eduQuestion extends CI_Model {
		
		/*
		 * Table Properties 
		   CREATE TABLE eduquestion (
				id INT NOT NULL AUTO_INCREMENT,
				question VARCHAR(250) NOT NULL,
		 		generalQuizzesInfo INT,
				quizzesHint VARCHAR(250), 
		 		quizzesAnswer VARCHAR(250),  
		 		answerType INT,        // 1 : T?F  , 2: radio , 3: checkbox
				addBy VARCHAR(100),
				addDate DATETIME,
				remark VARCHAR(100),
				PRIMARY KEY (id)
			);		
		 * 
		 */

		protected  $tableName ='eduquestion';
		protected  $where ="id";
		protected  $update ="";
		protected  $orderBy ="addDate";
		
		public function __construct() {
		
			parent::__construct ();
		
		}

		// Standed functions Start
		
		public function addData($data){
			
			$d = $topicData = $this->db->insert($this->tableName, $data); 
			if($d == TRUE)	{
				return $d;
			}
			
		}
		
		public function updateData($id,$data){
			
			$this->db->where($this->where, $id);
			$d = $this->db->update($this->tableName, $data); 
			if($d == TRUE)	{
				return $d;
			}
		}
		
		public function deleteData($id){
			
			$this->db->where($this->where, $id);
			$this->db->delete($this->tableName); 
			
		}
		
		public function getAllData(){
			$this->db->order_by($this->orderBy, "ASC"); 			
			return $query = $this->db->get($this->tableName)->result();	 
						
		}
		
		public function getAllDataBaseOnId($data){
									
			$this->db->where($this->where, $data);			
			return $query = $this->db->get($this->tableName)->result();	 
						
		}
		
		public function findDuplicate($data){
			// $data has to be an array
			$this->db->where($data);
			return $query = $this->db->get($this->tableName)->num_rows();
				
		}
		
		// Standed functions Start End
		
				
		
		public function getInfoBaseOnValue($data){
			// $data has to be an array
			$this->db->where($data);
			return $query = $this->db->get($this->tableName)->result();			
				
		}
		
		public function getNumberOfQuestionBaseOnGeneralInfoId($data){
			$this->db->select('COUNT(id) AS numberOfQuestion');
			$this->db->where('generalquizzesinfo',$data);
			return $query = $this->db->get($this->tableName)->result();	
		}
		
		public function allQuizWithAnswerBaseOnGeneralInfoId($id){
			
			$this->db->select('e.id, e.question, e.quizzesHint, e.quizzesAnswer, e.answerType, e.generalQuizzesInfo, a.correctAnswer');
			$this->db->from('eduquestion AS e');
			$this->db->join('answer AS a', 'e.id = a.questionId', 'left');
			$this->db->where('e.generalQuizzesInfo',$id);
			
			return $query = $this->db->get()->result();							
		}
		
		public function allQuizWithAnswerBaseOnQuestionId($id){
			
			$this->db->select('e.id, e.question, e.quizzesHint, e.quizzesAnswer, e.answerType, e.generalQuizzesInfo, a.correctAnswer');
			$this->db->from('eduquestion AS e');
			$this->db->join('answer AS a', 'e.id = a.questionId', 'left');
			$this->db->where('e.id',$id);
			
			return $query = $this->db->get()->result();							
		}
		
		public function allQuizWithCandidateAnswerBaseOnGeneralInfoIdAndAttempt($id){
			
			$query_str="SELECT cR.id, e.question, e.quizzesAnswer, e.answerType, e.generalQuizzesInfo, a.correctAnswer, cR.answer
					    FROM eduquestion AS e
						LEFT JOIN answer AS a
						ON e.id = a.questionId			
						LEFT JOIN candidateresult AS cR
						ON e.id = cR.questionId			 
						LEFT JOIN candidate AS c
						ON c.id = cR.candidateId			
						WHERE c.id = $id";
			
			return $query=$this->db->query($query_str)->result();
									
		}
		
		public function allQuizWithCandidateAnswerBaseOnGeneralInfoIdAndAttemptScore($id,$attempt){
				
			$query_str="SELECT cR.id, e.question, e.quizzesAnswer, e.answerType, e.generalQuizzesInfo, a.correctAnswer, cR.answer
			FROM eduquestion AS e
			LEFT JOIN answer AS a
			ON e.id = a.questionId
			LEFT JOIN candidateresult AS cR
			ON e.id = cR.questionId
			LEFT JOIN candidate AS c
			ON c.id = cR.candidateId
			WHERE e.generalQuizzesInfo = $id AND c.attempt = $attempt";
				
			return $query=$this->db->query($query_str)->result();
				
		}

	}


?>