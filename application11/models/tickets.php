<?php

	class tickets extends CI_Model {
		
		

		protected  $tableName ='tickets';
		protected  $where ="patID";
		protected  $update ="";
		protected  $dateOfLive = "2016-07-14";
		
		public function __construct() {
		
			parent::__construct ();
		
		}

		// Standed functions Start
		
		public function addData($data){
			
			$d = $this->db->insert($this->tableName, $data); 
			if($d == TRUE)	{
				return $d;
			}
			
		}
		
		public function updateData($id,$data){
			
			$this->db->where('ticketID', $id);
			$d = $this->db->update($this->tableName, $data); 
			if($d == TRUE)	{
				return $d;
			}
		}
		
		public function deleteData($id){
			
			$this->db->where('ticketID', $id);
			$this->db->delete($this->tableName); 
			
		}
		
		public function getAllData(){
			$this->db->order_by("topicsName", "ASC"); 			
			return $query = $this->db->get($this->tableName)->result();	 
						
		}
		
		public function getAllDataBaseOnId($data){
									
			$this->db->where($this->where, $data);			
			return $query = $this->db->get($this->tableName)->result();	 
						
		}
		
		public function getAllDataBaseOnTableId($data){
									
			$this->db->where('ticketID', $data);			
			return $query = $this->db->get($this->tableName)->row();	 
						
		}
		
		public function getInfoToLocalEboard(){
			//$str = "SELECT  ticketID , Description, rxnumList, dateOfDelivery, patID FROM tickets  WHERE batchNum = 1 AND ticketID NOT IN (SELECT ticketId FROM sendticketandmedchangetolocaleboard WHERE typeOfTicket = 1)";
			$str = "SELECT  t.ticketID AS TicketID, t.Description, t.rxnumList AS RxList, t.dateOfDelivery AS DateofDelivery, t.patID AS PatID, p.NHID, p.NHWardID, p.City, users.last_name, users.first_name FROM tickets AS t
			LEFT JOIN pat AS p ON p.ID = t.patID
			LEFT JOIN users ON users.id = t.createdBy
			WHERE t.batchNum = 0 AND t.dateOfDelivery >= '".$this->dateOfLive."' AND t.ticketID NOT IN (SELECT ticketId FROM sendticketandmedchangetolocaleboard WHERE typeOfTicket = 1)";
			return $query = $this->db->query($str)->result();						
		}
			
		
		public function findDuplicate($data){
			// $data has to be an array
			$this->db->where($data);
			return $query = $this->db->get($this->tableName)->num_rows();
				
		}
		
		// Standed functions Start End
		
		public function getNumberOfTicketsBaseOnPatId($patId){
			$str ="SELECT COUNT(*) FROM tickets WHERE patID =$patId AND dateComplete IS NULL";
			return $query = $this->db->query($str)->num_rows();
			
			//$query = $this->db->query($str)->result();
		}
		
		public function getPatAndTicketInfo($ticket){
			$str = "SELECT pat.ID, pat.LastName, pat.FirstName, tickets.Description, tickets.rxnumList, tickets.createdBy, tickets.dateOfDelivery FROM pat LEFT JOIN tickets ON tickets.patID = pat.ID WHERE tickets.ticketID =$ticket";
			return $query = $this->db->query($str)->row();
		}
		
		public function getCommentBaseOnTicketId($ticketId){
			$str = "SELECT * FROM ticketdiscussion WHERE ticketID =$ticketId";
			return $query = $this->db->query($str)->result();
		}
		
		public function getCurrentTicket(){
			$str = "SELECT pat.LastName, pat.FirstName, pat.NHWardID, t.dateOfDelivery FROM tickets  AS t
				LEFT JOIN pat ON pat.ID = t.patID
				WHERE  t.active = 1 AND t.dateComplete IS NULL AND (t.ticketTypeReferenceID = 1 OR t.ticketTypeReferenceID =2) AND t.dateOfDelivery = CURDATE()";
			return $query = $this->db->query($str)->result();
		}
		
		//************************************************************************************************************************//
	  //************************************************************************************************************************//
	                                   //*******  Med Change  ********//
	  //************************************************************************************************************************//
	  //************************************************************************************************************************//  
	  
		
		public function getNumberOfMedChangesBaseOnPatId($patId){
			$str ="SELECT COUNT(*) FROM medchanges WHERE patID =$patId AND active = 1";
			return $query = $this->db->query($str)->num_rows();			
			
		}
		
	   public function getTicketBaseOnPatId($type,$patId){
	   	   $last7Days = date('Y-m-d', strtotime('-7 days'));
		   
	   	   $str ="SELECT *, pat.LastName AS L, pat.FirstName AS F FROM tickets
	   	   LEFT JOIN pat ON pat.ID = tickets.patID
	   	   WHERE tickets.ticketTypeReferenceID = $type AND tickets.patID = $patId  AND ( tickets.active = 1 OR tickets.dateComplete > '".$last7Days."')";
		   
		   return $query = $this->db->query($str)->result();
		   
	   }
		
	   public function getActiveTicketsAndInfo(){
	   	   $str="SELECT t.ticketID, t.createdBy, t.datecreated, t.dateComplete, t.patID, t.ticketTypeReferenceID, t.Description, t.active, t.rank, t.rankAddDate, t.rxnumList, 
	   	   		pat.LastName, pat.FirstName, users.last_name, users.first_name
			FROM tickets AS t 
			LEFT JOIN pat ON pat.ID = t.patID
			LEFT JOIN users ON users.id = t.createdBy
			WHERE t.active =1 AND (t.ticketTypeReferenceID =1 OR t.ticketTypeReferenceID =2) ORDER BY t.datecreated DESC";
		   
		   return $query = $this->db->query($str)->result();
	   }
		
		
	  public function getLastTicket($tId){
	  	  $str ="SELECT * FROM ticketdiscussion WHERE ticketID = $tId ORDER BY messageDate DESC LIMIT 1";
	  	  return $query = $this->db->query($str)->row();
	  }
	  
	  
	  public function getTicketInfoToEboard(){
	  	   $str ="SELECT t.ticketID, t.dateOfDelivery, pat.NHID, pat.NHWardID ,pat.ID
				FROM tickets AS t 
				LEFT JOIN pat ON pat.ID = t.patID
				WHERE t.dateOfDelivery IS NOT NULL AND t.batchNum = 0 AND t.active = 0 AND t.ticketID NOT IN ( SELECT ticketID FROM eboard  WHERE type = 1 )";
	  	   return $query = $this->db->query($str)->result();
	  }
	  
	  
	  public function getMedchangeInfoToEboard(){
	  	   $str ="SELECT m.medChangeID, m.endDate, m.nextRDD, pat.NHID, pat.NHWardID ,pat.ID
				FROM medchanges AS m 
				LEFT JOIN pat ON pat.ID = m.patID
				WHERE m.actionComplete = 1 AND m.batchNum = 0 AND m.medChangeID NOT IN ( SELECT ticketID FROM eboard WHERE type =2 ) AND pat.ID IS NOT NULL AND m.endDate > '2016-04-08'";
		   return $query = $this->db->query($str)->result();
	  }
	  
	  
	  
	  public function getpatInfoBaseOnMedchangeId($medId){
	  	   $str = "SELECT * FROM pat LEFT JOIN medchanges ON medchanges.patID = pat.ID WHERE medchanges.medChangeID =$medId";
	  	   return $query = $this->db->query($str)->row();
	  }
	  
	  public function getPatAndMedChangeInfo($medId){
	  	   $str = "SELECT pat.ID, pat.LastName, pat.FirstName, medchanges.changeText, medchanges.technicianUserID, medchanges.pharmacistUserID, medchanges.endDate FROM pat LEFT JOIN medchanges ON medchanges.patID = pat.ID WHERE medchanges.medChangeID =$medId";
	  	   return $query = $this->db->query($str)->row();
	  }
	  
	  public function getUnCompletedMedCheck(){
	  	  $str ="SELECT MC.patID, MC.changeText, MC.endDate, pat.LastName, pat.FirstName, pat.NHWardID FROM medchanges AS MC 
				LEFT JOIN pat ON pat.ID = MC.patID
				WHERE MC.actionComplete = 0 AND MC.endDate = CURDATE() AND MC.active = 1 ORDER BY MC.endDate ASC"; 
	  	  return $query = $this->db->query($str)->result();
	  }
	  
	  public function getUnCompletedMedchangeForUpdate(){
	  	   $str = "SELECT * FROM medchanges WHERE startDate < CURDATE() AND actionComplete = 0 AND active = 1";
	  	   return $query = $this->db->query($str)->result();
	  }
	  
	  public function updateMedChanages($id,$data){
		  	$this->db->where('medChangeID', $id);
		  	$d = $this->db->update('medchanges', $data);
		  	if($d == TRUE)	{
		  		return $d;
		  	}
	  }
	  
	  public function getMedChangeForPharmacyEdord(){
	  	  $str  ="SELECT  m.medchangeID AS TicketID, m.changeText AS Description,  m.endDate AS DateofDelivery, m.patID AS PatID, p.NHID, p.NHWardID, p.City, users.last_name, users.first_name, tu.last_name AS tLName, tu.first_name AS tFName FROM medchanges AS m
				LEFT JOIN pat AS p ON p.ID = m.patID
				LEFT JOIN users ON users.id = m.pharmacistUserID
				LEFT JOIN users AS tu ON tu.id = m.technicianUserID
				WHERE m.batchNum = 0  AND m.endDate >= '".$this->dateOfLive."' AND m.medchangeID NOT IN (SELECT ticketId FROM sendticketandmedchangetolocaleboard WHERE typeOfTicket = 2)";
	  	  return $query = $this->db->query($str)->result();
	  }
	  
	  //************************************************************************************************************************//
	  //************************************************************************************************************************//
	                                   //*******  E Board  ********//
	  //************************************************************************************************************************//
	  //************************************************************************************************************************//  
	    
		  public function addEboard($data){
				
				$d = $this->db->insert('eboard', $data); 
				if($d == TRUE)	{
					return $d;
				}				
		  }
		  
		  public function updateEboard($id,$data){
			  	$this->db->where('id', $id);
			  	$d = $this->db->update('eboard', $data);
			  	if($d == TRUE)	{
			  		return $d;
			  	}
		  }
		  
		  public function getEboardTickets(){
		  	   $str ="SELECT * FROM eboard WHERE type = 1 AND active = 1";
		  	   return $query = $this->db->query($str)->result();
		  }
		  
		  public function getDuplicateTicket($id){
		      $str = "SELECT * FROM eboard WHERE ticketID = $id AND type = 1";
		      return $query = $this->db->query($str)->row();
		  }
		  
		  public function getDuplicateMedChange($id){
		  	  $str = "SELECT * FROM eboard WHERE ticketID = $id AND type = 2 ";
		  	  return $query = $this->db->query($str)->row();
		  }
		  
		  public function getDuplicateProduction($id){
		  	$str = "SELECT * FROM eboard WHERE ticketID = '".$id."' AND type = 3";
		  	return $query = $this->db->query($str)->row();
		  }
		  
		  public function getInfoBaseOnEboardId($id){
		  	  $str = "SELECT * FROM eboard WHERE id =$id";
		  	  return $query = $this->db->query($str)->row();
		  }
		  
		  public function getInfoBaseOnEboardOrderId($ticketId,$type){
		  	$str = "SELECT * FROM eboard WHERE ticketID ='$ticketId' AND type=$type AND userID > 0";
		  	return $query = $this->db->query($str)->row();
		  }
		  
		  public function getEboardTicketsNHWard(){
		  	   $str ="SELECT NHID,NHWardID, dateOfDelivery FROM eboard  WHERE NHID IS NOT NULL AND userID IS NULL AND active = 1  GROUP BY NHWardID, dateOfDelivery ORDER BY dateOfDelivery ASC";
			   return $query = $this->db->query($str)->result();
		  }
		  
		  public function getEboardTicketsNHWardByDay($day){
		  	$str ="SELECT NHID,NHWardID, dateOfDelivery FROM eboard  WHERE NHID IS NOT NULL AND userID IS NULL AND active = 1 AND DAYNAME(dateOfDelivery) = '$day'  GROUP BY NHWardID, dateOfDelivery ORDER BY dateOfDelivery ASC";
		  	return $query = $this->db->query($str)->result();
		  }
		  
		  public function getEboardMedChangeNHWard(){
		  	   $str ="SELECT NHID,NHWardID, dateOfDelivery FROM eboard  WHERE NHID IS NOT NULL AND userID IS NULL AND active = 1 AND type =2 GROUP BY NHWardID, dateOfDelivery ORDER BY dateOfDelivery ASC";
			   return $query = $this->db->query($str)->result();
		  }
		  
		  public function getLocalIndividual(){
		  	    $str ="SELECT eboard.id, pat.LastName, pat.FirstName FROM eboard
					LEFT JOIN tickets ON eboard.ticketID = tickets.ticketID
					LEFT JOIN pat ON tickets.patID = pat.ID 
					WHERE eboard.NHID =59  AND eboard.userID IS NULL AND eboard.active = 1 ORDER BY pat.LastName";
			   return $query = $this->db->query($str)->result();
		  }
		  
		  public function countNumberOfWard($nhwardid,$date){
		  	   $str = "SELECT COUNT(*) AS t FROM eboard WHERE NHWardID =".$nhwardid." AND userID IS NULL AND active = 1 AND dateOfDelivery = '".$date."' GROUP BY dateOfDelivery";
			   return $query = $this->db->query($str)->row();
		  }
		  
		  public function countNumberOfWardByChecked($nhwardid,$date){
		  	$str = "SELECT COUNT(*) AS t1 FROM eboard WHERE NHWardID =".$nhwardid." AND userID IS NULL AND active = 1 AND dateOfDelivery = '".$date."' AND eboard.check = 1 GROUP BY dateOfDelivery";
		  	return $query = $this->db->query($str)->row();
		  }
		  
		  public function gatpatInfoBaseOnNhIdAndNhWardId($nhid,$nhwardid,$date){
		  	   $str = "SELECT eboard.id, pat.LastName, pat.FirstName FROM eboard
						LEFT JOIN tickets ON tickets.ticketID = eboard.ticketID
						LEFT JOIN pat ON tickets.patID = pat.ID
						WHERE eboard.NHID = $nhid AND eboard.NHWardID = $nhwardid AND eboard.userID IS NULL AND eboard.active = 1 AND eboard.dateOfDelivery = '".$date."'";
			   return $query = $this->db->query($str)->result();
		  }
		  
		  public function getInfomationBaseOnNHIDandTypeAndDay($wardId,$diffDays,$type,$weekDay){
		  	  $str = "SELECT * FROM eboard WHERE NHWardID =$wardId AND DAYNAME(dateOfDelivery) = '".$weekDay."' AND type =$type AND active = 1 AND DATEDIFF(dateOfDelivery,NOW()) = $diffDays";
		  	  return $query = $this->db->query($str)->result();
		  }
		  
		  public function getAllInfoBaseOnEboardId($ticketIdInList){
		  	 $str ="SELECT eboard.id, eboard.dateOfDelivery, pat.LastName, pat.FirstName, tickets.Description FROM eboard
				LEFT JOIN tickets ON tickets.ticketID = eboard.ticketID
				LEFT JOIN pat ON tickets.patID = pat.ID
				WHERE eboard.id IN ($ticketIdInList)";
		  	 return $query = $this->db->query($str)->result();
		  }
		  
		  public function getMedChangeInformationBaseOnEboardId($id){
		  	  $str = "SELECT eboard.id, eboard.dateOfDelivery, pat.LastName, pat.FirstName, medchanges.actionText, medchanges.changeText, medchanges.spares, medchanges.mar, medchanges.extraLabels, medchanges.numOfStripts FROM eboard
					LEFT JOIN medchanges ON eboard.ticketID = medchanges.medChangeID
					LEFT JOIN pat ON medchanges.patID = pat.ID
					WHERE eboard.id IN ($id)";
		  	  return $query = $this->db->query($str)->result();
		  }
		  
		  public function getAllDetailsInEboardBaseOnNhWardId($nhward){
		  	   $str ="SELECT eboard.id, eboard.ticketID, eboard.type, eboard.dateOfDelivery, eboard.check, pacmed.event FROM eboard
					LEFT JOIN pacmed ON pacmed.orderNum = eboard.ticketID
					WHERE eboard.userID IS NULL AND eboard.active = 1 AND eboard.NHWardID =$nhward ORDER BY eboard.dateOfDelivery ASC";
		  	   return $query = $this->db->query($str)->result();
		  }
		  
		  public function getAllCheckedList(){		  	
		  	   $str = "SELECT * FROM eboard WHERE eboard.check = 1 AND type = 3 AND userID IS NULL AND active = 1";
		  	   return $query = $this -> db -> query($str) -> result();
		  }
		  		  
		  //************************************************************************************************************************//
		  //************************************************************************************************************************//
		  //*******  General Tables  ********//
		  //************************************************************************************************************************//
		  //************************************************************************************************************************//
	  
          public function getAllDepartments(){
          	   $str ="SELECT * FROM department ORDER BY depName ASC";
          	   return $query = $this->db->query($str)->result();
          }
          
          public function getAllTypeOfError(){
          	   $str ="SELECT * FROM typeoferror ORDER BY name ASC";
          	   return $query = $this->db->query($str)->result();
          }
          
          public function addPrintEboard($data){
	           $d = $this->db->insert('printeboard', $data);
	           if($d == TRUE)	{
	          		return $d;
	          } 
          }
          
          public function getPrintId($id){
          	   $str = "SELECT * FROM printeboard WHERE eBoardIdList ='".$id."' ORDER BY id DESC LIMIT 1";
          	   return $query = $this->db->query($str)->row();
          }
          
          public function getInfoBaseOnPrintId($id){
          	   $str = "SELECT * FROM printeboard WHERE id =$id";
          	   return $query = $this->db->query($str)->row();
          }
          
          
          //************************************************************************************************************************//
          //************************************************************************************************************************//
          //*******  Error Details Tables  ********//
          //************************************************************************************************************************//
          //************************************************************************************************************************//
          
          public function getErrorInfoBaseOnErrorId($eId){
          	   $str = "SELECT * FROM errorinfo WHERE id=$eId";
          	   return $query = $this->db->query($str)->row();
          }
          
          public function addErrorDetails($data){  
          	 
          	  $d = $this->db->insert('errorinfo', $data); 
         	  if($d == TRUE)	{ return $d; }
          }
          
          public function updateErrorDetails($id,$data){
	          $this->db->where('id', $id);
	          $d = $this->db->update('errorinfo', $data);
	          if($d == TRUE)	{ return $d; }
          }
          
          public function getAllErrorInformation(){
          	   $str ="SELECT e.id, e.description, e.resolution, e.sourceOferror, dep.depName, te.name AS typeOferror, pat.LastName, pat.FirstName
					FROM errorinfo AS e
					LEFT JOIN department AS dep ON e.departmentId = dep.id
					LEFT JOIN typeoferror AS te ON e.typeOferrorId = te.id
					LEFT JOIN pat ON e.patId = pat.ID  ORDER BY e.addDate1 DESC";
          	   return $query = $this->db->query($str)->result();
          	
          }
          
          public function getAllErrorInformationEid($id){
          	$str ="SELECT e.id, e.description, e.resolution, e.sourceOferror, e.addDate1, dep.depName, te.name AS typeOferror, pat.LastName, pat.FirstName
					FROM errorinfo AS e
					LEFT JOIN department AS dep ON e.departmentId = dep.id
					LEFT JOIN typeoferror AS te ON e.typeOferrorId = te.id
					LEFT JOIN pat ON e.patId = pat.ID WHERE e.id =$id";
          	return $query = $this->db->query($str)->row();
          	 
          }
          
          public function getNumberOferrorBaseOnDepartmentId($id){
          	   $str ="SELECT COUNT(*) AS total,  MONTH(addDate1) AS mon FROM errorinfo WHERE departmentId = $id  GROUP BY year(now()), MONTH(addDate1)";
          	   return $query = $this->db->query($str)->result();
          }
          
          public function getNumberOferrorBaseOnDepartmentIdAndMonth($id,$mon){
          	   $str ="SELECT COUNT(*) AS total,  MONTH(addDate1) AS mon FROM errorinfo WHERE departmentId = $id AND MONTH(addDate1) = $mon GROUP BY year(now()), MONTH(addDate1)";
          	   return $query = $this->db->query($str)->row();
          }
          
          public function getAllErrorInfo(){
          	   $str ="SELECT ei.description, ei.resolution, ei.sourceOferror, typeoferror.name, department.depName FROM errorinfo AS ei 
					LEFT JOIN typeoferror ON typeoferror.id =  ei.typeOferrorId
					LEFT JOIN department ON department.id = ei.departmentId
					WHERE MONTH(ei.addDate2) = MONTH(NOW()) ORDER BY ei.addDate1 DESC";
          	   
          	   return $query = $this->db->query($str)->result();
          }
          
          
          
          //************************************************************************************************************************//
          //************************************************************************************************************************//
          //*******  Sent info to Pharmacy database  ********//
          //************************************************************************************************************************//
          //************************************************************************************************************************//
          
          
          public function addSentToLocalEboard($data){
          	   $d = $this->db->insert('sendticketandmedchangetolocaleboard',$data); 
          	   if($d == TRUE) {return $d; }
          }
          
          public function updateSentToLocalEboard($id,$data){
          	   $this->db->where('id',$id);
          	   $d = $this->db->update('sendticketandmedchangetolocaleboard',$data);
          	   if($d == TRUE) { return $d; }
          }
                   
	}


?>