<?php
class pharm extends CI_Model {
	function __construct() {
		// Call the Model constructor
		parent::__construct ();
		//$this->db_pharm = $this->load->database ( 'pharm', TRUE );
		$this->db_sync = $this->load->database ( 'sync', TRUE );
	}
	public function ImportCompliancePatients() {
		// connect to pharmacy db, grab patid and initials where home
		$v = $this->db_sync->query ( "select * from dbo.Pat a where a.NHID >0 and a.Active= 1" );
		foreach ( $v->result () as $pat ) {
			// insert a new record in compliance patient
			
			$patCheck = TRUE;
			$v = $this->db->query ( "Select * from compliancepatient where patID = ?", array (
					$pat->ID 
			) );
			if ($v->num_rows () > 0) {
				$patCheck = TRUE;
			} else {
				$patCheck = FALSE;
			}
			
			// check to see if patient exists
			if ($patCheck == FALSE) {
				$data = array (
						// 'note' => '',
						// 'deliveryInstructions' => '',
						'deliver' => 0,
						'controlled' => 0,
						'deliveryTimeID' => 0,
						'deliveryHomeID' => 0,
						'patID' => $pat->ID,
						'patInitials' => substr ( $pat->FirstName, 0, 1 ) . substr ( $pat->LastName, 0, 1 ) 
				);
				
				if ($this->db->insert ( 'compliancepatient', $data )) {
				echo 'SUCCESS: ' . substr ( $pat->FirstName, 0, 1 ) . substr ( $pat->LastName, 0, 1 ) . $pat->ID;
				} else {
				echo '||==xxxFAILED: ' . substr ( $pat->FirstName, 0, 1 ) . substr ( $pat->LastName, 0, 1 ) . $pat->ID;
				}
				echo 'INSERTING: ' . $pat->ID . substr ( $pat->FirstName, 0, 1 ) . substr ( $pat->LastName, 0, 1 )  . '<br>';
			} else {
			}
		}
	}
	public function ImportMedChanges() {
		// connect to pharmacy db, grab patid and initials where home
		$v = $this->db_pharm->query ( "
				select ChangeID, PatID,date, Change, Action, TechnicianCompleted, PharmacistChecked , PharmacyID  
				from dbo.tblMedChanges
				WHERE PatID > 0 AND (PharmacyID= 2 OR PharmacyID IS NULL)
				" );
		foreach ( $v->result () as $chg ) {
			// insert a new record in compliance patient
			
			$techID;
			$pharmID;
			switch ($chg->TechnicianCompleted) {
				case 'Kim' :
					$techID = 26;
					break;
				case 'Kate' :
					$techID = 27;
					break;
				case 'Ahmed' :
					$techID = 3;
					break;
				case 'Amy' :
					$techID = 29;
					break;
				default :
					$techID = 26;
			}
			switch ($chg->PharmacistChecked) {
				case 'Young Kim' :
					$pharmID = 30;
					break;
				case 'Murad Younis' :
					$pharmID = 31;
					break;
				case 'Murad Y.' :
					$pharmID = 31;
					break;
				case 'Maha Younis' :
					$pharmID = 31;
					break;
				case 'Marwah' :
					$pharmID = 28;
					break;
				case 'Marwah Younis' :
					$pharmID = 28;
					break;
				default :
					$pharmID = 30;
			}
			
			$data = array (
					'patID' => $chg->PatID,
					'dateOfChange' => $chg->date,
					'changeText' => $chg->Change,
					'actionText' => $chg->Action,
					'technicianUserID' => $techID,
					'pharmacistUserID' => $pharmID,
					'active' => '1',
					'startDate' => $chg->date,
					'endDate' => $chg->date,
					'actionComplete' => '1' 
			);
			
			// if ($this->db->insert ( 'medchanges', $data )) {
			// echo '<br> SUCCESS: ' . $chg->PatID;
			// } else {
			// echo '<br>||||x=> FAILED: ' . $chg->PatID;
			// }
		}
	}
	public function userPatientAccess($patID = 0) {
		// return true if user has access to this patient false if no access
		// uses curent loged in user
		if ($patID > 0) {
			$v = $this->db->query ( "
					Select * from compliancepatient where patID IN 
(Select patID from pat_home a
LEFT JOIN user_home b on a.homeID=b.homeID
 where b.userID = ? AND patID=?);
					", array (
					$this->ion_auth->user ()->row ()->id,
					$patID 
			) );
			if ($v->num_rows () > 0) {
				return TRUE;
			} else {
				return FALSE;
			}
		} else {
			return FALSE;
		}
		return TRUE;
	}
	
	// Pat Profile
	public function getPatAlergies($patID) {
		// returns alergies
		$check = TRUE;
		if ($check) {
			$v = $this->db_sync->query ( "
Select A.AllergenDesc As Description
From [dbo].[AllergenGroupMast] A
INNER JOIN [pharmacy].[dbo].[PatAlg] B
ON A.AllergenGroup = B.Code
AND b.CodeType = 1
WHERE b.PatID = ?
UNION
Select b.Description
from [pharmacy].[dbo].PatAlg A
INNER JOIN [dbo].AllergyPickList b
ON A.Code = b.ConceptId
AND a.CodeType = 2
WHERE a.PatID = ?
UNION
SELECT B.IngredientDescription as Description
FROM [pharmacy].[dbo].PatAlg a
INNER JOIN [dbo].AllergyDrugIngredients b
on a.Code = b.HicSeqn
AND a.CodeType = 6 
WHERE a.PatID = ?", array (
					$patID,
					$patID,
					$patID 
			) );
			if ($v->num_rows () > 0) {
				return $v;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	public function getPatConditions($patID) {
		// returns conditions
		$check = TRUE;
		if ($check) {
			$v = $this->db_sync->query ( "
SELECT A.Description
FROM [dbo].[ICD10CAMast] A
INNER JOIN [pharmacy].[dbo].[PatCnd] B
ON B.Code = A.ICD10CA
where B.PatID = ?
AND B.CodeType = '3'
UNION ALL
SELECT FdbdxDesc as Description
FROM [dbo].[MedicalConditionsMast] A
INNER JOIN [pharmacy].[dbo].[PatCnd] B
ON B.Code = A.Fdbdx
where B.PatID = ?
AND B.CodeType = '1'", array (
					$patID,
					$patID 
			) );
			if ($v->num_rows () > 0) {
				return $v;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	public function getPatCompliancePackage($patID) {
		$check = TRUE;
		if ($check) {
			$v = $this->db_sync->query ( "
	SELECT   a_1.RxNum, a_1.DocID, a_1.PatID, b.BrandName, 
	b.GenericName, b.Strength, c.SIG, d.FirstName + ' ' + d.LastName AS 'doc', c.DIN
		FROM        (SELECT   a.OrigRxNum, a.PatID, a.DocID, MAX(a.RxNum) AS RxNum
					FROM pharmacy.dbo.Rx AS a 
					WHERE a.Inactive=0
					GROUP BY a.OrigRxNum, a.PatID, a.DocID) AS a_1  

	INNER JOIN
	pharmacy.dbo.Rx AS c ON a_1.RxNum = c.RxNum 
	INNER JOIN
	pharmacy.dbo.Drg AS b ON c.DrgID = b.ID 
	INNER JOIN
	pharmacy.dbo.Doc AS d ON a_1.DocID = d.ID
	WHERE 
	(c.NHBatchFill = 1) AND (c.Inactive = 0) AND a_1.PatID = ? 
	ORDER BY b.BrandName ASC
 ", array (
					$patID 
			) );
			if ($v->num_rows () > 0) {
				return $v;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
		// USER THIS TO FIND OUT DRUGS WE WANT TO OMIT= select * from drg a WHERE BrandName LIKE '%double%'
	public function getPatPRNs($patID) {
		$check = TRUE;
		if ($check) {
			$v = $this->db_sync->query ( "
SELECT     c.DrgID, a_1.RxNum, a_1.DocID, a_1.PatID, b.BrandName, b.GenericName, b.Strength, c.SIG, d.FirstName + ' ' + d.LastName AS 'doc'
	FROM         (SELECT   a.OrigRxNum, a.PatID, a.DocID, MAX(a.RxNum) AS RxNum
	FROM          pharmacy.dbo.Rx AS a 
	GROUP BY   a.OrigRxNum, a.PatID, a.DocID) AS a_1 INNER JOIN
	pharmacy.dbo.Rx AS c ON a_1.RxNum = c.RxNum INNER JOIN
	pharmacy.dbo.Drg AS b ON c.DrgID = b.ID INNER JOIN	
	pharmacy.dbo.Doc AS d ON a_1.DocID = d.ID
	WHERE     ((c.NHBatchFill = 0) OR c.NHBatchFill IS NULL) AND (c.Inactive = 0) AND a_1.PatID = ? AND c.DrgID NOT IN (
275103,
274000,
276187,
276188,
276190,
276191,
276189,
276975,
276976,
276977,
276978,
276979,
276980,
276961,
276265)
 ", array (
					$patID 
			) );
			if ($v->num_rows () > 0) {
				return $v;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	// AR
	public function getPatAR($patID) {
		return FALSE;
// 		$check = TRUE;
// 		if ($check) {
// 			$v = $this->db_pharm->query ( "
// select a.AccountNum, a.CreditLimit, CONVERT(VARCHAR(10), a.LastStatementDate, 126) as 'LastStatementDate',  sum((c.SubTotal + c.Tax1 + c.Tax2)  - c.Paid ) as 'balance'
// from AR a 
// left join dbo.Pat b on a.ID = b.ARID 
// LEFT JOIN dbo.ARInvoice c on b.ARID = c.ARID
// WHERE b.ID = ? 
// GROUP BY a.AccountNum, a.CreditLimit, a.LastStatementDate", array (
// 					$patID 
// 			) );
// 			if ($v->num_rows () > 0) {
// 				return $v->row ();
// 			} else {
// 				return false;
// 			}
// 		} else {
// 			return false;
// 		}
	}
	public function getPatARInvoices($patID) {
		return FALSE;
// 		$check = TRUE;
// 		if ($check) {
// 			$v = $this->db_pharm->query ( "SELECT InvoiceNum, CONVERT(VARCHAR(10), InvoiceDate, 126) as 'InvoiceDate', Status, SubTotal, 
// 											(Tax1 + Tax2) as 'tax',	Paid FROM dbo.ArInvoice a 
// 										LEFT JOIN dbo.Pat b on b.ARID = a.ARID where b.ID = ? AND (status = 0 OR Status = 1 or Status = 2 ) ORDER BY InvoiceNum DESC", array (
// 					$patID 
// 			) );
// 			if ($v->num_rows () > 0) {
// 				return $v;
// 			} else {
// 				return false;
// 			}
// 		} else {
// 			return false;
// 		}
	}
	public function getPatARBalance($patID) {
		return FALSE;
// 		$check = TRUE;
// 		if ($check) {
// 			$v = $this->db_pharm->query ( "
// select sum((c.SubTotal + c.Tax1 + c.Tax2)  - c.Paid ) as 'balance'
// from dbo.Pat b
// LEFT JOIN dbo.ARInvoice c on b.ARID = c.ARID
// WHERE b.ID = ? ", array (
// 					$patID 
// 			) );
// 			if ($v->num_rows () > 0) {
// 				return $v - row ();
// 			} else {
// 				return false;
// 			}
// 		} else {
// 			return false;
// 		}
	}
	public function getPatARInvoiceDetails($invoiceNum) {
		return FALSE;
// 		$check = TRUE;
// 		if ($check) {
// 			$v = $this->db_pharm->query ( "
// SELECT CONVERT(VARCHAR(10), a.Date, 126) as 'Date', a.RxNum, a.Amount, a.Comment from dbo.ARDetail a WHERE a.InvoiceNum = ? ", array (
// 					$invoiceNum 
// 			) );
// 			if ($v->num_rows () > 0) {
// 				return $v;
// 			} else {
// 				return false;
// 			}
// 		} else {
// 			return false;
// 		}
	}
	
	// CHARTING
	public function getPatCharts($patID) {
		$check = TRUE;
		if ($check) {
			$v = $this->db_sync->query ( "
select a.ChartType, CONVERT(VARCHAR(10), a.Date, 126) as 'Date', a.Value1, a.Value2, a.Value3 from dbo.PatChart a
WHERE a.PatID = ? ORDER BY Date ASC", array (
					$patID 
			) );
			if ($v->num_rows () > 0) {
				return $v;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	public function getANCValue($patID) { // 1 = green , 2 = yellow, 3 = red
		$check = TRUE;
		if ($check) {
			$v = $this->db_sync->query ( "
select TOP 1 a.Value1 from dbo.PatChart a
WHERE a.PatID = ? AND a.ChartType = 6 ORDER BY Date DESC", array (
					$patID 
			) );
			if ($v->num_rows () > 0) {
				return $v->row ()->Value1;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	public function getSig($SIG) {
		$sigParts = explode ( ' ', $SIG );
		$realSig = '';
		foreach ( $sigParts as $s ) {
			$d = $this->db->query ( "Select Text from IA_SIG where Token LIKE ?", array (
					$s 
			) );
			if ($d->num_rows () > 0) {
				$realSig .= ' ' . $d->row ()->Text;
			}
		}
		
		return $realSig;
	}
	

	public function getPatDrugPlan($patID = 0) {
		if ($patID > 0) {
			$c = $this->db_sync->query ( "
select
a.PatID, a.Comment,
b.SubPlanCode from PatPln a
left join dbo.PlnSub b on a.SubPlanID = b.ID
left join dbo.Pln c on c.id = b.PlanID
where  a.PatID=? 
AND c.ID != 58
ORDER BY a.PatID;
					", array (
								$patID
						)
			);
			if ($c->num_rows () > 0) {
				return $c->row ();
			} else {
				return false;
			}
		} else {
			// GET USERs pat drug cards
			return false;
		}
	}
	
	public function getPharmViewPatDrugPlans($patID = 0) {
		if ($patID > 0) {
			$c = $this->db_sync->query ( "
select 
DISTINCT (a.PatID), CONVERT(VARCHAR(MAX), a.Comment) as 'comment', b.SubPlanCode 
from PatPln a
left join PlnSub b on a.SubPlanID = b.ID
left join Pln c on c.id = b.PlanID
LEFT JOIN Rx d on d.PatID = a.PatID
where  Active = 1 
AND c.ID != 58
AND c.ID = 33
AND YEAR(d.FillDate) = YEAR(getdate()) 
ORDER BY a.PatID;
					", array (
								$patID
						)
			);
			if ($c->num_rows () > 0) {
				return $c->row ();
			} else {
				return false;
			}
		} else {
			// GET USERs pat drug cards
			return false;
		}
	}
	
	public function getAllPatients(){
		
		return true;		
	}
}