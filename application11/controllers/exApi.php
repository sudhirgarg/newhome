<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class exApi extends CI_Controller {
	
	//private $serverIp = 'http://76.65.220.97:80/';
	private $serverIp = 'http://76.65.220.97:8080/';
	
	function __construct() {
		
		// Call the Model constructor
		parent::__construct();
		
		 $this->rest->initialize(array('server' => $this->serverIp,
			'api_key' => '1245689',
			'api_name' => 'X-API-KEY',
			'http_user' => 'Rajah',
			'http_pass' => 'Hajar',
			'http_auth' => 'basic',
			));
			
		$this->rest->format('application/json');
			
		
	}
	
	public function callpage(){
		
		$this->getPat();
		$this->sentTicketAndMedChangeInfo();	
		$this->getInActivePat();
	}
	
	public function getPat(){
			
		
	    $user = $this->rest->get('production/exApi/user');
    
    	$this->load->model('Pat');
    
    
    	$data = array();
    	foreach($user AS $record){
    		$id = $record->ID;
    
    		$data['FamilyID'] = $record->FamilyID;
    		$data['Code'] =$record->Code;
    		$data['LastName'] =$record->LastName;
    		$data['FirstName'] = $record->FirstName;
    		$data['Address1'] = $record->Address1;
    		$data['Address2'] = $record->Address2;
    		$data['City'] = $record->City;
    		$data['Prov'] = $record->Prov;
    		$data['Postal'] = $record->Postal;
    		$data['Country'] = $record->Country;
    		$data['Birthday'] = $record->Birthday;
    		$data['Sex'] = $record->Sex;
    		$data['Language'] = $record->Language;
    		$data['RxTotalsResetDate'] = $record->RxTotalsResetDate;
    		$data['TotalDollars'] = $record->TotalDollars;
    		$data['TotalRx'] = $record->TotalRx;
    		$data['Weight'] = $record->Weight;
    		$data['Height'] = $record->Height;
    		$data['CreatedOn'] = $record->CreatedOn;
    		$data['LastChanged'] = $record->LastChanged;
    		$data['LastUsed'] = $record->LastUsed;
    		$data['Comment'] = $record->Comment;
    		$data['Salutation'] = $record->Salutation;
    		$data['DefKrollCare'] = $record->DefKrollCare;
    		$data['KrollCare'] = $record->KrollCare;
    		$data['SnapRequested'] = $record->SnapRequested;
    		$data['SnapDocumented'] = $record->SnapDocumented;
    		$data['LargeSig'] = $record->LargeSig;
    		$data['FirstDrugName'] = $record->FirstDrugName ;
    		$data['SecondDrugName'] = $record->SecondDrugName ;
    		$data['DeliveryRoute'] = $record->DeliveryRoute;
    		$data['EMail'] = $record->EMail ;
    		$data['AddressLink'] = $record->AddressLink ;
    		$data['NetworkKeyword'] = $record->NetworkKeyword ;
    		$data['NHID'] = $record->NHID ;
    		$data['PharmanetLog'] = $record->PharmanetLog ;
    		$data['NoKrollCare'] = $record->NoKrollCare ;
    		$data['UnitDoseType'] = $record->UnitDoseType ;
    		$data['UnitDoseCycle'] = $record->UnitDoseCycle ;
    		$data['DocID'] = $record->DocID ;
    		$data['PriceGroup'] = $record->PriceGroup ;
    		$data['PrintCompliance'] = $record->PrintCompliance ;
    		$data['ARID'] = $record->ARID ;
    		$data['NHWardID'] = $record->NHWardID ;
    		$data['NHAdmitDate'] = $record->NHAdmitDate;
    		$data['NHDischargeDate'] = $record->NHDischargeDate;
    		$data['NHDeceasedDate'] = $record->NHDeceasedDate;
    		$data['NHRoom'] = $record->NHRoom;
    		$data['NHBed'] = $record->NHBed;
    		$data['NHLastTMRDate'] = $record->NHLastTMRDate;
    		$data['NHInactive'] = $record->NHInactive;
    		$data['NHDiet'] = $record->NHDiet;
    		$data['NHComment'] = $record->NHComment;
    		$data['AutoRefillStatus'] = $record->AutoRefillStatus;
    		$data['AutoRefillNotification'] = $record->AutoRefillNotification;
    		$data['StoreID'] = $record->StoreID;
    		$data['StoreLocal'] = $record->StoreLocal;
    		$data['LastTMRPrinted'] =$record->LastTMRPrinted;
    		$data['Active'] = $record->Active;
    		$data['UnitDoseStrategyID'] = $record->UnitDoseStrategyID;
    		$data['NetworkKeywordDate'] = $record->NetworkKeywordDate;
    		$data['DefaultNHCycleId'] = $record->DefaultNHCycleId;
    		$data['OCMPin'] = $record->OCMPin;
    		$data['IsAnimal'] = $record->IsAnimal;
    		$data['AnimalType'] =$record->AnimalType;
    		$data['NetworkId'] =$record->NetworkId;
    		$data['AnimalOwnerPatId'] = $record->AnimalOwnerPatId;
    		$data['UnitDosePatPrcGrpId'] = $record->UnitDosePatPrcGrpId;
    		$data['MergedToId'] = $record->MergedToId;
    		$data['PickupNotificationEnrolment'] = $record->PickupNotificationEnrolment;
    		$data['LanguageSpoken'] = $record->LanguageSpoken;
    		$data['DeliveryRouteType'] = $record->DeliveryRouteType;
    		$data['DoubleCount'] = $record->DoubleCount;
    		$data['NetworkIdRoot'] = $record->NetworkIdRoot;
    		$data['VialIdentifier'] = $record->VialIdentifier;
    		$data['PatType'] = $record->PatType;
    		$data['NetworkSynchronizedDate'] = $record->NetworkSynchronizedDate;
    		$data['ID'] = $id;
    		 
    		$d = $this->Pat->getAllDataBaseOnIdRow($id);
    		if(empty($d->pid)){
    			$this->Pat->addData($data);
    		}else{
    			$this->Pat->updateData($id,$data);  		
    		}   
    	}   		
			
		//$this->rest->debug();	
	}
	
	public function getInActivePat(){
		
		$user = $this->rest->get('production/exApi/inActiveUser');
		
		$this->load->model('Pat');
		
		
		$data = array();
		foreach($user AS $record){
			$id = $record->ID;
		
			$data['FamilyID'] = $record->FamilyID;
			$data['Code'] =$record->Code;
			$data['LastName'] =$record->LastName;
			$data['FirstName'] = $record->FirstName;
			$data['Address1'] = $record->Address1;
			$data['Address2'] = $record->Address2;
			$data['City'] = $record->City;
			$data['Prov'] = $record->Prov;
			$data['Postal'] = $record->Postal;
			$data['Country'] = $record->Country;
			$data['Birthday'] = $record->Birthday;
			$data['Sex'] = $record->Sex;
			$data['Language'] = $record->Language;
			$data['RxTotalsResetDate'] = $record->RxTotalsResetDate;
			$data['TotalDollars'] = $record->TotalDollars;
			$data['TotalRx'] = $record->TotalRx;
			$data['Weight'] = $record->Weight;
			$data['Height'] = $record->Height;
			$data['CreatedOn'] = $record->CreatedOn;
			$data['LastChanged'] = $record->LastChanged;
			$data['LastUsed'] = $record->LastUsed;
			$data['Comment'] = $record->Comment;
			$data['Salutation'] = $record->Salutation;
			$data['DefKrollCare'] = $record->DefKrollCare;
			$data['KrollCare'] = $record->KrollCare;
			$data['SnapRequested'] = $record->SnapRequested;
			$data['SnapDocumented'] = $record->SnapDocumented;
			$data['LargeSig'] = $record->LargeSig;
			$data['FirstDrugName'] = $record->FirstDrugName ;
			$data['SecondDrugName'] = $record->SecondDrugName ;
			$data['DeliveryRoute'] = $record->DeliveryRoute;
			$data['EMail'] = $record->EMail ;
			$data['AddressLink'] = $record->AddressLink ;
			$data['NetworkKeyword'] = $record->NetworkKeyword ;
			$data['NHID'] = $record->NHID ;
			$data['PharmanetLog'] = $record->PharmanetLog ;
			$data['NoKrollCare'] = $record->NoKrollCare ;
			$data['UnitDoseType'] = $record->UnitDoseType ;
			$data['UnitDoseCycle'] = $record->UnitDoseCycle ;
			$data['DocID'] = $record->DocID ;
			$data['PriceGroup'] = $record->PriceGroup ;
			$data['PrintCompliance'] = $record->PrintCompliance ;
			$data['ARID'] = $record->ARID ;
			$data['NHWardID'] = $record->NHWardID ;
			$data['NHAdmitDate'] = $record->NHAdmitDate;
			$data['NHDischargeDate'] = $record->NHDischargeDate;
			$data['NHDeceasedDate'] = $record->NHDeceasedDate;
			$data['NHRoom'] = $record->NHRoom;
			$data['NHBed'] = $record->NHBed;
			$data['NHLastTMRDate'] = $record->NHLastTMRDate;
			$data['NHInactive'] = $record->NHInactive;
			$data['NHDiet'] = $record->NHDiet;
			$data['NHComment'] = $record->NHComment;
			$data['AutoRefillStatus'] = $record->AutoRefillStatus;
			$data['AutoRefillNotification'] = $record->AutoRefillNotification;
			$data['StoreID'] = $record->StoreID;
			$data['StoreLocal'] = $record->StoreLocal;
			$data['LastTMRPrinted'] =$record->LastTMRPrinted;
			$data['Active'] = $record->Active;
			$data['UnitDoseStrategyID'] = $record->UnitDoseStrategyID;
			$data['NetworkKeywordDate'] = $record->NetworkKeywordDate;
			$data['DefaultNHCycleId'] = $record->DefaultNHCycleId;
			$data['OCMPin'] = $record->OCMPin;
			$data['IsAnimal'] = $record->IsAnimal;
			$data['AnimalType'] =$record->AnimalType;
			$data['NetworkId'] =$record->NetworkId;
			$data['AnimalOwnerPatId'] = $record->AnimalOwnerPatId;
			$data['UnitDosePatPrcGrpId'] = $record->UnitDosePatPrcGrpId;
			$data['MergedToId'] = $record->MergedToId;
			$data['PickupNotificationEnrolment'] = $record->PickupNotificationEnrolment;
			$data['LanguageSpoken'] = $record->LanguageSpoken;
			$data['DeliveryRouteType'] = $record->DeliveryRouteType;
			$data['DoubleCount'] = $record->DoubleCount;
			$data['NetworkIdRoot'] = $record->NetworkIdRoot;
			$data['VialIdentifier'] = $record->VialIdentifier;
			$data['PatType'] = $record->PatType;
			$data['NetworkSynchronizedDate'] = $record->NetworkSynchronizedDate;
			$data['ID'] = $id;
			 
			$d = $this->Pat->getAllDataBaseOnIdRow($id);
			if(empty($d->pid)){
				$this->Pat->addData($data);
			}else{
				$this->Pat->updateData($id,$data);
			}
		}
		
		//$this->rest->debug();
		
	}
	
   
    public function sentTicketAndMedChangeInfo(){
   
	   	$this->load->model('tickets');
	   
	   
	   	$infoApi = $this -> tickets -> getInfoToLocalEboard();	   	 	  	   	
	   	
	   	if(!empty($infoApi)){
	   
		   	$infoAPI = json_encode($infoApi);
		   	$info = $this->rest->post('production/exApi/sentTicket',$infoAPI);
		   	
		   	foreach($infoApi AS $record){
		   		$data = array(
		   				 'ticketId' => $record->TicketID,
		   				 'typeOfTicket' => 1
		   		       );
		   		$this->tickets->addSentToLocalEboard($data);
		   		
		   	}
	   	}else{
	   		echo "No ticket record for to send";
	   	}
	   	
	   	$infoMed = $this -> tickets -> getMedChangeForPharmacyEdord();
	   	
	   	if(!empty($infoMed)){
	   		$infoMED = json_encode($infoMed);
	   		$medInfo = $this->rest->post('production/exApi/sentMedChangeInfo',$infoMED);
	   			   			   		
	   		foreach($infoMed AS $record){
	   			$data = array(
	   					'ticketId' => $record->TicketID,
	   					'typeOfTicket' => 2
	   			);
	   			$this->tickets->addSentToLocalEboard($data);	   			 
	   		}	   		
	   	}else{
	   		echo "No medchange record for to send";
	   	}
	   	
	   	
	   	//$this->rest->debug();	  
	   	
	   	
   }
   
   
	
	
}