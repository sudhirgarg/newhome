<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

include_once ('secure_controller.php');
class homeportal extends Secure_Controller {
	function __construct() {
		// Call the Model constructor
		parent::__construct ();
		if ($this->quick->accessTo ( 'homeportal' )) {
			// do nothing they are legit
		} else {
			// current user should not have access to this controller.
			redirect ( 'news/mynews' );
		}
		
		if (! isset ( $this->session->userdata ['user'] ['language'] )) {
			$data = array (
					'language' => 'english' 
			);
			$this->session->set_userdata ( 'user', $data );
		}
		
		$this->lang->load ( 'portal', $this->session->userdata ['user'] ['language'] );
	}
	public function index() {
		if (! $this->ion_auth->logged_in ()) {
			redirect ( base_url () . 'auth/login' );
		}
		
		$this->load->view ( 'homeportal/header' );
		$this->load->view ( 'homeportal/index' );
		$this->load->view ( 'homeportal/footer' );
	}
	public function patients() {
		$this->load->view ( 'homeportal/header' );
		$this->load->view ( 'homeportal/patients' );
		$this->load->view ( 'homeportal/footer' );
	}
	public function viewPat() {
		$check = $this->pharm->userPatientAccess ( $this->uri->segment ( 3 ) );
		
		if ($check == FALSE) {
			redirect ( base_url () );
		} else {
			$this->load->view ( 'homeportal/header' );
			$this->load->view ( 'homeportal/viewPatient' );
			$this->load->view ( 'homeportal/footer' );
		}
	}
	public function viewPatTest() {
		$check = $this->pharm->userPatientAccess ( $this->uri->segment ( 3 ) );
		
		if ($check == FALSE) {
			redirect ( base_url () );
		} else {
			$this->load->view ( 'homeportal/header' );
			$this->load->view ( 'homeportal/Copy of viewPatient' );
			$this->load->view ( 'homeportal/footer' );
		}
	}
	
	// events
	public function userEvents() {
		$this->load->view ( 'homeportal/header' );
		$this->load->view ( 'homeportal/userEvents' );
		$this->load->view ( 'homeportal/footer' );
	}
	public function clientEvents() {
		$check = $this->pharm->userPatientAccess ( $this->uri->segment ( 3 ) );
		
		if ($check == FALSE) {
			redirect ( base_url () );
		} else {
			$this->load->view ( 'homeportal/header' );
			$this->load->view ( 'homeportal/clientEvents' );
			$this->load->view ( 'homeportal/footer' );
		}
	}

    public function testclientEvents() {
        $this->load->view ( 'homeportal/header' );
        $this->load->view ( 'homeportal/testclientEvents' );
        $this->load->view ( 'homeportal/footer' );
    }


	public function saveEvent() {
		echo $this->portal->updateEvent ( $this->input->post ( 'eventID' ), $this->input->post ( 'eventTypeID' ), $this->input->post ( 'eventDate' ), $this->input->post ( 'reportedByUser' ), $this->input->post ( 'desc' ), $this->input->post ( 'patID' ) );
	}
	public function savePRNMonitor() {
		echo $this->portal->updatePRNMonitor ( $this->input->post ( 'eventID' ), $this->input->post ( 'prn1desc' ), $this->input->post ( 'prn2given' ), $this->input->post ( 'prn2desc' ), $this->input->post ( 'protocol' ), $this->input->post ( 'timeEffective' ), $this->input->post ( 'staffInitial' ) );
	}
	public function eventModal() {
		$this->load->view ( 'homeportal/eventModal' );
	}
	public function prnAdminModal() {
		$this->load->view ( 'homeportal/prnAdminModal' );
	}
	
	// incidents
	public function incidents() {
		$this->load->view ( 'homeportal/header' );
		$this->load->view ( 'homeportal/incidentsDash' );
		$this->load->view ( 'homeportal/footer' );
	}
	public function viewincident() {
		$this->load->view ( 'homeportal/header' );
		$this->load->view ( 'homeportal/incidents/viewIncident' );
		$this->load->view ( 'homeportal/footer' );
	}
	public function users() {
		$this->load->view ( 'homeportal/header' );
		$this->load->view ( 'homeportal/users' );
		$this->load->view ( 'homeportal/footer' );
	}
	
	// TICKETS
	public function tickets() {
		$this->load->view ( 'homeportal/header' );
		$this->load->view ( 'homeportal/tickets' );
		$this->load->view ( 'homeportal/footer' );
	}
	public function viewTicket() {
		$this->load->view ( 'homeportal/header' );
		$this->load->view ( 'homeportal/viewTicket' );
		$this->load->view ( 'homeportal/footer' );
	}
	public function editTicketModal() {
		$this->load->view ( 'homeportal/editTicketModal' );
	}
	public function saveTicket() {
		// email confirmation
		$this->load->library ( 'email' );
		$this->email->from ( 'portal@seamlesscare.ca', 'SEAMLESS HOME PORTAL' );
		$this->email->to ( 'refill@seamlesscare.ca' . ', ' . $this->ion_auth->user ()->row ()->email );
		$this->email->subject ( 'New ticket for patient id: ' . $this->input->post ( 'patID' ) );
		
		$this->email->message ( "Patient ID:  " . $this->input->post ( 'patID' ) . "/t/t" . date ( 'l jS \of F Y h:i:s A' ) . "\r\n\r\nTicket Description:\r\n\r\n" . $this->input->post ( 'Description' ) );
		$this->email->send ();
		echo $this->comp->updateTicket ( $this->input->post ( 'ticketID' ), $this->input->post ( 'patID' ), $this->input->post ( 'ticketTypeReferenceID' ), $this->input->post ( 'Description' ), $this->input->post ( 'active' ) );
	}
	public function addComment() {
		// need users attached to clients. ticket is 1-1 association with client.
		$bccmailString = '';
		// retrieve users email address attached to clients using ticket id
		$userEmails = $this->comp->getUsersEmailsByTicketID ( $this->input->post ( 'ticketID' ) );
		if ($userEmails) {
			$bccmailString = '' ;
			foreach ( $userEmails->result() as $userEmail ) {
				$bccmailString = $bccmailString . $userEmail->email . ',';
			}
			$bccmailString = substr($bccmailString, 0, -1);
		}
		
		// email confirmation
		$this->load->library ( 'email' );
		$this->email->from ( 'portal@seamlesscare.ca', 'Seamless Care Portal' );
		$this->email->to ( 'refill@seamlesscare.ca' . ', ' . $this->ion_auth->user ()->row ()->email );
		$this->email->bcc ( $bccmailString );
		$this->email->subject ( 'New comment was added to ticket#: ' . $this->input->post ( 'ticketID' ) );
		$this->email->message ( 'New Comment: ' . $this->input->post ( 'message' ));
		$this->email->send ();
		echo $this->comp->updateDiscussion ( $this->input->post ( 'ticketID' ), $this->input->post ( 'message' )  );
	}
	public function createReOrder() {
		echo $this->comp->updateTicket ( '-1', $this->input->post ( 'patID' ), '2', 'Please reorder the following RXs: ' . $this->input->post ( 'rxs' ), '1' );
		$this->load->library ( 'email' );
		$this->email->from ( 'portal@seamlesscare.ca', 'Seamless Care Portal' );
		$this->email->to ( 'refill@seamlesscare.ca' . ', ' . $this->ion_auth->user ()->row ()->email );
		$this->email->subject ( 'PRN Medication Reorder Created By: ' . $this->ion_auth->user ()->row ()->first_name . ' ' . $this->ion_auth->user ()->row ()->last_name );
		$this->email->message ( 'PRN Medication Reorder Created By: ' . $this->ion_auth->user ()->row ()->first_name . ' ' . $this->ion_auth->user ()->row ()->last_name . "\r\n\r\n The following RXs have been reordered: " . $this->input->post ( 'rxs' ) . "\r\n\r\nFor Patient ID: " . $this->input->post ( 'patID' ) );
		$this->email->send ();
	}
	
	// invoice
	public function viewInvoiceModal() {
		$this->load->view ( 'compliance/viewInvoiceModal' );
	}
	public function editIncidentModal() {
		$this->load->view ( 'homeportal/editIncidentModal' );
	}
	
	// MedChange
	public function editMedChangeModal() {
		$this->load->view ( 'homeportal/editMedChangeModal' );
	}
	public function printMedChange() {
		$this->load->view ( 'compliance/printMedChange' );
	}
	public function printPatProfile() {
		$this->load->view ( 'homeportal/reports/printDrReport' );
	}
	public function DrugInfoModal() {
		$this->load->view ( 'compliance/DrugInfoModal' );
	}
	public function pharmacyInfo() {
		$this->load->view ( 'homeportal/pharmacyInfo' );
	}
	public function feedback() {
		$this->load->view ( 'homeportal/feedback' );
	}
	// languages
	public function english() {
		$data = $this->session->userdata ( 'user' );
		$data ['language'] = "english";
		$this->session->set_userdata ( 'user', $data );
		
		$this->lang->load ( 'portal', $this->session->userdata ['user'] ['language'] );
		redirect ( 'homeportal/index' );
	}
	public function chinese() {
		$data = $this->session->userdata ( 'user' );
		$data ['language'] = "chinese";
		$this->session->set_userdata ( 'user', $data );
		
		$this->lang->load ( 'portal', $this->session->userdata ['user'] ['language'] );
		redirect ( 'homeportal/index' );
	}
	public function simplifiedchinese() {
		$data = $this->session->userdata ( 'user' );
		$data ['language'] = "simplifiedchinese";
		$this->session->set_userdata ( 'user', $data );
		
		$this->lang->load ( 'portal', $this->session->userdata ['user'] ['language'] );
		redirect ( 'homeportal/index' );
	}
	public function korean() {
		$data = $this->session->userdata ( 'user' );
		$data ['language'] = "korean";
		$this->session->set_userdata ( 'user', $data );
		
		$this->lang->load ( 'portal', $this->session->userdata ['user'] ['language'] );
		redirect ( 'homeportal/index' );
	}
	
	
	// CALENDAR
	
	public function schedules(){
        $this->load->view ( 'homeportal/header'  );
        $this->load->view ( 'homeportal/calendar/home');
        $this->load->view ( 'homeportal/footer'  );
    }


	 public function eventUpdate(){
		 $title = $_POST['title'];
		 $calendarId = $_POST['calendarId'];
		 $eventHome = $_POST['eventHome'];
		 if($eventHome == ""){
			 $eventHome = 0;
		 }
		 $eventPat = $_POST['eventPat'];
		 if($eventPat == ""){
			 $eventPat = 0;
		 }
		 $scheduleDate =$_POST['scheduleDate'];
		 $scheduleTime = $_POST['scheduleTime'];
		 $timeArray = explode(" ", $scheduleTime);
		 if($timeArray[1] == "PM"){
			 $tArray = explode(":", $timeArray[0]);
			 $hour = $tArray[0]+12;
			 $min = $tArray[1];
		 }else{
			 $tArray = explode(":", $timeArray[0]);
			 $hour = $tArray[0];
			 $min = $tArray[1];
		 }

		 if(count($hour) == 1){
			 $hour = "0".$hour;
		 }

		 $newTime = $hour.":".$min.":00";

		 $dateTimeStamp = $scheduleDate." ".$newTime;
		 $description = $_POST['description'];
		 $userID = $this->ion_auth->user ()->row ()->id;
		 $updResultMsg = $this->portal->UpdateCalendarEvent($calendarId,$dateTimeStamp, $description, $eventPat, $eventHome, $userID, $title);
		 $updResults = explode("|", $updResultMsg);
		 $resultMsg = $updResults[1];
		 if($updResults[0] == 1){
			 $dataArray = array(
				 "msg" => "success",
				 "updResult" => $resultMsg,
				 "rtnId" => $calendarId,
				 "rtnCalendarId" => $calendarId,
				 "rtnDesc" => $description,
				 "rtnPatID" => $eventPat,
				 "rtnHomeID" => $eventHome,
				 "rtnStart" => $dateTimeStamp,
				 "rtnTitle" => $title
			 );
		 }else{
			 $dataArray = array(
				 "msg" => "fail",
				 "updResult" => $resultMsg
		 	);
		 }

		 echo json_encode($dataArray);
	 }

	 public function addNewEvent(){
		 $newTitle = $_POST['newTitle'];
		 $calendarId = 0;
		 $homeId = $_POST['homeId'];
		 $patId = $_POST['patId'];
		 $newScheduleDate =$_POST['newScheduleDate'];
		 $newScheduleTime = $_POST['newScheduleTime'];
		 $timeArray = explode(" ", $newScheduleTime);
		 if($timeArray[1] == "PM"){
			 $tArray = explode(":", $timeArray[0]);
			 $hour = $tArray[0];
			 if($hour < 12){
				 $hour = $tArray[0]+12;
			 }elseif($hour == 12){
				 $hour = $tArray[0];
			 }

			 $min = $tArray[1];
		 }else{
			 $tArray = explode(":", $timeArray[0]);
			 $hour = $tArray[0];
			 if($hour == 12){
				 $hour = "00";
			 }else{
				 if(strlen($hour) == 1){
					 $hour = "0".$hour;
				 }
			 }
			 $min = $tArray[1];
		 }

		 $newTime = $hour.":".$min.":00";

		 $newDateTimeStamp = $newScheduleDate." ".$newTime;
		 $newDescription = $_POST['newDescription'];
		 $userID = $this->ion_auth->user ()->row ()->id;
		 $calendarId = $this->portal->AddCalendarEvent($newDateTimeStamp, $newDescription, $patId, $homeId, $userID, $newTitle);
		 if($calendarId != 0){
			 $dataArray = array(
				 "msg" => "success",
				 "rtnId" => $calendarId,
				 "rtnCalendarId" => $calendarId,
				 "rtnDesc" => $newDescription,
				 "rtnPatID" => $patId,
				 "rtnHomeID" => $homeId,
				 "rtnStart" => $newDateTimeStamp,
				 "rtnTitle" => $newTitle
			 );
		 }else{
			 $dataArray = array(
				 "msg" => "fail"
			 );
		 }

		 echo json_encode($dataArray);
	 }

	 public function calendarProcess(){
/*        $dataArray = array(
            "msg" => "success",
            "content" => array(
                array("title" => "All Day Event","evntType"=>array("medicine change", "client visitation"), "start" => "2015-02-01 16:20:00", "description" => "this is just a test text. please don't pay attention to this."),
                array("title" => "All Day March Event", "start" => "2015-03-01 09:40:00", "description" => "March event is here. Let's remember it."),
                array("title" => "My Event", "start" => "2015-03-10 14:30:00", "description" => "fsaef asefasefasef asefasefasefasef"),
                array("title" => "Long Event", "start" => "2015-02-07 14:50:00", "end" => "2015-02-10", "description" => "what is this about? I don't know about this. fseofjseoifjsaeofj cfseo"),
                 array("id" => "999", "title" => "Repeating Event", "start" => "2015-02-09 16:00:00", "description"=>"we need to meet and discuss something about this event. fsejaiofjoasiej fjsaeofj"),
                 array("id" => "999", "title" => "Repeating Event", "start" => "2015-02-16 16:00:00", "description"=>"we need to meet and discuss something about this event. fsejaiofjoasiej fjsaeofj"),
                 array("title" => "Conference", "start" => "2015-02-11", "end" => "2015-02-13"),
                 array("title" => "Meeting", "start" => "2015-02-12T10:30:00", "end" => "2015-02-12T12:30:00"),
                 array("title" => "Lunch", "start" => "2015-02-12T12:00:00"),
                 array("title" => "Meeting", "start" => "2015-02-12T14:30:00"),
                 array("title" => "Happy Hour", "start" => "2015-02-12 17:30:00"),
                 array("title" => "Dinner", "start" => "2015-02-12 20:00:00"),
                 array("title" => "Birthday Party", "start" => "2015-02-13T07:00:00"),
                 array("title" => "Click for Google", "url" => "http://google.com/", "start" => "2015-02-28")
            )
        );*/

		 $startDate = date("Y-m")."-01";
		 $EndDate = date("Y-m")."-".date("t");
		 $eventData = $this->portal->getUserCalendarEvents($startDate, $EndDate)->result();
		 $dataRows = array();
		 if(count($eventData) > 0){
			 foreach($eventData as $e){
				 $calendarId = $e->id;
				 $title = $e->title;
				 $dateTimeStamp = $e->dateTimeStamp;
				 $desc = $e->desc;
				 $patID = $e->patID;
				 $homeID = $e->homeID;

				 $row = array();
				 $row['id'] = $calendarId;
				 $row['calendarId'] = $calendarId;
				 $row['title'] = $title;
				 if($patID != 0){
					 $row['patID'] = $patID;

				 }elseif($homeID != 0){
					 $row['homeID'] = $homeID;
				 }

				 $row['desc'] = $desc;
				 $row['start'] = $dateTimeStamp;
				 array_push($dataRows, $row);
			 }//end of foreach
		 }

		 $dataArray = array(
			 "msg" => "success",
		 	 "content" => $dataRows
		 );

        echo json_encode($dataArray);
    }

	// HEALTH INDICATORS
	public function healthindicatorsdash(){
        $this->load->view ( 'homeportal/header'  );
        $this->load->view ( 'homeportal/healthindicators/healthindicatordash' );
        $this->load->view ( 'homeportal/footer'  );
    }
	
	
	
	public function getPatCompPack() {

		echo '<thead><tr><th>Medication</th><th>Doctor</th><th>Directions</th></tr></thead><tbody>';

		$compPack = $this -> pharm -> getPatCompliancePackage($this -> input -> post('patID'));
		if ($compPack != FALSE) {
			foreach ($compPack->result () as $drg) {
				echo '<tr>';
				echo '<td>' . $drg -> BrandName . ' ' . $drg -> Strength . '<br>RX-' . $drg -> RxNum . '</td>';
				echo '<td>' . $drg -> doc . '</td>';
				echo '<td>' . $drg -> SIG . '</td>';
				echo '</tr>';
			}
		} else {
			echo '<tr><td>No Medication Recorded</td></tr>';
		}

		echo '</tbody>';
	}

	public function getPatPRN() {

		echo '<thead><tr><th>Medication</th><th>Doctor</th><th>Directions</th><th>Comment</th><th>Select</th></tr></thead><tbody>';
		
		$patPRNs = $this->pharm->getPatPRNs ( $this -> input -> post('patID') );
		if ($patPRNs != FALSE) {
			$checkBoxCount = 1;
			foreach ($patPRNs->result () as $patPRN) {
				echo '<tr id="reOrderList">';
				echo '<td>' . $patPRN -> BrandName . ' ' . $patPRN -> Strength . '<br>RX-' . $patPRN -> RxNum . '</td>';
				echo '<td>' . $patPRN -> doc . '</td>';
				echo '<td>' . $patPRN -> SIG . '</td>';
				echo '<td><input type="text" name="reOrderComment" class="form-control reOrderCommentClass" id="comment' . $checkBoxCount . '" ></td>';
				echo '<td><input type="checkbox" name="reOrderCheckBox" class="form-control reOrderCheckBoxClass" value="' . $patPRN -> RxNum . '" id="checkBox' . $checkBoxCount++ . '"></td>';
				echo '</tr>';
			}
		} else {
			echo '<tr><td>No Medication Recorded..</td></tr>';
		}

		echo '</tbody>';

	}
	
	
	//eMAR
	
		// HEALTH INDICATORS
	public function emars(){
        $this->load->view ( 'homeportal/header'  );
        $this->load->view ( 'homeportal/emars' );
        $this->load->view ( 'homeportal/footer'  );
    }

}
?>