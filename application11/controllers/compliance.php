<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

include_once ('secure_controller.php');

class compliance extends Secure_Controller {	
	
	private $host = "http://192.168.15.50:86";
	
	
	function __construct() {
		
		// Call the Model constructor
		parent::__construct();	 
		
		
		if ($this -> quick -> accessTo('compliance')) {
			// do nothing they are legit
		} else {
			// current user should not have access to this controller.
			redirect('news/mynews');
		}
		
		//$this->getPat();
		
	}

    public function index(){
    			
		//$this->getPat();
		
	}
	

	public function getPatDetailsBySearch(){
		
		$in = $this->input->post('info');
		
		 $Rx = $this->rest->get('production/exApi/patSearch/'.$in);	
		 
		 if (!empty($Rx)) {
			foreach ($Rx as $pat) {
				echo "<p><a href='" . base_url("compliance/quickPatView/" . $pat -> ID . "/reorders") . "'>" . $pat -> FirstName . ' ' . $pat -> LastName . '</a></p>';
			}
		}else{
			echo "No Record Match Yet!";
		}
		
		//$this->rest->debug();	
		
	}
		
    /*public function getPatInfoToCombo(){
    	$this -> load -> model('Pat');
		
		$patInfo     = $this -> Pat     -> getAllActivePatInfo();
		
		echo'
		<table>	
			<tr>
				<td>
					<select data-placeholder="Choose Patient..." name="patId" id="patId" class="chosen-select" style="width:180px;" tabindex="2">
					<option value=""></option>';
					foreach($patInfo AS $comboRecord){
						echo '<option value="'.$comboRecord->ID.'">'.$comboRecord->LastName.' '.$comboRecord->FirstName.' ('.$comboRecord->NHBed.')</option>';
					}
					echo'</select>
				</td>
			    <td><button onclick="GoCloud();"> <i class="fa fa-cloud" aria-hidden="true"></i>      </button></td>
				<td><button onclick="GoLocal();"> <i class="fa fa-hospital-o" aria-hidden="true"></i> </button></td>					
		</table>		
		';			
					
    }*/

	public function getPatInfoToCombo(){
    	$this -> load -> model('Pat');
		
		$patInfo     = $this -> Pat     -> getAllActivePatInfo();
		
		echo'
		<table>	
			<tr>
				<td>
					<select name="patId" id="patId" class="form-control select2">
					<option value="">Select Patient...</option>';
					foreach($patInfo AS $comboRecord){
						echo '<option value="'.$comboRecord->ID.'">'.$comboRecord->LastName.' '.$comboRecord->FirstName.' ('.$comboRecord->NHBed.')</option>';
					}
					echo'</select>
				</td>
			    <td><button onclick="GoCloud();" style="padding: 8px 7px;"> <i class="fa fa-cloud" aria-hidden="true"></i>      </button></td>
				<td><button onclick="GoLocal();" style="padding: 8px 9px;"> <i class="fa fa-hospital-o" aria-hidden="true"></i> </button></td>					
		</table>		
		';			
					
    }

	public function getAllHome(){
							
		$homeList = $this->rest->get('production/exApi/listOfHome');
		$this->rest->debug();	
	}

	// dash

	public function dash() {
		$this -> load -> view('header');
		$this -> load -> view('compliance/dash');
		$this -> load -> view('footer');
	}

	// compliance Patient
	public function compliancepatients() {
		$this -> load -> view('header');
		$this -> load -> view('compliance/compliancepatients');
		$this -> load -> view('footer');
	}

	public function viewPat() {
		$patId = $this->uri->segment(3);		
		$this-> load -> model('production/commonModels','commonModels');
		
		$data['patRx']              = $this->rest->get('production/exApi/rxPat/'.$patId);
		$data['rxPatZero']          = $this->rest->get('production/exApi/rxPatZero/'.$patId);
		$data['patInfo']            = $this->rest->get('production/exApi/patInfoBaseOnPatID/'.$patId);
		$data['allergy']            = $this->rest->get('production/exApi/patAllergyBaseOnPataID/'.$patId);
		$data['condition']          = $this->rest->get('production/exApi/patConBaseOnPataID/'.$patId);
		$data['getMedInfo']         = $this->commonModels->getBatchNumberBaseOnPatId($patId);
		$data['getNonPacMedTicket'] = $this->commonModels->getNonPacMedTicketInfoBaseOnPatId($patId);
		$data['getNonPacMedMed']    = $this->commonModels->getNonPacMedMedChangeInfoBaseOnPatId($patId);
		
		
		$this -> load -> view('header');
		//$this -> load -> view('compliance/viewpat');
		$this -> load -> view('compliance/viewpatInNewMethod',$data);
		$this -> load -> view('footer');
	}
	
	public function quickPatView() {
		$patId = $this->uri->segment(3);
		$this-> load -> model('production/commonModels','commonModels');
	
		//$data['patRx']              = $this->rest->get('production/exApi/rxPat/'.$patId);
		$data['rxPatZero']          = $this->rest->get('production/exApi/rxPatZero/'.$patId);
		$data['patInfo']            = $this->rest->get('production/exApi/patInfoBaseOnPatID/'.$patId);
		$data['allergy']            = $this->rest->get('production/exApi/patAllergyBaseOnPataID/'.$patId);
		$data['condition']          = $this->rest->get('production/exApi/patConBaseOnPataID/'.$patId);
		//$data['getMedInfo']         = $this->commonModels->getBatchNumberBaseOnPatId($patId);
		//$data['getNonPacMedTicket'] = $this->commonModels->getNonPacMedTicketInfoBaseOnPatId($patId);
		//$data['getNonPacMedMed']    = $this->commonModels->getNonPacMedMedChangeInfoBaseOnPatId($patId);
	
	
		$this -> load -> view('header');
		//$this -> load -> view('compliance/viewpat');
		$this -> load -> view('compliance/quickPatView',$data);
		$this -> load -> view('footer');
	}
	
	public function getMedDetailsBaseOnPatIdAndBatchNum($patId,$batch){
		
		$medInfo = $this->rest->get('production/exApi/getDrgDetails/'.$patId.'/'.$batch);
		
		foreach($medInfo AS $info){
			echo $info->BrandName.' ('.date('Y-m-d',strtotime($info->StartDate)).' - '.date('Y-m-d',strtotime($info->EndDate)).')<br/>';			
		}
	}
	
	public function getNHWardInfo($id){
				
		$info = $this->rest->get('production/exApi/NHWardInfoFromPatTable/'.$id);
		
		foreach($info AS $record){
			echo '<a href="'.base_url().'compliance/viewPat/'.$record->ID.'/profile">'. $record->LastName.' '.$record->FirstName.'</a><br/>'.$record->Address1.', '.$record->Address2.'<br/>'.$record->PatCity.', '.$record->PatPro.'<br/>'.$record->PatPostal;
			echo "<br/>";
			$getPhone = $this->rest->get('production/exApi/PhoneNumberBaseOnPatID/'.$record->ID);
			
			foreach($getPhone AS $phoneNum){
				echo $phoneNum->Description." : ".$phoneNum->Phone."<br/>";
			}
			
			echo "<hr>";
			
			echo ($record->DeliveryRouteType == 0 ? "Regular Delivery on ": "Pick Up");
			
			if(!empty($record->DeliveryRoute)){
								
				$d = $record->DeliveryRoute;
			
				$day = substr($d, 0,3);
			    $time = substr($d,3,4);
						
			    
				if($day == "MON"){ echo $outp =" MONDAY";    }
				if($day == "TUE"){ echo $outp =" TUESDAY";   }
				if($day == "WED"){ echo $outp =" WEDNESDAY"; }
			    if($day == "THU"){ echo $outp =" THURSDAY";  }
			    if($day == "FRI"){ echo $outp =" FRIDAY";    }
			    
				if($time == "1000"){ echo " @ 10 AM.";   }
				if($time == "1300"){ echo " @ 01 PM.";   }
				if($time == "1500"){ echo " @ 03 PM.";   }	
				
				echo "<br/>";
				$d = strtotime("next $outp");
				echo "Next delivery date is ".date('Y-m-d',$d).".";	
				
			}			
		}
	}

	public function getLocalPatInfo($id){
				
		$info = $this->rest->get('production/exApi/patInfoBaseOnPatID/'.$id);
		
		foreach($info AS $record){
			//echo '<a href="'.base_url().'compliance/viewPat/'.$record->ID.'/profile">'. $record->LastName.' '.$record->FirstName.'</a><br/>'.$record->Address1.', '.$record->Address2.'<br/>'.$record->PatCity.', '.$record->PatPro.'<br/>'.$record->PatPostal;
			echo "<br/>";
			$getPhone = $this->rest->get('production/exApi/PhoneNumberBaseOnPatID/'.$record->ID);
			
			foreach($getPhone AS $phoneNum){
				echo $phoneNum->Description." : ".$phoneNum->Phone."<br/>";
			}
			
			echo "<hr>";
			
			echo ($record->DeliveryRouteType == 0 ? "Regular Delivery on ": "Pick Up");
			
			if(!empty($record->DeliveryRoute)){
								
				$d = $record->DeliveryRoute;
			
				$day = substr($d, 0,3);
			    $time = substr($d,3,4);
						
			    
				if($day == "MON"){ echo $outp =" MONDAY";    }
				if($day == "TUE"){ echo $outp =" TUESDAY";   }
				if($day == "WED"){ echo $outp =" WEDNESDAY"; }
			    if($day == "THU"){ echo $outp =" THURSDAY";  }
			    if($day == "FRI"){ echo $outp =" FRIDAY";    }
			    
				if($time == "1000"){ echo " @ 10 AM.";   }
				if($time == "1300"){ echo " @ 01 PM.";   }
				if($time == "1500"){ echo " @ 03 PM.";   }	
				
				echo "<br/>";
				$d = strtotime("next $outp");
				echo "Next delivery date is ".date('Y-m-d',$d).".";	
				
			}			
		}
	}

	public function getHomeANDdeliveryInfo($id){
		
		$info = $this->rest->get('production/exApi/NHWardInfoFromPatTable/'.$id);
				
		
		foreach($info AS $record){
					
		
			$dR = $record->DeliveryRoute;
			
			$day  = substr($dR,0,3);
		    $time = substr($dR,3,4);
					
		
			if($day == "MON"){ $outp =" MONDAY";    }
			if($day == "TUE"){ $outp =" TUESDAY";   }
			if($day == "WED"){ $outp =" WEDNESDAY"; }
		    if($day == "THU"){ $outp =" THURSDAY";  }
		    if($day == "FRI"){ $outp =" FRIDAY";    }	    	
			
			
			$d = strtotime("next $outp");
			
			echo '<td class="bg-warning">
	 			Home : '.$record->LastName.' '.$record->FirstName . '&nbsp<br> Next RDD : '. date('Y-m-d',$d).'&nbsp<br/>&nbsp;	
	 		</td>';
		}
		
	}
	
	
	
	public function getAllergyInfo($code){
		$this->load->model('allergy');
		$info = $this->allergy->getAllDataBaseOnId($code);
		
		foreach($info AS $record){
			echo $record->allergenDesc;
		}		
	}
	
	public function getConditionsInfo($code){
		
		$this->load->model('allergy');
		$info = $this->allergy->getConditionInfoDataBaseOnId($code);
		
		foreach($info AS $record){
			echo $record->description;
		}
	}
	
	

	public function viewInvoiceModal() {
		$this -> load -> view('compliance/viewInvoiceModal');
	}

	// Tickets
	public function ticketsDash() {
		$this->load->model('tickets');
		
		$data['totalTicketRankMonth'] = $this->comp->getTotalTicketsRankForMonth(); 
		$data['totalTicketRankWeek']  = $this->comp->getTotalTicketsRankForWeeks(); 
		$data['totalTicketRankDay']   = $this->comp->getTotalTicketsRankForDay(); 
		$data['ticketInfo']           = $this->tickets->getActiveTicketsAndInfo();
		$data['notifications']        = $this->comp->getNewComments ();
		
		$this -> load -> view('header');
		$this -> load -> view('compliance/ticketsDash',$data);
		$this -> load -> view('footer');
	}
	
	

    public function getNewTicket($tId){
    	
    	$this->load->model('tickets');
		$info = $this->tickets->getLastTicket($tId);
		
		if(!empty($info)){
			$user_groups = $this->ion_auth->get_users_groups($info->userID)->result();
		
			$gId ="";
			
			foreach($user_groups AS $record){
				$gId = $record->id;
			}
			
			if($gId == 18){				
				echo '<img src="'.base_url().'img/comment.png" width="38">';
			}
		}	
    }
	
	public function ticketsDashRefresh() {
		$this->load->model('tickets');
		$data['totalTicketRankMonth'] = $this->comp->getTotalTicketsRankForMonth(); 
		$data['totalTicketRankWeek']  = $this->comp->getTotalTicketsRankForWeeks(); 
		$data['totalTicketRankDay']   = $this->comp->getTotalTicketsRankForDay(); 
		$data['ticketInfo']           = $this->tickets->getActiveTicketsAndInfo();
		$data['notifications']        = $this->comp->getNewComments ();
		
		$this -> load -> view('header');
		$this -> load -> view('compliance/ticketsDashRefresh',$data);
		$this -> load -> view('footer');
	}

	public function editTicketModal() {
		$this->load->model('Pat');
		
		$data['user'] = $this -> ion_auth -> user() -> row();
		$data['ticketTypeReference'] = $this->comp->getTicketTypeReference ();
		
		if ($this -> uri -> segment(4) > 0) {
			
			$ticketID       = $this -> uri -> segment(4);
			$Ticket         = $this -> comp -> getTicket($this -> uri -> segment(4));
			$data['Ticket'] = $Ticket;
			if(!empty($Ticket)){
				$patID = $Ticket -> patID;
			    $data['patient'] = $this -> Pat -> getPatInfoBaseOnLastIdRow($patID);
				
			}
			
			$data['comments'] = $this->comp->getDiscussion ($ticketID);
			
		} elseif($this -> uri -> segment(3) > 0) {			
			
				$patID = $this -> uri -> segment(3);
				$data['patient'] = $this -> Pat -> getPatInfoBaseOnLastIdRow($patID);
			
		}else{
			redirect('ticketsDash');
		}
		
	
	    $this -> load -> view('compliance/editTicketModal',$data);
	}

	public function archivedTickets() {
		$this -> load -> view('header');
		$this -> load -> view('compliance/archivedTickets');
		$this -> load -> view('footer');
	}

	public function saveTicket() {
		// email confirmation
		$this -> load -> library('email');
		$this -> email -> from('portal@seamlesscare.ca', 'Seamless Care Portal');
		$this -> email -> to('refill@seamlesscare.ca');
		$this -> email -> subject('New ticket#: ' . $this -> input -> post('ticketID'));
		$this -> email -> message('Ticket Description: ' . $this -> input -> post('Description'));
		//$this -> email -> send();
		echo $this -> comp -> updateTicket(
		    $this -> input -> post('ticketID'),
		    $this -> input -> post('patID'),
		    $this -> input -> post('ticketTypeReferenceID'), 
		    $this -> input -> post('Description'), 
		    $this -> input -> post('active'),
		    $this -> input -> post('rxnumList'),
		    $this -> input -> post('deliveryDate'),
		    $this -> input -> post('batchNum')
		);
	}
	
	public function saveTicketTwo() {
		// email confirmation
		$this -> load -> library('email');
		$this -> email -> from('portal@seamlesscare.ca', 'Seamless Care Portal');
		$this -> email -> to('refill@seamlesscare.ca');
		$this -> email -> subject('New ticket#: ' . $this -> input -> post('ticketID'));
		$this -> email -> message('Ticket Description: ' . $this -> input -> post('Description'));
		//$this -> email -> send();
		
		$this -> load -> model('tickets');
		$ticketID              = $this -> input -> post('ticketID');
		$patID                 = $this -> input -> post('patID');
		$ticketTypeReferenceID = $this -> input -> post('ticketTypeReferenceID');
		$description           = $this -> input -> post('Description');
		$active                = $this -> input -> post('active');
		$rx                    = $this -> input -> post('rxnumList');
		$dd                    = $this -> input -> post('deliveryDate');
		$batchNum              = $this -> input -> post('batchNum');
		$getDeliveryDate       = $this -> input -> post('getDeliveryDate');
		$userId                = $this->ion_auth->user()->row()->id;
		
		$data = array (
				'ticketTypeReferenceID' => $ticketTypeReferenceID,
				'Description'           => $description,
				'rxnumList'             => $rx,
				'dateOfDelivery'        => $dd,
				'batchNum'              => $batchNum
				
		);
		
		if($ticketID > 0){
			$info = $this -> tickets -> getAllDataBaseOnTableId($ticketID);
			
			if($info->active == 1){			
				$data2 = array (
						'active' => '0',
						'dateComplete'   => date('Y-m-d H:i:s'),
						'rxnumList'      => $rx,
						'dateOfDelivery' => $dd,
						'batchNum'       => $batchNum,
						'closedBy'       => $userId,
						'closedDate'     => date('Y-m-d H:i:s')
				);
			}else{
				$data2 = array (
						'active' => '0',
						'dateComplete'   => date ( 'Y-m-d H:i:s' ),
						'rxnumList'      => $rx,
						'dateOfDelivery' => $dd,
						'batchNum'       => $batchNum,
						'deliveryDateChangeAfterClose' => $getDeliveryDate.';'.$userId
				);				
			}
		}
		
		$data3 = array (
				'createdBy'             => $userId,
				'dateCreated'           => date ( 'Y-m-d H:i:s' ),
				'patID'                 => $patID,
				'ticketTypeReferenceID' => $ticketTypeReferenceID,
				'Description'           => $description,
				'active' => '1',
				'rxnumList' => $rx
		);
		
		
		
	
	   if ($ticketID > 0) {
	   	
			if ($active > 0) { 
				
				$d = $this -> tickets -> updateData($ticketID,$data);
				if ($d > 0) {
					echo '1|Successfully Updated Ticket';
				} else {
					echo '0|Unknown Error';
				}
				
				
			} else { // Completing/closing ticket
				//print_r($data2);
				
				$d = $this -> tickets -> updateData($ticketID,$data2);
				if ($d > 0) {
					echo '1|Successfully Closed Ticket';
				} else {
					echo '0|An unknown Error Occurred';
				}
				
			}
		  	
		} else { // New Ticket
			
			$d = $this -> tickets -> addData($data3);
			if ($d > 0) {
				echo '1|Successfully Created Ticket';
			} else {
				echo '0|An unknown error occurred... Please try again';
			}
			
		}
		
	}

	public function addComment() {
		
		$this->load->model('tickets');
		$tId = $this -> input -> post('ticketID');
		$info = $this->tickets->getAllDataBaseOnTableId($tId);
		
		if($info->active == 0){
			$data = array( 'active' => 1);
			$this->tickets->updateData($tId,$data);			
		}
		
		// email confirmation
		$this -> load -> library('email');
		$this -> email -> from('portal@seamlesscare.ca', 'Seamless Care Portal');
		$this -> email -> to('refill@seamlesscare.ca');
		$this -> email -> subject('New comment was added to ticket#: ' . $this -> input -> post('ticketID'));
		$this -> email -> message('Comment: ' . $this -> input -> post('message'));
		//$this -> email -> send();
		echo $this -> comp -> updateDiscussion($this -> input -> post('ticketID'), $this -> input -> post('message'));
	}

	// Med Changes
	public function medchanges() {
		
		$this -> load -> model('comp');
		$data['waitingPharmCheck'] = $this->comp->getMedChangesAwaitingPharmacist ();
		$data['waitingAction']     = $this->comp->getMedChangesAwaitingAction ();
		$data['inProgress']        = $this->comp->getMedChangesInProgress ();
		
		$this -> load -> view('header');
		$this -> load -> view('compliance/medchanges',$data);
		$this -> load -> view('footer');
	}
	
	public function survey(){
		
		//$data['homeList'] = $this->rest->get('production/exApi/listOfHome');
		$this -> load -> model('survey');
		$this -> load -> model('Pat');
		$data['homeList'] = $this -> Pat -> getWardAll();
		$data['survey']   = $this -> survey -> getAllServeyRoles();
		 
		// print_r($data['homeList']);
				
		$this -> load -> view('header');
		$this -> load -> view('compliance/survey',$data);
		$this -> load -> view('footer');		
		
	}
	
	public function surveyQuestion(){		
		
		
			$this -> load -> model('survey');
			
			$user = $this -> ion_auth -> user() -> row();
			
			$nhid = $this -> input -> post('nhId');
			$role = $this -> input -> post('roleName');
			
			if($role == "Other") {
				$roles = $this -> input -> post('otherRole');
			}else{
				$roles = $this -> input -> post('roleName');
			}
			
			if(!empty($nhid)){
				$data = array(
					'roles'        => $roles,
					'technitionId' => $user -> id,
					'nhid'         => $nhid,
				    'addDate'      => date('Y-m-d')
				);
				$d = $this -> survey -> addSurveyRoles($data);
				if($d > 0){
					$info = $this -> survey -> getAutoIncrimentId($data);					
					redirect('compliance/surveyQuestionAnswer/'.$info->id);
					
				}else{
					$this -> survey();
				}
			}else{
				$this -> survey();
			}
			
		
	}
	
    public function surveyQuestionAnswer($id){
    	
    	if($id > 0){
	    	$data['roleId'] = $id;
	    	
	    	$this -> load -> model('survey');
	    	
	    	$questionIdArray           = array();
	    	$questionInfoSurveyArray   = array();
	    	
	    	$questionInfo       = $this -> survey -> getAllQuestion();
	    	$questionInfoSurvey = $this -> survey -> getInfoFromSurveyRolesId($id);
	    	
	    	foreach($questionInfo AS $record){
	    		$questionIdArray[] = $record ->id;
	    	}
	    	
	    	foreach($questionInfoSurvey AS $info){
	    		$questionInfoSurveyArray[] = $info ->surveyquestionId;
	    	}
	    	   	
	    	$data['answers'] = $this -> survey -> getAllAnswer();
	    	
	    	if(COUNT($questionInfoSurvey) == 0){
	    		$data['finished'] = 5;
	    		$data['Question'] = $this -> survey -> getOneQuestion();
	    	}else{
	    		//print_r($questionInfoSurvey);
	    		
	    		$arrayDiff = array_diff($questionIdArray,$questionInfoSurveyArray);
	    		
	    		
	    		
	    		$numOfQuestion = COUNT($arrayDiff);
	    		
	    		if(!empty($numOfQuestion)){
	    			$data['Question'] = $this -> survey -> getQuestionNoteInList(implode(',',$arrayDiff));
	    		}	    		
	    		
	    		if($numOfQuestion == 4){
	    			$data['finished'] = 24;
	    		}elseif($numOfQuestion == 3){
	    			$data['finished'] = 43;
	    		}elseif($numOfQuestion == 2){
	    			$data['finished'] = 62;
	    		}elseif($numOfQuestion == 1){
	    			$data['finished'] = 81;
	    		}else{
	    			$data['finished'] = 100;
	    		}
	    		
	    	}
	    	  
	    	    	    	
	    	$this -> load -> view('header');
	    	$this -> load -> view('compliance/surveyQuestionAnswer',$data);
	    	$this -> load -> view('footer');
    	
    	}else{
    		redirect('compliance/survey');
    	}
    	
    	
    }
    
    public function addSurveyQuestionAnswer(){
    	$this -> load -> model('survey');
    	
    	$data = $this -> input -> post();
    	$d = $this -> survey -> addQuestionAndAnswer($data);
    	
    	echo ($d > 0) ? '1' : '2';
    }
    
    public function viewSurvey(){
    	$id = $this -> uri -> segment(3);
    	
    	$this -> load -> model('survey');
    	$data['info'] = $this -> survey -> getSurveyIndivually($id);
    	$data['role'] = $this -> survey -> getDetailsBaseOnId($id);
    	
    	$this -> load -> view('header');
    	$this -> load -> view('compliance/viewSurvey',$data);
    	$this -> load -> view('footer');
    }
    
    public function printSurvey(){
    	$id = $this -> uri -> segment(3);
    	 
    	$this -> load -> model('survey');
    	$data['info'] = $this -> survey -> getSurveyIndivually($id);
    	$data['role'] = $this -> survey -> getDetailsBaseOnId($id);
    	
    	$this ->load -> view('compliance/printSurvey',$data);    	
    	
    }
    
    public function closeSurvey(){
    	$this -> load -> model('survey');
    	$roleId = $this -> input  -> post('roleId');
    	$data = array('complete' => 1);
    	
    	$d = $this -> survey -> updateSurveyRoles($roleId,$data);
    	
    	if($d > 0){
    		echo "1| Succesfully completed";
    	}else{
    		echo "0| Something went wrong, try again";
    	}
    }
    
    public function getSurveyInPersentage($id){
    	
    	$this -> load -> model('survey');
    	$info = $this -> survey -> getInfoFromSurveyRolesId($id);
    	$totalAnswer = 0;
    	foreach($info AS $record){
    		if($record -> surveyanswerId != 1){
    		    $totalAnswer += $record -> surveyanswerId; 
    		}
    	}
    	
    	echo ($totalAnswer/25)*100;
    	
    }
    
    
	public function editMedChangeModal() {
		
		$this->load->model('Pat');
		
		$patID = $this->uri->segment ( 3 );
	    $data['patient'] = $this -> Pat -> getPatInfoBaseOnLastIdRow($patID);
			
		$data['user'] = $this -> ion_auth -> user() -> row();
		$data['ticketTypeReference'] = $this->comp->getTicketTypeReference ();
		
		if ($patID > 0) {
						
			if($this->uri->segment ( 4 )  > 0){ $data['medChange'] = $this->comp->getMedChange ( $this->uri->segment ( 4 ) ); }			
			
		} else {
			
			redirect('compliance/medchanges');
		}
		
		$this -> load -> view('compliance/editMedChangeModal',$data);
	}

	public function saveMedChange() {
		$pId =$this -> input -> post('patID');
		$this->load->model('assignPatToCG');			
		$userId = $this->assignPatToCG->getValidEmailCGIDForTicket($pId);	
		
		
				
		$userEmail = array();
		foreach($userId AS $record){
			$user = $this->ion_auth->user($record->CGID)->row();
			//if($user->email != $this -> ion_auth -> user() -> row() -> email){
				$userEmail[] = $user->email;
			//}
		}

		$emailList = implode(",",$userEmail);
		
	
		$this -> load -> library('email');
		$this -> email -> from('portal@seamlesscare.ca', 'SEAMLESS HOME PORTAL');
		//$this -> email -> to('refill@seamlesscare.ca');
		$this -> email -> to($emailList);
		$this -> email -> subject('New Med change for patient id: ' . $this -> input -> post('patID'));
		$this -> email -> message("Patient ID:  " . $this -> input -> post('patID') . "/t/t" . date('l jS \of F Y h:i:s A') . "\r\n\r\n Med Change  Description:\r\n\r\n" . $this -> input -> post('actionText').$this -> input -> post('changeText'));
		$this -> email -> send();
		
		
		 
	  $ad = array(
	    
		    'spares'      => $this -> input -> post('spares'),
		    'mar'         => $this -> input -> post('marval'),
		    'numOfStripts' => $this -> input -> post('numOfStrips'),
		    'extraLabels' => $this -> input -> post('eLabels'),
		    'nextRDD'     => $this -> input -> post('rdd'),
		    'batchNum'    => $this -> input -> post('batchNum')
		);
		
	
		echo $this -> comp -> updateMedchange(
		
		    $this -> input -> post('medChangeID'),
		    $this -> input -> post('patID'), 
		    $this -> input -> post('dateOfChange'), 
		    $this -> input -> post('changeText'), 
		    $this -> input -> post('actionText'), 
		    $this -> input -> post('technicianUserID'), 
		    $this -> input -> post('pharmacistUserID'), 
		    $this -> input -> post('active'), 
		    $this -> input -> post('startDate'), 
		    $this -> input -> post('endDate'), 
		    $this -> input -> post('actionComplete'),
		    $ad	       		
			
		);
    }

	public function pharmacistCheckMedChange() {
		$pharmacist = $this -> ion_auth -> user() -> row();
		$ad = array();
		echo $this -> comp -> updateMedchange(
			$this -> input -> post('medChangeID'), 
			$this -> input -> post('patID'), 
			$this -> input -> post('dateOfChange'), 
			$this -> input -> post('changeText'), 
			$this -> input -> post('actionText'), 
			$this -> input -> post('technicianUserID'), 
			$pharmacist -> id, 
			$this -> input -> post('active'), 
			$this -> input -> post('startDate'), 
			$this -> input -> post('endDate'), 
			$this -> input -> post('actionComplete'),
		    $ad
		);
	}

	public function actionCompleteMedChange() {
		// $pharmacist = $this->ion_auth->user ()->row ();
		
		$ad = array(
	    
		    'spares'      => $this -> input -> post('spares'),
		    'mar'         => $this -> input -> post('marval'),
		    'numOfStripts' => $this -> input -> post('numOfStrips'),
		    'extraLabels' => $this -> input -> post('eLabels'),
		    'nextRDD'     => $this -> input -> post('rdd'),
		    'batchNum'    => $this -> input -> post('batchNum')
		);
		
		echo $this -> comp -> updateMedchange(
		
		     $this -> input -> post('medChangeID'), 
		     $this -> input -> post('patID'),
		     $this -> input -> post('dateOfChange'), 
		     $this -> input -> post('changeText'), 
		     $this -> input -> post('actionText'),
		     $this -> input -> post('technicianUserID'), 
		     $this -> input -> post('pharmacistUserID'), 
		     $this -> input -> post('active'), 
		     $this -> input -> post('startDate'), 
		     $this -> input -> post('endDate'),
		     $this -> input -> post('actionComplete'),
		     $ad
		);
	
	
	}

    public function actionComleted(){
    	
		$ad = array(
	    
		    'spares'      => $this -> input -> post('spares'),
		    'mar'         => $this -> input -> post('marval'),
		    'numOfStripts' => $this -> input -> post('numOfStrips'),
		    'extraLabels' => $this -> input -> post('eLabels'),
		    'nextRDD'     => $this -> input -> post('rdd'),
		    'batchNum'    => $this -> input -> post('batchNum')
		);
		
	
		
		
		echo $this -> comp -> updateMedchange(
		     $this -> input -> post('medChangeID'), 
		     $this -> input -> post('patID'), 
		     $this -> input -> post('dateOfChange'), 
		     $this -> input -> post('changeText'), 
		     $this -> input -> post('actionText'), 
		     $this -> input -> post('technicianUserID'), 
		     $this -> input -> post('pharmacistUserID'), 
		     $this -> input -> post('active'), 
		     $this -> input -> post('startDate'), 
		     $this -> input -> post('endDate'), 
		     $this -> input -> post('actionComplete'),
		     $ad
		);
		
	
	}
	
	public function printMedChange() {
		
		$this->load->model('Pat');
		
		$patID = $this->uri->segment ( 3 );
	    $data['patient'] = $this -> Pat -> getPatInfoBaseOnLastIdRow($patID);
			
		$data['user'] = $this -> ion_auth -> user() -> row();
		$data['ticketTypeReference'] = $this->comp->getTicketTypeReference ();
		
		if ($patID > 0) {
						
			if($this->uri->segment ( 4 )  > 0){ $data['medChange'] = $this->comp->getMedChange ( $this->uri->segment ( 4 ) ); }			
			
		} else {
			
			redirect('compliance/medchanges');
		}
		
		$this -> load -> view('compliance/printMedChange',$data);
	}
	
	
	public function deleteTicket() {
		$this -> load -> view('compliance/deleteTicket');
	}
	
	public function errorReport(){
		$this -> load -> model('Pat');
		$this -> load -> model('tickets');
		
		$errorId = $this->uri->segment(3);
		
		$data['patInfo']           = $this -> Pat     -> getAllActivePatInfo();
		$data['department']        = $this -> tickets -> getAllDepartments();
		$data['getAllTypeOfError'] = $this -> tickets -> getAllTypeOfError();
		
		$data['errorId'] = $errorId;
		if($errorId > 0) { $data['errorValueInfo'] = $this -> tickets -> getErrorInfoBaseOnErrorId($errorId); }
		
		$this -> load -> view('header');
		$this -> load -> view('compliance/errorReport',$data);
		$this -> load -> view('footer');
		
	}
	
	public function getErrorInfoForChart(){
		
		$this->load->model('tickets');
		$department = $this->tickets->getAllDepartments();
		
		foreach($department AS $info){
			echo'{
				"name": "'.$info->depName.'",
				"data": [';
			     $errorInfo = $this->tickets-> getNumberOferrorBaseOnDepartmentId($info->id);
			     
			     $i = 1;
			     for($i = 1; $i <= 12; $i++){
			     	$errorInfoByMonth = $this->tickets-> getNumberOferrorBaseOnDepartmentIdAndMonth($info->id,$i);
			     	echo (!empty($errorInfoByMonth->total)) ? $errorInfoByMonth->total.',' : '0,';
			     }
			     
			  echo ']
			},';
		}
		
	}
	
	public function addError(){
		
		//header("Access-Control-Allow-Origin: *");
		//header("Content-Type: application/json; charset=UTF-8");
		
		
		$this -> load -> model('tickets');
		$user =  $this->ion_auth->user()->row();
		
		$info = json_decode(file_get_contents("php://input"));
		
		$addDate       = date('Y-m-d', strtotime($info->addDate));
		$description   = $info->description;
		$resolution    = $info->resolution;
		$sourceOferror = $info->sourceOferror;
		
		if((!empty($info->department)) && (!empty($info->patId)) && (!empty($info->typeOferror))){ 
			$data = array(
				'addDate1'      => $addDate,
				'description'   => $description,
				'resolution'    => $resolution,
				'sourceOferror' => $sourceOferror,
				'departmentId'  => $info->department,
				'patId'         => $info->patId,
				'typeOferrorId' => $info->typeOferror,
			    'userId'        => $user->id,
				'addDate2'      => date('Y-m-d')
			);
			
			
			$errorId = $info->errorId;
			$d = "";
			if($errorId > 0){
				
			}else{
				$d = $this -> tickets ->addErrorDetails($data);	
				
				if($d > 0){
					$this -> load  -> library('email');
					$this -> email -> from('portal@seamlesscare.ca', 'SEAMLESS HOME PORTAL - ERROR REPORT');
					$this -> email -> to('team@seamlesscare.ca');
					$this -> email -> subject('New Error report');
					$this -> email -> message(
							"Description :  " . $description . "\r\n\r\n Source Of error : " . $sourceOferror ." \r\n\r\n Resolution : ".$resolution);
					$this -> email -> send();
				}			
			}
			
			echo ($d > 0) ? 'Successfully Update' : 'Something went wrong , please try again later';
			
		
		}else{
			echo "Kindly check department, error type and patient";
		}	
		
	}
	
	public function updateError(){
		
		$this -> load -> model('tickets');
		
		$info = json_decode(file_get_contents("php://input"));
		$id   = $info->errorId;
		
		if($id > 0){
			
			$data = array( 'resolution'    => $info->resolution );
			$d = $this -> tickets ->updateErrorDetails($id,$data);
			echo ($d > 0) ? 'Successfully Update' : 'Something went wrong , please try again later';
			
		}else{
			redirect('compliance/errorReport');
		}
	}
	
	public function getErrorDetails(){
		$this -> load -> model('tickets');
		$errorInfo = $this -> tickets -> getAllErrorInformation();
				
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=UTF-8");
		
		
		$outp = "";
		foreach( $errorInfo AS $record){
			if ($outp != "") {$outp .= ",";}
			$outp .= '{"id":"'               . $record->id             .'",';
			$outp .= '"description":"'       . $record->description    .'",';
			$outp .= '"resolution":"'        . $record->resolution     .'",';
			$outp .= '"sourceOferror":"'     . $record->sourceOferror  .'",';
			$outp .= '"depName":"'           . $record->depName        .'",';
			$outp .= '"typeOferror":"'       . $record->typeOferror    .'",';
			//$outp .= '"addDate":"'           . $record->addDate2       .'",';
			$outp .= '"patname":"'           . $record->LastName .' '. $record->FirstName .'"}';
		}
		$outp ='{"errorDetails":['.$outp.']}';
		
		echo($outp);	
		
		
	}
	
	public function getDisposalDetails(){
	
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=UTF-8");
	
		$this -> load -> model('allergy');
		$info = $this -> allergy -> getAllDisposalInfo();
	
		//$info = json_decode(file_get_contents("php://input"));
		//$patId = $info -> patId;
	
		if($info){
	
			$outPut = "";
				
			foreach($info AS $record){
				if ($outPut != "") {$outPut .= ",";}
				$outPut .= '{"id":"'    . $record->id.'",';
				$outPut .= '"dateCreated":"'  . $record->createdDate.'"}';
			}
				
			$outPut ='{"disposalDetailsInfo":['.$outPut.']}'; //'{"disposalDetailsInfo":['.$outPut.']}';
			echo ($outPut);
			
	
		}else{
			echo $outPut = '"statusText":["no result"]';
		}
	
	}
	
	public function getErrorDetailsById(){
		
		$this-> load -> model('tickets');
		
		$info = json_decode(file_get_contents("php://input"));
		$id = $info->id;
		
		$data = $this-> tickets -> getAllErrorInformationEid($id);
		
		$outp = "";
		$outp .= '{"id":"'               . $data->id             .'",';
		$outp .= '"description":"'       . $data->description    .'",';
		$outp .= '"resolution":"'        . $data->resolution     .'",';
		$outp .= '"sourceOferror":"'     . $data->sourceOferror  .'",';
		$outp .= '"depName":"'           . $data->depName        .'",';
		$outp .= '"typeOferror":"'       . $data->typeOferror    .'",';
		$outp .= '"addDate":"'           . $data->addDate1       .'",';
		$outp .= '"patname":"'           . $data->LastName .' '. $data->FirstName .'"}';
		
		$outp ='{"errorDetailsIndi":['.$outp.']}';
		
		echo($outp);
		
		
	}
	
	
	public function confirmDeleteTicket() {
		
		$id= $this->input->post('id');	
		$this->load->model('comp');
		$this->comp->deleteTicket($id);
		
	}
	
	// Homes
	
	public function profile(){
		
		$this->load->model('emailauth');
		$user = $this->ion_auth->user()->row();
		$info = array('userId' => $user->id);
		$d = $this->emailauth->findDuplicate($info);
		if($d == 0 ){
			$da = array('userId' => $user->id);
			$this->emailauth->addData($da);
		}
		
		$data['emailAuth'] = $this->emailauth->getInfoBaseOnUserId($user->id);		

		$this -> load -> view('header');
		$this -> load -> view('compliance/profile',$data);
		$this -> load -> view('footer');
		
	}
	
	public function homes() {
		$data['homeList'] = $this->rest->get('production/exApi/listOfHome');
		
		$this -> load -> view('header');
		$this -> load -> view('compliance/homes',$data);
		$this -> load -> view('footer');
	}

	public function getPatInfoBaseOnHomeID(){
		
		header("Access-Control-Allow-Origin: *");
	    header("Content-Type: application/json; charset=UTF-8");
        
        $data = json_decode(file_get_contents("php://input"));
	    $homeId   = $data->hId;
        $PatListBaseOnHomeID = $this->rest->get('production/exApi/patInfoBaseOnNHID/'.$homeId);
        
        $outp ="";
        foreach($PatListBaseOnHomeID AS $record){
            if($outp != "") { $outp .= ","; }
            $outp .='{"ID":"'        .$record->ID              .'",';
            $outp .='"LastName":"'   .$record->LastName        .'",';
			$outp .='"FirstName":"'  .$record->FirstName       .'"}';
        }
        
        $outp ='{"patInfo":['.$outp.']}';      
        
       	echo($outp);
		
	}
	
	public function getHomeDetailsBaseOnHOmeID(){
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=UTF-8");
		
		$data = json_decode(file_get_contents("php://input"));
		$homeId = $data->hId;
		$HomeInfoBaseOnHomeID = $this->rest->get('production/exApi/homeInfoBaseOnNHID/'.$homeId);
		
		$outp ="";
        foreach($HomeInfoBaseOnHomeID AS $record){
            if($outp != "") { $outp .= ","; }
            $outp .='{"ID":"'        .$record->ID              .'",';
			$outp .='"Name":"'       .$record->Name       .'"}';
        }
        
        $outp ='{"homeInfo":['.$outp.']}';      
        
       	echo($outp);
		
	}

	public function editHomeModal() {
		$this -> load -> view('compliance/editHomeModal');
	}

	public function editHome() {
		echo $this -> comp -> updateHome($this -> input -> post('homeID'), $this -> input -> post('homeName'), $this -> input -> post('homeAddress'), $this -> input -> post('contact'), $this -> input -> post('phone'));
	}

	public function homePatients() {
		$this -> load -> view('header');
		$this -> load -> view('compliance/homePatients');
		$this -> load -> view('footer');
	}

	public function printHomeInfo() {
		$this -> load -> view('compliance/printHomeInfo');
	}

	public function assignPatToHomeModal() {
		$this -> load -> view('compliance/assignPatToHomeModal');
	}

	public function assignPatToHome() {
		$this -> comp -> removePatFromHome($this -> input -> post('patID'), '0');
		$this -> comp -> assignPatToHome($this -> input -> post('patID'), $this -> input -> post('homeID'));
		echo $this -> comp -> updateCompliancePatient($this -> input -> post('patID'), $this -> input -> post('note'), $this -> input -> post('deliveryInstructions'), $this -> input -> post('$deliver'), $this -> input -> post('$controlled'), $this -> input -> post('$deliveryTimeID'), '0');
	}

	// Deliveries
	public function delivery() {
		$this -> load -> view('header');
		$this -> load -> view('delivery/deliveryruns');
		$this -> load -> view('footer');
	}

	// Portal Users
	public function portalUsers() {
		
		$data['portalUsers'] = $this->ion_auth->users('18')->result();
		$data['techUsers']   = $this->ion_auth->users ( '17' )->result();
		
		
		$this -> load -> view('header');
		$this -> load -> view('compliance/portalUsers',$data);
		$this -> load -> view('footer');
	}

    public function userStatus($id){
    	
		$user = $this->ion_auth->user($id)->row();
		
    	if($user->active == 0){
			$data = array( 'active' => 1 );
		}else{
			$data = array( 'active' => 0 );
		}
		
		$this->ion_auth->update($id, $data);
		
    	redirect('compliance/portalUsers');
    	
    }
	
	public function editPortalUserModal() {
		
		$this -> load -> view('compliance/editPortalUserModal');
	}

	
	
	// Add By Rajah
	public function deletePortalUserModal() {
		$this -> load -> view('compliance/deletePortalUser');
	}
	
	public function editPortalUserModalBySeperatePage() {
		
		
		$data['homeList'] = $this->rest->get('production/exApi/listOfHome');
		
		$this -> load -> view('header');	
		$this -> load -> view('compliance/editPortalUserModalSeperate',$data);
		$this -> load -> view('footer');
	}
	
	public function editPortalUserModalBySeperatePageRun() {
		$additional_data = array(
			 'first_name' => $this -> input -> post('first_name'),
			 'groups' => $this -> input -> post('groups'), 
			 'email' => $this -> input -> post('email'), 
			 'company' => $this -> input -> post('company'),
			 'username' => $this -> input -> post('username'),
			 'password' => $this -> input -> post('password'), 
			 'phone' => $this -> input -> post('phone'), 
			 'last_name' => $this -> input -> post('last_name'),
			 'NHID' => $this -> input -> post('NHID'),
			 'addBy'    => $this->ion_auth->user()->row()->first_name. ' ' .$this->ion_auth->user()->row()->last_name			 
	    );
			 
		$id = $this -> input -> post('id');			
		
		$up =  $this -> ion_auth -> update($id, $additional_data );
		
			
		if ($up > 0) {
			redirect('compliance/portalusers');
		} else {
			redirect('compliance/editPortalUserModalBySeperatePage/'.$this -> input -> post('groups').'/'.$this -> input -> post('id').'');
			
		}
		
	}
	
	public function reSetPawTech(){
		$id = $this->input->post('id');
		$reSetPassword = $this->input->post('reSetPassword');
		$reSetPasswordCon = $this->input->post('reSetPasswordCon');
		
		if(!empty($reSetPassword)) {
		 	  $d  = strcmp($reSetPassword,$reSetPasswordCon); 
		 }else{
		 	$d = 1;
		 }		 
		 
		 if ($d == 0) {
		 	
		 	
			 $additional_data = array(
			 			 
				 'password' => $reSetPassword
				 
			 );
		
			 $up =  $this -> ion_auth -> update($id,$additional_data );
			 
			 echo "Update succesfully"; 
			
		 }else{
		 	
		 	  echo "Password does not match."; 
			  
		 }		
		 
	}
	
	public function question(){
		$this-> load -> view('header');
		$this-> load -> view('compliance/question');
		$this-> load -> view('footer');
	}
	
    public function loadEboard(){
		$this-> load -> model('Pat');
		$this-> load -> model('tickets');
		$this-> load -> model('production/commonModels','commonModel');
		
		$ticket      = $this-> tickets->getTicketInfoToEboard();
		$medChange   = $this-> tickets->getMedchangeInfoToEboard();
		$production  = $this-> commonModel->getInfoToEboard();
		
		foreach($ticket AS $info){
			$ticketId = $info->ticketID;
			
			$niwardId = $info->NHWardID;
			
			if(!empty($route)){
				$dayTimeSub = strtoupper(substr($record->DeliveryRoute,3,5));
				if($dayTimeSub == 1000 )     { $dayTime = '10 AM';  }
				elseif($dayTimeSub == 1300 ) { $dayTime = '01 PM';  }
				elseif($dayTimeSub == 1500 ) { $dayTime = '03 PM';  }
				else                          { $dayTime = '11 AM';  }
			
			}else{
				$dayTime = "10 AM";
			}
			
			$dataT = array(
					'ticketID' => $ticketId,
					'NHID'     => $info->NHID,
					'NHWardID' => $niwardId,
					'type'     => 1,
					'dateOfDelivery' => $info->dateOfDelivery
			);
			$d1 = $this-> tickets->getDuplicateTicket($ticketId);
			if(empty($d1)){
			    $this->tickets->addEboard($dataT);
			}
		}
		
		foreach($production AS $pro){
			$tId = $pro->orderNum;
			$proData = array(
					'ticketID' => $tId,
					'NHID'     => $pro->nhid,
					'NHWardID' => $pro->nhwardid,
					'type'     => 3,
					'dateOfDelivery' => $pro->deliveryDate,
					'deliveryTime'   => $pro->DTime
			);
			$d = $this-> tickets->getDuplicateProduction($tId);
			if(empty($d)){
				$this->tickets->addEboard($proData);
			}
			
		}
		
		
		foreach($medChange AS $record){
				
			$data = array(
					'ticketID' => $record->medChangeID,
					'NHID'     => $record->NHID,
					'NHWardID' => $record->NHWardID,
					'type'     => 2,
					'dateOfDelivery' => $record->endDate
			);
				
			$this->tickets->addEboard($data);
				
			if($record->nextRDD > 0 ){
				$record->endDate;
				
		
				$day = "";
		
				if($record->NHID == 59) {
		
					$patInfo  = $this->tickets->getpatInfoBaseOnMedchangeId($record->medChangeID);
					$day = $patInfo->DeliveryRoute;
						
				} else {
						
					$wardInfo = $this->Pat->getWardName($record->NHWardID);
					$day = (!empty($wardInfo)) ? $wardInfo->DeliveryRoute : "";
						
				}
		
		
				if(!empty($day)){
		
					$day  = substr($day, 0,3);
					$time = substr($day,3,4);
						
		
					if($day == "MON"){ $outp =" Monday";    }
					if($day == "TUE"){ $outp =" Tuesday";   }
					if($day == "WED"){ $outp =" Wednesday"; }
					if($day == "THU"){ $outp =" Thursday";  }
					if($day == "FRI"){ $outp =" Friday";    }
		
					$d = date('Y-m-d',strtotime('next '.$outp));
						
				}else{
					$d = date('Y-m-d',strtotime('next '.date("l")));
				}
		
		
				$i =0;
		
				for($i =0; $i < $record->nextRDD; $i++ ){
						
					$day = (!empty($d)) ? date('Y-m-d', strtotime($d.'+'.$i.' Week')) : date('Y-m-d', strtotime('+'.$i.' Week'));
						
					$data = array(
							'ticketID' => $record->medChangeID,
							'NHID'     => $record->NHID,
							'NHWardID' => $record->NHWardID,
							'type'     => 2,
							'dateOfDelivery' => $day
					);
		
					$this->tickets->addEboard($data);
						
				}
		
			}
		}
	}
	
	public function getPatInfoBaseOnEboardId(){
		echo $eId = $this -> input -> post('id');
	}
	
	
	public function updateCheckItems(){
		 $eId = $this -> input -> post('eBoardId');
		 $eboardAllIdList = $this -> input -> post('eboardAllIdList');
		 
		 $this -> load -> model('tickets');
		 
		 $eIdArray   = explode(',',$eId);
		 $allIdArray = explode(',',$eboardAllIdList);
		 
		
		 $unselectArray = array_diff($allIdArray,$eIdArray);
		
		 $rearrangeArray = (array_values($unselectArray));
		 		 
		 for($d = 0; $d < COUNT($rearrangeArray); $d++ ){				 	
		 	 $Eid = $rearrangeArray[$d];
		 	 $data1 = array( 'check' => 0);
		 	 $a = $this -> tickets -> updateEboard($Eid,$data1);		 	 
		 }
		 
		 
		 $d = 0;
		 for($i = 0; $i< COUNT($eIdArray); $i++){
		 	 $id = $eIdArray[$i];
		 		 	 
		 	 $data = array( 'check' => 1);
		 	 $d = $this -> tickets -> updateEboard($id,$data);		 	
		 }		
		 
		 if($d > 0){
		 	echo "1|Succesfully checked";
		 }else{
		 	echo "0|Something went wrong, try again";		 
		 }
	}
	
	public function completeEboardTwo(){
		
		$user = $this->ion_auth->user()->row();
		
		$eId = $this -> input -> post('eBoardId');
		$eboardAllIdList = $this -> input -> post('eboardAllIdList');
			
		$this -> load -> model('tickets');
			
		$eIdArray   = explode(',',$eId);
		$allIdArray = explode(',',$eboardAllIdList);
			
	
		$unselectArray = array_diff($allIdArray,$eIdArray);
	
		$rearrangeArray = (array_values($unselectArray));
	
		for($d = 0; $d < COUNT($rearrangeArray); $d++ ){
			$Eid = $rearrangeArray[$d];
			$data1 = array( 'check' => 0);
			$a = $this -> tickets -> updateEboard($Eid,$data1);
		}
			
			
		$d = 0;
		for($i = 0; $i< COUNT($eIdArray); $i++){
			 $id = $eIdArray[$i];
	
			$data = array( 
				'check' => 1, 
				'date'	=> date('Y-m-d'),
				'userID'=> $user->id
			);
			$d = $this -> tickets -> updateEboard($id,$data);
		}
			
		if($d > 0){
			echo "1|Succesfully checked";
		}else{
			echo "0|Something went wrong, try again";
		}
	}
	
	public function deleteEboard(){
		$this -> load -> model('tickets');
		$user = $this->ion_auth->user()->row();
		
		$idList = $this->input->post('eboardDelIdList');
		$eIdArray   = explode(',',$idList);
		
		$d = 0;
		for($i = 0; $i< COUNT($eIdArray); $i++){
			$id = $eIdArray[$i];		
			$data = array( 
				'active' => 0,
				'date'	=> date('Y-m-d'),
				'userID'=> $user->id
			);
			$d = $this -> tickets -> updateEboard($id,$data);
		}
		
		if($d > 0){
			echo "1|Succesfully checked";
		}else{
			echo "0|Something went wrong, try again";
		}
		
	}
	
	public function printDeliverySheetWithIternary(){
				
		$patId = $this -> input -> post('patId');
		
		if(!empty($patId)){
			echo $patId;
		}else{
			echo "0|Select patient";
		}
		
		/*
		$eboardAllIdList = $this -> input -> post('eboardAllIdList');
			
		$this -> load -> model('tickets');
		
		$user = $this -> ion_auth -> user() -> row();
		
		$info = array(
			'eBoardIdList' => $eId,
			'userId'       => $user -> id,
			'printDate'    => date('Y-m-d')
		);
		
		if(!empty($eId)){
			$in = $this -> tickets -> addPrintEboard($info);
			
			if($in > 0){			
				$getInfo = $this -> tickets -> getPrintId($eId);
				echo $getInfo->id;				
			}else{
				echo "0|Something went wrong, try again";
			}
		}else{
			echo "0|Something went wrong, try again";
		}
		*/
	}
	
	public function eBoard(){
		
		$this-> load -> model('tickets');
		
		$this->loadEboard();
		
		$data['tickets']            = $this->tickets->getEboardTickets();
		$data['EboardHome']         = $this->tickets->getEboardTicketsNHWard();
		$data['medChange']          = $this->tickets->getEboardMedChangeNHWard();
		$data['upComingMedChange']  = $this->tickets->getUnCompletedMedCheck();
		$data['currentTicket']      = $this->tickets->getCurrentTicket();
				
				
		$this-> load -> view('header');
		$this-> load -> view('compliance/eBoard',$data);
		$this-> load -> view('footer');
		
	}
	
	public function completeOldMedChange(){
		$this-> load -> model('tickets');
		$info = $this->tickets->getUnCompletedMedchangeForUpdate();
		print_r($info);
		if(!empty($info)){
			foreach($info AS $record){
				$data = array(
					'actionComplete' => 1,
					'pharmacistUserID' => 1
				);
				$this->tickets->updateMedChanages($record->medChangeID,$data );
			}
			
		}
		
	}
	
	public function getHomeInfo($nhid){
		$homeInfo = $this->rest->get('production/exApi/getNHWardId/'.$nhid);
		echo $homeInfo ->Name;		
	}
	
	public function printOrderSheet(){
		$pId = $this -> uri -> segment(3);
		
		$this -> load -> model('Pat');
		$data['info'] = $this -> Pat -> getAllDataBaseOnIdRow($pId);
		
				
		$this-> load -> view('compliance/printOrderSheet',$data);
		
	}
	
	public function getWardPhoneNumPat($id){
		$getPhone = $this->rest->get('production/exApi/PhoneNumberBaseOnPatID/'.$id);
	
		foreach($getPhone AS $phoneNum){
			echo $phoneNum->Description." : ".$phoneNum->Phone."<br/>";
		}
	}
	
	public function findPatInHospital($pid){
		$getInfo = $this->rest->get('production/exApi/findPatInHospital/'.$pid);
		if(!empty($getInfo)){
			$today = date('Y-m-d');
			if(((empty($getInfo->LoaEndDate)) || ( date('Y-m-d', strtotime($getInfo->LoaEndDate)) >= $today )) && date('Y-m-d', strtotime($getInfo->LoaStartDate)) <= $today ){ 
				echo '<a href="#" data-toggle="tooltip" title="'.date('Y-m-d', strtotime($getInfo->LoaStartDate)).'"> <i class="fa fa-h-square" style="font-size:24px;color:red"></i></a>';			
			}
		}
				
	}
	
	public function printExtra(){
		$this -> load -> model('Pat');
		$data['patInfo']           = $this -> Pat     -> getAllActivePatInfo();
		
		$this-> load -> view('header');
		$this-> load -> view('compliance/printExtra',$data);
		$this-> load -> view('footer');
	}
	
	public function completeEboard(){
		$info = json_decode(file_get_contents("php://input"));
		$id = $info->data;
		
		$idArray = explode(',',$id);		
		$user = $this->ion_auth->user()->row();
		
		$this -> load -> model('tickets');
		
		
		for($i = 0; $i  < count($idArray); $i++){
			//echo $idArray[$i];
			$data = array(
				'date'   => date('Y-m-d'),
				'userID' => $user->id
			);
			
			$d = $this -> tickets -> updateEboard($idArray[$i],$data);
		}
		
		if($d == 1) { echo $d; }
		
	}
	
	public function eBoardRefresh(){
		$today = date('Y-m-d');
		
		$day = date('l',strtotime($today));
		$data['day'] = $day;
				
		$this-> load -> model('tickets');
		$data['tickets']     = $this->tickets->getEboardTickets();
		$data['EboardHome']  = $this->tickets->getEboardTicketsNHWardByDay($day);
		$data['medChange']   = $this->tickets->getEboardMedChangeNHWard();
		
		$this -> load -> view('compliance/eBoardRefresh',$data);	
		$this -> load -> view('footer');
	}
	
	public function eBoardByMon(){
		$this->loadEboard();
		
		$day = "MON"; 
		
		if($day == 'MON') { $data['day'] = "Monday"; }
		
		$this-> load -> model('tickets');
		$data['tickets']     = $this->tickets->getEboardTickets();
		$data['EboardHome']  = $this->tickets->getEboardTicketsNHWard();
		$data['medChange']   = $this->tickets->getEboardMedChangeNHWard();
		$data['upComingMedChange']  = $this->tickets->getUnCompletedMedCheck();
		$data['currentTicket']      = $this->tickets->getCurrentTicket();
		
		$this -> load -> view('header');
		$this -> load -> view('compliance/eBoardByDay',$data);	
		$this -> load -> view('footer');
	}
	
	public function eBoardByTue(){
		$this->loadEboard();
		
		$day = "TUE";
	
		if($day == 'TUE') { $data['day'] = "Tuesday"; }
	
		$this-> load -> model('tickets');
		$data['tickets']     = $this->tickets->getEboardTickets();
		$data['EboardHome']  = $this->tickets->getEboardTicketsNHWard();
		$data['medChange']   = $this->tickets->getEboardMedChangeNHWard();
		$data['upComingMedChange']  = $this->tickets->getUnCompletedMedCheck();
		$data['currentTicket']      = $this->tickets->getCurrentTicket();
	
		$this -> load -> view('header');
		$this -> load -> view('compliance/eBoardByDay',$data);
		$this -> load -> view('footer');
	}
	
	public function eBoardByWed(){
		$this->loadEboard();
		
		$day = "WED";
	
		if($day == 'WED') { $data['day'] = "Wednesday"; }
	
		$this-> load -> model('tickets');
		$data['tickets']     = $this->tickets->getEboardTickets();
		$data['EboardHome']  = $this->tickets->getEboardTicketsNHWard();
		$data['medChange']   = $this->tickets->getEboardMedChangeNHWard();
		$data['upComingMedChange']  = $this->tickets->getUnCompletedMedCheck();
		$data['currentTicket']      = $this->tickets->getCurrentTicket();
	
		$this -> load -> view('header');
		$this -> load -> view('compliance/eBoardByDay',$data);
		$this -> load -> view('footer');
	}
	
	public function eBoardByThu(){
		$this->loadEboard();
		
		$day = "THU";
	
		if($day == 'THU') { $data['day'] = "Thursday"; }
	
		$this-> load -> model('tickets');
		$data['tickets']     = $this->tickets->getEboardTickets();
		$data['EboardHome']  = $this->tickets->getEboardTicketsNHWard();
		$data['medChange']   = $this->tickets->getEboardMedChangeNHWard();
		$data['upComingMedChange']  = $this->tickets->getUnCompletedMedCheck();
		$data['currentTicket']      = $this->tickets->getCurrentTicket();
	
		$this -> load -> view('header');
		$this -> load -> view('compliance/eBoardByDay',$data);
		$this -> load -> view('footer');
	}
	
	public function eBoardByFri(){
		$this->loadEboard();
		
		$day = "FRI";
	
		if($day == 'FRI') { $data['day'] = "Friday"; }
	
		$this-> load -> model('tickets');
		$data['tickets']     = $this->tickets->getEboardTickets();
		$data['EboardHome']  = $this->tickets->getEboardTicketsNHWard();
		$data['medChange']   = $this->tickets->getEboardMedChangeNHWard();
		$data['upComingMedChange']  = $this->tickets->getUnCompletedMedCheck();
		$data['currentTicket']      = $this->tickets->getCurrentTicket();
	
		$this -> load -> view('header');
		$this -> load -> view('compliance/eBoardByDay',$data);
		$this -> load -> view('footer');
	}
	
	public function eBoardBySat(){
		$this->loadEboard();
		
		$day = "SAT";
	
		if($day == 'SAT') { $data['day'] = "Saturday"; }
	
		$this-> load -> model('tickets');
		$data['tickets']     = $this->tickets->getEboardTickets();
		$data['EboardHome']  = $this->tickets->getEboardTicketsNHWard();
		$data['medChange']   = $this->tickets->getEboardMedChangeNHWard();
		$data['upComingMedChange']  = $this->tickets->getUnCompletedMedCheck();
		$data['currentTicket']      = $this->tickets->getCurrentTicket();
	
		$this -> load -> view('header');
		$this -> load -> view('compliance/eBoardByDay',$data);
		$this -> load -> view('footer');
	}
	
	public function eBoardBySun(){
		$this->loadEboard();
		
		$day = "SUN";
	
		if($day == 'SUN') { $data['day'] = "Sunday"; }
	
		$this-> load -> model('tickets');
		$data['tickets']     = $this->tickets->getEboardTickets();
		$data['EboardHome']  = $this->tickets->getEboardTicketsNHWard();
		$data['medChange']   = $this->tickets->getEboardMedChangeNHWard();
		$data['upComingMedChange']  = $this->tickets->getUnCompletedMedCheck();
		$data['currentTicket']      = $this->tickets->getCurrentTicket();
	
		$this -> load -> view('header');
		$this -> load -> view('compliance/eBoardByDay',$data);
		$this -> load -> view('footer');
	}
	
	public function getEboardInfoBaseOnEboardId(){
		$info = json_decode(file_get_contents("php://input"));
		$id = $info->id;
		$this -> load -> model('tickets');
		$info = $this -> tickets -> getInfoBaseOnEboardId($id);
		$type = $info->type;
		
		if($type == 1){
			$ticketInfo = $this -> tickets -> getAllInfoBaseOnEboardId($id);
		}else{
			$ticketInfo = $this -> tickets -> getMedChangeInformationBaseOnEboardId($id);
		}
		
		$outp = "";
		$errorId = array();
		$errorIdList = "";
		if(!empty($ticketInfo)){
			foreach($ticketInfo AS $info){
				if($outp != "") { $outp .= ","; }
		
				$errorid[] = $info->id;
		
				$outp .='{"id":"'        .$info->id              .'",';
				$outp .='"LastName":"'   .$info->LastName        .'",';
		
				$outp .='"dateOfDelivery":"'.$info->dateOfDelivery.'",';
		
			    if(!empty($info->Description))  { $outp .='"Description":"'   .   preg_replace('/[^a-zA-Z0-9]/s', ' ',$info->Description)     .'",';  }
				if(!empty($info->actionText))   { $outp .='"actionText":"'    .   preg_replace('/[^a-zA-Z0-9]/s', ' ',$info->actionText)      .'",';  }
				if(!empty($info->changeText))   { $outp .='"changeText":"'    .   preg_replace('/[^a-zA-Z0-9]/s', ' ',$info->changeText)      .'",';  }
				
				if(!empty($info->spares))        { $outp .='"spares":"'       .    $info->spares          .'",';  }
				if(!empty($info->mar))           { $outp .='"mar":"'          .    $info->mar             .'",';  }
				if(!empty($info->extraLabels))   { $outp .='"extraLabels":"'  .    $info->extraLabels     .'",';  }
				if(!empty($info->numOfStripts))  { $outp .='"numOfStripts":"' .    $info->numOfStripts    .'",';  }
		
				$outp .='"Name":"'  .$info->LastName.' '.$info->FirstName       .'"}';
			}
			$errorIdList = implode(',',$errorid);
		}
		
		$outp ='{"ticketInfoById":['.$outp.',{"errorIdList":"'.$errorIdList.'"}]}';
		echo($outp);
		
	}
	
	public function getEboardDetailsForMiniBoard(){
		header('Content-type: text/html; charset=utf-8');
		
		$info = json_decode(file_get_contents("php://input"));
		
		$wardId   = $info->wardId;
		$diffDays = $info->numdays;
		$type     = $info->type;
	    $day      = $info->dayId;
	    
	    /*
	    $wardId   = 193;
	    $diffDays = -13;
	    $type     = 1;
	    $day        = 4;
	   */
		
		$weekDay = "";
		switch($day){
			case 1 :
				$weekDay = "Monday";
				break;
			case 2 :
				$weekDay = "Tuesday";
				break;
			case 3 :
				$weekDay = "Wednesday";
				break;
			case 4 :
				$weekDay = "Thursday";
				break;
			case 5 :
				$weekDay = "Friday";
				break;
			case 6 :
				$weekDay = "Saturday";
				break;
			case 7 :
				$weekDay = "Sunday";
				break;
			default :
				$weekDay = "NULL";				
		}
		
		$weekDay;
		
		$this -> load -> model('tickets');
		$info = $this -> tickets ->getInfomationBaseOnNHIDandTypeAndDay($wardId,$diffDays,$type,$weekDay);
				
		$eboardId = array();
		$ticketId = array();
		
		foreach($info AS $record){
			$eboardId[] = $record->id;
			$ticketId[] = $record->ticketID;
		}
				
		$ticketIdInList = implode(',',array_unique($ticketId));
		$eboardIdInList = implode(',',array_unique($eboardId));		
		$ticketInfo = array();
		if($type == 1){
			$ticketInfo = $this -> tickets -> getAllInfoBaseOnEboardId($eboardIdInList);
		}else{
			$ticketInfo = $this -> tickets -> getMedChangeInformationBaseOnEboardId($eboardIdInList);			
		}
		
				
		$outp = "";
		$errorId = array();
		$errorIdList = "";
		if(!empty($ticketInfo)){
			foreach($ticketInfo AS $info){
				if($outp != "") { $outp .= ","; }
				
				$errorid[] = $info->id;
				
				$outp .='{"id":"'        .$info->id              .'",';
				$outp .='"LastName":"'   .$info->LastName        .'",';
				
				$outp .='"dateOfDelivery":"'.$info->dateOfDelivery.'",';
				
			    if(!empty($info->Description))  { $outp .='"Description":"'   .   preg_replace('/[^a-zA-Z0-9]/s', ' ',$info->Description)     .'",';  }
				if(!empty($info->actionText))   { $outp .='"actionText":"'    .   preg_replace('/[^a-zA-Z0-9]/s', ' ',$info->actionText)      .'",';  }
				if(!empty($info->changeText))   { $outp .='"changeText":"'    .   preg_replace('/[^a-zA-Z0-9]/s', ' ',$info->changeText)      .'",';  }
					
				if(!empty($info->spares))        { $outp .='"spares":"'       .    $info->spares          .'",';  }
				if(!empty($info->mar))           { $outp .='"mar":"'          .    $info->mar             .'",';  }
				if(!empty($info->extraLabels))   { $outp .='"extraLabels":"'  .    $info->extraLabels     .'",';  }
				if(!empty($info->numOfStripts))  { $outp .='"numOfStripts":"' .    $info->numOfStripts    .'",';  }
				
				$outp .='"Name":"'  .$info->LastName.' '.$info->FirstName       .'"}';
			}
			$errorIdList = implode(',',$errorid);
		}			
		
		$outp ='{"ticketInfo":['.$outp.',{"errorIdList":"'.$errorIdList.'"}]}';		
		echo($outp);
		
	}
	
	public function Error(){
		
		$this-> load -> model('tickets');
		$data['info'] = $this -> tickets -> getAllErrorInfo();
		
		$this-> load -> view('header');
		$this-> load -> view('compliance/error',$data);
		$this-> load -> view('footer');
	}
	
	public function eBoardWeekEnd(){
		$this-> load -> model('tickets');
		$data['tickets']     = $this->tickets->getEboardTickets();
		$data['EboardHome']  = $this->tickets->getEboardTicketsNHWard();		
		$data['medChange']   = $this->tickets->getEboardMedChangeNHWard();
		
		$this-> load -> view('header');
		$this-> load -> view('compliance/eBoardWeekEnd',$data);
		$this-> load -> view('footer');
		
	}
	
	public function getAllProductionList(){
		$this->loadEboard();
		$this->checkStationANDupdateStaus();
		
		$nhid = $this -> uri -> segment(3);
		$this -> load -> model('tickets');
		
		$data['patInfo'] = $this->rest->get('production/exApi/getPatBaseOnNHWardId/'.$nhid);
		$data['productionDetails'] = $this -> tickets -> getAllDetailsInEboardBaseOnNhWardId($nhid);
		
		$data['nhwardInfo'] = $this->rest->get('production/exApi/getHomeProfileFromPat/'.$nhid);
		if(empty($data['nhwardInfo'])){
			$data['nhwardNonProfile'] = $this->rest->get('production/exApi/getNHWardId/'.$nhid);
		}
				
		$this-> load -> view('header');
		$this-> load -> view('compliance/getAllProductionList',$data);
		$this-> load -> view('footer');
	}
	
	public function updateDeliveryDate(){
		$eId  = $this -> input -> post('eId');
		$date = $this -> input -> post('dateOfDelivery');
		
		$data = array(
			'dateOfDelivery' => $date	
		);
		$this -> load -> model('tickets');
		$d = $this -> tickets -> updateEboard($eId,$data);
		
		echo ($d > 0)? "1|Successfully Updated" : "0|Something went wrong , try again";
	}
	
	function checkStationANDupdateStaus(){
		$this -> load -> model('production/commonModels','commonModels');
		$this -> load -> model('tickets');
		
		$check = $this -> tickets -> getAllCheckedList();			
		$user = $this -> ion_auth -> user() -> row();		
		
		
		foreach($check AS $info){
			
			$orderId = $info->ticketID;
			
			$data = array(
				'orderId' => $orderId,
				'userId'  => $user->id,
				'addDate' => date('Y-m-d')
			);
			
			$data1 = $this->commonModels->getRecordBaseOnOrderId($orderId);
			
			if(!empty($data1)){
				$info = $this->commonModels->getRecordBaseOnOrderIdInPacVision($orderId);
				if(empty($info))     { $d = $this->commonModels->addPackVision($data);  }
			
				$pacVision = $this->commonModels->getRecordBaseOnOrderIdInPharmacistcheck($orderId);
				if(empty($pacVision)){ $d = $this->commonModels->addPharmacistCheck($data); }
			
				$pacDelivery = $this->commonModels->getRecordBaseOnOrderIdInDelivery($orderId);
				if(empty($pacDelivery)){ $d = $this->commonModels->addDelivery($data); }
			}		
		}		
	}
	
	
	public function findOrderIn($orderId){
		$this -> load -> model('production/commonModels','commonModels');	
		$this -> load -> model('tickets');
		
		$delivery = $this -> commonModels -> getRecordBaseOnOrderIdInDelivery($orderId);
		$eBoard = $this -> tickets -> getInfoBaseOnEboardOrderId($orderId,3);
		if(!empty($eBoard)){
			if($eBoard->active == 0){
				echo "Removed";
			}else{
				echo "Sent Out";
			}
			
		}else{
		
			if(!empty($delivery)){
				if($delivery->finalCheck = 1){
					echo "Waiting for final check";
				}else{
					echo "Ready for Delivery";
				}
				
			}else{
				$pharmacist = $this -> commonModels -> getRecordBaseOnOrderIdInPharmacistcheck($orderId);
					
				if(!empty($pharmacist)){
					echo "Waiting at pac vision check";
				}else{
					$pacVision = $this -> commonModels -> getRecordBaseOnOrderIdInPacVision($orderId);
					if(!empty($pacVision)){
						echo "Waiting at pacmed";					
					}else{
						$pacMed = $this -> commonModels -> getRecordBaseOnOrderId($orderId);
						if (!empty($pacMed)){ 
							echo (($pacMed->event == "OPERATION") || ($pacMed->event == "Operation")) ? "Waiting at pharmacist check" : " Waiting at pacmed ";					
						
						}else{
							echo  "Some thing went wrong";
						}
					}
				}
			}
		}
	}
	
	public function getMedInfoBaseOnRxNum($rxnumList){
		
		$drgs = $this->rest->get('production/exApi/getDrgInfoBaseOnRxId/'.$rxnumList);
		
		
		foreach($drgs AS $drg){
			echo $drg->BrandName.", ";
		}
	}
	
	public function getPatAndMedInfoBaseOn($eboardTicketId){
		
		$this -> load -> model('tickets');
		$this -> load -> model('production/commonModels','commonModel');
		$this -> load -> model('Pat');
		$this -> load -> model('production/packagerBatch','PB');
		
		$info = $this -> tickets -> getInfoBaseOnEboardId($eboardTicketId);
		
	    switch($info->type){
			case 1:				
				$pat = $this -> tickets -> getPatAndTicketInfo($info->ticketID);
				$com = $this -> tickets -> getCommentBaseOnTicketId($info->ticketID);
				
				$todate = date('Y-m-d');
				$diff = date_diff(date_create($todate),date_create($pat->dateOfDelivery));
				$numdays = $diff->format('%R%a');
				
				echo '<span class="list-group-item .'; if($numdays < 0){ echo ' list-group-item-danger '; } elseif($numdays ==0){ echo ' list-group-item-success '; }else {  echo ' list-group-item-warning '; }  echo '"> 
						<a href="'.base_url().'compliance/viewPat/'.$pat -> ID.'/Delivery">
						<span class="label label-pill label-success">'.$pat -> LastName .' '.$pat -> FirstName. ' >> </span> </a>';
						
				        if(($pat->rxnumList == 0) || (empty($pat->rxnumList))){
				        	echo $pat -> Description."<br/>";
				        	
				        	if(!empty($com)){
				        		foreach($com AS $record){
				        			echo '<span class="list-group-item list-group-item-info">';
				        			echo $record->message."<br/>";
				        			echo '</span>';				        			
				        		}
				        	}
				        }else{
				        	$drgs = $this->rest->get('production/exApi/getDrgInfoBaseOnRxId/'.$pat->rxnumList);
				        	foreach($drgs AS $drg){
				        		echo $drg->BrandName.", ";				        		
				        	}
				        	
				        	if(!empty($com)){
				        		foreach($com AS $record){
				        			echo '<span class="list-group-item list-group-item-info">';
				        			echo $record->message."<br/>";
				        			echo '</span>';
				        			 
				        		}
				        	}
				        }
				        $user = $this -> ion_auth -> user($pat->createdBy)->row();
				        echo '<span class="list-group-item list-group-item-info">';
				        echo '<br/><br/> Created By :  '.$user->first_name.' '.$user->last_name;
				        echo '</span>';
				        
				        
				echo '</span>';	
				break;
				
			case 2:				
				$pat = $this -> tickets -> getPatAndMedChangeInfo($info->ticketID);
				
				$todate = date('Y-m-d');
				$diff = date_diff(date_create($todate),date_create($pat->endDate));
				$numdays = $diff->format('%R%a');
				
				echo '<span class="list-group-item .'; if($numdays < 0){ echo ' list-group-item-danger '; } elseif($numdays ==0){ echo ' list-group-item-success '; }else {  echo ' list-group-item-warning '; }  echo '"> 
						<a href="'.base_url().'compliance/viewPat/'.$pat -> ID.'/Delivery">
						<span class="label label-pill label-success">'.$pat -> LastName .' '.$pat -> FirstName. ' >> </span> </a> '.$pat ->changeText.'</span>';
				
				$user = $this -> ion_auth -> user($pat->technicianUserID)->row();
				echo '<span class="list-group-item list-group-item-info">';
				echo '<br/><br/> Created By  :  '.$user->first_name.' '.$user->last_name;
				echo '</span>';
				break;
				
			case 3:
				$batchInfo = $this -> commonModel -> getRecordBaseOnOrderId($info->ticketID);
				if(!empty($batchInfo)){
					$pats = $this -> Pat -> getInfoBaseOnListOfPatID($batchInfo->listOfPatId);
					
					$todate = date('Y-m-d');
					$diff = date_diff(date_create($todate),date_create($batchInfo->deliveryDate));
					$numdays = $diff->format('%R%a');
					
					echo '<span class="list-group-item .'; if($numdays < 0){ echo ' list-group-item-danger '; } elseif($numdays ==0){ echo ' list-group-item-success '; }else {  echo ' list-group-item-warning '; }  echo '">';
					foreach( $pats AS $pat){
						echo '<a href="'.base_url().'compliance/viewPat/'.$pat -> ID.'/Delivery">'. $pat -> LastName .' '.$pat -> FirstName. '</a><br/>';
					}
					$pb = $this -> PB -> findBatchNum($batchInfo->batchNum);
					echo '<span class="list-group-item list-group-item-info">';
					echo 'Start date : '.date('Y/m/d',strtotime($pb->startDate)).' - End Date : '.date('Y/m/d',strtotime($pb->endDate));
					$user = $this -> ion_auth -> user($batchInfo->userId)->row();
					echo '<br/><br/> Created By : '.$user->first_name.' '.$user->last_name;
					echo '</span>';
					echo '  </span>';
				}else{
					echo "Something went wrong";
				}				
				break;
				
			default:
				echo "Something went wrong";
		}
		
	}
	
	public function getHomeInfoForEboard($nhid,$nhwardid,$date,$type){
		$this-> load -> model('tickets');
		$this->load->model('Pat');	
			
			$todate = date('Y-m-d');
			
			$numdays = "";
			
			/*
			if($nhid == 59){
				$patInfo = $this->tickets->gatpatInfoBaseOnNhIdAndNhWardId($nhid,$nhwardid,$date);
				
				foreach($patInfo AS $info){
					$diff = date_diff(date_create($todate),date_create($date));
					$numdays = $diff->format('%R%a');
											
					echo '<li class="list-group-item .'; if($numdays < 0){ echo ' list-group-item-danger '; } elseif($numdays ==0){ echo ' list-group-item-success '; }else {  echo ' list-group-item-warning '; }  echo '"'; 
				    echo '<span class="badge"></span> <span ng-click="getEboardLocalIndividual('.$info->id.')" >'. $info->LastName .' '. $info->FirstName .'</span></li>';						
					
				}			
				
			}else{
			*/	
				$diff = date_diff(date_create($todate),date_create($date));
				$numdays = $diff->format('%R%a');
				
				$deliveryDate = date("l", strtotime($date));
					
				if($deliveryDate == "Monday")    { $day = 1; }
				if($deliveryDate == "Tuesday")   { $day = 2; }
				if($deliveryDate == "Wednesday") { $day = 3; }
				if($deliveryDate == "Thursday")  { $day = 4; }
				if($deliveryDate == "Friday")    { $day = 5; }
				if($deliveryDate == "Saturday")  { $day = 6; }
				if($deliveryDate == "Sunday")    { $day = 7; }
					
				$wardInfo      = $this->Pat->getWardName($nhwardid);			
				$count         = $this->tickets->countNumberOfWard($nhwardid,$date);
				$countCheck    = $this->tickets->countNumberOfWardByChecked($nhwardid,$date);
				
				echo '<a href="'.base_url().'compliance/getAllProductionList/'.$nhwardid.'">';
				echo '<li class="list-group-item .'; if($numdays < 0){ echo ' list-group-item-danger '; } elseif($numdays ==0){ echo ' list-group-item-success '; }else {  echo ' list-group-item-warning '; }  echo '" >'; 
				echo '<span class="badge" style="background-color:#9370DB;">';
								
				if(!empty($countCheck)){ echo $countCheck -> t1; } else { echo "0"; }
						
				echo '/'.intval($count->t).'</span>'; if(!empty($wardInfo)){ echo $wardInfo->LastName .' '. $wardInfo->FirstName; } else { 
					$nhwardInfo = $this->rest->get('production/exApi/getNHWardId/'.$nhwardid);
					echo $nhwardInfo->Name; 
					
				}
				echo '</li></a>';	
				
				
			//}	
				
	}

	
	public function getLoacalIndivualInfoForEboard($nhid,$nhwardid){
		$this-> load -> model('tickets');
		$this->load->model('Pat');
			
		$localIndi = $this->tickets->getLocalIndividual();
		
		foreach($localIndi AS $record){
			echo '<li class="list-group-item" >'; 
			echo '<span class="badge"></span> <span ng-click="getEboardLocalIndividual('.$record->id.')" >'. $record->LastName .' '. $record->FirstName;
			echo '</span></li>';
		}		
		
	}
	
	public function deletePortalUserConfirm() {
		
		 //$data['delete'] = $this->ion_auth->delete_user($this->uri->segment (3));
		 $data['delete'] = $this->ion_auth->delete_user($this -> input -> post('id'));
		 
		
		$this -> load -> view('header');		
		$this -> load -> view('compliance/portalUsers');
		$this -> load -> view('footer');
	}
	
	
	
	public function editPortalUser() {
		$additional_data = array('first_name' => $this -> input -> post('first_name'), 'last_name' => $this -> input -> post('last_name'), 'company' => $this -> input -> post('company'), 'phone' => $this -> input -> post('phone'));

		//$groups = array('18');
		$groups = array(trim($this -> input -> post('groups')));
		echo $this -> ion_auth -> register(trim($this -> input -> post('username')), $this -> input -> post('password'), trim($this -> input -> post('email')), $additional_data, $groups);
	}

	public function addUserToHomeModal() {
		$this -> load -> view('compliance/assignUserToHomeModal');
	}

	public function addUserToHome() {
		$userID = $this -> comp -> getHomePortalUser($this -> input -> post('email'));

		if ($userID != false) {
			echo $this -> comp -> assignUserToHome($userID, $this -> input -> post('homeID'));
		} else {
			return false;
		}
	}

	public function removeGroupUserAccess() {
		echo $this -> comp -> removeUserFromHome($this -> input -> post('userID'), $this -> input -> post('homeID'));
	}

	// daves way: foreach($this->input->post('reOrderCheckBox') as $val) {
	public function createReOrder() {		
		
		
		$rxs = $this -> input -> post('rxs');
		if(!empty($rxs)){
			
			echo $this -> comp -> updateTicket('-1', $this -> input -> post('patID'), '2', 'Please reorder the following RXs: ' . $this -> input -> post('rxs'), '1',$this -> input -> post('rxnumList'));
		}else{
			echo "Not sucessful";
		}		
	}

	public function DrugInfoModal() {
		$this -> load -> view('compliance/DrugInfoModal');
	}

	public function getPatientResults() {
		$pats = $this -> pharm -> getAllPatients($this -> input -> post('fullName'));
		$patId = $this -> pharm -> getAllPatientsBaseOnPatID($this -> input -> post('fullName'));
		
		if ($pats) {
			foreach ($pats->result () as $pat) {
				echo "<p><a href='" . base_url("compliance/viewPat/" . $pat -> ID . "/profile") . "'>" . $pat -> FirstName . ' ' . $pat -> LastName . '</a></p>';
			}
		}elseif($patId) {
			foreach ($patId->result () as $info) {
				echo "<p><a href='" . base_url("compliance/viewPat/" . $info -> ID . "/profile") . "'>" . $info -> FirstName . ' ' . $info -> LastName . '</a></p>';
			}
		}else{
			echo 'No Data.';
		}
	}


	public function getPatCompPack() {

		echo '<thead><tr><th>Medication</th><th>Doctor</th><th>Directions</th></tr></thead><tbody>';
		
		$compPack = $this -> pharm -> getPatCompliancePackage($this -> input -> post('patID'));
		if ($compPack != FALSE) {
			foreach ($compPack->result () as $drg) {
				echo '<tr>';
				echo '<td>' . $drg -> BrandName . ' ' . $drg -> Strength . '<br>RX-' . $drg -> RxNum . '</td>';
				echo '<td>' . $drg -> doc . '</td>';
				echo '<td>' . $drg -> SIG . '</td>';
				echo '</tr>';
			}
		} else {
			echo '<tr><td>No Medication Recorded</td></tr>';
		}
		 
		

		echo '</tbody>';
	}

	public function getPatPRN() {

		echo '<thead><tr><th>Medication</th><th>Doctor</th><th>Directions</th><th>Comment</th><th>Select</th></tr></thead><tbody>';
		
		$patPRNs = $this->pharm->getPatPRNs ( $this -> input -> post('patID') );
		if ($patPRNs != FALSE) {
			$checkBoxCount = 1;
			foreach ($patPRNs->result () as $patPRN) {
				$mix='';
				if($patPRN->MixID>0){$mix='class="warning"';}
				echo '<tr id="reOrderList" ' . $mix .'>';
				echo '<td>' . $patPRN -> BrandName . ' ' . $patPRN -> Strength . '<br>RX-' . $patPRN -> RxNum . '</td>';
				echo '<td>' . $patPRN -> doc . '</td>';
				echo '<td>' . $patPRN -> SIG . '</td>';
				echo '<td><input type="text" name="reOrderComment" class="form-control reOrderCommentClass" id="comment' . $checkBoxCount . '" ></td>';
                echo '<td><input type="checkbox" name="reOrderCheckBox" class="form-control reOrderCheckBoxClass" value="' . $patPRN -> RxNum . '" id="checkBox' . $checkBoxCount++ . '" onlick="addOnClickEvent()"></td>';
				echo '</tr>';
			}
		} else {
			echo '<tr><td>No Medication Recorded..</td></tr>';
		}

		echo '</tbody>'; 
		
		
	}



	// Production
	public function production() {
		$login = $this->ion_auth->user()->row()->id;
		$name  = $this->ion_auth->user()->row()->first_name. '.' .$this->ion_auth->user()->row()->last_name;	
		redirect($this->host.'/localWork/production/home/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/'.$name.'/'.$login);
		
	}
	
	public function eBoardLocal() {
		$login = $this->ion_auth->user()->row()->id;
		$name  = $this->ion_auth->user()->row()->first_name. '.' .$this->ion_auth->user()->row()->last_name;
		redirect($this->host.'/localWork/production/eBoard/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/'.$name.'/'.$login);
	
	}
	
	public function clinicalResearch() {
		$login = $this->ion_auth->user()->row()->id;
		$name  = $this->ion_auth->user()->row()->first_name. '.' .$this->ion_auth->user()->row()->last_name;
		redirect($this->host.'/localWork/production/clinicalResearch/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/'.$name.'/'.$login);	
	}
	
		
	public function wardLocationInDelivery() {
		$login = $this->ion_auth->user()->row()->id;
		$name  = $this->ion_auth->user()->row()->first_name. '.' .$this->ion_auth->user()->row()->last_name;
		redirect($this->host.'/localWork/production/wardLocationInDelivery/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/'.$name.'/'.$login);
	}
	
	public function pharmacistCheckGraph() {
		$login = $this->ion_auth->user()->row()->id;
		$name  = $this->ion_auth->user()->row()->first_name. '.' .$this->ion_auth->user()->row()->last_name;
		redirect($this->host.'/localWork/production/pharmacistCheckGraph/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/'.$name.'/'.$login);			
	}
	
	
	public function localPatProfile(){
		$login = $this->ion_auth->user()->row()->id;
		$name  = $this->ion_auth->user()->row()->first_name. '.' .$this->ion_auth->user()->row()->last_name;
		$patId = $this -> uri -> segment(3);
		redirect($this->host.'/localWork/production/localPatProfile/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/'.$name.'/'.$login.'/'.$patId);
	}
	
	public function pacMed() {
		$this -> load -> view('production/layout/productionHeader');
		$this -> load -> view('production/pacMed');
		$this -> load -> view('production/layout/productionFooter');
	}
	
	public function pacVision() {
		$this -> load -> view('production/layout/productionHeader');
		$this -> load -> view('production/pacVision');
		$this -> load -> view('production/layout/productionFooter');
	}

	public function pharmacistCheck() {
		$this -> load -> view('production/layout/productionHeader');
		$this -> load -> view('production/pharmacistCheck');
		$this -> load -> view('production/layout/productionFooter');
	}

	public function readyToDelivery() {
		$this -> load -> view('production/layout/productionHeader');
		$this -> load -> view('production/readyToDelivery');
		$this -> load -> view('production/layout/productionFooter');
	}	
	
	public function canceledMed() {
		$this -> load -> view('production/layout/productionHeader');
		$this -> load -> view('production/canceledMed');
		$this -> load -> view('production/layout/productionFooter');
	}
	
	public function assignForDelivery() {
		$this -> load -> view('production/layout/productionHeader');
		$this -> load -> view('production/assignForDelivery');
		$this -> load -> view('production/layout/productionFooter');
	}
	
	public function viewDeliveredItems() {
		$this -> load -> view('production/layout/productionHeader');
		$this -> load -> view('production/viewDeliveredItems');
		$this -> load -> view('production/layout/productionFooter');
	}

	public function birthdayList() {
		$this -> load -> view('production/layout/productionHeader');
		$this -> load -> view('production/birthdayList');
		$this -> load -> view('production/layout/productionFooter');
	}
	
	public function confirmRun() {
		$this -> load -> view('production/layout/productionHeader');
		$this -> load -> view('production/confirmRun');
		$this -> load -> view('production/layout/productionFooter');
	}
	
	
	
	
	
	
	
	public function updateProduction(){
		echo $this -> comp -> updateTicket($this -> input -> post('ticketID'), $this -> input -> post('patID'), $this -> input -> post('ticketTypeReferenceID'), $this -> input -> post('Description'), $this -> input -> post('active'));
		
	}

	public function printorders() {
		$this->load->library('Pdf');
		$this -> load -> view('compliance/printorders');
	}
	
	
	
		
	//Pat Profile Alergies and Conditions
	public function getAlergiesTable(){
        	echo '<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover"><thead><tr><th>Allergies</th><th>Comment</th><th>Severity</th></tr></thead><tbody>'	;				
			$alergies = $this->pharm->getPatAlergyCodes($this -> input -> post('patID') );
			if ($alergies!=FALSE){
				foreach($alergies->result() as $alergy)
				{
					$alergyDecoded = $this->pharm->getAlergyDecoded($alergy->Code);
					if ($alergyDecoded!=FALSE)
					{ 
						echo '<tr><td>' . $alergyDecoded->AllergenDesc . '</td><td>' . $alergy->Comment . '</td><td>' . $alergy->Severity . '</td></tr>';
					}
				}
			}
			else {
			echo '<tr><td>No Allergies Recorded..</td></tr>';
		}
			echo '</tbody></table>';   
    }
	
	
		//Pat Profile Alergies and Conditions
	public function getConditionsTable(){
        	echo '<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover"><thead><tr><th>Conditions</th></tr></thead><tbody>'	;				
			$conditions = $this->pharm->getPatCondCodes($this -> input -> post('patID') );
			if ($conditions!=FALSE){
				foreach($conditions->result() as $cond)
				{
					$condDecoded = $this->pharm->getCondDecoded($cond->Code);
					if ($condDecoded!=FALSE)
					{ 
						echo '<tr><td>' . $condDecoded->Description . '</td></tr>';
					}
				}
			}
			else {
			echo '<tr><td>No Conditions Recorded..</td></tr>';
		}
			echo '</tbody></table>';	   
    }
	

}
?>
