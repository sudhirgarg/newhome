<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

include_once ('secure_controller.php');
class production extends Secure_Controller {
	function __construct() {
		// Call the Model constructor
		parent::__construct();
		if ($this -> quick -> accessTo('production')) {
			// do nothing they are legit
		} else {
			// current user should not have access to this controller.
			redirect('news/mynews');
		}
	}

	// Production
	public function production() {
		$this -> load -> view('header');
		$this -> load -> view('compliance/production');
		$this -> load -> view('footer');
	}

	public function productiondetail() {
		$this -> load -> view('header');
		$this -> load -> view('compliance/productiondetail');
		$this -> load -> view('footer');
	}



}
?>