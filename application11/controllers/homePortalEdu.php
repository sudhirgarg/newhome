<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

include_once ('secure_controller.php');

class homePortalEdu extends Secure_Controller {
	
	function __construct() {
		// Call the Model constructor
		parent::__construct();
		if ($this -> quick -> accessTo('homePortalEdu')) {
			// do nothing they are legit
		} else {
			// current user should not have access to this controller.
			redirect('news/mynews');
		}

		if (!isset($this -> session -> userdata['user']['language'])) {
			$data = array('language' => 'english');
			$this -> session -> set_userdata('user', $data);
		}

		$this -> lang -> load('portal', $this -> session -> userdata['user']['language']);
		
		$imagetypes = array("image/jpeg", "image/gif", "image/jpg", "image/png");
		
	}
	
	
	public function index() {
		if (!$this -> ion_auth -> logged_in()) {
			redirect(base_url() . 'auth/login');
		}
		
		$this->load->model('emailauth');
		$user = $this->ion_auth->user()->row();
		$info = array('userId' => $user->id);
		$d = $this->emailauth->findDuplicate($info);
		if($d == 0 ){
			$da = array('userId' => $user->id);
			$this->emailauth->addData($da);
		}
		
		$data['emailAuth'] = $this->emailauth->getInfoBaseOnUserId($user->id);		

		$this -> load -> view('edu/eduHeader');
		$this -> load -> view('edu/index',$data);
		$this -> load -> view('edu/eduFooter');
	}
	
	public function setEmailCredential(){
		
		$this->load->model('emailauth');
		
		$userId      = $this->input->post('userId');
		
		$data = array(		
		    'medChange'  => $this->input->post('medChange'),
		    'ticket'     => $this->input->post('ticket'),
		    'comment'    => $this->input->post('comment')
		);
		
		$d= $this->emailauth->updateData($userId,$data);
		
		if($d == 1){
			echo "Successful";
		}else{
			echo "Sorry, Try one more time.";
		}
		
	}
	
	public function tutorials() {
		
		$this->load->model('eBookAndVideo');
		$this->load->model('candidate');
		
		$data['topicsList']	= $this->eBookAndVideo->getTopicsGroupInList();	
		
		$data['scoreDetails'] = $this->candidate->getAllScoreBaseOnUserId($this->ion_auth->user()->row()->id);
		
		$this -> load -> view('edu/eduHeader');
		//$this -> load -> view('edu/tutorials', $data);
		$this -> load -> view('edu/tutorialsView',$data);
		$this -> load -> view('edu/eduFooter');
	}
	
	public function listOfinfoBaseOnTopic(){
		
		$this->load->model('eBookAndVideo');
		$this->load->model('topics');
		$data['tutorialsList']	= $this->eBookAndVideo->getAllDataBaseOnTopicsId($this->uri->segment('3'));
		$data['topicName'] =  $this->topics->getAllDataBaseOnId($this->uri->segment('3'));
		$this -> load -> view('edu/eduHeader');
		$this -> load -> view('edu/tutorialsTitleList',$data);
		$this -> load -> view('edu/eduFooter');
		
	}
	
	
	public function getCourseStatus($id){
		$user = $this -> ion_auth -> user() ->  row();
		
		$this -> load -> model('eBookAndVideo');
		$info   = $this -> eBookAndVideo -> findUserStartCourse($id,$user -> id);
		$result = $this -> eBookAndVideo -> getResultAndStatus($id,$user -> id);
		
		echo '<a href="'.base_url().'homePortalEdu/viewTutorial/'.$id.'">';
		      
		      if(!empty($result)){
		      	
			      	if($result->score >=0){
			      	    echo '<img src="'.base_url().'img/Tested.png" >';			      	    
			        }else{
			      		//echo'<img src="'.base_url().'img/Certified.png" >';
			        	echo '<img src="'.base_url().'img/Continue.png" >';
			      	}
		      	
		      }else {
		      	
					if(empty($info)){
		               echo '<img src="'.base_url().'img/Start.png" >'; 	
					}elseif(!empty($info)){   
	                   echo '<img src="'.base_url().'img/Continue.png" >';
					}
		      }
         	 echo '</a>';
	}
	
	public function listOfQuizzesBaseOnTopic(){
		
		$this->load->model('generalQuizzesInfo');
		$this->load->model('topics');
		$data['tutorialsList']	= $this->generalQuizzesInfo->getAllCompletedBaseOnTopicsId($this->uri->segment('3'));
		$data['topicName'] =  $this->topics->getAllDataBaseOnId($this->uri->segment('3'));
		$this -> load -> view('edu/eduHeader');
		$this -> load -> view('edu/quizTitleListHome',$data);
		$this -> load -> view('edu/eduFooter');
		
	}
	
	public function viewTutorial(){
		$courseId = $this->uri->segment('3');
		$lessionId = $this->uri->segment('4');
		
		$this->load->model('eBookAndVideo');
		$this->load->model('generalQuizzesInfo');
		
		$user = $this -> ion_auth -> user() -> row();
		
		$data['byId']  = $this->eBookAndVideo->getAllDataBaseOnTopicsId($courseId,$user->id);
		
		if($lessionId == "q"){
		    $data['quiz']  = $this -> generalQuizzesInfo -> getAllDataBaseOnTopicsId($courseId);  
		}elseif($lessionId > 0){
			$data['intro'] = $this->eBookAndVideo->getLession($courseId,$lessionId,$user->id);
		}else{
		    $data['intro'] = $this->eBookAndVideo->getIntroVideo($courseId,$user->id);
		}
		$data['courseId'] = $courseId;
		
		$this -> load -> view('edu/eduHeader');
		$this -> load -> view('edu/viewTutorial', $data);
		$this -> load -> view('edu/eduFooter');
		
	}
	
	public function getStartQuizButton($courseId,$quizId){
		
	     $user = $this -> ion_auth -> user() -> row();	
	     
	     $this->load->model('eBookAndVideo');
	     $num = $this -> eBookAndVideo -> getUnWatchedVideo($user->id,$courseId);
	     if($num -> num == 0){
	     	 $user = $this -> ion_auth -> user() -> row();
	     	 $this -> load -> model('candidate');
	     	 $canInfo = $this -> candidate -> getCompletedQuiz($quizId,$user->id);
	     	 $canId = 0;
	     	 $canScore = -1;
	     	 if($canInfo){ $canId = $canInfo->id; $canScore = $canInfo->score; }
	     	 	     	 
	     	 
	     	      echo '<a href="'.base_url().'homePortalEdu/quizzesLoadOneByOne/'.$quizId.'/'.$courseId.'/'.$canId.'"> <button class="btn btn-success"> start </button> </a>';
	     	
	     	 
	     }else{
	     	echo '<button class="btn btn-warning disabled"> Pending lesson(s) : '.$num-> num.' </button>';	
	     	
	     }
	     
	}
	
	public function quizes() {
		$this->load->model('generalQuizzesInfo');
		$this->load->model('candidate');
		$data['generalInfoList'] = $this->generalQuizzesInfo->getCompletedQuizzesinfoGroupInListForClient();
		
		$data['scoreDetails'] = $this->candidate->getAllScoreBaseOnUserId($this->ion_auth->user()->row()->id);
		
		$this -> load -> view('edu/eduHeader');
		$this -> load -> view('edu/quizes',$data);
		$this -> load -> view('edu/eduFooter');
	}
	
	public function quizesStart() {
		
		$this -> load -> view('edu/eduHeader');
		$this -> load -> view('edu/quizesStart');
		$this -> load -> view('edu/eduFooter');
	}
	
	public function quizzesLoadOneByOne(){
		
		$courseId = $this->uri->segment(4);
		$user = $this -> ion_auth -> user() -> row();
		
		$this->load->model('eBookAndVideo');		
		$this->load->model('eBookAndVideo');
		
		$user = $this -> ion_auth -> user() -> row();
		
		$data['byId']     = $this->eBookAndVideo->getAllDataBaseOnTopicsId($courseId,$user->id);
		$data['courseId'] = $courseId;
		
		
		if(empty($courseId)) { redirect(base_url()); }
		
		$num = $this -> eBookAndVideo -> getUnWatchedVideo($user->id,$courseId);
		
		if($num -> num > 0) { redirect(base_url()); }		
				
				
		$this->load->model('eduQuestion');
		$this->load->model('generalQuizzesInfo');
		$this->load->model('candidate');
		
		$segment = $this->uri->segment(3);
		
		$quizCount =array(
			'generalQuizInfoId' => $segment,
			'userName' =>$this->ion_auth->user()->row()->id
		);
		
		
		$qCount = $this->candidate->findDuplicate($quizCount);
		$qCount += 1;
		
		$candidate = array(
			'generalQuizInfoId' => $segment,
			'userName' =>$this->ion_auth->user()->row()->id,
			'attempt' =>$qCount
		);
		
		$this->candidate->addData($candidate);
		
		$data['quiz']=$this->eduQuestion->allQuizWithAnswerBaseOnGeneralInfoId($segment);
		$data['info']  =  $this->generalQuizzesInfo->getAllDataBaseOnId($segment);	
		$data['topicsInfo']  =  $this->generalQuizzesInfo->getTopicsIdBaseOnGeneralQuizId($segment);		
		
		$data['qCount'] = $this->candidate->findDuplicate($quizCount);
		
		// get all last insert data 
		$getCandidateInfo = $this->candidate->getDataBaseOnGeneralQuizInfoIdUserNameAttempt($candidate);
		
		foreach($getCandidateInfo AS $row){
			$data['cId'] = $row->id;
		}		
		shuffle($data['quiz']);  
		
		$this -> load -> view('edu/eduHeader');
		$this -> load -> view('edu/quizzesLoadOneByOne',$data);
		$this -> load -> view('edu/eduFooter');
	}
	
	public function getResult(){
		
		$this->load->model('eduQuestion');
		$this->load->model('generalQuizzesInfo');
		$this->load->model('candidate');
		$this->load->model('candidateResult');	
		
		if($this->input->post('submit')){
			
			$qId = $this->input->post('qId');
			$numberOfQuestion = COUNT($qId);
			$n = 0;
			
			$attempt = $this->input->post('attempt');
			$cId = $this->input->post('cId');
			$correctAnswer = "";
			
			for($n=0; $n <$numberOfQuestion; $n++ ){
				$questionId = $qId[$n];
				$qType = $this->input->post('qtype');
				$qType[$n];
				if($qType[$n] == 3){
					$checkBox = 'answerCheck'.$qId[$n];					
					$checkList = $this->input->post($checkBox);
					foreach($checkList AS $check){						
						$correctAnswerArray[] = $check;
					}
					$correctAnswer = implode(';',$correctAnswerArray);
					$correctAnswerArray = "";
				}else{
					$pass = 'answer'.$qId[$n];
					$correctAnswer = $this->input->post($pass);
				}
				
				$rowData = array(
					'candidateId' => $cId,
					'questionId' => $questionId,
					'answer' => $correctAnswer
				);
				
				$this->candidateResult->addData($rowData);				
			}			
		}
		
		$segment = $this->input->post('cId');
		$data1 = $this->eduQuestion->allQuizWithCandidateAnswerBaseOnGeneralInfoIdAndAttempt($segment);
		$data['quiz']=$this->eduQuestion->allQuizWithCandidateAnswerBaseOnGeneralInfoIdAndAttempt($segment,$attempt);
		$data['info']  =  $this->generalQuizzesInfo->getAllDataBaseOnId($segment);	
		$data['topicsInfo']  =  $this->generalQuizzesInfo->getTopicsIdBaseOnGeneralQuizId($segment);
		
		$rightQuestionAndAnswer = 0;
		$totalQuestionAndAnswer = 0;
		foreach($data1 AS $qInfo){
			$candidateAnswers = explode(";", $qInfo->answer);
			$correctAnswer    = explode(";", $qInfo->correctAnswer);
			
			$result = array_diff($correctAnswer,$candidateAnswers);
			if(empty($result)) { $rightQuestionAndAnswer += 1; }
			$totalQuestionAndAnswer += 1;
		}
		
		floatval($rightQuestionAndAnswer);
		floatval($totalQuestionAndAnswer);
		$score = ((floatval($rightQuestionAndAnswer)/floatval($totalQuestionAndAnswer))*100);		
		
		$updateScore = array(			
			'score' => floatval($score)
		);
		$this->candidate->updateData($cId,$updateScore);
		
		$data['score'] = $score;
		
		$this -> load -> view('edu/eduHeader');
		$this -> load -> view('edu/getResult',$data);
		$this -> load -> view('edu/eduFooter');
	}
	
	
	public function startQuiz(){
		$segment = $this->uri->segment('3');		
		
		$this->load->model('generalQuizzesInfo');
		
		$segment = $this->uri->segment(3);		
		$data['info']  =  $this->generalQuizzesInfo->getAllDataBaseOnId($segment);	
		$data['topicsInfo']  =  $this->generalQuizzesInfo->getTopicsIdBaseOnGeneralQuizId($segment);
				
		$this -> load -> view('edu/eduHeader');
		$this -> load -> view('edu/startQuiz',$data);
		$this -> load -> view('edu/eduFooter');
		
	}
	
	
	public function text(){
		$this->load->model('generalQuizzesInfo');
		$a = $this->generalQuizzesInfo->getAllData();
		
		
			$outp = "";
			foreach($a AS $row) {
			    if ($outp != "") {$outp .= ",";}
			    $outp .= '{"id":"'  . $row->id. '",';
			    $outp .= '"gQname":"'  . $row->gQname  . '",';
			    $outp .= '"details":"'. $row->details   . '"}'; 
			}
			$outp ='{"records":['.$outp.']}';
			
			
			echo($outp);

	}
	
	
	public function viewVideos(){
		$topic = $this -> input -> post('topic');
		$id    = $this -> input -> post('id');
		$watchPersentage = $this -> input -> post('watchPersentage');
		
		$user = $this -> ion_auth -> user() ->  row();
		
		$data = array(
			'topicsId'        => $topic,
			'ebookandvideoId' => $id,
			'userId'          => $user -> id,
			'readDate'        => date('Y-m-d'),
			'readPersentage'  => $watchPersentage				
		);
		
		$this -> load -> model('eBookAndVideo');
		echo $d = $this -> eBookAndVideo -> addEduUserTrack($data);
		
		
	}
	
	
	
	
	// Admin Controllers pages start //
	//   ********************************* //
	

	public function uploadImage(){
		
		if(!$this->ion_auth->in_group ('pharmacyuser' )){ redirect(base_url()); }   
					
			$this -> load -> view('edu/eduHeader');
			$this -> load -> view('edu/uploadImage');
			$this -> load -> view('edu/eduFooter');
		
		
	}

	public function	viewQuizTech(){
		
		if(!$this->ion_auth->in_group ('pharmacyuser' )){ redirect(base_url()); }
		$this->load->model('eduQuestion');
		$this->load->model('generalQuizzesInfo');
		
		$segment = $this->uri->segment(3);
		$data['quiz']=$this->eduQuestion->allQuizWithAnswerBaseOnGeneralInfoId($segment);
		$data['info']  =  $this->generalQuizzesInfo->getAllDataBaseOnId($segment);	
		$data['topicsInfo']  =  $this->generalQuizzesInfo->getTopicsIdBaseOnGeneralQuizId($segment);
		shuffle($data['quiz']);  
		
			$this -> load -> view('edu/eduHeader');
			$this -> load -> view('edu/viewQuizTech',$data);
			$this -> load -> view('edu/eduFooter');		
		
	}

	public function updateTutorials(){
		
		if(!$this->ion_auth->in_group ('pharmacyuser' )){ redirect(base_url()); }
		
		    $path = base_url().'js/ckfinder';
		    $width = '100%';
			$height = '450px';
		    $this->editor($path, $width,$height);
			
			$map['imageFile'] = directory_map('./img/edu/');
			
			$this -> load -> view('edu/eduHeader');
			$this -> load -> view('edu/updateTutorials',$map);
			$this -> load -> view('edu/eduFooter');
		
		
	}
	
	public function viewLessonTech(){
		if(!$this->ion_auth->in_group ('pharmacyuser' )){ redirect(base_url()); }
		
		$this->load->model('eBookAndVideo');
		$data['byId'] = $this->eBookAndVideo->getAllDataBaseOnId($this->uri->segment('3'));
		$this -> load -> view('edu/eduHeader');
		$this -> load -> view('edu/viewLessonTech', $data);
		$this -> load -> view('edu/eduFooter');
		
	}
	
	public function listOfinfoBaseOnTopicTech(){
		if(!$this->ion_auth->in_group ('pharmacyuser' )){ redirect(base_url()); }
		$this->load->model('eBookAndVideo');
		$this->load->model('topics');
		$data['tutorialsList']	= $this->eBookAndVideo->getAllDataBaseOnTopicsId($this->uri->segment('3'));
		$data['topicName'] =  $this->topics->getAllDataBaseOnId($this->uri->segment('3'));
		$this -> load -> view('edu/eduHeader');
		$this -> load -> view('edu/tutorialsTitleListTech',$data);
		$this -> load -> view('edu/eduFooter');
		
	}
	
	public function listOfQuizzesBaseOnUncompletedTopicTech(){
		if(!$this->ion_auth->in_group ('pharmacyuser' )){ redirect(base_url()); }
		$this->load->model('generalQuizzesInfo');
		$this->load->model('topics');
		$data['tutorialsList']	= $this->generalQuizzesInfo->getAllUncompletedBaseOnTopicsId($this->uri->segment('3'));
		$data['topicName'] =  $this->topics->getAllDataBaseOnId($this->uri->segment('3'));
		$this -> load -> view('edu/eduHeader');
		$this -> load -> view('edu/quizTitleList',$data);
		$this -> load -> view('edu/eduFooter');
		
	}
	
	public function listOfQuizzesBaseOnCompletedTopicTech(){
		if(!$this->ion_auth->in_group ('pharmacyuser' )){ redirect(base_url()); }
		$this->load->model('generalQuizzesInfo');
		$this->load->model('topics');
		$data['tutorialsList']	= $this->generalQuizzesInfo->getAllCompletedBaseOnTopicsIdAdmin($this->uri->segment('3'));
		$data['topicName'] =  $this->topics->getAllDataBaseOnId($this->uri->segment('3'));
		$this -> load -> view('edu/eduHeader');
		$this -> load -> view('edu/quizTitleListComplete',$data);
		$this -> load -> view('edu/eduFooter');
		
	}
	
	public function editQuiz(){
		if(!$this->ion_auth->in_group ('pharmacyuser' )){ redirect(base_url()); }
		
		
		$path = base_url().'js/ckfinder';
	    $width = '100%';
		$height = '450px';
	    $this->editor($path, $width,$height);
		
		$this->load->model('topics');		
		$data['allTopics'] = $this->topics->getAllData();	
		
		$this->load->model('generalQuizzesInfo');
		$data['byId'] = $this->generalQuizzesInfo->getAllDataBaseOnId($this->uri->segment('3'));
		
		
		$this -> load -> view('edu/eduHeader');
		$this -> load -> view('edu/editQuiz',$data);
		$this -> load -> view('edu/eduFooter');
	}
	
	public function editQuizCom(){
		if(!$this->ion_auth->in_group ('pharmacyuser' )){ redirect(base_url()); }
		
		
		$path = base_url().'js/ckfinder';
	    $width = '100%';
		$height = '450px';
	    $this->editor($path, $width,$height);
		
		$this->load->model('topics');		
		$data['allTopics'] = $this->topics->getAllData();	
		
		$this->load->model('generalQuizzesInfo');
		$data['byId'] = $this->generalQuizzesInfo->getAllDataBaseOnId($this->uri->segment('3'));
		
		
		$this -> load -> view('edu/eduHeader');
		$this -> load -> view('edu/editQuizCom',$data);
		$this -> load -> view('edu/eduFooter');
	}
	
	public function editLesson(){
		if(!$this->ion_auth->in_group ('pharmacyuser' )){ redirect(base_url()); }
		
		$path = base_url().'js/ckfinder';
	    $width = '100%';
		$height = '650px';
	    $this->editor($path, $width,$height);
		
		$this->load->model('topics');		
		$data['allTopics'] = $this->topics->getAllData();		
		
		
		$this->load->model('eBookAndVideo');
		$data['byId'] = $this->eBookAndVideo->getAllDataBaseOnId($this->uri->segment('3'));
		$this -> load -> view('edu/eduHeader');
		$this -> load -> view('edu/editLesson', $data);
		$this -> load -> view('edu/eduFooter');
		
	}
	
	public function addSubject(){
		if(!$this->ion_auth->in_group ('pharmacyuser' )){ redirect(base_url()); }
		
		$this->load->model('topics');		
		$d['topics'] = $this->topics->getAllData();	
		$this -> load -> view('edu/eduHeader');
		$this -> load -> view('edu/addSubject',$d);
		$this -> load -> view('edu/eduFooter');
		
	}
	
	public function updateQuizzes(){
		if(!$this->ion_auth->in_group ('pharmacyuser' )){ redirect(base_url()); }
			
			$this->load->model('generalQuizzesInfo');
		    $data['generalUncompletedInfoList'] = $this->generalQuizzesInfo->getUncompletedQuizzesinfoGroupInList();
			$data['generalCompletedInfoList'] = $this->generalQuizzesInfo->getCompletedQuizzesinfoGroupInList();
			$data['generalFreezedCompletedInfoList'] = $this->generalQuizzesInfo->getFreezesQuizzesinfoGroupInList();
			$path = base_url().'js/ckfinder';
		    $width = '100%';
			$height = '300px';
		    $this->editor($path, $width,$height);
			
		
			$this -> load -> view('edu/eduHeader');
			$this -> load -> view('edu/updateQuizzes',$data);
			$this -> load -> view('edu/eduFooter');
		
	}
	
	public function manageVideo(){
		if(!$this->ion_auth->in_group ('pharmacyuser' )){ redirect(base_url()); }
		
		$this -> load -> view('edu/eduHeader');
		$this -> load -> view('edu/updateQuizzes');
		$this -> load -> view('edu/eduFooter');
	}
	
	public function manageEbooks(){
		if(!$this->ion_auth->in_group ('pharmacyuser' )){ redirect(base_url()); }
		
		$this -> load -> view('edu/eduHeader');
		$this -> load -> view('edu/updateQuizzes');
		$this -> load -> view('edu/eduFooter');
	}
	
	public function viewBookAndVideo(){
		if(!$this->ion_auth->in_group ('pharmacyuser' )){ redirect(base_url()); }
		
		$id = $this->uri->segment('3');
		
		$this->load->model('eBookAndVideo');
		$data['dataTutorial'] = $this->eBookAndVideo->getAllDataBaseOnId($id);
		
		$this -> load -> view('edu/eduHeader');
		$this -> load -> view('edu/viewBookAndVideo',$data);
		$this -> load -> view('edu/eduFooter');
	}
	
	// Inseert, Update, Delete, and Validation Forms rules
	
	public function addQuizzesGeneralInfo(){
		if(!$this->ion_auth->in_group ('pharmacyuser' )){ redirect(base_url()); }
		
		$path = base_url().'js/ckfinder';
	    $width = '100%';
		$height = '300px';
	    $this->editor($path, $width, $height);
		
		$this->load->model('generalQuizzesInfo');
		$message['generalInfoList'] = $this->generalQuizzesInfo->getGeneralquizzesinfoGroupInList();
		
		$this -> load -> view('edu/eduHeader');	
		
		$this->load->model('generalQuizzesInfo');
		
		$this->form_validation->set_rules('quizeTitle', 'Quize Title', 'required');
		$this->form_validation->set_rules('topicsId', 'Topics Name', 'required');		
		$this->form_validation->set_rules('quizeGeneralInformation', 'quizeGeneralInformation', 'required');
		$this->form_validation->set_rules('quizeTime', 'Quize Time', 'required');
				
		$segment = $this -> input -> post('id');
		
			if($segment > 0) {
				
				$topicName = $this -> input -> post('quizeTitle');
			
				if ($this->form_validation->run() != FALSE){							
						
				
					$data = array(
						'gQname' => $topicName,
						'addBy' => $this->ion_auth->user()->row()->id,
						'topicsId' => $this -> input -> post('topicsId'),
						'details' => $this -> input -> post('quizeGeneralInformation'),
						'timeDutation' => $this -> input -> post('quizeTime'),
						'addDate' => date("Y-m-d H:i:s")
					);		
								
					$this->generalQuizzesInfo->updateData($segment,$data);								 					 
					$message['info']  =  $this->generalQuizzesInfo->getAllDataBaseOnId($segment);
							
				    $message['s'] = "Topics successfully updated";
					$this -> load -> view('edu/addQuestion', $message);	 						
					
				}else{
					//echo validation_errors();
					$this -> load -> view('edu/editQuiz',$message);
				}

				
			}else{
			
		
			$topicName = $this -> input -> post('quizeTitle');
			
			if ($this->form_validation->run() != FALSE){
						
				$dup = array(
					'gQname' => $topicName
				);
				
				$duplicate = $this->generalQuizzesInfo->findDuplicate($dup);
				//$duplicate = 0;
				if($duplicate == 0){
					// Add Topic details here			
					
					$data = array(
						'gQname' => $topicName,
						'addBy' => $this->ion_auth->user()->row()->id,
						'topicsId' => $this -> input -> post('topicsId'),
						'details' => $this -> input -> post('quizeGeneralInformation'),
						'timeDutation' => $this -> input -> post('quizeTime'),
						'addDate' => date("Y-m-d H:i:s")
					);	
					
					$message['s'] = "";						
					$d = $this->generalQuizzesInfo->addData($data);
					if($d == TRUE){
						$dup = array( 'gQname' => $topicName );
											
						 $listBaseOnId = $this->generalQuizzesInfo->getId($dup);
						 
						 foreach ($listBaseOnId as $raw) {
							  $message['id']	=  $raw->id; 
						 }
						 					 
						 $message['info']  =  $listBaseOnId;			
						 $message['s'] = "Topics successfully updated";
						$this -> load -> view('edu/addQuestion', $message);
						 
					}else{
						$message['s'] = "Check Topics Model";
						$this -> load -> view('edu/updateQuizzes', $message);
					}			
					
				
				}else{
					$message['s'] = "This Topics ,\"".$topicName."\" is already updated";
					$this -> load -> view('edu/updateQuizzes', $message);
				}
				
				
			}else{
				//echo validation_errors();
				$this -> load -> view('edu/updateQuizzes',$message);
			}

		}
					
		$this -> load -> view('edu/eduFooter');
		
	} 
	
	public function addQuestion(){
		
		if(!$this->ion_auth->in_group ('pharmacyuser' )){ redirect(base_url()); }
			
			    
			$question           = $this -> input -> post('questionTitle');
			$answerOne          = $this -> input -> post('answerOne');
			$answerTwo          = $this -> input -> post('answerTwo');
			$answerThree        = $this -> input -> post('answerThree');
			$answerFour         = $this -> input -> post('answerFour');
			$quizzesHint        = $this -> input -> post('quizzesHint');
			$generalQuizzesInfo = $this -> input -> post('generalQuizzesInfo');
			
					
			$correctOne     = $this->input->post('correctOne');    			
		    $correctTwo     = $this->input->post('correctTwo'); 
			$correctThree   = $this->input->post('correctThree'); 
			$correctFour    = $this->input->post('correctFour'); 
			
			$this->load->model('eduQuestion');
			$this->load->model('answer');
			
			$answerType = '0';
			$correctAnswer = array();
			
			foreach( $question as $key => $qName ) {
				
				$questionName = $qName;				
				$answerInToArray = $answerOne[$key].";".$answerTwo[$key].";".$answerThree[$key].";".$answerFour[$key];				
				
				if(!empty($correctOne[$key]))   { $correctAnswer[] = $answerOne[$key];    }
				if(!empty($correctTwo[$key]))   { $correctAnswer[] = $answerTwo[$key];    }
				if(!empty($correctThree[$key])) { $correctAnswer[] = $answerThree[$key];  }
				if(!empty($correctFour[$key]))  { $correctAnswer[] = $answerFour[$key];   }			
			
							
				$countAnswer = count($correctAnswer);	
						
				$correctAnswer = implode(",",$correctAnswer)."<br/>";
				
				
				if(empty($answerThree[$key]) && empty($answerFour[$key]))	{
					$answerType = '1';
				}elseif($countAnswer == 1){
					$answerType = '2';
				}else{
					$answerType = '3';
				} 
		  
				$data = array(
					'question'           => $questionName,
					'generalQuizzesInfo' => $generalQuizzesInfo,
					'quizzesHint'        => $quizzesHint,
					'quizzesAnswer'      => $answerInToArray,
					'answerType'         => $answerType,
					'addBy'              => $this->ion_auth->user()->row()->id,
					'addDate'            => date("Y-m-d H:i:s")
				);
				
				$this->eduQuestion->addData($data);
				$getData = array(
					'question'           => $questionName,
					'generalQuizzesInfo' => $generalQuizzesInfo
				);
				
				$questionInfo = $this->eduQuestion->getInfoBaseOnValue($getData);
				$qId = "";
				
				foreach($questionInfo AS $raw) { $qId = $raw->id; }
				
				$dataAnswer = array(
					'questionId'         => $qId,
					'correctAnswer'      => $correctAnswer,
					'generalQuizInfoId'  => $generalQuizzesInfo					
				);	
				
				
				$this->answer->addData($dataAnswer);
				
				$correctAnswer =array();
			}			
		  
			//$this -> load -> view('edu/eduHeader');
			//$this -> load -> view('edu/verifyQuiz');
			//$this -> load -> view('edu/eduFooter');	
			
	}


	public function addQuestionByJquery(){
		if(!$this->ion_auth->in_group ('pharmacyuser' )){ redirect(base_url()); }
			
			    
			$questionName       = $this -> input -> post('questionTitle');
			$answerOne          = $this -> input -> post('answerOne');
			$answerTwo          = $this -> input -> post('answerTwo');
			$answerThree        = $this -> input -> post('answerThree');
			$answerFour         = $this -> input -> post('answerFour');
			$quizzesHint        = $this -> input -> post('quizzesHint');
			$generalQuizzesInfo = $this -> input -> post('generalQuizzesInfo');
			
			
					
			$correctOne     = $this->input->post('correctOne');    			
		    $correctTwo     = $this->input->post('correctTwo'); 
			$correctThree   = $this->input->post('correctThree'); 
			$correctFour    = $this->input->post('correctFour'); 
			
			    $answerType = '0';
			    $correctAnswer = array();
				
				$this->load->model('eduQuestion');
			    $this->load->model('answer');
				
			    $answerInToArray = $answerOne.";".$answerTwo.";".$answerThree.";".$answerFour;				
		
			    
			    
				if($correctOne == 'true')   { $correctAnswer[] = $answerOne;    }
				if($correctTwo == 'true')   { $correctAnswer[] = $answerTwo;    }
				if($correctThree == 'true') { $correctAnswer[] = $answerThree;  }
				if($correctFour == 'true')  { $correctAnswer[] = $answerFour;   }			
			
						
				$countAnswer = count($correctAnswer);	
						
				$correctAnswer = implode(";",$correctAnswer);
				
			
				if(empty($answerThree) && empty($answerFour)){
					$answerType = '1';
				}elseif($countAnswer == 1){
					$answerType = '2';
				}else{
					$answerType = '3';
				} 
		  						 
				$data = array(
					'question'           => $questionName,
					'generalQuizzesInfo' => $generalQuizzesInfo,
					'quizzesHint'        => $quizzesHint,
					'quizzesAnswer'      => $answerInToArray,
					'answerType'         => $answerType,
					'addBy'              => $this->ion_auth->user()->row()->id,
					'addDate'            => date("Y-m-d H:i:s")
				);
					
				
				 
				$this->eduQuestion->addData($data);
				
				$getData = array(
					'question'           => $questionName,
					'generalQuizzesInfo' => $generalQuizzesInfo
				);
				
				$questionInfo = $this->eduQuestion->getInfoBaseOnValue($getData);
				$qId = "";
				
				foreach($questionInfo AS $raw) { $qId = $raw->id; }
				
				$dataAnswer = array(
					'questionId'         => $qId,
					'correctAnswer'      => $correctAnswer,
					'generalQuizInfoId'  => $generalQuizzesInfo					
				);	
				
				$this->answer->addData($dataAnswer);	
				
				echo "Question update successfully!";				
			
			
	}

	public function getQuizQuestionNumberBaseOnGeneralInfoId(){
		if(!$this->ion_auth->in_group ('pharmacyuser' )){ redirect(base_url()); }
		$this->load->model('eduQuestion');
		$gId = $this -> input -> post('generalQuizzesInfo');
		$numberOfQs = $this->eduQuestion->getNumberOfQuestionBaseOnGeneralInfoId($gId);
		$num =0;
		foreach($numberOfQs AS $raw){
			$num = $raw->numberOfQuestion;
		}
		$num = $num + 1;
		if($num > 0) { echo 'Q'.$num; }else{ echo 'Q1'; }		
		 
	}
	
	public function allQuizWithAnswer(){
		if(!$this->ion_auth->in_group ('pharmacyuser' )){ redirect(base_url()); }
			
		$this->load->model('eduQuestion');
		$this->load->model('generalQuizzesInfo');
		
		$segment = $this->uri->segment(3);
		$data['quiz']=$this->eduQuestion->allQuizWithAnswerBaseOnGeneralInfoId($segment);
		$data['info']  =  $this->generalQuizzesInfo->getAllDataBaseOnId($segment);	
		$data['topicsInfo']  =  $this->generalQuizzesInfo->getTopicsIdBaseOnGeneralQuizId($segment);
		shuffle($data['quiz']);  
		
		$this -> load -> view('edu/eduHeader');
	    $this -> load -> view('edu/allQuizWithAnswer',$data);
	    $this -> load -> view('edu/eduFooter');
		
	}
	
	public function editGeneralInfoIndividually(){
		if(!$this->ion_auth->in_group ('pharmacyuser' )){ redirect(base_url()); }
		
		$questionId = $this->uri->segment ( 3 );
		$this->load->model('generalQuizzesInfo');			 
		$isNew['generalInfo'] = $this->generalQuizzesInfo->getAllDataBaseOnId($questionId);				
			
		$this -> load -> view('edu/editGeneralInfoIndividually',$isNew);
	}
	
	public function updateGeneralInfoIndividually(){
		if(!$this->ion_auth->in_group ('pharmacyuser' )){ redirect(base_url()); }
		
		$this->input->post('id');
		$this->input->post('timeDutation');
		
		$data = array(
			'timeDutation' => $this->input->post('timeDutation')
		);
		
		$this->load->model('generalQuizzesInfo');	
		$this->generalQuizzesInfo->updateData($this->input->post('id'),$data);
		
		echo "Record has been updated successfully";				
		
	}
	
	
	public function editQuizIndividually(){
		if(!$this->ion_auth->in_group ('pharmacyuser' )){ redirect(base_url()); }
		
		$questionId = $this->uri->segment ( 3 );
		$this->load->model('eduQuestion');			 
		$isNew['quiz'] = $this->eduQuestion->allQuizWithAnswerBaseOnQuestionId($questionId);				
		$this -> load -> view('edu/eduHeader');		
		$this -> load -> view('edu/editQuizIndividually',$isNew);
		$this -> load -> view('edu/eduFooter');
	}
	
	public function deleteQuestionAndAnswerIndividually(){
		if(!$this->ion_auth->in_group ('pharmacyuser' )){ redirect(base_url()); }
		
		$this->load->model('eduQuestion');
		$this->load->model('answer');
		$id =$this->input->post('id');
		$this->eduQuestion->deleteData($id);
		$this->answer->deleteDataByQuestionId($id);
	}	 
	 
	 
	public function saveEbookTutorial(){
		if(!$this->ion_auth->in_group ('pharmacyuser' )){ redirect(base_url()); }
		
		$path = base_url().'js/ckfinder';
	    $width = '100%';
		$height = '550px';
	    $this->editor($path, $width,$height);
		$this -> load -> view('edu/eduHeader');	
		
		$this->load->model('eBookAndVideo');
		
		$this->form_validation->set_rules('ebookTitle', 'eBook Title', 'required');
		$this->form_validation->set_rules('topicsId', 'Topics Name', 'required');		
		$this->form_validation->set_rules('quizeGeneralInfo', 'description', 'required');
		$this->form_validation->set_rules('documentType', 'Type', 'required');
		if (empty($_FILES['userfile']['name'])){
						
    		//$this->form_validation->set_rules('userfile', 'Image', 'required');
		}
		
		$message['imageFile'] = directory_map('./img/edu/');
		
		$topicName = $this -> input -> post('ebookTitle');
		
		if ($this->form_validation->run() != FALSE){
					
			$dup = array(
				'name' => $topicName
			);
			
			$duplicate = $this->eBookAndVideo->findDuplicate($dup);
			
			if($duplicate == 0){
				// Add Topic details here	
				
				if (!empty($_FILES['userfile']['name'])){ $new_name = time().$_FILES["userfile"]['name']; }				
				
				//$this->do_upload($new_name);	
				
				$data = array(
					'name' => $topicName,
					'addBy' => $this->ion_auth->user()->row()->username,
					'topicsId' => $this -> input -> post('topicsId'),
					//'images' => $new_name,
					'details' => $this -> input -> post('quizeGeneralInfo'),
					'typeInfo' => $this -> input -> post('documentType'),
					'addDate' => date("Y-m-d H:i:s")
				);	
				
				$message['s'] = "";						
				$d = $this->eBookAndVideo->addData($data);
				if($d == TRUE){			 			
					 $message['s'] = "Topics successfully updated";
				}else{
					$message['s'] = "Check Topics Model";
				}
			
			}else{
				$message['s'] = "This Topics ,\"".$topicName."\" is already updated";
			}
			
			$this -> load -> view('edu/updateTutorials', $message);
		}else{
			//echo validation_errors();
			$this -> load -> view('edu/updateTutorials', $message);
		}
					
		$this -> load -> view('edu/eduFooter');
	} 

    public function updateEbookTutorial(){
    	if(!$this->ion_auth->in_group ('pharmacyuser' )){ redirect(base_url()); }
		
		$new_name ="";
		$path = base_url().'js/ckfinder';
	    $width = '100%';
		$height = '650px';
	    $this->editor($path, $width,$height);
		
		$this->load->model('topics');		
		$message['allTopics'] = $this->topics->getAllData();		
		
		$id = $this -> input -> post('id');
		
		$this->load->model('eBookAndVideo');
		
		
		$this -> load -> view('edu/eduHeader');	
		
		$this->load->model('eBookAndVideo');
		
		$this->form_validation->set_rules('ebookTitle', 'eBook Title', 'required');
		$this->form_validation->set_rules('topicsId', 'Topics Name', 'required');		
		$this->form_validation->set_rules('quizeGeneralInfo', 'description', 'required');
		$this->form_validation->set_rules('documentType', 'Type', 'required');
		if (empty($_FILES['userfile']['name'])){
						
    		//$this->form_validation->set_rules('userfile', 'Image', 'required');
		}
		
		$message['imageFile'] = directory_map('./img/edu/');
		
		$topicName = $this -> input -> post('ebookTitle');
		
		if ($this->form_validation->run() != FALSE){
					
			// Edit Topic details here	
				
				if (!empty($_FILES['userfile']['name'])){ $new_name = time().$_FILES["userfile"]['name']; }				
				
				$this->do_upload($new_name);	
				
				$data = array(
					'name' => $topicName,
					'addBy' => $this->ion_auth->user()->row()->username,
					'topicsId' => $this -> input -> post('topicsId'),
					'images' => $new_name,
					'details' => $this -> input -> post('quizeGeneralInfo'),
					'typeInfo' => $this -> input -> post('documentType'),
					'addDate' => date("Y-m-d H:i:s")
				);	
				
				$message['s'] = "";
				$message['id'] = "";						
				$d = $this->eBookAndVideo->updateData($id,$data);
				if($d == TRUE){			 			
					$message['s'] = "Topics successfully updated";
					$message['id'] = $id;
					$message['byId'] = $this->eBookAndVideo->getAllDataBaseOnId($id);
					
				}else{
					$message['s'] = "Check Topics Model";
					$message['id'] = $id;
				}
			$this -> load -> view('edu/editLesson', $message);
		}else{
			//echo validation_errors();
			$this -> load -> view('edu/editLesson', $message);
		}
					
		$this -> load -> view('edu/eduFooter');
	} 
	
	
	public function do_upload($new_name){
		if(!$this->ion_auth->in_group ('pharmacyuser' )){ redirect(base_url()); }
		
		$config['upload_path'] = './img/edu/';
		$config['allowed_types'] = 'gif|jpg|png|mp4';
		$config['max_size']	= '10000';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';
		$config['file_name'] = $new_name;
			
		$this->upload->initialize($config);

		if ( ! $this->upload->do_upload()) {
			
			 $message['s'] = array('error' => $this->upload->display_errors());
			
		} else {
			
			//$data = array('upload_data' => $this->upload->data());
			
		}
	}
	
	
	public function saveVideoTutorial(){
		if(!$this->ion_auth->in_group ('pharmacyuser' )){ redirect(base_url()); }
		
		$this->load->model('eBookAndVideo');
		
		$this->form_validation->set_rules('videoTitle', 'Video Title', 'required');
		$this->form_validation->set_rules('topicsId', 'Topics Name', 'required');
		$this->form_validation->set_rules('videoFile', 'Video File', 'required');
		$this->form_validation->set_rules('videoDis', 'videoDescription', 'required');
		
		$topicName = $this -> input -> post('videoTitle');
		
		if ($this->form_validation->run() != FALSE){
					
			$dup = array(
				'name' => $topicName
			);
			
			$duplicate = $this->eBookAndVideo->findDuplicate($dup);
			
			if($duplicate == 0){
				// Add Topic details here		
				$data = array(
					'name' => $topicName,
					'addBy' => $this->ion_auth->user()->row()->id,
					'topicsId' => $this -> input -> post('topicsId'),
					'images' => $this -> input -> post('videoFile'),
					'details' => $this -> input -> post('videoDis'),
					'typeInfo' => $this -> input -> post('typeInfo'),
					'addDate' => date("Y-m-d H:i:s")
				);	
										
				$d = $this->eBookAndVideo->addData($data);
				if($d == TRUE){			 			
					 echo "Topics successfully updated";
				}else{
					echo "Check Topics Model";
				}
			
			}else{
				echo "This Topics ,\"".$topicName."\" is already updated";
			}
			 
		}else{
			echo validation_errors();
		}
		
	}
	
	public function saveTutorialTopics(){
		if(!$this->ion_auth->in_group ('pharmacyuser' )){ redirect(base_url()); }
		
		$topicName = $this -> input -> post('tutorialTitle');
		
		$this->load->model('topics');
			
		$this->form_validation->set_rules('tutorialTitle', 'tutorialTitle', 'required');	
		
		if ($this->form_validation->run() != FALSE){
			
			//check duplicate record 
			$dup = array(
				'topicsName' => $topicName
			);
			
			$duplicate = $this->topics->findDuplicate($dup);
			
			if($duplicate == 0){
				// Add Topic details here		
				$data = array(
					'topicsName' => $topicName,
					'addBy' => $this->ion_auth->user()->row()->id,
					'addDate' => date("Y-m-d H:i:s")
				);	
										
				$d = $this->topics->addData($data);
				if($d == TRUE){			 			
					 echo "Topics successfully updated";
				}else{
					echo "Check Topics Model";
				}
			
			}else{
				echo "This Topics ,\"".$topicName."\" is already updated";
			}
			 
		}else{
				echo validation_errors();
		}
		
	}
	
	public function getAllTopicsIntoComboBox(){
		
		$this->load->model('topics');		
		$d = $this->topics->getAllData();
		
		echo '<select class="form-control" id="topicsId" name="topicsId"> ';
		foreach ($d as $row)	{
		    echo '<option value="'.$row->id.'" >'.ucfirst($row->topicsName).'</option>';
		}				 	 	     
		echo '</select>';			
	}
	
	public function viewAllTopic(){
		if(!$this->ion_auth->in_group ('pharmacyuser' )){ redirect(base_url()); }
		
		$this->load->model('topics');		
		$d['topics'] = $this->topics->getAllData();		
		
		$this -> load -> view('edu/eduHeader');
		$this -> load -> view('edu/viewAllTopic',$d);
		$this -> load -> view('edu/eduFooter');
					
	}
	
	public function deleteTopic(){
		if(!$this->ion_auth->in_group ('pharmacyuser' )){ redirect(base_url()); }
		
		$topicID = $this->uri->segment ( 3 );
			$dup = array(
				'id' => $topicID
			);
		if ($topicID > 0) {
			$this->load->model('topics');
			$duplicate = $this->topics->findDuplicate($dup);
			if($duplicate > 0){			
				$isNew['topic'] = $topicID;
			}
	   }
		$this -> load -> view('edu/deleteTopic',$isNew);
	}

	public function deleteTopicConfirm(){
		
		$topicID = $this->input-> post('id');
		$this->load->model('topics');		
		$this->topics->deleteData($topicID);
		echo "Record has been deleted sucessfully";
	}

	public function deleteQuiz(){
		if(!$this->ion_auth->in_group ('pharmacyuser' )){ redirect(base_url()); }
			
		$generalquizzesinfo = $this->uri->segment ( 3 );
			$dup = array(
				'id' => $generalquizzesinfo
			);
		if ($generalquizzesinfo > 0) {
			$this->load->model('generalQuizzesInfo');
			$duplicate = $this->generalQuizzesInfo->findDuplicate($dup);
			if($duplicate > 0){			
				$isNew['gId'] = $generalquizzesinfo;
			}
	   }
		
		$this -> load -> view('edu/deleteQuiz',$isNew);
	}
	
	public function deleteQuizConfirm(){
		
		$gID = $this->input-> post('id');
		$this->load->model('generalQuizzesInfo');		
		$this->generalQuizzesInfo->deleteQuizAndAnswer($gID);
		echo "Record has been deleted sucessfully";
	}
	
	public function deleteLesson(){
		if(!$this->ion_auth->in_group ('pharmacyuser' )){ redirect(base_url()); }
		
		$lessonID = $this->uri->segment ( 3 );
		$dup = array(
			'id' => $lessonID
		);
		if ($lessonID > 0) {
			$this->load->model('eBookAndVideo');
			$duplicate = $this->eBookAndVideo->findDuplicate($dup);
			if($duplicate > 0){			
				$isNew['lesson'] = $lessonID;
			}
	   }
		$this -> load -> view('edu/deleteLesson',$isNew);
	}
	
	public function deleteLessonConfirm(){
		
		$lessonID = $this->input-> post('id');
		$this->load->model('eBookAndVideo');		
		$this->eBookAndVideo->deleteData($lessonID);
		echo "Record has been deleted sucessfully";
	}

	public function editTopic(){
		if(!$this->ion_auth->in_group ('pharmacyuser' )){ redirect(base_url()); }
		
		$topicID = $this->uri->segment ( 3 );
		$dup = array(
			'id' => $topicID
		);
		if ($topicID > 0) {
			$this->load->model('topics');
			$duplicate = $this->topics->findDuplicate($dup);
			if($duplicate > 0){					 
				$isNew['topic'] = $this->topics->getAllDataBaseOnId($topicID);				
			}
	   }
		$this -> load -> view('edu/editTopic',$isNew);
	}
	
	public function editTopicConfirm(){
		if(!$this->ion_auth->in_group ('pharmacyuser' )){ redirect(base_url()); }
		
		$this->load->model('topics');	
		
		$topicID = $this->input-> post('id');
		$topicName = $this->input->post('tutorialTitle');
			//check duplicate record 
		$dup = array(
			'topicsName' => $topicName
		);
		
		$duplicate = $this->topics->findDuplicate($dup);
		
		if($duplicate == 0){
			// Add Topic details here		
			$data = array(
				'topicsName' => $topicName,
				'addBy' => $this->ion_auth->user()->row()->id,
				'addDate' => date("Y-m-d H:i:s")
			);	
									
			$d = $this->topics->updateData($topicID,$data);
			if($d == TRUE){			 			
				 echo "Topics successfully updated";
			}else{
				echo "Check Topics Model";
			}
		
		}else{
			echo "This Topics ,\"".$topicName."\" is already updated";
		}		

	}
	
	
	
	public function certificate(){
		if(!$this->ion_auth->in_group ('pharmacyuser' )){ redirect(base_url()); }
		
		$this->load->model('candidate');	
		$data['scoreDetails'] = $this->candidate->getUserInfo();
		
		$this -> load -> view('edu/eduHeader');
		$this -> load -> view('edu/certificate',$data);		
		$this -> load -> view('edu/eduFooter');
		
	}
	
	public function getUserScore(){
		if(!$this->ion_auth->in_group ('pharmacyuser' )){ redirect(base_url()); }
		
		$data = json_decode(file_get_contents("php://input"));
	    $id   = $data->id;
		
		$this->load->model('candidate');
		$data = $this->candidate->getScoreBaseOnUserName($id);
		
		$outp = "";
		foreach( $data AS $record){
			if ($outp != "") {$outp .= ",";}
			$outp .= '{"id":"'         . $record->id         .'",';
			$outp .= '"attempt":"'     . $record->attempt    .'",';
			$outp .= '"score":"'       . $record->score      .'",';
			$outp .= '"Title":"'       . $record->gQname     .'"}';
		}
		$outp ='{"userScoreArray":['.$outp.']}';
		
		echo($outp);
		
	}
	
	public function completeCetificate(){
		
		$data = json_decode(file_get_contents("php://input"));
		$id = $data->id;
		
		$data = array('certified' => 1);
		
		$this -> load -> model('candidate');
		$d = $this -> candidate -> updateData($id,$data);
		
		echo ($d > 0 )? 1 : 0;
	}
	
	public function viewExam(){
		if(!$this->ion_auth->in_group ('pharmacyuser' )){ redirect(base_url()); }
		
		$this -> load -> model('eduQuestion');
		$this -> load -> model('candidate');
		
		$id = $this -> uri -> segment(3);
		$info = $this -> candidate -> getUserExamDetails($id);
		
		$data['info'] = $info;		
		$data['quiz']=$this->eduQuestion->allQuizWithCandidateAnswerBaseOnGeneralInfoIdAndAttempt($id);		
		
		$this -> load -> view('edu/viewExam',$data);
	}
	
	public function printExam(){
		if(!$this->ion_auth->in_group ('pharmacyuser' )){ redirect(base_url()); }
		
		$this -> load -> model('eduQuestion');
		$this -> load -> model('candidate');
		
		$id = $this -> uri -> segment(3);
		$info = $this -> candidate -> getUserExamDetails($id);
		
		$data['info'] = $info;		
		$data['quiz']=$this->eduQuestion->allQuizWithCandidateAnswerBaseOnGeneralInfoIdAndAttempt($info->generalQuizInfoId,$info->attempt);	
		
		$this -> load -> view('edu/printExam',$data);
		
	}
	
	
	
	
	
	
	// Global functions
	
	public function editPortalUserModalBySeperatePageRun() {
		
		$this->form_validation->set_rules('first_name', 'First Name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('groups', 'groups', 'required');
		$this->form_validation->set_rules('company', 'Company', 'required');
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('phone', 'Phone #', 'required');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required');	
		
		if ($this->form_validation->run() != FALSE){
			
			$additional_data = array(
				 'first_name' => $this -> input -> post('first_name'),
				 'groups' => $this -> input -> post('groups'), 
				 'email' => $this -> input -> post('email'), 
				 'company' => $this -> input -> post('company'),
				 'username' => $this -> input -> post('username'),
				 'password' => $this -> input -> post('password'), 
				 'phone' => $this -> input -> post('phone'), 
				 'last_name' => $this -> input -> post('last_name')				 
				 );
				 
			 $up =  $this -> ion_auth -> update($this -> input -> post('id'),$additional_data );
			 echo "Your personal information had been  changed successfully ";
			 
		}else{
				echo validation_errors();
		}	 
		 		
	}

	public function changePortalUserPassword() {
		
		$this->form_validation->set_rules('currentPassword', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required|matches[passconf]');
		$this->form_validation->set_rules('passconf', 'Password Confirmation', 'required');
		
		
		$id = $this -> input -> post('id');
		$originalPassword = $this -> input -> post('currentPassword');
		
		if ($this->form_validation->run() != FALSE){
		
			$additional_data = array(			 
				 'password' => $this -> input -> post('password')			 
				 );
						
			if ($this->ion_auth->hash_password_db($id, $originalPassword) == TRUE){
					
				 $up =  $this -> ion_auth -> update($this -> input -> post('id'),$additional_data );
				 echo "Your password had been  changed successfully ";
			}else{
				echo "Your current password is wrong"; 
			}				 
		 		
		} else {
			echo validation_errors();
		}
	}
	
	public function getBookAndVideoTitleInList(){
		
		$this->load->model('eBookAndVideo');		
		$d = $this->eBookAndVideo->getAllDataBaseOnTypeInFo($this->input->post('typeInfo'));		
		$i = 1;	
    	foreach ($d as $row)	{
		    //echo '<option value="'.$row->id.'" >'.$row->topicsName.'</option>';
			echo $i++.') <a href="'.base_url().'homePortalEdu/viewBookAndVideo/'.$row->id.'" <li>'.ucfirst($row->name).'  </li>  </a><br/>';
		}		
	}	
	
		
	public function getQuizzesTitleInList(){
		
		$this->load->model('generalQuizzesInfo');		
		$d = $this->generalQuizzesInfo->getAllData();		
		$i = 1;	
    	foreach ($d as $row)	{
		    //echo '<option value="'.$row->id.'" >'.$row->topicsName.'</option>';
			echo $i++.') <a href="'.base_url().'homePortalEdu/viewQuizzes/'.$row->id.'" <li>'.ucfirst($row->gQname).'  </li>  </a><br/>';
		}		
	}
	
	
	public function getTopicListInBox() {
		
		$this->load->model('eBookAndVideo');
		$data = $this->eBookAndVideo->getTopicsGroupInList();
		
		foreach ($data AS $info) {
        	echo '<a href="'.base_url().'homePortalEdu/listOfinfoBaseOnTopicTech/'.$info->topicsId.'" class="list-group-item ">
           		 <i class="fa fa-book"></i> '.ucfirst($info->topicsName).' <span class="badge badge-info">'.$info->numberOftopic.'</span>
      		 </a>';			        
        
		} 
		
	}	
	
	
	// Ck editor
	
	function editor($path,$width,$height) {
		
	    $this->load->library('ckeditor');
	    $this->load->library('ckFinder'); 
	    $this->ckeditor->basePath = base_url().'js/ckeditor/';
	    $this->ckeditor-> config['toolbar'] = array(
			    array('Bold', 'Italic', 'Underline', 'Strike'),
			    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'),
			    array('Format'),
			    array( 'Font', 'FontSize', 'FontColor', 'TextColor' , 'BGColor'),
			    array('Subscript','Superscript', '-','Link', 'Unlink'),
			    array('Anchor'),
			    array('Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord' ),
			    array('Image', 'Table', 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', 'Blockquote', 'Iframe', 'HorizontalRule', 'Smiley'),    			    
			    array('Maximize','Source'),
			    array('Checkbox', 'Radio','Button', 'ImageButton')
   		);
   		//$this->ckeditor->config['extraPlugins'] = 'imgupload';
		$this->ckeditor->config['filebrowserImageUploadUrl'] = base_url().'js/ckeditor/plugins/imgupload.php';
	    $this->ckeditor->config['language'] = 'en';
	    $this->ckeditor->config['width'] = $width;
		$this->ckeditor->config['height'] = $height;
	
	    $this->ckfinder->SetupCKEditor($this->ckeditor,$path); 
		
  	}

	function doMultiImageupload(){
        $upload_conf = array(
            'upload_path'   => './img/edu/',
            'allowed_types' => 'gif|jpg|png',
            'max_size'      => '30000',
            );
    
        $this->upload->initialize( $upload_conf );
    
        // Change $_FILES to new vars and loop them
        foreach($_FILES['userfile'] as $key=>$val) {
            $i = 1;
            foreach($val as $v) {
                $field_name = "file_".$i;
                $_FILES[$field_name][$key] = $v;
                $i++;   
            }
        }
		
        // Unset the useless one ;)
        unset($_FILES['userfile']);
    
        // Put each errors and upload data to an array
        $error = array();
       // $success = array();
        
        // main action to upload each file
        foreach($_FILES as $field_name => $file){
            if ( ! $this->upload->do_upload($field_name)) {
                // if upload fail, grab error 
                $error['upload'][] = $this->upload->display_errors();
				
            } else {
                $data['success'] = "Images are successfully updated ";                
            }
        }

        // see what we get
        if(count($error > 0)){
            $data['error'] = $error;
        } else{
            $data['success'] = "Images are successfully updated ";
        }
        
		$this -> load -> view('edu/eduHeader');
		$this -> load -> view('edu/uploadImage',$data);
		$this -> load -> view('edu/eduFooter');
    }
	

}
?>