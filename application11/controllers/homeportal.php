<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

include_once ('secure_controller.php');
class homeportal extends Secure_Controller {
	
		
	function __construct() {
		// Call the Model constructor
		parent::__construct();
					
		
		if ($this -> quick -> accessTo('homeportal')) {
			// do nothing they are legit
		} else {
			// current user should not have access to this controller.
			redirect('news/mynews');
		}

		if (!isset($this -> session -> userdata['user']['language'])) {
			$data = array('language' => 'english');
			$this -> session -> set_userdata('user', $data);
		}

		$this -> lang -> load('portal', $this -> session -> userdata['user']['language']);
	}

	public function index() {
		if (!$this -> ion_auth -> logged_in()) {
			redirect(base_url().'auth/login');
		}
		
		$NHID = $this->ion_auth->user()->row()->NHID;
		$userId = $this->ion_auth->user()->row()->id;
		
		$data['NHID'] = $NHID;
		
		if ($NHID > 0) {			
			$data['patInfo'] = $this->rest->get('production/exApi/patInfoBaseOnNHID/'.$NHID);			
		}else{
			
			$this->load->model('assignPatToCG');
			
			$CGinfo = $this->assignPatToCG->getInfoBaseOnCGID($userId);
			
			
		
			if(!empty($CGinfo)){
				$patId = "";
				foreach($CGinfo AS $recordCG){
					$patId = $recordCG->patID;
				}								
				
				$this->load->model('Pat');
				$data['patInfo'] =  $this->Pat->getInfoBaseOnListOfPatID($patId);			
								
			}
			
		}	
		
		$this -> load -> view('homeportal/header');
		$this -> load -> view('homeportal/patients',$data);
		$this -> load -> view('homeportal/footer');
	}

	public function addCG() {		 
				
		$this->form_validation->set_rules('first_name', 'First Name', 'required');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required');
		$this->form_validation->set_rules('username', 'User Name', 'required');
		
				
		if ($this->form_validation->run() != FALSE){
				
			$additional_data = array(
			 'first_name' => $this -> input -> post('first_name'),
			 'company' => $this -> input -> post('company'),			 
			 'phone' => $this -> input -> post('phone'), 
			 'last_name' => $this -> input -> post('last_name'),
			 'bossID'   => $this->ion_auth->user()->row()->NHID,
			 'addBy'    => $this->ion_auth->user()->row()->first_name. '.' .$this->ion_auth->user()->row()->last_name
			 
		    );
		  
		  		if (!$this->ion_auth->username_check($this -> input -> post('username'))){
				    $group_name = array('18');
						 		
					$up =  $this -> ion_auth -> register(trim($this -> input -> post('username')), $this -> input -> post('password'), trim($this -> input -> post('email')), $additional_data, $group_name);
					
					echo 'Successfully Created';
					
				}else{
					echo 'User name already taken, Try with another user name';
				}
		}else{
			echo validation_errors(); 
		}
		
	}
	
	public function openPDF(){
		header('Content-type: application/pdf');
		echo read_file("img/Portal Update Letter March 2016.pdf");		

	}
	
	
	public function assignClient(){
		$this -> load -> view('homeportal/header');
		$NHID = $this->ion_auth->user()->row()->NHID;
		
		$userId = $this->uri->segment(3);
		
	   if ($NHID > 0) {
	  		$this->load->model('assignPatToCG');
			
			$CGinfo = $this->assignPatToCG->getInfoBaseOnCGID($userId);
		
			if(!empty($CGinfo)){
				$patId = "";
				foreach($CGinfo AS $recordCG){
					$patId = $recordCG->patID;
				}								
				$patArray = explode(",",$patId);
				$data['patID'] = $patArray;			
			}
				
			$data['patInfo'] = $this->rest->get('production/exApi/patInfoBaseOnNHID/'.$NHID);
			$data['user'] = $this->ion_auth->user($userId)->row();
			
				
			$this -> load -> view('homeportal/assignClient',$data);		
		}else{
			redirect(base_url());			
		}
				
		$this -> load -> view('homeportal/footer');
	}
	
	public function getNumberOfTicketBaseOnPatId($id){
		
		$this->load->model('tickets');
		echo $this->tickets->getNumberOfTicketsBaseOnPatId($id);
	}
	
	public function getNumberOfMedChangesBaseOnPatId($id){
		$this->load->model('tickets');
		echo $this->tickets->getNumberOfMedChangesBaseOnPatId($id);
	}
	

	public function viewPat() {
		$check = $this -> pharm -> userPatientAccess($this -> uri -> segment(3));

		if ($check == FALSE) {
			redirect(base_url());
		} else {
			$this -> load -> view('homeportal/header');
			$this -> load -> view('homeportal/viewPatient');
			$this -> load -> view('homeportal/footer');
		}
	}
	
	public function production() {
		$this -> load -> view('header');
		//$this -> load -> view('compliance/production'); 
		echo "aaa";
		$this -> load -> view('footer');
	}

	public function viewPatTest() {
		$check = $this -> pharm -> userPatientAccess($this -> uri -> segment(3));

		if ($check == FALSE) {
			redirect(base_url());
		} else {
			$this -> load -> view('homeportal/header');
			$this -> load -> view('homeportal/Copy of viewPatient');
			$this -> load -> view('homeportal/footer');
		}
	}

	// events
	public function userEvents() {
		$NHID = $this->ion_auth->user()->row()->NHID;
		$userId = $this->ion_auth->user()->row()->id;
		
		$data['NHID'] = $NHID;
		
		if ($NHID > 0) {			
			$data['patInfo'] = $this->rest->get('production/exApi/patInfoBaseOnNHID/'.$NHID);			
		}else{
			$this->load->model('assignPatToCG');
			
			$CGinfo = $this->assignPatToCG->getInfoBaseOnCGID($userId);
		
			if(!empty($CGinfo)){
				$patId = "";
				foreach($CGinfo AS $recordCG){
					$patId = $recordCG->patID;
				}	
				
				$this->load->model('Pat');
				$data['patInfo'] =  $this->Pat->getInfoBaseOnListOfPatID($patId);	
											
				//$patArray = explode(",",$patId);
				//$data['patAssigined'] = $patArray;			
			}
		}
		
		$this -> load -> view('homeportal/header');
		$this -> load -> view('homeportal/userEvents',$data);
		$this -> load -> view('homeportal/footer');
	}

	public function clientEvents() {
		//$check = $this -> pharm -> userPatientAccess($this -> uri -> segment(3));
		
		//$this->load->model('assignPatToCG');
		//$check = $this->assignPatToCG->getAccessCOnfirm($this -> uri -> segment(3),  $this->ion_auth->user()->row()->id);

		//if ($check == FALSE) {
			//redirect(base_url());
		//} else {
		    
			$this->load->model('Pat');
		
		     $patID = $this->uri->segment ( 3 );
	         $data['patient'] = $this -> Pat -> getPatInfoBaseOnLastIdRow($patID);
			 $data['user']    = $this->ion_auth->user ()->row ();
		
			$this -> load -> view('homeportal/header');
			$this -> load -> view('homeportal/clientEvents',$data);
			$this -> load -> view('homeportal/footer');
		//}
	}
	
	public function ListOfPatForAgents(){
		$NHID = $this->ion_auth->user()->row()->NHID;
		$userId = $this->ion_auth->user()->row()->id;
		
		$data['NHID'] = $NHID;
		
		if ($NHID > 0) {
			
			$data['patInfo'] = $this->rest->get('production/exApi/patInfoBaseOnNHID/'.$NHID);
			
			$this->load->model('userHome');
			$info = $this->userHome->getHomeIdBaseOnUseId($userId);
			
			
			if(!empty($info)){
					$homeId= "";
					foreach($info AS $record){
						$homeId = $record->homeID;
					}
			
				$userInfo = $this->userHome->getUserIdBaseOnHomeId($homeId);
				
				
				if(!empty($userInfo)){	
					foreach($userInfo AS $record){
						$id = $record->userID;
						if($userId != $id){
							 $additional_data = array(
							      'bossID'   => $NHID,
							 	  'addBy'    => $this->ion_auth->user()->row()->first_name. '.' .$this->ion_auth->user()->row()->last_name
							 );	
							 //$up =  $this -> ion_auth -> update($id,$additional_data);
						}
					}
				}
				
			}
			
			
			$this->load->model('users');
			$data['CG'] = $this->users->getCareGivers($NHID);
	    
			
			$this -> load -> view('homeportal/header');
			$this -> load -> view('homeportal/ListOfPatForAgents',$data);
			$this -> load -> view('homeportal/footer');
						
		} else {
			
			redirect(base_url());
		}
		
	}

    public function activeInactiveuser(){
    	
    	header("Access-Control-Allow-Origin: *");
	    header("Content-Type: application/json; charset=UTF-8");
	
		$in = json_decode(file_get_contents("php://input"));
	    $id   = $in->id;	
		
		$user = $this->ion_auth->user($id)->row();
		
		if($user->active == 0){
			$data = array( 'active' => 1 );
		}else{
			$data = array( 'active' => 0 );
		}
		
		$this->ion_auth->update($id, $data);
		
		echo 1;
    }
	
    
	public function disposal(){
		$this -> load -> model('Pat');
		
	    $patId = $this -> uri -> segment(3);
	    $page  = $this -> uri -> segment(4);
	    
	    $start = ($page > 0 ) ? $page : 0;
	    $limit = 10;
	    
	    
	    $this->load->library('pagination');
	    
	  
	    
	    if($patId){
	    	
	    	$this -> load -> model('allergy');
	    	
		    $data['medInfo'] = $this->rest->get('production/exApi/getRxForDesporsal/'.$patId);
		    $data['patInfo'] = $this->Pat->getPatInfoBaseOnLastIdRow($patId);    
		    
		    $total = $this -> allergy -> getTotalNumberOfDisposalForPat($patId);
		    
		    $config['base_url']    = base_url() .'homeportal/disposal/'.$patId.'/';
		    $config['total_rows']  = $total -> num;
		    $config['per_page']    = $limit;
		    $config["uri_segment"] = 4;
		    $config['cur_tag_open'] = '&nbsp;<a class="current">';
			$config['cur_tag_close'] = '</a>';
			$config['next_link'] = 'Next';
			$config['prev_link'] = 'Previous';
			
			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] ="</ul>";
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";
		    
		     
		    $this->pagination->initialize($config);
		     
		    $data['total'] = $this -> allergy -> getTotalNumberOfDisposalForPat($patId);
		    $data['info']  = $this -> allergy -> getPaginatedInfoForPat($patId,$start,$limit);
		    $data['link']  = $this->pagination->create_links();
		    
		    
		    $this -> load -> view('homeportal/header');
		    $this -> load -> view('homeportal/disposal',$data);
		    $this -> load -> view('homeportal/footer');
	    }else{
	    	redirect('homeportal');
	    }
	    	    	    
	}
	
	public function viewDisposal(){
		$this -> load -> model('allergy');
		$this -> load -> model('Pat');
		
		$id = $this -> uri -> segment(3);
		$infoT           = $this -> allergy -> getDisposalInfoBaseOnId($id);
		$data['info']    = $infoT;
		$data['patInfo'] = $this -> Pat -> getPatInfoBaseOnLastIdRow($infoT -> patId);
		
		$this -> load -> view('homeportal/viewDisposal',$data);
		
	}
	
	public function printDisposal(){
		$this -> load -> model('allergy');
		$this -> load -> model('Pat');
		
		$id = $this -> uri -> segment(3);
		$infoT           = $this -> allergy -> getDisposalInfoBaseOnId($id);
		$data['info']    = $infoT;
		$data['patInfo'] = $this -> Pat -> getPatInfoBaseOnLastIdRow($infoT -> patId);
		
		$this -> load -> view('homeportal/printDisposal',$data);
		
	}
	
	public function createDisposal(){
		$printBy = $this -> input -> post('printBy');
		$info    = $this -> input -> post('info');
		$patId   = $this -> input -> post('patId');
		
		$this -> load -> model('allergy');
		
		$user = $this -> ion_auth -> user() -> row();
		
		$data = array(
			'printBy'     => $printBy,
			'info'        => $info,
			'patId'       => $patId,
			'createdDate' => date('Y-m-d'),
			'userId'      => $user -> id
		);
		
		$d = $this -> allergy -> addDisposalInfo($data);
		
		if($d > 0){
			echo '1|Successfully created';
		}else{
			echo '0|Something went wrong!, Try again';
		}
	}
	
	public function getMedInfoByRxNum($rx){
		
		$rxInfo = $this->rest->get('production/exApi/getRxInfo/'.$rx);
						
		foreach($rxInfo AS $record){
			echo $record->BrandName."(".$record->Strength.")";
		}
	}
	
	public function getDisposalDetails(){
		
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=UTF-8");
		
		$this -> load -> model('allergy');
		$info = $this -> allergy -> getAllDisposalInfo();
		
		//$info = json_decode(file_get_contents("php://input"));
		//$patId = $info -> patId;
		
		if($info){
		
			$outPut = "";
			
			foreach($info AS $record){
				if ($outPut != "") {$outPut .= ",";}
				$outPut .= '{"id":"'    . $record->id.'",';
				$outPut .= '"dateCreated":"'  . $record->createdDate.'"}';
			}
			
			echo $outPut ='{"disposalDetailsInfo":['.$outPut.']}';		
		
		}else{
			echo $outPut = '"statusText":["no result"]';
		}
		
	}
	
			
	public function agencyResetCGpwd(){
		
		 $userId = $this->input->post('userId');
		 $newPWD = $this->input->post('newPassword');
		 $newPWDConfirm = $this->input->post('newPasswordAgain');
		 
		
		 
		 if(!empty($newPWD)) {
		 	  $d  = strcmp($newPWD,$newPWDConfirm); 
		 }else{
		 	$d = 1;
		 }		 
		 
		 if ($d == 0) {
		 	
		 	
			 $data = array(
			 		
				 'password' => $newPWD
				 
			 );
		
			 $up =  $this -> ion_auth -> update($userId,$data );
			 
			 echo "Update succesfully"; 
			
		 }else{
		 	
		 	  echo "Password does not match."; 
			  
		 }		 
		 
	}
	
	public function AssignPatForCG(){
		
		$CG = $this->input->post('CG');
		$PatId = $this->input->post('id');
		
		$data = array(
			'CGID'       => $CG,
			'patID'      => $PatId,
			'assignBy'   => $this->ion_auth->user()->row()->id,
			'assignDate' => date('Y-m-d')
		);
		
		$this->load->model('assignPatToCG');
		
		$CGinfo = $this->assignPatToCG->getInfoBaseOnCGID($CG);
		
		if(!empty($CGinfo)){
			foreach($CGinfo AS $recordCG){
				$this->assignPatToCG->deleteData($recordCG->id);
			}			
		}
		
		$d = $this->assignPatToCG->addData($data);
		
		if($d){
			echo "Successfully Assigined";
		}else{
			echo "Try again!";
		}
		
	}
	
	public function viewpatInNewMethod(){
		
		$patId = $this->uri->segment(3);		
		
		$data['patRx'] = $this->rest->get('production/exApi/rxPat/'.$patId);
		$data['patInfo'] = $this->rest->get('production/exApi/patInfoBaseOnPatID/'.$patId);
		$data['allergy'] = $this->rest->get('production/exApi/patAllergyBaseOnPataID/'.$patId);
		$data['condition'] = $this->rest->get('production/exApi/patConBaseOnPataID/'.$patId);
		
		
		$this -> load -> view('homeportal/header');
		$this -> load -> view('homeportal/viewpatInNewMethod',$data);
		$this -> load -> view('homeportal/footer');
		
	}
	
	
	public function getUserExamResult(){
	    $userId = $this -> uri -> segment(3);
	    
	    $this->load->model('candidate');
	    
	    $data['examInfo'] = $this->candidate->getScoreBaseOnUserName($userId);
	    $data['user']     = $this -> ion_auth -> user($userId) -> row();
	    
		$this -> load -> view('homeportal/header');
		$this -> load -> view('homeportal/getUserExamResult',$data);
		$this -> load -> view('homeportal/footer');
	
	}
	
	
	public function getNHWardInfo($id){
				
		$info = $this->rest->get('production/exApi/NHWardInfoFromPatTable/'.$id);
		
		foreach($info AS $record){
			echo $record->LastName.' '.$record->FirstName.'<br/>'.$record->Address1.', '.$record->Address2.'<br/>'.$record->PatCity.', '.$record->PatPro.'<br/>'.$record->PatPostal;
			
			$getPhone = $this->rest->get('production/exApi/PhoneNumberBaseOnPatID/'.$record->ID);
			
			foreach($getPhone AS $phoneNum){
				echo $phoneNum->Description." : ".$phoneNum->Phone."<br/>";
			}
			
			echo "<hr>";
			
			echo ($record->DeliveryRouteType == 0 ? "Regular Delivery on ": "Pick Up");
			$d = $record->DeliveryRoute;
			
			$day = substr($d, 0,3);
		    $time = substr($d,3,4);
					
		
			if($day == "MON"){ echo $outp =" MONDAY";    }
			if($day == "TUE"){ echo $outp =" TUESDAY";   }
			if($day == "WED"){ echo $outp =" WEDNESDAY"; }
		    if($day == "THU"){ echo $outp =" THURSDAY";  }
		    if($day == "FRI"){ echo $outp =" FRIDAY";    }
		    
			if($time == "1000"){ echo " 10 AM.";   }
			if($time == "1300"){ echo " 01 PM.";   }
			if($time == "1500"){ echo " 03 PM.";   }	
			
			echo "<br/>";
			$d = strtotime("next $outp");
			echo "Next delivery date is ".date('Y-m-d',$d).".";	
			
		}
	}
	
	public function getHomePortalUserInfo(){
		
		header("Access-Control-Allow-Origin: *");
	    header("Content-Type: application/json; charset=UTF-8");
	
		$in = json_decode(file_get_contents("php://input"));
	    $id   = $in->id;	
		
		$user = $this->ion_auth->user($id)->row();
		
		$outp = "";       
        $outp .= '{"id":"'           . $user->id.'",';
        $outp .= '"firstName":"'     . $user->first_name.'",';
        $outp .= '"lastName":"'      . $user->last_name.'"}';
      
        $outp ='{"homeUserInfo":['.$outp.']}';      
        
       	echo($outp);       
	}
	
	
	public function getAllergyInfo($code){
		$this->load->model('allergy');
		$info = $this->allergy->getAllDataBaseOnId($code);
		
		foreach($info AS $record){
			echo $record->allergenDesc;
		}		
	}
	
	public function getConditionsInfo($code){
		
		$this->load->model('allergy');
		$info = $this->allergy->getConditionInfoDataBaseOnId($code);
		
		foreach($info AS $record){
			echo $record->description;
		}
	}

	public function testclientEvents() {
		$this -> load -> view('homeportal/header');
		$this -> load -> view('homeportal/testclientEvents');
		$this -> load -> view('homeportal/footer');
	}
    
	public function profile(){
		
		$this->load->model('emailauth');
		$user = $this->ion_auth->user()->row();
		$info = array('userId' => $user->id);
		$d = $this->emailauth->findDuplicate($info);
		if($d == 0 ){
			$da = array('userId' => $user->id);
			$this->emailauth->addData($da);
		}
		
		$data['emailAuth'] = $this->emailauth->getInfoBaseOnUserId($user->id);		

		$this -> load -> view('homeportal/header');
		$this -> load -> view('homeportal/profile',$data);
		$this -> load -> view('homeportal/footer');
		
	}
	
	public function saveEvent() {
		echo $this -> portal -> updateEvent($this -> input -> post('eventID'), $this -> input -> post('eventTypeID'), $this -> input -> post('eventDate'), $this -> input -> post('reportedByUser'), $this -> input -> post('desc'), $this -> input -> post('patID'));
	}

	public function savePRNMonitor() {
		echo $this -> portal -> updatePRNMonitor($this -> input -> post('eventID'), $this -> input -> post('prn1desc'), $this -> input -> post('prn2given'), $this -> input -> post('prn2desc'), $this -> input -> post('protocol'), $this -> input -> post('timeEffective'), $this -> input -> post('staffInitial'));
	}

	public function eventModal() {
		$this -> load -> view('homeportal/eventModal');
	}

	public function prnAdminModal() {
		$this -> load -> view('homeportal/prnAdminModal');
	}

	// incidents
	public function incidents() {
		$this -> load -> view('homeportal/header');
		$this -> load -> view('homeportal/incidentsDash');
		$this -> load -> view('homeportal/footer');
	}

	public function viewincident() {
		$this -> load -> view('homeportal/header');
		$this -> load -> view('homeportal/incidents/viewIncident');
		$this -> load -> view('homeportal/footer');
	}

	public function users() {
		$this -> load -> view('homeportal/header');
		$this -> load -> view('homeportal/users');
		$this -> load -> view('homeportal/footer');
	}

	// TICKETS
	public function tickets() {
		
		$NHID = $this->ion_auth->user()->row()->NHID;
		$userId = $this->ion_auth->user()->row()->id;
		
		$data['NHID'] = $NHID;
		
		if ($NHID > 0) {			
			$data['patInfo'] = $this->rest->get('production/exApi/patInfoBaseOnNHID/'.$NHID);			
		}else{
			$this->load->model('assignPatToCG');
			
			$CGinfo = $this->assignPatToCG->getInfoBaseOnCGID($userId);
		
			if(!empty($CGinfo)){
				$patId = "";
				foreach($CGinfo AS $recordCG){
					$patId = $recordCG->patID;
				}	
				
				$this->load->model('Pat');
				$data['patInfo'] =  $this->Pat->getInfoBaseOnListOfPatID($patId);	
											
				//$patArray = explode(",",$patId);
				//$data['patAssigined'] = $patArray;			
			}
		}
		
		$this -> load -> view('homeportal/header');
		$this -> load -> view('homeportal/tickets',$data);
		$this -> load -> view('homeportal/footer');
		
	}
	
	public function getTicketsBaseONPatID($type,$patId){
		
		$this->load->model('tickets');
		$info = $this->tickets->getTicketBaseOnPatId($type,$patId);
		
		//echo $patId."<br/>";
		
		
		foreach($info AS $record){
			echo '<tr  class="curses">
				  <td >
					<h6 class="pull-right">'.$record->dateCreated.'</h6>
					<h3>
						<table>
							<tr>
								<td onclick="loadModal(\''.base_url().'homeportal/editTicketModal/'. $record->patID.'/'.$record->ticketID.'\')">
									<span class="">'.$record->F.' '. $record->L .'</span>
									
								</td>
								<td>
									<p class="ratebox" data-id="'.$record->ticketID.'" data-rating="'.$record->rank.'"></p> 
								</td>
							</tr>
							<tr>
								<td onclick="loadModal(\''.base_url().'homeportal/editTicketModal/'. $record->patID.'/'.$record->ticketID.'\')">
									<span class="btn btn-primary">VIEW TICKET</span>';
									 if($record->dateComplete!=''){ echo '<img src="' .base_url('img/check.png') . '" width="25px" height="25px">'; }
								echo '</td>							
								
							</tr>
						</table>
					 </h3>
					<div class="well well-sm">'.$record->Description.'<br/><br/>';						
						if(!empty($record->rxnumList)){		
						    $rxInfo = $this->rest->get('production/exApi/rxInfo/'.$record->rxnumList);
							foreach($rxInfo AS $record){
								echo 'Rx# '.$record -> RxNum.' : '.$record -> BrandName.' ('.$record -> Strength.')<br/>';									
							}
						}
					echo'</div></td>					
			</tr>';
		} 
	}
	

	public function viewTicket() {
		$this -> load -> view('homeportal/header');
		$this -> load -> view('homeportal/viewTicket');
		$this -> load -> view('homeportal/footer');
	}

	public function editTicketModal() {
		$this->load->model('Pat');
		
		$data['user'] = $this -> ion_auth -> user() -> row();
		$data['ticketTypeReference'] = $this->comp->getTicketTypeReference ();
		
		if ($this -> uri -> segment(4) > 0) {
				
			$ticketID       = $this -> uri -> segment(4);
			$Ticket         = $this -> comp -> getTicket($this -> uri -> segment(4));
			$data['Ticket'] = $Ticket;
			if(!empty($Ticket)){
				$patID = $Ticket -> patID;
				$data['patient'] = $this -> Pat -> getPatInfoBaseOnLastIdRow($patID);
		
			}
				
			$data['comments'] = $this->comp->getDiscussion ($ticketID);
				
		} elseif($this -> uri -> segment(3) > 0) {
				
			$patID = $this -> uri -> segment(3);
			$data['patient'] = $this -> Pat -> getPatInfoBaseOnLastIdRow($patID);
				
		}else{
			redirect('ticket');
		}
		
		$this -> load -> view('homeportal/editTicketModal',$data);
	}
	
	public function deleteTicket() {
		$this -> load -> view('homeportal/deleteTicket');
	}

	public function saveTicket() {
		// email confirmation
		$pId = $this -> input -> post('patID');
		$data = $this->comp->getEmailAddressFromPatId($pId);
		
		$listEmail = "";
		foreach($data AS $info){
			$listEmail[] = $info->email;
		}
		$listEmail = array_filter($listEmail);
		$listEmail = implode(",",$listEmail); 
		
		
		$this->load->model('assignPatToCG');			
		$userId = $this->assignPatToCG->getValidEmailCGIDForTicket($pId);	
		
					
		$userEmail = array();
		foreach($userId AS $record){
			$user = $this->ion_auth->user($record->CGID)->row();
			if($user->email != $this -> ion_auth -> user() -> row() -> email){
				$userEmail[] = $user->email;
			}
		}

		$emailList = implode(",",$userEmail);
		
		
		$this -> load -> library('email');
		$this -> email -> from('portal@seamlesscare.ca', 'SEAMLESS HOME PORTAL');
		$this -> email -> to('refill@seamlesscare.ca' . ', ' . $this -> ion_auth -> user() -> row() -> email);
		$this -> email -> subject('New ticket for patient id: ' . $this -> input -> post('patID'));
        $this -> email -> cc($emailList);
		$this -> email -> message("Patient ID:  " . $this -> input -> post('patID') . "/t/t" . date('l jS \of F Y h:i:s A') . "\r\n\r\nTicket Description:\r\n\r\n" . $this -> input -> post('Description'));
		
		$user_groups = $this->ion_auth->get_users_groups()->result();
		
		$gId ="";
		
		foreach($user_groups AS $record){
			$gId = $record->id;
		}
		
		if($gId == 18){
			$this -> email -> send();
		}		
		
		echo $this -> comp -> updateTicket($this -> input -> post('ticketID'), $this -> input -> post('patID'), $this -> input -> post('ticketTypeReferenceID'), $this -> input -> post('Description'), $this -> input -> post('active'));
	
	}

	public function saveRanksToTicket() {
		
		$id= $this->input->post('id');
		
		$data = array(
			'rank' => $this->input->post('rant'),
			'rankAddDate' => date('Y-m-d H:i:s')					
			
		);		
		
		$this->load->model('comp');
		$this->comp->updateRankTicket($id,$data);
		
	}
	
	public function deleteTicketConfirm() {
		
		$id= $this->input->post('id');	
		$this->load->model('comp');
		$this->comp->deleteTicket($id);
		
	}

	public function addComment() {
			
		$this->load->model('tickets');
		$tId = $this -> input -> post('ticketID');
		$info = $this->tickets->getAllDataBaseOnTableId($tId);
		
		if($info->active == 0){
			$data = array( 'active' => 1);
			$this->tickets->updateData($tId,$data);			
		}
		
		// need users attached to clients. ticket is 1-1 association with client.
		$bccmailString = '';
		// retrieve users email address attached to clients using ticket id
		$ticketId = $this -> input -> post('ticketID');
		$userEmails = $this -> comp -> getUsersEmailsByTicketID($ticketId);
		if ($userEmails) {
			$bccmailString = '';
			foreach ($userEmails->result() as $userEmail) {
				$bccmailString = $bccmailString . $userEmail -> email . ',';
			}
			$bccmailString = substr($bccmailString, 0, -1);
		}
		
		
		
		$this->load->model('assignPatToCG');
		$this->load->model('tickets');
		
		$ticketInfo = $this->tickets->getAllDataBaseOnTableId($ticketId);
		$userId = $this->assignPatToCG->getValidEmailCGIDForComment($ticketInfo->patID);	
		
					
		$userEmail = array();
		foreach($userId AS $record){
			$user = $this->ion_auth->user($record->CGID)->row();
			//if($user->email != $this -> ion_auth -> user() -> row() -> email){
				$userEmail[] = $user->email;
			//}	
		}

		$emailList = implode(",",$userEmail);
		
		// email confirmation
		
		$this -> load -> library('email');
		$this -> email -> from('portal@seamlesscare.ca', 'Seamless Care Portal');
		$this -> email -> to('refill@seamlesscare.ca');
		$this -> email -> cc($emailList);
		$this -> email -> subject('New comment was added to ticket#: ' . $this -> input -> post('ticketID'));
		$this -> email -> message('New Comment: ' . $this -> input -> post('message'));
		
		$user_groups = $this->ion_auth->get_users_groups()->result();
		
		$gId ="";
		
		foreach($user_groups AS $record){
			$gId = $record->id;
		}
		
		if($gId == 18){
			$this -> email -> send();
		}
		
		echo $this -> comp -> updateDiscussion($this -> input -> post('ticketID'), $this -> input -> post('message'));
		
		
	}

	public function createReOrder() {
		$rxs = $this -> input -> post('rxs');
		if(!empty($rxs)){
			
			$pId = $this -> input -> post('patID');
			$data = $this->comp->getEmailAddressFromPatId($pId);
			
			$listEmail = "";
			foreach($data AS $info){
				$listEmail[] = $info->email;
			}
			$listEmail = array_filter($listEmail);
			$listEmail = implode(",",$listEmail); 
			
			$this->load->model('assignPatToCG');			
			$userId = $this->assignPatToCG->getValidEmailCGIDForTicket($pId);	
			
						
			$userEmail = array();
			foreach($userId AS $record){
				$user = $this->ion_auth->user($record->CGID)->row();
				//if($user->email != $this -> ion_auth -> user() -> row() -> email){
					$userEmail[] = $user->email;
				//}				
			}
	
			$emailList = implode(",",$userEmail);
			
			
			echo $this -> comp -> updateTicket('-1', $this -> input -> post('patID'), '2', 'Please reorder the following RXs: ' . $this -> input -> post('rxs'), '1',$this -> input -> post('rxnumList'));
			$this -> load -> library('email');
			$this -> email -> from('portal@seamlesscare.ca', 'Seamless Care Portal');
			$this -> email -> to('refill@seamlesscare.ca');
			$this -> email -> cc($emailList);
			$this -> email -> subject('PRN Medication Reorder Created By: ' . $this -> ion_auth -> user() -> row() -> first_name . ' ' . $this -> ion_auth -> user() -> row() -> last_name);
			$this -> email -> message('PRN Medication Reorder Created By: ' . $this -> ion_auth -> user() -> row() -> first_name . ' ' . $this -> ion_auth -> user() -> row() -> last_name . "\r\n\r\n The following RXs have been reordered: " . $this -> input -> post('rxs') . "\r\n\r\nFor Patient ID: " . $this -> input -> post('patID'));
			
			$user_groups = $this->ion_auth->get_users_groups()->result();
		
			$gId ="";
			
			foreach($user_groups AS $record){
				$gId = $record->id;
			}
			
			if($gId == 18){
				$this -> email -> send();
			}
			
		}else{
			echo "Not sucessful";
		}
		
		//echo "Successfully Updated!";
	}

	// invoice
	public function viewInvoiceModal() {
		$this -> load -> view('compliance/viewInvoiceModal');
	}

	public function editIncidentModal() {
		$this -> load -> view('homeportal/editIncidentModal');
	}

	// MedChange
	public function editMedChangeModal() {
		$this -> load -> view('homeportal/editMedChangeModal');
	}

	public function printMedChange() {
		//$this -> load -> view('compliance/printMedChange');
		
		$this->load->model('Pat');
		
		$patID = $this->uri->segment ( 3 );
		$data['patient'] = $this -> Pat -> getPatInfoBaseOnLastIdRow($patID);
			
		$data['user'] = $this -> ion_auth -> user() -> row();
		$data['ticketTypeReference'] = $this->comp->getTicketTypeReference ();
		
		if ($patID > 0) {
		
			if($this->uri->segment ( 4 )  > 0){ $data['medChange'] = $this->comp->getMedChange ( $this->uri->segment ( 4 ) ); }
				
		} else {
				
			redirect('compliance/medchanges');
		}
		
		$this -> load -> view('homeportal/printMedChange',$data);
		
	}

	public function printPatProfile() {
		$patId = $this->uri->segment(3);		
		
		$data['patRx'] = $this->rest->get('production/exApi/rxPat/'.$patId);
		$data['patInfo'] = $this->rest->get('production/exApi/patInfoBaseOnPatID/'.$patId);
		$data['allergy'] = $this->rest->get('production/exApi/patAllergyBaseOnPataID/'.$patId);
		$data['condition'] = $this->rest->get('production/exApi/patConBaseOnPataID/'.$patId);
		
		$this -> load -> view('homeportal/reports/printDrReport',$data);
		
	}

	public function DrugInfoModal() {
		$this -> load -> view('compliance/DrugInfoModal');
	}

	public function pharmacyInfo() {
		$this -> load -> view('homeportal/pharmacyInfo');
	}
	
	public function getPatientResults() {
		$pats = $this -> pharm -> getAllPatients($this -> input -> post('fullName'));
		if ($pats) {
			foreach ($pats->result () as $pat) {
				echo "<p><a href='" . base_url("compliance/viewPat/" . $pat -> ID . "/profile") . "'>" . $pat -> FirstName . ' ' . $pat -> LastName . '</a></p>';
			}
		} else {
			echo 'No Data.';
		}
	}

	public function feedback() {
		$this -> load -> view('homeportal/feedback');
	}

	// languages
	public function english() {
		$data = $this -> session -> userdata('user');
		$data['language'] = "english";
		$this -> session -> set_userdata('user', $data);

		$this -> lang -> load('portal', $this -> session -> userdata['user']['language']);
		redirect('homeportal/index');
	}

	public function chinese() {
		$data = $this -> session -> userdata('user');
		$data['language'] = "chinese";
		$this -> session -> set_userdata('user', $data);

		$this -> lang -> load('portal', $this -> session -> userdata['user']['language']);
		redirect('homeportal/index');
	}

	public function simplifiedchinese() {
		$data = $this -> session -> userdata('user');
		$data['language'] = "simplifiedchinese";
		$this -> session -> set_userdata('user', $data);

		$this -> lang -> load('portal', $this -> session -> userdata['user']['language']);
		redirect('homeportal/index');
	}

	public function korean() {
		$data = $this -> session -> userdata('user');
		$data['language'] = "korean";
		$this -> session -> set_userdata('user', $data);

		$this -> lang -> load('portal', $this -> session -> userdata['user']['language']);
		redirect('homeportal/index');
	}

	// CALENDAR  -----------------------------------------------START HERE

	public function schedules() {
		$this -> load -> view('homeportal/header');
		$this -> load -> view('homeportal/calendar/home');
		$this -> load -> view('homeportal/footer');
	}

	public function eventUpdate() {
		$title = $_POST['title'];
		$calendarId = $_POST['calendarId'];
		$eventHome = $_POST['eventHome'];
		if ($eventHome == "") {
			$eventHome = 0;
		}
		$eventPat = $_POST['eventPat'];
		if ($eventPat == "") {
			$eventPat = 0;
		}
		$scheduleDate = $_POST['scheduleDate'];
		$scheduleTime = $_POST['scheduleTime'];
		$timeArray = explode(" ", $scheduleTime);
		if ($timeArray[1] == "PM") {
			$tArray = explode(":", $timeArray[0]);
			$hour = $tArray[0] + 12;
			$min = $tArray[1];
		} else {
			$tArray = explode(":", $timeArray[0]);
			$hour = $tArray[0];
			$min = $tArray[1];
		}

		if (count($hour) == 1) {
			$hour = "0" . $hour;
		}

		$newTime = $hour . ":" . $min . ":00";

		$dateTimeStamp = $scheduleDate . " " . $newTime;
		$description = $_POST['description'];
		$appointmentType = $_POST['appointmentType'];
		$userID = $this -> ion_auth -> user() -> row() -> id;
		$updResultMsg = $this -> portal -> UpdateCalendarEvent($calendarId, $dateTimeStamp, $description, $eventPat, $eventHome, $userID, $title, $appointmentType);
		$updResults = explode("|", $updResultMsg);
		$resultMsg = $updResults[1];
		if ($updResults[0] == 1) {
			$dataArray = array("msg" => "success", "updResult" => $resultMsg, "rtnId" => $calendarId, "rtnCalendarId" => $calendarId, "rtnDesc" => $description, "rtnPatID" => $eventPat, "rtnHomeID" => $eventHome, "rtnStart" => $dateTimeStamp, "rtnTitle" => $title, "rtnappointmentType" => $appointmentType);
		} else {
			$dataArray = array("msg" => "fail", "updResult" => $resultMsg);
		}

		echo json_encode($dataArray);
	}

	public function addNewEvent() {
		$newTitle = $_POST['newTitle'];
		$calendarId = 0;
		$homeId = $_POST['homeId'];
		$patId = $_POST['patId'];
		$newappointmentType = $_POST['newappointmentType'];
		$newScheduleDate = $_POST['newScheduleDate'];
		$newScheduleTime = $_POST['newScheduleTime'];
		$timeArray = explode(" ", $newScheduleTime);
		if ($timeArray[1] == "PM") {
			$tArray = explode(":", $timeArray[0]);
			$hour = $tArray[0];
			if ($hour < 12) {
				$hour = $tArray[0] + 12;
			} elseif ($hour == 12) {
				$hour = $tArray[0];
			}

			$min = $tArray[1];
		} else {
			$tArray = explode(":", $timeArray[0]);
			$hour = $tArray[0];
			if ($hour == 12) {
				$hour = "00";
			} else {
				if (strlen($hour) == 1) {
					$hour = "0" . $hour;
				}
			}
			$min = $tArray[1];
		}

		$newTime = $hour . ":" . $min . ":00";

		$newDateTimeStamp = $newScheduleDate . " " . $newTime;
		$newDescription = $_POST['newDescription'];
		$userID = $this -> ion_auth -> user() -> row() -> id;
		$calendarId = $this -> portal -> AddCalendarEvent($newDateTimeStamp, $newDescription, $patId, $newappointmentType, $homeId, $userID, $newTitle);
		if ($calendarId != 0) {
			$dataArray = array("msg" => "success", "rtnId" => $calendarId, "rtnCalendarId" => $calendarId, "rtnDesc" => $newDescription, "rtnappointmentType" => $newappointmentType, "rtnPatID" => $patId, "rtnHomeID" => $homeId, "rtnStart" => $newDateTimeStamp, "rtnTitle" => $newTitle, );
		} else {
			$dataArray = array("msg" => "fail");
		}

		echo json_encode($dataArray);
	}

	public function calendarProcess() {

		$startDate = date("Y-m") . "-01";
		$EndDate = date("Y-m") . "-" . date("t");
		$eventData = $this -> portal -> getUserCalendarEvents($startDate, $EndDate) -> result();
		$dataRows = array();
		if (count($eventData) > 0) {
			foreach ($eventData as $e) {
				$calendarId = $e -> id;
				$title = $e -> title;
				$dateTimeStamp = $e -> dateTimeStamp;
				$desc = $e -> desc;
				$patID = $e -> patID;
				$homeID = $e -> homeID;

				$row = array();
				$row['id'] = $calendarId;
				$row['calendarId'] = $calendarId;
				$row['title'] = $title;
				if ($patID != 0) {
					$row['patID'] = $patID;

				} elseif ($homeID != 0) {
					$row['homeID'] = $homeID;
				}

				$row['desc'] = $desc;
				$row['start'] = $dateTimeStamp;
				array_push($dataRows, $row);
			}//end of foreach
		}

		$dataArray = array("msg" => "success", "content" => $dataRows);

		echo json_encode($dataArray);
	}

	// -----------------------------------------------END  HERE

	// HEALTH INDICATORS
	public function healthindicatorsdash() {
		$this -> load -> view('homeportal/header');
		$this -> load -> view('homeportal/healthindicators/healthindicatordash');
		$this -> load -> view('homeportal/footer');
	}

	public function getPatCompPack() {

		echo '<thead><tr><th>Medication</th><th>Doctor</th><th>Directions</th></tr></thead><tbody>';

		$compPack = $this -> pharm -> getPatCompliancePackage($this -> input -> post('patID'));
		if ($compPack != FALSE) {
			foreach ($compPack->result () as $drg) {
				echo '<tr>';
				echo '<td>' . $drg -> BrandName . ' ' . $drg -> Strength . '<br>RX-' . $drg -> RxNum . '</td>';
				echo '<td>' . $drg -> doc . '</td>';
				echo '<td>' . $drg -> SIG . '</td>';
				echo '</tr>';
			}
		} else {
			echo '<tr><td>No Medication Recorded</td></tr>';
		}

		echo '</tbody>';
	}

	public function getPatPRN() {

		echo '<thead><tr><th>Medication</th><th>Doctor</th><th>Directions</th><th>Comment</th><th>Select</th></tr></thead><tbody>';

		$patPRNs = $this -> pharm -> getPatPRNs($this -> input -> post('patID'));
		if ($patPRNs != FALSE) {
			$checkBoxCount = 1;
			foreach ($patPRNs->result () as $patPRN) {
				$mix='';
				if($patPRN->MixID>0){$mix='class="warning"';}
				echo '<tr id="reOrderList" ' . $mix .'>';
				echo '<td>' . $patPRN -> BrandName . ' ' . $patPRN -> Strength . '<br>RX-' . $patPRN -> RxNum . '</td>';
				echo '<td>' . $patPRN -> doc . '</td>';
				echo '<td>' . $patPRN -> SIG . '</td>';
				echo '<td><input type="text" name="reOrderComment" class="form-control reOrderCommentClass" id="comment' . $checkBoxCount . '" ></td>';
				echo '<td><input type="checkbox" name="reOrderCheckBox" class="form-control reOrderCheckBoxClass" value="' . $patPRN -> RxNum . '" id="checkBox' . $checkBoxCount++ . '"></td>';
				echo '</tr>';
			}
		} else {
			echo '<tr><td>No Medication Recorded..</td></tr>';
		}

		echo '</tbody>';

	}

	//eMAR-------------------------------------

	// HEALTH INDICATORS
	public function emars() {
		$this -> load -> view('homeportal/header');
		$this -> load -> view('homeportal/emars');
		$this -> load -> view('homeportal/footer');
	}

	//Pat Profile Alergies and Conditions
	public function getAlergiesTable() {
		echo '<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover"><thead><tr><th>Allergy</th><th>Comment</th><th>Severity</th></tr></thead><tbody>';
		$alergies = $this -> pharm -> getPatAlergyCodes($this -> input -> post('patID'));
		if ($alergies != FALSE) {
			foreach ($alergies->result() as $alergy) {
				$alergyDecoded = $this -> pharm -> getAlergyDecoded($alergy -> Code);
				if ($alergyDecoded != FALSE) {
					echo '<tr><td>' . $alergyDecoded -> AllergenDesc . '</td>' . $alergy -> Comment . '<td></td>' . $alergy -> Severity . '<td></td></tr>';
				}
			}
		} else {
			echo '<tr><td colspan="3">No Allergies Recorded..</td></tr>';
		}
		echo '</tbody></table>';   
	}

	//Pat Profile Alergies and Conditions
	public function getConditionsTable() {
		echo '<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover"><thead><tr><th>Condition</th></tr></thead><tbody>';
		$conditions = $this -> pharm -> getPatCondCodes($this -> input -> post('patID'));
		if ($conditions != FALSE) {
			foreach ($conditions->result() as $cond) {
				$condDecoded = $this -> pharm -> getCondDecoded($cond -> Code);
				if ($condDecoded != FALSE) {
					echo '<tr><td>' . $condDecoded -> Description . '</td></tr>';
				}
			}
		} else {
			echo '<tr><td>No Conditions Recorded..</td></tr></tbody></table>';
		}
		echo '</tbody></table>';
	}

}
?>