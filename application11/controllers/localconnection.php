
<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

include_once ('secure_controller.php');

class localconnection extends Secure_Controller {
	
	
	function __construct() {
		
		// Call the Model constructor
		parent::__construct();
		
		if ($this -> quick -> accessTo('localconnection')) {
			// do nothing they are legit
		} else {
			// current user should not have access to this controller.
			redirect('news/mynews');
		}

		if (!isset($this -> session -> userdata['user']['language'])) {
			$data = array('language' => 'english');
			$this -> session -> set_userdata('user', $data);
		}

		$this -> lang -> load('portal', $this -> session -> userdata['user']['language']);
		
		$imagetypes = array("image/jpeg", "image/gif", "image/jpg", "image/png");		
		
	}
	
	
	
	function rest_client_example() {
		
	   $this->load->library('rest', array(
	        'server' => 'http://192.168.30.3:8080/production/index.php/Api/',
	        'http_user' => 'Rajah',
	        'http_pass' => 'Hajar',
	        'http_auth' => 'basic' // or 'digest'
	    ));
	     
		 
		
		 $id = 55601;
		 
	    echo $user = $this->rest->get('user', array('id' => $id), 'json');
	     
	    echo $user->name;
		
	}
	
	
}

?>