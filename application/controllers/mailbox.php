<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mailbox extends CI_Controller {
	
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
		$this->login = 'mailtest@iapotheca.com';
        $this->pass = 'nofun4u';
        $this->host = 'iapotheca.com';
        $this->port = '143';
        $this->service_flags = '/imap/notls';
	}

    public function index() {
        $mbox = 'INBOX';
        
        // this shows basic IMAP, no TLS required
        $config['login'] = $this->login;
        $config['pass'] = $this->pass;
        $config['host'] = $this->host;
        $config['port'] = $this->port;
        $config['service_flags'] = $this->service_flags;
        
        $config['mailbox'] = $mbox;
        
        $this->load->library('peeker', $config);     
        $this->load->model('mailbox_model');
        
        $mboxes = $this->peeker->get_mailboxes();
        if(sizeof($mboxes) > 1) {
            $tmp = array_pop($mboxes);
            sort($mboxes);
            array_unshift($mboxes, $tmp);
        }
        
        // Gets messages headers for all messages in INBOX
        if($this->peeker->message_waiting()) {
            $ctr = $this->peeker->get_message_count();
            if($ctr > 1) {
                $msgs = $this->peeker->message(1,$ctr);
            } else {
                $msgs = array($this->peeker->message(1));
            }
        } else {
            $msgs = array();
        }

        $data['mbox'] = $mbox;
        $data['msgs'] = $msgs;
        $data['mboxes'] = $mboxes;

		$this->load->view('header');
		$this->load->view('mailbox/index', $data);
		$this->load->view('footer');
    }
    
    public function showInbox() {
	    $mbox = $this->input->post('mbox');
	    $mbox = (empty($mbox) ? 'INBOX' : $mbox);

        // this shows basic IMAP, no TLS required
        $config['login'] = $this->login;
        $config['pass'] = $this->pass;
        $config['host'] = $this->host;
        $config['port'] = $this->port;
        $config['service_flags'] = $this->service_flags;
        
        $config['mailbox'] = $mbox;
        
        $this->load->library('peeker', $config);
        $this->load->model('mailbox_model');

        $mboxes = $this->peeker->get_mailboxes();
        if(sizeof($mboxes) > 1) {
            $tmp = array_pop($mboxes);
            sort($mboxes);
            array_unshift($mboxes, $tmp);
        }
        
        // Gets messages headers for all messages in INBOX
        if($this->peeker->message_waiting()) {
            $ctr = $this->peeker->get_message_count();
            if($ctr > 1) {
                $msgs = $this->peeker->message(1,$ctr);
            } else {
                $msgs = array($this->peeker->message(1));
            }
        } else {
            $msgs = array();
        }

        $data['mboxes'] = $mboxes;
        $data['msgs'] = $msgs;
	    $data['mbox'] = $mbox;
    	$this->load->view('mailbox/inbox_main', $data);
    }
    
	public function showSingleEmail() {
        $msgNo = $this->input->post('msgNo');
        $mbox = $this->input->post('mbox');

        // this shows basic IMAP, no TLS required
        $config['login'] = $this->login;
        $config['pass'] = $this->pass;
        $config['host'] = $this->host;
        $config['port'] = $this->port;
        $config['service_flags'] = $this->service_flags;
        
        $config['mailbox'] = $mbox;
        
        $this->load->library('peeker', $config);
        $this->load->model('mailbox_model');

        $id = $msgNo;
        $msg = $this->peeker->get_message($id);
        
        $html = $msg->get_html();
        $plain = $msg->get_plain();
        
        if(strlen($html) > 0) {
            $body = $html;
        } else {
            $body = $plain;
        }

        $data['msg'] = $msg;
        $data['body'] = $body;
	    $data['mbox'] = $mbox;
	    $data['msgNo'] = $msgNo;
    	$this->load->view('mailbox/inbox_email',$data);
	}

	public function showEmailReply() {
    	$this->load->view('mailbox/inbox_email_reply');
	}

	public function showCompose() {
	    $mbox = $this->input->post('mbox');
	    $mbox = (empty($mbox) ? 'INBOX' : $mbox);

        // this shows basic IMAP, no TLS required
        $config['login'] = $this->login;
        $config['pass'] = $this->pass;
        $config['host'] = $this->host;
        $config['port'] = $this->port;
        $config['service_flags'] = $this->service_flags;
        
        $config['mailbox'] = $mbox;
        
        $this->load->library('peeker', $config);
        $this->load->model('mailbox_model');

	    $data['mbox'] = $mbox;
    	$this->load->view('mailbox/inbox_compose',$data);
	}

	public function markSeen() {
	    $selected = $this->input->post('selected');
	    $mbox = $this->input->post('mbox');

        // this shows basic IMAP, no TLS required
        $config['login'] = $this->login;
        $config['pass'] = $this->pass;
        $config['host'] = $this->host;
        $config['port'] = $this->port;
        $config['service_flags'] = $this->service_flags;
        
        $config['mailbox'] = $mbox;
        
        $this->load->library('peeker', $config);
        $this->load->model('mailbox_model');

        $msg_array = explode(',',$selected);
        foreach($msg_array as $msgId) {
            $this->peeker->flag_seen_id($msgId);
        }
	}

	public function markUnseen() {
	    $selected = $this->input->post('selected');
	    $mbox = $this->input->post('mbox');

        // this shows basic IMAP, no TLS required
        $config['login'] = $this->login;
        $config['pass'] = $this->pass;
        $config['host'] = $this->host;
        $config['port'] = $this->port;
        $config['service_flags'] = $this->service_flags;
        
        $config['mailbox'] = $mbox;

        $this->load->library('peeker', $config);
        $this->load->model('mailbox_model');

        $msg_array = explode(',',$selected);
        foreach($msg_array as $msgId) {
            $this->peeker->flag_unseen_id($msgId);
        }
	}

	public function deleteMail() {
	    $selected = $this->input->post('selected');
	    $mbox = $this->input->post('mbox');

        // this shows basic IMAP, no TLS required
        $config['login'] = $this->login;
        $config['pass'] = $this->pass;
        $config['host'] = $this->host;
        $config['port'] = $this->port;
        $config['service_flags'] = $this->service_flags;
        
        $config['mailbox'] = $mbox;

        $this->load->library('peeker', $config);
        $this->load->model('mailbox_model');

        $msg_array = explode(',',$selected);
        foreach($msg_array as $msgId) {
            $this->peeker->delete_and_expunge($msgId);
        }
	}

	public function moveMail() {
	    $selected = $this->input->post('selected');
	    $mbox = $this->input->post('mbox');
	    $moveTo = $this->input->post('moveTo');

        // this shows basic IMAP, no TLS required
        $config['login'] = $this->login;
        $config['pass'] = $this->pass;
        $config['host'] = $this->host;
        $config['port'] = $this->port;
        $config['service_flags'] = $this->service_flags;
        
        $config['mailbox'] = $mbox;

        $this->load->library('peeker', $config);
        $this->load->model('mailbox_model');

        $msg_array = explode(',',$selected);
        foreach($msg_array as $msgId) {
            $this->peeker->move_mail($msgId, $moveTo);
            $this->peeker->delete_and_expunge($msgId);
        }
	}
}
