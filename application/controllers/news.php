<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once('secure_controller.php');
class news extends Secure_Controller {
	
	
	function __construct() {
		// Call the Model constructor
		parent::__construct ();
		if ($this->quick->accessTo('index.php/news')){
			//do nothing they are legit
		}
		elseif ($this->quick->accessTo('homeportal'))
		{
			redirect(base_url() . 'index.php/homeportal');
		}
		else
		{
			//current user should not have access to this controller.
			redirect('index.html');
		}

	}
	
	public function myNews()
	{
		$this->load->view('header');
		$this->load->view('main/dash');
		$this->load->view('footer');
	}
	
	public function index()
	{
		$this->load->view('header');
		$this->load->view('main/dash');
		$this->load->view('footer');
	}	
}
