<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
include_once ('secure_controller.php');
class filingCab extends Secure_Controller {
	
	
	function __construct()
	{
		parent::__construct();
		if ($this->quick->accessTo('filingcab')){
			//do nothing they are legit
		}
		else 
		{
			//current user should not have access to this controller.
			redirect('news/mynews');
		}
	}
	
	// KPIs
	public function KPIDash() {
		$this->load->view ( 'header' );
		$this->load->view ( 'KPI/KPIDash' );
		$this->load->view ( 'footer' );
	}
	public function KPIDetail() {
		$this->load->view ( 'header' );
		$this->load->view ( 'KPI/KPIDetail' );
		$this->load->view ( 'footer' );
	}
	public function newKPIModal() {
		$this->load->view ( 'KPI/newKPIModal' );
	}
	public function addNewKPIPoint() {
		$this->load->view ( 'KPI/addNewKPIPoint' );
	}
	public function deleteKPIPoint() {
		echo $this->kpi->updateKPIDetail ( $this->input->post ( 'KPIDetailID' ) );
	}
	public function updateKPI() {
		echo $this->kpi->updateKPI ( $this->input->post ( 'KPIID' ), $this->input->post ( 'indicatorName' ), $this->input->post ( 'indicatorDescription' ), $this->input->post ( 'minValue' ), $this->input->post ( 'maxValue' ), $this->input->post ( 'targetValue' ), $this->input->post ( 'active' ) );
	}
	public function deleteKPI() {
		echo $this->kpi->updateKPI ( $this->input->post ( 'KPIID' ), $this->input->post ( 'active' ) );
	}
	public function updateKPIPoint() {
		echo $this->kpi->updateKPIDetail ( $this->input->post ( 'KPIDetailID' ), $this->input->post ( 'KPIID' ), $this->input->post ( 'pointValue' ), $this->input->post ( 'active' ), $this->input->post ( 'recordedDate' ) );
	}
	// Inventory Audit
	public function inventoryDash() {
		$this->load->view ( 'header' );
		$this->load->view ( 'inventory/inventoryDash' );
		$this->load->view ( 'footer' );
	}
	public function inventoryDetail() { // needs to create new inventory Audit on no URI segment 3
		$this->load->view ( 'header' );
		$this->load->view ( 'inventory/inventoryDetail' );
		$this->load->view ( 'footer' );
	}
	
	// Committees
	public function committeeDash() {
		$this->load->view ( 'header' );
		$this->load->view ( 'committee/committeeDash' );
		$this->load->view ( 'footer' );
	}
	public function editCommitteeModal() {
		$this->load->view ( 'committee/editCommitteeModal' );
	}
	public function viewCommittee() {
		$this->load->view ( 'header' );
		$this->load->view ( 'committee/viewCommittee' );
		$this->load->view ( 'footer' );
	}
	public function editMinutesModal() {
		$this->load->view ( 'committee/editMinutesModal' );
	}
	public function viewMinutesModal() {
		$this->load->view ( 'committee/viewMinutesModal' );
	}
	public function updateCommittee() {
		echo $this->committee->updateCommmittee ( $this->input->post ( 'committeeID' ), $this->input->post ( 'committeeName' ), $this->input->post ( 'committeeDescription' ), $this->input->post ( 'active' ) );
	}
	public function updateMinutes() {
		echo $this->committee->updateMeetingMinutes ( $this->input->post ( 'committeeMinutesID' ), $this->input->post ( 'dateCreated' ), $this->input->post ( 'title' ), $this->input->post ( 'committeeID' ), $this->input->post ( 'present' ), $this->input->post ( 'discussion' ), $this->input->post ( 'active' ) );
	}
	
	// Policies
	public function policiesDash() {
		$this->load->view ( 'header' );
		$this->load->view ( 'policies/policiesDash' );
		$this->load->view ( 'footer' );
	}
	
	// PolicyArea
	public function editPolicyAreaModal() {
		$this->load->view ( 'policies/modifyPolicyAreaModal' );
	}
	public function editPolicyArea() {
		echo $this->policy->modifyPolicyArea ( $this->input->post ( 'policyAreaID' ), $this->input->post ( 'policyAreaName' ), '1' );
	}
	public function newPolicyAreaModal() {
		$this->load->view ( 'policies/newPolicyAreaModal' );
	}
	public function newPolicyArea() {
		echo $this->policy->modifyPolicyArea ( '-1', $this->input->post ( 'policyAreaName' ), '1' );
	}
	
	
	// Policy
	public function editPolicyModal() {
		$this->load->view ( 'policies/modifyPolicyModal' );
	}
	public function editPolicy() {
		echo $this->policy->modifyPolicy ( $this->input->post ( 'policyID' ), $this->input->post ( 'policyAreaID' ), $this->input->post ( 'policyTitle' ), '-1', $this->input->post ( 'revisionDate' ), $this->input->post ( 'approvedOn' ), '-1', $this->input->post ( 'purpose' ), $this->input->post ( 'policy' ), $this->input->post ( 'procedure' ) );
	}
	public function newPolicyModal() {
		$this->load->view ( 'policies/modifyPolicyModal' );
	}
	public function newPolicy() {
		echo $this->policy->modifyPolicy ( '0', $this->input->post ( 'policyAreaID' ), $this->input->post ( 'policyTitle' ), '-1', $this->input->post ( 'revisionDate' ), $this->input->post ( 'approvedOn' ), '-1', $this->input->post ( 'purpose' ), $this->input->post ( 'policy' ), $this->input->post ( 'procedure' ) );
	}
	
	// Incidents
	public function incidentsDash() {
		$this->load->view ( 'header' );
		$this->load->view ( 'incidents/incidentsDash' );
		$this->load->view ( 'footer' );
	}
	
	public function editIncidentModal() {
		$this->load->view ( 'incidents/editIncidentModal' );
	}
	public function editIncident()
	{
		echo $this->incidents->modifyIncident($this->input->post('incidentID'),$this->input->post('incidentDatetime'),$this->input->post('description'),$this->input->post('actionTaken'),'',$this->input->post('reportedTo'),'', $this->input->post('incidentType'));	
	}
	public function editIncidentTypesModal()
	{
		$this->load->view ( 'incidents/editIncidentTypesModal' );
	}
	public function editIncidentType()
	{
		
	}
	
	//compliance pat
	public function compliancepatients() {
		$this->load->view ( 'header' );
		$this->load->view ( 'compliance/compliancepatients' );
		$this->load->view ( 'footer' );
	}
}