<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

include_once ('secure_controller.php');
class homeportal extends Secure_Controller {
	function __construct() {
		// Call the Model constructor
		parent::__construct ();
		if ($this->quick->accessTo ( 'homeportal' )) {
			// do nothing they are legit
		} else {
			// current user should not have access to this controller.
			redirect ( 'news/mynews' );
		}
		
		if (! isset ( $this->session->userdata ['user'] ['language'] )) {
			$data = array (
					'language' => 'english'
			);
			$this->session->set_userdata ( 'user', $data );
		}
		
		$this->lang->load ( 'portal', $this->session->userdata ['user'] ['language'] );
		
	}
	public function index() {
		if (! $this->ion_auth->logged_in ()) {
			redirect ( base_url () . 'auth/login' );
		}
		
		
		$this->load->view ( 'homeportal/header' );
		$this->load->view ( 'homeportal/index' );
		$this->load->view ( 'homeportal/footer' );
	}
	public function patients() {
		$this->load->view ( 'homeportal/header' );
		$this->load->view ( 'homeportal/patients' );
		$this->load->view ( 'homeportal/footer' );
	}
	public function viewPat() {
		$check = $this->pharm->userPatientAccess ( $this->uri->segment ( 3 ) );
		
		if ($check == FALSE) {
			redirect ( base_url () );
		} else {
			$this->load->view ( 'homeportal/header' );
			$this->load->view ( 'homeportal/viewPatient' );
			$this->load->view ( 'homeportal/footer' );
		}
	}
	public function viewPatTest() {
		$check = $this->pharm->userPatientAccess ( $this->uri->segment ( 3 ) );
		
		if ($check == FALSE) {
			redirect ( base_url () );
		} else {
			$this->load->view ( 'homeportal/header' );
			$this->load->view ( 'homeportal/Copy of viewPatient' );
			$this->load->view ( 'homeportal/footer' );
		}
	}
	
	// events
	public function userEvents() {
		$this->load->view ( 'homeportal/header' );
		$this->load->view ( 'homeportal/userEvents' );
		$this->load->view ( 'homeportal/footer' );
	}
	public function clientEvents() {
		$check = $this->pharm->userPatientAccess ( $this->uri->segment ( 3 ) );
		
		if ($check == FALSE) {
			redirect ( base_url () );
		} else {
			$this->load->view ( 'homeportal/header' );
			$this->load->view ( 'homeportal/clientEvents' );
			$this->load->view ( 'homeportal/footer' );
		}
	}
	public function saveEvent() {
		echo $this->portal->updateEvent ( $this->input->post ( 'eventID' ), $this->input->post ( 'eventTypeID' ), $this->input->post ( 'eventDate' ), $this->input->post ( 'reportedByUser' ), $this->input->post ( 'desc' ), $this->input->post ( 'patID' ) );
	}
	public function savePRNMonitor() {
		echo $this->portal->updatePRNMonitor ( $this->input->post ( 'eventID' ), $this->input->post ( 'prn1desc' ), $this->input->post ( 'prn2given' ), $this->input->post ( 'prn2desc' ), $this->input->post ( 'protocol' ), $this->input->post ( 'timeEffective' ), $this->input->post ( 'staffInitial' ) );
	}
	public function eventModal() {
		$this->load->view ( 'homeportal/eventModal' );
	}
	public function prnAdminModal() {
		$this->load->view ( 'homeportal/prnAdminModal' );
	}
	
	// incidents
	public function incidents() {
		$this->load->view ( 'homeportal/header' );
		$this->load->view ( 'homeportal/incidentsDash' );
		$this->load->view ( 'homeportal/footer' );
	}
	public function viewincident() {
		$this->load->view ( 'homeportal/header' );
		$this->load->view ( 'homeportal/incidents/viewIncident' );
		$this->load->view ( 'homeportal/footer' );
	}
	public function users() {
		$this->load->view ( 'homeportal/header' );
		$this->load->view ( 'homeportal/users' );
		$this->load->view ( 'homeportal/footer' );
	}
	
	// TICKETS
	public function tickets() {
		$this->load->view ( 'homeportal/header' );
		$this->load->view ( 'homeportal/tickets' );
		$this->load->view ( 'homeportal/footer' );
	}
	public function viewTicket() {
		$this->load->view ( 'homeportal/header' );
		$this->load->view ( 'homeportal/viewTicket' );
		$this->load->view ( 'homeportal/footer' );
	}
	public function editTicketModal() {
		$this->load->view ( 'homeportal/editTicketModal' );
	}
	public function saveTicket() {
		// email confirmation
		$this->load->library ( 'email' );
		$this->email->from ( 'portal@seamlesscare.ca', 'SEAMLESS HOME PORTAL' );
		$this->email->to ( 'refill@seamlesscare.ca' . ', ' . $this->ion_auth->user ()->row ()->email );
		$this->email->subject ( 'New ticket for patient id: ' . $this->input->post ( 'patID' ) );
		
		$this->email->message ("Patient ID:  " . $this->input->post ( 'patID' )  . "/t/t" . date('l jS \of F Y h:i:s A') .  "\r\n\r\nTicket Description:\r\n\r\n" . $this->input->post ( 'Description' ) );
		$this->email->send ();
		echo $this->comp->updateTicket ( $this->input->post ( 'ticketID' ), $this->input->post ( 'patID' ), $this->input->post ( 'ticketTypeReferenceID' ), $this->input->post ( 'Description' ), $this->input->post ( 'active' ) );
	}
	public function addComment() {
		// email confirmation
		$this->load->library ( 'email' );
		$this->email->from ( 'portal@seamlesscare.ca', 'Seamless Care Portal' );
		$this->email->to ( 'refill@seamlesscare.ca' . ', ' . $this->ion_auth->user ()->row ()->email );
		$this->email->subject ( 'New comment was added to ticket#: ' . $this->input->post ( 'ticketID' ) );
		$this->email->message ( 'New Comment: ' . $this->input->post ( 'message' ) );
		$this->email->send ();
		echo $this->comp->updateDiscussion ( $this->input->post ( 'ticketID' ), $this->input->post ( 'message' ) );
	}
	public function createReOrder() {
		echo $this->comp->updateTicket ( '-1', $this->input->post ( 'patID' ), '2', 'Please reorder the following RXs: ' . $this->input->post ( 'rxs' ), '1' );
		$this->load->library ( 'email' );
		$this->email->from ( 'portal@seamlesscare.ca', 'Seamless Care Portal' );
		$this->email->to ( 'refill@seamlesscare.ca' . ', ' . $this->ion_auth->user ()->row ()->email );
		$this->email->subject ( 'PRN Medication Reorder Created By: ' . $this->ion_auth->user ()->row ()->first_name . ' ' . $this->ion_auth->user ()->row ()->last_name );
		$this->email->message ( 'PRN Medication Reorder Created By: ' . $this->ion_auth->user ()->row ()->first_name . ' ' . $this->ion_auth->user ()->row ()->last_name . "\r\n\r\n The following RXs have been reordered: " . $this->input->post ( 'rxs' ) . "\r\n\r\nFor Patient ID: " . $this->input->post ( 'patID' ));
		$this->email->send ();
	}
	
	// invoice
	public function viewInvoiceModal() {
		$this->load->view ( 'compliance/viewInvoiceModal' );
	}
	public function editIncidentModal() {
		$this->load->view ( 'homeportal/editIncidentModal' );
	}
	
	// MedChange
	public function editMedChangeModal() {
		$this->load->view ( 'homeportal/editMedChangeModal' );
	}
	public function printMedChange() {
		$this->load->view ( 'compliance/printMedChange' );
	}
	public function printPatProfile() {
		$this->load->view ( 'homeportal/reports/printDrReport' );
	}
	public function DrugInfoModal() {
		$this->load->view ( 'compliance/DrugInfoModal' );
	}
	public function pharmacyInfo() {
		$this->load->view ( 'homeportal/pharmacyInfo' );
	}
	public function feedback() {
		$this->load->view ( 'homeportal/feedback' );
	}
	// languages

	public function english() {
		$data = $this->session->userdata ( 'user' );
		$data ['language'] = "english";
		$this->session->set_userdata ( 'user', $data );
		
		$this->lang->load ( 'portal', $this->session->userdata ['user'] ['language'] );
		redirect ( 'homeportal/index' );
	}
	public function chinese() {
		$data = $this->session->userdata ( 'user' );
		$data ['language'] = "chinese";
		$this->session->set_userdata ( 'user', $data );
	
		$this->lang->load ( 'portal', $this->session->userdata ['user'] ['language'] );
		redirect ( 'homeportal/index' );
	}
	public function simplifiedchinese() {
		$data = $this->session->userdata ( 'user' );
		$data ['language'] = "simplifiedchinese";
		$this->session->set_userdata ( 'user', $data );
	
		$this->lang->load ( 'portal', $this->session->userdata ['user'] ['language'] );
		redirect ( 'homeportal/index' );
	}
	public function korean() {
		$data = $this->session->userdata ( 'user' );
		$data ['language'] = "korean";
		$this->session->set_userdata ( 'user', $data );
	
		$this->lang->load ( 'portal', $this->session->userdata ['user'] ['language'] );
		redirect ( 'homeportal/index' );
	}
}
?>