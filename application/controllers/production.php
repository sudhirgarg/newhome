<?php

 if (!defined('BASEPATH')) {  exit('No direct script access allowed'); }

include_once ('secure_controller.php');
 
class Production extends Secure_Controller { 
               
                
    function __construct() {   
        
        
        
        parent::__construct();
		
		if ($this -> quick -> accessTo('production')) {
			// do nothing they are legit
		} else {
			// current user should not have access to this controller.
			redirect('news/mynews');
		}       
           
    }
	
	
	function getBatch(){
		
		$batches = $this->rest->get('production/exApi/BatchForPrint');	
		
		$this->load->model('production/packagerBatch','packagerBatchName');
		
		
		foreach($batches AS $record){
			$infoApi = array('batchNum' => $record->BatchNum );			
			$infoApi = json_encode($infoApi);			
			$this->rest->post('production/exApi/AddBatchNumber/',$infoApi);
					
			
			$info = array(
				'pId'         => $record->Id,
				'batchNum'    => $record->BatchNum,
				'description' => $record->Description,
				'instructions'=> $record->Instructions,
				'packagerId'  => $record->PackagerId,
				'cfOrderId'   => $record->CFOrderId,
				'storeId'     => $record->StoreId,
				'useRxDoc'    => $record->UseRxDoctor,
				'startDate'   => $record->StartDate,
				'endDate'     => $record->EndDate,
				'dateCreated' => $record->DateCreated
			);
			
			$d = $this->packagerBatchName->findBatchNum($record->BatchNum);
			if(empty($d)){ $this->packagerBatchName->addData($info); }
		}
		
		$data['batches'] = $this->packagerBatchName->getAllData();
		
		$this->load->view('header');
		$this->load->view('production/viewBatch',$data);
		$this->load->view('production/layout/productionFooter');
		
		//$this->rest->debug();
		
	}
    
	function deleteBatch(){
		$in         = json_decode(file_get_contents("php://input"));
		$batchNum   = $in->batchNum;
		
		$data = array(
			'removed' => 0	
		);
		
		$this->load->model('production/packagerBatch','packagerBatchName');
		$d = $this->packagerBatchName->updateDataBybatch($batchNum,$data);	
		
		echo ($d > 0) ? 'Removed' : 'Something went wrong, refresh the page and try again';
	}
	
    function getPatName($bId){
    	$batches = $this->rest->get('production/exApi/getPatName/'.$bId);
    	print_r($batches);    	
		
    }
	
	function getPatWard($patId){
		
		$this->load->model('Pat');
		$patInfo =$this->Pat->getAllDataBaseOnIdRow($patId);
		
		if($patInfo->NHID != 59){
			$wardInfo = $this->Pat->getWardName($patInfo->NHWardID);
			if(!empty($wardInfo)){ echo $wardInfo->LastName.' '.$wardInfo->FirstName; }
		}else{
			 echo $patInfo->LastName.' '.$patInfo->FirstName;
		}	
	}
	
	
	function getPatInfoBaseOnWardIdAndBatch($wardId, $patList){
		
		$this->load->model('Pat');
		$patInfo =$this->Pat->getAllDataBaseOnIdRow($wardId);
		
		$patArray = explode(',',$patList);
		
		if($patInfo->NHID != 59){
						
			 $patRecord = $this->Pat->getAllPatInfoBaseOnNHwardId($patInfo->NHWardID);
			
			 $conFirmPat = array();			
			 foreach($patRecord AS $info){
			 	 if(in_array($info->ID,$patArray)){
			 	 	  $conFirmPat[] = $info->ID;
			 	 }				 
			 }
			 
			 echo $conFirmPatList = implode(',',$conFirmPat);
			
		}else{
			 echo $patInfo->ID;
		}
	}
	

    function batchDetails(){
    	$batchNum = $this->uri->segment(3);
    	$data['batches'] = $this->rest->get('production/exApi/BatchDetails/'.$batchNum);    	    	
    	
		
		$this->load->model('Pat');
		$patId = array();	
		$ward  = array();
		
		foreach($data['batches'] AS $record){
			$patId[] = $record->ID;
			$patInfo =$this->Pat->getAllDataBaseOnIdRow($record->ID);
		
			if($patInfo->NHID != 59){
				$wardInfo = $this->Pat->getWardName($patInfo->NHWardID);
				if(!empty($wardInfo)){ $ward[] = $wardInfo->ID; } else { $ward[] = $patInfo->ID;  }
			}else{
				 $ward[] = $patInfo->ID;
			}
				
		}
				
		$listWard               = implode(',',array_unique($ward));
		$data['WardInfomation'] = $this->Pat->getInfoBaseOnListOfPatID($listWard);
		$data['patInfo']           = $this -> Pat     -> getAllActivePatInfo();
		$data['listPatId'] = implode(',',array_unique($patId));
		$data['batchNum']  = $batchNum;
		
		$this->load->view('header');
		$this->load->view('production/batchDetails',$data);
		$this->load->view('production/layout/productionFooter');
			
		
		//$this->rest->debug();
		
    }
    
    function generateOneSheet(){
    	$batchNum = $this -> input -> post('batchNum');
    	$orderNum = $batchNum.'-0';
    	$homePatId = $this -> input -> post('patId');
    	
    	$this -> load -> model('Pat');
    	
    	if($homePatId > 0){
    		$data['batches'] = $this->rest->get('production/exApi/BatchDetails/'.$batchNum);
    		
    		foreach($data['batches'] AS $record){
    			$patId[] = $record->ID;	
    		}
    		$listPatId = implode(',',array_unique($patId));
    		$patInfo = $this -> Pat ->getAllDataBaseOnIdRow($homePatId);
    		
    		$this->load->model('production/commonModels', 'commonModels');
    		 
    		$user =  $this->ion_auth->user()->row();
    		
    		$deliveryType = ($patInfo->DeliveryRouteType == 1) ? "N" : "Y";
    			
    		$route = $patInfo->DeliveryRoute;
    		$day = "";
    		$dayTime ="";
    			
    		if(!empty($route)){
    			$daySub = strtoupper(substr($patInfo->DeliveryRoute,0,3));
    			if($daySub == 'MON')    { $day = 'Monday'; }
    			elseif($daySub == 'TUE'){ $day = 'Tuesday'; }
    			elseif($daySub == 'WED'){ $day = 'Wednesday'; }
    			elseif($daySub == 'THU'){ $day = 'Thursday'; }
    			elseif($daySub == 'FRI'){ $day = 'Friday'; }
    			elseif($daySub == 'SAT'){ $day = 'Saturday'; }
    			elseif($daySub == 'SUN'){ $day = 'Sunday'; }
    			else { $day = date('l'); }
    		
    		}else{
    			$day = date('l');
    		}
    			
    			
    		if(!empty($route)){
    			$dayTimeSub = strtoupper(substr($patInfo->DeliveryRoute,3,3));
    			if($dayTimeSub == '1000')     { $dayTime = '10 AM';  }
    			elseif($dayTimeSub == '1300') { $dayTime = '01 PM';  }
    			elseif($dayTimeSub == '1500') { $dayTime = '03 PM';  }
    			else                          { $dayTime = '10 AM';  }
    		
    		}else{
    			$dayTime = "10 AM";
    		}
    			
    		
    		$date = date('Y-m-d',strtotime('next '.$day));
    		 
    		$data = array(
    				'listOfPatId' => $listPatId,
    				'batchNum'    => $batchNum,
    				'orderNum'    => $orderNum,
    				'DTime'       => $dayTime,
    				'deliveryDate'=> $date,
    				'address1'    => $patInfo->Address1.', '.$patInfo->City.', '.$patInfo->Prov.', '.$patInfo->Postal,
    				'address2'    => $patInfo->Address2,
    				'instruction' => '',
    				'home'        => $patInfo->LastName.' '.$patInfo->FirstName,
    				'deliveryType' => $deliveryType,
    				'event'        => 'Operation',
    				'comment'      => '',
    				'userId'       => $user->id,
    				'addDate'      => date('Y-m-d'),
    				'nhid'         => $patInfo->NHID,
    				'nhwardid'     => $patInfo->NHWardID
    		);
    			
    		 
    		$info = $this->commonModels->getRecordBaseOnOrderId($orderNum);
    		 
    		$d = "";
    		if(!empty($info)){
    			$d = $this->commonModels->updatePackMed($info->id,$data);
    		}else{
    			$d = $this->commonModels->addPackMed($data);
    		}
    		
    		     		
    		echo $orderNum; 		
    		
    	}else{
    		echo "Choose the home";
    	}
    }
    
    public function generateMultipleSheet(){
    	$batchNum = $this -> input -> post('batchNum');
    	
    	$data['batches'] = $this->rest->get('production/exApi/BatchDetails/'.$batchNum);
    	$this->load->model('production/commonModels', 'commonModels');    	
    	$user =  $this->ion_auth->user()->row();
    	$i = 0;
    	
    	foreach($data['batches'] AS $record){
    		  	$patId = $record->ID;	
    		  	$orderNum = $batchNum.'-'.$i++;
	    		$patInfo = $this -> Pat ->getAllDataBaseOnIdRow($patId);	
	    		
	    		$deliveryType = ($patInfo->DeliveryRouteType == 1) ? "N" : "Y";
	    		 
	    		$route = $patInfo->DeliveryRoute;
	    		$day = "";
	    		$dayTime ="";
	    		 
	    		if(!empty($route)){
	    			$daySub = strtoupper(substr($patInfo->DeliveryRoute,0,3));
	    			if($daySub == 'MON')    { $day = 'Monday'; }
	    			elseif($daySub == 'TUE'){ $day = 'Tuesday'; }
	    			elseif($daySub == 'WED'){ $day = 'Wednesday'; }
	    			elseif($daySub == 'THU'){ $day = 'Thursday'; }
	    			elseif($daySub == 'FRI'){ $day = 'Friday'; }
	    			elseif($daySub == 'SAT'){ $day = 'Saturday'; }
	    			elseif($daySub == 'SUN'){ $day = 'Sunday'; }
	    			else { $day = date('l'); }
	    		
	    		}else{
	    			$day = date('l');
	    		}
	    		 
	    		 
	    		if(!empty($route)){
	    			$dayTimeSub = strtoupper(substr($patInfo->DeliveryRoute,3,3));
	    			if($dayTimeSub == '1000')     { $dayTime = '10 AM';  }
	    			elseif($dayTimeSub == '1300') { $dayTime = '01 PM';  }
	    			elseif($dayTimeSub == '1500') { $dayTime = '03 PM';  }
	    			else                          { $dayTime = '10 AM';  }
	    		
	    		}else{
	    			$dayTime = "10 AM";
	    		}
	    		 
	    		
	    		$date = date('Y-m-d',strtotime('next '.$day));
	    		 
	    		$data = array(
	    				'listOfPatId' => $patId,
	    				'batchNum'    => $batchNum,
	    				'orderNum'    => $orderNum,
	    				'DTime'       => $dayTime,
	    				'deliveryDate'=> $date,
	    				'address1'    => $patInfo->Address1.', '.$patInfo->City.', '.$patInfo->Prov.', '.$patInfo->Postal,
	    				'address2'    => $patInfo->Address2,
	    				'instruction' => '',
	    				'home'        => $patInfo->LastName.' '.$patInfo->FirstName,
	    				'deliveryType' => $deliveryType,
	    				'event'        => 'Operation',
	    				'comment'      => '',
	    				'userId'       => $user->id,
	    				'addDate'      => date('Y-m-d'),
	    				'nhid'         => $patInfo->NHID,
	    				'nhwardid'     => $patInfo->NHWardID
	    		);
	    		 
	    		 
	    		$info = $this->commonModels->getRecordBaseOnOrderId($orderNum);
	    		 
	    		$d = "";
	    		if(!empty($info)){
	    			$d = $this->commonModels->updatePackMed($info->id,$data);
	    		}else{
	    			$d = $this->commonModels->addPackMed($data);
	    		}	
    	}
    	
    	echo $batchNum;
    }
    
    function pacMed(){
    	
    	$this->load->view('header');
    	$this->load->view('production/pacMed');
    	$this->load->view('production/layout/productionFooter');    	
    }
    
    function pacVision(){
    	 
    	$this->load->view('header');
    	$this->load->view('production/pacVision');
    	$this->load->view('production/layout/productionFooter');    	 
    }
    
    function pharmacistCheck(){
    	 
    	$this->load->view('header');
    	$this->load->view('production/pharmacistCheck');
    	$this->load->view('production/layout/productionFooter');    	 
    }
    
    function delivery(){    
    	$this->load->view('header');
    	$this->load->view('production/delivery');
    	$this->load->view('production/layout/productionFooter');
    }
    
    function finalCheck(){
    	$this->load->view('header');
    	$this->load->view('production/finalCheck');
    	$this->load->view('production/layout/productionFooter');
    }
    
    
    function saveOrderSheet(){
    	
    	$listOfPatId = $this -> input -> post('listOfPatId');
    	$batchNum    = $this -> input -> post('batchNum');
    	$orderNum    = $this -> input -> post('orderNum');
    	$DTime       = $this -> input -> post('DTime');
    	$deliverDate = $this -> input -> post('deliverDate');
    	$address1    = $this -> input -> post('address1');
    	$address2    = $this -> input -> post('address2');
    	$instruction = $this -> input -> post('DeliveryInstructions');
    	$home        = $this -> input -> post('home');

    	$nhid        = $this -> input -> post('NHID');
    	$nhwardid    = $this -> input -> post('NHWardID');
    	
    	$deliveryType = $this -> input -> post('DeliveryType');
    	$event        = $this -> input -> post('event');
    	$comment      = $this -> input -> post('comment');
    	$GaP          = $this -> input -> post('GaP');
    	
    	$this->load->model('production/commonModels', 'commonModels');
    	
    	$user =  $this->ion_auth->user()->row();
    	
    	$data = array(
    			'listOfPatId' => $listOfPatId,
    			'batchNum'    => $batchNum,
    			'orderNum'    => $orderNum,
    			'DTime'       => $DTime,
    			'deliveryDate' => $deliverDate,
    			'address1'    => $address1,
    			'address2'    => $address2,
    			'instruction' => $instruction,
    			'home'        => $home,
    			'deliveryType' => $deliveryType,
    			'event'        => $event,
    			'comment'      => $comment,
    			'userId'       => $user->id,
    			'addDate'      => date('Y-m-d'),
    			'nhid'         => $nhid,
    			'nhwardid'     => $nhwardid
    	);
    	   	
       
       $info = $this->commonModels->getRecordBaseOnOrderId($orderNum);
       
        $d = "";
    	if(!empty($info)){
    		$d = $this->commonModels->updatePackMed($info->id,$data);    		
    	}else{
    		$d = $this->commonModels->addPackMed($data);    		
    	}
    	
    	if($d > 0){
    		echo ($GaP == 1) ? $orderNum : 0;
    	}else{
    		echo 1;
    	}
    }
    
	function printOrderSheet(){
		$orderNum = $this -> uri -> segment(3);
		
		$this -> load -> model('production/commonModels','commonModels');
		$data['info'] = $this -> commonModels -> getRecordBaseOnOrderId($orderNum);
				
	    $this->load->view('production/printOrderSheet',$data);   
		
	}
	
	public function getPatNameBaseOnId($id){
		
		$this->load->model('production/commonModels','commonModels');
		$data = $this->commonModels->patInfoBaseOnPatId($id);
		
		echo ' - '.$data->LastName.' '.$data->FirstName.'<br/>';
		
	}
	
	public function getWardPhoneNum($id){
		$this->load->model('production/commonModels','commonModels');
		
		$data = $this->commonModels->patInfoBaseOnPatId($id);		
		
		$info = $this->rest->get('production/exApi/NHWardInfoFromPatTable/'.$data->NHWardID);
		
		foreach($info AS $record){
			echo "<br/>";
			$getPhone = $this->rest->get('production/exApi/PhoneNumberBaseOnPatID/'.$record->ID);
				
			foreach($getPhone AS $phoneNum){
				echo $phoneNum->Description." : ".$phoneNum->Phone."<br/>";
			}
		}
	}
	
	
	
	public function getWardPhoneNumPat($id){
		$getPhone = $this->rest->get('production/exApi/PhoneNumberBaseOnPatID/'.$id);
		
		foreach($getPhone AS $phoneNum){
			echo $phoneNum->Description." : ".$phoneNum->Phone."<br/>";
		}
	}
	
	function setBarcode($code){
	
		$this->load->library('Zend');
		//load in folder Zend
		$this->zend->load('Zend/Barcode');
		//generate barcode
		return $file = Zend_Barcode::render('code39', 'image', array('text'=>$code), array())->render();
	
		
	}
	
	function printAllOrderSheet(){
		$batchNum = $this -> uri -> segment(3);
		$this -> load -> model('production/commonModels','commonModels');
		$data['batchInfo'] = $this -> commonModels -> getAllInfoBaseOnBatchNum($batchNum);
		$this ->load->view('production/printAllOrderSheet',$data);
	}
	
    function getBatchDetailsBaseOnBatchNumber(){
   		  $info = $this->rest->get('production/exApi/getInfoBaseOnBatchNumber');		  
    }
	
    function getNumOFOrders(){
    	$this -> load -> model('production/packagerBatch','packagerBatch');
    	$info = $this -> packagerBatch -> getNumberOfOrders();
    	echo $info -> num;
    }
    
    function getNumOfPacmed(){
    	$this -> load -> model('production/commonModels','commonModels');
    	$info = $this -> commonModels -> getQueueOfPacMed();
    	echo $info-> num;
    }
   
    function getNumOfPacVision(){    	
    	$this -> load -> model('production/commonModels','commonModels');
    	$info = $this -> commonModels -> getQueueOfPacVision();
    	echo $info-> num;
    }
    
    function getNumOfPharmacist(){
    	$this -> load -> model('production/commonModels','commonModels');
    	$info = $this -> commonModels -> getQueueOfPharmacistCheck();
    	echo $info-> num;
    }
    
    function getNumOfDelivery(){
    	$this -> load -> model('production/commonModels','commonModels');
    	$info = $this -> commonModels -> getQueueOfDelivery();
    	echo $info-> num;
    }
    
    function getNumOfFinalCheck(){
    	$this -> load -> model('production/commonModels','commonModels');
    	$info = $this -> commonModels -> getNumOfFinalCheck();
    	echo $info-> num;
    }
    
    
    function deliveryShedule(){   	
	   
		$this->load->view('header');
		$this->load->view('production/deliveryShedule');
		$this->load->view('production/layout/productionFooter');
    }

	
	function getDeliveryShedule(){
		
				
		$this->load->model('Pat');
		$data = $this->Pat->getDeliveryDetails();
				
			
		$outp ="";
        foreach($data AS $record){
            if($outp != "") { $outp .= ","; }
            $outp .='{"id":"'          .$record->ID                               .'",';
			$outp .='"patId":"'        .$record->ID                               .'",';
			if($record->DeliveryRouteType == 0 ){
				$outp .='"noteInternal":" Delivery",';
			}else{
				$outp .='"noteInternal":" Pick Up ",';
			}
			
		    $dayAndTime = $record->DeliveryRoute;
		
			$day = substr($dayAndTime, 0,3);
		    $time = substr($dayAndTime,3,4);
					
		
			if($day == "MON"){ $outp .='"DayInfo":" MONDAY",';    }
			if($day == "TUE"){ $outp .='"DayInfo":" TUESDAY",';   }
			if($day == "WED"){ $outp .='"DayInfo":" WEDNESDAY",'; }
		    if($day == "THU"){ $outp .='"DayInfo":" THURSDAY",';  }
		    if($day == "FRI"){ $outp .='"DayInfo":" FRIDAY",';    }
		    
			if($time == "1000"){ $outp .='"TimeInfo":" 10AM",';   }
			if($time == "1300"){ $outp .='"TimeInfo":" 01PM",';   }
			if($time == "1500"){ $outp .='"TimeInfo":" 03PM",';   }			
			
			$outp .='"deliveryNote":"' .$record->Code                             .'",';
			$outp .='"patName":"'      .$record->LastName.' '.$record->FirstName  .'"}';
        }  
        
        $outp ='{"deliveryInfo":['.$outp.']}';      
        
       	echo($outp);
	}

   function getDeliverySheduleInfoById(){
   	
	   
	    $in = json_decode(file_get_contents("php://input"));
	    $id               = $in->id;
		
		$this->load->model('production/commonModels', 'commonModels');
		$data = $this->commonModels->getAllInfoBaseOnId($id);
		
		$outp ="";
		$outp .='{"id" : "'        .$data->id                           .'",';
		$outp .='"patId":"'        .$data->patId                        .'",';
		$outp .='"noteInternal":"' .$data->noteForInternal              .'",';
		$outp .='"deliveryNote":"' .$data->deliveryNote                 .'",';
        $outp .='"patName":"'      .$data->LastName.' '.$data->FirstName.'"}';
		
		$outp ='{"deliverySheduleInfo":['.$outp.']}';
		
		echo($outp);
   }
   
   function getInfoToPacMed(){
   	
   	   $this -> load -> model('production/commonModels','commonModels');
   	   $info = $this -> commonModels ->getAllInfoFromPacMed();
   	   

   	    $outp ="";
   	    foreach($info AS $record){
   	   		if($outp != "") { $outp .= ","; }
   	   		$outp .='{"id" : "'        .$record->id                             .'",';
   	   		$outp .='"batchNum":"'     .$record->batchNum                       .'",';
   	   		$outp .='"orderNum":"'     .$record->orderNum                       .'",';
   	   		$outp .='"home":"'         .$record->home                           .'",';
   	   		$outp .='"event":"'        .$record->event                           .'",';
   	   		$outp .='"delivery":"'     .$record->deliveryDate.' @ '.$record->DTime.'"}';
   	   	}
   	   	
   	   	$outp ='{"pacMed":['.$outp.']}';
   	   	
   	   	echo($outp);
   }
   
   function getInfoToPacVision(){
   
   	$this -> load -> model('production/commonModels','commonModels');
   	$info = $this -> commonModels ->getAllInfoFromPacVision();
   		
   
   	$outp ="";
   	foreach($info AS $record){
   		if($outp != "") { $outp .= ","; }
   		$outp .='{"id" : "'        .$record->id                             .'",';
   		$outp .='"batchNum":"'     .$record->batchNum                       .'",';
   		$outp .='"orderNum":"'     .$record->orderNum                       .'",';
   		$outp .='"home":"'         .$record->home                           .'",';
   		$outp .='"delivery":"'     .$record->deliveryDate.' @ '.$record->DTime.'"}';
   	}
   
   	$outp ='{"pacVision":['.$outp.']}';
   
   	echo($outp);
   }
   
   function getInfoToPharmacist(){
	   	$this -> load -> model('production/commonModels','commonModels');
	   	$info = $this -> commonModels ->getAllInfoFrompharmacistcheck();
	   	 
	   	 
	   	$outp ="";
	   	foreach($info AS $record){
	   		if($outp != "") { $outp .= ","; }
	   		$outp .='{"id" : "'        .$record->id                             .'",';
	   		$outp .='"batchNum":"'     .$record->batchNum                       .'",';
	   		$outp .='"orderNum":"'     .$record->orderNum                       .'",';
	   		$outp .='"home":"'         .$record->home                           .'",';
	   		$outp .='"event":"'        .$record->event                           .'",';
	   		$outp .='"delivery":"'     .$record->deliveryDate.' @ '.$record->DTime.'"}';
	   	}
	   	 
	   	$outp ='{"pharmacist":['.$outp.']}';
	   	 
	   	echo($outp);
   }
   
   function getInfoToFinalCheck(){
	   	$this -> load -> model('production/commonModels','commonModels');
	   	$info = $this -> commonModels ->getAllInfoFinalCheck();
	   
	   	$outp ="";
	   	foreach($info AS $record){
	   		
		   		if($outp != "") { $outp .= ","; }
		   		$outp .='{"id" : "'        .$record->id                             .'",';
		   		$outp .='"batchNum":"'     .$record->batchNum                       .'",';
		   		$outp .='"orderNum":"'     .$record->orderNum                       .'",';
		   		$outp .='"home":"'         .$record->home                           .'",';
		   		$outp .='"event":"'        .$record->event                           .'",';
		   		$outp .='"delivery":"'     .$record->deliveryDate.' @ '.$record->DTime.'"}';
	   		
	   	}
	   
	   	$outp ='{"deliveryFinal":['.$outp.']}';
	   
	   	echo($outp);
	   	
   }
   
   function getInfoToDelivery(){
	   	$this -> load -> model('production/commonModels','commonModels');
	   	$info = $this -> commonModels ->getAllInfoFromDelivery();
	   	   
	   	$outp ="";
	   	foreach($info AS $record){	   		
		   		if($outp != "") { $outp .= ","; }
		   		$outp .='{"id" : "'        .$record->id                             .'",';
		   		$outp .='"batchNum":"'     .$record->batchNum                       .'",';
		   		$outp .='"orderNum":"'     .$record->orderNum                       .'",';
		   		$outp .='"home":"'         .$record->home                           .'",';
		   		$outp .='"event":"'        .$record->event                           .'",';
		   		$outp .='"delivery":"'     .$record->deliveryDate.' @ '.$record->DTime.'"}';	   		
	   	}
	   	
	   
	   	$outp ='{"delivery":['.$outp.']}';
	   
	   	echo($outp);
   }
	
   function complatePACmedByOrderId(){
   	  $orderId =  $this ->input ->post('orderId');
   	  
   	  $this -> load -> model('production/commonModels','commonModels');   	 
   	  $user = $this -> ion_auth -> user() -> row(); 
   	  
   	  $data = array(
   	  	  'orderId' => $orderId,
   	  	  'userId'  => $user->id,
   	  	  'addDate' => date('Y-m-d')
   	  );
   	  
   	  $data1 = $this->commonModels->getRecordBaseOnOrderId($orderId);
   	  
   	  if(!empty($data1)){
	   	  	$info = $this->commonModels->getRecordBaseOnOrderIdInPacVision($orderId);
	   	  	if(empty($info)){
	   	  		$d = $this->commonModels->addPackVision($data);
	   	  		if($d > 0){
	   	  			echo 'Update Successfully!!!';
	   	  		}else{
	   	  			echo 'Some things went wrong, try again';
	   	  		}
	   	  	}else{
	   	  		echo "Already updated";
	   	  	}
   	  }else{
   	  	 echo "Invalid Sheet!!!";
   	  	 
   	  }
   }
   
   
   function complatePACvisionByOrderId(){
   	
	   	$orderId =  $this ->input ->post('orderId');
	   
	   
	   	$this -> load -> model('production/commonModels','commonModels');
	   	$user = $this -> ion_auth -> user() -> row();
	   
	   	$data = array(
	   			'orderId' => $orderId,
	   			'userId'  => $user->id,
	   			'addDate' => date('Y-m-d')
	   	);
	   
	   	$data1 = $this->commonModels->getRecordBaseOnOrderId($orderId);
	   
	   	if(!empty($data1)){
	   		$info = $this->commonModels->getRecordBaseOnOrderIdInPacVision($orderId);
	   		if(empty($info)){ $d = $this->commonModels->addPackVision($data);  }
	   		
	   		
	   		$pacVision = $this->commonModels->getRecordBaseOnOrderIdInPharmacistcheck($orderId);
	   	    if(empty($pacVision)){
	   	    	$d = $this->commonModels->addPharmacistCheck($data);
	   	    	if($d > 0){
	   	    		echo 'Update Successfully!!!';
	   	    	}else{
	   	    		echo 'Some things went wrong, try again';
	   	    	}
	   		}else{
	   			echo "Already updated";
	   		}
	   	}else{
	   		echo "Invalid Sheet!!!";
	   
	   	}	   	
   }
   
   function complatePharmacistByOrderId(){
   	    $orderId =  $this ->input ->post('orderId');
   	    
   	    $this -> load -> model('production/commonModels','commonModels');
   	    $user = $this -> ion_auth -> user() -> row();
   	    
   	    $data = array(
   	    		'orderId' => $orderId,
   	    		'userId'  => $user->id,
   	    		'addDate' => date('Y-m-d')
   	    );
   	    
   	    $data1 = $this->commonModels->getRecordBaseOnOrderId($orderId);
   	    
   	    if(!empty($data1)){
   	    	$info = $this->commonModels->getRecordBaseOnOrderIdInPacVision($orderId);
   	    	if(empty($info))     { $d = $this->commonModels->addPackVision($data);  }
   	        
   	    	$pacVision = $this->commonModels->getRecordBaseOnOrderIdInPharmacistcheck($orderId);
   	    	if(empty($pacVision)){ $d = $this->commonModels->addPharmacistCheck($data); }
   	    
   	    	$pacDelivery = $this->commonModels->getRecordBaseOnOrderIdInDelivery($orderId);
   	    	if(empty($pacDelivery)){
   	    		$d = $this->commonModels->addDelivery($data);
   	    		if($d > 0){
   	    			echo 'Update Successfully!!!';
   	    		}else{
   	    			echo 'Some things went wrong, try again';
   	    		}
   	    	}else{
   	    		echo "Already updated";
   	    	}
   	    }else{
   	    	echo "Invalid Sheet!!!";
   	    
   	    }
   }
   
   
   function complateFinalCheckOrderId(){
	   	$orderId =  $this ->input ->post('orderId');
	   
	   	$this -> load -> model('production/commonModels','commonModels');
	   	$user = $this -> ion_auth -> user() -> row();
	   
	   	$data = array(
	   			'orderId' => $orderId,
	   			'userId'  => $user->id,
	   			'addDate' => date('Y-m-d')
	   	);
	   
	   	$data1 = $this->commonModels->getRecordBaseOnOrderId($orderId);
	   
	   	if(!empty($data1)){
	   		$info = $this->commonModels->getRecordBaseOnOrderIdInPacVision($orderId);
	   		if(empty($info))     { $d = $this->commonModels->addPackVision($data);  }
	   
	   		$pacVision = $this->commonModels->getRecordBaseOnOrderIdInPharmacistcheck($orderId);
	   		if(empty($pacVision)){ $d = $this->commonModels->addPharmacistCheck($data); }
	   
	   		$pacDelivery = $this->commonModels->getRecordBaseOnOrderIdInDelivery($orderId);
	   		if(empty($pacDelivery)){
	   			
	   			$data['finalCheck'] = 0;
	   			
	   			
	   			$d = $this->commonModels->addDelivery($data);
	   			if($d > 0){
	   				echo 'Update Successfully!!!';
	   			}else{
	   				echo 'Some things went wrong, try again';
	   			}
	   		}else{	   			
	   			$in = array(
	   				 'finalCheck' => 0	
	   			);
	   			
	   			$id = $pacDelivery->id;
	   			$n = $this->commonModels->updateDelivey($id,$in);
	   			if($n > 0){
	   				echo 'Update Successfully!!!';
	   			}else{
	   				echo 'Some things went wrong, try again';
	   			}
	   		}
	   	}else{
	   		echo "Invalid Sheet!!!";
	   
	   	}
   }
   
   public function completeAllBackLog(){
   	    
		   	$this -> load -> model('production/commonModels','commonModels');
		   	$user = $this -> ion_auth -> user() -> row();
		   	$info = $this -> commonModels -> getAllBacklog();
		   	
		   	foreach($info AS $record){		   		
		   		     $orderId = $record->orderNum;
				   	$data = array(
			   			'orderId' => $orderId,
			   			'userId'  => $user->id,
			   			'addDate' => date('Y-m-d')
				   	);
				   	
				   	$data1 = $this->commonModels->getRecordBaseOnOrderId($orderId);
				   	
				   	if(!empty($data1)){
				   		$info = $this->commonModels->getRecordBaseOnOrderIdInPacVision($orderId);
				   		if(empty($info))     { $d = $this->commonModels->addPackVision($data);  }
				   	
				   		$pacVision = $this->commonModels->getRecordBaseOnOrderIdInPharmacistcheck($orderId);
				   		if(empty($pacVision)){ $d = $this->commonModels->addPharmacistCheck($data); }
				   	
				   		$pacDelivery = $this->commonModels->getRecordBaseOnOrderIdInDelivery($orderId);
				   		
				   		if(empty($pacDelivery)){
				   			$d = $this->commonModels->addDelivery($data);
				   			if($d > 0){
				   				echo 'Update Successfully!!!';
				   			}else{
				   				echo 'Some things went wrong, try again';
				   			}
				   		}else{
				   			echo "Already updated";
				   		}
				   	}else{
				   		echo "Invalid Sheet!!!";
				   	
				   	}
		   	
            }
   	    
   }
   	 
	
    
	
}