<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

include_once ('secure_controller.php');

class setting extends Secure_Controller {	
		
	function __construct() {
		
		// Call the Model constructor
		parent::__construct();	 
		
		
		if ($this -> quick -> accessTo('compliance')) {
			// do nothing they are legit
		} else {
			// current user should not have access to this controller.
			redirect('news/mynews');
		}	
	}
	
	public function getDeliveryTimeAndHomeOrPatInfoBaseOnDay(){
		$day = $this->uri->segment(3);
		$data['days'] = $day;
		$data['deliveryDetails']    = $this->rest->get('production/exApi/getDeliveryDetails/'.$day);
		
		$this -> load -> view('settings/printDeliveryDetails',$data);
	}
	
	public function home(){
		$this -> load -> view('header');
		$this -> load -> view('settings/settings');
		$this -> load -> view('footer');
	}
	
}
	
