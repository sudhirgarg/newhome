<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left">Reconcile Delivery Orders</h3>
</div>
<div class="description">
	<a class="btn btn-default btn-lg" href="#ActionConfirmation">Delivered</a>
	<a class="btn btn-default btn-lg" href="#ActionConfirmation">Returned</a>
	<a class="btn btn-default btn-lg" href="#ActionConfirmation">Cancelled</a>
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->

<!--  ORDERS SELECTED -->
<div class="row">
	<div class="col-lg-12">
		<div class="input-group">
			<span class="input-group-addon"><i class="fa fa-barcode"></i> </span>
			<input type="text" class="form-control"
				placeholder="Scan Delivery Order here.">
		</div>
		<!-- BOX -->
		<div class="box border blue">
			<div class="box-title">
				<h4>
					<i class="fa fa-reorder"></i> <span class="hidden-inline-mobile">
						Selected Orders</span>
				</h4>
			</div>
			<div class="box-body">
				<table class="table">
					<thead>
						<tr>
							<th>ORDER ID</th>
							<th>Patient</th>
							<th>Delivery Date</th>
							<th>Complete Date</th>
							<th>Status</th>
							<th>Remove</th>
							<th>View</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>12343</td>
							<td>Mohammed Mohammed 743 2324</td>
							<td>Fri, Mar 12, 10 am</td>
							<td>Fri, Mar 13, 5 pm</td>
							<td>DELIVERED</td>
							<td><a class="btn btn-default btn-lg"><i class="fa fa-times"></i>
									Remove</a></td>
							<td><a class="btn btn-default btn-lg"><i class="fa fa-eye"></i>
									View</a></td>
						</tr>
						<tr>
							<td>12343</td>
							<td>Mohammed Mohammed 743 2324</td>
							<td>Fri, Mar 12, 10 am</td>
							<td>Fri, Mar 13, 5 pm</td>
							<td>DELIVERED</td>
							<td><a class="btn btn-default btn-lg"><i class="fa fa-times"></i>
									Remove</a></td>
							<td><a class="btn btn-default btn-lg"><i class="fa fa-eye"></i>
									View</a></td>
						</tr>
						<tr>
							<td>12343</td>
							<td>Mohammed Mohammed 743 2324</td>
							<td>Fri, Mar 12, 10 am</td>
							<td>Fri, Mar 13, 5 pm</td>
							<td>DELIVERED</td>
							<td><a class="btn btn-default btn-lg"><i class="fa fa-times"></i>
									Remove</a></td>
							<td><a class="btn btn-default btn-lg"><i class="fa fa-eye"></i>
									View</a></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

	</div>
</div>
<!-- /ORDERS WAITING DELivery -->





<!-- Modal -->
<div class="modal fade" id="ActionConfirmation" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Confirmation</h4>
			</div>
			<div class="modal-body">
				<!--   MODEL CONTENT -->
				<div class="row">
					<div class="col-lg-12">
						<table class="table">
							<thead>
								<tr>
									<th>Order #</th>
									<th>Driver</th>
									<th>Patient</th>
									<th>Age</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>123</td>
									<td>Delivery service Ontario | 742 5323</td>
									<td>John Doe | 753 2345 | 123 Alexander St.</td>
									<td><i class="fa fa-clock-o"></i> 3 hrs ago</td>
								</tr>
								<tr>
									<td>123</td>
									<td>Delivery service Ontario | 742 5323</td>
									<td>John Doe | 753 2345 | 123 Alexander St.</td>
									<td><i class="fa fa-clock-o"></i> 3 hrs ago</td>
								</tr>
								<tr>
									<td>123</td>
									<td>Delivery service Ontario | 742 5323</td>
									<td>John Doe | 753 2345 | 123 Alexander St.</td>
									<td><i class="fa fa-clock-o"></i> 3 hrs ago</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<!-- / MODEL CONTENT -->
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!--  / MODAL -->
<!-- /CONTENT -->



<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>


