<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left">Delivery Run Details</h3>
</div>
<div class="description">

	<h4>
		<span class="label label-primary arrow-in">RunID:2133 Status: Created
			01/13/2014</span>
	</h4>

	<form class="navbar-form " role="search">
		<div class="form-group">
			<select class="form-control">
				<option>Richard</option>
				<option>Poney</option>
				<option>Express Service</option>
			</select>
		</div>
		<div class="form-group">
			<input type="text" class="form-control" placeholder="Run Note">
		</div>
		<button type="submit" class="btn btn-default">Save</button>
		<button type="submit" class="btn btn-default">Archive</button>
		<a class="btn btn-lg btn-default" href="<?php echo base_url()?>delivery/map">Map</a>
	</form>


</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DELIVERY RUNS CONTENT -->
<!-- DATA TABLES -->
<div class="row">
	<div class="col-md-12">



		<!--  ORDERS WAITING DELivery -->
		<div class="row">
			<div class="col-lg-12">
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-barcode"></i> </span>
					<input type="text" class="form-control"
						placeholder="Scan Delivery Order here.">
				</div>

				<div class="col-lg-12">
					<!-- BOX -->
					<div class="box border blue">
						<div class="box-title">
							<h4>
								<i class="fa fa-compass"></i>Map of route
							</h4>
							<div class="tools">
								<a href="#box-config" data-toggle="modal" class="config"> <i
									class="fa fa-cog"></i>
								</a> <a href="javascript:;" class="reload"> <i
									class="fa fa-refresh"></i>
								</a> <a href="javascript:;" class="collapse"> <i
									class="fa fa-chevron-up"></i>
								</a> <a href="javascript:;" class="remove"> <i
									class="fa fa-times"></i>
								</a>
							</div>
						</div>
						<div class="box-body">
							<div id="map-canvas"/>
						</div>
					</div>
					<!-- /BOX -->
				</div>

				<!-- BOX -->
				<div class="box border blue">
					<div class="box-title">
						<h4>
							<i class="fa fa-reorder"></i> <span class="hidden-inline-mobile">
								Orders in this Run</span>
						</h4>
					</div>
					<div class="box-body">
						<table class="table">
							<thead>
								<tr>
									<th>ORDER ID</th>
									<th>Patient</th>
									<th>Delivery Date</th>
									<th>Delivery Note</th>
									<th>Status</th>
									<th>Remove</th>
									<th>View</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>12343</td>
									<td>Mohammed Mohammed 743 2324</td>
									<td>Fri, Mar 12, 10 am</td>
									<td>deliver back door</td>
									<td>DELIVERED</td>
									<td><a class="btn btn-default btn-lg"><i class="fa fa-times"></i>
											Remove</a></td>
									<td><a class="btn btn-default btn-lg"><i class="fa fa-eye"></i>
											View</a></td>
								</tr>
								<tr>
									<td>12343</td>
									<td>Mohammed Mohammed 743 2324</td>
									<td>Fri, Mar 12, 10 am</td>
									<td>Knock loudly</td>
									<td>DELIVERED</td>
									<td><a class="btn btn-default btn-lg"><i class="fa fa-times"></i>
											Remove</a></td>
									<td><a class="btn btn-default btn-lg"><i class="fa fa-eye"></i>
											View</a></td>
								</tr>
								<tr>
									<td>12343</td>
									<td>Mohammed Mohammed 743 2324</td>
									<td>Fri, Mar 12, 10 am</td>
									<td></td>
									<td>DELIVERED</td>
									<td><a class="btn btn-default btn-lg"><i class="fa fa-times"></i>
											Remove</a></td>
									<td><a class="btn btn-default btn-lg"><i class="fa fa-eye"></i>
											View</a></td>
								</tr>
							</tbody>
						</table>
						<a class="btn btn-block btn-primary"><h4>Deliver</h4></a>
					</div>
				</div>
			</div>
		</div>
		<!-- /ORDERS WAITING DELivery -->



		<!--  ORDERS WAITING DELivery -->
		<div class="row">
			<div class="col-lg-12">
				<!-- BOX -->
				<div class="box border blue">
					<div class="box-title">
						<h4>
							<i class="fa fa-reorder"></i> <span class="hidden-inline-mobile">
								Orders Waiting for Delivery</span>
						</h4>
					</div>
					<div class="box-body">
						<table id="datatable1" cellpadding="0" cellspacing="0" border="0"
							class="datatable table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th>ORDER ID</th>
									<th>Patient</th>
									<th>Delivery Date</th>
									<th>Status</th>
									<th>Add</th>
									<th>View</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>12343</td>
									<td>Mohammed Mohammed 743 2324</td>
									<td>Fri, Mar 12, 10 am</td>
									<td>AWAITING DELIVERY</td>
									<td><a class="btn btn-default btn-lg"><i class="fa fa-plus"></i>
											Add to run</a></td>
									<td><a class="btn btn-default btn-lg"><i class="fa fa-eye"></i>
											View</a></td>
								</tr>
								<tr>
									<td>12343</td>
									<td>Mohammed Mohammed 743 2324</td>
									<td>Fri, Mar 12, 10 am</td>
									<td>AWAITING DELIVERY</td>
									<td><a class="btn btn-default btn-lg"><i class="fa fa-plus"></i>
											Add to run</a></td>
									<td><a class="btn btn-default btn-lg"><i class="fa fa-eye"></i>
											View</a></td>
								</tr>
								<tr>
									<td>12343</td>
									<td>Mohammed Mohammed 743 2324</td>
									<td>Fri, Mar 12, 10 am</td>
									<td>AWAITING DELIVERY</td>
									<td><a class="btn btn-default btn-lg"><i class="fa fa-plus"></i>
											Add to run</a></td>
									<td><a class="btn btn-default btn-lg"><i class="fa fa-eye"></i>
											View</a></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- /ORDERS WAITING DELivery -->
	</div>
</div>
<!-- /CONTENT -->

<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>


