<div class="clearfix">
	<h3 class="content-title pull-left">Delivery Runs</h3>
</div>
<div class="description">
	<a class="btn btn-default btn-lg"
		href="<?php echo base_url("delivery/reconciledeliveries");?>"> <i
		class="fa fa-pencil"></i> Reconcile Delivery Orders
	</a> <a class="btn btn-default btn-lg"
		href="<?php echo base_url("delivery/rundetail/-1");?>"> <i
		class="fa fa-plus"></i> Delivery Run
	</a> <a class="btn btn-default btn-lg"
		href="<?php echo base_url("delivery/order/-1");?>"> <i
		class="fa fa-plus"></i> Delivery Order
	</a> | Delivery Runs and Order tracking
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DELIVERY RUNS CONTENT -->
<div class="row">


</div>
<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>