<html>
   <head>
      <meta http-equiv="content-type" content="text/html; charset=UTF-8">
   </head>
   <body>
	  <div class="row">
		  <div class="col-md-12 toolbar">
				<span class="btn-group">
				  <button class="btn btn-light-grey replyBtn">
					<i class="fa fa-reply"></i>
				  </button>
				  <button class="btn btn-light-grey dropdown-toggle" data-toggle="dropdown">
				  <i class="fa fa-angle-down"></i>
				  </button>
				  <ul class="dropdown-menu context pull-right text-left">
					 <li><a href="#"><i class="fa fa-reply reply-btn"></i> Reply All</a></li>
					 <li><a href="#"><i class="fa fa-arrow-right reply-btn"></i> Forward</a></li>
					 <li><a href="#"><i class="fa fa-print"></i> Print</a></li>
					 <li><a href="#"><i class="fa fa-ban"></i> Spam</a></li>
					 <li><a href="#"><i class="fa fa-trash-o"></i> Delete</a></li>
					 <li></li>
				  </ul>
				</span>
				<span class="btn-group">
					<button class="btn btn-light-grey">
						<i class="fa fa-cloud-download"></i>
					</button>
					<button class="btn btn-light-grey">
						<i class="fa fa-share"></i>
					</button>
					<button class="btn btn-light-grey">
						<i class="fa fa-trash-o"></i>
					</button>
				</span>
		  </div>
	  </div>
	  <div class="divide-20"></div>
      <div class="emailTitle emailViewHeader">
         <h1><?php echo htmlspecialchars($msg->get_subject()); ?></h1> 
      </div>
	  <hr>
      <div class="emailViewContent">
         <form class="form-horizontal" role="form">
			<div class="form-group">
				<label class="col-sm-1 control-label">From:</label>
				<label class="col-sm-11 control-label"><?php echo htmlspecialchars($msg->get_from()); ?></label>
			</div>
			<div class="form-group">
				<label class="col-sm-1 control-label">To:</label>
				<label class="col-sm-11 control-label"><?php echo htmlspecialchars($msg->get_to()); ?></label>
			</div>
			<div class="form-group">
				<label class="col-sm-1 control-label">Date:</label>
				<label class="col-sm-11 control-label"><?php echo date('D, M j, Y', $msg->get_timestamp()) . ' at ' . date('g:i A', $msg->get_timestamp()); ?></label>
			</div>
		 </form>
      </div>
	  <hr>
      <div class="emailView">
        <?php echo $body; ?>
      </div>
      
      <!-- Attachment -->
<?php /*
      <hr>
      <div class="emailAttached">
         <div class="margin-bottom-15">
            <span class="font-600">3 attachments —</span> 
            <a href="#">Download all attachments</a>   
         </div>
		 <div class="row">
			<div class="col-md-4">
				<div class="margin-bottom-25">
					<img src="img/inbox/1.jpg">
					<div>
					   <strong>lighthouse.jpg</strong>
					   <span>373K</span>
					   <div class="btn-group">
						   <a href="#" class="btn btn-light-grey btn-xs">View</a>
						   <a href="#" class="btn btn-light-grey btn-xs">Share</a>
						   <a href="#" class="btn btn-light-grey btn-xs">Download</a>
					   </div>
					</div>
				 </div>
			</div>
			<div class="col-md-4">
				<div class="margin-bottom-25">
					<img src="img/inbox/2.jpg">
					<div>
					   <strong>bokeh-bg.jpg</strong>
					   <span>124K</span>
					   <div class="btn-group">
						   <a href="#" class="btn btn-light-grey btn-xs">View</a>
						   <a href="#" class="btn btn-light-grey btn-xs">Share</a>
						   <a href="#" class="btn btn-light-grey btn-xs">Download</a>
					   </div>
					</div>
				 </div>
			</div>
			<div class="col-md-4">
				<div class="margin-bottom-25">
					<img src="img/inbox/3.jpg">
					<div>
					   <strong>sunshine.jpg</strong>
					   <span>222K</span>
					   <div class="btn-group">
						   <a href="#" class="btn btn-light-grey btn-xs">View</a>
						   <a href="#" class="btn btn-light-grey btn-xs">Share</a>
						   <a href="#" class="btn btn-light-grey btn-xs">Download</a>
					   </div>
					</div>
				 </div>
			</div>
		 </div>
      </div>
*/ ?>
   </body>
</html>