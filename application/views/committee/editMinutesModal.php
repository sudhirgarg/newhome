<?php
$minutes = 
$isNew = true;
$minutes;
if ($this->uri->segment ( 4 ) > 0) {
	$isNew = false;
	$minutes = $this->committee->getMeeting ( $this->uri->segment ( '4', '-1' ) );
}
?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"
		aria-hidden="true">&times;</button>
	<h4 class="modal-title">Edit Meeting details</h4>
</div>
<div class="modal-body">
	<div class="form-horizontal">
		<div class="form-group">
			<label class="col-sm-3 control-label">Meeting Title</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" id="minutesTitle"
					placeholder="Ex: Progress Report on Continuous Improvement"
					value="<?php if($isNew==false){echo $minutes->title;} ?>" />
			</div>
		</div>
		<div class="form-group">
			<input id="committeeMinutesID"
				value="<?php if($isNew==false){echo $minutes->committeeMinutesID;} ?>" type="hidden"> <label
				class="col-sm-3 control-label">Committee</label>
			<div class="col-sm-9">
				<select class="form-control" id="committeeID">
					<?php
					
$committees = $this->committee->getCommittees ();
					foreach ( $committees->result () as $com ) {
						?>
                        <option value="<?php echo $com->committeeID;?>"
						<?php if($isNew==false){if ($minutes->committeeID== $com->committeeID) { echo ' selected="selected"';} }?>> <?php echo $com->committeeName; ?> </option>   
                        <?php
					}
					?>
				</select>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label">Present</label>
			<div class="col-sm-9">
				<input type="text" class="form-control"
					placeholder="Who was present during the meeting?" id="present"
					value="<?php if($isNew==false){echo $minutes->present; }?>" />
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label">Meeting Date</label>
			<div class="col-sm-9">
				<input type="text" id="meetingDate"
					class="form-control datepicker-fullscreen"
					value="<?php if($isNew==false){echo $minutes->dateCreated; }?>">
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label">Discussion</label>
			<div class="col-sm-9">
				<textarea class="form-control" rows="15" id="meetingDiscussion"><?php if($isNew==false){echo $minutes->discussion;} ?></textarea>
			</div>
		</div>

	</div>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal"
		id="closeModalButton">Close</button>
	<button type="button" class="btn btn-primary"
		data-loading-text="Loading..." onclick="saveMeeting()"
		id="saveMinutesButton">Save changes</button>
</div>

<script>
    $("#meetingDate").datepicker({dateFormat: 'yy-mm-dd'});
</script>