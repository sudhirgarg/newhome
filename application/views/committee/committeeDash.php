<div class="clearfix">
	<h3 class="content-title pull-left">Committees</h3>
</div>
<div class="description">
	<button class="btn btn-primary btn-lg"
		onclick="loadModal('<?php echo site_url("filingCab/editCommitteeModal/-1");?>')">
		<i class="fa fa-plus"></i> Add Committee
	</button>
	<button class="btn btn-primary btn-lg"
		onclick="loadModal('<?php echo base_url("filingCab/editMinutesModal/-1");?>')">
		<i class="fa fa-plus"></i> Minutes
	</button>
	| The path to discussion, discovery, and change Overview, Statistics
	and more
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!--  MAIN PAGE HERE -->


<div class="description"></div>

<div class="box border">
	<div class="box-title">
		<h4>
			<i class="fa fa-columns"></i> <span class="hidden-inline-mobile">Committees
				and Meeting Minutes</span>
		</h4>
	</div>
	<div class="box-body">
		<div class="tabbable header-tabs">
			<ul class="nav nav-tabs">
				<li><a href="#DeliveryOrders" data-toggle="tab"><i class="fa fa-rss"></i>
						<span class="hidden-inline-mobile">Activity / Minutes</span></a></li>
				<li class="active"><a href="#DeliveryRuns" data-toggle="tab"><i
						class="fa fa-users"></i> <span class="hidden-inline-mobile">Committees</span></a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="DeliveryRuns">
					<!--  DELIVERY RUN TAB -->

					<table id="datatable1" cellpadding="0" cellspacing="0" border="0"
						class="datatable table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th>Committee Name</th>
								<th>Description</th>
								<th>Meetings</th>
								<th>Last Meeting</th>
							</tr>
						</thead>
						<tbody>
                        <?php
																								$v = $this->committee->getCommittees ();
																								
																								if ($v != false) {
																									foreach ( $v->result () as $com ) {
																										?>
                                    <tr
								onclick="window.location='<?php echo site_url('filingCab/viewCommittee/' . $com->committeeID);?>'">
								<td><?php echo $com->committeeName; ?></td>
								<td><?php echo $com->committeeDescription; ?></td>
								<td><?php echo $this->committee->getCommitteeMeetings($com->committeeID)->num_rows(); ?></td>
								<td><i class="fa fa-clock-o"></i> <?php echo $this->committee->getLastMeetingTimeString($com->committeeID); ?></td>
							</tr>
                                    <?php
																									}
																								}
																								?>
						
						</tbody>
					</table>
					<!--  /DELIVERY RUN TAB -->
				</div>
				<div class="tab-pane" id="DeliveryOrders">
					<!--  Committee TAB -->
					<table id="datatable2" cellpadding="0" cellspacing="0" border="0"
						class="datatable table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th>Title</th>
								<th>Committee</th>
								<th>Present</th>
								<th>Discussion</th>
								<th>Meeting Date</th>
							</tr>
						</thead>
						<tbody>
						
						<?php
						$minutes = $this->committee->getMeetings ();
						if ($minutes != false) {
							foreach ( $minutes->result () as $minute ) {
								?>
								<tr
								onclick="loadModal('<?php echo site_url('filingCab/editMinutesModal/' . '-1' . '/'. $minute->committeeMinutesID);?>')">

								<td><?php echo $minute->title;?></td>
								<td><?php $committee = $this->committee->getCommittee($minute->committeeID); echo $committee->committeeName; ?></td>
								<td><?php echo $minute->present; ?></td>
								<td><?php echo $minute->discussion; ?></td>
								<td><i class="fa fa-clock-o"></i> <?php echo date('Y-m-d', strtotime($minute->dateCreated)); ?> </td>
							</tr>
										
							<?php
							}
						} else {
							?>
<tr>
								<td>No Meetings</td>
							</tr><?php
						}
						?>
							
						</tbody>
					</table>
					<!--  /Committee TAB -->
				</div>
			</div>
		</div>
	</div>
</div>

<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>
