<?php
$this->load->library ( 'ion_auth' );
$this->load->helper ( 'url' );
?>

<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left"><?php echo lang('create_group_heading');?></h3>
</div>
<div class="description">
	<a class="btn btn-default btn-lg" href="<?php echo base_url();?>auth/">
		<i class="fa fa-back"> Back to Users </i>
	</a>
<?php echo lang('create_group_subheading');?>
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->


<!-- START   --!>


<div class="box border primary">
			<div class="box-title">
				<h4>
					<i class="fa fa-bars"></i>Group Properties
				</h4>

			</div>
			<div class="box-body big">

<div id="infoMessage"><?php echo $message;?></div>


<?php echo form_open("auth/create_group");?>

      <p>
            <?php echo lang('create_group_name_label', 'group_name');?> <br />
            <?php echo form_input($group_name);?>
      </p>

      <p>
            <?php echo lang('create_group_desc_label', 'description');?> <br />
            <?php echo form_input($description);?>
      </p>

      <p><?php echo form_submit('submit', lang('create_group_submit_btn'));?></p>

      <h2>Available Groups</h2>
      <table id="datatable1" cellpadding="0" cellspacing="0" border="0"
					class="datatable table table-striped table-bordered table-hover">
		<thead>
		<tr><td>Group Name</td><td>Description</td><td>Edit</td></tr>
		<thead>
		<tbody>
<?php
echo form_close ();
$groups = $this->ion_auth->groups ()->result ();
foreach ( $groups as $group ) {
	?>
			<tr><td><?php echo $group->name;?></td>
			<td><?php echo $group->description;?></td>
			<td><a class="btn btn-lg btn-default" href="<?php echo base_url()?>auth/edit_group/<?php echo $group->id;?>">Edit</td>
			</tr>
	<?php
}
?>
</tbody
</table>


</div>
</div>
<!-- END   --!>
<!-- /CONTENT -->
<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>
</div>
<!-- /CONTENT-->
</div>
</div>
</div>
</section>
<!--/PAGE -->
<!-- JAVASCRIPTS -->
<!-- Placed at the end of the document so the pages load faster -->
<!-- JQUERY -->
<script src="<?php echo base_url()?>js/jquery/jquery-2.0.3.min.js"></script>
<!-- JQUERY UI-->
<script
	src="<?php echo base_url()?>js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
<!-- BOOTSTRAP -->
<script
	src="<?php echo base_url()?>js/bootstrap-dist/js/bootstrap.min.js"></script>


<!-- DATA TABLES -->
<script type="text/javascript"
	src="<?php echo base_url();?>js/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript"
	src="<?php echo base_url();?>js/datatables/media/assets/js/datatables.min.js"></script>
<script type="text/javascript"
	src="<?php echo base_url();?>js/datatables/extras/TableTools/media/js/TableTools.min.js"></script>
<script type="text/javascript"
	src="<?php echo base_url();?>js/datatables/extras/TableTools/media/js/ZeroClipboard.min.js"></script>



<!-- DATE RANGE PICKER -->
<script
	src="<?php echo base_url()?>js/bootstrap-daterangepicker/moment.min.js"></script>
<script
	src="<?php echo base_url()?>js/bootstrap-daterangepicker/daterangepicker.min.js"></script>
<!-- SLIMSCROLL -->
<script type="text/javascript"
	src="<?php echo base_url()?>js/jQuery-slimScroll-1.3.0/jquery.slimscroll.min.js"></script>
<script type="text/javascript"
	src="<?php echo base_url()?>js/jQuery-slimScroll-1.3.0/slimScrollHorizontal.min.js"></script>
<!-- BLOCK UI -->
<script type="text/javascript"
	src="<?php echo base_url()?>js/jQuery-BlockUI/jquery.blockUI.min.js"></script>
<!-- SPARKLINES -->
<script type="text/javascript"
	src="<?php echo base_url()?>js/sparklines/jquery.sparkline.min.js"></script>
<!-- EASY PIE CHART -->
<script
	src="<?php echo base_url()?>js/jquery-easing/jquery.easing.min.js"></script>
<script type="text/javascript"
	src="<?php echo base_url()?>js/easypiechart/jquery.easypiechart.min.js"></script>
<!-- FLOT CHARTS -->
<script src="<?php echo base_url()?>js/flot/jquery.flot.min.js"></script>
<script src="<?php echo base_url()?>js/flot/jquery.flot.time.min.js"></script>
<script
	src="<?php echo base_url()?>js/flot/jquery.flot.selection.min.js"></script>
<script src="<?php echo base_url()?>js/flot/jquery.flot.resize.min.js"></script>
<script src="<?php echo base_url()?>js/flot/jquery.flot.pie.min.js"></script>
<script src="<?php echo base_url()?>js/flot/jquery.flot.stack.min.js"></script>
<script
	src="<?php echo base_url()?>js/flot/jquery.flot.crosshair.min.js"></script>
<!-- TODO -->
<script type="text/javascript"
	src="<?php echo base_url()?>js/jquery-todo/js/paddystodolist.js"></script>
<!-- TIMEAGO -->
<script type="text/javascript"
	src="<?php echo base_url()?>js/timeago/jquery.timeago.min.js"></script>
<!-- FULL CALENDAR -->
<script type="text/javascript"
	src="<?php echo base_url()?>js/fullcalendar/fullcalendar.min.js"></script>
<!-- COOKIE -->
<script type="text/javascript"
	src="<?php echo base_url()?>js/jQuery-Cookie/jquery.cookie.min.js"></script>
<!-- GRITTER -->
<script type="text/javascript"
	src="<?php echo base_url()?>js/gritter/js/jquery.gritter.min.js"></script>
<!-- CUSTOM SCRIPT -->
<script src="<?php echo base_url()?>js/script.js"></script>
<script src="<?php echo base_url()?>js/googlemaps.js"></script>
<script>
		jQuery(document).ready(function() {		

			$('#datatable1').dataTable();
			App.setPage("creategroup");  //Set current page
			App.init(); //Initialise plugins and elements
		});
	</script>
<!-- /JAVASCRIPTS -->
