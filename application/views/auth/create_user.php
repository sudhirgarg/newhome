
<?php
$this->load->library('ion_auth');
$this->load->helper('url');
?>

<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left">Create User</h3>
</div>
<div class="description">

<?php echo lang('create_user_subheading');?>
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->


<!-- START   --!>


<div class="box border primary">
			<div class="box-title">
				<h4>
					<i class="fa fa-bars"></i>User Properties
				</h4>

			</div>
			<div class="box-body big">

<div id="infoMessage"><?php echo $message;?></div>

<?php echo form_open("auth/create_user");?>

      <p>
            <?php echo lang('create_user_fname_label', 'first_name');?> <br />
            <?php echo form_input($first_name);?>
      </p>

      <p>
            <?php echo lang('create_user_lname_label', 'last_name');?> <br />
            <?php echo form_input($last_name);?>
      </p>

      <p>
            <?php echo lang('create_user_company_label', 'company');?> <br />
            <?php echo form_input($company);?>
      </p>

      <p>
            <?php echo lang('create_user_email_label', 'email');?> <br />
            <?php echo form_input($email);?>
      </p>

      <p>
            <?php echo lang('create_user_phone_label', 'phone');?> <br />
            <?php echo form_input($phone);?>
      </p>

      <p>
            <?php echo lang('create_user_password_label', 'password');?> <br />
            <?php echo form_input($password);?>
      </p>

      <p>
            <?php echo lang('create_user_password_confirm_label', 'password_confirm');?> <br />
            <?php echo form_input($password_confirm);?>
      </p>


      <p><?php echo form_submit('submit', lang('create_user_submit_btn'));?></p>

<?php echo form_close();?>


</div>

</div>



<!-- END   --!>




<!-- /CONTENT -->



<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>
</div>
<!-- /CONTENT-->
</div>
</div>
</div>
</section>
<!--/PAGE -->
<!-- JAVASCRIPTS -->
<!-- Placed at the end of the document so the pages load faster -->
<!-- JQUERY -->
<script src="<?php echo base_url()?>js/jquery/jquery-2.0.3.min.js"></script>
<!-- JQUERY UI-->
<script
	src="<?php echo base_url()?>js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
<!-- BOOTSTRAP -->
<script
	src="<?php echo base_url()?>js/bootstrap-dist/js/bootstrap.min.js"></script>



<!-- DATE RANGE PICKER -->
<script
	src="<?php echo base_url()?>js/bootstrap-daterangepicker/moment.min.js"></script>
<script
	src="<?php echo base_url()?>js/bootstrap-daterangepicker/daterangepicker.min.js"></script>
<!-- SLIMSCROLL -->
<script type="text/javascript"
	src="<?php echo base_url()?>js/jQuery-slimScroll-1.3.0/jquery.slimscroll.min.js"></script>
<script type="text/javascript"
	src="<?php echo base_url()?>js/jQuery-slimScroll-1.3.0/slimScrollHorizontal.min.js"></script>
<!-- BLOCK UI -->
<script type="text/javascript"
	src="<?php echo base_url()?>js/jQuery-BlockUI/jquery.blockUI.min.js"></script>
<!-- SPARKLINES -->
<script type="text/javascript"
	src="<?php echo base_url()?>js/sparklines/jquery.sparkline.min.js"></script>
<!-- EASY PIE CHART -->
<script
	src="<?php echo base_url()?>js/jquery-easing/jquery.easing.min.js"></script>
<script type="text/javascript"
	src="<?php echo base_url()?>js/easypiechart/jquery.easypiechart.min.js"></script>
<!-- FLOT CHARTS -->
<script src="<?php echo base_url()?>js/flot/jquery.flot.min.js"></script>
<script src="<?php echo base_url()?>js/flot/jquery.flot.time.min.js"></script>
<script
	src="<?php echo base_url()?>js/flot/jquery.flot.selection.min.js"></script>
<script src="<?php echo base_url()?>js/flot/jquery.flot.resize.min.js"></script>
<script src="<?php echo base_url()?>js/flot/jquery.flot.pie.min.js"></script>
<script src="<?php echo base_url()?>js/flot/jquery.flot.stack.min.js"></script>
<script
	src="<?php echo base_url()?>js/flot/jquery.flot.crosshair.min.js"></script>
<!-- TODO -->
<script type="text/javascript"
	src="<?php echo base_url()?>js/jquery-todo/js/paddystodolist.js"></script>
<!-- TIMEAGO -->
<script type="text/javascript"
	src="<?php echo base_url()?>js/timeago/jquery.timeago.min.js"></script>
<!-- FULL CALENDAR -->
<script type="text/javascript"
	src="<?php echo base_url()?>js/fullcalendar/fullcalendar.min.js"></script>
<!-- COOKIE -->
<script type="text/javascript"
	src="<?php echo base_url()?>js/jQuery-Cookie/jquery.cookie.min.js"></script>
<!-- GRITTER -->
<script type="text/javascript"
	src="<?php echo base_url()?>js/gritter/js/jquery.gritter.min.js"></script>
<!-- CUSTOM SCRIPT -->
<script src="<?php echo base_url()?>js/script.js"></script>
<script src="<?php echo base_url()?>js/googlemaps.js"></script>
<script>
		jQuery(document).ready(function() {		

			App.setPage("create_user");  //Set current page
			App.init(); //Initialise plugins and elements
		});
	</script>
<!-- /JAVASCRIPTS -->

