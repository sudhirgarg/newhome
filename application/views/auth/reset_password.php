

<?php
$this->load->library ( 'ion_auth' );
$this->load->helper ( 'url' );

?>

<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left">Users</h3>
</div>
<div class="description">

	<a class="btn btn-default btn-lg"> <i class="fa fa-plus"> User</i>
	</a>

</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->



<!-- START   --!>

<h1><?php echo lang('reset_password_heading');?></h1>

<div id="infoMessage"><?php echo $message;?></div>

<?php echo form_open('auth/reset_password/' . $code);?>

	<p>
		<label for="new_password"><?php echo sprintf(lang('reset_password_new_password_label'), $min_password_length);?></label> <br />
		<?php echo form_input($new_password);?>
	</p>

	<p>
		<?php echo lang('reset_password_new_password_confirm_label', 'new_password_confirm');?> <br />
		<?php echo form_input($new_password_confirm);?>
	</p>

	<?php echo form_input($user_id);?>
	<?php echo form_hidden($csrf); ?>

	<p><?php echo form_submit('submit', lang('reset_password_submit_btn'));?></p>

<?php echo form_close();?>



<!-- END   --!>



<!-- /CONTENT -->



<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>
</div>
<!-- /CONTENT-->
</div>
</div>
</div>
</section>
<!--/PAGE -->
<!-- JAVASCRIPTS -->
<!-- Placed at the end of the document so the pages load faster -->
<!-- JQUERY -->
<script src="<?php echo base_url()?>js/jquery/jquery-2.0.3.min.js"></script>
<!-- JQUERY UI-->
<script
	src="<?php echo base_url()?>js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
<!-- BOOTSTRAP -->
<script
	src="<?php echo base_url()?>js/bootstrap-dist/js/bootstrap.min.js"></script>


<!-- DATA TABLES -->
<script type="text/javascript"
	src="<?php echo base_url();?>js/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript"
	src="<?php echo base_url();?>js/datatables/media/assets/js/datatables.min.js"></script>
<script type="text/javascript"
	src="<?php echo base_url();?>js/datatables/extras/TableTools/media/js/TableTools.min.js"></script>
<script type="text/javascript"
	src="<?php echo base_url();?>js/datatables/extras/TableTools/media/js/ZeroClipboard.min.js"></script>



<!-- DATE RANGE PICKER -->
<script
	src="<?php echo base_url()?>js/bootstrap-daterangepicker/moment.min.js"></script>
<script
	src="<?php echo base_url()?>js/bootstrap-daterangepicker/daterangepicker.min.js"></script>
<!-- SLIMSCROLL -->
<script type="text/javascript"
	src="<?php echo base_url()?>js/jQuery-slimScroll-1.3.0/jquery.slimscroll.min.js"></script>
<script type="text/javascript"
	src="<?php echo base_url()?>js/jQuery-slimScroll-1.3.0/slimScrollHorizontal.min.js"></script>
<!-- BLOCK UI -->
<script type="text/javascript"
	src="<?php echo base_url()?>js/jQuery-BlockUI/jquery.blockUI.min.js"></script>
<!-- SPARKLINES -->
<script type="text/javascript"
	src="<?php echo base_url()?>js/sparklines/jquery.sparkline.min.js"></script>
<!-- EASY PIE CHART -->
<script
	src="<?php echo base_url()?>js/jquery-easing/jquery.easing.min.js"></script>
<script type="text/javascript"
	src="<?php echo base_url()?>js/easypiechart/jquery.easypiechart.min.js"></script>
<!-- FLOT CHARTS -->
<script src="<?php echo base_url()?>js/flot/jquery.flot.min.js"></script>
<script src="<?php echo base_url()?>js/flot/jquery.flot.time.min.js"></script>
<script
	src="<?php echo base_url()?>js/flot/jquery.flot.selection.min.js"></script>
<script src="<?php echo base_url()?>js/flot/jquery.flot.resize.min.js"></script>
<script src="<?php echo base_url()?>js/flot/jquery.flot.pie.min.js"></script>
<script src="<?php echo base_url()?>js/flot/jquery.flot.stack.min.js"></script>
<script
	src="<?php echo base_url()?>js/flot/jquery.flot.crosshair.min.js"></script>
<!-- TODO -->
<script type="text/javascript"
	src="<?php echo base_url()?>js/jquery-todo/js/paddystodolist.js"></script>
<!-- TIMEAGO -->
<script type="text/javascript"
	src="<?php echo base_url()?>js/timeago/jquery.timeago.min.js"></script>
<!-- FULL CALENDAR -->
<script type="text/javascript"
	src="<?php echo base_url()?>js/fullcalendar/fullcalendar.min.js"></script>
<!-- COOKIE -->
<script type="text/javascript"
	src="<?php echo base_url()?>js/jQuery-Cookie/jquery.cookie.min.js"></script>
<!-- GRITTER -->
<script type="text/javascript"
	src="<?php echo base_url()?>js/gritter/js/jquery.gritter.min.js"></script>
<!-- CUSTOM SCRIPT -->
<script src="<?php echo base_url()?>js/script.js"></script>
<script src="<?php echo base_url()?>js/googlemaps.js"></script>
<script>
		jQuery(document).ready(function() {		

			$('#datatable1').dataTable();
			App.setPage("resetpassword");  //Set current page
			App.init(); //Initialise plugins and elements
		});
	</script>
<!-- /JAVASCRIPTS -->
