
<div class="modal-header">

</div>

<div class="modal-body">

	<div id="topicsFormChangeInUpdate"></div>
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

	
		<h4 class="modal-title">Update Quiz  </h4>
		
		<?php foreach ($generalInfo as $raw ){ ?>
			 
			 <input id="id" type="hidden" value="<?php echo $raw->id ?>">
			 <table>
			 	<tr>
			 		<td>
			 			Quiz Title :
			 		</td>
			 		<td>
			 			 <input id="topTitle" class="form-control" type="text" readonly="readonly" value="<?php echo $raw->gQname ?>">
			 		</td>
			 	</tr>
			 	<tr>
			 		<td>
			 			Time Duration :
			 		</td>
			 		<td>
			 			 <input id="timeDutation" class="form-control" type="text" value="<?php echo $raw->timeDutation ?>">
			 		</td>
			 	</tr>
			 	
			 </table>
			
			 
			
			 
		<?php }  ?>
	
		
</div>

<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal" id="closeButtonModal">Close</button>
		
	 <button type="button" class="btn btn-warning" id="updateButton" onclick="updateQuiz()"  data-loading-text="updating..."><i class="fa fa-square-o"></i> Update Quiz</button>
	
</div>
