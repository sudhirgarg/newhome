<!-- BEGIN FOOTER -->
                <p class="copyright"> 2017 &copy; Seamless Care
                    <!--<a target="_blank" href="http://keenthemes.com">Keenthemes</a> &nbsp;|&nbsp;
                    <a href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" title="Purchase Metronic just for 27$ and get lifetime updates for free" target="_blank">Purchase Metronic!</a>-->
                </p>
                <a href="#index" class="go2top">
                    <i class="icon-arrow-up"></i>
                </a>
                <!-- END FOOTER -->
            </div>
</div>
<!--/PAGE -->
<!-- JAVASCRIPTS -->
<!-- Placed at the end of the document so the pages load faster -->
<!-- JQUERY -->

<script src="<?php echo base_url(); ?>js/jquery/jquery-2.0.3.min.js"></script>
<script	src="<?php echo base_url(); ?>js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="<?php echo base_url(); ?>js/script.js"></script>
<script src=" <?php echo base_url(); ?>js/datatables/media/js/jquery.dataTables.min.js"></script>


<!-- New Design -->

<!-- BEGIN CORE PLUGINS -->
        <script src="<?php echo base_url(); ?>js_new/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/js.cookie.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?php echo base_url(); ?>js_new/moment.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/morris/morris.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/morris/raphael-min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/counterup/jquery.counterup.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/amcharts/amcharts/serial.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/amcharts/amcharts/pie.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/amcharts/amcharts/radar.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/amcharts/ammap/ammap.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/horizontal-timeline/horizontal-timeline.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/flot/jquery.flot.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/jquery.sparkline.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?php echo base_url(); ?>js_new/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?php echo base_url(); ?>js_new/dashboard.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>js_new/bootstrap-table/bootstrap-table.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>js_new/table-datatables-responsive.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="<?php echo base_url(); ?>js_new/layout.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js_new/quick-nav.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS --> 



<!-- New Design End -->







<script src="https://www.youtube.com/iframe_api"></script>

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular.js"></script>
<script src="<?php echo base_url().'js/eduAngular.js'?>" ></script>

<?php $this->load->view('edu/jsFunction/jsFunMain'); ?>

<script>	

    var watchedVideo	
  
	var player,
	time_update_interval = 0;
	
	function onYouTubeIframeAPIReady() {
		
		var video = $("#introVideo").val();
		
		player = new YT.Player('video-placeholder', {
		    width: '100%',
		    height: 400,
		    videoId: video,
		    playerVars: {
		        color: 'white',
		        autoplay: 1,
		        rel : 0
		    },
		    events: {
		        onReady: initialize
		    }
		});		 
	}
	

	function initialize(){

	    // Update the controls on load
	    updateTimerDisplay();
	    updateProgressBar();

	    // Clear any old interval.
	    clearInterval(time_update_interval);

	    // Start interval to update elapsed time display and
	    // the elapsed part of the progress bar every second.
	    time_update_interval = setInterval(function () {
	        updateTimerDisplay();
	        updateProgressBar();
	    }, 1000);


	    $('#volume-input').val(Math.round(player.getVolume()));

	    watchedVideo = setInterval(function(){ saveViewedVideoOnceHundredPersentageComplate() }, 1000);
	    
	   
	}

	function updateTimerDisplay(){
	    // Update current time text display.
	   $('#current-time').text(formatTime( player.getCurrentTime() ));
	   $('#duration').text(formatTime( player.getDuration() ));

	   var watchDuration = $("#current-time").html().replace(":", ".");
	   var totalDuration = $("#duration").html().replace(":", ".");
	   var watchPersentage = parseFloat(((parseFloat(watchDuration).toFixed(2) / parseFloat(totalDuration).toFixed(2)) * 100)).toFixed(2);  

	   //if(watchPersentage == 100) {  $('.loadNextVideo').click(); }
	   
	}
	

	function updateProgressBar(){
	    // Update the value of our progress bar accordingly.
	    $('#progress-bar').val((player.getCurrentTime() / player.getDuration()) * 100);
	}
	

	$('.loadNextVideo').on('click', function () {

		var video = $("#introVideo").val();

		if(video){
			
			var watchDuration = $("#current-time").html().replace(":", ".");
			var totalDuration = $("#duration").html().replace(":", ".");
			
	
			var watchPersentage = parseFloat(((parseFloat(watchDuration).toFixed(2) / parseFloat(totalDuration).toFixed(2)) * 100)).toFixed(2);  
				    
		    var watched = $(this).attr('data-watch-video');
		    var watch   = $("#watchedId").val();
			
			if((watchPersentage > 75 ) || (watch > 0 )){
			
		        var info  = $(this).html();
			    var url   = $(this).attr('data-video-id');  
			    var id    = $(this).attr('data-info-id');
			    var topic = $(this).attr('data-topic-id');
	
			    var currentVideoId = $("#introVideoId").val();
			    		    			        
			    $("#lessonTitle").html(info);	    
			    //player.cueVideoById(url);
			    //player.playVideo();
			    $("#alertInfo").html('');
	
			    $.post('<?php echo site_url("homePortalEdu/viewVideos");?>',{ 
				    id : currentVideoId, 
				    topic : topic, 
				    watchPersentage : watchPersentage  
				},function(data){
	                 if(data > 0){
	                	 window.location = "<?php echo base_url(); ?>/homePortalEdu/viewTutorial/"+topic+"/"+id;
	                 }				 
			    });
			    	    
			    
			}else {
							             
	             $("#alertInfo").html('<div class="alert alert-danger"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Allert!</strong> Please watch this video lesson first.</div>');
	             
			}

		}else{
			  var id    = $(this).attr('data-info-id');
			  var topic = $(this).attr('data-topic-id');
              window.location = "<?php echo base_url(); ?>/homePortalEdu/viewTutorial/"+topic+"/"+id;  
		}		
	    
	});

	

	function saveViewedVideoOnceHundredPersentageComplate(){
		
		var watchDuration = $("#current-time").html().replace(":", ".");
		var totalDuration = $("#duration").html().replace(":", ".");
		

		var watchPersentage = parseFloat(((parseFloat(watchDuration).toFixed(2) / parseFloat(totalDuration).toFixed(2)) * 100)).toFixed(2);

		if(watchPersentage > 95 ){			
	       
		    var topic = $("#topic").val();
		    var currentVideoId = $("#introVideoId").val();  		    			        
		  

		    $.post('<?php echo site_url("homePortalEdu/viewVideos");?>',{ 
			    id : currentVideoId, 
			    topic : topic, 
			    watchPersentage : watchPersentage  
			},function(data){
				 if(data > 0){
				   clearInterval(watchedVideo);	
				 }			 
		    });   	    
		    
		}  

	}
	
	
	
	function formatTime(time){
	    time = Math.round(time);

	    var minutes = Math.floor(time / 60),
	        seconds = time - minutes * 60;

	    seconds = seconds < 10 ? '0' + seconds : seconds;

	    return minutes + ":" + seconds;
	}
  
	
	function saveEmailCredential(){
		var med;
		var ticketToken;
		var commentToken;
		
		if($("#medChange").prop('checked') == true)  { med          = $("#medChange").val(); }
		if($("#ticket").prop('checked')    == true)  { ticketToken  = $("#ticket").val();    }
		if($("#comment").prop('checked')   == true)  { commentToken = $("#comment").val();   }
		
				
        $.post('<?php echo site_url("homePortalEdu/setEmailCredential") ?>',{
            userId     : $("#userIDEmail").val(),
            medChange  : med,
            ticket     : ticketToken,
            comment    : commentToken           
            
        },function(data){
        	              
            $("#credentialAlert").text(data);  
            setTimeout('autoRefresh1()', 1000);               
            
        });
                
	}
	
	
	function autoRefresh1(){
         window.location.reload();
    }
    
	function goBack() {
	    window.history.back();
	}

 $(document).ready(function()
            {
                $('#clickmewow').click(function()
                {
                    $('#radio1003').attr('checked', 'checked');
                });
            })
	
</script>


</body>
</html>
