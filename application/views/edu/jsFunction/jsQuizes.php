<script>

	jQuery(document).ready(function() {
		
		$("#checkTrue").toggle();			
		topicsCombo();
		booklist();
		getQuizNumber();
		
		$("#showTrueAndFalse").click(function(){
							 
			$("#checkTrue").toggle("show");	
			$(".checkBox").toggle("hide");
			
		});
		
	});
	
	function addRow(tableID) {
		var table = document.getElementById(tableID);
		var rowCount = table.rows.length;
		
		if(rowCount < 20){							
			document.getElementById("questionNumber").innerHTML = "Q" + (rowCount + 1);
			var row = table.insertRow(rowCount);
			var colCount = table.rows[0].cells.length;	
				
			for(var i=0; i<colCount; i++) {			
				var newcell = row.insertCell(i);
				newcell.innerHTML = table.rows[0].cells[i].innerHTML;			
			}
		}else{
			 alert("You can only add up to twenty questions!!.");
				   
		}
	}
	
	function deleteRow(tableID) {
		var table = document.getElementById(tableID);
		var rowCount = table.rows.length;
		for(var i=0; i<rowCount; i++) {
			var row = table.rows[i];
			var chkbox = row.cells[0].childNodes[0];
			if(null != chkbox && true == chkbox.checked) {
				if(rowCount <= 1) { 						// limit the user from removing all the fields
					alert("Cannot Remove all the Passenger.");
					break;
				}
				table.deleteRow(i);
				rowCount--;
				i--;
			}
		}
	}
	
	function topicsCombo(){
		$.get( "<?php echo site_url('homePortalEdu/getAllTopicsIntoComboBox'); ?>", function( data, status ) {
			// alert( status );
			  $( "#topicsCombo" ).html( data );
			  $( "#topicsComboEbook" ).html( data );
		 	
		});	
	}
	
	
	function getQuizNumber(){
				
		 $.post(
        	'<?php echo site_url('homePortalEdu/getQuizQuestionNumberBaseOnGeneralInfoId');?>',  {        		
	             generalQuizzesInfo : $("#generalQuizzesInfo").val()
             }, 
	        function (data, status) {                    		
				 $("#qNumber").html(data);     	           
	        	 
	        });
	        
	}
	
	
	function booklist(){
					        
	    $.get( "<?php echo site_url('homePortalEdu/getQuizzesTitleInList'); ?>", function( data, status ) {
			// alert( status );
			  $( "#listOfQuizze" ).html( data );
		 	
		});	
	}
	
	function addQuizInfo() {
			
        $("#addQuiz").button('loading');
        $.post(
        	'<?php echo site_url('homePortalEdu/addQuestionByJquery');?>',  {        		
	             questionTitle: $("#questionTitle").val(),
	             answerOne:     $("#answerOne").val(), 
	             answerTwo:     $("#answerTwo").val(),  
	             answerThree:   $("#answerThree").val(),  
	             answerFour:    $("#answerFour").val(),
	             correctOne:    $("#correctOne").is(':checked'),
	             correctTwo:    $("#correctTwo").is(':checked'),
	             correctThree:  $("#correctThree").is(':checked'),
	             correctFour:   $("#correctFour").is(':checked'),
	             generalQuizzesInfo : $("#generalQuizzesInfo").val(),
	             quizzesHint : $("#quizzesHint").val()
             }, 
	        function (data, status) {  
	          	
	            var pTags = $( "#topicsFormChange" ); 	
	         	
	         	if ( pTags.parent().is( "p" ) ) {	         		
				    $("#topicsFormChange").html(data);				    
				 } else {				   
	         		$("#topicsFormChange").html(data);	
	         		$("#topicsFormChange").wrap('<p class="alert alert-warning" role="alert" id="wrapData"> </p>');			
				}	         	           
	        	 $("#addQuiz").button('reset');      
	        	 $("#questionTitle, #answerOne, #answerTwo, #answerThree, #answerFour, #correctOne, #correctTwo, #correctThree, #correctFour, #quizzesHint").val(''); 
	        	 $("#correctOne, #correctTwo, #correctThree, #correctFour").prop( "checked", false ); 
	        	 getQuizNumber();
	        });
    }
    
    function getQuestionDuplicate(){
    	
    	
    }
    
   function updateQuiz() {
        $("#saveButtonTopics").button('loading');
        $.post(
        	'<?php echo site_url('homePortalEdu/updateGeneralInfoIndividually');?>',  {        		
	             id: $("#id").val(),
	             timeDutation: $("#timeDutation").val()
	             
             }, 
	        function (data, status) {  
	          	
	            var pTags = $( "#topicsFormChangeInUpdate" ); 	
	         	
	         	if ( pTags.parent().is( "p" ) ) {	         		
				    $("#topicsFormChangeInUpdate").html(data);				    
				 } else {				   
	         		$("#topicsFormChangeInUpdate").html(data);	
	         		$("#topicsFormChangeInUpdate").wrap('<p class="alert alert-success" role="alert" id="wrapData"> </p>');			
				}	         	           
	        	  
	        	 setTimeout(function() {
               		 location.reload();
                 }, 1500);      
	        
	        });
    }
    
     function deleteQuestion() {
        $("#saveButtonTopics").button('loading');
        $.post(
        	'<?php echo site_url('homePortalEdu/deleteQuestionAndAnswerIndividually');?>',  {        		
	             id: $("#qId").val()	            
	             
             }, 
	        function (data, status) {  
	          	
	            var pTags = $( "#topicsFormChangeInUpdate" ); 	
	         	
	         	if ( pTags.parent().is( "p" ) ) {	         		
				    $("#topicsFormChangeInUpdate").html(data);				    
				 } else {				   
	         		$("#topicsFormChangeInUpdate").html(data);	
	         		$("#topicsFormChangeInUpdate").wrap('<p class="alert alert-success" role="alert" id="wrapData"> </p>');			
				}	         	           
	        	  
	        	 window.history.back();     
	        
	        });
    }
    
    function goBack() {             	           
	        	  
	       window.history.back();         
	        
    }
    
     function deleteQuizConfirm() {
        $("#deleteButton").button('loading');
        $.post(
        	'<?php echo site_url('homePortalEdu/deleteQuizConfirm');?>',  {        		
	             id: $("#id").val()	            
	             
             }, 
	        function (data, status) {  
	          	
	            var pTags = $( "#topicsFormChangeInDelete" ); 	
	         	
	         	if ( pTags.parent().is( "p" ) ) {	         		
				    $("#topicsFormChangeInDelete").html(data);				    
				 } else {				   
	         		$("#topicsFormChangeInDelete").html(data);	
	         		$("#topicsFormChangeInDelete").wrap('<p class="alert alert-success" role="alert" id="wrapData"> </p>');			
				}	         	           
	        	  
	        	 setTimeout(function() {
               		 location.reload();
                 }, 1500);   
	        
	        });
    }
	
</script>
