<script>
	
	jQuery(document).ready(function() {				
		topicsCombo();	
		//booklist();
		//videolist();
		topicsBox();
		$('#topicTable').dataTable(); 
		$('#topicTableTech').dataTable();
								
	});

	function topicsCombo(){
		$.get( "<?php echo site_url('homePortalEdu/getAllTopicsIntoComboBox'); ?>", function( data, status ) {
			// alert( status );
			  $( "#topicsCombo" ).html( data );
			  $( "#topicsComboEbook" ).html( data );
		 	
		});	
	}
	
	function topicsBox(){
		$.get( "<?php echo site_url('homePortalEdu/getTopicListInBox'); ?>", function( data, status ) {
			// alert( status );
			  $("#listOfBook").html(data);  
	          $("#listOfBook1").html(data);
		 	
		});	
	}
	
	function booklist(){
		$.post(
        	'<?php echo site_url('homePortalEdu/getBookAndVideoTitleInList');?>',  { 	
	            
	             typeInfo: 2	             
             }, 
	        function (data, status) {  	          	           	         	           
	        	 $("#listOfvideo").html(data);  
	        	 $("#listOfvideo1").html(data);
	        });
	}
	
	function videolist(){
		$.post(
        	'<?php echo site_url('homePortalEdu/getBookAndVideoTitleInList');?>',  { 	
	            
	             typeInfo: 1	             
             }, 
	        function (data, status) {  	          	           	         	           
	        	 $("#listOfBook").html(data);  
	        	 $("#listOfBook1").html(data);
	        });
	}
	
	function saveEbookTutorial() {
			
        $("#saveButton").button('loading');
        $.post(
        	'<?php echo site_url('homePortalEdu/saveEbookTutorial');?>',  {        		
	            
	             ebookTitle: $("#ebookTitle").val(), 
	             topicsId: $("#topicsId").val(),  
	             ebookImage: $("#ebookImage").val(),  
	             description: $("#description").val()
             }, 
	        function (data, status) {  
	          	
	            var pTags = $( "#formChange" ); 	
	         	
	         	if ( pTags.parent().is( "p" ) ) {	         		
				    $("#formChange").html(data);				    
				 } else {				   
	         		$("#formChange").html(data);	
	         		$("#formChange").wrap('<p class="alert alert-warning" role="alert" id="wrapData"> </p>');			
				}	         	           
	        	 $("#saveButton").button('reset');      
	        	 $("#ebookTitle, #ebookImage, #description").val('');  
	        });
    }
    
    function saveTutorialTopics() {
        $("#saveButtonTopics").button('loading');
        $.post(
        	'<?php echo site_url('homePortalEdu/saveTutorialTopics');?>',  {        		
	            
	             tutorialTitle: $("#tutorialTitle").val()
	             
             }, 
	        function (data, status) {  
	          	
	            var pTags = $( "#topicsFormChange" ); 	
	         	
	         	if ( pTags.parent().is( "p" ) ) {	         		
				    $("#topicsFormChange").html(data);				    
				 } else {				   
	         		$("#topicsFormChange").html(data);	
	         		$("#topicsFormChange").wrap('<p class="alert alert-warning" role="alert" id="wrapData"> </p>');			
				}	         	           
	        	 $("#saveButtonTopics").button('reset'); 
	        	 $("#tutorialTitle").val(''); 
	        	 topicsCombo(); 
	        	 
	        	 setTimeout(function() {
               		 location.reload();
                 }, 1000);     
	        
	        });
    }
    
    function updateTutorialTopics() {
        $("#saveButtonTopics").button('loading');
        $.post(
        	'<?php echo site_url('homePortalEdu/editTopicConfirm');?>',  {        		
	             id: $("#tId").val(),
	             tutorialTitle: $("#topTitle").val()
	             
             }, 
	        function (data, status) {  
	          	
	            var pTags = $( "#topicsFormChangeInUpdate" ); 	
	         	
	         	if ( pTags.parent().is( "p" ) ) {	         		
				    $("#topicsFormChangeInUpdate").html(data);				    
				 } else {				   
	         		$("#topicsFormChangeInUpdate").html(data);	
	         		$("#topicsFormChangeInUpdate").wrap('<p class="alert alert-success" role="alert" id="wrapData"> </p>');			
				}	         	           
	        	  
	        	 setTimeout(function() {
               		 location.reload();
                 }, 1500);      
	        
	        });
    }
    
    function saveVideoTutorial() {
        $("#saveButtonVideo").button('loading');
        $.post(
        	'<?php echo site_url('homePortalEdu/saveVideoTutorial');?>',  {        		
	            
	             videoTitle: $("#videoTitle").val(), 
	             topicsId: $("#topicsId").val(),  
	             videoFile: $("#videoFile").val(),  
	             videoDis: $("#videoDis").val(),
	             typeInfo: $("#typeInfo").val()
             }, 
	        function (data, status) {  
	          	
	            var pTags = $( "#formChangeInVideoPanel" ); 	
	         	
	         	if ( pTags.parent().is( "p" ) ) {	         		
				    $("#formChangeInVideoPanel").html(data);				    
				 } else {				   
	         		$("#formChangeInVideoPanel").html(data);	
	         		$("#formChangeInVideoPanel").wrap('<p class="alert alert-warning" role="alert" id="wrapData"> </p>');			
				}	         	           
	        	 $("#saveButtonVideo").button('reset');   
	        	 $("#videoTitle, #videoDis, #videoFile").val('');            
	        
	        });
    }
    
    function saveTutorialTopicsInVideoPanel() {
        $("#saveButton").button('loading');
        $.post(
        	'<?php echo site_url('homePortalEdu/saveTutorialTopics');?>',  {        		
	            
	             tutorialTitle: $("#topicVideo").val()
	             
             }, 
	        function (data, status) {  
	          	
	            var pTags = $( "#topicsFormChangeInVideoPanel" ); 	
	         	
	         	if ( pTags.parent().is( "p" ) ) {	         		
				    $("#topicsFormChangeInVideoPanel").html(data);				    
				 } else {				   
	         		$("#topicsFormChangeInVideoPanel").html(data);	
	         		$("#topicsFormChangeInVideoPanel").wrap('<p class="alert alert-warning" role="alert" id="wrapData"> </p>');			
				}	         	           
	        	 $("#saveButton").button('reset');      
	        	 $("#topicVideo").val('');	
	        	 topicsCombo();        	     	 
                 
	        });
    }
    
     function deleteTopicConfirm() {
        $("#deleteButton").button('loading');
        $.post('<?php echo site_url('homePortalEdu/deleteTopicConfirm');?>', 
                {
            	id: $("#id").val()
                }, 
        function (data, status) { 
        	
        	 var pTags = $( "#topicsFormChangeInDelete" ); 	
	         	
         	if ( pTags.parent().is( "p" ) ) {	         		
			    $("#topicsFormChangeInDelete").html(data);				    
			 } else {				   
         		$("#topicsFormChangeInDelete").html(data);	
         		$("#topicsFormChangeInDelete").wrap('<p class="alert alert-success" role="alert" id="wrapData"> </p>');			
			}
				          
            setTimeout(function() {
                location.reload();
                }, 1500);           
            
        });
    }    
    
    
    function deleteLessonConfirm() {
        $("#deleteButton").button('loading');
        $.post('<?php echo site_url('homePortalEdu/deleteLessonConfirm');?>', 
                {
            	id: $("#id").val()
                }, 
        function (data, status) { 
        	
        	 var pTags = $( "#topicsFormChangeInDelete" ); 	
	         	
         	if ( pTags.parent().is( "p" ) ) {	         		
			    $("#topicsFormChangeInDelete").html(data);				    
			 } else {				   
         		$("#topicsFormChangeInDelete").html(data);	
         		$("#topicsFormChangeInDelete").wrap('<p class="alert alert-success" role="alert" id="wrapData"> </p>');			
			}
				          
            setTimeout(function() {
                location.reload();
                }, 1500);           
            
        });
    }
    		
		
</script>


