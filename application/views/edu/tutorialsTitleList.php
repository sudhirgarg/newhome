

<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left">Tutorials</h3>
	
</div>
<div class="description">
	<a class="btn btn-lg btn-default" href="<?php echo base_url();?>homePortalEdu/tutorials"><img src="<?php echo base_url();?>img/Arrow-Back-icon.png"  width="24px" > Go Back </a> 
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DELIVERY RUNS CONTENT -->
<!-- DATA TABLES -->

<!--***************************************** PAGE BODY STARTS ***************************************************************-->

<div class="row">
	<div class="col-md-8">
		
			
		<div class="panel panel-info">
			
	    <div class="panel-heading">
	        <h3 class="panel-title"> 
	        	<!--a href="<?php echo base_url();?>homePortalEdu/tutorials"><img src="<?php echo base_url();?>img/Arrow-Back-icon.png"  width="24px" ></a--> 
	        	<?php foreach ($topicName AS $info1) { echo ucfirst($info1->topicsName);   }?> 
	        </h3>
	    </div>
	    <div class="panel-body">
	    	
	    	<div class="bs-example">
			    <div class="list-group">
			    <?php 
			    	$i = 1;
					$image = "";
			    	foreach ($tutorialsList AS $info) {			    		
			    		if($info->typeInfo == 1) { $image = "Holiday-Diary-Book-icon.png"; } 
						if($info->typeInfo == 2) { $image = "Fish-Movie-icon.png"; } 
			        	echo '<a href="'.base_url().'homePortalEdu/viewTutorial/'.$info->id.'" class="list-group-item ">
			           		 <span class="badge pull-left badge-info">'.$i++.' </span>&nbsp;&nbsp;' .ucfirst($info->name).' <img src="'.base_url().'img/'.$image.'"  width="18px" >
			      		 </a>';			        
			        	
					} 
					
				?>
				
			    </div>
			</div>
		
	
		</div>
		</div>
	</div>
</div>



<!--***************************************** PAGE BODY ENDS ***************************************************************-->


<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>