<?php $ci = &get_instance(); ?>

<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<!--<div class="clearfix">
	<h3 class="content-title pull-left">Education Portal</h3>
</div>
<div class="description">
	
</div>
</div>
</div>
</div>



<div class="row">
    <div class="col-md-3"> </div>
    
	<div class="col-md-6">
		
		<div class="panel panel-info">
		    
		    <div class="panel-body">		    
		         <div class="embed-responsive embed-responsive-16by9">
			       
			       	<iframe class="embed-responsive-item" width="100%" height="360" src="https://www.youtube.com/embed/aETuNXNcRG0?rel=0&amp;controls=0&amp;showinfo=0;autoplay=1" frameborder="0" allowfullscreen></iframe> 
			     </div>		       		
			</div>
			
		</div>
		
	</div>
	
	<div class="col-md-3"> </div>
</div>

<div class="row">
    <div class="col-md-7"> 
         
         <div class="panel panel-info">
		
		    <div class="panel-heading">
	              Courses
	        </div>
		    
		    <div class="panel-body">
		    
		      <div class="table-responsive">
		      
				  <table class="table table-hover">
				  
				    <thead>
					      <tr>
					        <th width="5%">#</th>
					        <th>Details</th>
					        <th width="10%">Status</th>
					      </tr>
				    </thead>
				    <tbody>
				         <?php 
				           
				             if(!empty($topicsList)){
				             	$i = 1;
				             	foreach($topicsList AS $record){
				             	     echo '<tr>
								        <td>'.$i++.'</td>
								        <td>'.$record -> topicsName.'</td>
								        <td>';
				             	           $ci -> getCourseStatus($record->topicsId);		                                     
		                                echo '</td>
								      </tr>';
				             	}
				             	
				             }
				         
				         ?>
					     
				    </tbody>				    
				  </table>
				  
				 
				  
			  </div>
		       		
			</div>
		</div>
    
    </div>
    
	<div class="col-md-5">
		
		<div class="panel panel-info">
		
		    <div class="panel-heading">
	              List of score
	        </div>
		    
		    <div class="panel-body">
		          <?php 
		          $get = "";
			          foreach($scoreDetails AS $row)  {
		  		 			$a = ceil(round($row->score));
		  		 			if($a >= 70 ){
		  		 				$get = "success";
		  		 			}elseif(($a >= 50) && ($a <= 59)){
		  		 				$get ="warning";
		  		 			}elseif($a <= 49 ){
		  		 				$get ="danger";
		  		 			}
							
							echo '<div class="bs-example">
				    			Quiz Title: '.ucwords($row->gQname).' ( Attempt : '.$row->attempt.' ,  Score: '.round($row->score).' )
								<div class="progress progress-striped active ">
									 <div class="progress-bar progress-bar-'.$get.'" style="width: '.round($row->score).'%;"> <span class="sr-only">'.round($row->score).'% Complete</span></div>
								</div>
							</div>';			
	  			 	 }  
  		 	    ?>
		       		
			</div>
		</div>
		
	</div>
	
</div>-->



<!--***************************************** PAGE BODY ENDS ***************************************************************-->







<div class="container-fluid">
                <div class="page-content">
                    <!-- BEGIN BREADCRUMBS -->
                    <div class="breadcrumbs">
                        <h1>Education Portal</h1>
                        <ol class="breadcrumb">
                            <li>
                                <a href="#">Home</a>
                            </li>
                            <li class="active">Education Portal</li>
                        </ol>
                    </div>
                    <!-- END BREADCRUMBS -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    
                    <div class="row">
                        <div class="col-lg-12 col-xs-12 col-sm-12">
                            <div class="portlet light bordered">
                                

                                <div class="portlet-body">
					<iframe class="embed-responsive-item" width="100%" height="360" src="https://www.youtube.com/embed/aETuNXNcRG0?rel=0&amp;controls=0&amp;showinfo=0;autoplay=1" frameborder="0" allowfullscreen></iframe> 
                                    <!--<div id="dashboard_amchart_1" class="CSSAnimationChart"></div>-->
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                        <div class="portlet light bordered">
                                            <div class="portlet-title">
                                                <div class="caption font-dark">
                                                    <i class="icon-settings font-dark"></i>
                                                    <span class="caption-subject bold uppercase">Courses</span>
                                                </div>
                                                <div class="tools"> </div>
                                            </div>
                                            <div class="portlet-body">
                                                <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                                                    <thead>
                                                        <tr>
                                                            <th class="all">#</th>
                                                            <th class="min-phone-l">Details</th>
                                                            
                                                            <th class="all">Status</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
							<?php 
				           
				             if(!empty($topicsList)){
				             	$i = 1;
				             	foreach($topicsList AS $record){ ?>
                                                        <tr>
                                                            <td><?php echo $i; ?></td>
                                                            <td><?php echo $record -> topicsName; ?></td>
                                                            
                                                            <td>
								<?php echo $ci -> getCourseStatus($record->topicsId);				
								 ?>
                                                                
                                                            </td>
                                                        </tr>
                                                      <?php $i++; }} ?>  
                                                        
                                                       
                                                       
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- END EXAMPLE TABLE PORTLET-->
                                    </div>




				<div class="col-md-6">
                                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                        <div class="portlet light bordered">
                                            <div class="portlet-title">
                                                <div class="caption font-dark">
                                                    <i class="icon-settings font-dark"></i>
                                                    <span class="caption-subject bold uppercase">List Of Score</span>
                                                </div>
                                                <div class="tools"> </div>
                                            </div>
                                            <div class="portlet-body">
                                                <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                                                    <thead>
                                                        <tr>
                                                            <th class="all">Quiz Title</th>
                                                            <th class="min-phone-l">Progress</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
							<?php 
		          $get = "";
			          foreach($scoreDetails AS $row)  {
		  		 			$a = ceil(round($row->score));
		  		 			if($a >= 70 ){
		  		 				$get = "success";
		  		 			}elseif(($a >= 50) && ($a <= 59)){
		  		 				$get ="warning";
		  		 			}elseif($a <= 49 ){
		  		 				$get ="danger";
		  		 			}
							?>
                                                        <tr>
                                                            <td><?php echo ucwords($row->gQname).' ( Attempt : '.$row->attempt.' ,  Score: '.round($row->score).' )'; ?></td>
                                                            <td><?php echo round($row->score).'% Complete'; ?></td>
                                                            
                                                            
                                                        </tr>
                                                        <?php } ?>
                                                        
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- END EXAMPLE TABLE PORTLET-->
                                    </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
