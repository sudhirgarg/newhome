

<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left">Tutorials </h3>
</div>
<div class="description">
	
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DELIVERY RUNS CONTENT -->
<!-- DATA TABLES -->

<!--***************************************** PAGE BODY STARTS ***************************************************************-->

<div class="row">
	<div class="col-md-8">
		
	<div class="panel panel-info">
    <div class="panel-heading">
    <?php foreach ($dataTutorial as $row)	{  ?>	
    	
        <h3 class="panel-title"><?php echo ucfirst($row->name); ?></h3>
    </div>
    <div class="panel-body" style="text-align:justify; ">	
    	
		<?php 
			echo '<div class="thumbnail">';
			$info = new SplFileInfo($row->images);
            $imgArray =  $info->getExtension() ;
			if(!empty($imgArray)){
		    	echo '<img src="'.base_url().'img/edu/'.$row->images.'" width="500">';
			}
			echo '<div class="caption">';
			echo $row->details; 
			echo '</div></div>';
		} ?>
	
	</div>
	</div>
	
	</div>
	
</div>


<!--***************************************** PAGE BODY ENDS ***************************************************************-->


<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>