

<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left">Tutorials</h3>
</div>
<div class="description">
	
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DELIVERY RUNS CONTENT -->
<!-- DATA TABLES -->

<!--***************************************** PAGE BODY STARTS ***************************************************************-->

<div class="row">
	<div class="col-md-6">
		
		<div class="panel panel-info">
	    <div class="panel-heading">
	        <h3 class="panel-title">Quiz Titiles</h3>
	    </div>
	    <div class="panel-body">
	    	
	    	<div class="bs-example">
			    <div class="list-group">
			    <?php 
			    	
			    	foreach ($generalInfoList AS $info) {
			        	echo '<a href="'.base_url().'homePortalEdu/listOfQuizzesBaseOnTopic/'.$info->topicsId.'" class="list-group-item ">
			           		 <i class="fa fa-book"></i> '.ucfirst($info->topicsName).' <span class="badge badge-info">'.$info->totailQuzzes.'</span>
			      		 </a>';			        
			        
					} 
					
				?>
			    </div>
			</div>
		
	
		</div>
		</div>

	</div>
	
	<div class="col-md-6">
		
		<div class="panel panel-info">
 		 <div class="panel-heading">
   			 <h3 class="panel-title">Your Activities </h3>
  		 </div>
  		 <div class="panel-body">
  		 	<?php  foreach($scoreDetails AS $row)  {
  		 			$a = ceil(round($row->score));
  		 			if($a >= 60 ){
  		 				$get = "success";
  		 			}elseif(($a >= 50) && ($a <= 59)){
  		 				$get ="warning";
  		 			}elseif($a <= 49 ){
  		 				$get ="danger";
  		 			}
					
					echo '<div class="bs-example">
		    			Quiz Title: '.ucwords($row->gQname).' ( Attempt : '.$row->attempt.' ,  Score: '.round($row->score).' )
						<div class="progress progress-striped active ">
							 <div class="progress-bar progress-bar-'.$get.'" style="width: '.round($row->score).'%;"> <span class="sr-only">'.round($row->score).'% Complete</span></div>
						</div>
					</div>';			
  			 	}  
  		 	?>
    		
 		 </div>
 		  
   		 </div>
		
	</div>
	
</div>


<!--***************************************** PAGE BODY ENDS ***************************************************************-->


<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>