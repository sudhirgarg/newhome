
<!DOCTYPE html>
<html lang="en">
<head>
	
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title> <?php if($this->uri->segment(2) == "NULL") { echo "Home"; } else { echo $this->uri->segment(2); } ?> </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/cloud-admin.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/themes/default.css" id="skin-switcher">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">		

   <link rel="stylesheet" href = "http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">	
   <style>
       
        
        /* Print styling */

			@media print {
			
				[class*="col-sm-"] {
					float: left;
				}
				
				[class*="col-xs-"] {
					float: left;
				}
				
				.col-sm-12, .col-xs-12 { 
					width:100% !important;
				}
				
				.col-sm-11, .col-xs-11 { 
					width:91.66666667% !important;
				}
				
				.col-sm-10, .col-xs-10 { 
					width:83.33333333% !important;
				}
				
				.col-sm-9, .col-xs-9 { 
					width:75% !important;
				}
				
				.col-sm-8, .col-xs-8 { 
					width:66.66666667% !important;
				}
				
				.col-sm-7, .col-xs-7 { 
					width:58.33333333% !important;
				}
				
				.col-sm-6, .col-xs-6 { 
					width:50% !important;
				}
				
				.col-sm-5, .col-xs-5 { 
					width:41.66666667% !important;
				}
				
				.col-sm-4, .col-xs-4 { 
					width:33.33333333% !important;
				}
				
				.col-sm-3, .col-xs-3 { 
					width:25% !important;
				}
				
				.col-sm-2, .col-xs-2 { 
					width:16.66666667% !important;
				}
				
				.col-sm-1, .col-xs-1 { 
					width:8.33333333% !important;
				}
				  
				.col-sm-1,
				.col-sm-2,
				.col-sm-3,
				.col-sm-4,
				.col-sm-5,
				.col-sm-6,
				.col-sm-7,
				.col-sm-8,
				.col-sm-9,
				.col-sm-10,
				.col-sm-11,
				.col-sm-12,
				.col-xs-1,
				.col-xs-2,
				.col-xs-3,
				.col-xs-4,
				.col-xs-5,
				.col-xs-6,
				.col-xs-7,
				.col-xs-8,
				.col-xs-9,
				.col-xs-10,
				.col-xs-11,
				.col-xs-12 {
				float: left !important;
				}
				
				body {
					margin: 0;
					padding 0 !important;
					min-width: 768px;
				}
				
				.container {
					width: auto;
					min-width: 750px;
				}
				
				body {
					font-size: 10px;
				}
				
				a[href]:after {
					content: none;
				}
				
				.noprint, 
				div.alert, 
				header, 
				.group-media, 
				.btn, 
				.footer, 
				form, 
				#comments, 
				.nav, 
				ul.links.list-inline,
				ul.action-links {
					
				}			
			}
			
			 .page {
		        width: 210mm;
		        min-height: 297mm;
		        margin: 0mm auto;
		        border-radius: 5px;
		        background: white;
		        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
		    }
		    .subpage {
		        padding: 1cm;
		        height: 257mm;
		    }
		    
		   

   </style>
</head>
<body onLoad="myPrint();">
  

<!--***************************************** PAGE BODY STARTS ***************************************************************-->
<div class="book">




<div class="page">
   <div class="subpage">
      <div class="row">
      
        <div class="col-md-6">
        
          <div class="panel panel-info">
		      <div class="panel-body">
		          <?php
		                echo 'Subject   : '.$info -> gQname.'<br/>';
		                echo 'Fullname  : '.$info -> first_name.' '.$info -> last_name.'<br/>';
		                echo 'Attempted : '.$info -> attempt.'<br/>';
		                echo 'Score     : '.round($info -> score).'<br/>';
		          ?>
		       </div>
		  </div>
      
         <div class="panel panel-info">
	     <div class="panel-body">
           <?php
		    	 
		    		$i = 1;
		    		foreach($quiz AS $raw){
		    			
		    			$raw->question;
						$answers = explode(";", $raw->quizzesAnswer);
						$answers = array_filter($answers);
						shuffle($answers);						
						$candidateAnswers = explode(";", $raw->answer);
						$countAnswer = count($answers);
						$countCandidateAnswers = count($candidateAnswers);						
						
						$A1 = "";
					
						if($raw->answerType == 3) {
							$inputType = 'checkbox';	         										
							
						}else{
							$inputType = 'radio';
							for($c= 0; $c < $countCandidateAnswers; $c++){
	    			             $A1 = $candidateAnswers[$c];			
	    		         	}
						}
						
						$a = 0;
						$c = 0;
						
		    								
		    		echo '<ul class="list-unstyled">
						<li>
						  '.$i++.') '.$raw->question.'<br/>   		
						</li>';
						for($a=0; $a < $countAnswer; $a++){
							if($raw->answerType == 3){
								
								 echo '<li><input type="'.$inputType.'" name="answer[]" id="answer[]"';
								  if(in_array($answers[$a],$candidateAnswers)) { echo ' checked="checked"'; } 
								 echo 'value="'.$answers[$a].'">'.$answers[$a].'</li>';	
								
							}else{
								 echo '<li><input type="'.$inputType.'" name="answer'.$i.'" id="answer[]"';								   								  
									  if($answers[$a] == $A1) { echo ' checked="checked"'; }  	
								 echo ' value="'.$answers[$a].'">'.$answers[$a].'</li>';	
							}			
						}					
					echo'<li>
	        					<div class="alert alert-success">
								  <strong>Right Answer:</strong> '.$raw->correctAnswer.'
								</div>
	        		  </li>';
								
		    		echo '</ul>';	
		    		
				}		    	
				
		    	
		   ?>
		   </div>
        </div>	
    </div>      

        
		</div>
    </div>
</div>





<!--***************************************** PAGE BODY ENDS ***************************************************************-->

<script>
    function myPrint() {
        window.print();
    }
        
</script>

</body>
</html>