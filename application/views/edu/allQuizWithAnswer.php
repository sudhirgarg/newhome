 <?php
    	$id = "";
   		foreach ($info as $raw) {
   			$id = $raw->topicsId;
			//echo "Name   :". $raw->gQname ."<br/>"; 
			//echo "Details: ". $raw->details ."<br/>";
			//echo "Time Duration : ". $raw->timeDutation;  
			//echo "EDIT";
		}   
		
		$courseName = "";
		foreach($topicsInfo AS $tInfo){
			$courseName = $tInfo->topicsName;
		} 	   
   ?> 

<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left">Questions and Answers</h3>
</div>
<div class="description">
	<?php 				 
			echo '<a  class="btn btn-lg btn-default"  href="'.base_url().'homePortalEdu/listOfQuizzesBaseOnCompletedTopicTech/'.$id.'"><img src="'.base_url().'img/Arrow-Back-icon.png"  width="24px" >Go Back</a>';	
		
	 ?>
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DELIVERY RUNS CONTENT -->
<!-- DATA TABLES -->

<!--***************************************** PAGE BODY STARTS ***************************************************************-->

<div class="row">
	<div class="col-md-8">
		<div class="panel panel-info">			
	    <div class="panel-heading">
	        <h3 class="panel-title"><?php echo ucfirst($courseName); ?></h3>
	    </div>
	    
	     <div class="panel-body">	   	   
		    <div class="panel panel-info my">
			  <div class="panel-body">
			    <?php
			    	$id = "";
	    	   		foreach ($info as $row) {
	    	   			$id = $row->id;
	    	   			echo'
	    	   				<table>
	    	   				<tr>
	    	   					<td align="right">Title Of Quiz   : </td>
	    	   					<td>'.ucfirst($row->gQname).' </td>
							</tr>
							<tr>
	    	   					<td align="right">Details:  </td>
	    	   					<td>'.$row->details.'</td>
							</tr>
							<tr>
	    	   					<td align="right">Time Duration : </td>
	    	   					<td>'.$row->timeDutation.' </td>
							</tr>
							
							</table>
	    	   			
	    	   			
	    	   			';
						//echo "Name   : ". ucfirst($row->gQname)."<br/>"; 
						//echo "Details: ". $row->details ."<br/>";
						//echo "Time Duration : ". $row->timeDutation;  
						echo '<button type="button" class="btn btn-warning" onclick="loadModal(\''.base_url().'homePortalEdu/editGeneralInfoIndividually/'.$row->id.'\')" >									
						<i class="fa fa-square-o"></i> Edit 
						</button>';
					}    	   
		       ?> 
			  </div>
			</div>
       
	    	 <div class="panel panel-info">
			  <div class="panel-body">
		    	<?php
		    		$i = 1;
		    		foreach($quiz AS $raw){
		    			
		    			$raw->question;
						$answers = explode(";", $raw->quizzesAnswer);
						$answers = array_filter($answers);
						shuffle($answers);						
						$correctAnswers = explode(";", $raw->correctAnswer);
						$countAnswer = count($answers);
						$countCorrectAnswers = count($correctAnswers);						
						
						$A1 = "";
					
						if($raw->answerType == 3) {
							$inputType = 'checkbox';	         										
							
						}else{
							$inputType = 'radio';
							for($c= 0; $c < $countCorrectAnswers; $c++){
	    			             $A1 = $correctAnswers[$c];			
	    		         	}
						}
						
						$a = 0;
						$c = 0;
						
		    								
		    		echo '<ul class="list-unstyled">
						<li>
						  '.$i++.') '.$raw->question.'
						</li>';
						for($a=0; $a < $countAnswer; $a++){
							if($raw->answerType == 3){
								
								 echo '<li><input type="'.$inputType.'" name="answer[]" id="answer[]"';
								  if(in_array($answers[$a],$correctAnswers)) { echo ' checked="checked"'; } 
								 echo 'value="'.$answers[$a].'">'.$answers[$a].'</li>';	
								
							}else{
								 echo '<li><input type="'.$inputType.'" name="answer'.$i.'" id="answer[]"';								   								  
									  if($answers[$a] == $A1) { echo ' checked="checked"'; }  	
								 echo ' value="'.$answers[$a].'">'.$answers[$a].'</li>';	
							}			
						}					
					echo'<li>
						 <span class="label label-info">Hint :</span>  '.$raw->quizzesHint.'
						</li>';
								
		    		echo '</ul>';
					
					echo '<a href="'.base_url().'homePortalEdu/editQuizIndividually/'.$raw->id.'"><button type="button" class="btn btn-danger" 									
						<i class="fa fa-square-o"></i> Delete 
						</button><br/></a><br/>';	
		    		
				}		    	
		    	
		    	?>		    		
		    											    	
	    	</div>
			</div>		
		</div>
		</div>
	</div>
</div>



<!--***************************************** PAGE BODY ENDS ***************************************************************-->


<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>