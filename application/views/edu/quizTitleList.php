

<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left">Uncomplete Quizzes</h3>
</div>
<div class="description">
	<a class="btn btn-lg btn-default" href="<?php echo base_url();?>homePortalEdu/updateQuizzes"><img src="<?php echo base_url();?>img/Arrow-Back-icon.png"  width="24px" > Go Back </a> 
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DELIVERY RUNS CONTENT -->
<!-- DATA TABLES -->

<!--***************************************** PAGE BODY STARTS ***************************************************************-->

<div class="row">
	<div class="col-md-6">
		
			
		<div class="panel panel-info">
			
	    <div class="panel-heading">
	        <h3 class="panel-title"> 
	        	<!--a href="<?php echo base_url();?>homePortalEdu/tutorials"><img src="<?php echo base_url();?>img/Arrow-Back-icon.png"  width="24px" ></a--> 
	        	Course : <?php foreach ($topicName AS $info1) { echo ucfirst($info1->topicsName);   }?> 
	        </h3>
	    </div>
	    <div class="panel-body">
	    	
	    	
		
		<table id="topicTableTech" cellpadding="0" cellspacing="0" border="0" class="datatable table table-striped table-bordered table-hover">
			<thead class="active">
				<tr>
					<th align="center" >#</th>
					<th>Name</th>
					<th>Edit/Delete</th>			
				</tr>			
			</thead>
				<?php
					$i = 1;
					foreach ($tutorialsList as $row)	{
					   
						echo '<tr><td align="center" style="width:50px;">'.$i++.'</td>
						<td>'.ucfirst($row->gQname).'</td>
						<td style="width:200px;"> 					
						
						<!--a href="'.base_url().'homePortalEdu/viewQuizTech/'.$row->id.'" ><button type="button" class="label label-info" >									
						<i class="fa fa-eye-open"></i> View 
						</button> </a-->
						
						<a href="'.base_url().'homePortalEdu/editQuiz/'.$row->id.'" ><button type="button" class="label label-warning" >									
						<i class="fa fa-square-o"></i> Edit 
						</button> </a>
						
						<button type="button" class="label label-danger" onclick="loadModal(\''.base_url().'homePortalEdu/deleteQuiz/'.$row->id.'\')" >									
						<i class="fa fa-trash-o"></i> Delete 
						</button>
						
						</td></tr>';	
					}				 	 	     
					
				?>
			<tfoot>
				<tr>
					<th>#</th>
					<th>Name</th>
					<th>Edit/Delete</th>
				</tr>
			</tfoot>		
			</table>	
			
	
		</div>
		</div>
	</div>
</div>



<!--***************************************** PAGE BODY ENDS ***************************************************************-->


<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>