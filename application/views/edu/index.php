<?php
	$isNew = true;
	$userID = $this->ion_auth->user()->row()->id;
	$user;
	if ($userID > 0) {
		$isNew = false;
		$user = $this->ion_auth->user ($userID)->row ();
	}
?>

<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left">Dashboard</h3>
</div>
<div class="description">
	
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DELIVERY RUNS CONTENT -->
<!-- DATA TABLES -->

<!--***************************************** PAGE BODY STARTS ***************************************************************-->

<div class="row">
	<div class="col-md-12">

		<div class="well well-sm">
			<h3><?php echo $this->lang->line('HELLO');?> <?php echo $this->ion_auth->user()->row()->first_name;?> </h3>
		</div>
	
		<div class="col-md-6">
			<div class="panel panel-primary">
				
				
				
				<div class="bs-example">
			    <ul class="nav nav-tabs">
			        <li class="active"><a data-toggle="tab" href="#sectionA">Personal Info</a></li>
			        <li><a data-toggle="tab" href="#sectionB">Change Password</a></li>
			        <li><a data-toggle="tab" href="#sectionC">Manage Emails</a></li>
			        <!--li class="dropdown">
			            <a data-toggle="dropdown" class="dropdown-toggle" href="#">Dropdown <b class="caret"></b></a>
			            <ul class="dropdown-menu">
			                <li><a data-toggle="tab" href="#dropdown1">Dropdown1</a></li>
			                <li><a data-toggle="tab" href="#dropdown2">Dropdown2</a></li>
			            </ul>
			        </li-->
			    </ul>
			    <div class="tab-content">
			        <div id="sectionA" class="tab-pane fade in active">
		            <h3 class="panel-title"> </h3>          
		          
					  <div class="panel-body">
						  <div>
	              			<a href="#" class="close" data-dismiss="alert">&times;</a>		  	
						  	<div id="formChange"></div>
						  </div>
					  	
					    <div class="modal-body">
		
						<div class="form-horizontal" role="form">
							<input id="userID" type="hidden" value="<?php echo $user->id  ?>">	
						<input id="groups" type="hidden" value="<?php if($isNew==false){ $a= $this->ion_auth->get_users_groups()->result(); $groups = array(); foreach ($a as $userGroup){ $groups[] = $userGroup->id; } /*echo $groups;*/ }else{ echo ''; } ?>">


					<div class="form-group">
						<label class="col-sm-3 control-label">First Name</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="first_name" placeholder="Enter First Name" value="<?php if($isNew==false){echo $user->first_name; }else{echo '';} ?>">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label">Last Name</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="last_name" placeholder="Enter Last Name" value="<?php if($isNew==false){echo $user->last_name; }else{echo '';} ?>">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label">Organization</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="company" placeholder="Enter Organization" value="<?php if($isNew==false){echo $user->company; } else { echo '';} ?>">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label">Log In Email</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="email" placeholder="Enter Email" readonly="readonly" value="<?php if($isNew==false){echo $user->email; } else {echo '';} ?>">
						</div>
					</div>
		
					<div class="form-group">
						<label class="col-sm-3 control-label">username</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="username" placeholder="Enter Username" readonly="readonly" value="<?php if($isNew==false){echo $user->username; } else {echo '';} ?>">
						</div>
					</div>

					<!--div class="form-group">
						<label class="col-sm-3 control-label">Password</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="password" placeholder="Enter password" value="<?php if($isNew==false){ echo $user->password; } else { echo ''; }?>">
						</div>
					</div-->
		
				    <div class="form-group">
						<label class="col-sm-3 control-label">Phone</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="phone" placeholder="Enter phone #" value="<?php if($isNew==false){ echo $user->phone; } else { echo '';}?>">
						</div>
					</div>
				</div>
				</div>

				   <div class="modal-footer">			
					  <button type="button" class="btn btn-primary" id="saveButton" onclick="saveHomeUser()" data-loading-text="Saving...">Save</button>
				   </div>
			    </div> 
        </div>
        
        
        <div id="sectionB" class="tab-pane fade">
           
              <div class="panel-body">	
              		 <div>
              			<a href="#" class="close" data-dismiss="alert">&times;</a>		  	
					  	<div id="passwordChange"></div>
					  </div>
					  
					    <div class="modal-body">
		
						<div class="form-horizontal" role="form">
							<input id="userID" type="hidden" value="<?php echo $user->id  ?>">
	
						<input id="groups" type="hidden" value="<?php if($isNew==false){ $a= $this->ion_auth->get_users_groups()->result(); $groups = array(); foreach ($a as $userGroup){ $groups[] = $userGroup->id; } /*echo $groups;*/ }else{ echo ''; } ?>">		

					<div class="form-group">
						<label class="col-sm-3 control-label">Current Password</label>
						<div class="col-sm-9">
							<input type="password" class="form-control" id="currentPassword" placeholder="Enter current password" value="">
						</div>
					</div>
						<div class="form-group">
						<label class="col-sm-3 control-label">New Password</label>
						<div class="col-sm-9">
							<input type="password" class="form-control" id="password" placeholder="Enter new password" value="">
						</div>
					</div>
						<div class="form-group">
						<label class="col-sm-3 control-label">Confirm Password</label>
						<div class="col-sm-9">
							<input type="password" class="form-control" id="passconf" placeholder="Confirm new password" value="">
						</div>
					</div>
		
				 
				</div>
				</div>

				   <div class="modal-footer">			
					  <button type="button" class="btn btn-primary" id="saveButtonPassword" onclick="saveNewUserPassword()" data-loading-text="Saving...">Save</button>
				   </div>
			    </div>
        		
        </div>
        
        	
        	<div id="sectionB" class="tab-pane fade">
           
              <div class="panel-body">	
              		 <div>
              			<a href="#" class="close" data-dismiss="alert">&times;</a>		  	
					  	<div id="passwordChange"></div>
					  </div>
					  
					    <div class="modal-body">
		
						<div class="form-horizontal" role="form">
							<input id="userID" type="hidden" value="<?php echo $user->id  ?>">
	
						<input id="groups" type="hidden" value="<?php if($isNew==false){ $a= $this->ion_auth->get_users_groups()->result(); $groups = array(); foreach ($a as $userGroup){ $groups[] = $userGroup->id; } /*echo $groups;*/ }else{ echo ''; } ?>">		

					<div class="form-group">
						<label class="col-sm-3 control-label">Current Password</label>
						<div class="col-sm-9">
							<input type="password" class="form-control" id="currentPassword" placeholder="Enter current password" value="">
						</div>
					</div>
						<div class="form-group">
						<label class="col-sm-3 control-label">New Password</label>
						<div class="col-sm-9">
							<input type="password" class="form-control" id="password" placeholder="Enter new password" value="">
						</div>
					</div>
						<div class="form-group">
						<label class="col-sm-3 control-label">Confirm Password</label>
						<div class="col-sm-9">
							<input type="password" class="form-control" id="passconf" placeholder="Confirm new password" value="">
						</div>
					</div>
		
				 
				</div>
				</div>

				   <div class="modal-footer">			
					  <button type="button" class="btn btn-primary" id="saveButtonPassword" onclick="saveNewUserPassword()" data-loading-text="Saving...">Save</button>
				   </div>
			    </div>
        		
        	</div>
        
	        <div id="sectionC" class="tab-pane fade">
	        	
	           <div class="panel-body">	
              		 <div>
              			<a href="#" class="close" data-dismiss="alert">&times;</a>		  	
					  	<div id="credentialAlert"></div>
					  </div>
					  
					    <div class="modal-body">
		
						<div class="form-horizontal" role="form">
							<input id="userIDEmail" type="hidden" value="<?php echo $user->id  ?>">
	
								

						<div class="form-group">
							<label class="col-sm-3 control-label">Choose credential </label>
							<div class="col-sm-9">
								<input type="checkbox" class="" id="ticket"    value="1"   <?php if($emailAuth->ticket == 1)    echo " checked"; ?> >Tickets <br/>
								<input type="checkbox" class="" id="medChange" value="1"   <?php if($emailAuth->medChange == 1) echo " checked"; ?> >Med Changes <br/>
								<input type="checkbox" class="" id="comment"   value="1"   <?php if($emailAuth->comment == 1)   echo " checked"; ?> >Comments <br/>
							</div>
						</div>						
		
				 
					</div>
					</div>

				   <div class="modal-footer">			
					  <button type="button" class="btn btn-primary" id="saveButtonPassword" onclick="saveEmailCredential()" data-loading-text="Saving...">Save</button>
				   </div>
			    </div>
	        
	        </div>
	        
	        
	        
	        
	        <div id="dropdown2" class="tab-pane fade">
	            <h3>Dropdown 2</h3>
	            <p>Donec vel placerat quam, ut euismod risus. Sed a mi suscipit, elementum sem a, hendrerit velit. Donec at erat magna. Sed dignissim orci nec eleifend egestas. Donec eget mi consequat massa vestibulum laoreet. Mauris et ultrices nulla, malesuada volutpat ante. Fusce ut orci lorem. Donec molestie libero in tempus imperdiet. Cum sociis natoque penatibus et magnis.</p>
	        </div>
	    </div>
		</div>
					
				
				
			  
			</div>
			
			
			
			<div class="panel panel-primary">
			  <div class="panel-heading">
			    	<h3 class="panel-title">Assigned Quizes</h3>
			  </div>
			  <div class="panel-body">
			    	Coming Soon<br/>
			    	<br/><br/><br/>
			  </div>
			</div>
			
		</div>

		<div class="col-md-6">
			<div class="panel panel-info">
			  <div class="panel-heading">
			    	<h3 class="panel-title">User Activites </h3>
			  </div>
			  <div class="panel-body">
			    	Last Login :  AAAAA<br/>
			    	Last Quizes  : AAAAA<br/>
			    	Last Quizes Result :    AAAAA<br/>			    	
			  </div>
			</div>
			
			<div class="panel panel-success">
			  <div class="panel-heading">
			    	<h3 class="panel-title">Results</h3>
			  </div>
			  <div class="panel-body">
			  	<div class="table-responsive">
			    	<table class="table table-striped" >
			    		<thead>
			    			<tr class="info">
			    				<td>No</td>
			    				<td>Quizes Name</td>
			    				<td>Marks</td>
			    				<td>States</td>
			    			</tr>
			    		</thead>
			    		<tbody>
			    			<tr>
			    				<td>1</td>
			    				<td>Quizes Name</td>
			    				<td>Marks</td>
			    				<td>States</td>
			    			</tr>
			    			
			    			<tr>
			    				<td>2</td>
			    				<td>Quizes Name</td>
			    				<td>Marks</td>
			    				<td>States</td>
			    			</tr>
			    			
			    			<tr>
			    				<td>3</td>
			    				<td>Quizes Name</td>
			    				<td>Marks</td>
			    				<td>States</td>
			    			</tr>
			    			
			    		</tbody>
			    	</table>
			     </div>			    	
			  </div>
			</div>
			
			<div class="panel panel-warning">
			  <div class="panel-heading">
			    	<h3 class="panel-title">Assigned Turotials</h3>
			  </div>
			  <div class="panel-body">
			    	Coming Soon<br/>
			    	<br/><br/><br/>
			    	
			  </div>
			</div>
			
		</div>
		
	</div>
</div>











<!--***************************************** PAGE BODY ENDS ***************************************************************-->









<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>
