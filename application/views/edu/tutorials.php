<?php
	$isNew = true;
	$userID = $this->ion_auth->user()->row()->id;
	$user;
	if ($userID > 0) {
		$isNew = false;
		$user = $this->ion_auth->user ($userID)->row ();
	}
?>

<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left">Tutorials</h3>
</div>
<div class="description">
	
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DELIVERY RUNS CONTENT -->
<!-- DATA TABLES -->

<!--***************************************** PAGE BODY STARTS ***************************************************************-->

<div class="row" style ="min-height: 550px;" >
	<div class="col-md-12">

		<div class="well well-sm">
			<h3><?php echo $this->lang->line('HELLO');?> <?php echo $this->ion_auth->user()->row()->first_name;?> </h3>
		</div>
	
		<ul id="myTab" class="nav nav-tabs">
		   <li class="active"> <a href="#home" data-toggle="tab"> E Books </a> </li>
		   <li><a href="#videos" data-toggle="tab">Videos</a></li>		   
		</ul>
		
		
		<div id="myTabContent" class="tab-content" >
			<!-- ebooks start -->
		   <div class="tab-pane fade in active" id="home" >
		  
		   	<div class="col-md-8">
		   		
		   		<?php
		   			$pattern = "/<p[^>]*><\\/p[^>]*>/";
		   			foreach ($dataEbook as $eData1) {
						echo '<div class="bs-example">
					    <div class="panel panel-success">
					   		<div class="panel-heading">
					            <h3 class="panel-title">'.ucfirst($eData1->name).'</h3>
					        </div>
					        <div class="panel-body ebookBody"><p>'.			        
					        	
					        	        substr($eData1->details, 0, 2000)
					        	
					        	.'</p></div> 
					        	
					        <div class="panel-footer clearfix">
					            <div class="pull-right">
					                <a href="'.base_url().'homePortalEdu/viewTutorial/'.$eData1->id.'" class="btn btn-primary">Read More</a>
					                
					            </div>
					        </div>        
					    </div>
						</div>';   
				    }
		   		
		  		?>
		   			
				<div class="bs-example">
				    <ul class="pagination">
				        <li class="disabled"><a href="#">&laquo;</a></li>
				        <li class="active"><a href="#">1</a></li>
				        <li><a href="#">2</a></li>
				        <li><a href="#">3</a></li>
				        <li><a href="#">4</a></li>
				        <li><a href="#">5</a></li>
				        <li><a href="#">&raquo;</a></li>
				    </ul>
				</div>
	
					   		 	     
			</div>  
						    
		     <div class="col-md-4">
		     	
		     	<div class="bs-example">
				    <div class="list-group">
				        <a href="#" class="list-group-item active">
				            <span class="glyphicon glyphicon-camera"></span> Pictures <span class="badge">25</span>
				        </a>
				        <a href="#" class="list-group-item">
				            <span class="glyphicon glyphicon-file"></span> Documents <span class="badge">145</span>
				        </a>
				        <a href="#" class="list-group-item">
				            <span class="glyphicon glyphicon-music"></span> Music <span class="badge">50</span>
				        </a>
				        <a href="#" class="list-group-item">
				            <span class="glyphicon glyphicon-film"></span> Videos <span class="badge">8</span>
				        </a>
				    </div>
				</div>
				
		     </div>
		   </div>
		   <!-- ebooks end -->
		   
		   
		   
		   <!-- Videos start -->
		   <div class="tab-pane fade" id="videos">
		   	  <div class="col-md-8">
		      <?php
		   		
		   			foreach ($dataVideo as $eData) {
						echo '<div class="bs-example">
					    <div class="panel panel-success">
					   		<div class="panel-heading">
					            <h3 class="panel-title">'.ucfirst($eData->name).'</h3>
					        </div>
					        <div class="panel-body ebookBody"><p>'.			        
					        	
					        	        substr($eData->details, 0, 2000)	
					        	
					        	.'</p></div> 
					        	
					        <div class="panel-footer clearfix">
					            <div class="pull-right">
					                <a href="'.base_url().'homePortalEdu/viewTutorial/'.$eData->id.'" class="btn btn-primary">View More</a>
					                
					            </div>
					        </div>        
					    </div>
						</div>';   
				    }
		   		
		  		?>
				
					<div class="bs-example">
					    <ul class="pagination">
					        <li class="disabled"><a href="#">&laquo;</a></li>
					        <li class="active"><a href="#">1</a></li>
					        <li><a href="#">2</a></li>
					        <li><a href="#">3</a></li>
					        <li><a href="#">4</a></li>
					        <li><a href="#">5</a></li>
					        <li><a href="#">&raquo;</a></li>
					    </ul>
					</div>
				
						  
		      </div>
		      
		       <div class="col-md-4">
		     	<div class="bs-example">
				    <div class="list-group">
				        <a href="#" class="list-group-item active">
				            <span class="glyphicon glyphicon-camera"></span> Pictures <span class="badge">25</span>
				        </a>
				        <a href="#" class="list-group-item">
				            <span class="glyphicon glyphicon-file"></span> Documents <span class="badge">145</span>
				        </a>
				        <a href="#" class="list-group-item">
				            <span class="glyphicon glyphicon-music"></span> Music <span class="badge">50</span>
				        </a>
				        <a href="#" class="list-group-item">
				            <span class="glyphicon glyphicon-film"></span> Videos <span class="badge">8</span>
				        </a>
				    </div>
				</div>
		     </div>
		   </div>
		    <!-- Videos end -->
		   
		 
		</div>
		
		
	</div>
</div>











<!--***************************************** PAGE BODY ENDS ***************************************************************-->









<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>