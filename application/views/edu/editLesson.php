

<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left">Tutorials</h3>
</div>
<div class="description">
	
	<?php foreach ($byId as $value) {				 
			echo '<a  class="btn btn-lg btn-default"  href="'.base_url().'homePortalEdu/listOfinfoBaseOnTopicTech/'.$value->topicsId.'"><img src="'.base_url().'img/Arrow-Back-icon.png"  width="24px" >Go Back</a>';	
		}
	 ?>
		
	
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DELIVERY RUNS CONTENT -->
<!-- DATA TABLES -->

<!--***************************************** PAGE BODY STARTS ***************************************************************-->

	
	<div class="row">
		<div class="col-md-7">
			<?php 
			
				$details = "";
				$name = "";
				$type = "";
				$id = "";
				$topics = "";
				
				foreach ($byId as $value) {
					$details = $value->details;	
					$name = $value->name;
					$type = $value->typeInfo;
					$id = $value->id;
					$topics = $value->topicsId;
							
				} 
			
			?>	 
				
						 
		<?php echo form_open_multipart('homePortalEdu/updateEbookTutorial') ?>
		 
		 <div class="panel panel-info">
		    <div class="panel-heading">
		        <h3 class="panel-title">
		        	Edit :  <?php echo ucfirst($name); ?>
		        </h3>
		    </div>
		    <div class="panel-body">
		    	
		    <?php if(validation_errors()) { ?>
			        		
	        	 <div class="alert alert-warning" role="alert" id="wrapData" >
	      			 <a href="#" class="close" data-dismiss="alert">&times;</a>		  	
				  	 <div id="formChange"><?php echo validation_errors(); ?></div>
				 </div>
				 
				<?php }else	{
					$a = $this->uri->segment('2');
					if ($a == "updateEbookTutorial"){
						
					echo '
					 <div class="alert alert-warning" role="alert" id="wrapData" >
	      				 <a href="#" class="close" data-dismiss="alert">&times;</a>		  	
				  		 <div id="formChange">'.$s.'</div>
					 </div>';
					}
				}						
			
		  ?>  
		    	        		
		  <div class="form-group">
		     <label for="ebookTitle">Title</label>
		     <input type="text" class="form-control" id="ebookTitle" name="ebookTitle" placeholder="Title" value="<?php echo $name; ?>">
		  </div>
		  <div class="form-group">
		     <label for="exampleInputPassword1">Topics</label>
		     <?php
		     	echo '<select class="form-control" id="topicsId" name="topicsId">
		     		<option value="" > </option>
				 ';
				
				foreach ($allTopics as $row)	{
				    echo '<option value="'.$row->id.'"';
					if($row->id == $topics) { echo "selected"; }
					echo '>'.ucfirst($row->topicsName).'</option>';
				}				 	 	     
				echo '</select>';
		     
		     ?>
		     <!--div id="topicsComboEbook"> </div-->	
		  </div>
		  <div class="form-group">
		     <label for="exampleInputPassword1">Type</label>
		     <input type="radio" name="documentType" value="1" <?php if($type == 1) { echo "checked"; } ?> > E book
		     <input type="radio" name="documentType" value="2" <?php if($type == 2) { echo "checked"; } ?> > Video
		  </div>
		  <!--div class="form-group">
		     <label for="exampleInputFile">File input</label>
		     <input type="file" id="userfile" name="userfile">
		   
		  </div-->						  
		  <div class="form-group">
		  	<?php  echo $this->ckeditor->editor('quizeGeneralInfo',@$details);?> 
		
		  </div>
		  <input type="hidden"  id="id" name="id" value="<?php echo $id;?>">
		  <input type="submit" class="btn btn-primary" id="saveButton"  data-loading-text="Saving..." value="Save">
		  
		</form>	   
						
		</div>	
	</div>
	
	</div>
	</div>



<!--***************************************** PAGE BODY ENDS ***************************************************************-->


<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>