

<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left">Tutorials</h3>
</div>
<div class="description">
	
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DELIVERY RUNS CONTENT -->
<!-- DATA TABLES -->

<!--***************************************** PAGE BODY STARTS ***************************************************************-->

<div class="row">

	    	
	 <div class="col-md-8">     	
     	<div class="bs-example">
	    <div class="panel panel-success">
	   		<div class="panel-heading">
	            <h3 class="panel-title">Add Course</h3>
	        </div>
	        <div class="panel-body ebookBody">
	        	 <div>
          			 <a href="#" class="close" data-dismiss="alert">&times;</a>		  	
				  	 <div id="topicsFormChange"></div>
				 </div>
	        	
				  <div class="form-group">
				     <label for="tutorialTitle">Title</label>
				     <input type="text" class="form-control" id="tutorialTitle" placeholder="Enter Title">
				  </div>
				 		  
				  <button type="submit" class="btn btn-primary" id="saveButtonTopics" onclick="saveTutorialTopics()" data-loading-text="Saving...">Submit</button>
						        	
	        </div> 
	    </div>
		</div>		
     </div>
     
     
     
     
     <div class="col-md-8">
		
		 <div class="panel panel-primary">
	     <div class="panel-heading">
	        <h3 class="panel-title">List of Courses</h3>
	     </div>
	     <div class="panel-body">    	
			<table id="topicTable" cellpadding="0" cellspacing="0" border="0" class="datatable table table-striped table-bordered table-hover">
			<thead class="active">
				<tr>
					<th align="center" >#</th>
					<th>Name</th>
					<th>Edit/Delete</th>			
				</tr>			
			</thead>
				<?php
					$i = 1;
					foreach ($topics as $row)	{
					   
						echo '<tr><td align="center" style="width:50px;">'.$i++.'</td>
						<td>'.ucfirst($row->topicsName).'</td>
						<td style="width:150px;"> 
						<button type="button" class="label label-warning" onclick="loadModal(\''.base_url().'homePortalEdu/editTopic/'.$row->id.'\')" >									
						<i class="fa fa-square-o"></i> Edit 
						</button>
						
						<button type="button" class="label label-danger" onclick="loadModal(\''.base_url().'homePortalEdu/deleteTopic/'.$row->id.'\')" >									
						<i class="fa fa-trash-o"></i> Delete 
						</button>
						
						</td></tr>';	
					}				 	 	     
					
				?>
			<tfoot>
				<tr>
					<th>#</th>
					<th>Name</th>
					<th>Edit/Delete</th>
				</tr>
			</tfoot>		
			</table>		
			</div>
		   </div>	
	 </div>
	    	
		
 </div>	



<!--***************************************** PAGE BODY ENDS ***************************************************************-->


<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>