<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left">Policies & Procedures</h3>
</div>
<div class="description">
	<button class="btn btn-primary btn-lg"
		onclick="loadModal('<?php echo site_url('filingCab/newPolicyAreaModal');?>')">
		<i class="fa fa-plus"></i> Policy Area
	</button>
	<button class="btn btn-primary btn-lg"
		onclick="loadModal('<?php echo site_url('filingCab/newPolicyModal');?>')">
		<i class="fa fa-plus"></i> Policy
	</button>
	| Establisging best practices and sharing experiences
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->


<div class="row">
	<div class="col-md-6 pull-right"></div>
</div>

<!-- FAQ -->
<div class="row">
	<!-- NAV -->
	<div id="list-toggle" class="col-md-3">
		<div class="list-group">
		<?php
		$v = $this->policy->getPolicyArea ();
		$count = 1;
		foreach ( $v->result () as $policyArea ) {
			?>
			<a href="#tab<?php echo $count++;?>" data-toggle="tab"
				class="list-group-item">
			 <?php echo $policyArea->policyAreaName;?><button
					onclick="loadModal('<?php echo site_url('filingCab/editPolicyAreaModal').'/'.$policyArea->policyAreaID;?>')"
					class="pull-right">
					<i class="fa fa-pencil"></i> Edit
				</button>
			</a> 
								<?php }?>
		</div>
	</div>
	<!-- /NAV -->
	<!-- CONTENT -->
	<div class="col-md-9">
		<div class="tab-content">
		<?php
		$PolicyAreaCount = 1;
		foreach ( $v->result () as $policyArea2 ) {
			$policies = $this->policy->getPolicies ( $policyArea2->policyAreaID );
			?>
					<div class="tab-pane"
				id="tab<?php echo $PolicyAreaCount; ?>">
				<div class="panel-group" id="accordion">
				<?php
				$policyCount=1;
			foreach ( $policies->result () as $policy ) {
				?>		
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">
								<a class="accordion-toggle" data-toggle="collapse"
									data-parent="#accordion" href="#collapse<?php echo $policyCount; ?>"><?php echo $policy->policyTitle;?>
									 <button class="pull-right"
										onclick="loadModal('<?php echo site_url('filingCab/editPolicyModal').'/'.$policy->policyID;?>')">
										<i class="fa fa-pencil pull-right"></i> Edit
									</button></a>
							</h3>
						</div>
						<div id="collapse<?php echo $policyCount++; ?>" class="panel-collapse collapse in">
							<div class="panel-body">
								<p>Purpose: <?php echo $policy->purpose;?></p>
								<p>Policy: <?php echo $policy->policy;?></p>
								<p>Purpose: <?php echo $policy->procedures;?></p>
							</div>
						</div>
					</div>
					<?php }?>
				</div>
			</div>
<?php $PolicyAreaCount++; }?>
		</div>
	</div>
</div>
<!-- /CONTENT -->
<!-- /FAQ -->


<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>


