<?php $policyArea = $this->policy->getPolicyArea($this->uri->segment(3)); ?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"
		aria-hidden="true">&times;</button>
	<h4 class="modal-title">Edit Policy Area</h4>
</div>
<div class="modal-body">
	<div class="form-horizontal">
		<div class="form-group">
			<label class="col-sm-3 control-label">Policy Area</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" id="policyAreaName" placeholder="Text input" value="<?php echo $policyArea->policyAreaName; ?>">
			</div>
		</div>
	</div>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	<button type="button" class="btn btn-primary" id="savePolicyAreaButton" onclick="modifyPolicyArea(<?php echo $policyArea->policyAreaID;?>)">Save changes</button>
</div>


