<?php $gf = &get_instance(); ?>

<!DOCTYPE html>
<html lang="en">
<head>
	
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title> <?php if($this->uri->segment(2) == "NULL") { echo "Home"; } else { echo $this->uri->segment(2); } ?> </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
	

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/cloud-admin.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/themes/default.css" id="skin-switcher">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/chosen.css">	    

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    
    <link href="<?php echo base_url(); ?>css/raterater.css" rel="stylesheet"/>
    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/datepicker/themes/default.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/datepicker/themes/default.date.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/datepicker/themes/default.time.min.css" />

	<!-- DATE TIME PICKER -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/bootstrap-datetimepicker.min.css" />
	
	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/datatables/media/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/datatables/media/assets/css/datatables.min.css" />
   
    <link href="<?php echo base_url(); ?>css/anysearch.css" media="screen" rel="stylesheet" type="text/css">

	
</head>
<body>
<?php $index='index.php/'; ?>
	<!-- HEADER -->
	<header class="navbar clearfix navbar-fixed-top" id="header">
		<div class="container">
			<div class="navbar-brand">
				<!-- COMPANY LOGO -->
				<a href="<?php echo base_url(); ?><?php $index; ?>compliance/ticketsdash"> <img
					src="<?php echo base_url(); ?>img/logo/logo.png"
					alt="Cloud Admin Logo" class="img-responsive" height="30"
					width="200">
				</a>
				<!-- /COMPANY LOGO -->
				<!-- TEAM STATUS FOR MOBILE -->
				<div class="visible-xs">
					<a href="#" class="team-status-toggle switcher btn dropdown-toggle">
						<i class="fa fa-users"></i>
					</a>
				</div>
				<!-- /TEAM STATUS FOR MOBILE -->

				<!-- SIDEBAR COLLAPSE -->
				<div id="sidebar-collapse" class="sidebar-collapse btn">
					<i class="fa fa-bars" data-icon1="fa fa-bars"
						data-icon="fa fa-bars"></i>
				</div>
				<!-- /SIDEBAR COLLAPSE -->
			</div>
			<!-- NAVBAR LEFT -->
			<ul class="nav navbar-nav pull-left hidden-xs" id="navbar-left">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle tip-bottom" data-toggle="tooltip" title="Home Portal"> 
						<i class="fa fa-users"></i> <span class="name">Home Portal</span>
						<i class="fa fa-angle-down"></i>
					</a>
				</li>
			</ul>
			
			<ul class="nav navbar-nav pull-left hidden-xs" id="navbar-left">
				<li class="dropdown">
					<a href="<?php echo base_url().''.$index.'homePortalEdu/tutorials' ;?>" class="dropdown-toggle tip-bottom" data-toggle="tooltip" title="Education Portal"> 
						<i class="fa fa-users"></i> <span class="name">Education Portal</span>
						<i class="fa fa-angle-down"></i>
					</a>
				</li>
			</ul>
			
			<!-- /NAVBAR LEFT -->
			<!-- BEGIN TOP NAVIGATION MENU -->
			<ul class="nav navbar-nav pull-right">
			
					
				<!-- BEGIN INBOX DROPDOWN -->
				<li class="dropdown" id="header-message">
					<a href="<?php echo base_url($index.'compliance/question'); ?>"	class="dropdown-toggle" > 
						<i class="fa fa-question-circle"></i> <span class="badge"></span> </a>
			    </li>
				
				<li class="dropdown" id="header-message"><a href="#"
					class="dropdown-toggle" data-toggle="dropdown"> <i
						class="fa fa-envelope"></i> <span class="badge"></span>
				</a>
					<ul class="dropdown-menu inbox">
						<li class="dropdown-title"><span><i class="fa fa-envelope-o"></i>
								Messages</span> <span class="compose pull-right tip-right"
							title="Compose message"><i class="fa fa-pencil-square-o"></i></span>
						</li>
						<?php 
							$homeComments = $this->comp->getNewHomeComments();
									if ($homeComments) {
										foreach ( $homeComments->result () as $homeComment ) {
											$time_ago =strtotime($homeComment->messageDate);
																											
							?>						
						<li  onclick="loadModal('<?php echo base_url($index.'compliance/editTicketModal/') . '/' . 0 . '/' . $homeComment->ticketID ;?>')">
							<a> 
								<span class="body"> <span class="from"><?php echo $homeComment->first_name . ' ' . $homeComment->last_name ; ?> </span> <span
									class="message"> <?php echo $homeComment->message; ?> </span> <span class="time"> <i
										class="fa fa-clock-o"></i> <span><?php echo $this->quick->timeAgo($time_ago); ?></span>
								</span>
							</span>

						</a></li>
						<?php }}?>
						
						
						<li class="footer"><a href="#">See all messages <i
								class="fa fa-arrow-circle-right"></i></a></li>
					</ul></li>
				<!-- END INBOX DROPDOWN -->
	
				<!-- BEGIN USER LOGIN DROPDOWN -->
				<li class="dropdown user" id="header-user"><a href="#"
					class="dropdown-toggle" data-toggle="dropdown"> 
					<img alt="" src="<?php echo base_url(); ?>img/avatars/avatar3.jpg" />
					 <span class="username">
						<?php
							$user = $this -> ion_auth -> user() -> row();
							if (!empty($user -> first_name)) {
								echo $user -> first_name . ' ' . $user -> last_name;
							}
						?>
						</span> <i class="fa fa-angle-down"></i>
				</a>
					<ul class="dropdown-menu">
						<li><a href="<?php echo base_url($index.'compliance/profile'); ?>"><i class="fa fa-user"></i> My Profile</a></li>
						<li><a href="#"><i class="fa fa-cog"></i> Account Settings</a></li>
						<li><a href="http://www.seamlesscare.ca/privacy-policy/"><i class="fa fa-eye"></i> Privacy Settings</a></li>
						<li>
						<a>
							<?php
								if ($this -> quick -> is_internal()) {
									echo '<i class="fa fa-building-o"></i> Internal User';
								} else {
									echo '<i class="fa fa-cloud"></i> External User';
								}
	 						?>
 						</a>
 						</li>
						<li><a href="<?php echo base_url(); ?><?php echo $index; ?>auth/logout"><i
								class="fa fa-power-off"></i> Log Out</a></li>
					</ul></li>
				<!-- /END USER LOGIN DROPDOWN -->
			</ul>
			<!-- /END TOP NAVIGATION MENU -->
		</div>
		<!-- TEAM STATUS -->
		<div class="container team-status" id="team-status">
			<div id="scrollbar">
				<div class="handle"></div>
			</div>
			<div id="teamslider">
				<ul class="team-list">
					
				</ul>
			</div>
		</div>
		<!-- /TEAM STATUS -->
	</header>
	<!--/HEADER -->

	<!-- PAGE -->
	<section id="page">
		<!-- SIDEBAR -->
		<div id="sidebar" class="sidebar sidebar-fixed">
			<div class="sidebar-menu nav-collapse">
				<div class="divide-20"></div>
				<!-- SEARCH BAR -->
				<!--div id="search-bar">
					<input id="patFuzySearch" type="text" size="30" onkeyup="getPatFromApi()">
					<div id="patientResults"></div>
				</div-->
				
				<!-- /SEARCH BAR -->

				<!-- SIDEBAR QUICK-LAUNCH -->
				<!-- <div id="quicklaunch">
						<!-- /SIDEBAR QUICK-LAUNCH -->

				<!-- SIDEBAR MENU -->
				<ul>
				
				    <li>				       
				         <?php $gf->getPatInfoToCombo(); ?>				     			       
				    </li>

                    <li <?php if ($this -> uri -> segment(2, 0) == 'pharmacistCheckGraph') {echo 'class="active"'; } ?>>
						<a href="<?php echo base_url(); ?><?php echo $index; ?>compliance/pharmacistCheckGraph"> <spanclass="sub-menu-text"> Dashboard </span></a>
					</li>
					
					<!-- li <?php if ($this -> uri -> segment(2, 0) == 'homes') {echo 'class="active"'; } ?> >
					     <a href="<?php echo base_url("compliance/homes"); ?>"> <span class="sub-menu-text">Homes</span> </a>
					</li-->
										
					<li <?php if (($this -> uri -> segment(2, 0) == 'survey') || ($this -> uri -> segment(2, 0) == 'surveyQuestion') || ($this -> uri -> segment(2, 0) == 'surveyQuestionAnswer') ) {echo 'class="active"'; } ?> >
					     <a href="<?php echo base_url($index.'compliance/survey'); ?>"> <span class="sub-menu-text">Survey</span> </a>
					</li>
					
					
					<li <?php if ($this -> uri -> segment(2, 0) == 'ticketsDash') {echo 'class="active"'; } ?> >
					     <a href="<?php echo base_url("index.php/compliance/ticketsDash"); ?>"><span class="sub-menu-text">Tickets</span></a>
					</li>
							
					<li <?php if ($this -> uri -> segment(2, 0) == 'medchanges') {echo 'class="active"'; } ?>>
						<a href="<?php echo base_url($index.'compliance/medchanges'); ?>"> <span class="sub-menu-text">Med Changes</span></a>
					</li>
					
					<li <?php if (($this -> uri -> segment(2, 0) == 'Error') || ($this -> uri -> segment(2, 0) == 'errorReport')) {echo 'class="active"'; } ?>>
						<a href="<?php echo base_url($index.'compliance/Error'); ?>"> <span class="sub-menu-text">Error</span></a>
					</li>
							
					<li <?php if ($this -> uri -> segment(2, 0) == 'eBoard') {echo 'class="active"'; } ?>>
						<!-- a href="<?php echo base_url("compliance/eBoard"); ?>"> <span class="sub-menu-text">E board</span></a-->
						<a href="<?php echo base_url(); ?><?php echo $index; ?>compliance/eBoardLocal"> <spanclass="sub-menu-text">Deliveries </span></a>
					</li>
					
					<li <?php if ($this -> uri -> segment(2, 0) == 'getBatch') {echo 'class="active"'; } ?>>
				        <!-- a href="<?php echo base_url(); ?>production/getBatch"> <spanclass="sub-menu-text">Production </span></a-->
				         <a href="<?php echo base_url(); ?><?php echo $index; ?>compliance/production"> <spanclass="sub-menu-text">Production </span></a>
				    </li>
				    
				    <li <?php if ($this -> uri -> segment(2, 0) == 'clinicalResearch') {echo 'class="active"'; } ?>>
				        <!-- a href="<?php echo base_url(); ?>production/getBatch"> <spanclass="sub-menu-text">Production </span></a-->
				         <a href="<?php echo base_url(); ?><?php echo $index; ?>compliance/clinicalResearch"> <spanclass="sub-menu-text">Clinical DOC </span></a>
				    </li>
								
					<li <?php if ($this -> uri -> segment(2, 0) == 'portalUsers') {echo 'class="active"'; }?>>
						<a href="<?php echo base_url($index.'compliance/portalUsers'); ?>"> <span class="sub-menu-text"> Users</span></a>
					</li>
							
							
					<li <?php if ($this -> uri -> segment(2, 0) == 'home') {echo 'class="active"'; } ?> > 
						  <a href="<?php echo base_url(); ?><?php echo $index; ?>setting/home"><span class="sub-menu-text">Settings</span></a>
					</li>
					
					 <li <?php if ($this -> uri -> segment(2, 0) == 'wardLocationInDelivery') {echo 'class="active"'; } ?> >
				         <a href="<?php echo base_url(); ?><?php echo $index; ?>compliance/wardLocationInDelivery"> <spanclass="sub-menu-text"> Bin Location </span></a>
				    </li>
					
					
			</ul>
			
			</div>
		</div>
		<!-- /SIDEBAR -->
		<div id="main-content">
			<div class="container">
				<div class="row">
					<div id="content" class="col-lg-12">
						<!-- PAGE HEADER-->
						<div class="row">
							<div class="col-sm-12">
								<div class="page-header">
