<?php
//$user = strtoupper(substr($this->ion_auth->user ()->row ()->first_name, 0, 1)).strtoupper(substr($this->ion_auth->user ()->row ()->last_name, 0, 1))."-".($this->ion_auth->user ()->row ()->id);
//$homesPatients = $this->portal->getUserHomesAndPatients()->result();

?>
<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
    <h3 class="content-title pull-left">Calendar</h3> &nbsp;&nbsp;&nbsp;<button class="btn btn-primary" data-toggle="modal" data-target="#NewCalModal" style="margin-top:10px;margin-left:20px;">Add Event</button>
</div>
<div class="description"></div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DELIVERY RUNS CONTENT -->
<!-- DATA TABLES -->

<!--<div class="row">
    <div class="col-md-12">
        <?php
/*        foreach($patHomeResults as $patHomeResult){
            print_r($patHomeResult);
        }
        */?>
    </div>
</div>-->
<div class="row">
    <div class="col-lg-12">
        <div id="NewCalModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close closeNewEvent" data-dismiss="modal"><span aria-hidden="true">X</span> <span class="sr-only">close</span></button>
                        <h4 id="newModalTitle" class="modal-title">New Event</h4>
                    </div>
                    <div id="modalBody" class="modal-body">
                        <div class="form-horizontal" role="form">
                            <div class="form-group"><div id="new-main-message" style="text-align:center;"></div></div>
                            <div class="form-group">
                                <label id="newEventDateLabel" class="col-sm-3 control-label">Event Title*</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="newEventTitle" value="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Event Type</label>
                                <div class="col-sm-9">
                                    <?php
                                    //collect patients
                                    $patients = array();
                                    //collect homes
                                    $homes = array();
                                    foreach($homesPatients as $homepat){
                                        $patID = $homepat->patID;
                                        $patInitials = $homepat->patInitials;
                                        $homeID = $homepat->homeID;
                                        $homeName = $homepat->homeName;

                                        $patients[$patID] = $patInitials."-".$patID;
                                        $homes[$homeID] = $homeName;
                                    }
                                    ?>
                                    <select class="form-control" id="newEventType">
                                        <option value="">Select one</option>
                                        <?php
                                        if((count($patients) > 0)|| (count($homes) > 0)){
                                            foreach($patients as $patId => $patVal){
                                                ?>
                                                <option value="<?php echo "p".$patId; ?>"><?php echo $patVal; ?></option>
                                                <?php
                                            }//end of patients foreach
                                            foreach($homes as $homeId => $homeVal){
                                                ?>
                                                <option value="<?php echo "h".$homeId; ?>"><?php echo $homeVal; ?></option>
                                                <?php
                                            }
                                        }else{
                                            ?>
                                            <option value="">N/A</option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
							
							<!--Start My Code-->

								<div class="form-group">
                                <label class="col-sm-3 control-label">Appointment Type</label>
                                <div class="col-sm-9">
                                   
                                        <select class="form-control" id="newappointmentType">
                                        <option value="">Select one</option>
                                        <option value="family">Family</option>
                                        <option value="personal">Personal</option>
                                        <option value="business">Business</option>
                                        
                                        <?php
                                        
                                        /*if((count($patients) > 0)|| (count($homes) > 0)){
                                            foreach($patients as $patId => $patVal){
                                                ?>
                                                <option value="<?php echo "p".$patId; ?>"><?php echo $patVal; ?></option>
                                                <?php
                                            }//end of patients foreach
                                            foreach($homes as $homeId => $homeVal){
                                                ?>
                                                <option value="<?php echo "h".$homeId; ?>"><?php echo $homeVal; ?></option>
                                                <?php
                                            }
                                        }else{
                                            ?>
                                            <option value="">N/A</option>
                                            <?php
                                        }*/
                                        ?>
                                    </select>
                                </div>
                            </div>

<!--End My Code-->
                            <div class="form-group">
                                <label id="eventDateLabel" class="col-sm-3 control-label">Event Date*</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control datepicker-fullscreen" id="newScheduleDate" name="newScheduleDate" value="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label id="eventDateLabel" class="col-sm-3 control-label">Event Time*</label>
                                <div class="col-sm-9">
                                    <input type="text" id="newScheduleTime" name="newScheduleTime" class="form-control timepicker-fullscreen" value="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label id="descriptionLabel" class="col-sm-3 control-label">Description:</label>
                                <div class="col-sm-9">
                                    <textarea id="newDescription" class="form-control" rows="10" cols="50" name="newDescription"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
<!--                        <input type="hidden" name="calendarId" id="calendarId" value=""/>
                        <input type="hidden" name="eventHome" id="eventHome" value=""/>
                        <input type="hidden" name="eventPat" id="eventPat" value=""/>-->
                        <button type="button" class="btn btn-default closeNewEvent" data-dismiss="modal" id="newEventClose">Close</button>
                        <button class="btn btn-primary" onclick="return addEvent();">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
	

    <div class="col-lg-12">
        <div id='calendar'></div>
        <div id="fullCalModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close eventClose" data-dismiss="modal"><span aria-hidden="true">X</span> <span class="sr-only">close</span></button>
                        <h4 id="modalTitle" class="modal-title">Event</h4>
                    </div>
                    <div id="modalBody" class="modal-body">
                        <div class="form-horizontal" role="form">
                            <div class="form-group"><div id="main-message" style="text-align:center;"></div></div>
                            <div class="form-group">
                                <label id="eventDateLabel" class="col-sm-3 control-label">Event Title*</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="eventTitle" value="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Event Type</label>
                                <div class="col-sm-9">
                                    <?php
                                    //collect patients
                                    $patients = array();
                                    //collect homes
                                    $homes = array();
                                    foreach($homesPatients as $homepat){
                                        $patID = $homepat->patID;
                                        $patInitials = $homepat->patInitials;
                                        $homeID = $homepat->homeID;
                                        $homeName = $homepat->homeName;

                                        $patients[$patID] = $patInitials."-".$patID;
                                        $homes[$homeID] = $homeName;
                                    }
                                    ?>
                                    <select class="form-control" id="eventType">
                                        <option value="">Select one</option>
                                        <?php
                                        if((count($patients) > 0)|| (count($homes) > 0)){
                                            foreach($patients as $patId => $patVal){
                                            ?>
                                                <option value="<?php echo "p".$patId; ?>"><?php echo $patVal; ?></option>
                                            <?php
                                            }//end of patients foreach
                                            foreach($homes as $homeId => $homeVal){
                                            ?>
                                                <option value="<?php echo "h".$homeId; ?>"><?php echo $homeVal; ?></option>
                                            <?php
                                            }
                                        }else{
                                        ?>
                                            <option value="">N/A</option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
							<!--Start My Code-->

								<div class="form-group">
                                <label class="col-sm-3 control-label">Appointment Type</label>
                                <div class="col-sm-9">
                                    <?php
                                    //collect patients
                                    $patients = array();
                                    //collect homes
                                    $homes = array();
                                    foreach($homesPatients as $homepat){
                                        $patID = $homepat->patID;
                                        $patInitials = $homepat->patInitials;
                                        $homeID = $homepat->homeID;
                                        $homeName = $homepat->homeName;

                                        $patients[$patID] = $patInitials."-".$patID;
                                        $homes[$homeID] = $homeName;
                                    }
                                    ?>
                                    <select class="form-control" id="newappointmentType">
                                        <option value="">Select one</option>
                                        <option value="family">Family</option>
                                        <option value="family">Personal</option>
                                        <option value="family">Business</option>
                                        
                                        <?php
                                        
                                        /*if((count($patients) > 0)|| (count($homes) > 0)){
                                            foreach($patients as $patId => $patVal){
                                                ?>
                                                <option value="<?php echo "p".$patId; ?>"><?php echo $patVal; ?></option>
                                                <?php
                                            }//end of patients foreach
                                            foreach($homes as $homeId => $homeVal){
                                                ?>
                                                <option value="<?php echo "h".$homeId; ?>"><?php echo $homeVal; ?></option>
                                                <?php
                                            }
                                        }else{
                                            ?>
                                            <option value="">N/A</option>
                                            <?php
                                        }*/
                                        ?>
                                    </select>
                                </div>
                            </div>

<!--End My Code-->

                            <div class="form-group">
                                <label id="eventDateLabel" class="col-sm-3 control-label">Event Date*</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control datepicker-fullscreen" id="scheduleDate" value="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label id="eventDateLabel" class="col-sm-3 control-label">Event Time*</label>
                                <div class="col-sm-9">
                                    <input type="text" id="scheduleTime" name="scheduleTime" class="form-control timepicker-fullscreen" value="">
                                </div>
                            </div>
					
                        <!--<div class="form-group">
                            <label for="start_dt">Start Date/Time:</label>
                            <div class='input-group date' id='dateTimePicker'>
                                <input type='text' id="start_dt" name="start_dt" class="form-control" />
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
                            </div>
                        </div>-->
						
						
                            <div class="form-group">
                                <label id="descriptionLabel" class="col-sm-3 control-label">Description:</label>
                                <div class="col-sm-9">
                                    <textarea id="description" class="form-control" rows="10" cols="50" name="description"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="calendarId" id="calendarId" value=""/>
                        <button type="button" class="btn btn-default eventClose" data-dismiss="modal">Close</button>
                        <button class="btn btn-primary" id="updateEvent" onclick="return saveEvent();">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /CONTENT -->

<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>
