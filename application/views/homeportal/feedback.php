<style>
.modal-dialog {
	width: 70%;
}
</style>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"
		aria-hidden="true">&times;</button>
	<h1 class="modal-title">
		<i class="fa fa-info-circle"></i> Feedback - Coming soon
	</h1>
</div>
<div class="modal-body">

	
	<div class="form-group">
		<label class="col-md-4 control-label">Delivery Service: </label>
		<div class="col-md-8">
			<div id="size-demo"></div>
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-4 control-label">Accounts Receivables:</label>
		<div class="col-md-8">
			<div id="size-demo"></div>
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-4 control-label">Medication Change Process: </label>
		<div class="col-md-8">
			<div id="size-demo"></div>
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-4 control-label">Competency of Staff: </label>
		<div class="col-md-8">
			<div id="size-demo"></div>
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-4 control-label">After-hours service: </label>
		<div class="col-md-8">
			<div id="size-demo"></div>
		</div>
	</div>

	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal"
			id="closeButtonModal">Close</button>
	</div>
</div>

