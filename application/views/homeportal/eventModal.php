<?php
$isNew = true;
$patID = $this->uri->segment ( 3 );
$eventID = $this->uri->segment ( 4 );
$event;
if ($this->uri->segment ( 4 ) > 0) {
	$isNew = false;
	$event = $this->portal->getPatEvents ( $patID, $this->uri->segment ( 4 ) );
}
?>


<script>
$(".datepicker-fullscreen").pickadate();
$(".timepicker-fullscreen").pickatime();
</script>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"
		aria-hidden="true">&times;</button>
	<h4 class="modal-title">Event Detail - <?php echo $patID; ?> - * Fields are required</h4>
</div>
<div class="modal-body">
	<div class="form-horizontal" role="form">
		<input id="eventID" type="hidden"
			value="<?php if($isNew==false){ echo $event->id;} else{echo '0';}?>">
		<input id="patID" type="hidden"
			value="<?php if($isNew==false){ echo $event->patID;} else{ echo $patID;}?>">
		<div class="form-group">
			<label class="col-sm-3 control-label">Event Type</label>
			<div class="col-sm-9">
				<select class="form-control" id="eventTypeID">
				<?php
				$eventPatTypes = $this->portal->getPatEventTypes ();
				foreach ( $eventPatTypes->result () as $eventType ) {
					?>
					<option value="<?php echo $eventType->id;?>"
						<?php if($isNew==false && $eventType->id==$event->eventTypeID){ echo 'selected="selected"';}?>><?php echo $eventType->eventType;?></option>
				<?php }?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label id="eventDateLabel" class="col-sm-3 control-label">Event Date*</label>
			<div class="col-sm-9">
				<input type="text" class="form-control datepicker-fullscreen"
					id="eventDate"
					value="<?php  if($isNew==false){
						$date=date_create(substr($event->eventDate,0,10));
						echo date_format($date,"d F Y"); }?>">

			</div>
		</div>

		<div class="form-group">
			<label id="eventDateLabel" class="col-sm-3 control-label">Event Time*</label>
			<div class="col-sm-9">
				<input type="text" id="eventTime" name="regular"
					class="form-control timepicker-fullscreen"
					value="<?php
					
if ($isNew == false) {
						$time_in_12_hour_format = date ( "g:i a", strtotime ( substr ( $event->eventDate, 10, 6 ) ) );
						echo $time_in_12_hour_format;
					}
					?>">

			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label">Reported By</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" id="reportedByUserName"
					placeholder="Text input"
					value="<?php
					if ($isNew == false) {
						$user = $this->ion_auth->user ( $event->reportedByUser )->row ();
						echo $user->first_name . ' ' . $user->last_name;
					} else {
						$user = $this->ion_auth->user ()->row ();
						echo $user->first_name . ' ' . $user->last_name;
					}
					?>"
					disabled> <input id="reportedByUser" type="hidden"
					value="<?php
					
					if ($isNew == false) {
						echo $event->reportedByUser;
					} else {
						$user = $this->ion_auth->user ()->row ();
						echo $user->id;
					}
					?>">
			</div>
		</div>
		<div class="form-group">
			<label id="descriptionLabel" class="col-sm-3 control-label">Description</label>
			<div class="col-sm-9">
				<textarea class="form-control" id="desc" rows="10"> <?php if($isNew==false){ echo $event->desc ;}?></textarea>
			</div>
		</div>

	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal"
			id="closeButtonModal">Close</button>
		<button type="button" class="btn btn-primary" id="saveEventButton"
			onclick="saveEvent()" data-loading-text="Saving...">Save changes</button>
	</div>
	<script>

	jQuery(document).ready(function() {	
		
		$( "#eventTypeID" ).on('change',function(e){
			var optionSelected = $("option:selected", this);
			var valueSelected = this.value;
			if (valueSelected == 3)
			{
				$("#eventDateLabel").text("PRN Administation Date & Time");
				$("#descriptionLabel").text("Reason PRN given");
				
			}
			else
			{
				$("#eventDateLabel").text("Event Date");
				$("#descriptionLabel").text("Description");
				
			}		
		});
	});
	
	</script>