<?php
	$patID = $patient ->ID;
	//$patient = $this->comp->getCompliancePatient ( $patID );
	//$user = $this->ion_auth->user ()->row ();
?>


<!-- STYLER -->

<style>
.modal-dialog {
	width: 60%;
}
</style>
<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
  <h3 class="content-title pull-left"><?php echo $patient -> LastName .' '.$patient->FirstName. ' - ' . $patID;?> Client Events</h3>
</div>
<div class="description">
  <a class="btn btn-default btn-lg" href="<?php echo base_url() .'index.php/homeportal/viewpatInNewMethod/' . $patID . '/profile'; ?>"><i class="fa fa-chevron-left"></i> Back to <?php echo $patient -> LastName .' '.$patient->FirstName;?> Profile</a>
  <button class="btn btn-primary btn-lg" onclick="loadModal('<?php echo base_url() .'homeportal/eventModal/' . $patient ->ID; ?>')"><i class="fa fa-plus"></i> Event</button>| Discovering trends, increasing awareness
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DASHBOARD CONTENT -->

<!--  ACTIVITY -->
<div class="row">
  <div class="col-lg-12">
    <div class="box border">
      <div class="box-body">
        <div class="row">
          <div class="col-lg-12"><h1><?php echo $patient -> LastName .' '.$patient->FirstName;?>'s Events Dashboard</h1></div>
        </div>
        <div class="row">
          <div class="col-lg-1 center"></div>
          <div class="col-lg-10 center">
            <hr>
            <h2>Events for this year: <?php echo date("Y"); ?></h2>
            <canvas id="myChart" style="height: 600px;"></canvas>
          </div>
          <div class="col-lg-1"></div>
        </div>
      </div>
    </div>
  </div>
  <?php
    $current_date=date("Y-m-d");
    $patEvents = $this->portal->getPatEvents( $patID );
    $calData=array();
    foreach($patEvents->result () as $lists){
      $time=date('H:m',strtotime($lists->eventDate));
      switch ($lists->eventTypeID) {					
        case 1 :
          $type='Bowel Movement |';
          $color='#5cb85c'; 
        break;
        case 2 :
          $type='Sleep Event | ';
          $color='#5bc0de';
        break;
        case 3 :
          $type='PRN Administration | ';
          $color='#337ab7'; 
        break;
        case 4 :
          $type='Emgergency Room Visit | ';
          $color='#d9534f';
        break;
        case 5 :
          $type='Seizure | ';
          $color='#fcd76a';
        break;
        case 6 :
          $type='Diabetes- Blood glucose monitoring | ';
          $color='#d9534f';
        break;
        case 7 :
          $type='Urine Discharge | ';
          $color='#f0ad4e';
        break;
        case 8 :
          $type='Menstrual Cycle | ';
          $color='#EC971F';
        break;
        case 9 :
          $type='Behavior | ';
          $color='#f0ad4e';
        break;
        default :
      }
      $desc=$type.' '.$lists->desc.' at '.$time;
      $title=str_replace(',',' ',$desc);
      $title=str_replace("'",'',$title);
      $title=str_replace("/",'',$title);
      $title=str_replace("\n",'',$title);
      $calData[]=array('start'=>date('Y-m-d',strtotime($lists->eventDate)),'title'=>strip_tags(trim(stripslashes($title))),'end'=>date('Y-m-d',strtotime($lists->eventDate)),'color'=>$color);			
    }
  ?>
  <script>
    var noc=jQuery.noConflict();
    noc(document).ready(function() {
      noc('#calendar').fullCalendar({
        header: {
          left: 'prev,next today',
          center: 'title',
          right: 'month,listYear'
        },
        defaultDate: '<?php echo $current_date; ?>',
        editable: false,
        eventLimit: false, // allow "more" link when too many events
        events: [
          <?php if(!empty($calData)):
            foreach($calData as $kk=>$cal):?>
              {title: '<?php echo $cal['title']; ?>',start: '<?php echo $cal['start']; ?>',end: '<?php echo $cal['end']; ?>',color: '<?php echo $cal['color']; ?>'},
          <?php	endforeach;
          endif; ?>
        ]
      });

      $('.list_view').on('click',function(){
        $('.list-view').css('display','block');
        $('#calendar').css('display','none');	
      });
      $('.calender_view').on('click',function(){
        $('#calendar').css('display','block');
        $('.list-view').css('display','none');	
      });		
    });
  </script>
    
  <input type="hidden" value="<?php echo $patID; ?>" id="patID">
  <div class="col-lg-12">
    <div class="box border">
      <div class="box-title">
        <h4><i class="fa fa-reorder"></i> <span class="hidden-inline-mobile">Activity</span></h4>
        <div class="icons">
          <span class="list_view"><i class="fa fa-list" aria-hidden="true"></i></span>
          <span class="calender_view"><i class="fa fa-calendar-o" aria-hidden="true"></i></span>
        </div>
      </div>
      <div id='calendar'></div>
      <div class="box-body list-view" style="display:none;">
        <div data-always-visible="1" data-rail-visible="1">
        <?php
          $patEvents = $this->portal->getPatEvents ( $patID );
          if ($patEvents != false) {
            foreach ( $patEvents->result () as $event ) {
        ?>
        <div class="feed-activity clearfix">
          <div>
            <?php
              switch ($event->eventTypeID) {
                case 1 :
            ?><i class="pull-left roundicon fa fa-male btn btn-success"></i> <strong>Bowel Movement | </strong>
            <?php
                break;
                case 2 :
            ?><i class="pull-left roundicon fa fa-moon-o btn btn-info"></i>  <strong>Sleep Event | </strong>
            <?php
                break;
                case 3 :
            ?><i class="pull-left roundicon fa fa-edit btn btn-primary"></i>  <strong>PRN Administration | </strong><button class="btn btn-primary btn-sm" onclick="loadModal('<?php echo base_url() ."homeportal/prnAdminModal/" . $patID . '/' . $event->id ; ?>')"><i class="fa fa-eye"></i>  Monitor</button>
            <?php
                break;
                case 4 :
            ?><i class="pull-left roundicon fa fa-ambulance btn btn-danger"></i>  <strong>Emgergency Room Visit | </strong>
            <?php
                break;
                case 5 :
            ?><i class="pull-left roundicon fa fa-flash btn btn-yellow"></i>  <strong>Seizure | </strong>
            <?php
                break;
                case 6 :
            ?><i class="pull-left roundicon fa fa-tint btn btn-danger"></i>  <strong> Diabetes- Blood glucose monitoring | </strong>
            <?php
                break;
                case 7 :
            ?><i class="pull-left roundicon fa fa-edit btn btn-warning"></i>  <strong> Urine Discharge | </strong>
            <?php
                break;
                case 8 :
            ?><i class="pull-left roundicon fa fa-edit btn btn-warning"></i>  <strong> Menstrual Cycle | </strong>
            <?php
                break;
                case 9 :
            ?><i class="pull-left roundicon fa fa-edit btn btn-warning"></i>  <strong> Behavior | </strong>
            <?php
                break;
                default :
            ?><i class="pull-left roundicon fa fa-edit btn btn-primary"></i> 
            <?php
              }
            ?>
            <?php echo $event->desc ;?> <br>
          </div>
          <div class="time">
            <i class="fa fa-clock-o"></i> <?php echo $event->eventDate;?><button class="btn btn-default btn-lg"	onclick="loadModal('<?php echo base_url() .'homeportal/eventModal/' . $patID . '/' . $event->id ; ?>')"><i class="fa fa-edit"></i> Edit</button>
          </div>
        </div>
        <?php
            }
          } else {
            echo 'no events recorded';
          }
        ?>
      </div>
    </div>
  </div>
</div>
</div>
	
<div class="footer-tools">
  <span class="go-top"> <i class="fa fa-chevron-up"></i> Top</span>
</div>
