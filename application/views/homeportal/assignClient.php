<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left"> Manage Your Care Givers</h3>
</div>
<div class="description">

</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DELIVERY RUNS CONTENT -->
<!-- DATA TABLES -->
<div class="row" ng-app="myApp" ng-controller="DoubleController">
	<div class="col-md-6">

	
		<div class="box border">
			<div class="box-title">
				<h4>
					<i class="fa fa-ticket"></i> <span class="hidden-inline-mobile">Assign Patient for Care Giver</span>
				</h4>
			</div>
			
			
			<div class="box-body">				
					 
				
					<?php 
						
					   echo '<table class="table">
							    <thead> </thead>
							    <tbody>
							        <tr class="danger"> <td><h4>Care Givers Information  </h4> </td></tr> 
							      	<tr class="success"> <td> Name : '.$user->last_name.' '.$user->first_name.'</td> </tr>							      	
							      	<tr class="info">   <td> Email : '.$user->email.'</td> </tr>
							      	<input type="hidden" id="CG" name="CG" value="'.$user->id.'" />
							    </tbody>
							  </table>
							  
							  <table class="table">
							       <thead> <tr><th colspan="4"> Patients Name <span id="aMsg" class="label label-warning"> </span> </th></tr>	 </thead>
							       <tbody>	
							       <tr>						  
							  ';
							  $i = 0;				 
					     	  $y =0;				    
						  foreach($patInfo AS $record){
								if($i > 0 && $i % 4 === 0) {
					                echo '</tr><tr>';
					            }
					            $i++;								
								echo '<td id="getPatsInfo"><input type="checkbox" name="patId" id="'.$y++.'" '; 
								if(!empty($patID)) { if(in_array($record->ID , $patID)) { echo "checked"; } } 
								echo' value="'.$record->ID.'"/> '.$record->LastName .' '.$record->FirstName.'</td>';
							}
							
							echo '</tr>
						  	  </tbody>
						 	  </table>';
					?>
					<input type="hidden" id="patList" name="patList" />
					<button id="assignCG" onclick="assignPatToCG();" class="btn btn-success"> Update</button>		
					
							
			</div>
			
		  </div>
	  


	</div>
</div>
<!-- /PAGE -->


<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>

