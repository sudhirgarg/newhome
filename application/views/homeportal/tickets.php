<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left"><?php echo $this->lang->line('MENU_TICKETS');?></h3>
</div>
<div class="description">

</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DELIVERY RUNS CONTENT -->
<!-- DATA TABLES -->
<div class="row">
	<div class="col-md-12">

		<div class="box border">
			<div class="box-title">
				<h4>
					<i class="fa fa-ticket"></i> <span class="hidden-inline-mobile"><?php echo $this->lang->line('MENU_TICKETS');?></span>
				</h4>
			</div>
			<div class="box-body">

				<div class="row">

					<div class="col-lg-4">
						
						<table id="1" cellpadding="0" cellspacing="0" border="0"
							class="table table-striped table-bordered table-hover">
							<thead>
								<tr >
									<th colspan="2"><h3 class="center">
											<span class="label label-info"><?php echo $this->lang->line('GENERAL');?></span>
										</h3>
										           									          										

										</th>
								</tr>
							</thead>
							<tbody>
								
								<?php
									$ci =& get_instance();
									
									if(!empty($patInfo)){
										foreach($patInfo AS $info){
											$ci->getTicketsBaseONPatID(1,$info->ID);
										}										
									}	
									
									/**
									if(!empty($patAssigined)){							
										$a = 0;										
										for($a < 0 ; $a < count($patAssigined); $a++){
											$patId= $patAssigined[$a];
								 			$ci->getTicketsBaseONPatID(1,$patId);
							 			}										
									}
									 * 
									 */											
								?>
																
							</tbody>
						</table>
						
					</div>
					<div class="col-lg-4">
						<table id="1" cellpadding="0" cellspacing="0" border="0"
							class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th><h3 class="center">
											<span class="label label-warning"><?php echo $this->lang->line('RE_ORDER');?>
										
										</h3></th>
								</tr>
							</thead>
							<tbody>
							   <?php
									
									if(!empty($patInfo)){
										foreach($patInfo AS $info){											
											$ci->getTicketsBaseONPatID(2,$info->ID);
										}										
									}
									
									/*									
									if(!empty($patAssigined)){							
										$a = 0;										
										for($a < 0 ; $a < count($patAssigined); $a++){
											$patId= $patAssigined[$a];
								 			$ci->getTicketsBaseONPatID(2,$patId);
							 			}										
									}
									 * 
									 */												
								?>
							</tbody>
						</table>
					</div>
					<!-- div class="col-lg-4">
						<table id="1" cellpadding="0" cellspacing="0" border="0"
							class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th><h3 class="center">
											<span class="label label-success"><?php echo $this->lang->line('PHARMACIST');?>
										
										</h3></th>
								</tr>
							</thead>
							<tbody>
								
							    <?php
									
									if(!empty($patInfo)){
										foreach($patInfo AS $info){
											$ci->getTicketsBaseONPatID(3,$info->ID);
										}										
									}	
									
									/*
									if(!empty($patAssigined)){							
										$a = 0;										
										for($a < 0 ; $a < count($patAssigined); $a++){
											$patId= $patAssigined[$a];
								 			$ci->getTicketsBaseONPatID(3,$patId);
							 			}										
									}
									 * 
									 */										
								?>
							</tbody>
						</table>
					</div-->
					
				</div>
			</div>
		</div>


	</div>
</div>
<!-- /PAGE -->


<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>

