<?php $index='index.php/'; ?>
<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left"><?php	
if ($this->uri->segment ( 4 ) != "") {
		echo $this->uri->segment ( 4 );
	}else{
echo 'All ';
}
	?> <?php echo $this->lang->line('PATIENTS_TITLE');?></h3>
</div>
<div class="description">
    <a class="btn btn-default btn-lg"
		href="<?php echo base_url() ."<?php echo $index; ?>/homeportal/";?>">
		<i class="fa fa-chevron-left"></i> <?php echo $this->lang->line('BACK_TO_HOMES');?>
	</a>
    <p class="bg-danger"> 
       We have updates in our homeportal, kindly read the manual and setup your account. 
        <a href="<?php echo base_url();?><?php echo $index; ?>/homeportal/openPDF" target="_blank" >Please click here </a> <br/>
       
       **If you are a manager (Super User), and you still do not see your client information under your profile, kindly call us : 416-281-9900.
       Others kindly contact your managers for setup your account.<br/>
       Thank you
    </p>
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!--  MAIN PAGE HERE -->

<div class="row">
	<div class="col-lg-12">
		<div class="box border blue">
			<div class="box-title">
				<h4>
					<i class="fa fa-users"></i> <?php echo $this->lang->line('PATIENTS_TITLE');?>
				</h4>
			</div>
			<div class="box-body">
													
				 <table class="table table-hover">
				    <thead>
				      <tr>
				        <th>#</th>
				        <th>Pat ID</th>
				        <th>Lastname</th>
				        <th>Firstname</th>
				        <th>Tickets</th>
				        <th>Med Changes</th>
				        <th>Event</th>
				        						        
				      </tr>
				    </thead>
				    <tbody>
					<?php
				 		$i = 1;
						if(!empty($patInfo)){
							 $ci =& get_instance();
							
							foreach($patInfo as $record){
					 			echo '<tr>
							        <td>'.$i++.'</td>
							        <td><a href="'.base_url().''.$index.'homeportal/viewpatInNewMethod/'.$record->ID.'/profile"> <div class="btn btn-lg btn-primary"> '.$record->ID.' </div> </a> </td>
									<td>'.$record->LastName.'</td>
							        <td>'.$record->FirstName.'</td>	
							        <td> <a href="'.base_url().''.$index.'/homeportal/viewpatInNewMethod/'.$record->ID.'/tickets"> <div class="btn btn-lg btn-primary"> Tickets <span class="badge badge-info" style="background-color:blue;">'; 
									//$ci->getNumberOfTicketBaseOnPatId($record->ID);
									echo '</span>	 </div> </a>   </td>
							        <td> <a href="'.base_url().''.$index.'/homeportal/viewpatInNewMethod/'.$record->ID.'/medchanges"> <div class="btn btn-lg btn-warning"> Med Change <span class="badge badge-info" style="background-color:blue;">'; 
									//$ci->getNumberOfMedChangesBaseOnPatId($record->ID);
									echo '</span>	 </div> </a>   </td>	
							        <td> </td>					        
							      </tr>';
				 			}	
							
						}
					    
						/**						
						if(!empty($patAssigined)){
							$ci =& get_instance();
							$a = 0;
							print_r($patAssigined);
							foreach($patAssigined AS $record){
								
					 			echo '<tr>
							        <td>'.$i++.'</td>
							        <td><a href="'.base_url().'homeportal/viewpatInNewMethod/'.$record->ID.'/profile"> <div class="btn btn-lg btn-primary"> '.$record->id.' </div> </a> </td>
									<td></td>
							        <td></td>	
							        <td> <a href="'.base_url().'homeportal/viewpatInNewMethod/'.$record->id.'/tickets"> <div class="btn btn-lg btn-primary"> Tickets <span class="badge badge-info" style="background-color:blue;">'; 
									//$ci->getNumberOfTicketBaseOnPatId($patId);
									echo '</span>	 </div> </a>   </td>
							        <td> <a href="'.base_url().'homeportal/viewpatInNewMethod/'.$record->id.'/medchanges"> <div class="btn btn-lg btn-warning"> Med Change <span class="badge badge-info" style="background-color:blue;">'; 
									//$ci->getNumberOfMedChangesBaseOnPatId($patId);
									echo '</span>	 </div> </a>   </td>	
							        <td> </td>					        
							      </tr>';
				 			}	
							
						}
						 * **
						 */
				 							 		
				 	?>
				 	</tbody>
				</table> 	

			</div>
		</div>
	</div>
</div>

<!-- / MAIN PAGE HERE -->
</div>
<!-- /CALENDAR & CHAT -->
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>



