
<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left">Incidents</h3>
</div>
<div class="description">

	<button class="btn btn-default btn-lg"
		onclick="loadModal('<?php echo base_url("homeportal/editIncidentModal");?>')"> <i
		class="fa fa-plus"></i> Incident
	</button> | Discovering trends, increasing awareness
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DASHBOARD CONTENT -->



<!--  ACTIVITY -->
<div class="row">
<div class="col-lg-12">
		<div class="box border blue">
			<div class="box-title">
				<h4>
					<i class="fa fa-truck"></i>Incidents vs Months
				</h4>
				<div class="tools">
					<a href="#box-config" data-toggle="modal" class="config"> <i
						class="fa fa-cog"></i>
					</a> <a href="javascript:;" class="reload"> <i
						class="fa fa-refresh"></i>
					</a> <a href="javascript:;" class="collapse"> <i
						class="fa fa-chevron-up"></i>
					</a> <a href="javascript:;" class="remove"> <i class="fa fa-times"></i>
					</a>
				</div>
			</div>
			<div class="box-body">

				<div id="mychart" style="height: 200px;"></div>

			</div>
		</div>
	</div>
	<div class="col-lg-12">
		<!-- BOX -->
		<div class="box border blue">
			<div class="box-title">
				<h4>
					<i class="fa fa-reorder"></i> <span class="hidden-inline-mobile">
						Activity</span>
				</h4>
			</div>
			<div class="box-body">
				<table class="table">
					<thead>
						<tr>
							<th>Date</th>
							<th>Incident Type</th>
							<th>Description</th>
							<th>View</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><i class="fa fa-clock-o"></i> 3 hrs ago</td>
							<td>Med Error</td>
							<td>client missed noon dose...</td>
							<td><a class="btn btn-default btn-lg" href="<?php echo base_url();?>incidents/viewIncident">
									<i class="fa fa-eye fa-1x"></i> View
								</a></td>
						</tr>
						<tr>
							<td><i class="fa fa-clock-o"></i> 3 hrs ago</td>
							<td>Injury</td>
							<td>Client tripped and hurt their leg at the store...</td>
							<td><button class="btn btn-default btn-lg">
									<i class="fa fa-eye fa-1x"></i> View
								</button></td>
						</tr>
						<tr>
							<td><i class="fa fa-clock-o"></i> 3 hrs ago</td>
							<td>Med Error</td>
							<td>Client missed his dosage again today...</td>
							<td><button class="btn btn-default btn-lg">
									<i class="fa fa-eye fa-1x"></i> View
								</button></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<!-- /BOX -->


<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>
</div>
<!-- /CONTENT-->
</div>
</div>
</div>
</section>
<!--/PAGE -->
<!-- JAVASCRIPTS -->
<!-- Placed at the end of the document so the pages load faster -->
<!-- JQUERY -->
<script src="<?php echo base_url("js/jquery/jquery-2.0.3.min.js");?>"></script>
<!-- JQUERY UI-->
<script
	src=<?php echo base_url("js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js");?>></script>
<!-- BOOTSTRAP -->
<script
	src=<?php echo base_url("js/bootstrap-dist/js/bootstrap.min.js");?>></script>


<!-- DATE RANGE PICKER -->
<script
	src=<?php echo base_url("js/bootstrap-daterangepicker/moment.min.js");?>></script>

<script
	src=<?php echo base_url("js/bootstrap-daterangepicker/daterangepicker.min.js");?>></script>
<!-- SLIMSCROLL -->
<script type="text/javascript"
	src=<?php echo base_url("js/jQuery-slimScroll-1.3.0/jquery.slimscroll.min.js");?>></script>
<!-- SLIMSCROLL -->
<script type="text/javascript"
	src=<?php echo base_url("js/jQuery-slimScroll-1.3.0/jquery.slimscroll.min.js");?>></script>
<script type="text/javascript"
	src=<?php echo base_url("js/jQuery-slimScroll-1.3.0/slimScrollHorizontal.min.js");?>></script>
<!-- BLOCK UI -->
<script type="text/javascript"
	src=<?php echo base_url("js/jQuery-BlockUI/jquery.blockUI.min.js");?>></script>
<!-- SPARKLINES -->
<script type="text/javascript"
	src=<?php echo base_url("js/sparklines/jquery.sparkline.min.js");?>></script>
<!-- EASY PIE CHART -->
<script
	src=<?php echo base_url("js/jquery-easing/jquery.easing.min.js");?>></script>
<script type="text/javascript"
	src=<?php echo base_url("js/easypiechart/jquery.easypiechart.min.js");?>></script>
<!-- FLOT CHARTS -->
<script src=<?php echo base_url("js/flot/jquery.flot.min.js");?>></script>
<script src=<?php echo base_url("js/flot/jquery.flot.time.min.js");?>></script>
<script
	src=<?php echo base_url("js/flot/jquery.flot.selection.min.js");?>></script>
<script src=<?php echo base_url("js/flot/jquery.flot.resize.min.js");?>></script>
<script src=<?php echo base_url("js/flot/jquery.flot.pie.min.js");?>></script>
<script src=<?php echo base_url("js/flot/jquery.flot.stack.min.js");?>></script>
<script
	src=<?php echo base_url("js/flot/jquery.flot.crosshair.min.js");?>></script>
<!-- TODO -->
<script type="text/javascript"
	src=<?php echo base_url("js/jquery-todo/js/paddystodolist.js");?>></script>
<!-- TIMEAGO -->
<script type="text/javascript"
	src=<?php echo base_url("js/timeago/jquery.timeago.min.js");?>></script>
<!-- FULL CALENDAR -->
<script type="text/javascript"
	src=<?php echo base_url("js/fullcalendar/fullcalendar.min.js");?>></script>
<!-- COOKIE -->
<script type="text/javascript"
	src=<?php echo base_url("js/jQuery-Cookie/jquery.cookie.min.js");?>></script>
<!-- GRITTER -->
<script type="text/javascript"
	src=<?php echo base_url("js/gritter/js/jquery.gritter.min.js");?>></script>
<!-- CUSTOM SCRIPT -->
<script src=<?php echo base_url("js/script.js");?>></script>
<script>
		jQuery(document).ready(function() {		

			var d2 = [ { 
				label: "type 1", 
				data: [ [1, 75], [2, 72], [3, 62], [4, 79], [5, 60], [6, 37], [7, 58], [8, 59], [9, 64], [10, 99], [11, 80], [12, 86] ],
				color: "green"

			},
			           { label: "type 2", data: [ [1, 11], [2, 30], [3, 40], [4, 70], [5, 50], [6, 80], [7, 50], [8, 20], [9, 50], [10, 50], [11, 50], [12, 27] ],
				 color: "blue" },
		           { label: "type 3", data: [ [1, 100], [2, 100], [3, 100], [4, 100], [5, 100], [6,100], [7,100], [8, 100], [9, 100], [10, 100], [11,100], [12,100] ],
					 color: "red" }
			];

			$.plot("#mychart", d2);
			App.setPage("viewCommittee");  //Set current page
			App.init(); //Initialise plugins and elements
		});
	</script>
<!-- /JAVASCRIPTS -->