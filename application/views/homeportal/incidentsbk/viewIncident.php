<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left">Indient</h3>
</div>
<div class="description">

	<a class="btn btn-default btn-lg" href="#ActionConfirmation">Archive</a>




</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DELIVERY RUNS CONTENT -->
<!-- DATA TABLES -->
<div class="row">
	<div class="col-md-12">



		<div class="box border primary">
			<div class="box-title">
				<h4>
					<i class="fa fa-bars"></i>Incident details
				</h4>

			</div>
			<div class="box-body big">

				<form id="fileupload"
					action="http://jquery-file-upload.appspot.com/" method="POST"
					enctype="multipart/form-data">
					<!-- Redirect browsers with JavaScript disabled to the origin page -->
					<noscript>
						<input type="hidden" name="redirect"
							value="http://blueimp.github.io/jQuery-File-Upload/">
					</noscript>
					<!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
					<div class="divide-20"></div>
					<div class="row fileupload-buttonbar">
						<div class="col-lg-12">
							<!-- The fileinput-button span is used to style the file input field as button -->
							<span class="btn btn-success fileinput-button"> <i
								class="fa fa-plus"></i> <span>Add files</span> <input
								type="file" name="files[]" multiple>
							</span>
							<button type="submit" class="btn btn-primary start">
								<i class="fa fa-arrow-circle-o-up"></i> <span>Start upload</span>
							</button>
							<button type="reset" class="btn btn-warning cancel">
								<i class="fa fa-ban"></i> <span>Cancel upload</span>
							</button>
							<button type="button" class="btn btn-danger delete">
								<i class="fa fa-trash-o"></i> <span>Delete</span>
							</button>
							<input type="checkbox" class="toggle">
							<!-- The loading indicator is shown during file processing -->
							<span class="fileupload-loading"></span>
						</div>
						<!-- The global progress information -->
						<div class="col-lg-5 fileupload-progress fade">
							<!-- The global progress bar -->
							<div class="progress progress-striped active" role="progressbar"
								aria-valuemin="0" aria-valuemax="100">
								<div class="progress-bar progress-bar-success"
									style="width: 0%;"></div>
							</div>
							<!-- The extended global progress information -->
							<div class="progress-extended">&nbsp;</div>
						</div>
					</div>
					<!-- The table listing the files available for upload/download -->
					<table role="presentation" class="table table-striped">
						<tbody class="files"></tbody>
					</table>
				</form>


				<form class="form-horizontal" role="form">




					<div class="form-group">
						<label class="col-sm-3 control-label">Incident Date</label>
						<div class="col-sm-9">
							<input type="text" name="regular"
								class="form-control datepicker-fullscreen">
						</div>
					</div>


					<div class="form-group">
						<label class="col-sm-3 control-label">Reported By</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" placeholder="Text input">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label">Reported To</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" placeholder="Text input">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label">Description</label>
						<div class="col-sm-9">
							<textarea class="form-control" rows="3"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label">Action Taken</label>
						<div class="col-sm-9">
							<textarea class="form-control" rows="3"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label">Stages Involved</label>
						<div class="col-sm-9">
						    <div class="btn-group" data-toggle="buttons">
						    	<label class="btn btn-primary"><input type="checkbox">Dispensory</label>
						    	<label class="btn btn-primary"><input type="checkbox">Delivery</label>
						    	<label class="btn btn-primary"><input type="checkbox">Data Entry</label>
						    </div>
						</div>
					</div>


					<div class="form-group">
						<label class="col-sm-3 control-label">DIN</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" placeholder="Text input">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label">Incident Type</label>
						<div class="col-sm-9">
							<select class="form-control">
								<option>Front Shop</option>
								<option>Back Shop</option>
								<option>Delivery</option>
								<option>Rx Entry</option>
								<option>Wrong Patient</option>
								<option>Patient Confidentiality</option>
							</select>

						</div>
					</div>


					<div class="form-group">
						<label class="col-sm-3 control-label">ISMP Outcome</label>
						<div class="col-sm-9">
							<select class="form-control">
								<option>A</option>
								<option>B</option>
								<option>C</option>
								<option>D</option>
								<option>E</option>
							</select>

						</div>
					</div>


					<div class="form-group">
						<label class="col-sm-3 control-label"></label>
						<div class="col-sm-9">
							<a class="btn btn-default btn-lg">Save</a>
						</div>
					</div>

				</form>
			</div>
		</div>



	</div>
</div>
<!-- /CONTENT -->



<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>
</div>
<!-- /CONTENT-->
</div>
</div>
</div>
</section>
<!--/PAGE -->
<!-- JAVASCRIPTS -->
<!-- Placed at the end of the document so the pages load faster -->
<!-- JQUERY -->
<script src="<?php echo base_url()?>js/jquery/jquery-2.0.3.min.js"></script>
<!-- JQUERY UI-->
<script
	src="<?php echo base_url()?>js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
<!-- BOOTSTRAP -->
<script
	src="<?php echo base_url()?>js/bootstrap-dist/js/bootstrap.min.js"></script>


<!-- DATE RANGE PICKER -->
<script
	src="<?php echo base_url()?>js/bootstrap-daterangepicker/moment.min.js"></script>
<script
	src="<?php echo base_url()?>js/bootstrap-daterangepicker/daterangepicker.min.js"></script>
<!-- SLIMSCROLL -->
<script type="text/javascript"
	src="<?php echo base_url()?>js/jQuery-slimScroll-1.3.0/jquery.slimscroll.min.js"></script>
<script type="text/javascript"
	src="<?php echo base_url()?>js/jQuery-slimScroll-1.3.0/slimScrollHorizontal.min.js"></script>
<!-- BLOCK UI -->
<script type="text/javascript"
	src="<?php echo base_url()?>js/jQuery-BlockUI/jquery.blockUI.min.js"></script>
<!-- SPARKLINES -->
<script type="text/javascript"
	src="<?php echo base_url()?>js/sparklines/jquery.sparkline.min.js"></script>
<!-- EASY PIE CHART -->
<script
	src="<?php echo base_url()?>js/jquery-easing/jquery.easing.min.js"></script>
<script type="text/javascript"
	src="<?php echo base_url()?>js/easypiechart/jquery.easypiechart.min.js"></script>
<!-- FLOT CHARTS -->
<script src="<?php echo base_url()?>js/flot/jquery.flot.min.js"></script>
<script src="<?php echo base_url()?>js/flot/jquery.flot.time.min.js"></script>
<script
	src="<?php echo base_url()?>js/flot/jquery.flot.selection.min.js"></script>
<script src="<?php echo base_url()?>js/flot/jquery.flot.resize.min.js"></script>
<script src="<?php echo base_url()?>js/flot/jquery.flot.pie.min.js"></script>
<script src="<?php echo base_url()?>js/flot/jquery.flot.stack.min.js"></script>
<script
	src="<?php echo base_url()?>js/flot/jquery.flot.crosshair.min.js"></script>
<!-- TODO -->
<script type="text/javascript"
	src="<?php echo base_url()?>js/jquery-todo/js/paddystodolist.js"></script>
<!-- TIMEAGO -->
<script type="text/javascript"
	src="<?php echo base_url()?>js/timeago/jquery.timeago.min.js"></script>


<!-- DATE PICKER -->
<script type="text/javascript"
	src="<?php echo base_url()?>js/datepicker/picker.js"></script>
<script type="text/javascript"
	src="<?php echo base_url()?>js/datepicker/picker.date.js"></script>
<script type="text/javascript"
	src="<?php echo base_url()?>js/datepicker/picker.time.js"></script>

<!-- FULL CALENDAR -->
<script type="text/javascript"
	src="<?php echo base_url()?>js/fullcalendar/fullcalendar.min.js"></script>
<!-- GOOGLE MAPS -->
<script src="http://maps.google.com/maps/api/js?sensor=true"
	type="text/javascript"></script>
<script type="text/javascript"
	src="<?php echo base_url()?>js/gmaps/gmaps.min.js"></script>
<!-- COOKIE -->
<script type="text/javascript"
	src="<?php echo base_url()?>js/jQuery-Cookie/jquery.cookie.min.js"></script>
<!-- GRITTER -->
<script type="text/javascript"
	src="<?php echo base_url()?>js/gritter/js/jquery.gritter.min.js"></script>
<!-- CUSTOM SCRIPT -->
<script src="<?php echo base_url()?>js/script.js"></script>
<script src="<?php echo base_url()?>js/googlemaps.js"></script>
<script>
		jQuery(document).ready(function() {		

			$(".datepicker-fullscreen").pickadate();
			
			App.setPage("ViewIncident");  //Set current page
			App.init(); //Initialise plugins and elements
			MapsGoogle.init(); //Init the google maps
		});
	</script>
<!-- /JAVASCRIPTS -->
s

