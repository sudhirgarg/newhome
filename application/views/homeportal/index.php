<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left"><?php echo $this->lang->line('HOMES');?></h3>
</div>
<div class="description">
	
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DELIVERY RUNS CONTENT -->
<!-- DATA TABLES -->
<div class="row">
	<div class="col-md-12">

		<div class="well well-sm">
			<h1><?php echo $this->lang->line('HELLO');?> <?php echo $this->ion_auth->user()->row()->first_name;?> </h1>
		</div>


		<div class="box border">
			<div class="box-title">
				<h4>
					<i class="fa fa-ticket"></i> <span class="hidden-inline-mobile"><?php echo $this->lang->line('HOMES');?></span>
				</h4>
			</div>
			<div class="box-body">
				<table id="datatable1" cellpadding="0" cellspacing="0" border="0"
					class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th><?php echo $this->lang->line('HOMES');?></th>
							<th><?php echo $this->lang->line('ADDRESS');?></th>
							<th><?php echo $this->lang->line('CONTACT');?></th>
							<th><?php echo $this->lang->line('PHONE');?></th>
						</tr>
					</thead>
					<tbody>
					<?php
					$groups = $this->portal->getHome ();
					if ($groups) {
						foreach ( $groups->result () as $group ) {
							?>
						<tr onclick="window.location='<?php echo base_url("index.php/homeportal/patients") . "/" . $group->homeID . "/" . $group->homeName;?>'">
							<td><a class="btn btn-lg btn-primary" href="<?php echo base_url("index.php/homeportal/patients") . "/" . $group->homeID . "/" . $group->homeName ;?>" ><?php echo $group->homeName;?></a></td>
							<td><?php echo $group->homeAddress;?></td>
							<td><?php echo $group->contact;?></td>
							<td><?php echo $group->phone;?></td>
						</tr>
						<?php }}else{?> <tr>
							<td>No data</td>
						</tr> <?php }?>
					</tbody>
				</table>
			</div>
		</div>


	</div>
</div>
<!-- /PAGE -->


<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>


