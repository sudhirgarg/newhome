<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>Seamless Home Portal</title>

<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>css/cloud-admin.css">
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>css/themes/default.css"
	id="skin-switcher">
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>css/responsive.css">
<!-- STYLESHEETS -->
<!--[if lt IE 9]><script src="js/flot/excanvas.min.js"></script><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
<!-- ANIMATE -->
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>css/animatecss/animate.min.css" />
<!-- DATE RANGE PICKER -->
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
<!-- DATA TABLES -->
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>js/datatables/media/css/jquery.dataTables.min.css" />
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>js/datatables/media/assets/css/datatables.min.css" />
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>js/datatables/extras/TableTools/media/css/TableTools.min.css" />

<!-- TABLE CLOTH -->
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>js/tablecloth/css/tablecloth.min.css" />
<!-- TODO -->
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>js/jquery-todo/css/styles.css" />
<!-- FULL CALENDAR -->
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>js/fullcalendar/fullcalendar.min.css" />
<!-- GRITTER -->
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>js/gritter/css/jquery.gritter.css" />
<!-- XCHARTS -->
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>js/xcharts/xcharts.min.css" />

<!-- DATE PICKER -->
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>js/datepicker/themes/default.min.css" />
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>js/datepicker/themes/default.date.min.css" />
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>js/datepicker/themes/default.time.min.css" />


<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>css/custom.css" />

<!-- FONTS -->
<link
	href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'
	rel='stylesheet' type='text/css'>
<link
	href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css"
	rel="stylesheet">
</head>
<body onload="window.print()">
	<!-- HEADER -->

	<!-- PAGE -->
	<section id="page">
		
	
<?php

$patID = $this->uri->segment ( 3 );
$patient = $this->portal->getCompliancePatient ( $this->uri->segment ( 3 ) );
 $ci =& get_instance();

?>

<div class="center">
			<img src="<?php echo base_url();?>img/seamlogo.gif"
				alt="Seamless Care Pharmacy" height="50px" width="300px"> <br> <br>
			<br> <br>
			<p>Please note that this document may not be accurate. Please call
				Seamless Care Pharmacy to get an accurate client profile</p>
			<p>Address: 360 King St W. Oshawa, ON L1J 2J9 Tel.: 1-877-666-9502
				Tel.: (905) 432-9900 Fax: (905) 432-9905</p>
		</div>

		<div class="form-horizontal" role="form">

			<div class="box border purple center">
				<div class="box-title">
					<h1>
						Profile For: <i class="fa fa-user"></i> <?php  echo $patient->patInitials . '-' . $patient->patID;?></span>
					</h1>
					<div class="tools hidden-xs">
						<button href="#box-config" onClick="window.print()"
							data-toggle="modal" class="config">
							<i class="fa fa-print"></i>
						</button>
					</div>
				</div>
				<div class="box-body big">
					<div class="row">
						<div class="col-lg-8">


							<div class="col-lg-4">
								<h2>Allergies</h2>
								<p>
								<div id="alergiesTable">
									 <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover datatable table-responsive">
									 <thead >
											<tr>												
												<th>Allergy</th>
												<th>Comment</th>
												
											</tr>
										</thead>
										<tbody>
									<?php 
									
										foreach($allergy AS $aRecord){
											echo '<tr>
												<td>'; $ci->getAllergyInfo($aRecord->Code);  echo'</td>
												<td>'.$aRecord->Comment.'</td>																	
											</tr>';
											
										}
									
									?>
										</tbody>
									</table>
								</div>
								</p>
							</div>

							<div class="col-lg-4">
								<h2>Conditions</h2>
								<div id="conditionsTable">
									<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover datatable table-responsive">
									 <thead >
											<tr>												
												<th>Conditions</th>
												
												
											</tr>
										</thead>
										<tbody>
									<?php 
										foreach($condition AS $aRecord){
											echo '<tr>
												<td>'; $ci->getConditionsInfo($aRecord->Code);  echo'</td>
																												
											</tr>';
											
										}
									
									?>
										</tbody>
									</table>
									
								</div>
							</div>
							<div class="col-lg-4">
								<h2>Note</h2>
								<p><?php echo $patient->note;?></p>
							</div>
							<br>
						</div>
					</div>
				</div>
			</div>

			<div class="box border purple center">
				<div class="box-title">
					<h4>Compliance Package</h4>
				</div>
				<div class="box-body big">
					<div class="row">
						<div class="col-lg-12">
							<div class="box-body">
															
								<table id="" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover datatable table-responsive">
									
									 <thead >
											<tr>												
												<th>Medication</th>
												<th>Doctor</th>
												<th>Directions</th>
											</tr>
										</thead>
										<tbody>
											<?php																	
										
												foreach ($patRx as $drg) {
													if($drg->NHBatchFill == 1){
														echo '<tr>';
														echo '<td>' . $drg -> BrandName . ' ' . $drg -> Strength . '<br>RX-' . $drg -> RxNum . '</td>';
														echo '<td>' . $drg -> doc.'</td>';
														echo '<td>' . $drg -> SIG . '</td>';
														echo '</tr>';
													}
													
												} 
																			
											?>
										</tbody>
								</table>
							</div>
							<br>
						</div>
					</div>
				</div>
			</div>

			<div class="box border purple center">
				<div class="box-title">
					<h4>Other Medication</h4>
				</div>
				<div class="box-body big">
					<div class="row">
						<div class="col-lg-12">
							<div class="box-body">
															
									<table id="" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover datatable table-responsive">
										
										 <thead >
												<tr>												
													<th>Medication</th>
													<th>Doctor</th>
													<th>Directions</th>
												</tr>
											</thead>
											<tbody>
												<?php																	
											
													foreach ($patRx as $drg) {
														if($drg->NHBatchFill == 2){
															echo '<tr>';
															echo '<td>' . $drg -> BrandName . ' ' . $drg -> Strength . '<br>RX-' . $drg -> RxNum . '</td>';
															echo '<td>' . $drg -> doc.'</td>';
															echo '<td>' . $drg -> SIG . '</td>';
															echo '</tr>';
														}
														
													} 
																				
												?>
											</tbody>
									</table>
								</div>
							<br>
						</div>
					</div>
				</div>
			</div>

			<div class="box border purple center">
				<div class="box-title">
					<h4>Med Changes</h4>
				</div>
				<div class="box-body big">
					<div class="row">
						<div class="col-lg-12">
							<div>

								<table cellpadding="0" cellspacing="0" border="0"
									class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Recorded On</th>
											<th>Tech</th>
											<th>Pharmacist</th>
											<th>Starts</th>
											<th>Change</th>
											<th>Action</th>
											<th>Action Completed</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$medChange = $this->comp->getMedChange ( '-1', $patient->patID );
										if ($medChange != FALSE) {
											foreach ( $medChange->result () as $medChangeItem ) {
												?>
											<tr
											onclick="loadModal('<?php echo base_url('homeportal/editMedChangeModal') . '/' . $patient->patID . '/' . $medChangeItem->medChangeID ;?>')">
											<td><?php echo $medChangeItem->dateOfChange;?></td>
											<td><?php $tech = $this->ion_auth->user($medChangeItem->technicianUserID)->row(); echo $tech->first_name;?></td>
											<td><?php if($medChangeItem->pharmacistUserID > 0) {$pharm = $this->ion_auth->user($medChangeItem->pharmacistUserID)->row(); echo $pharm->first_name;}?></td>


											<td><?php echo $medChangeItem->startDate;?></td>
											<td><?php echo $medChangeItem->changeText;?></td>
											<td><?php echo $medChangeItem->actionText;?></td>
											<td><?php
												
												if ($medChangeItem->actionComplete > 0) {
													?>
													<span class="badge badge-green"><i class="fa fa-check"></i></span>
												<?php
													;
												}
												?></td>
										</tr>
											<?php
											}
										} else {
											echo '<tr><td colspan="7"><div class="center">No MED CHANGES</div></td></tr>';
										}
										?>
										</tbody>
								</table>
							</div>
							<br>
						</div>
					</div>
				</div>
			</div>



			<div class="row">
				
				<div class="col-lg-12">
					<!-- BOX -->
					<div class="box border blue">
						<div class="box-title">
							<h4>
								<i class="fa fa-reorder"></i> <span>
									Event Activity</span>
							</h4>
						</div>
						<div class="box-body">
							<div data-always-visible="1" data-rail-visible="1">
					<?php
					$patEvents = $this->portal->getPatEvents ( $patID );
					if ($patEvents != false) {
						foreach ( $patEvents->result () as $event ) {
							?>
					<div class="feed-activity clearfix">
									<div>
						<?php
							
							switch ($event->eventTypeID) {
								
								case 1 :
									?><i class="pull-left roundicon fa fa-male btn btn-success"></i> Bowel Movement: 									
																 
																<?php
									break;
								case 2 :
									?>
									<i class="pull-left roundicon fa fa-moon-o btn btn-info"></i> Sleep Event: 
																
																<?php
									break;
								case 3 :
									?>
																	<i
											class="pull-left roundicon fa fa-edit btn btn-primary"></i> PRN Administration: 
																<?php
									break;
								case 4 :
									?>
									<i class="pull-left roundicon fa fa-ambulance btn btn-danger"></i> Emgergency Room Visit: 
															 
																<?php
									break;
								case 5 :
									?>
																<i
											class="pull-left roundicon fa fa-flash btn btn-yellow"></i>  Seizure: 
																<?php
									break;
								default :
									?>
																								<i
											class="pull-left roundicon fa fa-edit btn btn-primary"></i> 
																								<?php
							}
							?>
							<?php echo $event->desc;?> <br>
									</div>
									<div class="time">
										<i class="fa fa-clock-o"></i> <?php echo timespan(strtotime($event->eventDate), time()) . ' ago';?>
						</div>
								</div>
					
					<?php
						
}
					} else {
						echo 'no events recorded';
					}
					?>

				</div>

						</div>
					</div>
				</div>
			</div>
	
	</section>
	<!--/PAGE -->
	<!-- JAVASCRIPTS -->
	<!-- Placed at the end of the document so the pages load faster -->
	<!-- JQUERY -->
	<script src="<?php echo base_url();?>js/jquery/jquery-2.0.3.min.js"></script>
	<!-- JQUERY UI-->
	<script
		src="<?php echo base_url();?>js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
	<!-- BOOTSTRAP -->
	<script src="<?php echo base_url();?>js/bootstrap.min.js"></script>

	<!-- Data Tables-->
	<script
		src="<?php echo base_url();?>js/datatables/media/js/jquery.dataTables.min.js"></script>


	<!-- FLOT CHARTS -->
	<script src=<?php echo base_url("js/flot/jquery.flot.min.js");?>></script>
	<script src=<?php echo base_url("js/flot/jquery.flot.time.min.js");?>></script>
	<script
		src=<?php echo base_url("js/flot/jquery.flot.selection.min.js");?>></script>
	<script src=<?php echo base_url("js/flot/jquery.flot.resize.min.js");?>></script>
	<script src=<?php echo base_url("js/flot/jquery.flot.pie.min.js");?>></script>
	<script src=<?php echo base_url("js/flot/jquery.flot.stack.min.js");?>></script>
	<script
		src=<?php echo base_url("js/flot/jquery.flot.crosshair.min.js");?>></script>

	<!--  FOR STACKED AREA CHARTS -->
	<script type="text/javascript"
		src=<?php echo base_url("/js/jquery-1.8.3.min.js");?>></script>
	<script type="text/javascript"
		src=<?php echo base_url("/js/flot/jquery.flot.js");?>></script>
	<script type="text/javascript"
		src=<?php echo base_url("/js/flot/jquery.flot.axislabels.js");?>></script>
	<script type="text/javascript"
		src=<?php echo base_url("/js/flot/jquery.flot.stack.js");?>></script>
	<script type="text/javascript"
		src=<?php echo base_url("/js/flot/jshashtable-2.1.js");?>></script>
	<script type="text/javascript"
		src=<?php echo base_url("/js/flot/jquery.numberformatter-1.2.3.min.js");?>></script>
	<script type="text/javascript"
		src=<?php echo base_url("js/flot/jquery.flot.time.js");?>></script>
	<!--   -------------------- -->

	<!-- DATE RANGE PICKER -->
	<script
		src="<?php echo base_url();?>js/bootstrap-daterangepicker/moment.min.js"></script>
	<script
		src="<?php echo base_url();?>js/bootstrap-daterangepicker/daterangepicker.min.js"></script>
	<!-- DATE PICKER -->
	<script type="text/javascript"
		src="<?php echo base_url()?>js/datepicker/picker.js"></script>
	<script type="text/javascript"
		src="<?php echo base_url()?>js/datepicker/picker.date.js"></script>
	<script type="text/javascript"
		src="<?php echo base_url()?>js/datepicker/picker.time.js"></script>
	<!-- FULL CALENDAR -->
	<script type="text/javascript"
		src="<?php echo base_url()?>js/fullcalendar/fullcalendar.min.js"></script>
	<!-- SLIMSCROLL -->
	<script type="text/javascript"
		src="<?php echo base_url();?>js/jQuery-slimScroll-1.3.0/jquery.slimscroll.min.js"></script>
	<script type="text/javascript"
		src="<?php echo base_url();?>js/jQuery-slimScroll-1.3.0/slimScrollHorizontal.min.js"></script>
	<!-- COOKIE -->
	<script type="text/javascript"
		src="<?php echo base_url();?>js/jQuery-Cookie/jquery.cookie.min.js"></script>
	<!-- CUSTOM SCRIPT -->
	<script src="<?php echo base_url();?>js/script.js"></script>
	<script>
		jQuery(document).ready(function() {		

			$(".datepicker-fullscreen").pickadate();
			
			App.setPage("widgets_box");  //Set current page
			App.init(); //Initialise plugins and elements
		});
	</script>
<?php $this->load->view('extras'); ?>
<script>

		function searchPat()
		{
			var patID = $("#patSearch").val();
			window.location.href = "<?php echo base_url();?>compliance/viewPat/" + patID + "/profile";
		}
		
		function globalSuccess(successMessage)
		{
			$("#successMessageContent").html(successMessage);
			$("#successMessage").fadeIn().delay(3500).fadeOut();
		}

		function globalError(errorMessage)
		{
			$("#errorMessageContent").html(errorMessage);
			$("#errorMessage").fadeIn().delay(3500).fadeOut();
		}

		function loadModal(ajaxURL)
		{
			$.get(ajaxURL, function(data) {
				$("#myModalContent").html(data);
				$('#myModal').modal();
			});
		}

		function read(data)
		{
			var args = data.split('|');
			if (args[0] == 0)
			{
				globalError(args[1]);
			} else {
				globalSuccess(args[1]);
			}
		}
	</script>
	<!-- /JAVASCRIPTS -->
<?php $this->load->view('js/footer');?>

</body>
</html>
