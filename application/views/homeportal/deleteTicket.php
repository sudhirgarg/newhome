<?php
$isNew = true;
$patID = $this->uri->segment ( 3 );
$patient = $this->comp->getCompliancePatient($patID);
$ticketID = 0;
$Ticket;
$user = $this->ion_auth->user ()->row ();
if ($this->uri->segment ( 4 ) > 0) {
	$isNew = false;
	$ticketID = $this->uri->segment ( 4 );
	$Ticket = $this->comp->getTicket ( $this->uri->segment ( 4 ) );
}
?>



	<div class="modal-header">
	
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	
		<h4 class="modal-title">Are you sure? (Note: You will lose this ticket data, once you confirm!)</h4>
		<?php if ($isNew==false){ ?>
		<!--a href="<?php echo base_url().'homeportal/deleteTicketConfirm/'. $Ticket->ticketID; ?>"> <button type="button" class="btn btn-danger" id="deleteButton"
			 data-loading-text="deleting..."><i class="fa fa-trash-o"></i> Delete Ticket</button> </a-->
			 
			  <input id="ticketID" type="hidden" value="<?php echo $Ticket->ticketID; ?>">
			  <button type="button" class="btn btn-danger" id="deleteButton" onclick="deleteTicketButton()" data-loading-text="deleting..."><i class="fa fa-trash-o"></i> Delete Ticket</button>
		 
		 
			<?php } ?>
	</div>
	
	
	
	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal" id="closeButtonModal">Close</button>		
		
	</div>
