<?php
  
    $ci =& get_instance();
?>


<!DOCTYPE html>
<html lang="en">
<head>
	
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title> <?php if($this->uri->segment(2) == "NULL") { echo "Home"; } else { echo $this->uri->segment(2); } ?> </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/cloud-admin.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/themes/default.css" id="skin-switcher">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">		

   <link rel="stylesheet" href = "http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">	
   <style>
       .border-bottom {
            border-bottom: 1px solid black;
        }
        
         body {
        width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
        background-color: #FAFAFA;
        font: 12pt "Tahoma";
	    }
	    * {
	        box-sizing: border-box;
	        -moz-box-sizing: border-box;
	    }
	    .page {
	        width: 210mm;
	        min-height: 297mm;
	        margin: 10mm auto;
	        border-radius: 5px;
	        background: white;
	        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
	    }
	    .subpage {
	        padding: 1cm;
	        height: 257mm;
	    }
	    
	    @page {
	        size: A4;
	        margin: 0;
	    }
	    @media print {
	        html, body {
	            width: 210mm;
	            height: 297mm;        
	        }
	        .page {
	            margin: 0;
	            border: initial;
	            border-radius: initial;
	            width: initial;
	            min-height: initial;
	            box-shadow: initial;
	            background: initial;
	            page-break-after: always;
	        }
	    }
   </style>
</head>
<body onLoad="myPrint();">
  

<!--***************************************** PAGE BODY STARTS ***************************************************************-->
<div class="book">




<div class="page">
<div class="subpage">
<div class="row">
    <div class="col-md-8 pull-center" style="padding-left:50px;padding-right:50px;">
    
        <img src="<?php echo base_url();?>img/logo/logo.png" width="250px">
    <table width="100%" border="1">
        <tr>
            <td>
                <div class="panel panel-default">
                <div class="panel-body">
                    <table width="100%">
                        <tr>
                            <td>
                            
                             
                                 <?php 
                                     $day ="";
	                                 if($days == 'MON')    { $day = 'Monday'; }
	                                 elseif($days == 'TUE'){ $day = 'Tuesday'; }
	                                 elseif($days == 'WED'){ $day = 'Wednesday'; }
	                                 elseif($days == 'THU'){ $day = 'Thursday'; }
	                                 elseif($days == 'FRI'){ $day = 'Friday'; }
	                                 elseif($days == 'SAT'){ $day = 'Saturday'; }
	                                 elseif($days == 'SUN'){ $day = 'Sunday'; }
	                                 else { $day = date('l'); } 
	                                 echo '<h2>'.$day.' Delivery Details</h2>';
                                 ?>
                                 
                                 
                                
                            </td> 
                        </tr>    
                        <tr>
                            <td>    
                               <table width="100%" style="border:1px;">
                                   <tr>
                                      <td> # </td>
                                      <td> Name </td>
                                      <td> Time </td>
                                   </tr>
                                     <?php 
                                        $i = 1;
                                          foreach($deliveryDetails AS $record){
                                          	   echo '<tr>
				                                      <td>'.$i++.'</td>
				                                      <td>'.$record->Home.' '.$record->FirstName.'</td>
				                                      <td>'.$record->timeDay.'</td>
				                                   </tr>';
                                          }
                                     ?>
                               </table>
                                                                
                            </td> 
                        </tr>
                       
                    </table>
                </div>
                </div>
            </td>              
        </tr>
     
        
       
    </table>
    
    
    
	    </div>
		</div>
    </div>
</div>





<!--***************************************** PAGE BODY ENDS ***************************************************************-->

<script>
    function myPrint() {
        window.print();
    }
        
</script>

</body>
</html>