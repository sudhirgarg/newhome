<div class="alert alert-block alert-success" id="successMessage" style="display:none; width:100%; max-width:300px; position:fixed; top:65px; right:15px;">
	<a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
	<p></p><h4><i class="fa fa-checkmark"></i></h4> 
	<p id="successMessageContent"></p>
	<p></p>
</div>

<div class="alert alert-block alert-danger" id="errorMessage" style="display:none; width:100%; max-width:300px; position:fixed; top:65px; right:15px;">
	<a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
	<p></p><h4><i class="fa fa-times"></i></h4> 
	<p id="errorMessageContent"></p>
	<p></p>
</div>


<div class="modal fade" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content" id="myModalContent">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body">
        <p>One fine body&hellip;</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->