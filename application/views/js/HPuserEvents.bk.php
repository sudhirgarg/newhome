<script>

function myDateFormatter (dateObject) {
    var d = new Date(dateObject);
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    if (day < 10) {
        day = "0" + day;
    }
    if (month < 10) {
        month = "0" + month;
    }
    var date = year + "-" + month + "-" + day;

    return date;
}; 


function saveEvent()
{
	
	var time = $("#eventTime").val();
	var hours = Number(time.match(/^(\d+)/)[1]);
	var minutes = Number(time.match(/:(\d+)/)[1]);
	var AMPM = time.match(/\s(.*)$/)[1];
	if(AMPM == "PM" && hours<12) hours = hours+12;
	if(AMPM == "AM" && hours==12) hours = hours-12;
	var sHours = hours.toString();
	var sMinutes = minutes.toString();
	if(hours<10) sHours = "0" + sHours;
	if(minutes<10) sMinutes = "0" + sMinutes;
	var time24 = sHours + ":" + sMinutes;
	
	 $("#saveEventButton").button('loading');
	    $.post('<?php echo site_url('homeportal/saveEvent');?>',
	             {
	        eventID: $("#eventID").val(),
	        eventTypeID: $("#eventTypeID").val(),
	        eventDate: myDateFormatter($("#eventDate").val()) + " " +  time24,
	        reportedByUser: $("#reportedByUser").val(),
	        patID: $("#patID").val(),
	        desc: $("#desc").val()
	        }, 
	    function (data) {
	        
	        $("#closeButtonModal").click();
	        read(data);
	        var args = data.split('|');
	        if (args[0] == 1)
	        {
	            setTimeout(function() {
	                location.reload();
	                }, 1000);	                
	        }
	    });	
}

function savePRNMonitor()
{
	var effectiveDateTime = "";
	if ($("#dateEffective").val() == "" && $("#timeEffective").val() == "")
	{
		effectiveDateTime ="";
	}else
	{
		var time = $("#timeEffective").val();
		var hours = Number(time.match(/^(\d+)/)[1]);
		var minutes = Number(time.match(/:(\d+)/)[1]);
		var AMPM = time.match(/\s(.*)$/)[1];
		if(AMPM == "PM" && hours<12) hours = hours+12;
		if(AMPM == "AM" && hours==12) hours = hours-12;
		var sHours = hours.toString();
		var sMinutes = minutes.toString();
		if(hours<10) sHours = "0" + sHours;
		if(minutes<10) sMinutes = "0" + sMinutes;
		effectiveDateTime = myDateFormatter($("#dateEffective").val()) + " " +  sHours + ":" + sMinutes;
	}
	 $("#savePRNButton").button('loading');
	    $.post('<?php echo site_url('homeportal/savePRNMonitor');?>',
	             {
	        eventID: $("#eventID").val(),
	        prn1desc: $("#prn1desc").val(),
	        prn2given: $("#prn2given").val(),
	        prn2desc: $("#prn2desc").val(),
	        protocol: $("#protocol").val(),
	        timeEffective: effectiveDateTime, 
	        staffInitial: $("#staffInitial").val()
	        }, 
	    function (data) {	        
	        $("#closeButtonModal").click();
	        read(data);
	        var args = data.split('|');
	        if (args[0] == 1)
	        {
	            setTimeout(function() {
	                location.reload();
	                }, 1000);	                
	        }
	    });	
}



</script>


<script src="<?php echo base_url();?>js/Chart.min.js"></script>


<script>

jQuery(document).ready(function() {	

	options = {
		    // Boolean - Whether to animate the chart
		    animation: true,

		    // Number - Number of animation steps
		    animationSteps: 60,

		    // String - Animation easing effect
		    animationEasing: "easeOutQuart",

		    // Boolean - If we should show the scale at all
		    showScale: true,

		    // Boolean - If we want to override with a hard coded scale
		    scaleOverride: false,

		    // ** Required if scaleOverride is true **
		    // Number - The number of steps in a hard coded scale
		    scaleSteps: null,
		    // Number - The value jump in the hard coded scale
		    scaleStepWidth: null,
		    // Number - The scale starting value
		    scaleStartValue: null,

		    // String - Colour of the scale line
		    scaleLineColor: "rgba(0,0,0,.1)",

		    // Number - Pixel width of the scale line
		    scaleLineWidth: 1,

		    // Boolean - Whether to show labels on the scale
		    scaleShowLabels: true,

		    // Interpolated JS string - can access value
		    scaleLabel: "<%=value%>",

		    // Boolean - Whether the scale should stick to integers, not floats even if drawing space is there
		    scaleIntegersOnly: true,

		    // Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
		    scaleBeginAtZero: false,

		    // String - Scale label font declaration for the scale label
		    scaleFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",

		    // Number - Scale label font size in pixels
		    scaleFontSize: 12,

		    // String - Scale label font weight style
		    scaleFontStyle: "normal",

		    // String - Scale label font colour
		    scaleFontColor: "#666",

		    // Boolean - whether or not the chart should be responsive and resize when the browser does.
		    responsive: true,

		    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
		    maintainAspectRatio: true,

		    // Boolean - Determines whether to draw tooltips on the canvas or not
		    showTooltips: true,

		    // Array - Array of string names to attach tooltip events
		    tooltipEvents: ["mousemove", "touchstart", "touchmove"],

		    // String - Tooltip background colour
		    tooltipFillColor: "rgba(0,0,0,0.8)",

		    // String - Tooltip label font declaration for the scale label
		    tooltipFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",

		    // Number - Tooltip label font size in pixels
		    tooltipFontSize: 14,

		    // String - Tooltip font weight style
		    tooltipFontStyle: "normal",

		    // String - Tooltip label font colour
		    tooltipFontColor: "#fff",

		    // String - Tooltip title font declaration for the scale label
		    tooltipTitleFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",

		    // Number - Tooltip title font size in pixels
		    tooltipTitleFontSize: 14,

		    // String - Tooltip title font weight style
		    tooltipTitleFontStyle: "bold",

		    // String - Tooltip title font colour
		    tooltipTitleFontColor: "#fff",

		    // Number - pixel width of padding around tooltip text
		    tooltipYPadding: 6,

		    // Number - pixel width of padding around tooltip text
		    tooltipXPadding: 6,

		    // Number - Size of the caret on the tooltip
		    tooltipCaretSize: 8,

		    // Number - Pixel radius of the tooltip border
		    tooltipCornerRadius: 6,

		    // Number - Pixel offset from point x to tooltip edge
		    tooltipXOffset: 10,

		    // String - Template string for single tooltips
		    tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= value %>",

		    // String - Template string for single tooltips
		    multiTooltipTemplate: "<%= datasetLabel %> = <%= value %>",

		    // Function - Will fire on animation progression.
		    onAnimationProgress: function(){},

		    // Function - Will fire on animation completion.
		    onAnimationComplete: function(){}
		};

<?php

$bowel = $this->portal->getPatEventsByType ( 0, 1 );
$bowelStr = "";

$sleep = $this->portal->getPatEventsByType ( 0, 2 );
$sleepStr = "";

$prn = $this->portal->getPatEventsByType ( 0, 3 );
$prnStr = "";

$emerg = $this->portal->getPatEventsByType ( 0, 4 );
$emergStr = "";

$sez = $this->portal->getPatEventsByType ( 0, 5 );
$sezStr = "";

if (is_null ( $bowel->jan )) {
	$bowelStr = "0,0,0,0,0,0,0,0,0,0,0,0";
} else {
	$bowelStr = $bowel->jan . ',';
	$bowelStr .= $bowel->feb . ',';
	$bowelStr .= $bowel->mar . ',';
	$bowelStr .= $bowel->apr . ',';
	$bowelStr .= $bowel->may . ',';
	$bowelStr .= $bowel->jun . ',';
	$bowelStr .= $bowel->jul . ',';
	$bowelStr .= $bowel->aug . ',';
	$bowelStr .= $bowel->sep . ',';
	$bowelStr .= $bowel->oct . ',';
	$bowelStr .= $bowel->nov . ',';
	$bowelStr .= $bowel->dece;
}
if (is_null ( $sleep->jan )) {
	$sleepStr = "0,0,0,0,0,0,0,0,0,0,0,0";
} else {
	$sleepStr = $sleep->jan . ',';
	$sleepStr .= $sleep->feb . ',';
	$sleepStr .= $sleep->mar . ',';
	$sleepStr .= $sleep->apr . ',';
	$sleepStr .= $sleep->may . ',';
	$sleepStr .= $sleep->jun . ',';
	$sleepStr .= $sleep->jul . ',';
	$sleepStr .= $sleep->aug . ',';
	$sleepStr .= $sleep->sep . ',';
	$sleepStr .= $sleep->oct . ',';
	$sleepStr .= $sleep->nov . ',';
	$sleepStr .= $sleep->dece;
}
if (is_null ( $prn->jan )) {
	$prnStr = "0,0,0,0,0,0,0,0,0,0,0,0";
} else {
	$prnStr = $prn->jan . ',';
	$prnStr .= $prn->feb . ',';
	$prnStr .= $prn->mar . ',';
	$prnStr .= $prn->apr . ',';
	$prnStr .= $prn->may . ',';
	$prnStr .= $prn->jun . ',';
	$prnStr .= $prn->jul . ',';
	$prnStr .= $prn->aug . ',';
	$prnStr .= $prn->sep . ',';
	$prnStr .= $prn->oct . ',';
	$prnStr .= $prn->nov . ',';
	$prnStr .= $prn->dece;
}
if (is_null ( $emerg->jan )) {
	$emergStr = "0,0,0,0,0,0,0,0,0,0,0,0";
} else {
	$emergStr = $emerg->jan . ',';
	$emergStr .= $emerg->feb . ',';
	$emergStr .= $emerg->mar . ',';
	$emergStr .= $emerg->apr . ',';
	$emergStr .= $emerg->may . ',';
	$emergStr .= $emerg->jun . ',';
	$emergStr .= $emerg->jul . ',';
	$emergStr .= $emerg->aug . ',';
	$emergStr .= $emerg->sep . ',';
	$emergStr .= $emerg->oct . ',';
	$emergStr .= $emerg->nov . ',';
	$emergStr .= $emerg->dece;
}
if (is_null ( $sez->jan )) {
	$sezStr = "0,0,0,0,0,0,0,0,0,0,0,0";
} else {
	$sezStr = $sez->jan . ',';
	$sezStr .= $sez->feb . ',';
	$sezStr .= $sez->mar . ',';
	$sezStr .= $sez->apr . ',';
	$sezStr .= $sez->may . ',';
	$sezStr .= $sez->jun . ',';
	$sezStr .= $sez->jul . ',';
	$sezStr .= $sez->aug . ',';
	$sezStr .= $sez->sep . ',';
	$sezStr .= $sez->oct . ',';
	$sezStr .= $sez->nov . ',';
	$sezStr .= $sez->dece;
}
?>
	
	var data = {
		    labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
		    datasets: [		   		    
		        {
		            label: "Bowel Movement",
		            fillColor: "rgba(255,255,255,0.2)",
		            strokeColor: "rgba(168,188,123,1)",
		            pointColor: "rgba(168,188,123,1)",
		            pointStrokeColor: "#fff",
		            pointHighlightFill: "#fff",
		            pointHighlightStroke: "rgba(220,220,220,1)",
		            data: [<?php echo $bowelStr;?>]
		        },
		        {
		            label: "Sleep Event",
		            fillColor: "rgba(255,255,255,0.2)",
		            strokeColor: "rgba(112,175,196,1)",
		            pointColor: "rgba(112,175,196,1)",
		            pointStrokeColor: "#fff",
		            pointHighlightFill: "#fff",
		            pointHighlightStroke: "rgba(151,187,205,1)",
		            data: [<?php echo $sleepStr;?>]
		        }
		        ,
		        {
		            label: "PRN Administration",
		            fillColor: "rgba(255,255,255,0.2)",
		            strokeColor: "rgba(94,135,176,1)",
		            pointColor: "rgba(94,135,176,1)",
		            pointStrokeColor: "#fff",
		            pointHighlightFill: "#fff",
		            pointHighlightStroke: "rgba(151,187,205,1)",
		            data: [<?php echo $prnStr;?>]
		        }
		        ,
		        {
		            label: "Emerg Visit",
		            fillColor: "rgba(255,255,255,0.2)",
		            strokeColor: "rgba(217,83,79,1)",
		            pointColor: "rgba(217,83,79,1)",
		            pointStrokeColor: "#fff",
		            pointHighlightFill: "#fff",
		            pointHighlightStroke: "rgba(151,187,205,1)",
		            data: [<?php  echo $emergStr;?>]
		        }
		        ,
		        {
		            label: "Seizure",
		            fillColor: "rgba(255,255,255,0.2)",
		            strokeColor: "rgba(252,215,106,1)",
		            pointColor: "rgba(252,215,106,1)",
		            pointStrokeColor: "#fff",
		            pointHighlightFill: "#fff",
		            pointHighlightStroke: "rgba(151,187,205,1)",
		            data: [<?php echo $sezStr;?>]
		        }
		    ]
		};
	
	// Get the context of the canvas element we want to select
	var ctx = document.getElementById("myChart").getContext("2d");
	ctx.canvas.height = 60;
	var myLineChart = new Chart(ctx).Line(data, options);


	<?php 
	$pieData = $this->portal->getThisMonthEventTypes ( 0 );	
	?>

	var pieData = [
					{
						value: <?php echo $pieData->BM;?>,
						color:"#009900",
						highlight: "#009900",
						label: "Bowel Movement"
					},
					{
						value: <?php echo $pieData->UR;?>,
						color: "#CFCF00",
						highlight: "#CFCF00",
						label: "Urine Discharge"
					},
					{
						value: <?php echo $pieData->SE;?>,
						color: "#66CCFF",
						highlight: "#66CCFF",
						label: "Sleep Event"
					},
					{
						value: <?php echo $pieData->PR;?>,
						color: "#0000CC",
						highlight: "#0000CC",
						label: "PRN Administration"
					},
					{
						value: <?php echo $pieData->ER;?>,
						color: "#FF0000",
						highlight: "#FF0000",
						label: "ER"
					},
					{
						value: <?php echo $pieData->SZ;?>,
						color: "#FF9900",
						highlight: "#FF9900",
						label: "Sezuire"
					}

				];

 
	var ctx4 = document.getElementById("chart-area").getContext("2d");
	window.myPie = new Chart(ctx4).Pie(pieData);
 

});
			
</script>

