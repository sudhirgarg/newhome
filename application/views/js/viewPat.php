<style>
    .modal-dialog {
        width:60%;
    }

</style>

<script>

function savePat()
{
    $("#saveButton").button('loading');
    $.post('<?php echo site_url('compliance/assignPatToHome');?>',
             {
        patID: $("#patID").val(),
        homeID: $("#homeID").val(),
        deliver: $("#deliver").val(),
        controlled: $("#controlled").val(),
        deliveryTimeID: $("#deliveryTimeID").val(),
        note: $("#note").val(),
        deliveryInstructions: $("#deliveryInstructions").val()
        }, 
    function (data) {
        $("#closeButtonModal").click();
        read(data);
        location.reload();
        var args = data.split('|');
        if (args[0] == 1)
        {
            setTimeout(function() {
                location.reload();
                }, 1000);
                
        }
    });

}

function saveMedChange()
{
	 $("#saveButton").button('loading');
	    $.post('<?php echo site_url('compliance/saveMedChange');?>',
	             {
	        medChangeID: $("#medChangeID").val(),
	        patID: $("#patID").val(),
	        dateOfChange: $("#dateOfChange").val(),
	        changeText: $("#changeText").val(),
	        actionText: $("#actionText").val(),
	        technicianUserID: $("#technicianUserID").val(),
	        pharmacistUserID: $("#pharmacistUserID").val(),
	        active: $("#active").val(),
	        startDate: $("#startDate").val(),
	        endDate: $("#endDate").val(),
	        actionComplete: $("#actionComplete").val(),
	        spares : $('input[name=spares]:checked').val(),
	        marval : $('input[name=marval]:checked').val(),
	        eLabels : $('input[name=eLabels]:checked').val(),
	        numOfStrips: $("#numOfStrips").val(),
	        rdd: $("#rdd").val(),
	        batchNum : $('input[name=batchNum]:checked').val()
	        }, 
	    function (data) {
	    	//alert(data);
	        $("#closeButtonModal").click();
	        read(data);
	        var args = data.split('|');
	        if (args[0] == 1)
	        {
	            setTimeout(function() {
	                location.reload();
	                }, 1000);
	                
	        }
	    });	
}



function deleteMedChange()
{
	
	 $("#deleteButton").button('loading');
	 
	 if (window.confirm("Are you sure?")) { 
	 	
	    $.post('<?php echo site_url('compliance/saveMedChange');?>',
	             {
	        medChangeID: $("#medChangeID").val(),
	        patID: $("#patID").val(),
	        dateOfChange: $("#dateOfChange").val(),
	        changeText: $("#changeText").val(),
	        actionText: $("#actionText").val(),
	        technicianUserID: $("#technicianUserID").val(),
	        pharmacistUserID:  $("#pharmacistUserID").val(),
	        active: 0,
	        startDate: $("#startDate").val(),
	        endDate: $("#endDate").val(),
	        actionComplete: $("#actionComplete").val()
	        }, 
	    function (data) {
	        $("#closeButtonModal").click();
	        read(data);
	        var args = data.split('|');
	        if (args[0] == 1)
	        {
	            setTimeout(function() {
	                location.reload();
	                }, 1000);
	                
	        }
	    });	
	 }else{
	 	
	 	 setTimeout(function() {
            location.reload();
         }, 1000);	 	
	 }
}


function pharmacistCheckButtonModal()
{

	
	 $("#pharmacistCheckButton").button('loading');
	    $.post('<?php echo site_url('compliance/pharmacistCheckMedChange');?>',
	             {
            
	        medChangeID: $("#medChangeID").val(),
	        patID: $("#patID").val(),
	        dateOfChange: $("#dateOfChange").val(),
	        changeText: $("#changeText").val(),
	        actionText: $("#actionText").val(),
	        technicianUserID: $("#technicianUserID").val(),
	        active: $("#active").val(),
	        startDate: $("#startDate").val(),
	        endDate: $("#endDate").val(),
	        actionComplete: $("#actionComplete").val()
	        }, 
	    function (data) {
	        $("#closeButtonModal").click();
	        read(data);
	        var args = data.split('|');
	        if (args[0] == 1)
	        {
	            setTimeout(function() {
	                location.reload();
	                }, 1000);
	                
	        }
	    });	
}


function actionCompleteButtonModal(){
	 $("#actionCompleteButton").button('loading');
	    $.post('<?php echo site_url('compliance/actionCompleteMedChange');?>', {
	    	
	        medChangeID: $("#medChangeID").val(),
	        patID: $("#patID").val(),
	        dateOfChange: $("#dateOfChange").val(),
	        changeText: $("#changeText").val(),
	        actionText: $("#actionText").val(),
	        technicianUserID: $("#technicianUserID").val(),
	        pharmacistUserID: $("#pharmacistUserID").val(),
	        active: $("#active").val(),
	        startDate: $("#startDate").val(),
	        endDate: $("#endDate").val(),
	        actionComplete: 1,
	        spares : $('input[name=spares]:checked').val(),
	        marval : $('input[name=marval]:checked').val(),
	        eLabels : $('input[name=eLabels]:checked').val(),
	        numOfStrips: $("#numOfStrips").val(),
	        rdd: $("#rdd").val(),
	        batchNum : $('input[name=batchNum]:checked').val()
	    },function (data) {
	        $("#closeButtonModal").click();
	        read(data);
	        var args = data.split('|');
	        if (args[0] == 1)
	        {
	            setTimeout(function() {
	                location.reload();
	                }, 1000);
	                
	        }
	    });	
}


function saveTicket()
{
	 $("#saveTicketButton").button('loading');
	    $.post('<?php echo site_url('compliance/saveTicketTwo');?>',
	             {
	        ticketID: $("#ticketID").val(),
	        createdBy: $("#createdBy").val(),
	        dateCreated: $("#dateCreated").val(),
	        dateComplete: $("#dateComplete").val(),
	        patID: $("#patID").val(),
	        ticketTypeReferenceID: $("#ticketTypeReferenceID").val(),
	        Description: $("#Description").val(),
	        active: $("#active").val(),
	        deliveryDate :$("#deliveryDate").val(),
	        rxnumList : $("#rxnumList").val(),
	        batchNum   : $('input[name=batchNum]:checked').val(),
	        getDeliveryDate : $("#getDeliveryDate").val()
	   }, 
	    function (data) {
	        $("#closeTicketButtonModal").click();
	        
	        read(data);
	        var args = data.split('|');
	        if (args[0] == 1)
	        {
	            setTimeout(function() {
	                location.reload();
	                }, 1000);
	                
	        }
	    });	
}


function addComment()
{
	 $("#addCommentButton").button('loading');
	    $.post('<?php echo site_url('compliance/addComment');?>',
	             {
	        ticketID: $("#ticketID").val(),
	        message: $("#message").val()
	        }, 
	    function (data) {
	        var uriString = $("#ticketModalURI").val();
            $.get('<?php echo base_url();?>'+uriString, function(data) {
				$("#myModalContent").html(data);
                });
            read(data);
	    });	
}

function closeTicket()
{

	if(confirm("Are you sure you want to close this ticket?")) {
	$("#closeTicketButton").button('loading');
    $.post('<?php echo site_url('compliance/saveTicketTwo');?>',
             {
        ticketID: $("#ticketID").val(),
        createdBy: $("#createdBy").val(),
        dateCreated: $("#dateCreated").val(),
        dateComplete: $("#dateComplete").val(),
        patID: $("#patID").val(),
        ticketTypeReferenceID: $("#ticketTypeReferenceID").val(),
        Description: $("#Description").val(),
        active: 0,
        deliveryDate :$("#deliveryDate").val(),
        rxnumList : $("#rxnumList").val(),
        batchNum   : $('input[name=batchNum]:checked').val(),
        getDeliveryDate : $("#getDeliveryDate").val()
    }, 
    function (data) {
        $("#closeTicketButtonModal").click();
        read(data);
        var args = data.split('|');
        if (args[0] == 1)
        {
            setTimeout(function() {
                location.reload();
                }, 1000);
                
        }
    });	
	} else {
	    // do something
	}
}


function reOrderPRNs()
{
    updateTextArea();
    if(confirm("Please confirm to reorder the following prescription numbers: " + $("#reOrderBox").val())) {
        $.post('<?php echo site_url('homeportal/createReOrder');?>',
            {
                patID: $("#patID").val(),
                rxs: $("#reOrderBox").val()
            },
            function (data) {
                read(data);
               
                var args = data.split('|');
                if (args[0] == 1)
                {
                    setTimeout(function() {
                        location.reload();
                    }, 1000);
                }
            });
    }else{
        //do nothing
    }
}

function updateTextArea() {
    var allVals = [];
    $('#reOrderList :checked').each(function() {
        allVals.push("| Rx# " + $(this).val() + " comment: " + $(this).closest('tr').find('.reOrderCommentClass').val() );

    });
    $('#reOrderBox').val(allVals)
}


$(function () {
    $("#dtorders input[type=checkbox]").each(function (index) {
        $(this).click(function () {
            var $curObjText = $($('#dtorders input[type=text]')[index]);
            if ($(this).is(':checked')) {
                $curObjText.removeAttr("disabled");
            } else {
                $curObjText.attr("disabled", "disabled");
            }
        });
    });
});

function addOnClickEvent() 
{
	$(":checkbox").click(function(){
		 $("input:text").attr("disabled",!this.checked)
	});
}

</script>



<script>
    jQuery(document).ready(function() {
    	
        $(':checkbox').click(function(){
            $('input:text').attr('disabled',!this.checked)
        });
        
        // load compliance pack and prns
        

		$.post('<?php echo site_url('compliance/getPatCompPack'); ?>',
		{
			patID: $("#patID").val()
		},
		function(data, status){
			$("#dtmeds").html(data);
			$("#patCompPackLoading").hide();
		});
		
		
		$.post('<?php echo site_url('compliance/getPatPRN'); ?>',
		{
			patID: $("#patID").val()
		},
		function(data, status){
			$("#dtorders").html(data);
			$("#patPRNLoading").hide();
		});
		
		
		$.post('<?php echo site_url('compliance/getAlergiesTable'); ?>',
		{
			patID: $("#patID").val()
		},
		function(data, status){
			$("#alergiesTable").html(data);
			$("#patAlergyLoading").hide();
		});
		
		
		$.post('<?php echo site_url('compliance/getConditionsTable'); ?>',
		{
			patID: $("#patID").val()
		},
		function(data, status){
			$("#conditionsTable").html(data);
			$("#patConditionsLoading").hide();
		});
                
        
    });

</script>



