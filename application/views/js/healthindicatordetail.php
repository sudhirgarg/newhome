
<script>
<?php
$patID = $this->uri->segment ( 3 );
$Charts = $this->pharm->getPatCharts ( $patID );
?>

		jQuery(document).ready(function() {	
				
			function gd(year, month, day) {
			    //return new Date(year, month - 1, day).getTime();
			   return new Date(year + '-' + month + '-' + day).getTime() / 1000;
			}

			

var d1 = [
<?php
$PointCount = 1;
if ($Charts != FALSE) {
	foreach ( $Charts->result () as $point ) {
		$formatedDate = new DateTime ( $point->Date );
		$year = date_format ( $formatedDate, 'Y' );
		$month = date_format ( $formatedDate, 'm' );
		$day = date_format ( $formatedDate, 'd' );
		if ($PointCount != 1) {
			echo ',';
		}
		echo '[gd(' . $year . ',' . $month . ',' . $day . '),' . $point->Value1 . ']';
		$PointCount ++;
	}
}
?>
];

var redRange = [
<?php
$PointCount = 1;
if ($Charts != FALSE) {
	foreach ( $Charts->result () as $point ) {
		$formatedDate = new DateTime ( $point->Date );
		$year = date_format ( $formatedDate, 'Y' );
		$month = date_format ( $formatedDate, 'm' );
		$day = date_format ( $formatedDate, 'd' );
		if ($PointCount != 1) {
			echo ',';
		}
		echo '[gd(' . $year . ',' . $month . ',' . $day . '),' . 1.5 . ']';
		$PointCount ++;
	}
}
?>
];

var yellowRange = [ 
					<?php
					$PointCount = 1;
					if ($Charts != FALSE) {
						foreach ( $Charts->result () as $point ) {
							$formatedDate = new DateTime ( $point->Date );
							$year = date_format ( $formatedDate, 'Y' );
							$month = date_format ( $formatedDate, 'm' );
							$day = date_format ( $formatedDate, 'd' );
							if ($PointCount != 1) {
								echo ',';
							}
							echo '[gd(' . $year . ',' . $month . ',' . $day . '),1.5]';
							$PointCount ++;
						}
					}
					?>];

var greenRange =[ 
					<?php
					$PointCount = 1;
					if ($Charts != FALSE) {
						foreach ( $Charts->result () as $point ) {
							$formatedDate = new DateTime ( $point->Date );
							$year = date_format ( $formatedDate, 'Y' );
							$month = date_format ( $formatedDate, 'm' );
							$day = date_format ( $formatedDate, 'd' );
							if ($PointCount != 1) {
								echo ',';
							}
							echo '[gd(' . $year . ',' . $month . ',' . $day . '),2]';
							$PointCount ++;
						}
					}
					?>];


			<?php
			if ($Charts != FALSE) {
				?>
						var data = [ 


						
						{
							label: "RED",
							data: redRange,
							color: "red" ,
							lines: {
					            show: true,
					            fill: true,
					            lineWidth: 5
					          },
					          
								series: {
							          stack: true,
							          group: true,
							          groupInterval: 2,
							          shadowSize: 0
							        }
						}
						,
						{
							label: "yellow",
							data: yellowRange,
							color: "yellow" ,
							lines: {
					            show: true,
					            fill: false,
					            lineWidth: 5
					          },
					          
					          series: {
					              stack: true,
					              group: true,
					              groupInterval: 3,
					              lines: {
					                show: true,
					                fill: true,
					                lineWidth: 25
					              },
					              shadowSize: 0
					            }
						}
						,
						{
							label: "green",
							data: greenRange,
							color: "green" ,
							lines: {
					            show: true,
					            fill: false,
					            lineWidth: 5
					          },
					          series: {
					              stack: true,
					              group: true,
					              groupInterval: 2,
					              lines: {
					                show: true,
					                fill: true,
					                lineWidth: 5
					              },
					              shadowSize: 0
					            }
								
						},
						{
							label: "ANC",
							data: d1,
							color: "blue" ,
							series: {
						          stack: false,
						          group: false,
						          groupInterval: 1,
						          shadowSize: 5
						        },
						        lines: {
						            show: true,
						            fill: false,
						            lineWidth: 1
						          },
						points: { fillColor: "#0062E3", show: true } 
						}
						];
						
						 var options = {
								    
								      asLines: {
								    	  xaxis: {
								              
								              axisLabel: "Date"
								          }
								      }
								    };
													
						$.plot("#mychart", data, 
								{ 

							 grid: {
							        borderWidth: 1,
							        minBorderMargin: 20,
							        labelMargin: 10,
							        backgroundColor: {
							            colors: ["#fff", "#e4f4f4"]
							        },
							        margin: {
							            top: 8,
							            bottom: 20,
							            left: 20
							        }
							    },
							
				            xaxis: {
				            	mode: "time",
				            	tickSize: [3, "day"],
				                tickLength: 10,
				                axisLabel: 'Date',
				                axisLabelUseCanvas: true,
				                axisLabelFontSizePixels: 15,
				                axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
				                axisLabelPadding: 5
				            },
				            yaxis: {
				                min: 0,
				                max: 6,
				                position: "right",
				                tickDecimals: 1,
				                axisLabel: 'ANC',
				                axisLabelUseCanvas: true,
				                axisLabelFontSizePixels: 15,
				                axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
				                axisLabelPadding: 10
				            }
				       }
								);
						<?php
			}
			?>
								Charts.initCharts();		
							});
			
	</script>
