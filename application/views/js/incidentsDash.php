<script>

function saveIncident()
{
    $("#saveIncidentButton").button('loading');
    $.post('<?php echo site_url('filingCab/editIncident');?>',
             {
        incidentID: $("#incidentID").val(),
        incidentDatetime: $("#incidentDatetime").val(),
        description: $("#description").val(),
        actionTaken: $("#actionTaken").val(),
        reportedBy: $("#reportedBy").val(),
        reportedTo: $("#reportedTo").val(),
        patientID: $("#patientID").val(),
        incidentType: $("#incidentType").val()         
        }, 
    function (data) {
        $("#closeButtonModal").click();
        read(data);
        var args = data.split('|');
        if (args[0] == 1)
        {
            setTimeout(function() {
                location.reload();
                }, 1000);
                
        }
    });
}

</script>


<script type="text/javascript">
		jQuery(document).ready(function() {	
		<?php
		$incidentTypes = $this->incidents->getIncidentTypes ();
		?>
		var data = [ 
		<?php 
		foreach ( $incidentTypes->result () as $incidentType ) {
			$incidentGraph = $this->incidents->getIncidentsByTypeVsMonth ( '2014', $incidentType->incidentTypeID );
			if ($incidentGraph != false) {
				?>
				{
		label: "<?php echo $incidentType->incidentType;?>",
		data: [ 
		<?php
				$KPIPointCount = 1;
				foreach ( $incidentGraph as $key => $value ) {
					if ($KPIPointCount != 1) {
						echo ',';
					}
					echo '[' . $KPIPointCount . ',' . $value . ']';
					$KPIPointCount ++;
				}
				?>]
		},
	<?php
			}
		}
		?>
		];
		$.plot("#mychart", data);
			Charts.initCharts();		
		});
	</script>