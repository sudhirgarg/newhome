<script>

	function saveKPI()
	{
		$("#saveKPIButton").button('loading');
		$.post('<?php echo site_url('filingCab/updateKPI');?>',
				{
					KPIID: $("#KPIID").val(),
					active: '1',
					indicatorName: $("#indicatorName").val(),
					indicatorDescription: $("#indicatorDescription").val(),
					minValue: $("#minValue").val(),
					maxValue: $("#maxValue").val(),
					targetValue: $("#targetValue").val()	
				}, function(data) {
					read(data);
					$("#saveKPIButton").button('reset');
					$("#closeModalButton").click();
					var args = data.split('|');
			        if (args[0] == 1)
			        {
			            setTimeout(function() {
			                location.reload();
			                }, 1000);
			                
			        }
				});
	}
</script>


<script type="text/javascript">
		jQuery(document).ready(function() {	
<?php
$count2 = 1;
$KPIs = $this->kpi->getKPIs ();
foreach ( $KPIs->result () as $KPI ) {
	$KPIPoints = $this->kpi->getKPIPoints ( $KPI->KPIID );
	if ($KPIPoints) {
		?>
	var data<?php echo $count2;?> = [ {
		label: "<?php echo $KPI->indicatorName;?>",
		data: [ 
		<?php
		$KPIPointCount = 1;
		foreach ( $KPIPoints->result () as $KPIPoint ) {
			if ($KPIPointCount != 1) {
				echo ',';
			}
			echo '[' . $KPIPointCount . ',' . $KPIPoint->pointValue . ']';
			$KPIPointCount ++;
		}
		?>],
		color: "green"
	},{ label: "Target", data: [ [1, <?php echo $KPI->targetValue;?>], [2, <?php echo $KPI->targetValue;?>], [3, <?php echo $KPI->targetValue;?>], [4, <?php echo $KPI->targetValue;?>], [5, <?php echo $KPI->targetValue;?>], [6,<?php echo $KPI->targetValue;?>], [7,<?php echo $KPI->targetValue;?>], [8, <?php echo $KPI->targetValue;?>], [9, <?php echo $KPI->targetValue;?>], [10, <?php echo $KPI->targetValue;?>], [11,<?php echo $KPI->targetValue;?>], [12,<?php echo $KPI->targetValue;?>] ],
		 color: "red" }];
	$.plot("#mychart<?php echo $count2; ?>", data<?php echo $count2++;?>);
	<?php
	}
}
?>
			Charts.initCharts();		
		});
	</script>
	