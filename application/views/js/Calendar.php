
<script>

function saveMedChange()
{
	 $("#saveButton").button('loading');
	    $.post('<?php echo site_url('compliance/saveMedChange');?>',
	             {
	        medChangeID: $("#medChangeID").val(),
	        patID: $("#patID").val(),
	        dateOfChange: $("#dateOfChange").val(),
	        changeText: $("#changeText").val(),
	        actionText: $("#actionText").val(),
	        technicianUserID: $("#technicianUserID").val(),
	        pharmacistUserID: $("#pharmacistUserID").val(),
	        active: $("#active").val(),
	        startDate: $("#startDate").val(),
	        endDate: $("#endDate").val(),
	        actionComplete: $("#actionComplete").val()
	        }, 
	    function (data) {
	        $("#closeButtonModal").click();
	        read(data);
	        var args = data.split('|');
	        if (args[0] == 1)
	        {
	            setTimeout(function() {
	                location.reload();
	                }, 1000);
	                
	        }
	    });	
}


function deleteMedChange()
{
	 $("#deleteButton").button('loading');
	    $.post('<?php echo site_url('compliance/saveMedChange');?>',
	             {
	        medChangeID: $("#medChangeID").val(),
	        patID: $("#patID").val(),
	        dateOfChange: $("#dateOfChange").val(),
	        changeText: $("#changeText").val(),
	        actionText: $("#actionText").val(),
	        technicianUserID: $("#technicianUserID").val(),
	        pharmacistUserID:  $("#pharmacistUserID").val(),
	        active: 0,
	        startDate: $("#startDate").val(),
	        endDate: $("#endDate").val(),
	        actionComplete: $("#actionComplete").val()
	        }, 
	    function (data) {
	        $("#closeButtonModal").click();
	        read(data);
	        var args = data.split('|');
	        if (args[0] == 1)
	        {
	            setTimeout(function() {
	                location.reload();
	                }, 1000);
	                
	        }
	    });	
}

function pharmacistCheckButtonModal()
{

	
	 $("#pharmacistCheckButton").button('loading');
	    $.post('<?php echo site_url('compliance/pharmacistCheckMedChange');?>',
	             {
            
	        medChangeID: $("#medChangeID").val(),
	        patID: $("#patID").val(),
	        dateOfChange: $("#dateOfChange").val(),
	        changeText: $("#changeText").val(),
	        actionText: $("#actionText").val(),
	        technicianUserID: $("#technicianUserID").val(),
	        active: $("#active").val(),
	        startDate: $("#startDate").val(),
	        endDate: $("#endDate").val(),
	        actionComplete: $("#actionComplete").val()
	        }, 
	    function (data) {
	        $("#closeButtonModal").click();
	        read(data);
	        var args = data.split('|');
	        if (args[0] == 1)
	        {
	            setTimeout(function() {
	                location.reload();
	                }, 1000);
	                
	        }
	    });	
}


function actionCompleteButtonModal()
{
	 $("#actionCompleteButton").button('loading');
	    $.post('<?php echo site_url('compliance/actionCompleteMedChange');?>',
	             {
	        medChangeID: $("#medChangeID").val(),
	        patID: $("#patID").val(),
	        dateOfChange: $("#dateOfChange").val(),
	        changeText: $("#changeText").val(),
	        actionText: $("#actionText").val(),
	        technicianUserID: $("#technicianUserID").val(),
	        pharmacistUserID: $("#pharmacistUserID").val(),
	        active: $("#active").val(),
	        startDate: $("#startDate").val(),
	        endDate: $("#endDate").val(),
	        actionComplete: 1
	        }, 
	    function (data) {
	        $("#closeButtonModal").click();
	        read(data);
	        var args = data.split('|');
	        if (args[0] == 1)
	        {
	            setTimeout(function() {
	                location.reload();
	                }, 1000);
	                
	        }
	    });	
}

    </script>