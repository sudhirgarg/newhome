<script>
    function saveUser()
    {
        $("#saveButton").button('loading');
        $.post('<?php echo site_url('compliance/editPortalUser');?>', 
                {
            	username: $("#username").val(), 
            	password: $("#password").val(),  
            	email: $("#email").val(),  
            	groups: $("#groups").val(),
            	company: $("#company").val(),
            	phone: $("#phone").val(),
            	first_name: $("#first_name").val(),
            	last_name: $("#last_name").val()
                }, 
        function (data) {
            $("#closeButtonModal").click();
            read(data);
            var args = data.split('|');
            if (args[0] == 1)
            {
                setTimeout(function() {
                    location.reload();
                    }, 1000);
            }
            
        });
    }
    
    function deleteUser()
    {
        $("#deleteButton").button('loading');
        $.post('<?php echo site_url('compliance/deletePortalUserConfirm');?>', 
                {
            	id: $("#id").val()
                }, 
        function (data) {
            $("#closeButtonModal").click();
            read(data);
            var args = data.split('|');
            if (args[0] == 1)
            {
                setTimeout(function() {
                    location.reload();
                    }, 1000);
            }
            
        });
    }
  
</script>
