
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>Cloud Admin | Login</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">
<!-- STYLESHEETS -->
<!--[if lt IE 9]><script src="<?php echo base_url(); ?>js/flot/excanvas.min.js"></script><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url(); ?>css/cloud-admin.css">

<link
	href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css"
	rel="stylesheet">
<!-- DATE RANGE PICKER -->
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url(); ?>js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
<!-- UNIFORM -->
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url(); ?>js/uniform/css/uniform.default.min.css" />
<!-- ANIMATE -->
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>css/animatecss/animate.min.css" />
<!-- FONTS -->
<link
	href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'
	rel='stylesheet' type='text/css'>
</head>
<body class="login">
	<!-- PAGE -->
	<section id="page">
		<!-- HEADER -->
		<header>
			<!-- NAV-BAR -->
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-md-offset-4">
						<div id="logo">
							<a href="index.html"><img
								src="<?php echo base_url();?>img/logo/logo.png" height="40"
								alt="logo name" /></a>
						</div>
					</div>
				</div>
			</div>
			<!--/NAV-BAR -->
		</header>
		<!--/HEADER -->
		<!-- LOGIN -->
		<section id="login_bg" class="visible">
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-md-offset-4">
						<div class="login-box">
							<h2 class="bigintro">Sign In</h2>
							<div class="divide-40"></div>


							<div id="infoMessage"><?php echo $message;?></div>

<?php echo form_open("auth/login");?>

  <p>
    <?php echo lang('login_identity_label', 'identity');?>
    <?php echo form_input($identity);?>
  </p>

							<p>
    <?php echo lang('login_password_label', 'password');?>
    <?php echo form_input($password);?>
  </p>

							<p>
    <?php echo lang('login_remember_label', 'remember');?>
    <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
  </p>

							<p><?php echo form_submit('submit', lang('login_submit_btn'));?></p>
<?php echo form_close();?>
							<!-- /CONTENT -->



							<div class="login-helpers">
								<a href="#" onclick="swapScreen('forgot_bg');return false;">Forgot
									Password?</a> <br> Don't have an account with us? <a href="#"
									onclick="swapScreen('register_bg');return false;">Register now!</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--/LOGIN -->
		<!-- REGISTER -->
		<section id="register_bg" class="font-400">
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-md-offset-4">
						<div class="login-box">
							<h2 class="bigintro">Register</h2>
							<div class="divide-40"></div>

							<h1>Please speak to your manager/system admin to create your user
								account.</h1>


							<div class="login-helpers">
								<a href="#" onclick="swapScreen('login_bg');return false;"> Back
									to Login</a> <br>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--/REGISTER -->
		<!-- FORGOT PASSWORD -->
		<section id="forgot_bg">
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-md-offset-4">
						<div class="login-box">
							<h2 class="bigintro">Reset Password</h2>
							<div class="divide-40"></div>

							<h1>Please speak to your manager/system admin to reset your password.</h1>

							<div class="login-helpers">
								<a href="#" onclick="swapScreen('login_bg');return false;">Back
									to Login</a> <br>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- FORGOT PASSWORD -->
	</section>
	<!--/PAGE -->
	<!-- JAVASCRIPTS -->
	<!-- Placed at the end of the document so the pages load faster -->
	<!-- JQUERY -->
	<script src="<?php echo base_url("js/jquery/jquery-2.0.3.min.js");?>"></script>

	<script
		src="<?php echo base_url(); ?>js/jquery/jquery-migrate-1.2.1.min.js"></script>
	<!-- JQUERY UI-->
	<script
		src="<?php echo base_url(); ?>js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
	<!-- BOOTSTRAP -->
	<script
		src=<?php echo base_url("js/bootstrap.min.js");?>></script>


	<!-- UNIFORM -->
	<script type="text/javascript"
		src="<?php echo base_url(); ?>js/uniform/jquery.uniform.min.js"></script>
	<!-- BACKSTRETCH -->
	<script type="text/javascript"
		src="<?php echo base_url(); ?>js/backstretch/jquery.backstretch.min.js"></script>
	<!-- CUSTOM SCRIPT -->
	<script src="<?php echo base_url(); ?>js/script.js"></script>
	<script>
		jQuery(document).ready(function() {		
			App.setPage("login_bg");  //Set current page
			App.init(); //Initialise plugins and elements
		});
	</script>
	<script type="text/javascript">
		function swapScreen(id) {
			jQuery('.visible').removeClass('visible animated fadeInUp');
			jQuery('#'+id).addClass('visible animated fadeInUp');
		}
	</script>
	<!-- /JAVASCRIPTS -->
</body>
</html>


