<script>

function savePat()
{
    $("#saveButton").button('loading');
    $.post('<?php echo site_url('compliance/assignPatToHome');?>',
             {
        patID: $("#patID").val(),
        homeID: $("#homeID").val(),
        deliver: $("#deliver").val(),
        controlled: $("#controlled").val(),
        deliveryTimeID: $("#deliveryTimeID").val(),
        note: $("#note").val(),
        deliveryInstructions: $("#deliveryInstructions").val()
        }, 
    function (data) {
        $("#closeButtonModal").click();
        read(data);
        var args = data.split('|');
        if (args[0] == 1)
        {
            setTimeout(function() {
                location.reload();
                }, 1000);
                
        }
    });

}

function saveMedChange()
{
	 $("#saveButton").button('loading');
	    $.post('<?php echo site_url('compliance/saveMedChange');?>',
	             {
	        medChangeID: $("#medChangeID").val(),
	        patID: $("#patID").val(),
	        dateOfChange: $("#dateOfChange").val(),
	        changeText: $("#changeText").val(),
	        actionText: $("#actionText").val(),
	        technicianUserID: $("#technicianUserID").val(),
	        pharmacistUserID: $("#pharmacistUserID").val(),
	        active: $("#active").val(),
	        startDate: $("#startDate").val(),
	        endDate: $("#endDate").val(),
	        actionComplete: $("#actionComplete").val()
	        }, 
	    function (data) {
	        $("#closeButtonModal").click();
	        read(data);
	        var args = data.split('|');
	        if (args[0] == 1)
	        {
	            setTimeout(function() {
	                location.reload();
	                }, 1000);
	                
	        }
	    });	
}


function deleteMedChange()
{
	 $("#deleteButton").button('loading');
	    $.post('<?php echo site_url('compliance/saveMedChange');?>',
	             {
	        medChangeID: $("#medChangeID").val(),
	        patID: $("#patID").val(),
	        dateOfChange: $("#dateOfChange").val(),
	        changeText: $("#changeText").val(),
	        actionText: $("#actionText").val(),
	        technicianUserID: $("#technicianUserID").val(),
	        pharmacistUserID:  $("#pharmacistUserID").val(),
	        active: 0,
	        startDate: $("#startDate").val(),
	        endDate: $("#endDate").val(),
	        actionComplete: $("#actionComplete").val()
	        }, 
	    function (data) {
	        $("#closeButtonModal").click();
	        read(data);
	        var args = data.split('|');
	        if (args[0] == 1)
	        {
	            setTimeout(function() {
	                location.reload();
	                }, 1000);
	                
	        }
	    });	
}

function pharmacistCheckButtonModal()
{

	
	 $("#pharmacistCheckButton").button('loading');
	    $.post('<?php echo site_url('compliance/pharmacistCheckMedChange');?>',
	             {
            
	        medChangeID: $("#medChangeID").val(),
	        patID: $("#patID").val(),
	        dateOfChange: $("#dateOfChange").val(),
	        changeText: $("#changeText").val(),
	        actionText: $("#actionText").val(),
	        technicianUserID: $("#technicianUserID").val(),
	        active: $("#active").val(),
	        startDate: $("#startDate").val(),
	        endDate: $("#endDate").val(),
	        actionComplete: $("#actionComplete").val()
	        }, 
	    function (data) {
	        $("#closeButtonModal").click();
	        read(data);
	        var args = data.split('|');
	        if (args[0] == 1)
	        {
	            setTimeout(function() {
	                location.reload();
	                }, 1000);
	                
	        }
	    });	
}


function actionCompleteButtonModal()
{
	 $("#actionCompleteButton").button('loading');
	    $.post('<?php echo site_url('compliance/actionCompleteMedChange');?>',
	             {
	        medChangeID: $("#medChangeID").val(),
	        patID: $("#patID").val(),
	        dateOfChange: $("#dateOfChange").val(),
	        changeText: $("#changeText").val(),
	        actionText: $("#actionText").val(),
	        technicianUserID: $("#technicianUserID").val(),
	        pharmacistUserID: $("#pharmacistUserID").val(),
	        active: $("#active").val(),
	        startDate: $("#startDate").val(),
	        endDate: $("#endDate").val(),
	        actionComplete: 1
	        }, 
	    function (data) {
	        $("#closeButtonModal").click();
	        read(data);
	        var args = data.split('|');
	        if (args[0] == 1)
	        {
	            setTimeout(function() {
	                location.reload();
	                }, 1000);
	                
	        }
	    });	
}


function saveTicket()
{
	 $("#saveTicketButton").button('loading');
	    $.post('<?php echo site_url('compliance/saveTicket');?>',
	             {
	        ticketID: $("#ticketID").val(),
	        createdBy: $("#createdBy").val(),
	        dateCreated: $("#dateCreated").val(),
	        dateComplete: $("#dateComplete").val(),
	        patID: $("#patID").val(),
	        ticketTypeReferenceID: $("#ticketTypeReferenceID").val(),
	        Description: $("#Description").val(),
	        active: $("#active").val()
	        }, 
	    function (data) {
	        $("#closeTicketButtonModal").click();
	        read(data);
	        var args = data.split('|');
	        if (args[0] == 1)
	        {
	            setTimeout(function() {
	                location.reload();
	                }, 1000);
	                
	        }
	    });	
}


function addComment()
{
	 $("#addCommentButton").button('loading');
	    $.post('<?php echo site_url('compliance/addComment');?>',
	             {
	        ticketID: $("#ticketID").val(),
	        message: $("#message").val()
	        }, 
	    function (data) {
	        $("#addCommentButton").button('Yays');
	        read(data);
	    });	
}


function closeTicket()
{
	$("#closeTicketButton").button('loading');
    $.post('<?php echo site_url('compliance/saveTicket');?>',
             {
        ticketID: $("#ticketID").val(),
        createdBy: $("#createdBy").val(),
        dateCreated: $("#dateCreated").val(),
        dateComplete: $("#dateComplete").val(),
        patID: $("#patID").val(),
        ticketTypeReferenceID: $("#ticketTypeReferenceID").val(),
        Description: $("#Description").val(),
        active: 0
        }, 
    function (data) {
        $("#closeTicketButtonModal").click();
        read(data);
        var args = data.split('|');
        if (args[0] == 1)
        {
            setTimeout(function() {
                location.reload();
                }, 1000);
                
        }
    });	
}

</script>





<script>


function gd(year, month, day) {
    return new Date(year, month - 1, day).getTime();
}


		jQuery(document).ready(function() {		

			<?php
			$patID = $this->uri->segment ( 3 );
			$Charts = $this->pharm->getPatCharts ( $patID );
			if ($Charts != FALSE) {
				?>
						var data = [ {
							label: "ANC",
							data: [ 
							<?php
				$PointCount = 1;
				foreach ( $Charts->result () as $point ) {
					$formatedDate = new DateTime($point->Date);
					$year= date_format($formatedDate, 'Y');
					$month= date_format($formatedDate, 'm');
					$day= date_format($formatedDate, 'd');
					if ($PointCount != 1) {
						echo ',';
					}
					echo '[gd(' . $year . ',' . $month . ',' . $day . '),' . $point->Value1 . ']';
					$PointCount ++;
				}
				?>],
							color: "blue" 
						},
						{
							label: "RED",
							data: [ 
							<?php
				$PointCount = 1;
				foreach ( $Charts->result () as $point ) {
					$formatedDate = new DateTime($point->Date);
					$year= date_format($formatedDate, 'Y');
					$month= date_format($formatedDate, 'm');
					$day= date_format($formatedDate, 'd');
					if ($PointCount != 1) {
						echo ',';
					}
					echo '[gd(' . $year . ',' . $month . ',' . $day . '),1.5]';
					$PointCount ++;
				}
				?>],
							color: "red" 
						}
						,
						{
							label: "yellow",
							data: [ 
							<?php
				$PointCount = 1;
				foreach ( $Charts->result () as $point ) {
					$formatedDate = new DateTime($point->Date);
					$year= date_format($formatedDate, 'Y');
					$month= date_format($formatedDate, 'm');
					$day= date_format($formatedDate, 'd');
					if ($PointCount != 1) {
						echo ',';
					}
					echo '[gd(' . $year . ',' . $month . ',' . $day . '),2]';
					$PointCount ++;
				}
				?>],
							color: "yellow" 
						}
						,
						{
							label: "green",
							data: [ 
							<?php
				$PointCount = 1;
				foreach ( $Charts->result () as $point ) {
					$formatedDate = new DateTime($point->Date);
					$year= date_format($formatedDate, 'Y');
					$month= date_format($formatedDate, 'm');
					$day= date_format($formatedDate, 'd');
					if ($PointCount != 1) {
						echo ',';
					}
					echo '[gd(' . $year . ',' . $month . ',' . $day . '),10]';
					$PointCount ++;
				}
				?>],
							color: "green" 
						}
						];
						
						 var options = {
								    
								      asLines: {
								        series: {
								          stack: true,
								          group: true,
								          groupInterval: 5,
								          lines: {
								            show: true,
								            fill: true,
								            lineWidth: 1
								          },
								          shadowSize: 0
								        }
								      }
								    };
													
						$.plot("#mychart", data, options.asLines);
						$("#mychart").UseTooltip();
						<?php
			}
			?>

								Charts.initCharts();		
							});
			
	</script>

