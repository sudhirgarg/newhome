<script>

function saveSettings()
{
	$("#saveSettingsButton").button('loading');
	$.post('<?php echo site_url('setting/saveSettings');?>', 
			{
		storename: $("#storename").val(), 
		phone: $("#phone").val(), 
		address: $("#address").val(), 
		city: $("#city").val(), 
		province: $("#province").val() 
		},
		function(data){
			read(data);
            window.location='<?php echo site_url('setting/settings');?>';
		});
	}



function saveUser()
{
    $("#saveButton").button('loading');
    $.post('<?php echo site_url('setting/createPharmacyUser');?>', 
            {
        	username: $("#username").val(), 
        	password: $("#password").val(),  
        	email: $("#email").val(), 
        	company: $("#company").val(),
        	phone: $("#phone").val(),
        	first_name: $("#first_name").val(),
        	last_name: $("#last_name").val()
            }, 
    function (data) {
        $("#closeButtonModal").click();
        read(data);
        var args = data.split('|');
        if (args[0] == 1)
        {
            setTimeout(function() {
                location.reload();
                }, 1000);
        }
        
    });
    location.reload();
}



function createNewPatient()
{
    $("#createNewPatientButton").button('loading');
    $.post('<?php echo site_url('setting/createNewPatient');?>', 
            {
        	patID: $("#patID").val(), 
        	patInitial: $("#patInitial").val()
            }, 
    function (data) {
        read(data);
        var args = data.split('|');
        if (args[0] == 1)
        {
        	$("#createNewPatientButton").button('Created.');
            setTimeout(function() {
                location.reload();
                }, 1000);
        }
        
    });
}


</script>