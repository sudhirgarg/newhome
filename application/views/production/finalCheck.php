<?php $ci = get_instance(); ?>
<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left"> Pac med Details </h3>
</div>
<div class="description">
	
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DELIVERY RUNS CONTENT -->
<!-- DATA TABLES -->

<!--***************************************** PAGE BODY STARTS ***************************************************************-->

<div class="row" ng-app="myApp" ng-controller="proController">
    <div class="col-sm-6 col-md-8">
    <div class="panel panel-info">
        <div class="panel-heading">

         <?php  include_once('subMenu.php'); ?>
         
         		
        </div>
        
        <div class="panel-body">
        	
        	<div class="table-responsive">
								  
				   <table id="example" class="table table-striped table-bordered table-hover" >
                <form class="form-inline">
                    <div class ="row">

                        <div class="col-md-3">
                            <div class="form-group" style="width:250px;">
                                <input type="text" ng-model="search" class="form-control" placeholder="Search">
                            </div>
                        </div>

                        <div class="col-md-1">				
                            <div class="form-group" style="width:65px;"> 				
                                <input type="number" min="1" max="100" class="form-control" ng-model="pageSize"> 
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group" style="width:75px;"> 
                                <button class="form-control btn btn-info" >
                                #R's:{{totalFinalDelivery}}
                                </button>
                            </div>			
                        </div>
                        
                        <div class="col-md-3">
                            <div class="form-group" style="width:250px;">
                                <div class="alert alert-warning" role="alert"><span id="returnInfo"> {{confirmPacMed}} </span></div>                                
                            </div>
                        </div>
                        
                    </div>
                </form>


                    <thead>
                        <tr>
                            <th style="width:50px;">Post</th>			
                            <th style="width:50px;" align="center">#</th>
                             <th style="width:135px;" ng-click="sort('home')">Home
                                    <span class="glyphicon sort-icon" ng-show="sortKey=='home'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                            </th>
                            
                             <th style="width:135px;" ng-click="sort('event')">Event
                                    <span class="glyphicon sort-icon" ng-show="sortKey=='event'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                            </th>
                                                                                    
                            <th style="width:100px;" ng-click="sort('batchNum')">Batch #
                                    <span class="glyphicon sort-icon" ng-show="sortKey=='batchNum'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                            </th>
                            <th style="width:133px;" ng-click="sort('orderNum')">Order Number
                                    <span class="glyphicon sort-icon" ng-show="sortKey=='orderNum'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                            </th>
                            
                             <th style="width:133px;" ng-click="sort('delivery')">Delivery Date
                                    <span class="glyphicon sort-icon" ng-show="sortKey=='delivery'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                            </th>
                           
                        </tr>
                    </thead>
                    <tbody>
                        <tr dir-paginate="deliverysFinal in deliverysFinals | filter:search | orderBy:sortKey:reverse | itemsPerPage:pageSize"  >
                            <td> 
                                <button class="btn" ng-click="confirmPacMedOneTouch(pacMed.orderNum)" name="" type="button">
                                    <span class="glyphicon glyphicon-send"></span> 
                                </button>                                   
                            </td>
                            <td align="center">{{ $index + 1 }}   </td>
                            <td>{{ deliverysFinal.home }}         </td>
                            <td>{{ deliverysFinal.event }}         </td>
                            <td>{{ deliverysFinal.batchNum }}     </td>
                            <td>{{ deliverysFinal.orderNum }}  </td>
                            <td>{{ deliverysFinal.delivery }} </td>
                          
                        </tr>
                    </tbody>
                </table>
                        
                <dir-pagination-controls
                    max-size="10"
                    direction-links="true"
                    boundary-links="true" >
                </dir-pagination-controls>
                
			</div>
			

        </div>
    </div>
    </div>
</div>



<!--***************************************** PAGE BODY ENDS ***************************************************************-->


<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>

