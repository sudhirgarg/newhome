
<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left"> Current Batch Details </h3>
</div>
<div class="description">
	
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DELIVERY RUNS CONTENT -->
<!-- DATA TABLES -->

<!--***************************************** PAGE BODY STARTS ***************************************************************-->

<div class="row">
    <div class="col-sm-6 col-md-8">
    <div class="panel panel-info">
        <div class="panel-heading">

         <?php  include_once('subMenu.php'); ?>
         
         		
        </div>
        
        <div class="panel-body" ng-app="myApp" ng-controller="DoubleController">
            
			<h3>Delivery Details</h3>

                <table id="example" class="table table-striped table-bordered table-hover" >
                <form class="form-inline">
                    <div class ="row">

                        <div class="col-md-3">
                            <div class="form-group" style="width:250px;">
                                <input type="text" ng-model="search" class="form-control" placeholder="Search">
                            </div>
                        </div>

                        <div class="col-md-1">				
                            <div class="form-group" style="width:65px;"> 				
                                <input type="number" min="1" max="100" class="form-control" ng-model="pageSize"> 
                            </div>
                        </div>
                        
                        <div class="col-md-2">
                            <div class="form-group" style="width:75px;"> 
                                <button class="form-control btn btn-info" >
                                #R's:{{totalNumberOfDeliveryRecord}}
                                </button>
                            </div>			
                        </div>

                    </div>
                </form>


                    <thead>
                        <tr>
                            <!--th style="width:50px;">Post</th-->			
                            <th style="width:50px;" align="center">#</th>
                             <th style="width:100px;" ng-click="sort('patId')">PatID
                                    <span class="glyphicon sort-icon" ng-show="sortKey=='patId'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                            </th>
                            <th style="width:300px;" ng-click="sort('patName')">Patient
                                    <span class="glyphicon sort-icon" ng-show="sortKey=='patName'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                            </th>
                            <th ng-click="sort('noteInternal')">Delivery Type
                                    <span class="glyphicon sort-icon" ng-show="sortKey=='noteInternal'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                            </th>
                             <th ng-click="sort('deliveryNote')">Delivery Note
                                    <span class="glyphicon sort-icon" ng-show="sortKey=='deliveryNote'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                            </th>
                            <th ng-click="sort('DayInfo')">Day
                                    <span class="glyphicon sort-icon" ng-show="sortKey=='DayInfo'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                            </th>
                            
                            <th ng-click="sort('TimeInfo')">Time
                                    <span class="glyphicon sort-icon" ng-show="sortKey=='TimeInfo'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                            </th>
                            
                            
                            
                        </tr>
                    </thead>
                    <tbody>
                        <tr dir-paginate="deliveryInfo in deliveryInfos | filter:search | orderBy:sortKey:reverse | itemsPerPage:pageSize"  >
                            <!--td> 
                                <button class="btn" ng-click="updateDeliveryShedule(deliveryInfo.id)" name="edit" type="button">
                                    <span class="glyphicon glyphicon-pencil"></span> 
                                </button>                                   
                            </td-->
                            <td align="center">{{ $index + 1 }}   </td>
                            <td>{{ deliveryInfo.patId }}          </td>
                            <td>{{ deliveryInfo.patName }}        </td>
                            <td>{{ deliveryInfo.noteInternal }}   </td>
                            <td>{{ deliveryInfo.deliveryNote }}   </td>
                            <td>{{ deliveryInfo.DayInfo}}         </td>
                            <td>{{ deliveryInfo.TimeInfo}}        </td>
                            
                            
                        </tr>
                    </tbody>
                </table>
                        
                <dir-pagination-controls
                    max-size="10"
                    direction-links="true"
                    boundary-links="true" >
                </dir-pagination-controls>
                
                <div class="modal fade" id="myModal" role="dialog">
					<div class="modal-dialog">  
					<div class="modal-content">
			                   			
			                    <div class="modal-body" >
			                        
			                        <h3 ng-hide="edit">Update Notes and Instruction</h3>
			
			                        <form class="form-horizontal" method="POST">
			                            <ul >
			                                <li class="err" ng-repeat="error in errors"> {{error}} </li>
			                            </ul>
			                            <ul>
			                                <li ng-repeat="msg in msgs">							 
			                                    <div ng-if="display(msg)" >
			                                        <div class="alert alert-success">
			                                          <strong>{{msg}} </strong> 
			                                        </div>															   
			                                    </div>
			                                </li>
			                            </ul>
			
			                            <div class="form-group">    
			                            	                       
			                                <div class="col-sm-10"><label class="control-label">Patient ID:</label> {{ patId }} </div> 
			                            </div>     
			                            
			                            <div class="form-group">
			                                <div class="col-sm-10"><label class="control-label">Patient Name:</label> {{ patName }} </div> 
			                            </div>
			                            <div class="form-group">
			                                <div class="col-sm-10"><label class="control-label">Note for internal:</label>
			                                	<textarea cols="77" rows="6" ng-model="noteInternal">{{ noteInternal }} </textarea>			                                	 
			                                </div> 
			                            </div>
			                            
			                            <div class="form-group">
			                                <div class="col-sm-10"><label class="control-label">Delivery Note:</label>  
			                                    <textarea cols="77" rows="6" ng-model="deliveryNote">{{ deliveryNote }} </textarea>
			                                </div> 
			                            </div>
			                                                   
			                        </form>	
			                    </div>
			
			                    <div class="modal-footer">
			                        <input type="hidden" ng-model="id" value="">	                       
			                        <button class="btn btn-success" ng-disabled="error || incomplete" ng-click='updateDeliveryNotes();'><span class="glyphicon glyphicon-save"></span>Update</button>
			                        <button type="button" class="btn btn-default" data-dismiss="modal" ng-click='LoadPage();'>Close</button>
			                    </div>
						
					</div>		
					</div>	
				</div>  

        </div>
    </div>
    </div>
</div>



<!--***************************************** PAGE BODY ENDS ***************************************************************-->


<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>

