<?php       
    $returnUrl = "http://miiza.com/";
    //$returnUrl = "https://www.seamlessportal.ca/";
     
?>
<!DOCTYPE html>
<html lang="en">
<head>
	
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title> <?php if($this->uri->segment(2) == "NULL") { echo "Home"; } else { echo $this->uri->segment(2); } ?> </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/cloud-admin.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/themes/default.css" id="skin-switcher">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">	
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/chosen/prism.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/chosen/chosen.css">
    
    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/anysearch.css">

    <link rel="stylesheet" href = "http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">	
    
    <style>
        .border-bottom {
            border-bottom: 1px solid black;
        }
        
        .horizontral {
            width:auto;
            z-index: 100;
            -webkit-backface-visibility: hidden;
            -webkit-transform: translateX(-100%) rotate(-90deg);
            -webkit-transform-origin: right top;
            -moz-transform: translateX(-100%) rotate(-90deg);
            -moz-transform-origin: right top;
            -o-transform: translateX(-100%) rotate(-90deg);
            -o-transform-origin: right top;
            transform: translateX(-100%) rotate(-90deg);
            transform-origin: right top;
            color:red;
        }


    </style>
</head>
<body>
	<!-- HEADER -->
	<header class="navbar clearfix navbar-fixed-top" id="header">
            <div class="container">
                <div class="navbar-brand">
                    <!-- COMPANY LOGO -->
                    <a href="<?php echo base_url(); ?>compliance/ticketsdash"> 
                            <img src="<?php echo base_url(); ?>img/logo/logo.png" alt="Cloud Admin Logo" class="img-responsive" height="30" width="200">
                    </a>
                    <!-- /COMPANY LOGO -->
                    <!-- TEAM STATUS FOR MOBILE -->
                    <div class="visible-xs">
                            <a href="#" class="team-status-toggle switcher btn dropdown-toggle"> <i class="fa fa-users"></i></a>
                    </div>
                    <!-- /TEAM STATUS FOR MOBILE -->

                    <!-- SIDEBAR COLLAPSE -->
                    <div id="sidebar-collapse" class="sidebar-collapse btn">
                             <i class="fa fa-bars" data-icon1="fa fa-bars" data-icon="fa fa-bars"></i>
                    </div>
                    <!-- /SIDEBAR COLLAPSE -->
                </div>
                
                <!-- NAVBAR LEFT -->
                <ul class="nav navbar-nav pull-left hidden-xs" id="navbar-left">
                    <li class="dropdown">
                        <a href="<?php echo $returnUrl;?>" class="dropdown-toggle tip-bottom" data-toggle="tooltip" title="Home Portal"> 
                            <i class="fa fa-users"></i> <span class="name">Home Portal</span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                    </li>
                </ul>

                <ul class="nav navbar-nav pull-left hidden-xs" id="navbar-left">
                    <li class="dropdown">
                        <a href="<?php echo $returnUrl.'homePortalEdu/addSubject' ;?>" class="dropdown-toggle tip-bottom" data-toggle="tooltip" title="Education Portal"> 
                            <i class="fa fa-users"></i> <span class="name">Education Portal</span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                    </li>
                </ul>

                    <!-- /NAVBAR LEFT -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                <ul class="nav navbar-nav pull-right">			

                    <!-- BEGIN INBOX DROPDOWN -->
                    <li class="dropdown" id="header-message">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
                            <i class="fa fa-envelope"></i> <span class="badge"></span>
                        </a>

                        <ul class="dropdown-menu inbox">
                            <li class="dropdown-title">
                                    <span><i class="fa fa-envelope-o"></i>Messages</span> 
                                    <span class="compose pull-right tip-right" title="Compose message"><i class="fa fa-pencil-square-o"></i></span>
                            </li>
                              						
                            <li >
                                <a> 
                                    <span class="body"> 
                                        <span class="from"> </span>
                                        <span class="message">  </span> 
                                        <span class="time"> <i class="fa fa-clock-o"></i> <span></span></span>
                                    </span>
                               </a>
                            </li>

                            					

                            <li class="footer"><a href="#">See all messages <i class="fa fa-arrow-circle-right"></i></a></li>
                        </ul>
                    </li>

                        <!-- END INBOX DROPDOWN -->

                        <!-- BEGIN USER LOGIN DROPDOWN -->
                        <li class="dropdown user" id="header-user">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
                                        <img alt="" src="<?php echo base_url(); ?>img/avatars/avatar3.jpg" /> 
                                        <span class="username">
                                              <?php echo $authendication = $this->input->cookie('localAdminName');   ?>
                                        </span> <i class="fa fa-angle-down"></i>
                                </a>

                                <ul class="dropdown-menu">
                                        <li><a href="<?php echo $returnUrl."homePortalEdu"; ?>"><i class="fa fa-user"></i> My Profile</a></li>
                                        <li><a href="#"><i class="fa fa-cog"></i> Account Settings</a></li>
                                        <li><a href="http://www.seamlesscare.ca/privacy-policy/"><i class="fa fa-eye"></i> Privacy Settings</a></li>
                                        <li>
                                                <a>
                                                        <?php
                                                                //if ($this -> quick -> is_internal()) {
                                                                        //echo '<i class="fa fa-building-o"></i> Internal User';
                                                                //} else {
                                                                        //echo '<i class="fa fa-cloud"></i> External User';
                                                                //}
                                                        ?>
                                                </a>
                                        </li>
                                        <li><a href="<?php echo base_url(); ?>production/logout"><i class="fa fa-power-off"></i> Log Out</a></li>
                                </ul>
                        </li>
                        <!-- /END USER LOGIN DROPDOWN -->
                </ul>
                    <!-- /END TOP NAVIGATION MENU -->
            </div>
		<!-- TEAM STATUS -->
		
		<div class="container team-status" id="team-status">
                    <div id="scrollbar">
                        <div class="handle"></div>
                    </div>
                    <div id="teamslider">
                        <ul class="team-list"></ul>
                    </div>
		</div>
		<!-- /TEAM STATUS -->
	</header>
	<!--/HEADER -->
	

	<!-- PAGE -->
	<section id="page">
		<!-- SIDEBAR -->
		<div id="sidebar" class="sidebar sidebar-fixed">
			<div class="sidebar-menu nav-collapse">
				<div class="divide-20"></div>
				<!-- SEARCH BAR -->
				<div id="search-bar">
					<input id="patFuzySearch" type="text" size="30" onkeyup="showResult()">
					<div id="patientResults"></div>
				</div>
				

				<!-- SIDEBAR MENU -->
				<ul>
                                    <!--li>
                                        <a href="<?php echo $returnUrl; ?>"> <span class="sub-menu-text">Homes</span> </a>
                                    </li-->

                                    <li>
                                        <a href="<?php echo $returnUrl."compliance/portalUsers"; ?>">
                                            <span class="sub-menu-text">Home Portal Users</span>
                                        </a>
                                    </li>

                                    <li >
                                        <a href="<?php echo $returnUrl."compliance/ticketsDash"; ?>">
                                           <span class="sub-menu-text">Tickets</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="<?php echo $returnUrl."compliance/medchanges"; ?>">
                                            <span class="sub-menu-text">Med Changes</span>
                                        </a>
                                    </li>

                                    <li <?php 
                                            if ($this -> uri -> segment(1, 0) == 'production') { echo 'class="active"'; } ?>>
                                            <a href="<?php echo base_url(); ?>Production/home">
                                                    <span class="sub-menu-text">Production</span>
                                            </a>
                                    </li>

                                    <li >
                                        <a href="<?php echo $returnUrl."setting/settings"; ?>"
                                                <span class="sub-menu-text">Settings</span>
                                        </a>
                                    </li>				
			  </ul>
			
			</div>
		</div>
		<!-- /SIDEBAR -->
		<div id="main-content">
			<div class="container">
				<div class="row">
					<div id="content" class="col-lg-12">
						<!-- PAGE HEADER-->
						<div class="row">
							<div class="col-sm-12">
								<div class="page-header">