<?php $ci = get_instance(); ?>
<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left"> Current Batch Details </h3>
</div>
<div class="description">
	
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DELIVERY RUNS CONTENT -->
<!-- DATA TABLES -->

<!--***************************************** PAGE BODY STARTS ***************************************************************-->

<div class="row" ng-app="myApp" ng-controller="proController">
    <div class="col-sm-6 col-md-8">
    <div class="panel panel-info">
        <div class="panel-heading">

         <?php  include_once('subMenu.php'); ?>
         
         		
        </div>
        
        <div class="panel-body">
        	
        	<div class="table-responsive">
				  <table class="table table-striped">
				  	
				  		<thead>
						    <tr>
						    	  <th width="50px">View</th>
							      <th>#</th>
							      <th>Batch #</th>
							      <th>Description</th>
							      <th>Start - End Dates</th>
						    </tr>
						  </thead>
						  
						  <tbody>
						  	<?php 
						  		  $i =1;
							  	  foreach($batches AS $record ){
							  	  	   echo '<tr>
							  	  	          <td>
							  	  	          	<a href="'.base_url().'production/batchDetails/'.$record->batchNum.'"> 
								  	  	          	<button class="btn" name="" type="button">
					                                    <span class="glyphicon glyphicon-eye-open"></span> 
					                                </button>  
				                                </a>
							  	  	          </td>
										      <th scope="row">'.$i++.'</th>
										      <td>'.$record->batchNum.'</td>
										      <td>'; $ci->getPatName($record->batchNum); echo'</td>
										      <td>'.date('Y-m-d',strtotime($record->startDate)).' - '.date('Y-m-d',strtotime($record->endDate)).'</td>									   
 											<td> <button class="btn" ng-click="deleteBatch('.$record->batchNum.')"   type="button"> <span class="glyphicon glyphicon-remove-circle"></span>  </button> </td>
        		                           </tr>
				  		
        								';								
							  	  }
						  	 ?>
						    
						    
						  </tbody>
										   
				  </table>
			</div>
			

        </div>
    </div>
    </div>
</div>



<!--***************************************** PAGE BODY ENDS ***************************************************************-->


<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>

