<!-- STYLER -->


<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left"> Survey </h3>
</div>
<div class="description">

	
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!--  MAIN PAGE HERE -->

<div class="row" >
	<div class="col-lg-6">
		<div class="box border blue">
			<div class="box-title">
				<h4>
					<i class="fa fa-home"></i>Survey question
				</h4>
			</div>
			<div class="box-body">
			
			  
			    <?php 
			      if($finished != 100){
					  echo '
		               <div class="progress">
		               <div class="progress-bar progress-bar-striped active" role="progressbar"
					    aria-valuenow="'.$finished.'" aria-valuemin="0" aria-valuemax="100" style="width:'.$finished.'%">
					    '.$finished.'%
					  </div>
			          </div>';
			      }else{
			      	
			      	 echo '<div class="progress">
						  <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar"
						  aria-valuenow="'.$finished.'" aria-valuemin="0" aria-valuemax="100" style="width:'.$finished.'%">
						    '.$finished.'% Completed (success)
						  </div>
						</div>';			      	 		      	 
			      }
			      
			   ?>				  
			
			 
			
			<?php if(!empty($Question)) { ?>
			
			   <form class="surveyQuestionAnswer">
					  <input type="hidden" value="<?php  echo $roleId; ?>" id="surveyrolsId" name="surveyrolsId" >
					  <input type="hidden" value="<?php  echo $Question->id; ?>" id="surveyquestionId" name="surveyquestionId" >
					  					
					  <fieldset class="form-group">
					    <legend>  <?php   echo $Question->questuion;   ?> </legend>
					    
					    <?php 
					        foreach($answers AS $info){
					        	echo '<div class="form-check">
							       <label class="form-check-label">
							        <input type="radio" class="form-check-input" name="surveyanswerId" id="surveyanswerId" value="'.$info ->id.'"> '.$info ->details.'
							       </label>
							    </div>';
					        }
					    
					    ?>
					  </fieldset>
					  
					   <div class="form-group">
					       <label for="exampleTextarea">Comment</label>
					       <textarea class="form-control" id="comment" name="comment" rows="5"> </textarea>
					   </div>
					  
					  					 
					  <button type="submit" class="btn btn-primary">  Next </button>
					  
				</form>		    
			<?php } else {
				  echo "You have successfully sumbited your survey. Thanks for your time.";
			}?>   
			</div>
		</div>
	</div>	
	
	
</div>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>