<?php
$isNew = true;
$userID = $this->uri->segment ( 4 );
$groups = $this->uri->segment ( 3 );
$user;
if ($userID > 0) {
	$isNew = false;
	$user = $this->ion_auth->user ($userID)->row ();
}
?>
<div class="modal-header">

	<button type="button" class="close" data-dismiss="modal"
		aria-hidden="true">&times;</button>

	<h4 class="modal-title"><?php if($isNew==false){echo 'User Detail';}else{echo 'Create New Home Portal User';}?> </h4>
	<?php if ($isNew==false){?>
	<a href="<?php echo base_url().'compliance/deletePortalUserConfirm/'. $userID; ?>"> <button type="button" class="btn btn-danger" id="deleteButton"
		 data-loading-text="deleting..."><i class="fa fa-trash-o"></i> Delete User</button> </a>
		<?php }?>
</div>

<div class="modal-body">

	<div class="form-horizontal" role="form">
		<input id="userID" type="hidden" value="<?php ?>">
		
		<input id="groups" type="hidden" value="<?php echo $groups; ?>">


		<div class="form-group">
			<label class="col-sm-3 control-label">First Name</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" id="first_name"
					placeholder="Text input"
					value="<?php if($isNew==false){echo $user->first_name; }else{echo '';};?>">
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label">Last Name</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" id="last_name"
					placeholder="Text input"
					value="<?php if($isNew==false){echo $user->last_name; }else{echo '';};?>">
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label">Organization</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" id="company"
					placeholder="Text input"
					value="<?php if($isNew==false){echo $user->company; }else{echo '';};?>">
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label">Log In Email</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" id="email"
					placeholder="Text input"
					value="<?php if($isNew==false){echo $user->email; }else{echo '';};?>">
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-3 control-label">username</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" id="username"
					placeholder="Text input"
					value="<?php if($isNew==false){echo $user->username; }else{echo '';};?>">
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label">Password</label>
			<div class="col-sm-9">
				<input type="password" class="form-control" id="password"
					placeholder="Text input"
					value="<?php if($isNew==false){echo $user->password; }else{echo '';};?>">
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-3 control-label">Phone</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" id="phone"
					placeholder="Text input"
					value="<?php if($isNew==false){echo $user->phone; }else{echo '';};?>">
			</div>
		</div>
	</div>
</div>

<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal"
		id="closeButtonModal">Close</button>
		
	<button type="button" class="btn btn-primary" id="saveButton"
		onclick="saveUser()" data-loading-text="Saving...">Save</button>
</div>

