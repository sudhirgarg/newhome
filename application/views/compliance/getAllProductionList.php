<?php $ci = get_instance(); ?>
<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left">
		 Delivery board
		 <br/>
	    <button class="btn btn-default" onclick="goBack()"> <img src="<?php echo base_url();?>img/go-back-icon.png" width="22px;" > Go Back</button>
	    <a href="<?php echo base_url();?>compliance/printExtra" > <button class="btn btn-info"> <i class="fa fa-print" aria-hidden="true"></i> Print Extra Sheet </button> </a>
	</h3>
</div>
<div class="description">
	
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DELIVERY RUNS CONTENT -->
<!-- DATA TABLES -->

<!--***************************************** PAGE BODY STARTS ***************************************************************-->
<div class="row" >
     <div class="col-sm-12 col-md-12">
	    <div class="panel panel-info">
	        <div class="panel-heading">
	          <h1> <?php 
	                    if(!empty($nhwardInfo)){
	                    	echo $nhwardInfo->LastName.' '.$nhwardInfo->FirstName; echo '      <span class="label label-danger">'.$nhwardInfo->Code.'</span>';
	                    }else{
	                    	echo $nhwardNonProfile->Name;
	                    }
	          			   
	          		?>
	           </h1> 
	        </div>
	     </div>
     </div>
</div>

<div class="row" >

    <div class="col-sm-2 col-md-2">
    <div class="panel panel-info">
        <div class="panel-heading">

         List of patient 
         
         		
        </div>
        
        <div class="panel-body">
        	
        	<div class="table-responsive">
				  <table class="table table-striped">
				  	
				  		<thead>
						    <tr>
						    	  <th width="50px">#</th>
							      <th>Name</th>
							      
						    </tr>
						  </thead>
						  
						  <tbody>
						  	<?php 
						  		  $i =1;
						  		  if(!empty($patInfo)){
						  		  	  foreach($patInfo AS $recordPat){
						  		  	  	echo '<tr>
							  		  	  	<td width="50px">'.$i++.'</td>
							  		  	  	<td>'.$recordPat->LastName.' '.$recordPat->FirstName;
							  		  	  	     if(!empty($recordPat->Code)) { echo '<br/><span class="label label-warning">'.$recordPat->Code.'</span>'; }
							  		  	  	echo '</td>
	    		                            <td>'; $ci->findPatInHospital($recordPat->ID);   echo '</td>
						  		  	  	</tr>';
						  		  	  	
						  		  	  }
						  		  }
						  		  		  		  
							  	 
						  	 ?>
						    						    
						  </tbody>
										   
				  </table>
			</div>
			

        </div>
    </div>
    </div>
    
     <div class="col-sm-10 col-md-10" >
     
    <div class="panel panel-info">
        <div class="panel-heading">

           List Of orders
         
         		
        </div>
        
        <div class="panel-body">
        	
        	<div class="table-responsive">
				  <table class="table table-striped">
				  	
				  		<thead>
						    <tr>
						    	  <th width="50px">#</th>
						    	  <th width="30px">Del</th>
							      <th width="150px">Event</th>
							      <th width="100px">Delivery Date</th>
							      <th width="150px">Status</th>
							      <th colspan="2">Pat & Med Info</th>
							      <th width="30px">Check</th>
						    </tr>
						  </thead>
				
						  <tbody>
						  
						  	<?php 
						  		  $i =1;
						  		  $allEbaordId = array();
						  		  $n =1;
						  		  if(!empty($productionDetails)){
						  		  	   foreach($productionDetails AS $pro){
						  		  	   //	echo '<form class="deliveryDate">';
						  		  	   	$todate = date('Y-m-d');
						  		  	   	$diff = date_diff(date_create($todate),date_create($pro->dateOfDelivery));
						  		  	   	$numdays = $diff->format('%R%a');
						  		  	   	
						  		  	   	$allEbaordId[] = $pro->id;
						  		  	   	echo'<tr class="'; if($numdays < 0){ echo ' danger '; } elseif($numdays ==0){ echo ' success '; }else {  echo ' warning '; } echo ' " >
							  		  	   	<td width="50px">'.$n++.'</th>
		                                    <td class="'; if($numdays < 0){ echo ' danger '; } elseif($numdays ==0){ echo ' success '; }else {  echo ' warning '; } echo ' ">
				  		                         <span >    
						  		  		           <input type="checkbox" name="delEdoardId" class="form-control reOrderCheckBoxClass" value="'.$pro->id.'" id="delEdoardId' . $i++. '">
							  		  	  		</span>	
						  		  	
							  		  	  	</td> 
    		
							  		  	   	<td class="'; if($numdays < 0){ echo ' danger '; } elseif($numdays ==0){ echo ' success '; }else {  echo ' warning '; } echo ' ">'; if(!empty($pro->event)){
							  		  	   	            echo $pro->event.' / '.$pro->ticketID;
						  		  	               }elseif(($pro->type) == 1){
						  		  	               	    echo 'Ticket';
						  		  	               }elseif(($pro->type) == 2){
						  		  	               	    echo 'Med Change';
						  		  	 			   }
						  		  	 			  
						  		  	        echo'</td>
		                                 
		                                          <td>    		                                       
    											     <form class="deliveryDate">	
    		                                             <table>
    		                                                 <tr>  
    		                                                    <input type="hidden" id="eId" name="eId" value="'.$pro->id.'">  
	    		                                                <td><input type="date" value="'.$pro->dateOfDelivery.'" name="dateOfDelivery" id="dateOfDelivery" ></td>
	        			                                        <td><button type="submit" class="btn btn-info" > <i class="fa fa-upload" aria-hidden="true"></i> </button></td>
						    		                         </tr>
						    	  		                  </table>
        			                                 </form>
        			
        			                              </td>
         		                         
							  		  	   	<td>'; 
							  		  	   	    if((($pro->type) == 1) || (($pro->type) == 2)){
							  		  	   	    	if($pro->check == 1){   
							  		  	   	    		echo '<span class="list-group-item list-group-item-danger">';
							  		  	   	    		echo "Already in Delivery bag";
							  		  	   	    		echo '</span>';							  		  	   	    		
							  		  	   	    	}else{
							  		  	   	    		echo "Non pacmed";
							  		  	   	    	}							  		  	   	    	
							  		  	   	    }else{
							  		  	   	    	if($pro->check == 1){
							  		  	   	    		echo '<span class="list-group-item list-group-item-danger">';
							  		  	   	    		echo "Already in Delivery bag";
							  		  	   	    		echo '</span>';	
							  		  	   	    	}else{
							  		  	   	    		$ci -> findOrderIn($pro->ticketID);
							  		  	   	        }	
							  		  	   	        
							  		  	   	    }
							  		  	   	echo'</td>
						  		  	  		
						  		  	  		<td colspan="2">';
						  		  	  		  	$ci -> getPatAndMedInfoBaseOn($pro->id);				  		  	  		
						  		  	  		echo'</td>
							  		  	   	<td class="'; if($numdays < 0){ echo ' danger '; } elseif($numdays ==0){ echo ' success '; }else {  echo ' warning '; } echo ' ">
				  		                         <span >    
						  		  		           <input type="checkbox" name="edoardId" class="form-control reOrderCheckBoxClass" '; 
						  		  		           if($pro->check == 1) { echo " checked ";}   echo' value="'.$pro->id.'" id="edoardId' . $i++. '">
							  		  	  		</span>	
						  		  	
							  		  	  	</td>  
							  		  	  			
						  		  	   	</tr>';
						  		  		  // echo '</form>';
						  		  	   }
						  		  	   
						  		  	  
						  		  }	
						  		  
						  		  $listOfAllEbaordId = implode(',',$allEbaordId);
						  		  echo '<input type="hidden" id="eboardAllIdList" value="'.$listOfAllEbaordId.'">';
						  	 ?>	 
						  	  <input type="hidden" id="eboardIdList" >
						  	  <input type="hidden" id="delEboardIdList" >
						  	 <tr>
						  	 	 <td>  </td>
						  	 	 <td> <button class="btn btn-danger" id="delEboard" onclick="deleteEboard()">Delete</button>  </td>
						  	 	 <td>  </td>
						  	 	 <td>  </td>
						  	 	 <td colspan="3"><button class="btn btn-success pull-right" id="comEboard" onclick="completeEboard()"> Complete </button>  </td>
						  	 	 <td> <button class="btn btn-info" id="updateEboard" onclick="updateEboard()"> Update </button> </td>
						  	 </tr> 
						  </tbody>	
				   
				     						  									   
				  </table>
				  
				   
			</div>
			

        </div>
    </div>
    </div>
    
    
    
</div>



<!--***************************************** PAGE BODY ENDS ***************************************************************-->


<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>

