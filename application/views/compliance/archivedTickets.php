<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<!--<div class="clearfix">
	<h3 class="content-title pull-left">Archived Tickets</h3>
</div>
<div class="description"></div>
</div>
</div>
</div>



<div class="row">

	<div class="col-lg-12">
		<table id="datatable1" cellpadding="0" cellspacing="0" border="0"
			class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th>Date Created</th>
					<th>Patient</th>
					<th>Description</th>
				</tr>
			</thead>
			<tbody>
			<?php
			$generalTicket = $this->comp->getTodayCompleteTickets ();
			if ($generalTicket != FALSE) {
				foreach ( $generalTicket->result () as $generalTicketItem ) {
					?>
				<tr class="curses"
					onclick="loadModal('<?php echo base_url('compliance/editTicketModal/') . '/' . $generalTicketItem->patID . '/' . $generalTicketItem->ticketID ;?>')">
					<?php
					$patient = $this->comp->getCompliancePatient ( $generalTicketItem->patID );
					?>
					<td><h6 class="pull-right"><?php echo $generalTicketItem->dateCreated;?></h6></td>
					<td><h3><?php if($generalTicketItem->ticketTypeReferenceID != 4){ echo '<span class="label label-success">'.$patient->patInitials . '-' . $patient->patID .'</span>' ;} else { $tech = $this->ion_auth->user($generalTicketItem->createdBy)->row(); echo '<span class="label label-danger">'. $tech->first_name .'</span>' ; }?> </h3></td>
					<td><div class="well well-sm"><?php echo $generalTicketItem->Description;?></div>
					</td>
				</tr>   
							<?php  }}else{echo '<tr><td></td></tr>';}?>
							</tbody>
		</table>
	</div>
</div>  
 



<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>-->



<?php $ci = &get_instance();
$index='index.php/';	
?>
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN BREADCRUMBS -->
        <div class="breadcrumbs">
            <h1>Archived Tickets</h1>
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?><?php echo $index; ?>compliance/ticketsdash">Home</a></li>
                <li class="active">Archived Tickets</li>
            </ol>
            <!-- Sidebar Toggle Button -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".page-sidebar">
                <span class="sr-only">Toggle navigation</span>
                <span class="toggle-icon">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </span>
            </button>
            <!-- Sidebar Toggle Button -->
        </div>
        <!-- END BREADCRUMBS -->
        <!-- BEGIN SIDEBAR CONTENT LAYOUT -->
        <div class="page-content-container">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="fa fa-ticket font-dark"></i>
                                <span class="caption-subject bold uppercase">Archived Tickets</span>
                            </div>
                            <div class="tools"> </div>
                        </div>
                        <div class="portlet-body">
                            <table class="table table-striped table-bordered table-hover" id="sample_2">
                                <thead>
                                    <tr>
                                        <th> Date Created </th>
                                        <th> Patient </th>
                                        <th> Description </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $generalTicket = $this->comp->getTodayCompleteTickets ();
                                    if ($generalTicket != FALSE) {
                                        foreach ( $generalTicket->result () as $generalTicketItem ) {
                                            $patient = $this->comp->getCompliancePatient ( $generalTicketItem->patID );
                                    ?>
                                        <tr onclick="loadModal('<?php echo base_url() ?><?php echo $index ?>compliance/editTicketModal/<?php echo $generalTicketItem->patID; ?>/<?php echo $generalTicketItem->ticketID ; ?>')">
                                            <td> <?php echo $generalTicketItem->dateCreated; ?> </td>
                                            <td> <?php if($generalTicketItem->ticketTypeReferenceID != 4){ echo '<span class="label label-success">'.$patient->patInitials . '-' . $patient->patID .'</span>' ; } else{ $tech = $this->ion_auth->user($generalTicketItem->createdBy)->row(); echo '<span class="label label-danger">'. $tech->first_name .'</span>' ; } ?> </td>
                                            <td> <?php echo $generalTicketItem->Description; ?> </td>
                                        </tr>
                                    <?php }
                                    } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
        </div>
        <!-- END SIDEBAR CONTENT LAYOUT -->
    </div>
