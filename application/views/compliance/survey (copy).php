<?php $ci = &get_instance();
$index='index.php/';	
?>
<!-- STYLER -->


<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left"> Survey </h3>
</div>
<div class="description">

	
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!--  MAIN PAGE HERE -->

<div class="row" >
	<div class="col-lg-6">
		<div class="box border blue">
			<div class="box-title">
				<h4>
					<i class="fa fa-home"></i>Survey 
				</h4>
			</div>
			<div class="box-body">
			
			     <form action="<?php echo base_url();?><?php echo $index; ?>compliance/surveyQuestion" method="POST">
					
					  <div class="form-group">
					    <label for="homeName">Select home</label>					    
					     <select data-placeholder="Choose home..." name="nhId" id="nhId" class="chosen-select form-control" style="width:280px;" tabindex="2">
							<option value=""></option>
							<?php 
							   	    
								foreach($homeList AS $comboRecord){
									echo '<option value="'.$comboRecord->ID.'">'.$comboRecord->LastName.' '.$comboRecord->FirstName.' </option>';
								}
								
								
							?>										
						</select>
					  </div>
					
					  <fieldset class="form-group">
					    <legend> Which of following best describe your role? </legend>
					    
					    <div class="form-check">
					       <label class="form-check-label">
					        <input type="radio" class="form-check-input" name="roleName" id="roleName" value="Administrator"> Administrator
					       </label>
					    </div>		    
					   
					    <div class="form-check">
					       <label class="form-check-label">
					        <input type="radio" class="form-check-input" name="roleName" id="roleName" value="Caregiver" checked > Caregiver
					       </label>
					    </div>
					    
					    <div class="form-check disabled">
					        <label class="form-check-label">
					        <input type="radio" class="form-check-input" name="roleName" id="roleName" value="Case Manager Nurse" > Case Manager Nurse
					        </label>
					    </div>
					    
					    <div class="form-check">
					        <label class="form-check-label">
						        <input type="radio" class="form-check-input" name="roleName" id="roleName" value="Patient" > Patient
					        </label>
					    </div>
					    
					     <div class="form-check">
					        <label class="form-check-label">
						        <input type="radio" class="form-check-input" name="roleName" id="roleName" value="Physician Executive" > Physician Executive
					        </label>
					    </div>
					    
					    
					    <div class="form-check disabled">
					        <label class="form-check-label">
					        <input type="radio" class="form-check-input" name="roleName" id="roleName" value="Other" > Other :
					        <input type="text" name="otherRole" >
					        </label>
					    </div>
					    
					  </fieldset>
					  
					  					 
					  <button type="submit" class="btn btn-primary">Continue </button>
					  
					</form>
				   
			</div>
		</div>
	</div>	
	
	<div class="col-lg-6">
		<div class="box border blue">
			<div class="box-title">
				<h4>
					<i class="fa fa-home"></i>Survey List
				</h4>
			</div>
			<div class="box-body">
	              
	                <table class="table table-hover">
					    <thead>
					      <tr>
					        <th>#</th>
					        <th>Home name</th>
					        <th>Roles</th>
					        <th>Satisfaction</th>					        
					        <th>view</th>
					      </tr>
					    </thead>
					    <tbody>
					     <?php 
					     $i = 1;
					         foreach($survey AS $record){
					         	 echo '<tr>
							        <td>'.$i++.'</td>
							        <td>'.$record->LastName.' '.$record->FirstName.'</td>
							        <td>'.$record->roles.'</td>
		                            <td>';
					         	      $ci -> getSurveyInPersentage($record -> id);
		                          echo'% </td><td>		                               
										<a href="'.base_url().''.$index.'compliance/viewSurvey/'.$record->id.'">
							                <button type="button" class="btn btn-default btn-sm">
											    <span class="glyphicon glyphicon-eye-open"></span>
							                </button>
							            </a>				           
							
				                     </td>
							      </tr>';
					         }
					         
					     ?>
					      
					      					      
					    </tbody>
				  </table>
	        </div>
		</div>
	</div>	
	
</div>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>
