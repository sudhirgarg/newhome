
<!--<div class="clearfix">
	<h3 class="content-title pull-left">Compliance Med Changes</h3>
</div>
<div class="description"></div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!--  MAIN PAGE HERE -->
<!--
<div class="row">

	<div class="col-lg-4">
		<table id="1" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th><h3 class="center"><span class="label label-info"> Awaiting Pharmacist Check </span></h3></th>
				</tr>
			</thead>
			<tbody>
				
				<?php
									
					 if ($waitingPharmCheck != FALSE) {
							foreach ( $waitingPharmCheck->result () as $waitingPharmCheckItem ) {
							
							$tech = $this->ion_auth->user($waitingPharmCheckItem->technicianUserID)->row();							
					
							echo '<tr onclick="loadModal(\''.base_url().'compliance/editMedChangeModal/'.$waitingPharmCheckItem->patID.'/'. $waitingPharmCheckItem->medChangeID.'\')">
								<td>									
										
									<h6 class="pull-right">		
										 <table>
										 	<tr>';
												if((!empty($waitingPharmCheckItem->NHWardID)) && ($waitingPharmCheckItem->NHID !=59)){
													
													 //$ci->getHomeANDdeliveryInfo($waitingPharmCheckItem->NHWardID); 
												}else{
													
													$dR = $waitingPharmCheckItem->DeliveryRoute;
			
													$day  = substr($dR,0,3);
												    $time = substr($dR,3,4);
															
												    $outp ="";
													if($day == "MON"){ $outp =" MONDAY";    }
													if($day == "TUE"){ $outp =" TUESDAY";   }
													if($day == "WED"){ $outp =" WEDNESDAY"; }
												    if($day == "THU"){ $outp =" THURSDAY";  }
												    if($day == "FRI"){ $outp =" FRIDAY";    }	    	
													
													
													$d = strtotime("next $outp");
													
													echo '<td class="bg-warning">
											 			 Next RDD : '. date('Y-m-d',$d).'&nbsp;<br/>&nbsp;
											 		</td>';
												}
												
										 		
										 		
										 	echo'<td>
										 			Start Date: '.$waitingPharmCheckItem->startDate . '<br> MDD : ' . $waitingPharmCheckItem->endDate.'	
										 		</td>
										 	</tr>								 
										 </table>		  							
									</h6>
									<h3><span class="label label-info">'.$waitingPharmCheckItem->FirstName .' '.$waitingPharmCheckItem->LastName.'</span> </h3>
									<div class="well well-sm">'.$waitingPharmCheckItem->changeText.'</div>
								</td>
							</tr>';
					 	} 
					 } else {
						 echo '<tr><td></td></tr>';
					 } 
					 
				?>
				
			</tbody>
		</table>
	</div>


	<div class="col-lg-4">
		<table id="2" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th><h3 class="center"><span class="label label-warning"> Awaiting Action </span></h3></th>
				</tr>
			</thead>
			<tbody>
				
				<?php				
					
					if ($waitingAction != FALSE) {
						
						foreach ( $waitingAction->result () as $waitingActionItem ) {							
							
							$tech = $this->ion_auth->user($waitingActionItem->pharmacistUserID)->row();	
					
							echo '<tr>
								<td>
								    
									 <h6 class="pull-right">		
										 <table cellpadding="10">
										 	<tr>';
												if((!empty($waitingActionItem->NHWardID)) && ($waitingActionItem->NHID !=59)){
													
													// $ci->getHomeANDdeliveryInfo($waitingActionItem->NHWardID); 
												}else{
													
													$dR = $waitingActionItem->DeliveryRoute;
			
													$day  = substr($dR,0,3);
												    $time = substr($dR,3,4);
															
												    $outp ="";
													if($day == "MON"){ $outp =" MONDAY";    }
													if($day == "TUE"){ $outp =" TUESDAY";   }
													if($day == "WED"){ $outp =" WEDNESDAY"; }
												    if($day == "THU"){ $outp =" THURSDAY";  }
												    if($day == "FRI"){ $outp =" FRIDAY";    }	    	
													
													
													$d = strtotime("next $outp");
													
													echo '<td class="bg-warning">
											 			 Next RDD : '. date('Y-m-d',$d).'<br/>&nbsp;	
											 		</td>';
												}
												
										 		
										 		
										 	echo'<td>
										 			Tech : '.$tech->first_name . '<br> Start Date: '.$waitingActionItem->startDate . '<br/> MDD : ' . $waitingActionItem->endDate.'	
										 		</td>
										 	</tr>								 
										 </table>		  							
									</h6>
									
									
									 <h3  onclick="loadModal(\''.base_url('index.php/compliance/editMedChangeModal/') . '/' . $waitingActionItem->patID . '/' . $waitingActionItem->medChangeID.'\')" ><span class="label label-info">'.$waitingActionItem->FirstName .' '.$waitingActionItem->LastName.'</span> </h3>
									 <div class="well well-sm">'.$waitingActionItem->changeText.'</div>
								</td>
							</tr>';
						
					  }
				   }else{
					  	echo '<tr><td></td></tr>';
				   }
				?>
				
			</tbody>
		</table>
	</div>

	<!-- div class="col-lg-4">
		<table id="3" cellpadding="0" cellspacing="0" border="0"
			class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th><h3 class="center"><span class="label label-success">Awaiting Action</span></h3></th>
				</tr>
			</thead>
			<tbody>
			
				<?php				
					
					if ($inProgress != FALSE) {
						foreach ( $inProgress->result () as $inProgressItem ) {							
							
							$tech = $this->ion_auth->user($inProgressItem->technicianUserID)->row();
				
				            echo '<tr onclick="loadModal(\''.base_url('compliance/editMedChangeModal/') . '/' . $inProgressItem->patID . '/' . $inProgressItem->medChangeID.'\')">								
								<td>
								    <h6 class="pull-right">		
										 <table >
										 	<tr>';
											
												if((!empty($inProgressItem->NHWardID)) &&($inProgressItem->NHID !=59)){
													
													 $ci->getHomeANDdeliveryInfo($inProgressItem->NHWardID); 
												}else{
													
													$dR = $inProgressItem->DeliveryRoute;
			
													$day  = substr($dR,0,3);
												    $time = substr($dR,3,4);
															
												    $outp ="";
													if($day == "MON"){ $outp =" MONDAY";    }
													if($day == "TUE"){ $outp =" TUESDAY";   }
													if($day == "WED"){ $outp =" WEDNESDAY"; }
												    if($day == "THU"){ $outp =" THURSDAY";  }
												    if($day == "FRI"){ $outp =" FRIDAY";    }	    	
													
													
													$d = strtotime("next $outp");
													
													echo '<td class="bg-warning">
											 			Next RDD : '. date('Y-m-d',$d).'<br/>&nbsp;	
											 		</td>';
												}
												
										 		
										 		
										 	echo'<td>
										 			Tech : '.$tech->first_name . '<br> Start Date: '.$inProgressItem->startDate . '<br> MDD : ' . $inProgressItem->endDate.'	
										 		</td>
										 	</tr>								 
										 </table>		  							
									</h6>								  
									    
									<h3><span class="label label-info">'.$inProgressItem->FirstName .' '.$inProgressItem->LastName.'</span> </h3>
									<div class="well well-sm">'.$inProgressItem->changeText.' </div>
								</td>								
							</tr>';
						 }
                    } else {
                    	 echo '<tr><td></td></tr>';
				    }
					
				 ?>
			</tbody>
			
		</table>		
	</div-->


<!--</div>

<!-- /CALENDAR & CHAT -->
<!--<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>-->







































<?php $ci = &get_instance();
$index='index.php/';	
?>
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN BREADCRUMBS -->
        <div class="breadcrumbs">
            <h1>Compliance Med Changes</h1>
            <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li class="active">Compliance Med Changes</li>
            </ol>
        </div>
        <!-- END BREADCRUMBS -->
        <!-- BEGIN SIDEBAR CONTENT LAYOUT -->
        <div class="page-content-container">
            <div class="page-content-row">
                <div class="page-content-col">
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <!-- BEGIN : RIBBONS -->
                    <div class="row">
                        <div class="col-lg-6"><!-- First Div Start -->
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class=" icon-layers font-red"></i>
                                        <span class="caption-subject font-red bold uppercase">Awaiting Pharmacist Check</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
                                        <?php if ($waitingPharmCheck != FALSE) {
                                            foreach ( $waitingPharmCheck->result () as $waitingPharmCheckItem ) {
                                                $tech = $this->ion_auth->user($waitingPharmCheckItem->technicianUserID)->row();
                                        ?>
                                                <div class="col-md-12">
                                                    <div class="mt-element-ribbon bg-grey-steel">
                                                        <a href="#" onclick="loadModal('<?php echo base_url() ?><?php echo $index; ?>compliance/editMedChangeModal/<?php echo $waitingPharmCheckItem->patID; ?>/<?php echo $waitingPharmCheckItem->medChangeID; ?>')">
                                                            <?php if((!empty($waitingPharmCheckItem->NHWardID)) && ($waitingPharmCheckItem->NHID !=59)){
                                                            //$ci->getHomeANDdeliveryInfo($waitingPharmCheckItem->NHWardID); 
                                                            }else{
                                                                $dR = $waitingPharmCheckItem->DeliveryRoute;
                                                                $day  = substr($dR,0,3);
                                                                $time = substr($dR,3,4);
                                                                $outp ="";
                                                                if($day == "MON"){ $outp =" MONDAY";    }
                                                                if($day == "TUE"){ $outp =" TUESDAY";   }
                                                                if($day == "WED"){ $outp =" WEDNESDAY"; }
                                                                if($day == "THU"){ $outp =" THURSDAY";  }
                                                                if($day == "FRI"){ $outp =" FRIDAY";    }	    	
                                                                $d = strtotime("next $outp"); ?>
                                                                <div class="ribbon ribbon-shadow ribbon-color-danget uppercase"><?php echo 'Next RDD : '.date('Y-m-d',$d); ?></div>
							    <?php } ?>
                                                            <div class="ribbon ribbon-shadow ribbon-color-success uppercase"><?php echo 'Start Date : '.$waitingPharmCheckItem->startDate . '<br>MDD : ' . $waitingPharmCheckItem->endDate ?></div>
                                                            <div class="ribbon ribbon-border-dash ribbon-shadow ribbon-color-danger uppercase"><?php echo $waitingPharmCheckItem->FirstName .' '.$waitingPharmCheckItem->LastName; ?></div>
                                                            <p class="ribbon-content"><?php echo $waitingPharmCheckItem->changeText; ?></p>
                                                        </a>
                                                    </div>
                                                </div>
                                        <?php }}else{ ?>
                                                <div class="col-md-12">
                                                    No Record Found!
                                                </div>
                                        <?php } ?>                                                
                                    </div>
                                </div>
                            </div>
                        </div><!-- First Div End -->
                        <div class="col-lg-6"><!-- Second Div Start -->
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class=" icon-layers font-red"></i>
                                        <span class="caption-subject font-red bold uppercase">Awaiting Action</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
                                        <?php if ($waitingAction != FALSE) {
                                        foreach ( $waitingAction->result () as $waitingActionItem ){
                                            $tech = $this->ion_auth->user($waitingActionItem->pharmacistUserID)->row();
                                        ?>
                                            <div class="col-md-12">
                                                <div class="mt-element-ribbon bg-grey-steel">
                                                    <?php if((!empty($waitingActionItem->NHWardID)) && ($waitingActionItem->NHID !=59)){
                                                        // $ci->getHomeANDdeliveryInfo($waitingActionItem->NHWardID); 
                                                    }else{
                                                        $dR = $waitingActionItem->DeliveryRoute;
                                                        $day  = substr($dR,0,3);
                                                        $time = substr($dR,3,4);
                                                        $outp ="";
                                                        if($day == "MON"){ $outp =" MONDAY";    }
                                                        if($day == "TUE"){ $outp =" TUESDAY";   }
                                                        if($day == "WED"){ $outp =" WEDNESDAY"; }
                                                        if($day == "THU"){ $outp =" THURSDAY";  }
                                                        if($day == "FRI"){ $outp =" FRIDAY";    }	    	
													
                                                        $d = strtotime("next $outp"); ?>
                                                        <div class="ribbon ribbon-shadow ribbon-color-danger uppercase"><?php echo 'Next RDD : '.date('Y-m-d',$d); ?></div> 
                                                    <?php } ?> 
                                                    <div class="ribbon ribbon-shadow ribbon-color-success uppercase"><?php echo 'Tech : '.$tech->first_name . '<br/>Start Date : '.$waitingActionItem->startDate . '<br/>MDD : ' . $waitingActionItem->endDate; ?></div>
                                                    <div class="ribbon ribbon-border-dash ribbon-shadow ribbon-color-danger uppercase" onclick="loadModal('<?php echo base_url() ?><?php echo $index ?>compliance/editMedChangeModal/<?php echo $waitingActionItem->patID ?>/<?php echo $waitingActionItem->medChangeID; ?>')"><?php echo $waitingActionItem->FirstName .' '.$waitingActionItem->LastName; ?></div>
                                                    <p class="ribbon-content"><?php echo $waitingActionItem->changeText; ?></p>
                                                </div>
                                            </div>
                                        <?php }}else{ ?>
                                            <div class="col-md-12">
                                                No Record Found!
                                            </div>
                                        <?php } ?>                                                 
                                    </div>
                                </div>
                            </div>
                        </div><!-- Second Div End -->	 
                    </div>
                    <!-- END : RIBBONS -->
                    <!-- END PAGE BASE CONTENT -->
                </div>
            </div>
        </div>
        <!-- END SIDEBAR CONTENT LAYOUT -->
    </div>
