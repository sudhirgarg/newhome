<?php $ci =&get_instance();
$index='index.php/';
 ?>
<!-- STYLER -->


<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<!--<div class="clearfix">
	<h3 class="content-title pull-left"> Survey report
	<br/>
	<button class="btn btn-default" onclick="goBack()"> <img src="<?php echo base_url();?>img/go-back-icon.png" width="22px;" > Go Back</button>
	</h3>
</div>
<div class="description">

	
</div>
</div>
</div>
</div>


<div class="row" >
	<div class="col-lg-6">
		<div class="box border blue">
			<div class="box-title">
				<h4>
					<i class="fa fa-home"></i>Home :  
					  <?php 
					  		echo $role -> LastName.' '.$role -> FirstName.' ('.$role -> roles.')  -  ';
					  		$ci -> getSurveyInPersentage($role -> id);
					  ?>%
				</h4>
					                   
				
			</div>
			
			<div class="box-body">			
			    <div class="panel panel-default">
			    <input type="hidden" id="roles" value="<?php echo $role -> id; ?>" >
			  	    		    
			    <?php 
			       if(!empty($info)){
			       	 $i = 1;
			       	   foreach($info AS $record){
			       	   	   echo '
								  <div class="panel-body">
			                         '.$i++.') '.$record -> questuion .' : '.$record -> details.'
		                             <br/>
		                             '.$record -> comment.'
		                           </div>
								  <div class="panel-footer"></div>
								';
			       	   }
			       	   
			       	   echo '<div class="panel-footer">
							
							 <button type="button" class="btn btn-info btn-sm" onclick="printSurvey()">
						          <span class="glyphicon glyphicon-print"></span> Print
						     </button>
							
							  <button type="button" class="btn btn-default btn-sm" onclick="closeSurvey();" id="closeS">
						          <span class="glyphicon glyphicon-eye-close"></span> Close survey
						      </button>
													
							</div>';
			       	   
			       }			      
			   ?>	
			    </div>	
			
			</div>
		</div>
	</div>	
	
	
</div>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>-->




<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN BREADCRUMBS -->
        <div class="breadcrumbs">
            <h1>Survey report</h1>
            <a class="btn btn-sm btn-info" href="#" onclick="goBack()" style="margin-left: 9px;padding: 4px 10px 4px 10px;">Go Back</a>
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?><?php echo $index; ?>compliance/ticketsdash">Home</a></li>
                <li><a href="<?php echo base_url(); ?><?php echo $index; ?>compliance/survey">Survey List</a></li>
                <li class="active">Survey report</li>
            </ol>
            <!-- Sidebar Toggle Button -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".page-sidebar">
                <span class="sr-only">Toggle navigation</span>
                <span class="toggle-icon">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </span>
            </button>
            <!-- Sidebar Toggle Button -->
        </div>
        <!-- END BREADCRUMBS -->
        <!-- BEGIN SIDEBAR CONTENT LAYOUT -->
        <div class="page-content-container">
            <div class="page-content-row">
                <div class="page-content-col">
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <!-- BEGIN : LISTS -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-body">
                                    <div class="mt-element-list">
                                        <div class="mt-list-head list-simple font-white bg-red">
                                            <div class="list-head-title-container">
                                                <h3 class="list-title"><i class="fa fa-home"></i>Home : 
                                                    <?php echo $role -> LastName.' '.$role -> FirstName.' ('.$role -> roles.') -  ';
                                                    $ci -> getSurveyInPersentage($role -> id); ?>%
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="mt-list-container list-simple">
                                            <input type="hidden" id="roles" value="<?php echo $role -> id; ?>" >
                                            <?php if(!empty($info)){ ?>
                                                <ul>
                                                <?php $i = 1;
			       	                foreach($info AS $record){ ?>
                                                    <li class="mt-list-item">
                                                        <div class="list-icon-container done">
                                                            <?php echo $i; ?>)
                                                        </div>
                                                        <div class="list-item-content">
                                                            <h3 class="uppercase">
                                                                <?php echo $record -> questuion .' : '.$record -> details;  ?>
                                                            </h3>
                                                            <?php if(!empty($record -> comment)){ ?>
                                                            <br/>
                                                            <p><?php echo $record -> comment; ?></p>
                                                            <?php } ?>	
                                                        </div>
                                                    </li>
                                                <?php $i++; } ?>
                                                </ul>
                                            <?php } ?>
                                        </div>
                                        <?php if(!empty($info)){ ?>
                                            <div class="clearfix">
                                                <a href="#" class="btn btn-sm red" onclick="printSurvey()"> Print
                                                    <i class="fa fa-print"></i>
                                                </a>
                                                <a href="#" class="btn btn-sm blue" onclick="closeSurvey();">
                                                    <i class="fa fa-eye-slash"></i> Close Survey
                                                </a>
                                            </div>
                                        <?php } ?>						
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END : LISTS -->
                    <!-- END PAGE BASE CONTENT -->
                </div>
            </div>
        </div>
        <!-- END SIDEBAR CONTENT LAYOUT -->
