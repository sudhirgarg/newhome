<?php
$patID = $this->uri->segment ( 3 );
$patient = $this->comp->getCompliancePatient ( $patID );
$homeID = $this->uri->segment ( 4 );
?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"
		aria-hidden="true">&times;</button>
	<h4 class="modal-title">Edit Client</h4>
</div>
<div class="modal-body">
	<div class="form-horizontal" role="form">
		<input id="patID" type="hidden" value="<?php echo $patID;?>">
		<div class="form-group">
			<label class="col-sm-3 control-label">Home Name</label>
			<div class="col-sm-9">
				<select class="form-control" id="homeID">
				<?php
				$homes = $this->comp->getHome ();
				
				foreach ( $homes->result () as $home ) {
					?>
					<option value="<?php echo $home->homeID;?>"
						<?php if($homeID == $home->homeID){ echo 'selected="selected"';}?>><?php echo $home->homeName;?></option>
				<?php }?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label">Delivery/Pick Up</label>
			<div class="col-sm-9">
				<select class="form-control" id="deliver">
					<option value="0"
						<?php if($patient->deliver == FALSE){ echo 'selected="selected"';}?>>Pick
						Up</option>
					<option value="1"
						<?php if($patient->deliver == TRUE){ echo 'selected="selected"';}?>>Delivery</option>
				</select>
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-3 control-label">Controlled</label>
			<div class="col-sm-9">
				<select class="form-control" id="controlled">
					<option value="0"
						<?php if($patient->controlled == FALSE){ echo 'selected="selected"';}?>>No</option>
					<option value="1"
						<?php if($patient->controlled == TRUE){ echo 'selected="selected"';}?>>Yes</option>
				</select>
			</div>
		</div>
		
	<div class="form-group">
			<label class="col-sm-3 control-label">Delivery Day and Time</label>
			<div class="col-sm-9">
				<select class="form-control" id="deliveryTimeID">
				<?php
				$dayTimes = $this->comp->getProductionTimeReference ();
				
				foreach ( $dayTimes->result () as $dayTime ) {
					?>
					<option value="<?php echo $dayTime->productionTimeID;?>"
						<?php if($patient->deliveryTimeID == $dayTime->productionTimeID){ echo 'selected="selected"';}?>><?php echo $dayTime->productionDay . ' ' . $dayTime->productionTime;?></option>
				<?php }?>
				</select>
			</div>
		</div>
        
		<div class="form-group">
			<label class="col-sm-3 control-label">Note</label>
			<div class="col-sm-9">
				<textarea class="form-control" rows="3" id="note"><?php echo $patient->note;?></textarea>
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-3 control-label">Delivery Instructions</label>
			<div class="col-sm-9">
				<textarea class="form-control" rows="3" id="deliveryInstructions"><?php echo $patient->deliveryInstructions;?></textarea>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal"
			id="closeButtonModal">Close</button>
		<button type="button" class="btn btn-primary" id="saveButton"
			onclick="savePat()" data-loading-text="Saving...">Save changes</button>
	</div>