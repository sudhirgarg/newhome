<?php
	$isNew = true;
	$userID = $this->ion_auth->user()->row()->id;
	$user;
	if ($userID > 0) {
		$isNew = false;
		$user = $this->ion_auth->user ($userID)->row ();
	}
?>

<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->

<!--<div class="clearfix">
	<h3 class="content-title pull-left">Dashboard</h3>
</div>
<div class="description">
	
</div>
</div>
</div>
</div>


<div class="row">
	<div class="col-md-12">

		<div class="well well-sm">
			<h3><?php echo $this->lang->line('HELLO');?> <?php echo $this->ion_auth->user()->row()->first_name;?> </h3>
		</div>
	
		<div class="col-md-6">
			<div class="panel panel-primary">
				
				
				
				<div class="bs-example">
			    <ul class="nav nav-tabs">
			        <li class="active"><a data-toggle="tab" href="#sectionA">Personal Info</a></li>
			        <li><a data-toggle="tab" href="#sectionB">Change Password</a></li>
			        <li><a data-toggle="tab" href="#sectionC">Manage Emails</a></li>
			        <!--li class="dropdown">
			            <a data-toggle="dropdown" class="dropdown-toggle" href="#">Dropdown <b class="caret"></b></a>
			            <ul class="dropdown-menu">
			                <li><a data-toggle="tab" href="#dropdown1">Dropdown1</a></li>
			                <li><a data-toggle="tab" href="#dropdown2">Dropdown2</a></li>
			            </ul>
			        </li-->
<!--			    </ul>
			    <div class="tab-content">
			        <div id="sectionA" class="tab-pane fade in active">
		            <h3 class="panel-title"> </h3>          
		          
					  <div class="panel-body">
						  <div>
	              			<a href="#" class="close" data-dismiss="alert">&times;</a>		  	
						  	<div id="formChange"></div>
						  </div>
					  	
					    <div class="modal-body">
		
						<div class="form-horizontal" role="form">
							<input id="userID" type="hidden" value="<?php echo $user->id  ?>">
	
						<input id="groups" type="hidden" value="<?php if($isNew==false){							
						
						    $a= $this->ion_auth->get_users_groups()->result(); 
							$groups = array();
							foreach ($a as $userGroup){	
								$groups[] = $userGroup->id;
							}
							echo $groups;
																
						 }else{
						 	echo '';
						 } 
						 ?>">


					<div class="form-group">
						<label class="col-sm-3 control-label">First Name</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="first_name" placeholder="Enter First Name" value="<?php if($isNew==false){echo $user->first_name; }else{echo '';} ?>">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label">Last Name</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="last_name" placeholder="Enter Last Name" value="<?php if($isNew==false){echo $user->last_name; }else{echo '';} ?>">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label">Organization</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="company" placeholder="Enter Organization" value="<?php if($isNew==false){echo $user->company; } else { echo '';} ?>">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label">Log In Email</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="email" placeholder="Enter Email" readonly="readonly" value="<?php if($isNew==false){echo $user->email; } else {echo '';} ?>">
						</div>
					</div>
		
					<div class="form-group">
						<label class="col-sm-3 control-label">username</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="username" placeholder="Enter Username" readonly="readonly" value="<?php if($isNew==false){echo $user->username; } else {echo '';} ?>">
						</div>
					</div>

					<!--div class="form-group">
						<label class="col-sm-3 control-label">Password</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="password" placeholder="Enter password" value="<?php if($isNew==false){ echo $user->password; } else { echo ''; }?>">
						</div>
					</div-->
		
<!--				    <div class="form-group">
						<label class="col-sm-3 control-label">Phone</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="phone" placeholder="Enter phone #" value="<?php if($isNew==false){ echo $user->phone; } else { echo '';}?>">
						</div>
					</div>
				</div>
				</div>

				   <div class="modal-footer">			
					  <button type="button" class="btn btn-primary" id="saveButton" onclick="saveHomeUser()" data-loading-text="Saving...">Save</button>
				   </div>
			    </div> 
        </div>
        
        
        <div id="sectionB" class="tab-pane fade">
           
              <div class="panel-body">	
              		 <div>
              			<a href="#" class="close" data-dismiss="alert">&times;</a>		  	
					  	<div id="passwordChange"></div>
					  </div>
					  
					    <div class="modal-body">
		
						<div class="form-horizontal" role="form">
							<input id="userID" type="hidden" value="<?php echo $user->id  ?>">
	
						<input id="groups" type="hidden" value="<?php if($isNew==false){							
						
						    $a= $this->ion_auth->get_users_groups()->result(); 
							$groups = array();
							foreach ($a as $userGroup){	
								$groups[] = $userGroup->id;
							}
							echo $groups;
																
						 }else{
						 	echo '';
						 } 
						 ?>">		

					<div class="form-group">
						<label class="col-sm-3 control-label">Current Password</label>
						<div class="col-sm-9">
							<input type="password" class="form-control" id="currentPassword" placeholder="Enter current password" value="">
						</div>
					</div>
						<div class="form-group">
						<label class="col-sm-3 control-label">New Password</label>
						<div class="col-sm-9">
							<input type="password" class="form-control" id="password" placeholder="Enter new password" value="">
						</div>
					</div>
						<div class="form-group">
						<label class="col-sm-3 control-label">Confirm Password</label>
						<div class="col-sm-9">
							<input type="password" class="form-control" id="passconf" placeholder="Confirm new password" value="">
						</div>
					</div>
		
				 
				</div>
				</div>

				   <div class="modal-footer">			
					  <button type="button" class="btn btn-primary" id="saveButtonPassword" onclick="saveNewUserPassword()" data-loading-text="Saving...">Save</button>
				   </div>
			    </div>
        		
        </div>
        
        	
        	<div id="sectionB" class="tab-pane fade">
           
              <div class="panel-body">	
              		 <div>
              			<a href="#" class="close" data-dismiss="alert">&times;</a>		  	
					  	<div id="passwordChange"></div>
					  </div>
					  
					    <div class="modal-body">
		
						<div class="form-horizontal" role="form">
							<input id="userID" type="hidden" value="<?php echo $user->id  ?>">
	
						<input id="groups" type="hidden" value="<?php if($isNew==false){							
						
						    $a= $this->ion_auth->get_users_groups()->result(); 
							$groups = array();
							foreach ($a as $userGroup){	
								$groups[] = $userGroup->id;
							}
							echo $groups;
																
						 }else{
						 	echo '';
						 } 
						 ?>">		

					<div class="form-group">
						<label class="col-sm-3 control-label">Current Password</label>
						<div class="col-sm-9">
							<input type="password" class="form-control" id="currentPassword" placeholder="Enter current password" value="">
						</div>
					</div>
						<div class="form-group">
						<label class="col-sm-3 control-label">New Password</label>
						<div class="col-sm-9">
							<input type="password" class="form-control" id="password" placeholder="Enter new password" value="">
						</div>
					</div>
						<div class="form-group">
						<label class="col-sm-3 control-label">Confirm Password</label>
						<div class="col-sm-9">
							<input type="password" class="form-control" id="passconf" placeholder="Confirm new password" value="">
						</div>
					</div>
		
				 
				</div>
				</div>

				   <div class="modal-footer">			
					  <button type="button" class="btn btn-primary" id="saveButtonPassword" onclick="saveNewUserPassword()" data-loading-text="Saving...">Save</button>
				   </div>
			    </div>
        		
        	</div>
        
	        <div id="sectionC" class="tab-pane fade">
	        	
	           <div class="panel-body">	
              		 <div>
              			<a href="#" class="close" data-dismiss="alert">&times;</a>		  	
					  	<div id="credentialAlert"></div>
					  </div>
					  
					    <div class="modal-body">
		
						<div class="form-horizontal" role="form">
							<input id="userIDEmail" type="hidden" value="<?php echo $user->id  ?>">
	
								

						<div class="form-group">
							<label class="col-sm-3 control-label">Choose credential </label>
							<div class="col-sm-9">
								<input type="checkbox" class="" id="ticket"    value="1"   <?php if($emailAuth->ticket == 1)    echo " checked"; ?> >Tickets <br/>
								<input type="checkbox" class="" id="medChange" value="1"   <?php if($emailAuth->medChange == 1) echo " checked"; ?> >Med Changes <br/>
								<input type="checkbox" class="" id="comment"   value="1"   <?php if($emailAuth->comment == 1)   echo " checked"; ?> >Comments <br/>
							</div>
						</div>						
		
				 
					</div>
					</div>

				   <div class="modal-footer">			
					  <button type="button" class="btn btn-primary" id="saveButtonPassword" onclick="saveEmailCredential()" data-loading-text="Saving...">Save</button>
				   </div>
			    </div>
	        
	        </div>
	        
	        
	        
	        
	        <div id="dropdown2" class="tab-pane fade">
	            <h3>Dropdown 2</h3>
	            <p>Donec vel placerat quam, ut euismod risus. Sed a mi suscipit, elementum sem a, hendrerit velit. Donec at erat magna. Sed dignissim orci nec eleifend egestas. Donec eget mi consequat massa vestibulum laoreet. Mauris et ultrices nulla, malesuada volutpat ante. Fusce ut orci lorem. Donec molestie libero in tempus imperdiet. Cum sociis natoque penatibus et magnis.</p>
	        </div>
	    </div>
		</div>
					
				
				
			  
			</div>
			
			
			
			<div class="panel panel-primary">
			  <div class="panel-heading">
			    	<h3 class="panel-title">Assigned Quizes</h3>
			  </div>
			  <div class="panel-body">
			    	Coming Soon<br/>
			    	<br/><br/><br/>
			  </div>
			</div>
			
		</div>

		<div class="col-md-6">
			<div class="panel panel-info">
			  <div class="panel-heading">
			    	<h3 class="panel-title">User Activites </h3>
			  </div>
			  <div class="panel-body">
			    	Last Login :  AAAAA<br/>
			    	Last Quizes  : AAAAA<br/>
			    	Last Quizes Result :    AAAAA<br/>			    	
			  </div>
			</div>
			
			<div class="panel panel-success">
			  <div class="panel-heading">
			    	<h3 class="panel-title">Results</h3>
			  </div>
			  <div class="panel-body">
			  	<div class="table-responsive">
			    	<table class="table table-striped" >
			    		<thead>
			    			<tr class="info">
			    				<td>No</td>
			    				<td>Quizes Name</td>
			    				<td>Marks</td>
			    				<td>States</td>
			    			</tr>
			    		</thead>
			    		<tbody>
			    			<tr>
			    				<td>1</td>
			    				<td>Quizes Name</td>
			    				<td>Marks</td>
			    				<td>States</td>
			    			</tr>
			    			
			    			<tr>
			    				<td>2</td>
			    				<td>Quizes Name</td>
			    				<td>Marks</td>
			    				<td>States</td>
			    			</tr>
			    			
			    			<tr>
			    				<td>3</td>
			    				<td>Quizes Name</td>
			    				<td>Marks</td>
			    				<td>States</td>
			    			</tr>
			    			
			    		</tbody>
			    	</table>
			     </div>			    	
			  </div>
			</div>
			
			<div class="panel panel-warning">
			  <div class="panel-heading">
			    	<h3 class="panel-title">Assigned Turotials</h3>
			  </div>
			  <div class="panel-body">
			    	Coming Soon<br/>
			    	<br/><br/><br/>
			    	
			  </div>
			</div>
			
		</div>
		
	</div>
</div>


















<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>-->

















<?php $index='index.php/';
if($isNew==false){
	$a= $this->ion_auth->get_users_groups()->result(); 
        $array = array();
        foreach ($a as $userGroup){	
	    $array[] = $userGroup->id;
        }
        $groups=implode(",",$array);
}else{
		$groups='';
}
?>
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN BREADCRUMBS -->
        <div class="breadcrumbs">
            <h1><?php echo $this->lang->line('HELLO');?> <?php echo $this->ion_auth->user()->row()->first_name.' '.$this->ion_auth->user()->row()->last_name; ?></h1>
            <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li class="active">User Information</li>
            </ol>
            <!-- Sidebar Toggle Button -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".page-sidebar">
                <span class="sr-only">Toggle navigation</span>
                <span class="toggle-icon">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </span>
            </button>
            <!-- Sidebar Toggle Button -->
        </div>
        <!-- END BREADCRUMBS -->
        <!-- BEGIN SIDEBAR CONTENT LAYOUT -->
        <div class="page-content-container">
            <div class="page-content-row">
                <div class="page-content-col">
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption"><i class="fa fa-info"></i>User Information</div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"> </a>
                                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="tabbable-custom ">
                                        <ul class="nav nav-tabs ">
                                            <li class="active"><a href="#tab_5_1" data-toggle="tab">Personal Info </a></li>
                                            <li><a href="#tab_5_2" data-toggle="tab"> Change Password </a></li>
                                            <li><a href="#tab_5_3" data-toggle="tab"> Manage Emails </a></li>
                                            <li><a href="#tab_5_4" data-toggle="tab"> User Activities </a></li>	
                                        </ul>
                                        <div class="tab-content">
                                            <!-- START PERSONAL INFO DIV -->
                                            <div class="tab-pane active" id="tab_5_1">
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form action="<?php echo base_url() ?>index.php/compliance/editPortalUserModalBySeperatePageRun" class="form-horizontal form-row-seperated">
                                                        <input id="userID" type="hidden" value="<?php echo $user->id  ?>">
                                                        <input id="groups" type="hidden" value="<?php echo $groups; ?>">
                                                        <div class="form-body">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">First Name</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" id="first_name" placeholder="Enter First Name" value="<?php if($isNew==false){ echo $user->first_name; }else{ echo ''; } ?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Last Name</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" id="last_name" placeholder="Enter Last Name" value="<?php if($isNew==false){ echo $user->last_name; }else{ echo ''; } ?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Organisation</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" id="company" placeholder="Enter Organization" value="<?php if($isNew==false){ echo $user->company; } else { echo ''; } ?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Log In Email</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" id="email" placeholder="Enter Email" readonly="readonly" value="<?php if($isNew==false){ echo $user->email; } else { echo ''; } ?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Username</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" id="username" placeholder="Enter Username" readonly="readonly" value="<?php if($isNew==false){ echo $user->username; } else { echo ''; } ?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Phone</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" id="phone" placeholder="Enter phone #" value="<?php if($isNew==false){ echo $user->phone; } else { echo ''; }?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-offset-3 col-md-9">
                                                                    <button type="button" class="btn btn-primary" id="saveButton" onclick="saveHomeUser()" data-loading-text="Saving...">Save</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>
                                            <!-- END PERSONAL INFO DIV -->
                                            <!-- START CHANGE PASSWORD DIV -->
                                            <div class="tab-pane" id="tab_5_2">
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form action="#" class="form-horizontal form-row-seperated">
                                                        <div class="form-body">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Current Password</label>
                                                                <div class="col-md-9">
                                                                    <input type="password" class="form-control" id="currentPassword" placeholder="Enter current password" value="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">New Password</label>
                                                                <div class="col-md-9">
                                                                    <input type="password" class="form-control" id="password" placeholder="Enter new password" value="">
                                                                </div>
                                                            </div>  
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Confirm Password</label>
                                                                <div class="col-md-9">
                                                                    <input type="password" class="form-control" id="passconf" placeholder="Confirm new password" value="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-offset-3 col-md-9">
                                                                    <button type="button" class="btn btn-primary" id="saveButtonPassword" onclick="saveNewUserPassword()" data-loading-text="Saving...">Save</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>
                                            <!-- END CHANGE PASSWORD DIV -->
                                            <!-- START MANAGE MAILS DIV -->
                                            <div class="tab-pane" id="tab_5_3">
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form action="#" class="form-horizontal form-row-seperated">
                                                        <input id="userIDEmail" type="hidden" value="<?php echo $user->id  ?>">
                                                        <div class="form-body">
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label">Choose credential </label>
                                                                <div class="col-md-9">
                                                                    <div class="mt-checkbox-list">
                                                                        <label class="mt-checkbox mt-checkbox-outline">
                                                                            <input type="checkbox" id="ticket" value="1" <?php if($emailAuth->ticket == 1){ echo "checked"; } ?>> Tickets
                                                                            <span></span>
                                                                        </label>
                                                                        <label class="mt-checkbox mt-checkbox-outline">
                                                                            <input type="checkbox" id="medChange" value="1" <?php if($emailAuth->medChange == 1){ echo "checked"; } ?>> Med Changes
                                                                            <span></span>
                                                                        </label>
                                                                        <label class="mt-checkbox mt-checkbox-outline">
                                                                            <input type="checkbox" id="comment" value="1" <?php if($emailAuth->comment == 1){ echo "checked"; } ?>> Comments
                                                                            <span></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-offset-3 col-md-9">
                                                                    <button type="button" class="btn btn-primary" id="saveButtonPassword" onclick="saveEmailCredential()" data-loading-text="Saving...">Save</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>
                                            <!-- END MANAGE MAILS DIV -->
                                            <!-- START USER ACTIVITY DIV -->
                                            <div class="tab-pane" id="tab_5_4">
                                                <p>Last Login : AAAAA</p>
                                                <p>Last Quizes : AAAAA</p>
                                                <p>Last Quizes Result : AAAAA</p>
                                            </div>
                                            <!-- END USER ACTIVITY DIV -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                    <!-- START RESULTS LISTING-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-users"></i>
                                        <span class="caption-subject bold uppercase">Results</span>
                                    </div>
                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th> No </th>
                                                <th> Quizes Name </th>
                                                <th> Marks </th>
                                                <th> States </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php for($i=1;$i<=3;$i++){ ?>
                                            <tr>
                                                <td> <?php echo $i; ?> </td>
                                                <td> Quizes Name </td>
                                                <td> Marks </td>
                                                <td> States </td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    <!-- END RESULTS LISTING -->
                    <div class="row">
                        <div class="col-md-6">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-file-pdf-o"></i>
                                        <span class="caption-subject bold uppercase">Assigned Turotials</span>
                                    </div>
                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                                        <!--<thead>
                                            <tr>
                                                <th> Name </th>
                                                <th> Email </th>
                                                <th> Phone </th>
                                                <th> Last Login </th>
                                                <th> Actions </th>
                                            </tr>
                                        </thead>-->
                                        <tbody>Comming Soon
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                        <div class="col-md-6">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box red">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-users"></i>
                                        <span class="caption-subject bold uppercase">Assigned Quizes</span>
                                    </div>
                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover" id="sample_3">
                                        <!--<thead>
                                            <tr>
                                                <th> Name </th>
                                                <th> Email </th>
                                                <th> Phone </th>
                                                <th> Last Login </th>
                                                <th> Actions </th>
                                            </tr>
                                        </thead>-->
                                        <tbody>Comming Soon
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END SIDEBAR CONTENT LAYOUT -->
    </div>
