<?php

// SET UP -------------------------------------------------------

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetTitle('Cover Sheet');

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(50);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);


// PAGE CONTENT---------------------------------------------------------



$pdf->AddPage();

//Logo
$pdf->Image(base_url() . 'img/logo/logo.png', 15, 12, 90, 15, 'PNG', 'http://www.tcpdf.org', '', true, 150, '', false, false, 0, false, false, false);


// Barcode
$pdf->SetFont('helvetica', '', 10);
$style = array(
    'position' => '',
    'align' => 'C',
    'stretch' => false,
    'fitwidth' => true,
    'cellfitalign' => '',
    'border' => true,
    'hpadding' => 'auto',
    'vpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255),
    'text' => true,
    'font' => 'helvetica',
    'fontsize' => 8,
    'stretchtext' => 4
);
$pdf->write1DBarcode('12345678901', 'UPCA', 150, 5, '', 18, 0.4, $style, 'N');


//SHIP TO
$pdf->SetFont('times', 'B', 25);
$pdf->Write(20, 'Delivery Order', '', 0, 'C', true, 0, false, false, 0);

$pdf->SetFont('times', '', 20);
$pdf->Write(20, 'SHIP TO:', '', 0, 'L', true, 0, false, false, 0);
$pdf->Write(20, 'ADDRESS:', '', 0, 'L', true, 0, false, false, 0);

$style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0));
$pdf->Line(20, 10, 100, 20, $style);

// add a page
$pdf->AddPage();
$pdf->Image(base_url() . 'img/logo/logo.png', 15, 12, 90, 15, 'PNG', 'http://www.tcpdf.org', '', true, 150, '', false, false, 0, false, false, false);

$pdf->Output('My-File-Name.pdf', 'I');


//============================================================+
// END OF FILE
//============================================================+