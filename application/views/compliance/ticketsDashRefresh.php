<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->

<!--
<div class="clearfix">
	<h3 class="content-title pull-left">Tickets</h3>
</div>
<div class="description">
	
	<table>
		<tr>
			<td>
				<a class="btn btn-lg btn-success" href="<?php echo base_url(). 'compliance/ticketsDash';?>">
					<i class="fa fa-stop"></i> STOP
				</a>
			</td>
			
			<td>
				<?php 
					$monthRank = "";
					$weekRank = "";
					$dayRank = "";
					
					foreach ( $totalTicketRankMonth as $totalTicketRankList ) {						
						if(!empty($totalTicketRankList->monthRank)) { $monthRank =  $totalTicketRankList->monthRank / $totalTicketRankList->idTickets; } 
					}					
					
				
					foreach ( $totalTicketRankWeek as $totalTicketRankList ) {
						if(!empty($totalTicketRankList->weekRank)) { $weekRank = $totalTicketRankList->weekRank / $totalTicketRankList->idTickets; } 
					}					
					
				
					foreach ( $totalTicketRankDay as $totalTicketRankList ) {
						if(!empty($totalTicketRankList->dayRank)) { $dayRank =  $totalTicketRankList->dayRank / $totalTicketRankList->idTickets; }
					}
					
				 ?>
			<table>
				<tr>
					<td width="150px" align="right" valign="top">
						<strong>Today's Score:</strong>
					</td>
					<td>
						<p class="ratebox" data-id="2" data-rating="<?php echo $dayRank; ?>"></p>
					</td>
					
					<td width="150px" align="right" valign="top">
						<strong>This Week:</strong>
					</td>
					<td>
						<p class="ratebox" data-id="2" data-rating="<?php echo $weekRank; ?>"></p>
					</td>
					
					<td width="150px" align="right" valign="top">
						<strong>This Month:</strong>
					</td>
					<td>
						<p class="ratebox" data-id="2" data-rating="<?php echo $monthRank; ?>"></p>
					</td>
					
				</tr>
			</table>
			</td>	
		</tr>
	</table>
	
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!--
<div class="row">

	<div class="col-md-6">
		<table id="1" cellpadding="0" cellspacing="0" border="0"
			class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th><h3 class="center">
							<span class="label label-info">General Ticket</span>
						</h3></th>
				</tr>
			</thead>
			<tbody>
			<?php
			$generalTicket = $this->comp->getTickets ( '1', '1', '-1' );
			if ($generalTicket != FALSE) {
				foreach ( $generalTicket->result () as $generalTicketItem ) {
					?>
				<tr class="curses"
					onclick="">
					<td>
						<?php	$patient = $this->comp->getCompliancePatient ( $generalTicketItem->patID );	?>
						
						<table width="100%">
								<tr >
									
														 
									<td colspan="2" onclick="loadModal('<?php echo base_url('compliance/editTicketModal/') . '/' . $generalTicketItem->patID . '/' . $generalTicketItem->ticketID ;?>')" width="150px">
										
										<?php 
											 echo '<span class="btn btn-primary"> <i class="fa fa-home"></i>' . $patient->homeName .'
													<i class="fa fa-angle-double-right"></i>
													<i class="fa fa-user"></i>
												   ';
											 echo $patient->patInitials . '-' . $patient->patID .'</span>';
										 ?>															 		
									 </td>	
								</tr >
							    <tr >
									  <td colspan="3" style="text-align:left;">							 	
											<span class="btn btn-default"><i class="fa fa-user"></i>
												<?php echo $this->ion_auth->user ($generalTicketItem->createdBy)->row ()->first_name;?> 
											:: 
												
												<?php echo date('Y-m-d',strtotime($generalTicketItem->dateCreated));?>
											</span> 							 							 			
									  </td>						
								</tr>							
												
							</table>
						
					</td>
				</tr>
							<?php  }}?>
							
							
							<?php
							$spareTicket = $this->comp->getTickets ( '5', '1', '-1' );
							if ($spareTicket != FALSE) {
								foreach ( $spareTicket->result () as $spareTicketItem ) {
									?>
				<tr class="curses"
					onclick="">
					<td><?php
									$patient = $this->comp->getCompliancePatient ( $spareTicketItem->patID );
									
									?>
						<h6 class="pull-right"><?php echo  $spareTicketItem->dateCreated;?></h6>
						
						<h3><?php  echo '<span class="label label-info">'.$patient->patInitials . '-' . $patient->patID .'</span>' ;?> </h3><?php echo $this->ion_auth->user ($generalTicketItem->createdBy)->row ()->first_name;?>
						<div class="well well-sm"><?php echo "SPARES- " . $spareTicketItem->Description;?></div></td>
				</tr>
							<?php  }}{echo '<tr><td></td></tr>';} ?>
							
							</tbody>
		</table>
	</div>
	
	<div class="col-md-6">
		<table id="1" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th>
						<h3 class="center">
							<span class="label label-warning">Re-Order 						
						</h3>
					</th>
				</tr>
			</thead>
			<tbody>
			<?php
			$generalTicket = $this->comp->getTickets ( '2', '1', '-1' );
			if ($generalTicket != FALSE) {
				foreach ( $generalTicket->result () as $generalTicketItem ) {?>
				<tr class="curses" onclick="">
					<td>
						<?php	$patient = $this->comp->getCompliancePatient ( $generalTicketItem->patID );	?>
						
						<table width="75%">
								<tr >									
														 
									<td colspan="2" onclick="loadModal('<?php echo base_url('compliance/editTicketModal/') . '/' . $generalTicketItem->patID . '/' . $generalTicketItem->ticketID ;?>')" width="150px">
										
										<?php 
											 echo '<span class="btn btn-primary"> <i class="fa fa-home"></i>' . $patient->homeName .'
													<i class="fa fa-angle-double-right"></i>
													<i class="fa fa-user"></i>
												   ';
											 echo $patient->patInitials . '-' . $patient->patID .'</span>';
										 ?>															 		
									 </td>	
								</tr >
							    <tr >
									  <td colspan="3" style="text-align:left;">							 	
											<span class="btn btn-default"><i class="fa fa-user"></i>
												<?php echo $this->ion_auth->user ($generalTicketItem->createdBy)->row ()->first_name;?> 
											:: 
												
												<?php echo date('Y-m-d',strtotime($generalTicketItem->dateCreated));?>
											</span> 							 							 			
									  </td>						
								</tr>							
												
						</table>
						
					</td>
				</tr>
							<?php  }} ?>
			 </tbody>
		</table>
	</div>
	
	
</div>

<div class="row">
<div class="col-lg-12"><h3>Comments</h3></div>

</div>

<div class="row">
	<div class="col-lg-12">
		<table id="datatable2" cellpadding="0" cellspacing="0" border="0"
			class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th>User</th>
					<th>Message</th>
					<th>Date</th>
				</tr>
			</thead>
			<tbody>
			<?php			
			
			
				if ($notifications) {
					foreach ( $notifications as $notification ) {
						$time_ago = strtotime ( $notification->messageDate );
						if (isset ( $notification->ticketID )){
							echo '<tr>
								<td>'.$notification->first_name.'</td>
								<td>'.$notification->message.'</td>
			
								<td><i class="fa fa-clock-o"></i> <span>'; $this->quick->timeAgo ( $time_ago ); echo '</span></td>
							</tr>';
						}else {				
				   			echo '<tr><td>No date</td></tr>';
						}				
				   } 
				
			     }				
								
			 ?>
				
			
			</tbody>
		</table>
	</div>
</div>

<!---------      PAGE FOORER    ------------>
<!--<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>-->












<?php $ci = &get_instance();
$index='index.php/';	
?>

<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN BREADCRUMBS -->
        <div class="breadcrumbs">
            <h1>Tickets</h1>
            <a href="<?php echo base_url().''.$index; ?>compliance/ticketsDash" class="btn btn-icon-only red"><i class="fa fa-stop"></i></a>
            <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li class="active">Tickets</li>
            </ol>
        </div>
        <?php 
        $monthRank = "";
        $weekRank = "";
        $dayRank = "";
        foreach ( $totalTicketRankMonth as $totalTicketRankList ) {						
            if(!empty($totalTicketRankList->monthRank)) {
                $monthRank =  $totalTicketRankList->monthRank / $totalTicketRankList->idTickets;
            } 
        }					
        foreach ( $totalTicketRankWeek as $totalTicketRankList ) {
            if(!empty($totalTicketRankList->weekRank)) {
                $weekRank = $totalTicketRankList->weekRank / $totalTicketRankList->idTickets;
            } 
        }					
        foreach ( $totalTicketRankDay as $totalTicketRankList ) {
            if(!empty($totalTicketRankList->dayRank)) {
                $dayRank =  $totalTicketRankList->dayRank / $totalTicketRankList->idTickets;
            }
        }
        ?>
        <!-- END BREADCRUMBS -->
        <div class="rating_top">
	    <div class="score-common score">
                <h4>Today's Score:</h4>
                <p class="ratebox raterater-wrapper" data-id="2" data-rating="<?php echo $dayRank; ?>"></p>
 	    </div>
            <div class="score-common week">
                <h4>This Week:</h4>
                <p class="ratebox" data-id="2" data-rating="<?php echo $weekRank; ?>"></p>
	    </div>
            <div class="score-common month">
                <h4>This Month:</h4>
                <p class="ratebox" data-id="2" data-rating="<?php echo $monthRank; ?>"></p>
	    </div>
        </div>
        <!-- BEGIN SIDEBAR CONTENT LAYOUT -->
        <div class="page-content-container" style="overflow:visible;">
            <div class="page-content-row">
                <div class="page-content-col">
                    <!-- BEGIN : RIBBONS -->
                    <div class="row">
                        <div class="col-lg-6"><!-- First Div Start -->
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class=" fa fa-ticket font-red"></i>
                                        <span class="caption-subject font-red bold uppercase">General Ticket</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
                                        <?php $generalTicket = $this->comp->getTickets ( '1', '1', '-1' );
			                if ($generalTicket != FALSE) {
                                            foreach ( $generalTicket->result () as $generalTicketItem ) {
                                                $patient = $this->comp->getCompliancePatient ( $generalTicketItem->patID );
                                        ?>
                                        <div class="col-md-12">
                                            <div class="mt-element-ribbon bg-grey-steel">
                                                <?php if(!empty($patient)){ ?>
                                                <a href="#" onclick="loadModal('<?php echo base_url() ?><?php echo $index; ?>compliance/editTicketModal/<?php echo $generalTicketItem->patID; ?>/<?php echo $generalTicketItem->ticketID; ?>')">
                                                    <div class="ribbon ribbon-shadow ribbon-color-primary uppercase"><i class="fa fa-home"></i> <?php echo $patient->homeName; ?> <i class="fa fa-angle-double-right"></i> <i class="fa fa-user"></i> <?php echo $patient->patInitials . '-' . $patient->patID ?></div>
                                                </a>
                                                <?php } ?>
                                                <div class="ribbon ribbon-shadow ribbon-color uppercase"><i class="fa fa-user"></i> <?php echo $this->ion_auth->user ($generalTicketItem->createdBy)->row ()->first_name;?> :: <?php echo date('Y-m-d',strtotime($generalTicketItem->dateCreated));?></div>
                                                <p class="ribbon-content" style="padding:6px;"></p> 
                                            </div>
                                        </div>
                                        <?php }}else{ ?>
                                        <div class="col-md-12">
                                            No Record Found!
                                        </div>
                                        <?php } ?>                                                
                                    </div>
                                </div>
                            </div>
                        </div><!-- First Div End -->
                        <div class="col-lg-6"><!-- Second Div Start -->
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class=" fa fa-ticket font-red"></i>
                                        <span class="caption-subject font-red bold uppercase">Re-Order</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
                                        <?php $generalTicket = $this->comp->getTickets ( '2', '1', '-1' );
			                if ($generalTicket != FALSE) {
				            foreach ( $generalTicket->result () as $generalTicketItem ) {
                                                $patient = $this->comp->getCompliancePatient ( $generalTicketItem->patID );
                                        ?>
                                        <div class="col-md-12">
                                            <div class="mt-element-ribbon bg-grey-steel">
                                                <?php if(!empty($patient)){ ?>                                          
                                                    <div class="ribbon ribbon-shadow ribbon-color-primary uppercase" onclick="loadModal('<?php echo base_url() ?><?php echo $index; ?>compliance/editTicketModal/<?php echo $generalTicketItem->patID; ?>/<?php echo $generalTicketItem->ticketID; ?>')"><i class="fa fa-home"></i> <?php echo $patient->homeName; ?> <i class="fa fa-angle-double-right"></i> <i class="fa fa-user"></i> <?php echo $patient->patInitials . '-' . $patient->patID; ?></div>
                                                <?php } ?>
                                                <div class="ribbon ribbon-shadow ribbon-color uppercase" ><i class="fa fa-user"></i> <?php echo $this->ion_auth->user ($generalTicketItem->createdBy)->row ()->first_name;?> :: <?php echo date('Y-m-d',strtotime($generalTicketItem->dateCreated));?></div>
                                                <p class="ribbon-content" style="padding:6px;"></p>
                                            </div>
                                        </div>
                                        <?php }}else{ ?>
                                        <div class="col-md-12">
                                            No Record Found!
                                        </div>
                                        <?php } ?>                                                 
                                    </div>
                                </div>
                            </div>
                        </div><!-- Second Div End -->	 
                    </div>
                    <!-- END : RIBBONS -->
                    <!-- Comments List Div Start -->  
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="fa fa-comments font-dark"></i>
                                        <span class="caption-subject bold uppercase">Comments</span>
                                    </div>
                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                                        <thead>
                                            <tr>
                                                <th> User </th>
                                                <th> Message </th>
                                                <th> Date </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php 
                                        if ($notifications) {
                                            foreach ( $notifications as $notification ) {
                                                $time_ago = strtotime ( $notification->messageDate );
                                                if (isset ( $notification->ticketID )){
                                        ?>
                                        <tr>
                                            <td> <?php echo ucfirst($notification->first_name); ?> </td>
                                            <td> <?php echo $notification->message; ?> </td>
                                            <td> <i class="fa fa-clock-o"> <?php echo $this->quick->timeAgo ( $time_ago ); ?></i></td>
                                        </tr>
                                        <?php } ?> 
                                        <?php }} ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    <!-- End Comment Div --> 
                </div>
            </div>
        </div>
        <!-- END SIDEBAR CONTENT LAYOUT -->
    </div>
