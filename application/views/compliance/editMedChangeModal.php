<?php
    header("Cache-Control: no-cache, must-revalidate");
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
    $isNew = true;
    if ($this->uri->segment ( 4 ) > 0) {
        $isNew = false;
    }
?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">
        <?php if($isNew==true){
                  echo 'New Med Change';
              }else{
                  echo 'Med Change Detail'; // . ' ChangeID : '.$medChange->medChangeID;
        ?>
                  <button type="button" class="btn btn-danger" id="deleteButton" onclick="deleteMedChange()" data-loading-text="deleting...">Delete</button>';		
	          <a href="<?php echo base_url() ?>compliance/printMedChange/<?php echo $patient ->ID ?>/<?php echo $medChange->medChangeID; ?>" target="_blank"> <button id="printbutton" class="btn btn-info"><i class="fa fa-print"></i> Print</button></a>
        <?php } ?>
        <a href="<?php echo base_url() ?>compliance/quickPatView/<?php echo $patient ->ID; ?>/reorders"><span class="label label-primary"><?php echo ucfirst($patient -> LastName) .' '.ucfirst($patient->FirstName). '-' . $patient ->ID; ?></span></a>
    </h4>
</div>

<div class="modal-body">
    <div class="form-horizontal" role="form">
        <input id="medChangeID" type="hidden" value="<?php if($isNew==false){ echo $medChange->medChangeID; } else { }?>">
        <input id="patID" type="hidden" value="<?php if($isNew==false){ echo $medChange->patID; } else { echo $patient ->ID; } ?>">
        <input id="active" type="hidden" value="<?php if($isNew==false){ echo $medChange->active; } else { echo '1'; } ?>">
        <input id="actionComplete" type="hidden" value="<?php if($isNew==false){ echo $medChange->actionComplete; } else { echo '0'; } ?>">
        <?php if($isNew==false){ ?>
            <?php if($isNew==FALSE){ ?>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Date Recorded</label>
                    <div class="col-sm-9">
                        <input type="text" id="dateOfChange" class="form-control" disabled value="<?php echo $medChange->dateOfChange; ?>">
                    </div>
                </div>
            <?php } ?>		
            <input type="hidden" class="form-control" id="technicianUserID" placeholder="Text input" value="<?php $tech = $this->ion_auth->user()->row(); echo $tech->id;?>">
            <!--div class="form-group">
                <label class="col-sm-3 control-label">Created By</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="technicianUserName" placeholder="Text input" disabled value="<?php $tech = $this->ion_auth->user($medChange->technicianUserID)->row(); echo $tech->first_name;?>">
                </div>
            </div-->
            <?php if($medChange->pharmacistUserID > 0){?>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Sent By</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="pharmacistUserName" placeholder="Text input" disabled value="<?php $pharm = $this->ion_auth->user($medChange->pharmacistUserID)->row(); echo $pharm->first_name;?>">
                        <input type="hidden" class="form-control" id="pharmacistUserID" placeholder="Text input" value="<?php echo $pharm->id ; ?>">
                    </div>
                </div>
            <?php }else{?>
                <input type="hidden" class="form-control" id="pharmacistUserID" placeholder="Text input" value="0">
                <!--div class="form-group">
                    <label class="col-sm-3 control-label">Pharmacist Checked By</label>
                    <div class="col-sm-9">
                        <span class="label label-danger">Not Verified</span> 
                        <input type="hidden" class="form-control" id="pharmacistUserID" placeholder="Text input" value="0">
                    </div>
                </div-->
            <?php } ?>
        <?php } else { ?>
            <input id="technicianUserID" type="hidden" value="<?php $user = $this->ion_auth->user ()->row (); echo $user->id; ?>">
            <input id="pharmacistUserID" type="hidden" value="0"> 
            <input id="dateOfChange" type="hidden" value="<?php echo date('Y-m-d');?>"> 
        <?php } ?>
        <div class="form-group">
            <label class="col-sm-3 control-label">Change</label>
            <div class="col-sm-9">
                <textarea class="form-control" rows="3" id="changeText" <?php if(!empty($medChange)) { if(($medChange->pharmacistUserID > 0) && ($medChange->actionComplete == 2)){ echo "disabled "; }} ?> ><?php if($isNew==false){ echo $medChange->changeText; } ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Action</label>
            <div class="col-sm-9">
                <textarea class="form-control" id="actionText" rows="3" <?php if(!empty($medChange)) { if(($medChange->pharmacistUserID > 0) && ($medChange->actionComplete == 2)){ echo "disabled "; }} ?> ><?php if($isNew==false){ echo $medChange->actionText; } ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Starts on</label>
            <div class="col-sm-6">
                <input type="date" class="form-control" id="startDate" placeholder="Text input" <?php if(!empty($medChange)) { if(($medChange->pharmacistUserID > 0) && ($medChange->actionComplete == 2)){ echo "disabled "; }} ?>  value="<?php if($isNew==false){ echo $medChange->startDate;} else { echo date('Y-m-d'); } ?>">
            </div>
        </div>
        <?php 
        /* if($isNew==FALSE){
            if (($medChange->pharmacistUserID > 0) && ($medChange->actionComplete < 1)) {
                echo '<div class="form-group">
                    <label class="col-sm-3 control-label"> Spares </label>
                    <div class="col-sm-3">
                        <div class="radio"> 
                            <label>
                                <input type="radio" name="spares" value="1"'; if($medChange->spares == 1){ echo ' checked '; } echo'> Yes
                            </label>
                            <label>
                                <input type="radio" name="spares" value="0"'; if($medChange->spares == 0){ echo ' checked '; } echo'> No
                            </label>
                        </div>
                    </div>
                </div>';
                echo '<div class="form-group">
                    <label class="col-sm-3 control-label"> Mar </label>
                    <div class="col-sm-3">
                        <div class="radio"> 
                            <label>
                                <input type="radio" name="marval" value="1"'; if($medChange->mar == 1){ echo ' checked '; } echo'> Yes
                            </label> 
                            <label>
                                <input type="radio" name="marval" value="0"'; if($medChange->mar == 0){ echo ' checked '; } echo'> No
                            </label>
                        </div>
                    </div>
                </div>';
                echo '<div class="form-group">
                    <label class="col-sm-3 control-label"> Extra Labels </label>
                    <div class="col-sm-3">
                        <div class="radio"> 
                            <label>
                                <input type="radio" name="eLabels" value="1"'; if($medChange->extraLabels == 1){ echo 'checked'; } echo'> Yes
                            </label> 
                            <label>
                                <input type="radio" name="eLabels" value="0"'; if($medChange->extraLabels == 0){ echo ' checked '; } echo'> No
                            </label>
                        </div>
                    </div>
                </div>';
                echo ' <div class="form-group">
                    <label class="col-sm-3 control-label"> # of Strips  </label>
                    <div class="col-sm-3">
                        <select class="form-control" id="numOfStrips">
                            <option '; if($medChange->numOfStripts == 0){ echo ' SELECTED '; } echo'>0</option>
                            <option '; if($medChange->numOfStripts == 1){ echo ' SELECTED '; } echo'>1</option>
                            <option '; if($medChange->numOfStripts == 2){ echo ' SELECTED '; } echo'>2</option>
                            <option '; if($medChange->numOfStripts == 3){ echo ' SELECTED '; } echo'>3</option>
                            <option '; if($medChange->numOfStripts == 4){ echo ' SELECTED '; } echo'>4</option>
                            <option '; if($medChange->numOfStripts == 5){ echo ' SELECTED '; } echo'>5</option>
                        </select>
                    </div>
                </div>';
            }
        }*/		
	?>
        <div class="form-group">
            <label class="col-sm-3 control-label">Delivery on</label>
            <div class="col-sm-6">
                <input type="date" class="form-control" id="endDate" placeholder="Text input" value="<?php if($isNew==false){ echo $medChange->endDate;} else { echo date('Y-m-d'); }   ?>">
            </div>
        </div>
	<?php 
        if($isNew==FALSE){ 
            if (($medChange->pharmacistUserID > 0) && ($medChange->actionComplete < 1)) {
                /*echo ' <div class="form-group">
                    <label class="col-sm-3 control-label"> # of Next Regular Delivery  </label>
                    <div class="col-sm-3">
                        <select class="form-control" id="rdd">
                            <option '; if($medChange->nextRDD == 0){ echo ' SELECTED '; } echo'>0</option>
                            <option '; if($medChange->nextRDD == 1){ echo ' SELECTED '; } echo'>1</option>
                            <option '; if($medChange->nextRDD == 2){ echo ' SELECTED '; } echo'>2</option>
                            <option '; if($medChange->nextRDD == 3){ echo ' SELECTED '; } echo'>3</option>
                            <option '; if($medChange->nextRDD == 4){ echo ' SELECTED '; } echo'>4</option>
                            <option '; if($medChange->nextRDD == 5){ echo ' SELECTED '; } echo'>5</option>
                        </select>
                    </div>
                </div>'; */	  
                echo '<div class="form-group">
                    <label class="col-sm-3 control-label"> Send to Eboard : </label>
                    <div class="col-sm-6">
                        <div class="radio"> 
                            <label>
                                <input type="radio" name="batchNum" value="1"'; if($medChange->batchNum == 1){ echo ' checked '; } echo'> No
                            </label> 
                            <label>
                                <input type="radio" name="batchNum" value="0"'; if($medChange->batchNum == 0){ echo ' checked '; } echo'> Yes
                            </label>
                        </div>
                    </div>
                </div>';
            }
        }
        ?>
    </div>
    <div class="modal-footer">
        <?php
        if ($isNew == false) {
            if ($medChange->pharmacistUserID == 0) { ?>
                <button type="button" class="btn btn-warning pull-left" onclick="pharmacistCheckButtonModal()" id="pharmacistCheckButtonModal" 	data-loading-text="Pharmacist Checking...">Pharmacist Checked</button>
            <?php } elseif (($medChange->pharmacistUserID > 0) && ($medChange->actionComplete < 1)) { 
                echo '<button type="button" class="btn btn-warning pull-left" onclick="actionCompleted()" id="actionComplete" data-loading-text="Action completing...">Action Complete</button>';
            } else{ }
        }
        ?>
        <button type="button" class="btn btn-default" data-dismiss="modal" id="closeButtonModal">Close</button>
        <button type="button" class="btn btn-primary" id="saveButton" onclick="saveMedChange()"  data-loading-text="Saving...">Save</button>
    </div>
</div>
