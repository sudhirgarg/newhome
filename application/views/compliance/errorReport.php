


<!--
<div class="clearfix">
	<h3 class="content-title pull-left">Error summary </h3>
</div>

<div class="description">
	<a href="<?php echo base_url("compliance/Error/") ;?>">
		<button class="btn btn-primary btn-lg" type="button">
			<i class="fa fa-angle-double-left"></i> Back To Chart
	    </button>
   </a>
	
		
</div>
</div>
</div>
</div>



<div class="row" ng-app="myApp" ng-controller="DoubleController" >

	<div class="col-lg-6">
		<div class="box border blue">
			<div class="box-title">
				<h4>
					<i class="fa fa-exclamation-triangle"></i>List of error  <button class="btn btn-danger" ng-click="addError()"> <i class="fa fa-plus"></i> ADD </button>
				</h4>
			</div>
			<div class="box-body">
			<div class="table-responsive">	
				<table id="example" class="table table-striped table-bordered table-hover" >
                <form class="form-inline">
                    <div class ="row">

                        <div class="col-md-3">
                            <div class="form-group" style="width:175px;">
                                <input type="text" ng-model="search" class="form-control" placeholder="Search">
                            </div>
                        </div>

                        <div class="col-md-1">				
                            <div class="form-group" style="width:65px;"> 				
                                <input type="number" min="1" max="100" class="form-control" ng-model="pageSize"> 
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group" style="width:75px;"> 
                                <button class="form-control btn btn-info" >
                                #R's:{{totalNumberOferrorDetails}}
                                </button>
                            </div>			
                        </div> 

                    </div>
                </form>


                    <thead>
                        <tr>
                            <th style="width:50px;">View</th>			
                            <th style="width:75px;" align="center">#</th>
                             <th style="width:135px;" ng-click="sort('patname')">Name
                                    <span class="glyphicon sort-icon" ng-show="sortKey=='patname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                            </th>
                            <th style="width:100px;" ng-click="sort('depName')">Department
                                    <span class="glyphicon sort-icon" ng-show="sortKey=='depName'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                            </th>
                            <th ng-click="sort('sourceOferror')">Source of error
                                    <span class="glyphicon sort-icon" ng-show="sortKey=='sourceOferror'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                            </th>
                            
                            
                        </tr>
                    </thead>
                    <tbody>
                        <tr dir-paginate="errorDetail in errorDetails | filter:search | orderBy:sortKey:reverse | itemsPerPage:pageSize"  >
                            <td> 
                                <button class="btn" ng-click="updateError ( errorDetail.id )" name="" type="button">
                                    <span class="glyphicon glyphicon-eye-open"></span> 
                                </button>                                   
                            </td>
                            
                            <td align="center">{{ $index + 1 }}   </td>
                            <td>{{ errorDetail.patname }}         </td>
                            <td>{{ errorDetail.depName }}     </td>
                            <td>{{ errorDetail.sourceOferror }}  </td>
                            
                        </tr>
                    </tbody>
                </table>
                        
                <dir-pagination-controls
                    max-size="10"
                    direction-links="true"
                    boundary-links="true" >
                </dir-pagination-controls> 
                
		    </div>		
			</div>
		</div>
		<br/><br/>
	</div>
	
	<div class="col-lg-6" ng-hide="addErrorShow">
			
			<div class="box border blue">
				<div class="box-title">	
					<h4 class="modal-title"> Add error details </h4>			
				</div>
			
			<div class="box-body">
			
				<div class="form-horizontal" role="form"> 
					
					<input id="errorId" ng-model="errorId" type="hidden" value="">
			
					<div class="form-group">
						<label class="col-sm-3 control-label"> Date </label>
						<div class="col-sm-9">
							<input type="date" class="form-control" ng-model="addDate"  value="<?php echo date('Y-m-d');?>">
						</div>
					</div>
			
					<div class="form-group">
						<label class="col-sm-3 control-label"> Description </label>
						<div class="col-sm-9">				
							<textarea rows="5" cols="10" class="form-control" id="description" ng-model="description"> </textarea>
						</div>
					</div>
			
					<div class="form-group">
						<label class="col-sm-3 control-label"> Resolution </label>
						<div class="col-sm-9">
							<textarea rows="5" cols="10" class="form-control" id="resolution" ng-model="resolution" > </textarea>
						</div>
					</div>
			
					<div class="form-group">
						<label class="col-sm-3 control-label"> Source of error </label>
						<div class="col-sm-9">
							<textarea rows="5" cols="10" class="form-control" id="sourceOferror" ng-model="sourceOferror"> </textarea>
						</div>
					</div>
					
					         
			                    
					<div class="form-group">
						<label class="col-sm-3 control-label"> Department </label>
						<div class="col-sm-9">
						
							<?php
							    // $depId = (!empty($errorValueInfo))? $errorValueInfo->departmentId : 0;
							     
								 echo'<select data-placeholder="Choose department..." name="department" ng-model="department" class="chosen-select" style="width:250px;" tabindex="2">
			                        <option value=""></option>';
			                        foreach($department AS $dRecord){
			                        	
			                            echo '<option value="'.$dRecord->id.'" {{department}} ';
			                            // if($dRecord->id == $depId){ echo ' SELECTED '; }
			                            echo '>'.$dRecord->depName.'</option>';
			                        }     
			                    echo'</select>';
							?>
							
						</div>
					</div>
			
					<div class="form-group">
						<label class="col-sm-3 control-label"> Type of error </label>
						<div class="col-sm-9">
																			
							<?php
							    //$typeOfErrorId = (!empty($errorValueInfo))? $errorValueInfo->typeOferrorId : 0;
								 echo'<select data-placeholder="Choose a type of error" ng-model="typeOferror" class="chosen-select form-control" style="width:250px;"  tabindex="2">
			                        <option value=""></option>';
			                        foreach($getAllTypeOfError AS $eRecord){
			                            echo '<option value="'.$eRecord->id.'">'.$eRecord->name.'</option>';
			                        }     
			                    echo'</select>';
							?>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-3 control-label"> Patient </label>
						<div class="col-sm-9">
							<?php
							   // $patId = (!empty($errorValueInfo))? $errorValueInfo->patId : 0;
								 echo'<select data-placeholder="Choose Patient..." name="patId" ng-model="patId" class="chosen-select" style="width:250px;" tabindex="2">
			                        <option value=""></option>';
			                        foreach($patInfo AS $comboRecord){
			                            echo '<option value="'.$comboRecord->ID.'">'.$comboRecord->LastName.' '.$comboRecord->FirstName.'</option>';
			                        }     
			                    echo'</select>';
							?>
							
						</div>
					</div>
					
				</div>
			</div>
			
			<div class="modal-footer">
						
				<button type="button" class="btn btn-primary" id="saveButton" ng-click="saveError()" data-loading-text="Saving...">Save</button>
			</div>

	</div>
	</div>
	
	<div class="col-lg-6" ng-hide="editResolution">
			
			<div class="box border blue">
				<div class="box-title">	
					<h4 class="modal-title"> Edit error details </h4>			
				</div>
			
			<div class="box-body">
			
				<div class="form-horizontal" role="form"> 
					
					<input type="hidden" id="errorId" ng-model="eId" value="">
			        
			        <div class="form-group">
						<label class="col-sm-3 control-label"> Patient </label>
						<div class="col-sm-9">
							{{patNameEdit}}
							
						</div>
					</div>         
			                    
					<div class="form-group">
						<label class="col-sm-3 control-label"> Department </label>
						<div class="col-sm-9">
						    {{departmentEdit}}
						
						</div>
					</div>
			
					<div class="form-group">
						<label class="col-sm-3 control-label"> Type of error </label>
						<div class="col-sm-9">
							{{typeOfErrorEdit}}												
							
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-3 control-label"> Date </label>
						<div class="col-sm-9">
							{{addDate1}}
						</div>
					</div>
			
					<div class="form-group">
						<label class="col-sm-3 control-label"> Description </label>
						<div class="col-sm-9">				
							{{descriptionEdit}}
						</div>
					</div>
			
					<div class="form-group">
						<label class="col-sm-3 control-label"> Resolution </label>
						<div class="col-sm-9">
							<textarea rows="5" cols="10" class="form-control" id="resolutionEdit" ng-model="resolutionEdit" > </textarea>
						</div>
					</div>
			
					<div class="form-group">
						<label class="col-sm-3 control-label"> Source of error </label>
						<div class="col-sm-9">
							{{sourceOferrorEdit}}
						</div>
					</div>
					
				</div>
			</div>
			
			<div class="modal-footer">
						
				<button type="button" class="btn btn-primary"  ng-click="editError()" data-loading-text="Saving...">Save</button>
			</div>

	</div>
	</div>
	
	
	
</div>




<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>-->





<?php $ci = &get_instance();
$index='index.php/';	
?>
<div class="container-fluid">
                <div class="page-content">
                    <!-- BEGIN BREADCRUMBS -->
                    <div class="breadcrumbs">
                        <h1>Error summary</h1>
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo base_url(); ?><?php echo $index; ?>compliance/ticketsdash">Home</a>
                            </li>
			    <li>
                                <a href="<?php echo base_url($index.'compliance/Error/') ;?>">Back to Errors</a>
                            </li>	
                            <li class="active">Error summary</li>
                        </ol>
                        <!-- Sidebar Toggle Button -->
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".page-sidebar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="toggle-icon">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </span>
                        </button>
                        <!-- Sidebar Toggle Button -->
                    </div>
                    <!-- END BREADCRUMBS -->
                    <!-- BEGIN SIDEBAR CONTENT LAYOUT -->
                    <div class="page-content-container" ng-app="myApp" ng-controller="DoubleController">
                        <div class="page-content-row">
                            
                            <div class="page-content-col">
                                <!-- BEGIN PAGE BASE CONTENT -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="tabbable-line boxless tabbable-reversed">
                                            <div class="tab-content">
                                                <div class="tab-pane active" id="tab_0">
                                                    <!-- Add Error Detail Form Start -->
                                                    <div class="portlet box blue-hoki" ng-hide="addErrorShow">
                                                        <div class="portlet-title">
                                                            <div class="caption">
                                                                <i class="fa fa-home"></i>Add Error Detail </div>
                                                            
                                                        </div>
                                                        <div class="portlet-body form">
                                                            <!-- BEGIN FORM-->
                                                            <!-- BEGIN FORM-->
                                                            <div class="form-horizontal" role="form">
                                                                <input id="errorId" ng-model="errorId" type="hidden" value="">
                                                                <div class="form-body">
                                                                    <div class="form-group">
                                                                        <label class="col-md-4 control-label">Date</label>
                                                                        <div class="col-md-8">
                                                                            <input type="date" class="form-control" ng-model="addDate"  value="<?php echo date('Y-m-d');?>">
                                                                        </div>
                                                                    </div>
								    <div class="form-group">
                                                                        <label class="col-md-4 control-label">Description</label>
                                                                        <div class="col-md-8">
                                                                            <textarea rows="5" cols="10" class="form-control" id="description" ng-model="description"> </textarea>
                                                                        </div>
                                                                    </div>	
								    <div class="form-group">
                                                                        <label class="col-md-4 control-label">Resolution</label>
                                                                        <div class="col-md-8">
                                                                            <textarea rows="5" cols="10" class="form-control" id="resolution" ng-model="resolution" > </textarea>
                                                                        </div>
                                                                    </div>	
								    <div class="form-group">
                                                                        <label class="col-md-4 control-label">Source of Error</label>
                                                                        <div class="col-md-8">
                                                                            <textarea rows="5" cols="10" class="form-control" id="sourceOferror" ng-model="sourceOferror"> </textarea>
                                                                        </div>
                                                                    </div>
								    <div class="form-group">
                                                                        <label class="col-md-4 control-label">Department</label>
                                                                        <div class="col-md-8">
                                                                            <select name="department" ng-model="department" class="form-control select2">
										<option value="">Choose Department...</option>
										<?php 
							   	    		foreach($department as $dRecord){
										echo '<option value="'.$dRecord->id.'" {{department}} ';
			                            				// if($dRecord->id == $depId){ echo ' SELECTED '; }
			                            				echo '>'.$dRecord->depName.'</option>';
										} ?>
									    </select>
                                                                        </div>
                                                                    </div>
								    <div class="form-group">
                                                                        <label class="col-md-4 control-label">Type of Error</label>
                                                                        <div class="col-md-8">
                                                                            <select name="typeOferror" ng-model="typeOferror" class="form-control select2">
										<option value="">Choose Type of Error...</option>
										<?php 
							   	    		foreach($getAllTypeOfError AS $eRecord){
										echo '<option value="'.$eRecord->id.'">'.$eRecord->name.'</option>';
										} ?>
									    </select>
                                                                        </div>
                                                                    </div>
								    <div class="form-group">
                                                                        <label class="col-md-4 control-label">Patient</label>
                                                                        <div class="col-md-8">
                                                                            <select name="patId" ng-model="patId" class="form-control select2">
										<option value="">Choose Patient...</option>
										<?php 
							   	    		foreach($patInfo AS $comboRecord){
										echo '<option value="'.$comboRecord->ID.'">'.$comboRecord->LastName.' '.$comboRecord->FirstName.'</option>';
										} ?>
									    </select>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    
                                                                 
                                                                </div>
                                                                <div class="form-actions">
                                                                    <div class="row">
                                                                        <div class="col-md-offset-4 col-md-9">
                                                                            <button type="button" id="saveButton" ng-click="saveError()" data-loading-text="Saving..." class="btn green">Save</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- END FORM-->
                                                        </div>
                                                    </div>
						    <!-- Add Error Detail Form End -->	
						    <!-- Edit Error Detail Form Start -->
						    <div class="portlet box blue-hoki" ng-hide="editResolution">
                                                        <div class="portlet-title">
                                                            <div class="caption">
                                                                <i class="fa fa-home"></i>Edit Error Detail </div>
                                                            
                                                        </div>
                                                        <div class="portlet-body form">
                                                            <!-- BEGIN FORM-->
                                                            <!-- BEGIN FORM-->
                                                            <div class="form-horizontal" role="form">
								<input type="hidden" id="errorId" ng-model="eId" value="">
                                                                <div class="form-body">
                                                                    <div class="form-group">
                                                                        <label class="col-md-4 control-label">Patient</label>
                                                                        <div class="col-md-8">
                                                                            {{patNameEdit}}
                                                                        </div>
                                                                    </div>
								    <div class="form-group">
                                                                        <label class="col-md-4 control-label">Department</label>
                                                                        <div class="col-md-8">
                                                                            {{departmentEdit}}
                                                                        </div>
                                                                    </div>	
								    <div class="form-group">
                                                                        <label class="col-md-4 control-label">Type of Error</label>
                                                                        <div class="col-md-8">
                                                                            {{typeOfErrorEdit}}
                                                                        </div>
                                                                    </div>	
								    <div class="form-group">
                                                                        <label class="col-md-4 control-label">Date</label>
                                                                        <div class="col-md-8">
                                                                            {{addDate1}}
                                                                        </div>
                                                                    </div>
								    <div class="form-group">
                                                                        <label class="col-md-4 control-label">Description</label>
                                                                        <div class="col-md-8">
                                                                            {{descriptionEdit}}
                                                                        </div>
                                                                    </div>
								    <div class="form-group">
                                                                        <label class="col-md-4 control-label">Resolution</label>
                                                                        <div class="col-md-8">
                                                                            <textarea rows="5" cols="10" class="form-control" id="resolutionEdit" ng-model="resolutionEdit" > </textarea>
                                                                        </div>
                                                                    </div>
								    <div class="form-group">
                                                                        <label class="col-md-4 control-label">Source of Error</label>
                                                                        <div class="col-md-8">
                                                                            {{sourceOferrorEdit}}
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    
                                                                 
                                                                </div>
                                                                <div class="form-actions">
                                                                    <div class="row">
                                                                        <div class="col-md-offset-4 col-md-9">
                                                                            <button type="button"  ng-click="editError()" data-loading-text="Saving..." class="btn green">Update</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- END FORM-->
                                                        </div>
                                                    </div>
						    <!-- Edit Error Detail Form End -->
                                		</div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Information Div Start -->   
                                    <div class="col-md-6">
                                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                        <div class="portlet light bordered">
                                            <div class="portlet-title">
                                                <div class="caption font-dark">
                                                    <i class="fa fa-info-circle font-dark"></i>
                                                    <span class="caption-subject bold uppercase">Information</span>
                                                </div>
                                                <div class="tools"> </div>
                                            </div>
                                            <div class="portlet-body">
                                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                            </div>
                                        </div>
                                        <!-- END EXAMPLE TABLE PORTLET-->
                                    </div>
                                    <!-- Information Div End -->
                                </div>
                                <!-- END PAGE BASE CONTENT -->
                            </div>
                        </div>
                        
                        <!-- Error Listing Div Start -->
			<div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            <i class="fa fa-paperclip font-dark"></i>
                                            <span class="caption-subject bold uppercase">Error summary</span>
                                        </div>
                                        <div class="tools"> <div class="dt-buttons"><a class="dt-button buttons-print btn dark btn-outline" ng-click="addError()" href="#"><span>Add Error Detail</span></a></div></div>
                                    </div>
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover">
					    <form class="form-inline">
                    			        <div class ="row">
						    <div class="col-md-3" style="float:right;">
                            			        <div class="form-group">
                                			    <input type="text" ng-model="search" class="form-control col-md-8" placeholder="Search">
                            				</div>
                        			    </div>
						    <div class="col-md-1">				
                            			        <div class="form-group"> 				
                                			    <input type="number" min="1" max="100" class="form-control" ng-model="pageSize"> 
                            				</div>
                        			    </div>
						    <!--<div class="col-md-2">
							<div class="form-group" style="width:75px;"> 
							    <button class="form-control btn btn-info" >
								#R's:{{totalNumberOferrorDetails}}
							    </button>
							</div>			
						    </div>--> 
                  			        </div>
               			 	    </form>	
                                            <thead>
                                                <tr>
                                                    <th ng-click="sort('patname')"> Name<span class="glyphicon sort-icon" ng-show="sortKey=='patname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span> </th>
                                                    <th ng-click="sort('depName')"> Department<span class="glyphicon sort-icon" ng-show="sortKey=='patname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span> </th>
                                                    <th ng-click="sort('sourceOferror')"> Source of Error<span class="glyphicon sort-icon" ng-show="sortKey=='patname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span> </th>
                                                    <th> View </th>
                                                </tr>
                                            </thead>
                                            <tbody>
					        <tr dir-paginate="errorDetail in errorDetails | filter:search | orderBy:sortKey:reverse | itemsPerPage:pageSize">
                                                    <td> {{ errorDetail.patname }} </td>
                                                    <td> {{ errorDetail.depName }} </td>
                                                    <td> {{ errorDetail.sourceOferror }} </td>
                                                    <td><button class="btn" ng-click="updateError ( errorDetail.id )" name="" type="button"><span class="glyphicon glyphicon-eye-open"></span></button></td>
                                                </tr>
                                            </tbody>
                                        </table>
					<dir-pagination-controls
                    			max-size="10"
                    			direction-links="true"
                    			boundary-links="true" >
                			</dir-pagination-controls> 
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>
		        <!-- Error Listing Div End -->
                    </div>
                    <!-- END SIDEBAR CONTENT LAYOUT -->
                </div>

