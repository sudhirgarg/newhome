<!-- STYLER -->
<?php 
	$index='index.php/';	
	$oneYearBack = date('Y-m-d',strtotime(date("Y-m-d", time()) . " - 365 day"));
?>
<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<!--<div class="clearfix">
	<h3 class="content-title pull-left">Users</h3>
</div>

<div class="description">
	<button class="btn btn-primary btn-lg" type="button"
		onclick="loadModal('<?php echo base_url($index.'compliance/editPortalUserModal/18') ;?>')">
		<i class="fa fa-pencil"></i> New Home Portal User
</button>


	<button class="btn btn-primary btn-lg" type="button"
		onclick="loadModal('<?php echo base_url($index.'compliance/editPortalUserModal/17/') ;?>')">
		<i class="fa fa-pencil"></i> New Pharmacy  Portal User
</button>

	<?php 
		if(!empty($delete)) {
			
			echo '<div class="alert alert-success" role="alert">Well done! You record has been deleted .</div>';
		}
	
	?>
		
</div>
</div>
</div>
</div>


<div class="row">
	<div class="col-lg-8">
		<div class="box border blue">
			<div class="box-title">
				<h4>
					<i class="fa fa-users"></i>Home Portal Super Users
				</h4>
			</div>
			<div class="box-body">
				<table id="datatable2" cellpadding="0" cellspacing="0" border="0" class="datatable table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>Name</th>
							<th>Email</th>
							<th>Phone</th>
							<th>Last Login</th>
							<th>Edit/Remove Access</th>
						</tr>
					</thead>
					<tbody>
					<?php
									
							if ($portalUsers != false) { // Often times I'll return false in a model if no results are found
									foreach ( $portalUsers as $portalUser ) {
										
										 //if($portalUser->NHID > 0){
										 	echo '<tr>
													<td >
														<h4>'.$portalUser->first_name . ' ' . $portalUser->last_name.'</h4>
													</td>
													<td>'.$portalUser->email.'</td>
													<td>'.$portalUser->phone.'</td>
													<td>'.unix_to_human($portalUser->last_login).'</td>
													<td >														
														<a href="'.base_url().''.$index.'compliance/editPortalUserModalBySeperatePage/18/'.$portalUser->id.'">
															<button class="btn btn-info" type="button"><i class="fa fa-pencil-square-o"></i> Edit</button> 
													    </a>	
													    <a href="'.base_url().''.$index.'compliance/userStatus/'.$portalUser->id.'">													
														    <button class="btn btn-'; echo $a = ($portalUser->active == 0 ) ? 'danger' : 'success'; echo'" type="button" >Status </button>	
														</a>													
													</td>
												</tr>';
											
										// }					
			                       }
							 } else { 
								echo'<tr>
									<td>No Records</td>
								</tr>'; 
						     }				     
				     ?>
				     
					</tbody>
				</table>
			</div>
		</div>
		<br/><br/>
	</div>
</div>


<div class="row">
	<div class="col-lg-8">
		<div class="box border blue">
			<div class="box-title">
				<h4>
					<i class="fa fa-users"></i>Home Portal Users
				</h4>
			</div>
			<div class="box-body">
				<table id="datatable1" cellpadding="0" cellspacing="0" border="0" class="datatable table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>Name</th>
							<th>Email</th>
							<th>Phone</th>
							<th>Last Login</th>
							<th>Edit/Remove Access</th>
						</tr>
					</thead>
					<tbody>
					<?php
									
							if ($portalUsers != false) { // Often times I'll return false in a model if no results are found
									foreach ( $portalUsers as $portalUser ) {
										
										 //if((($portalUser->NHID == 0) || empty($portalUser->NHID)) && (unix_to_human($portalUser->last_login) > $oneYearBack )){
										if(unix_to_human($portalUser->last_login) > $oneYearBack ){
										 	echo '<tr>
													<td >
														<h4>'.$portalUser->first_name . ' ' . $portalUser->last_name.'</h4>
													</td>
													<td>'.$portalUser->email.'</td>
													<td>'.$portalUser->phone.'</td>
													<td>'.unix_to_human($portalUser->last_login).'</td>
													<td >
													
														<a href="'.base_url().''.$index.'compliance/editPortalUserModalBySeperatePage/18/'.$portalUser->id.'">
															<button class="btn btn-info" type="button"><i class="fa fa-pencil-square-o"></i> Edit</button> 
													    </a>														
														<a href="'.base_url().''.$index.'compliance/userStatus/'.$portalUser->id.'">													
														    <button class="btn btn-'; echo $a = ($portalUser->active == 0 ) ? 'danger' : 'success'; echo'" type="button" >Status </button>	
														</a>												
													</td>
												</tr>';
											
										 }					
			                       }
							 } else { 
								echo'<tr>
									<td>No Records</td>
								</tr>'; 
						     }				     
				     ?>
				     
					</tbody>
				</table>
			</div>
		</div>
		<br/><br/>
	</div>
</div>

<div class="row">
	<div class="col-lg-8">
		<div class="box border blue">
			<div class="box-title">
				<h4>
					<i class="fa fa-users"></i>Pharmacy Tech
				</h4>
			</div>
			<div class="box-body">
				<table id="datatable3" cellpadding="0" cellspacing="0" border="0" class="datatable table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>Name</th>
							<th>Email</th>
							<th>Phone</th>
							<th>Last Login</th>
							<th>Edit/Remove Access</th>
						</tr>
					</thead>
					<tbody>
					<?php
					
						if ($techUsers != false) { // Often times I'll return false in a model if no results are found
							foreach ( $techUsers as $portalUser ) {
								
							echo '<tr>
								<td  >
									<h4>'.$portalUser->first_name . ' ' . $portalUser->last_name.'</h4>
								</td>
								<td>'.$portalUser->email.'</td>
								<td>'.$portalUser->phone.'</td>
								<td>'.unix_to_human($portalUser->last_login).'</td>
								<td>
									<a href="'.base_url().''.$index.'compliance/editPortalUserModalBySeperatePage/17/'.$portalUser->id.'">
										<button class="btn btn-info" type="button"><i class="fa fa-pencil-square-o"></i> Edit</button> 
								    </a>														
									<a href="'.base_url().''.$index.'compliance/userStatus/'.$portalUser->id.'">													
									    <button class="btn btn-'; echo $a = ($portalUser->active == 0 ) ? 'danger' : 'success'; echo'" type="button" >Status </button>	
									</a>								
									
								</td>
							</tr>';
						 }} else { 
						 	echo'<tr>
							
								<td>No Records</td>
							</tr>';
							
						 }
					 ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>


<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>-->


<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN BREADCRUMBS -->
        <div class="breadcrumbs">
            <h1>Users</h1>
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?><?php echo $index; ?>compliance/ticketsdash">Home</a></li>
                <li class="active">Users</li>
            </ol>
            <!-- Sidebar Toggle Button -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".page-sidebar">
                <span class="sr-only">Toggle navigation</span>
                <span class="toggle-icon">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </span>
            </button>
            <!-- Sidebar Toggle Button -->
        </div>
        <!-- END BREADCRUMBS -->
        <!-- BEGIN BUTTONS -->
        <div class="portlet-body util-btn-margin-bottom-5 button_div">
            <div class="btn-group btn-group-solid custom-button">
                <p>
                    <button type="button" class="btn purple" onclick="loadModal('<?php echo base_url($index.'compliance/editPortalUserModal/18') ;?>')"><i class="fa fa-user"></i> New Home Portal User</button>
                    <button type="button" class="btn purple" onclick="loadModal('<?php echo base_url($index.'compliance/editPortalUserModal/17') ;?>')"><i class="fa fa-user"></i> New Pharmacy User Portal</button>
                </p>   
            </div>
        </div>
        <!-- END BUTTONS -->
        <!-- BEGIN MESSAGE BOX -->
        <?php 
        if(!empty($delete)) {
            echo '<div class="alert alert-success" role="alert" style="width:100%;float:left;">Well done! You record has been deleted .</div>';
        }
	?>
        <!-- END MESSAGE BOX -->
        <!-- BEGIN SIDEBAR CONTENT LAYOUT -->
        <div class="page-content-container users_list">
            <div class="page-content-row">
                <div class="page-content-col">
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-users"></i>
                                        <span class="caption-subject bold uppercase">Home Portal Super Users</span>
                                    </div>
                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th> Name </th>
                                                <th> Email </th>
                                                <th> Phone </th>
                                                <th> Last Login </th>
                                                <th> Actions </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if ($portalUsers != false) {
                                            foreach ( $portalUsers as $portalUser ) {
                                            ?>
                                            <tr>
                                                <td> <?php echo ucfirst($portalUser->first_name) . ' ' . ucfirst($portalUser->last_name); ?> </td>
                                                <td> <?php echo $portalUser->email; ?> </td>
                                                <td> <?php echo $portalUser->phone; ?> </td>
                                                <td> <?php echo unix_to_human($portalUser->last_login); ?> </td>
                                                <td> <a href="<?php echo base_url(); ?><?php echo $index; ?>compliance/editPortalUserModalBySeperatePage/18/<?php echo $portalUser->id; ?>"><i class="fa fa-pencil-square-o font-dark"></i></a>
                                                <a href="<?php echo base_url() ?><?php echo $index; ?>compliance/userStatus/<?php echo $portalUser->id; ?>"> 
                                                <?php if($portalUser->active == 0){ ?>
                                                    <i class="fa fa-toggle-off" style="color:red;"></i></a>
                                                <?php }else{ ?>
                                                    <i class="fa fa-toggle-on" style="color:green;"></i>
                                                <?php } ?>
                                                </a>
                                                </td>
                                            </tr>
                                            <?php }} ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption"><i class="fa fa-users"></i><span class="caption-subject bold uppercase">Home Portal Users</span> </div>
                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                                        <thead>
                                            <tr>
                                                <th> Name </th>
                                                <th> Email </th>
                                                <th> Phone </th>
                                                <th> Last Login </th>
                                                <th> Actions </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if ($portalUsers != false) {
                                            foreach ( $portalUsers as $portalUser ) {
                                            if(unix_to_human($portalUser->last_login) > $oneYearBack ){
                                            ?>
                                            <tr>
                                                <td> <?php echo ucfirst($portalUser->first_name) . ' ' . ucfirst($portalUser->last_name); ?> </td>
                                                <td> <?php echo $portalUser->email; ?> </td>
                                                <td> <?php echo $portalUser->phone; ?> </td>
                                                <td> <?php echo unix_to_human($portalUser->last_login); ?> </td>
                                                <td> <a href="<?php echo base_url(); ?><?php echo $index; ?>compliance/editPortalUserModalBySeperatePage/18/<?php echo $portalUser->id; ?>"><i class="fa fa-pencil-square-o font-dark"></i></a>
                                                <a href="<?php echo base_url() ?><?php echo $index; ?>compliance/userStatus/<?php echo $portalUser->id; ?>"> 
                                                <?php if($portalUser->active == 0){ ?>
                                                    <i class="fa fa-toggle-off" style="color:red;"></i></a>
                                                <?php }else{ ?>
                                                    <i class="fa fa-toggle-on" style="color:green;"></i>
                                                <?php } ?>
                                                </a>
                                                </td>
                                            </tr>
                                            <?php }}} ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box red">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-users"></i>
                                        <span class="caption-subject bold uppercase">Pharmacy Tech</span>
                                    </div>
                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover" id="sample_3">
                                        <thead>
                                            <tr>
                                                <th> Name </th>
                                                <th> Email </th>
                                                <th> Phone </th>
                                                <th> Last Login </th>
                                                <th> Actions </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if ($techUsers != false) {
                                            foreach ( $techUsers as $portalUser ) {  
                                            ?>
                                            <tr>
                                                <td> <?php echo ucfirst($portalUser->first_name) . ' ' . ucfirst($portalUser->last_name); ?> </td>
                                                <td> <?php echo $portalUser->email; ?> </td>
                                                <td> <?php echo $portalUser->phone; ?> </td>
                                                <td> <?php echo unix_to_human($portalUser->last_login); ?> </td>
                                                <td> <a href="<?php echo base_url(); ?><?php echo $index; ?>compliance/editPortalUserModalBySeperatePage/18/<?php echo $portalUser->id; ?>"><i class="fa fa-pencil-square-o font-dark"></i></a>
                                                <a href="<?php echo base_url() ?><?php echo $index; ?>compliance/userStatus/<?php echo $portalUser->id; ?>"> 
                                                <?php if($portalUser->active == 0){ ?>
                                                    <i class="fa fa-toggle-off" style="color:red;"></i></a>
                                                <?php }else{ ?>
                                                    <i class="fa fa-toggle-on" style="color:green;"></i>
                                                <?php } ?>
                                                </a>
                                                </td>
                                            </tr>
                                            <?php }} ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
            </div>
        </div>
        <!-- END SIDEBAR CONTENT LAYOUT -->
    </div>
