<?php $ci = & get_instance(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title> <?php if($this->uri->segment(2) == "NULL") { echo "Home"; } else { echo $this->uri->segment(2); } ?> </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
	

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/cloud-admin.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/themes/default.css" id="skin-switcher">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/chosen.css">	    

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    
    <link href="<?php echo base_url(); ?>css/raterater.css" rel="stylesheet"/>
    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/datepicker/themes/default.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/datepicker/themes/default.date.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/datepicker/themes/default.time.min.css" />

	<!-- DATE TIME PICKER -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/bootstrap-datetimepicker.min.css" />
	
	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/datatables/media/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/datatables/media/assets/css/datatables.min.css" />
   
    <link href="<?php echo base_url(); ?>css/anysearch.css" media="screen" rel="stylesheet" type="text/css">

	<style>
	   .re{
	       font-size: 18px;
	   }
	</style>
</head>
<body>
	
								
	<div class="col-lg-12">
		<div class="panel panel-primary">
		    <div class="panel-heading"> <?php echo $day; ?> </div>
		    <div class="panel-body">
		    	
		    	<ul class="list-group">	 
		    	<table class="re">
		    	
		    	    <tr>		    	    
		    	    <td width="350">
			    	<?php
			    	      $i = 1;
						  $day = $day;
						  $type = 1;
			    	      foreach($EboardHome AS $record){	
			    	      	    $i++;
			    	      	    $deliveryDate = date("l", strtotime($record->dateOfDelivery));	
							  	$break = $i % 12;
							  	
								if($day == $deliveryDate){
									echo "<b>";
									$ci->getHomeInfoForEboard($record->NHID, $record->NHWardID, $record->dateOfDelivery,$type);	
									echo "</b>";
								}
								
								if($break == 1) { echo '</td><td>&nbsp;</td><td valign="top" width="350">'; } 
								
			    	      }		    	
			    	?>	
			    	</td>
			    	</tr>
		    	</table>   		    	
		    	</ul>
		    	
		   </div>
		    <div class="panel-footer"> 
		    	
		    	
		    		
		    </div>
	 	</div>
	</div>