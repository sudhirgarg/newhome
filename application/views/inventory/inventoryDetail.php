<!-- STYLER -->

<!-- /STYLER -->
<!-- BREADCRUMBS -->
<!-- /BREADCRUMBS -->
<div class="clearfix">
	<h3 class="content-title pull-left">Inventory</h3>
</div>
<div class="description">
	<a class="btn btn-default btn-lg"
		href="<?php echo base_url("inventory/viewAudit");?>"> <i
		class="fa fa-save"></i> Finalize
	</a>
	<h4>
		<span class="label label-default arrow-in">Audit ID: 261 | Sample
			Size: 23 | %Error:+3%</span> <span
			class="label label-warning arrow-in">In Progress 01/23/2014</span> <span
			class="label label-success arrow-in">Finalized On: 01/25/2014</span>
	</h4>
</div>
</div>
</div>
</div>
<!-- /PAGE HEADER -->
<!-- DELIVERY RUNS CONTENT -->
<!-- DATA TABLES -->
<div class="row">
	<div class="col-md-12">


		<!-- Audit Items -->
		<div class="row">
			<div class="col-lg-12">
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-barcode"></i> </span>
					<input type="text" class="form-control"
						placeholder="Scan stock bottle here.">
				</div>
				<div class="box border blue">
					<div class="box-title">
						<h4>
							<i class="fa fa-bars"></i>Inventory Items
						</h4>
					</div>
					<div class="box-body">
						<table id="datatable1" cellpadding="0" cellspacing="0" border="0"
							class="datatable table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th>Drug Name</th>
									<th>Kroll On Hand</th>
									<th>Counted Value</th>
									<th>Audited by</th>
									<th>% Error</th>
									<th>% Standard Deviation</th>
									<th>Delete</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Alomide, 0.1%, DIN:00893560</td>
									<td>10</td>
									<td>14</td>
									<td>John Doe</td>
									<td>+ 3 %</td>
									<td>+ 14 %</td>
									<td><a class="btn btn-default btn-lg"
										href="<?php echo base_url("delivery/rundetail/");?>"> <i
											class="fa fa-minus-circle"></i> Delete
									</a></td>
								</tr>
								<tr>
									<td>Alomide, 0.1%, DIN:00893560</td>
									<td>10</td>
									<td>14</td>
									<td>John Doe</td>
									<td>+ 3 %</td>
									<td>+ 14 %</td>
									<td><a class="btn btn-default btn-lg"
										href="<?php echo base_url("delivery/rundetail/");?>"> <i
											class="fa fa-minus-circle"></i> Delete
									</a></td>
								</tr>
								<tr>
									<td>Alomide, 0.1%, DIN:00893560</td>
									<td>10</td>
									<td>14</td>
									<td>John Doe</td>
									<td>+ 3 %</td>
									<td>+ 14 %</td>
									<td><a class="btn btn-default btn-lg"
										href="<?php echo base_url("delivery/rundetail/");?>"> <i
											class="fa fa-minus-circle"></i> Delete
									</a></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- /Audit Items -->

	</div>
</div>
<!-- /PAGE -->

<!---------      PAGE FOORER    ------------>
<div class="footer-tools">
	<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
	</span>
</div>
