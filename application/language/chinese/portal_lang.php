<?php
/* 
------------------
Language: Traditional Chinese 
------------------
*/

// Menu

$lang['MENU_HOMES'] = '護理組';
$lang['MENU_CLIENTS'] = '客戶資料';
$lang['MENU_TICKETS'] = '服務問詢';
$lang['MENU_EVENTS'] = '健康監測';
$lang['MENU_COVERAGE_INFO'] = '藥品保險信息';
$lang['MENU_SITE_MAP'] = '網站地圖';
$lang['MENU_PHARMACY_INFO'] = '藥房信息';


//TOP MENU
$lang['MY_PROFILE'] = '我的賬戶';
$lang['LANGUAGE'] = '語言';
$lang['TERMS_OF_USE'] = '服務條款';
$lang['PRIVACY_SETTINGS'] = '隱私條例';
$lang['LOG_OUT'] = '退出系統';


// Homes Page

// homes table
$lang['HELLO']= '您好';
$lang['HOMES'] = '護理組';
$lang['ADDRESS'] = '地址';
$lang['CONTACT'] = '聯絡人';
$lang['PHONE'] = '聯絡電話';


// CLIENTS

//table
$lang['TITLE'] = '客戶資料';
$lang['BACK_TO_HOMES'] = '退到首頁';
$lang['CLIENT'] = '客戶';
$lang['HOME'] = '護理組';
$lang['TICKETS'] = '服務問詢';
$lang['MED CHANGES'] = '藥物變更';
$lang['BILLING'] = '健康监测';

// Tickets

$lang['GENERAL'] = '一般問詢';
$lang['RE_ORDER'] = '再次訂購';
$lang['PHARMACIST'] = '詢問藥劑師';
$lang['INTERNAL'] = '內部通信';
$lang['COMPLETE'] = '完成服務';

// Events

$lang['TITLE'] = '健康監測';
$lang['EVENTS_DASHBOARD'] = '健康信息監測表';
$lang['RECENT_ACTIVITY'] = '最近健康事件記錄';
$lang['EVENT_TYPES'] = '本月監測的健康事件种类';
$lang['EVENTS_FOR_THIS_YEAR'] = '本年健康事件監測表';
$lang['ACTIVITY'] = '健康事件記錄';

// Profile

$lang['CLIENT_PROFILE'] = '個體客戶詳細信息';
$lang['CLIENTS'] = '所有客戶';
$lang['NEW_TICKETS'] = '新的服務問詢';
$lang['HEALTH_PROFILE'] = '客戶健康監測信息';
$lang['PRINT_PATIENT_PROFILE'] = '打印客戶的詳細用藥信息';
$lang['PROFILE'] = '客戶信息';
$lang['RE_ORDERS'] = '再次訂購';
$lang['SERVICE_TICKETS'] = '服務問詢';
$lang['MED_CHANGES'] = '用藥變更';
$lang['ALLERGIES'] = '過敏史';
$lang['CONDITIONS'] = '健康狀況';
$lang['NOTE'] = '備註';
$lang['HOME'] = '護理組';
$lang['DELIVERY'] = '配送';
$lang['COMPLIANCE_PACKAGE'] = '目前用藥';
$lang['MEDICATION'] = '藥品名稱';
$lang['DOCTOR'] = '醫生';
$lang['DIRECTIONS'] = '用藥指南';
$lang['COMMENT'] = '留言備註';
$lang['SELECT'] = '選擇';
$lang['CREATE_ORDER'] = '下訂單';
$lang['CREATED'] = '下單時間';
$lang['COMPLETED_DATE'] = '完成時間';
$lang['CREATED_BY'] = '問詢人';
$lang['TICKET_TYPE'] = '問詢類別';
$lang['DESCRIPTION'] = '詳細描述';
$lang['COMPLETE'] = '完成';
$lang['TECHNICIAN'] = '藥房技師';
$lang['PHARMACIST'] = '藥劑師';
$lang['STARTS_ENDS'] = '開始日期';
$lang['CHANGE'] = '用藥變更描述';
$lang['DrugPlan'] = '受保計劃 ';

