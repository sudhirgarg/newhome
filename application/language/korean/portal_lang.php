<?php
/* 
------------------
Language: Traditional Chinese 
------------------
*/

// Menu

$lang['MENU_HOMES'] = '집';
$lang['MENU_CLIENTS'] = '환자';
$lang['MENU_TICKETS'] = '대화창구';
$lang['MENU_EVENTS'] = '행사';
$lang['MENU_COVERAGE_INFO'] = '약물정보';
$lang['MENU_SITE_MAP'] = '';
$lang['MENU_PHARMACY_INFO'] = '약국정보';


//TOP MENU
$lang['MY_PROFILE'] = '내 약물정보';
$lang['LANGUAGE'] = '언어';
$lang['TERMS_OF_USE'] = '사용언어';
$lang['PRIVACY_SETTINGS'] = '개인정보 새팅';
$lang['LOG_OUT'] = '로그아웃';


// Homes Page

// homes table
$lang['HELLO']= '안녕';
$lang['HOMES'] = '집';
$lang['ADDRESS'] = '주소';
$lang['CONTACT'] = '연락정보';
$lang['PHONE'] = '전화번호';


// CLIENTS

//table
$lang['TITLE'] = '제목';
$lang['BACK_TO_HOMES'] = '집(환자) 한테로';
$lang['CLIENT'] = '환자';
$lang['HOME'] = '집';
$lang['TICKETS'] = '대화(약주문,질문,기타)';
$lang['MED CHANGES'] = '복용약변화';
$lang['BILLING'] = '약물외기타(혈압,혈당..)';

// Tickets

$lang['GENERAL'] = '일반적인';
$lang['RE_ORDER'] = '얄물 재주문';
$lang['PHARMACIST'] = '약사';
$lang['INTERNAL'] = '내부';
$lang['COMPLETE'] = '완성';

// Events

$lang['TITLE'] = '재목';
$lang['EVENTS_DASHBOARD'] = '계시판';
$lang['RECENT_ACTIVITY'] = '최근행사';
$lang['EVENT_TYPES'] = '행사내역';
$lang['EVENTS_FOR_THIS_YEAR'] = '올해의 활동';
$lang['ACTIVITY'] = '활동';

// Profile

$lang['CLIENT_PROFILE'] = '환자복용약정보';
$lang['CLIENTS'] = '환자';
$lang['NEW_TICKETS'] = '새로운 주문,질문';
$lang['HEALTH_PROFILE'] = '건강정보';
$lang['PRINT_PATIENT_PROFILE'] = '환자개인정보';
$lang['PROFILE'] = '정보(약물정보,건강정보기타)';
$lang['RE_ORDERS'] = '약물재주문';
$lang['SERVICE_TICKETS'] = '서비스요구';
$lang['MED_CHANGES'] = '복용약변화';
$lang['ALLERGIES'] = '알러지';
$lang['CONDITIONS'] = '질병상태';
$lang['NOTE'] = '주의사항';
$lang['HOME'] = '집';
$lang['DELIVERY'] = '배달';
$lang['COMPLIANCE_PACKAGE'] = '약물패캐지';
$lang['MEDICATION'] = '약';
$lang['DOCTOR'] = '의사';
$lang['DIRECTIONS'] = '지시,복용법';
$lang['COMMENT'] = '노트,의견';
$lang['SELECT'] = '선택';
$lang['CREATE_ORDER'] = '주문창조';
$lang['CREATED'] = '주문완성';
$lang['COMPLETED_DATE'] = '완성날짜';
$lang['CREATED_BY'] = '주문한사람';
$lang['TICKET_TYPE'] = '주문형태';
$lang['DESCRIPTION'] = '특징';
$lang['COMPLETE'] = '완성';
$lang['TECHNICIAN'] = '약사보조자';
$lang['PHARMACIST'] = '약사';
$lang['STARTS_ENDS'] = '시작_끝';
$lang['CHANGE'] = '변화';


