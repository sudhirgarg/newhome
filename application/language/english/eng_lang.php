<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
/* 
------------------
Language: English
------------------
*/


// Menu

$lang['MENU_HOMES'] = 'Homes';
$lang['MENU_CLIENTS'] = 'Clients';
$lang['MENU_TICKETS'] = 'Tickets';
$lang['MENU_EVENTS'] = 'Events';
$lang['MENU_COVERAGE_INFO'] = 'Advertise';
$lang['MENU_SITE_MAP'] = 'Site Map';

//TOP MENU
$lang['MY_PROFILE'] = 'My Profile';
$lang['LANGUAGE'] = 'Language';
$lang['TERMS_OF_USE'] = 'Terms of use';
$lang['PRIVACY_SETTINGS'] = 'Privacy Settings';
$lang['LOG_OUT'] = 'Log out';


// Homes Page

// homes table
$lang['HOMES'] = 'Homes';
$lang['ADDRESS'] = 'Address';
$lang['CONTACT'] = 'Contact';
$lang['PHONE'] = 'Phone';


// CLIENTS

//table
$lang['TITLE'] = 'Clients';
$lang['BACK_TO_HOMES'] = 'Back to Homes';
$lang['CLIENT'] = 'Client';
$lang['HOME'] = 'Home';
$lang['TICKETS'] = 'Tickets';
$lang['MED CHANGES'] = 'Med Changes';
$lang['BILLING'] = 'Billing';

// Tickets

$lang['GENERAL'] = 'General';
$lang['RE_ORDER'] = 'Re-Order';
$lang['PHARMACIST'] = 'Pharmacist';
$lang['INTERNAL'] = 'Internal';
$lang['COMPLETE'] = 'Complete';

// Events

$lang['TITLE'] = 'Events';
$lang['EVENTS_DASHBOARD'] = 'Events Dashboard';
$lang['RECENT_ACTIVITY'] = 'Recent Activity';
$lang['EVENT_TYPES'] = 'Types of events in November';
$lang['EVENTS_FOR_THIS_YEAR'] = 'Events for this year';
$lang['ACTIVITY'] = 'Activity';

// Profile

$lang['CLIENT_PROFILE'] = 'Client Profile';
$lang['CLIENTS'] = 'Clients';
$lang['NEW_TICKETS'] = 'New Tickers';
$lang['HEALTH_PROFILE'] = 'Events';
$lang['PRINT_PATIENT_PROFILE'] = 'Print Patient Profile';
$lang['PROFILE'] = 'Profile';
$lang['RE_ORDERS'] = 'Re-Orders';
$lang['SERVICE_TICKETS'] = 'Service-Tickets';
$lang['MED_CHANGES'] = 'Med Changes';
$lang['ALLERGIES'] = 'Allergies';
$lang['CONDITIONS'] = 'Conditions';
$lang['NOTE'] = 'Note';
$lang['HOME'] = 'Home';
$lang['DELIVERY'] = 'Delivery';
$lang['COMPLIANCE_PACKAGE'] = 'Compliance Package';
$lang['MEDICATION'] = 'Medication';
$lang['DOCTOR'] = 'Doctor';
$lang['DIRECTIONS'] = 'Directions';
$lang['COMMENT'] = 'Comment';
$lang['SELECT'] = 'Select';
$lang['CREATE_ORDER'] = 'Create Order';
$lang['CREATED'] = 'Created';
$lang['COMPLETED'] = 'Completed';
$lang['CREATED_BY'] = 'Created By';
$lang['TICKET_TYPE'] = 'Ticket Type';
$lang['DESCRIPTION'] = 'Description';
$lang['TECHNICIAN'] = 'Technician';
$lang['PHARMACIST'] = 'Pharmacist';
$lang['STARTS_ENDS'] = 'Starts / Ends';
$lang['CHANGE'] = 'Change';


?>