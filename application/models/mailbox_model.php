<?php if (! defined ( 'BASEPATH' ))	exit ( 'No direct script access allowed' );

class mailbox_model extends CI_Model {
	public function __construct() {
		parent::__construct ();
	}
	
    public function utcToDynDate($dts) {
        $utc = date('U');
        $diff = $utc - $dts;
        
        switch($diff) {
            case $diff <= 86400:
                $dt = date('g:ia', $dts);
                break;
                
            case $diff > 86400 && $diff <= 604800:
                $dt = date('D', $dts);
                break;
                
            case $diff > 604800 && $diff <= 31536000:
                $dt = date('M j', $dts);
                break;
                
            case $diff > 31536000:
                $dt = date('n/j/y', $dts);
                break;
        }
        return $dt;
    }
    
    public function get_messages_in_mailbox($mailbox, $config) {
        $mbox_string = "{".$config['host'].":".$config['port'].$config['service_flags']."}".$mailbox;
        $mbox = imap_open($mbox_string, $config['login'], $config['pass'], OP_HALFOPEN)
              or die("can't connect: " . imap_last_error());
              
        $list = imap_list($mbox, $mbox_string, "*");
        return $list;
    }
}

