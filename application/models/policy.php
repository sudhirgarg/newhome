<?php
class policy extends CI_Model {
	function __construct() {
		// Call the Model constructor
		parent::__construct ();
	}
	
    function getPolicyArea($policyAreaID='0')
    {
        if ($policyAreaID == '0')
        { // get them all
            return $this->db->query("Select * from policyArea Order by policyAreaName");
        } else 
        { // get specific one
            if ($policyAreaID == '-1')
            {
                $p = new stdClass();
                $p->policyAreaID = '-1';
                $p->policyAreaName = '';
                return $p;
            } else 
            {
                $v = $this->db->query('select * from policyArea where policyAreaID = ?', array($policyAreaID));
                if ($v->num_rows() > 0)
                {   // array $p->policyAreaID, $p->policyAreaName
                    return $v->row();
                } else
                {  // returns Empty Policy Area
                    return $this->policy->getPolicyArea('-1');
                }
            }
        }    
    }
    
    function getPolicies($policyAreaID='-1', $policyID='0')
    {
        if ($policyAreaID == '-1')
        {   // get all policies regardless of policy Area
            return $this->db->query("Select * from policies");
        } else 
        {
            if ($policyID == '0')
            {   // retreive all policies under the policyAreaID
                return $this->db->query("Select * from policies where policyAreaID = ?", array($policyAreaID));
            } else 
            {
                if ($policyID == '-1')
                {   // return empty Policy
                    $p = new stdClass();
                    $p->policyID = '-1';
                    $p->policyAreaID = $policyAreaID;
                    $p->policyTitle = '';
                    $p->dateCreated = date('Y-m-d');
                    $p->dateRevised = date('Y-m-d');
                    $p->effectiveDate = date('Y-m-d');
                    $p->createdBy = $this->ion_auth->user()->row()->id;
                    $p->purpose ='';
                    $p->policy = '';
                    $p->procedures = '';
                    
                    return $p;
                } else 
                {   //return specific policy
                    $v = $this->db->query("Select * from policies where policyID = ?", array($policyID));
                    if ($v->num_rows() > 0)
                    {
                        return $v->row();
                    } else {
                        return $this->policy->getPolicies($policyAreaID, '-1');
                    }
                }
            }
        }
    }
    
    function modifyPolicyArea($policyAreaID='-1', $policyAreaName='', $active='1')
    {
        if ($policyAreaID > 0)
        {   // Update Existing Policy
            if ($active == 0)
            { //delete policy Area
                $this->db->query("delete from policyArea where policyAreaID = ?", array($policyAreaID));
                return '1|Deleted Policy Area';
            } else 
            {
                $data = array("policyAreaName"=>$policyAreaName);
                $this->db->where('policyAreaID', $policyAreaID);
                if ($this->db->update('policyArea', $data))
                {
                    return '1|Successfully Updated Policy Area';
                } else {
                    return 'Unknown Error Occurred, Please Try again';
                }
            }
        } else 
        {   // Create New Policy
            $data = array('policyAreaName'=>$policyAreaName);
            if ($this->db->insert('policyArea', $data))
            {
                return '1|Successfully Created New Policy Area';
            } else 
            {
                return '0|Unknown Error Occurred, Please Refresh and Try again';
            }
        }
    }
    
    function modifyPolicy($policyID='0', $policyAreaID='-1', $policyTitle='', $dateCreated='-1', $dateRevised='-1', $effectiveDate='-1', $createdBy='-1', $purpose='',$policy='', $procedures='', $active='1')
    {
        if ($createdBy == '-1')
        {
            $createdBy = $this->ion_auth->user()->row()->id;
        }
        
        if ($dateCreated == '-1')
        {
            $dateCreated= date('Y-m-d');
        }
        
        if ($dateRevised == '-1')
        {
            $dateRevised = date('Y-m-d');
        }
        
        if ($effectiveDate = '-1')
        {
            $effectiveDate = date('Y-m-d');
        }
        
        if ($policyID != '0')
        {   //Update Existing Policy
            if ($active == '0')
            {   // Delete Policy
                $this->db->query("delete from policies where policyID = ?", array($policyID));
                return '1| Deleted Policy';
            } else {
                // Update Policy
                $data = array('policyTitle'=>$policyTitle, 'dateRevised'=>$dateRevised, 'effectiveDate'=>$effectiveDate, 'purpose'=>$purpose, 'policy'=>$policy,'procedures'=>$procedures);
                $this->db->where('policyID', $policyID);
                if ($this->db->update('policies', $data))
                {
                    return '1|Successfully Updated Policy';
                } else {
                    return '0|Database Error, Please Try Again';
                }
            }
        } else {
            //Insert Policy
            $data = array('policyAreaID'=>$policyAreaID, 'policyTitle'=>$policyTitle, 'dateCreated'=>$dateCreated, 'dateRevised'=>$dateRevised, 'effectiveDate'=>$effectiveDate, 'createdBy'=>$createdBy, 'purpose'=>$purpose, 'policy'=>$policy,'procedures'=>$procedures);
            if ($this->db->insert('policies', $data))
            {
                return '1|Successfully Created Policy';
            } else 
            {
                return '0|Database Error, Please Try Again';
            }
        }
    }
    
}