<?php

    class assignPatTOCG extends CI_Model {
       
        protected  $tableName ='assignpattocg';
        protected  $where ="id";
        protected  $update ="";
        protected  $orderBy ="";

        public function __construct() {

            parent::__construct ();

        }

        // Standed functions Start

        public function addData($data){

            $d = $this->db->insert($this->tableName, $data); 
            if($d == TRUE)	{
                return $d;
            }

        }

        public function updateData($id,$data){

            $this->db->where($this->where, $id);
            $d = $this->db->update($this->tableName, $data); 
            if($d == TRUE)	{
                return $d;
            }
        }

        public function deleteData($id){

            $this->db->where($this->where, $id);
            $this->db->delete($this->tableName); 

        }

        public function getAllData(){
            $this->db->order_by($this->orderBy, "ASC"); 			
            return $query = $this->db->get($this->tableName)->result();	

        }

        public function getAllDataBaseOnId($data){
            $this->db->where($this->where, $data);			
            return $query = $this->db->get($this->tableName)->result();	 

        }

        public function findDuplicate($data){
            // $data has to be an array
            $this->db->where($data);
            return $query = $this->db->get($this->tableName)->num_rows();
        }

        // Standed functions Start End    
        
        
        public function getInfoBaseOnCGID($code){
        	 $str ="SELECT * FROM assignpattocg WHERE CGID =".$code;
             return $query = $this->db->query($str)->result();
        }
		
		public function getAccessCOnfirm($pid,$CGID){
			$str ="SELECT * FROM assignpattocg WHERE patID LIKE '%$pid%' AND CGID =".$CGID;
             return $query = $this->db->query($str)->result();
			
		}
		
		public function getValidEmailCGIDForTicket($patID){
								
			$str="SELECT CGID FROM assignpattocg
					LEFT JOIN emailauth
					ON assignpattocg.CGID = emailauth.userId
					WHERE assignpattocg.patID LIKE '%$patID%' AND emailauth.ticket = 1
				
					UNION
				
				    SELECT users.id AS CGID FROM users
				    LEFT JOIN pat ON users.NHID = pat.NHID
				    WHERE pat.ID =$patID AND users.id IN (SELECT userId FROM emailauth WHERE ticket =1)";
					
			return $query = $this->db->query($str)->result();
		}
		
		public function getValidEmailCGIDForComment($patID){
								
			$str="SELECT CGID FROM assignpattocg
				LEFT JOIN emailauth
				ON assignpattocg.CGID = emailauth.userId
				WHERE assignpattocg.patID LIKE '%$patID%' AND emailauth.comment = 1
			
				UNION
			
			    SELECT users.id AS CGID FROM users
			    LEFT JOIN pat ON users.NHID = pat.NHID
			    WHERE pat.ID =$patID AND users.id IN (SELECT userId FROM emailauth WHERE comment =1)";
					
			return $query = $this->db->query($str)->result();
		}
		
		public function getValidEmailCGIDForMedChange($patID){
								
			$str="SELECT CGID FROM assignpattocg
				LEFT JOIN emailauth
				ON assignpattocg.CGID = emailauth.userId
				WHERE assignpattocg.patID LIKE '%$patID%' AND emailauth.medChange = 1
			
				UNION
			
			    SELECT users.id AS CGID FROM users
			    LEFT JOIN pat ON users.NHID = pat.NHID
			    WHERE pat.ID =$patID AND users.id IN (SELECT userId FROM emailauth WHERE medChange =1)";
			    
			return $query = $this->db->query($str)->result();
		}
       
    }
?>
