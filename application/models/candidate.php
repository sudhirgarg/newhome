<?php

	class candidate extends CI_Model {
		
		/*
		 * Table Properties 
		   CREATE TABLE candidate (
				id INT NOT NULL AUTO_INCREMENT,
		 		generalQuizInfoId INT ,
				userName VARCHAR(250),
		 		score VARCHAR(250),
		 		attempt INT , 
				remark VARCHAR(100),
				PRIMARY KEY (id)
			);		
		 * 
		 */

		protected  $tableName ='candidate';
		protected  $where ="id";
		protected  $update ="";
		protected  $orderBy ="";
		
		public function __construct() {
		
			parent::__construct ();
		
		}

		// Standed functions Start
		
		public function addData($data){
			
			$d = $topicData = $this->db->insert($this->tableName, $data); 
			if($d == TRUE)	{
				return $d;
			}
			
		}
		
		public function updateData($id,$data){
			
			$this->db->where($this->where, $id);
			$d = $this->db->update($this->tableName, $data); 
			if($d == TRUE)	{
				return $d;
			}
		}
		
		public function deleteData($id){
			
			$this->db->where($this->where, $id);
			$this->db->delete($this->tableName); 
			
		}
		
		public function getAllData(){
			$this->db->order_by($this->orderBy, "ASC"); 			
			return $query = $this->db->get($this->tableName)->result();	 
						
		}
		
		public function getAllDataBaseOnId($data){
									
			$this->db->where($this->where, $data);			
			return $query = $this->db->get($this->tableName)->result();	 
						
		}
		
		public function getAllDataBaseOnIdInRow($data){
				
			$this->db->where($this->where, $data);
			return $query = $this->db->get($this->tableName)->row();
		
		}
		
		public function findDuplicate($data){
			// $data has to be an array
			$this->db->where($data);
			return $query = $this->db->get($this->tableName)->num_rows();
				
		}
		
		// Standed functions Start End
		
		public function getDataBaseOnGeneralQuizInfoIdUserNameAttempt($data){
									
			$this->db->where($data);			
			return $query = $this->db->get($this->tableName)->result();	 
						
		}	
		
		
		public function getAllScoreBaseOnUserId ($data){
			$str = "SELECT candidate.score, candidate.attempt, generalquizzesinfo.gQname FROM candidate
					LEFT JOIN generalquizzesinfo
					ON candidate.generalQuizInfoId = generalquizzesinfo.id
					WHERE candidate.userName = '$data' AND candidate.score >= 0
					ORDER BY generalquizzesinfo.gQname ASC, candidate.attempt DESC";
		
			return $query=$this->db->query($str)->result();
		}	
		
		public function getCompletedQuiz($qId,$userId){
			$str = "SELECT * FROM candidate WHERE generalQuizInfoId =$qId AND userName = $userId AND score >= 0";
			return $this -> db -> query($str) -> row();
		}
		
		public function getUserInfo(){
			$str = "SELECT * FROM users LEFT JOIN candidate ON candidate.userName = users.id WHERE candidate.score >= 0 GROUP BY users.id ORDER BY users.first_name ASC";		
			return $query=$this->db->query($str)->result();
		}
		
		public function getScoreBaseOnUserName($id){
			$str = "SELECT c.id, c.attempt, c.score, gq.gQname FROM candidate AS c
					LEFT JOIN generalquizzesinfo AS gq ON gq.id = c.generalQuizInfoId
					WHERE c.score >= 0 AND c.userName = $id ORDER BY gq.gQname ASC";
			return $this -> db -> query($str) -> result();
		}
		
		public function getUserExamDetails($id){
			$str ="SELECT c.id, c.generalQuizInfoId, c.attempt, c.score, u.first_name, u.last_name, gq.gQname FROM candidate AS c
					LEFT JOIN users AS u ON u.id = c.userName
					LEFT JOIN generalquizzesinfo AS gq ON gq.id = c.generalQuizInfoId
					WHERE c.id =$id";
			return $this -> db -> query($str) -> row();
		}

	}


?>