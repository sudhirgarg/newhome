
<?php

	class PackagerBatch extends CI_Model {
		
		

		protected  $tableName ='packagerbatch';
		protected  $where ="id";
		protected  $update ="";
		protected  $orderBy ="batchNum";
		
		public function __construct() {
		
			parent::__construct ();
		
		}

		// Standed functions Start
		
		public function addData($data){
			
			$d = $topicData = $this->db->insert($this->tableName, $data); 
			if($d == TRUE)	{
				return $d;
			}
			
		}
		
		public function updateData($id,$data){
			
			$this->db->where($this->where, $id);
			$d = $this->db->update($this->tableName, $data); 
			if($d == TRUE)	{
				return $d;
			}
		}
		
		public function updateDataBybatch($id,$data){
				
			$this->db->where('batchNum', $id);
			$d = $this->db->update($this->tableName, $data);
			if($d == TRUE)	{
				return $d;
			}
		}
		
		public function deleteData($id){
			
			$this->db->where($this->where, $id);
			$this->db->delete($this->tableName); 
			
		}
		
		public function getAllData(){
			//$this->db->order_by($this->orderBy, "ASC"); 			
			//return $query = $this->db->get($this->tableName)->result();	
			
			$str ="SELECT * FROM $this->tableName WHERE batchNum NOT IN (SELECT batchNum FROM pacmed) AND removed = 1 ORDER BY batchNum ASC";
			return $query = $this->db->query($str)->result();
						
		}
		
		public function getAllDataBaseOnId($data){
									
			$this->db->where($this->where, $data);			
			return $query = $this->db->get($this->tableName)->result();	 
						
		}
		
		public function findDuplicate($data){
			// $data has to be an array
			$this->db->where($data);
			return $query = $this->db->get($this->tableName)->num_rows();
				
		}
		
		public function getNumberOfOrders(){
			$str ="SELECT COUNT(*) AS num FROM $this->tableName WHERE batchNum NOT IN  (SELECT batchNum FROM pacmed) AND removed = 1";
			return $query = $this->db->query($str)->row();
		}
		
		public function findBatchNum($id){
			$str = "SELECT * FROM $this->tableName WHERE batchNum =$id";
			return $query = $this->db->query($str)->row();
		}
		// Standed functions Start End
		
		
		
		

	}


?>