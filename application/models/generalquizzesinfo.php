<?php

	class generalQuizzesInfo extends CI_Model {
		
		/*
		 * Table Properties 
		   CREATE TABLE generalquizzesinfo (
				id INT NOT NULL AUTO_INCREMENT,
				gQname VARCHAR(100) NOT NULL,
		 		topicsId INT,
				details TEXT,
		        timeDutation INT,              
				addBy VARCHAR(100),
				addDate DATETIME,
				remark VARCHAR(100),
				PRIMARY KEY (id)
			);		
		 * 
		 */

		protected  $tableName ='generalquizzesinfo';
		protected  $where ="id";
		protected  $orderBy ="gQname";
		
		public function __construct() {
		
			parent::__construct ();
		
		}

		// Standed functions Start
		
		public function addData($data){
			
			$d = $topicData = $this->db->insert($this->tableName, $data); 
			if($d == TRUE)	{
				return $d;
			}
			
		}
		
		public function updateData($id,$data){
			
			$this->db->where($this->where, $id);
			$d = $this->db->update($this->tableName, $data); 
			if($d == TRUE)	{
				return $d;
			}
		}
		
		public function deleteData($id){
			
			$this->db->where($this->where, $id);
			$this->db->delete($this->tableName); 
			
		}
		
		public function getAllData(){
			$this->db->order_by($this->orderBy, "ASC"); 			
			return $query = $this->db->get($this->tableName)->result();	 
						
		}
		
		public function getAllDataBaseOnId($data){
									
			$this->db->where($this->where, $data);			
			return $query = $this->db->get($this->tableName)->result();	 
						
		}
		
		public function findDuplicate($data){
			// $data has to be an array
			$this->db->where($data);
			return $query = $this->db->get($this->tableName)->num_rows();
				
		}
		
		public function getId($data){
			// $data has to be an array
			$this->db->where($data);
			return $query = $this->db->get($this->tableName)->result();
				
		}
		
		// Standed functions Start End
			
		
		public function getGeneralquizzesinfoGroupInList(){
	    	
			$this->db->select('generalquizzesinfo.topicsId , COUNT(generalquizzesinfo.id) AS totailQuzzes , topics.topicsName');
			$this->db->from('generalquizzesinfo');
			$this->db->join('topics', 'generalquizzesinfo.topicsId = topics.id', 'left');
			$this->db->group_by("generalquizzesinfo.topicsId"); 
			$this->db->order_by("topics.topicsName", "ASC"); 
			
			return $query = $this->db->get()->result();	    	
			
	    } 
		
		public function getUncompletedQuizzesinfoGroupInList(){
	 			
			$query_str="SELECT generalquizzesinfo.topicsId , COUNT(generalquizzesinfo.id) AS totailQuzzes , topics.topicsName
						from generalquizzesinfo
						LEFT JOIN topics 
						ON generalquizzesinfo.topicsId = topics.id
				        WHERE generalquizzesinfo.id NOT IN (SELECT generalQuizzesInfo FROM eduquestion)
					    GROUP BY generalquizzesinfo.topicsId
						ORDER BY topics.topicsName ASC";
			
			return $query = $this->db->query($query_str)->result();
			
	    }
		
		public function getCompletedQuizzesinfoGroupInList(){
	 			
			$query_str="SELECT generalquizzesinfo.topicsId , COUNT(generalquizzesinfo.id) AS totailQuzzes , topics.topicsName
						from generalquizzesinfo
						LEFT JOIN topics 
						ON generalquizzesinfo.topicsId = topics.id
				        WHERE generalquizzesinfo.id IN (SELECT generalQuizzesInfo FROM eduquestion) AND generalquizzesinfo.id NOT IN (SELECT generalQuizInfoId FROM candidate)
					    GROUP BY generalquizzesinfo.topicsId
						ORDER BY topics.topicsName ASC";
			
			return $query = $this->db->query($query_str)->result();
			
	    }
	    
	    public function getCompletedQuizzesinfoGroupInListForClient(){
	 			
			$query_str="SELECT generalquizzesinfo.topicsId , COUNT(generalquizzesinfo.id) AS totailQuzzes , topics.topicsName
						from generalquizzesinfo
						LEFT JOIN topics 
						ON generalquizzesinfo.topicsId = topics.id
				        WHERE generalquizzesinfo.id IN (SELECT generalQuizzesInfo FROM eduquestion)
					    GROUP BY generalquizzesinfo.topicsId
						ORDER BY topics.topicsName ASC";
			
			return $query = $this->db->query($query_str)->result();
			
	    }
	    
	    public function getFreezesQuizzesinfoGroupInList(){
	 			
			$query_str="SELECT generalquizzesinfo.topicsId , COUNT(generalquizzesinfo.id) AS totailQuzzes , topics.topicsName
						from generalquizzesinfo
						LEFT JOIN topics 
						ON generalquizzesinfo.topicsId = topics.id
				        WHERE generalquizzesinfo.id IN (SELECT generalQuizInfoId FROM candidate)
					    GROUP BY generalquizzesinfo.topicsId
						ORDER BY topics.topicsName ASC";
			
			return $query = $this->db->query($query_str)->result();
			
	    }
		
		public function getAllDataBaseOnTopicsId($data){
									
			$this->db->where('topicsId', $data);	
			$this->db->order_by("gQname", "ASC"); 		
			return $query = $this->db->get($this->tableName)->row();	 
						
		}
		
		public function getAllUncompletedBaseOnTopicsId($data){
			$str = "SELECT * FROM $this->tableName WHERE topicsId = $data  AND generalquizzesinfo.id NOT IN (SELECT generalQuizzesInfo FROM eduquestion) ORDER BY gQname ASC";	
			return $query = $this->db->query($str)->result();		
						
		}
		
		public function getAllCompletedBaseOnTopicsId($data){
			$str = "SELECT * FROM $this->tableName 
			WHERE topicsId = $data  
			AND generalquizzesinfo.id IN (SELECT generalQuizzesInfo FROM eduquestion) 
			ORDER BY gQname ASC";	
			return $query = $this->db->query($str)->result();		
						
		}
		
		public function getAllCompletedBaseOnTopicsIdAdmin($data){
			$str = "SELECT * FROM $this->tableName 
			WHERE topicsId = $data  
			AND generalquizzesinfo.id IN (SELECT generalQuizzesInfo FROM eduquestion) 
			AND generalquizzesinfo.id NOT IN (SELECT generalQuizInfoId FROM candidate)
			ORDER BY gQname ASC";	
			return $query = $this->db->query($str)->result();		
						
		}
		
		public function getTopicsIdBaseOnGeneralQuizId($data){
			$str = "SELECT * FROM topics
					LEFT JOIN $this->tableName
					ON $this->tableName.topicsId = topics.id
			        WHERE $this->tableName.id = $data";	
			return $query = $this->db->query($str)->result();		
						 
		}
		
		public function deleteQuizAndAnswer($id){
			 $str ="SELECT answer.id AS answerId , eduquestion.id AS questionId
					FROM answer
					LEFT JOIN eduquestion
					ON answer.questionId = eduquestion.id
					WHERE eduquestion.generalQuizzesInfo = $id
					";
					
			$query = $this->db->query($str)->result();	
			
			foreach($query AS $row){
				$this->db->where($this->where, $row->answerId);
			    $this->db->delete('answer'); 
			    
			    $this->db->where($this->where, $row->questionId);
			    $this->db->delete('eduquestion'); 
			}
			
			$this->db->where($this->where, $id);
			$this->db->delete($this->tableName); 
			
		}
	
}

?>