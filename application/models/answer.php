<?php

	class answer extends CI_Model {
		
		/*
		 * Table Properties 
		   CREATE TABLE answer (
				id INT NOT NULL AUTO_INCREMENT,
		 		generalQuizInfoId INT ,
				questionId INT NOT NULL,
		 		correctAnswer VARCHAR(250), 
				remark VARCHAR(100),
				PRIMARY KEY (id)
			);		
		 * 
		 */

		protected  $tableName ='answer';
		protected  $where ="id";
		protected  $update ="";
		protected  $orderBy ="";
		
		public function __construct() {
		
			parent::__construct ();
		
		}

		// Standed functions Start
		
		public function addData($data){
			
			$d = $topicData = $this->db->insert($this->tableName, $data); 
			if($d == TRUE)	{
				return $d;
			}
			
		}
		
		public function updateData($id,$data){
			
			$this->db->where($this->where, $id);
			$d = $this->db->update($this->tableName, $data); 
			if($d == TRUE)	{
				return $d;
			}
		}
		
		public function deleteData($id){
			
			$this->db->where($this->where, $id);
			$this->db->delete($this->tableName); 
			
		}
		
		public function getAllData(){
			$this->db->order_by($this->orderBy, "ASC"); 			
			return $query = $this->db->get($this->tableName)->result();	 
						
		}
		
		public function getAllDataBaseOnId($data){
									
			$this->db->where($this->where, $data);			
			return $query = $this->db->get($this->tableName)->result();	 
						
		}
		
		public function findDuplicate($data){
			// $data has to be an array
			$this->db->where($data);
			return $query = $this->db->get($this->tableName)->num_rows();
				
		}
		
		// Standed functions Start End
		
		
		public function getId($data){
			// $data has to be an array
			$this->db->where($data);
			//return $query = $this->db->get($this->tableName)->result();
				
		}
				
		public function deleteDataByQuestionId($id){
			
			$this->db->where('questionId', $id);
			$this->db->delete($this->tableName); 
			
		}
		

	}


?>