<?php

    class Allergy extends CI_Model {
       
        protected  $tableName ='allergengroup';
        protected  $where ="allergenGroupMat";
        protected  $update ="";
        protected  $orderBy ="";

        public function __construct() {

            parent::__construct ();

        }

        // Standed functions Start

        public function addData($data){

            $d = $this->db->insert($this->tableName, $data); 
            if($d == TRUE)	{
                return $d;
            }

        }

        public function updateData($id,$data){

            $this->db->where($this->where, $id);
            $d = $this->db->update($this->tableName, $data); 
            if($d == TRUE)	{
                return $d;
            }
        }

        public function deleteData($id){

            $this->db->where($this->where, $id);
            $this->db->delete($this->tableName); 

        }

        public function getAllData(){
            $this->db->order_by($this->orderBy, "ASC"); 			
            return $query = $this->db->get($this->tableName)->result();	

        }

        public function getAllDataBaseOnId($data){
            $this->db->where($this->where, $data);			
            return $query = $this->db->get($this->tableName)->result();	 

        }

        public function findDuplicate($data){
            // $data has to be an array
            $this->db->where($data);
            return $query = $this->db->get($this->tableName)->num_rows();
        }

        // Standed functions Start End    
        
        
        public function getConditionInfoDataBaseOnId($code){
        	 $str ="SELECT description
				FROM icd10camast
				where icd10ca = '".$code."'
				UNION ALL
				SELECT fdbdxDesc as Description
				FROM medicalconditionsmast
				where fdbdx =  '".$code."'";
             return $query = $this->db->query($str)->result();
        }
        
        
        
        /*****************************************************************/
        /*****************************************************************/
        /***********************DisposalInfo*******************************/
        /*****************************************************************/
        /*****************************************************************/
        
        public function addDisposalInfo($data){
        	
        	$d = $this -> db -> insert('disposalinfo',$data);
        	if($d){ return $d; }
        	
        }
        
        public function getAllDisposalInfo(){
        	$str = "SELECT * FROM disposalinfo";
        	return $this->db->query($str)->result();
        }
        
        public function getTotalNumberOfDisposalForPat($patId){
        	$str = 'SELECT COUNT(*) AS num FROM disposalinfo WHERE patId = '.$patId; 
        	return $this -> db -> query($str) -> row();        	
        }
        
        public function getPaginatedInfoForPat($patId,$start,$limit){
        	 $str = "SELECT * FROM disposalinfo WHERE patId = $patId ORDER BY id DESC LIMIT $start, $limit";
        	 return $this -> db -> query($str) -> result();
        }
        
        public function getDisposalInfoBaseOnId($id){
        	$str = "SELECT * FROM disposalinfo WHERE id =$id";
        	return $this -> db -> query($str) -> row();
        }
        
        
       
    }
?>