<?php

    class Pat extends CI_Model {
       
        protected  $tableName ='pat';
        protected  $where ="id";
        protected  $update ="";
        protected  $orderBy ="";

        public function __construct() {

            parent::__construct ();

        }

        // Standed functions Start

        public function addData($data){

                $d = $this->db->insert($this->tableName, $data); 
                if($d == TRUE)	{
                        return $d;
                }

        }

        public function updateData($id,$data){

            $this->db->where($this->where, $id);
            $d = $this->db->update($this->tableName, $data); 
            if($d == TRUE)	{
                return $d;
            }
        }

        public function deleteData($id){

            $this->db->where($this->where, $id);
            $this->db->delete($this->tableName); 

        }

        public function getAllData(){
            $this->db->order_by($this->orderBy, "ASC"); 			
            return $query = $this->db->get($this->tableName)->result();	

        }

        public function getAllDataBaseOnId($data){
            $this->db->where('ID', $data);			
            return $query = $this->db->get($this->tableName)->result();	 

        }
		
		public function getWardName($wardId){
			$this->db->where('NHWardID',$wardId);
			$this->db->where('PatType', 5);
			return $query = $this->db->get($this->tableName)->row();
		}
		
		public function getAllPatInfoBaseOnNHwardId($wardId){
			$this->db->where('NHWardID',$wardId);
			$this->db->where('PatType != ', 5);
			return $query = $this->db->get($this->tableName)->result();
		}
		
		public function getWardAll(){
			$str ="SELECT * FROM $this->tableName WHERE PatType = 5 AND NHID > 0";
			return $query = $this ->db -> query($str)->result();
		}
		
		public function getAllDataBaseOnIdRow($data){
            $this->db->where('ID', $data);			
            return $query = $this->db->get($this->tableName)->row();	 

        }

        public function findDuplicate($data){
            // $data has to be an array
            $this->db->where($data);
            return $query = $this->db->get($this->tableName)->num_rows();
        }

        // Standed functions Start End    
        public function getBirthday(){
            /*
            $str = "SELECT LastName, FirstName, Birthday, FLOOR(DATEDIFF(dd, Birthday, GETDATE()) / 365.25) AS AGE_NOW, FLOOR(DATEDIFF(dd, Birthday, GETDATE() + 15) / 365.25) AS AGE_ONE_WEEK_FROM_NOW
                    FROM Pat
                    WHERE (1 = FLOOR(DATEDIFF(dd, Birthday, GETDATE() + 15) / 365.25) - FLOOR(DATEDIFF(dd, Birthday, GETDATE()) / 365.25)) AND NHID > 0
                   ";
            */
            $str ="DECLARE
                    @StartDate DATETIME,
                    @EndDate DATETIME

                    SET @StartDate = GETDATE() 
                    SET @EndDate   = DATEADD(day,10,GETDATE()) 

                    SELECT

                    LastName,
                    FirstName,
                    Birthday,
                    ID,
                    DATEPART(day,Birthday) as da, 
                    DATEPART(month,Birthday) as mn,
	            DATEPART(month,@StartDate) as cm,
                    DATEPART(year,@StartDate) as yy,
                    DATEDIFF(YY,Birthday, @StartDate) AS AGE_ONE_WEEK_FROM_NOW


                    FROM pat

                    WHERE
                    NHID > 0
                    AND 
                    DATEADD(YEAR, DATEDIFF(YEAR,  Birthday, @StartDate), Birthday) BETWEEN @StartDate AND @EndDate
                    AND
                    DATEADD(YEAR, DATEDIFF(YEAR,  Birthday, @EndDate), Birthday) BETWEEN @StartDate AND @EndDate 
                    ORDER BY LastName ASC";
            
            return $query=$this->db->query($str)->result();
        }
        
        public function getBirthdayBaseOnID($patId){
            /*
            $str = "SELECT LastName, FirstName, Birthday, FLOOR(DATEDIFF(dd, Birthday, GETDATE()) / 365.25) AS AGE_NOW, FLOOR(DATEDIFF(dd, Birthday, GETDATE() + 15) / 365.25) AS AGE_ONE_WEEK_FROM_NOW
                    FROM Pat
                    WHERE (1 = FLOOR(DATEDIFF(dd, Birthday, GETDATE() + 15) / 365.25) - FLOOR(DATEDIFF(dd, Birthday, GETDATE()) / 365.25)) AND NHID > 0
                   ";
            */
           
            $str ="DECLARE
                    @StartDate DATETIME,
                    @EndDate DATETIME

                    SET @StartDate = GETDATE() 
                    SET @EndDate   = DATEADD(day,10,GETDATE()) 

                    SELECT

                    LastName,
                    FirstName,
                    Birthday,
                    ID,
                    DATEPART(day,Birthday) as da, 
                    DATEPART(month,Birthday) as mn,
	            DATEPART(month,@StartDate) as cm,
                    DATEPART(year,@StartDate) as yy,
                    DATEDIFF(YY,Birthday, @StartDate) AS AGE_ONE_WEEK_FROM_NOW


                    FROM pat

                    WHERE
                    NHID > 0 AND ID = '".$patId."'
                    AND 
                    DATEADD(YEAR, DATEDIFF(YEAR,  Birthday, @StartDate), Birthday) BETWEEN @StartDate AND @EndDate
                    AND
                    DATEADD(YEAR, DATEDIFF(YEAR,  Birthday, @EndDate), Birthday) BETWEEN @StartDate AND @EndDate 
                    ORDER BY LastName ASC";
            
            return $query=$this->db->query($str)->result();
        }
        
        public function getPatInfoToAssignDeliveryDetails(){
            $str = "SELECT ID FROM pat WHERE ID NOT IN (SELECT PatID FROM LDeliverySchedule) AND (NHID > 0)";
            
            return $query=$this->db->query($str)->result();
        } 
        
        public function getPatInfoBaseOnId($id){
            $str = "SELECT DISTINCT Pat.ID, Pat.FamilyID, Pat.Code, Pat.LastName, Pat.FirstName, Pat.Address1, Pat.Address2, Pat.City, Pat.Prov, Pat.Postal, Pat.Country, Pat.Birthday, Pat.Sex, Pat.Language, Pat.RxTotalsResetDate, 
                        Pat.TotalDollars, Pat.TotalRx, Pat.Weight, Pat.Height, Pat.CreatedOn, Pat.LastChanged, Pat.LastUsed, Pat.Salutation, Pat.DefKrollCare, Pat.KrollCare, Pat.SnapRequested, Pat.SnapDocumented, 
                        Pat.LargeSig, Pat.FirstDrugName, Pat.SecondDrugName, Pat.DeliveryRoute, Pat.EMail, Pat.AddressLink, Pat.NetworkKeyword, Pat.NHID, Pat.NoKrollCare, Pat.UnitDoseType, 
                        Pat.UnitDoseCycle, Pat.DocID, Pat.PriceGroup, Pat.PrintCompliance, Pat.ARID, Pat.NHWardID, Pat.NHAdmitDate, Pat.NHDischargeDate, Pat.NHDeceasedDate, Pat.NHRoom, Pat.NHBed, Pat.NHLastTMRDate, 
                        Pat.NHInactive, Pat.NHDiet, Pat.NHComment, Pat.AutoRefillStatus, Pat.AutoRefillNotification, Pat.StoreID, Pat.StoreLocal, Pat.LastTMRPrinted, Pat.Active, Pat.UnitDoseStrategyID, Pat.NetworkKeywordDate, 
                        Pat.DefaultNHCycleId, Pat.OCMPin, Pat.IsAnimal, Pat.AnimalType, Pat.NetworkId, Pat.AnimalOwnerPatId, Pat.UnitDosePatPrcGrpId, Pat.MergedToId, Pat.PickupNotificationEnrolment, Pat.LanguageSpoken, 
                        Pat.DeliveryRouteType, Pat.DoubleCount, Pat.NetworkIdRoot, Pat.VialIdentifier, Pat.PatType, Pat.NetworkSynchronizedDate, LDeliverySchedule.ID AS Expr1, LDeliverySchedule.PatID, LDeliverySchedule.Note, 
                        LDeliverySchedule.Deliver, LDeliverySchedule.DeliveryDateTime, LDeliverySchedule.Controlled, LDeliverySchedule.DeliveryHomeID, LDeliverySchedule.PharmacyID, 
                        LDeliverySchedule.DeliveryInstructions, (SELECT TOP 1 Phone FROM PatPhone WHERE PatID IN ($id)) AS Phone
                        FROM pat as Pat LEFT OUTER JOIN
                        LDeliverySchedule ON Pat.ID = LDeliverySchedule.PatID 
                        WHERE (Pat.ID IN ($id))"; 
             
            return $query=$this->db->query($str)->result();
        } 
        
        public function getAllActivePatInfo(){
            $str = "SELECT * FROM pat WHERE (NHID > 0) ORDER BY LastName ASC";            
            return $query=$this->db->query($str)->result();
        } 
        
        public function getAllActivePatInfoNotInLDeliverySheduleTable(){
            $str = "SELECT * FROM pat WHERE ID NOT IN (SELECT PatID FROM LDeliverySchedule) AND (NHID > 0)";            
            return $query=$this->db->query($str)->result();
        }
        
        public function getMedReportBaseOnPatID($id){
             $str ="SELECT PB.BatchNum, PB.StartDate, PB.EndDate, PBR.RxNum, Drg.BrandName, Drg.GenericName
                    FROM PackagerBatch AS PB
                    LEFT JOIN PackagerBatchRx AS PBR
                    ON PBR.PackagerBatchId = PB.Id
                    LEFT JOIN Rx
                    ON Rx.RxNum = PBR.RxNum
                    LEFT JOIN Drg
                    ON Rx.DrgID = Drg.ID
                    WHERE Rx.PatID = ".$id." ORDER BY PB.EndDate DESC";
                    
             return $query=$this->db->query($str)->result();
        }
        
        public function getPatInfoBaseOnLastAndFirstName($lastName, $firstName){
            $str ="SELECT * FROM pat WHERE LastName LIKE '".$lastName."%' and FirstName LIKE '".$firstName."%'";
            return $this->db->query($str)->result();
        }
        
        public function getPatInfoBaseOnLastId($patId){
            $str ="SELECT * FROM pat WHERE NHID > 0 AND ID = '".$patId."'";
            return $this->db->query($str)->result();
        }
        
        public function getPatInfoBaseOnLastIdRow($patId){
           $str ="SELECT * FROM pat WHERE NHID > 0 AND ID = '".$patId."'";
           return $this->db->query($str)->row();
        }
        
        public function getModifiedAndUpdatedPatInfo(){
            $str = "SELECT * FROM pat WHERE NHID > 0 AND LastChanged > dateadd(day,datediff(day,1,GETDATE()),0)";
            return $this->db->query($str)->result();
        }
        
        public function getInfoBaseOnListOfPatID($id){
        	$str ="SELECT * FROM pat WHERE NHID IS NOT NULL AND ID IN ($id)";
        	return $this->db->query($str)->result();
        }
        
        
        // compliancepatient table
        public function findPatId($id){
        	$str = "SELECT * FROM compliancepatient WHERE patID = $id";
			return $this->db->query($str)->result();
        }
		
		public function addComplinceTable($data){
			 $d = $this->db->insert('compliancepatient', $data); 
                if($d == TRUE)	{
                        return $d;
                }
		}
		
		public function updateComplinceTable($id,$data){
							
			$this->db->where('patID', $id);
            $d = $this->db->update('compliancepatient', $data); 
            if($d == TRUE)	{
                return $d;
            }
		}
		
		public function getDeliveryDetails(){
			$str ="SELECT ID, LastName, FirstName, DeliveryRoute, DeliveryRouteType , Code
					FROM pat
					WHERE Pattype = 5 OR NHID =59";
			return $this->db->query($str)->result();
		} 
        
    }
?>
