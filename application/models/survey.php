<?php 


class survey extends CI_Model {

	
   
   /**************************************************************************************/
   /******************************surveyroles**********************************************/
   /**************************************************************************************/
   
   
   
   public function addSurveyRoles($data){
   	   $d = $this -> db -> insert('surveyrols',$data);
   	   if($d){
   	   	   return $d;
   	   }
   }
   
   public function updateSurveyRoles($id,$data){
   	  $this -> db -> where('id',$id);
   	  $d = $this -> db -> update('surveyrols',$data);
   	  if($d) { return $d; }
   }
   
   public function getAutoIncrimentId($data){
   	   $this -> db -> where($data);
   	   $this->db->order_by("id", "desc");
   	   return $this -> db -> get('surveyrols') -> row();
   }
   
   public function getAllServeyRoles(){
   	   $str = "SELECT SR.id, SR.roles, pat.LastName, pat.FirstName FROM surveyrols AS SR
		LEFT JOIN pat ON pat.ID = SR.nhid
		WHERE SR.complete = 0 ORDER BY SR.addDate DESC";
   	   return $this -> db -> query($str) -> result();
   }
   
   public function getDetailsBaseOnId($id){
   	   $str ="SELECT SR.id, SR.roles, pat.LastName, pat.FirstName FROM surveyrols AS SR
		LEFT JOIN pat ON pat.ID = SR.nhid WHERE SR.id =$id";
   	   return $this -> db -> query($str) -> row();
   }
   
   /**************************************************************************************/
   /****************************surveyquestionandanswer***********************************/
   /**************************************************************************************/
   
   public function addQuestionAndAnswer($data){
   	    $d = $this -> db -> insert('surveyquestionandanswer',$data);
   	    if($d) { return $d; }
   }
   
   public function getInfoFromSurveyRolesId($id){
   	    $str = "SELECT * FROM surveyquestionandanswer WHERE surveyrolsId = $id";
   	    return $this -> db -> query($str) -> result();
   }
   
   public function getSurveyIndivually($id){
   	     $str = "SELECT q.questuion, a.details, sa.comment FROM surveyquestionandanswer AS sa
						LEFT JOIN surveyquestion AS q ON  q.id =  sa.surveyquestionId
						LEFT JOIN surveyanswer AS a ON a.id = sa.surveyanswerId
						WHERE sa.surveyrolsId = $id";
   	     return $this -> db -> query($str) -> result();
   }
   
   
   
   
   /**************************************************************************************/
   /****************************surveyquestion********************************************/
   /**************************************************************************************/
       
    
   public function getAllQuestion(){
   	    $str = "SELECT * FROM surveyquestion";
   	    return $this -> db -> query($str) -> result();
   }
   
   public function getOneQuestion(){
   	  $str = "SELECT * FROM surveyquestion LIMIT 1";
   	  return $this -> db -> query($str) -> row();
   }
   
   public function getQuestionNoteInList($idList){
   	   $str = "SELECT * FROM surveyquestion WHERE id IN ($idList) LIMIT 1";
   	   return $this -> db -> query($str) -> row();
   }
   
   
   
   /**************************************************************************************/
   /****************************surveyanswer  ********************************************/
   /**************************************************************************************/
   
   public function getAllAnswer(){
   	   $str = "SELECT * FROM surveyanswer";
   	   return $this -> db -> query($str) -> result();
   }
   
   
   
}
?>