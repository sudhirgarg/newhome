<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the 'Database Connection'
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['dbdriver'] The database type. ie: mysql.  Currently supported:
				 mysql, mysqli, postgre, odbc, mssql, sqlite, oci8
|	['dbprefix'] You can add an optional prefix, which will be added
|				 to the table name when using the  Active Record class
|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['cache_on'] TRUE/FALSE - Enables/disables query caching
|	['cachedir'] The path to the folder where cache files should be stored
|	['char_set'] The character set used in communicating with the database
|	['dbcollat'] The character collation used in communicating with the database
|				 NOTE: For MySQL and MySQLi databases, this setting is only used
| 				 as a backup if your server is running PHP < 5.2.3 or MySQL < 5.0.7
|				 (and in table creation queries made with DB Forge).
| 				 There is an incompatibility in PHP with mysql_real_escape_string() which
| 				 can make your site vulnerable to SQL injection if you are using a
| 				 multi-byte character set and are running versions lower than these.
| 				 Sites using Latin-1 or UTF-8 database character set and collation are unaffected.
|	['swap_pre'] A default table prefix that should be swapped with the dbprefix
|	['autoinit'] Whether or not to automatically initialize the database.
|	['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
|							- good for ensuring strict SQL while developing
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the 'default' group).
|
| The $active_record variables lets you determine whether or not to load
| the active record class
*/


// $active_group = 'pharm';
// $active_record = TRUE;

// $db['pharm']['hostname'] = '10.1.0.4';
// $db['pharm']['username'] = 'iapotheca';
// $db['pharm']['password'] = 'kRka{yaMq5siybxr~e|lvszamsFT7_&#$!~<qaswto|yswdi';
// $db['pharm']['database'] = 'Pharmacy';
// $db['pharm']['dbdriver'] = 'sqlsrv';
// $db['pharm']['dbprefix'] = '';
// $db['pharm']['pconnect'] = FALSE;
// $db['pharm']['db_debug'] = TRUE;
// $db['pharm']['cache_on'] = FALSE;
// $db['pharm']['cachedir'] = '';
// $db['pharm']['char_set'] = 'utf8';
// $db['pharm']['dbcollat'] = 'utf8_general_ci';
// $db['pharm']['swap_pre'] = '';
// $db['pharm']['autoinit'] = TRUE;
// $db['pharm']['stricton'] = FALSE;


$active_group = 'syncfdb';
$active_record = TRUE;

$db['syncfdb']['hostname'] = 'x1xccbchmf.database.windows.net';
$db['syncfdb']['username'] = 'krolladmin';
$db['syncfdb']['password'] = 'Digip0rt';
$db['syncfdb']['database'] = 'FDBWestmount';
$db['syncfdb']['dbdriver'] = 'sqlsrv';
$db['syncfdb']['dbprefix'] = '';
$db['syncfdb']['pconnect'] = FALSE;
$db['syncfdb']['db_debug'] = TRUE;
$db['syncfdb']['cache_on'] = FALSE;
$db['syncfdb']['cachedir'] = '';
$db['syncfdb']['char_set'] = 'utf8';
$db['syncfdb']['dbcollat'] = 'utf8_general_ci';
$db['syncfdb']['swap_pre'] = '';
$db['syncfdb']['autoinit'] = TRUE;
$db['syncfdb']['stricton'] = FALSE;


$active_group = 'sync';
$active_record = TRUE;

$db['sync']['hostname'] = 'x1xccbchmf.database.windows.net';
$db['sync']['username'] = 'krolladmin';
$db['sync']['password'] = 'Digip0rt';
$db['sync']['database'] = 'seamlessnewkrollserverdb';
$db['sync']['dbdriver'] = 'sqlsrv';
$db['sync']['dbprefix'] = '';
$db['sync']['pconnect'] = FALSE;
$db['sync']['db_debug'] = TRUE;
$db['sync']['cache_on'] = FALSE;
$db['sync']['cachedir'] = '';
$db['sync']['char_set'] = 'utf8';
$db['sync']['dbcollat'] = 'utf8_general_ci';
$db['sync']['swap_pre'] = '';
$db['sync']['autoinit'] = TRUE;
$db['sync']['stricton'] = FALSE;


$active_group = 'default';
$active_record = TRUE;

$db['default']['hostname'] = 'localhost';
$db['default']['username'] = 'root';
$db['default']['password'] = 'KN9RHnZ58R9sHHdl1#FiW'; //EFqwgR8)qqsJOqZD@?BA
$db['default']['database'] = 'iardev'; //iardev
$db['default']['dbdriver'] = 'mysqli';
$db['default']['dbprefix'] = '';
$db['default']['pconnect'] = FALSE;
$db['default']['db_debug'] = TRUE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = '';
$db['default']['char_set'] = 'utf8';
$db['default']['dbcollat'] = 'utf8_general_ci';
$db['default']['swap_pre'] = '';
$db['default']['autoinit'] = TRUE;
$db['default']['stricton'] = FALSE;





/* End of file database.php */
/* Location: ./application/config/database.php */

