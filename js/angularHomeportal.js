var myApp = angular.module('myApp',['angularUtils.directives.dirPagination','ng']);

myApp.controller('DoubleController', ['$scope', '$http', '$window', function($scope,$http,$window,$route) {
	
	  $scope.id           = "";
	  $scope.addNew       = false;
	  $scope.changePwd    = true;
	  $scope.assignClient = true;
	  $scope.test         = "Hi Rajah!";
	  $scope.homeIcon     = true;
	  $scope.arrow        = true;
	  $scope.pageSize     = 10;
	  $scope.patName      = "";
	  $scope.DayInfo      = "";
	  $scope.TimeInfo     = "";
	  $scope.addErrorShow = false;
	  $scope.editResolution = true;
	  
	  $scope.homePatient   = true;
	  $scope.individualPat = false;
	  
	  
	  
	  var today = new Date();
	  var dd = today.getDate();
	  var mm = today.getMonth()+1; 
	  var yyyy = today.getFullYear();
	  if(dd<10) {  dd='0'+dd  } 
	  if(mm<10) {  mm='0'+mm   } 
	  
	  $scope.addDate      = yyyy+"-"+mm+"-"+dd;
	  $scope.description  = "";
	  $scope.resolution   = "";
	  $scope.sourceOferror= "";
	  $scope.department   = "";
	  $scope.patId        = "";
	  $scope.typeOferror  = "";
	  $scope.errorId      = "";
	  
	  
	  $scope.eId               = "";
	  $scope.addDate1          = "";
	  $scope.descriptionEdit   = "";
	  $scope.resolutionEdit    = "";
	  $scope.sourceOferrorEdit = "";
	  $scope.departmentEdit    = "";
	  $scope.typeOfErrorEdit   = "";
	  $scope.patNameEdit       = "";
           
      $scope.assignPatToCG = function(data){
      	  $scope.id = data;
      	  $scope.addNew = true;
      	  $scope.assignClient = false;
      	  $scope.changePwd = true;
      	  
      };
      
      $scope.reSetPwd = function(data){
      	      	 
      	  $scope.addNew = true;
      	  $scope.assignClient = true;
      	  $scope.changePwd = false;
      	  
      	  $http.post('getHomePortalUserInfo',{ 'id' : data }).success(function(response){
      	  	  $scope.homePortalUser = response.homeUserInfo;
      	  	  $scope.id = $scope.homePortalUser[0].id;
      	  	  $scope.FirstName = $scope.homePortalUser[0].firstName;
      	  	  $scope.LastName = $scope.homePortalUser[0].lastName;
      	  	
      	  });
      };
      
      $scope.addNewCG = function(){
      	 
      	  $scope.addNew = false;
      	  $scope.assignClient = true;
      	  $scope.changePwd = true;
      };
      
      $scope.getHomeID = function(data){
      	  $scope.test     = data;
      	  $scope.homeIcon = false;
      	  $scope.arrow    = false;
      	  
      	  $http.post('getPatInfoBaseOnHomeID', { 'hId' : data }).success(function(response){
      	  	    $scope.patientInfos = response.patInfo;      	  	
      	  });
      	  
      	  $http.post('getHomeDetailsBaseOnHOmeID',{ 'hId' : data }).success(function(response){
      	  	    $scope.homeInfo = response.homeInfo;
      	  	    $scope.homeName = $scope.homeInfo[0].Name; 
      	  });
      };
      
      $scope.updateDeliveryShedule = function(data){
      	    
            $http.post('getDeliverySheduleInfoById',{ 'id' : data}).success(function(response){
            	 $scope.DeliveryInfo = response.deliverySheduleInfo;
            	 $scope.id           = $scope.DeliveryInfo[0].id ;
            	 $scope.patName      = $scope.DeliveryInfo[0].patName;
            	 $scope.noteInternal = $scope.DeliveryInfo[0].noteInternal;
            	 $scope.deliveryNote = $scope.DeliveryInfo[0].deliveryNote;
            	 $scope.patId        = $scope.DeliveryInfo[0].patId;
            	 $scope.DayInfo      = $scope.DeliveryInfo[0].DayInfo;
            	 $scope.TimeInfo     = $scope.DeliveryInfo[0].TimeInfo;
            });
      	
      };
      
      $scope.updateDeliveryNotes = function(){
      	  
      		      	
      };
      
      
      $scope.LoadPage = function(){
      	  window.location.reload(true);
      };
      
    
      $http.get("getErrorDetails").success(function (response) {
    	 
           $scope.errorDetails               = response.errorDetails;
           $scope.totalNumberOferrorDetails  = response.errorDetails.length;
      });
      
      
     $scope.activeUser =function(data){
     	
     	$http.post('activeInactiveuser', { 'id' : data }).success(function(response){
     		    if(response == 1){
     		    	$scope.LoadPage();  
     		    }      	  	        	  	
      	  });    	 
     };
     
     
     $scope.getEboardNHWardId = function(data){
    	
    	 /*
    	 $scope.homePatient   = true;
		 $scope.individualPat = false;
    	 
    	 $http.post('getEboardDetailsForMiniBoard',{
    		 'wardId' : data,
    		 'dayId'  : day,
    		 'numdays': numdays,
    		 'type'   : type
    	 }).success(function(data){    		 
    		 
    		 $scope.getErrorDetailsByIds  = data.ticketInfo;
    		 $scope.totalError            = data.ticketInfo.length;    		 
    	 }); 
    	 */    	 
     };
     
     
     $scope.getEboardLocalIndividual = function(data){     	
     	 $scope.homePatient   = false;
		 $scope.individualPat = true;
		 
		 $http.post('getEboardInfoBaseOnEboardId',{
			 'id' : data
		 }).success(function(data){
			 
			 $scope.getErrorDetailsByIdEboardIds  = data.ticketInfoById;
    		 $scope.totalErrorEboard              = data.ticketInfoById.length; 
		 });
     };
     
     $scope.complteAllEboard = function(data){
    	
    	 
    	 $http.post('completeEboard',{
    		 'data' : data
    	 }).success(function(data){
    		 if(data == 1){
    			 alert('Successfully compleated');
    			 $scope.LoadPage(); 
    		 }else{
    			 alert('Something went wrong, try again!');
    		 }
    	 });
     };
     
     $scope.goToWeekDay = function(data){   
    	 if(data == 'MON'){ $window.location.href = '/compliance/eBoardByMon'; }
    	 if(data == 'TUE'){ $window.location.href = '/compliance/eBoardByTue'; }
    	 if(data == 'WED'){ $window.location.href = '/compliance/eBoardByWed'; }
    	 if(data == 'THU'){ $window.location.href = '/compliance/eBoardByThu'; }
    	 if(data == 'FRI'){ $window.location.href = '/compliance/eBoardByFri'; }
    	 if(data == 'SAT'){ $window.location.href = '/compliance/eBoardBySat'; }
    	 if(data == 'SUN'){ $window.location.href = '/compliance/eBoardBySun'; }
    	 
     };
     
     $scope.saveError = function(){    	 
    	 
       	
    	 $http.post('addError',{
    		 
    		  'addDate'      : $scope.addDate,
    		  'description'  : $scope.description,
    		  'resolution'   : $scope.resolution,
    		  'sourceOferror': $scope.sourceOferror,
    		  'department'   : $scope.department,
    		  'patId'        : $scope.patId,
    		  'typeOferror'  : $scope.typeOferror,
    		  'errorId'      : $scope.errorId
    		  
    	 }).success(function(data){
    		 alert(data);
    		 $scope.LoadPage();  
    	 });
    	
     };
     
     $scope.editError = function(){
    	     	 
    	 $http.post('updateError',{
    		 'errorId'      : $scope.eId,
    		 'resolution'   : $scope.resolutionEdit
    	 }).success(function(data){
    		  alert(data);
    		  $scope.LoadPage();  
    	 });
     };
     
     $scope.updateError = function(data){
    	 $scope.addErrorShow   = true;
   	     $scope.editResolution = false;
   	
   	     $http.post('getErrorDetailsById',{ 'id' : data }).success(function(response){
   	    	 $scope.errorDetailsInd   = response.errorDetailsIndi;					
             $scope.eId               = $scope.errorDetailsInd[0].id;
             $scope.addDate1          = $scope.errorDetailsInd[0].addDate; 
             $scope.descriptionEdit   = $scope.errorDetailsInd[0].description; 
	       	 $scope.resolutionEdit    = $scope.errorDetailsInd[0].resolution; 
	       	 $scope.sourceOferrorEdit = $scope.errorDetailsInd[0].sourceOferror; 
	       	 $scope.departmentEdit    = $scope.errorDetailsInd[0].depName; 
	       	 $scope.typeOfErrorEdit   = $scope.errorDetailsInd[0].typeOferror; 
	       	 $scope.patNameEdit       = $scope.errorDetailsInd[0].patname; 
   	     });
     };
     
     $scope.addError = function(){
    	 $scope.addErrorShow   = false;
   	     $scope.editResolution = true;
   	     
     };
     
     $scope.getPatInfoBaseOnEboardId = function(data){
    	alert(data);
    	 $http.post('getPatInfoBaseOnEboardId',{ 'id' : data }).success(function(response){
    		 alert(response);
    	 });
     };
     
     
   
     
     /*
     $http.get('getDisposalDetails').then(function mySucces(response) {
    	 $scope.disposalDetails               = response.disposalDetailsInfo;
         $scope.totalNumberOfdisposalDetails  = response.disposalDetailsInfo.length; 
         $scope.statuscode = response.status;
     }, function myError(response) {
         $scope.myWelcome = response.statusText;
         $scope.statuscode = response.status;
     });    
    
     
     $http.get("getDisposalDetails").success(function (response) {
    	 $scope.disposalDetails               = response.disposalDetailsInfo;
         $scope.totalNumberOfdisposalDetails  = response.disposalDetailsInfo.length; 
    });
     */
     
     
     
     
}]);

