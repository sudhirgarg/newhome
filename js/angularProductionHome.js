var myApp = angular.module('myApp',['angularUtils.directives.dirPagination','ng']);

myApp.controller('proController', ['$scope', '$http', '$window', function($scope,$http,$window) {
	
	  $scope.id           = "";
	  $scope.test         = "Hi Production!";
	  $scope.pageSize     = 10;
	  
	  $scope.home         = "";
	  $scope.batchNum     = "";
	  $scope.orderNum     = "";
	  $scope.delivery     = "";
      
     
     
    $http.get("getInfoToPacMed").success(function (response) {
        $scope.pacMeds = response.pacMed;
        $scope.totalPacMed  = response.pacMed.length;
    });
    
    $scope.refreshPacMedOrder = function(){
        $http.get("getInfoToPacMed").success(function (response) {
            $scope.pacMeds = response.pacMed;
            $scope.totalPacMed  = response.pacMed.length;
        });
    };
    
    $http.get("getInfoToPacVision").success(function (response) {
        $scope.pacVisions = response.pacVision;
        $scope.totalPacVision  = response.pacVision.length;
    });
    
    $scope.refreshPacMedOrder = function(){
        $http.get("getInfoToPacVision").success(function (response) {
            $scope.pacVisions = response.pacVision;
            $scope.totalPacVision  = response.pacVision.length;
        });
    };
    
    $http.get("getInfoToPharmacist").success(function (response) {
        $scope.pharmacists = response.pharmacist;
        $scope.totalPharmacist  = response.pharmacist.length;
    });
    
    $scope.refreshPacMedOrder = function(){
        $http.get("getInfoToPharmacist").success(function (response) {
            $scope.pharmacists = response.pharmacist;
            $scope.totalPharmacist  = response.pharmacist.length;
        });
    };
    
    $http.get("getInfoToFinalCheck").success(function (response) {
        $scope.deliverysFinals = response.deliveryFinal;
        $scope.totalFinalDelivery  = response.deliveryFinal.length;
    });
    
    $scope.refreshPacMedOrder = function(){
        $http.get("getInfoToFinalCheck").success(function (response) {
            $scope.deliverysFinals      = response.delivery;
            $scope.totalFinalDelivery  = response.delivery.length;
        });
    };
    
    $http.get("getInfoToDelivery").success(function (response) {
        $scope.deliverys = response.delivery;
        $scope.totalDelivery  = response.delivery.length;
    });
    
    $scope.refreshPacMedOrder = function(){
        $http.get("getInfoToDelivery").success(function (response) {
            $scope.deliverys      = response.delivery;
            $scope.totalDelivery  = response.delivery.length;
        });
    };
    
    
    $scope.confirmPacMedOneTouch = function(data){
    	alert(data);
    };
    
    $scope.confirmPacVisionOneTouch = function(data){
    	alert(data);
    };
    
    $scope.confirmPharmacistOneTouch = function(data){
    	alert(data);
    };
    
    $scope.deleteBatch = function(data){
    	
    	$http.post('deleteBatch',{ 'batchNum' : data}).success(function(response){
    		alert(response);
    	});
    } 
     
     
}]);